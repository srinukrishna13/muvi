jQuery(document).ready(function($){ 
    videoHeight();
 });
$(window).on('load', function(){
        videoHeight();

});
$(window).on('resize', function(){
	$('.cust-row > div').css('height', '');
        videoHeight();

}); 
function videoHeight() {
	var desWidth = $(".carousel-inner").innerWidth();
	var widthTest = $('#widthImage').val();
	var heightTest = $('#heightImage').val();
	var windWidth = $(window).width();
	var isIE = false;
	if (navigator.appVersion.toUpperCase().indexOf("MSIE") != -1 ||
			navigator.appVersion.toUpperCase().indexOf("TRIDENT") != -1 ||
			navigator.appVersion.toUpperCase().indexOf("EDGE") != -1) {
		isIE = true;
	}
	var desHeight = desWidth / 2.67;
	if(windWidth < 400){
		var ratio = widthTest/heightTest;
		heightTest = windWidth / ratio;
		if ($('#background').length > 0){
			$(".item").each(function () {
				if (isIE == false) {
					$(this).find('.videocontainer').css({'height': heightTest});
					$(this).find(".slide-image").css({'height': heightTest});
				} else {
					$(this).find('#background').css({
						'width': desWidth,
					});
					$(this).find('.videocontainer').css({'height': desHeight});
					$(this).find(".slide-image").css({'height': desHeight});
				}
			});
		}
	}else{
		if ($('#background').length > 0){
			$(".item").each(function () {
				if (isIE == false) {
					$(this).find('.videocontainer').css({'height': desHeight});
					$(this).find(".slide-image").css({'height': desHeight});
				} else {
					if(heightTest == widthTest){
						$(this).find('#background').css({
							'height': desHeight,
						});
					}
					$(this).find('.videocontainer').css({'height': desHeight});
					$(this).find(".slide-image").css({'height': desHeight});
				}	
			});
		}
	};
}


