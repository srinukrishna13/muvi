$(document).ready(function () {
var log_id_temp = 0;
var length = 0;
var resume_time = 0;
var tTime = 0;
var previousTime = 0;
var currentTime = 0;
var log_seekStart = null;
var check_played_from ='';
if(typeof played_from != 'undefined'){
    check_played_from = played_from;
}
	player.ready(function () {
		if(is_live == 0){
			videoFullLength = player.duration();
		}
		var log_check = 1;
		player.on("play", function() {
			if(log_check == 1){
				resume_time = player.currentTime();
				var play_time = player.currentTime();
				if((typeof adavail != 'undefined' && play_status == 0) || (typeof adavail == 'undefined')) {

					if ((started === 0 && typeof adavail != 'undefined' && (adavail === 0 || adavail === 2)) || (started === 0 && typeof adavail == 'undefined')){
						log_check = 0;
						if(typeof play_length != 'undefined' && play_length > 0) {
							play_time = play_length;              
						}
						tTime = player.currentTime();
						$.post(video_view_log_url, { log_id_temp:log_id_temp,movie_id: movie_id, episode_id: stream_id, status: "start", log_id: log_id,percent: percen,played_length: 0, video_length: player.duration(),'studio_id' : studio_id , 'resume_time':resume_time ,'played_from': check_played_from}, function(res) {
							if (res['log_id'] > 0){       
								log_id_temp = res['log_id_temp'];
															log_id = res['log_id'];
							}
							else {
								history.go(-1);
							}
						},'json');
								$.post('/user/setWatchPeriod',{movie_id: movie_id,'studio_id' : studio_id},function(res){
								});
						started = 1;
						ended = 0;
						logged = 0
					}
				}
			}
		});
		if(is_live == 0){
			player.on("ended", function() {
				if ((ended === 0 && typeof adavail != 'undefined' && (adavail === 0 || adavail === 2)) || (started === 0 && typeof adavail == 'undefined')|| (ended === 0 && typeof adavail == 'undefined')) {
					$.post(video_view_log_url, {log_id_temp:log_id_temp,movie_id: movie_id, episode_id: stream_id, status: "complete", log_id: log_id, played_length: (player.currentTime()-tTime),'studio_id' : studio_id , 'resume_time':tTime, 'played_from': check_played_from}, function(res) {
						log_id_temp = 0;
						log_id = 0;
					}, 'json');
					ended = 1;
					started = 0;
				}
			});
		}
		player.on('timeupdate', function () {
			previousTime = currentTime;
			currentTime = player.currentTime();
			if(log_check == 1){
				resume_time = player.currentTime();
				var play_time = player.currentTime();
				if((typeof adavail != 'undefined' && play_status == 0) || (typeof adavail == 'undefined')) {
					if ((started === 0 && typeof adavail != 'undefined' && (adavail === 0 || adavail === 2)) || (started === 0 && typeof adavail == 'undefined')){
				log_check = 0;
						if(typeof play_length != 'undefined' && play_length > 0) {
							play_time = play_length;              
						}
						tTime = player.currentTime();
						$.post(video_view_log_url, { log_id_temp:log_id_temp,movie_id: movie_id, episode_id: stream_id, status: "start", log_id: log_id,percent: percen,played_length: 0, video_length: player.duration(),'studio_id' : studio_id , 'resume_time':resume_time ,'played_from': check_played_from}, function(res) {
							if (res['log_id'] > 0){       
								log_id_temp = res['log_id_temp'];
															log_id = res['log_id'];
							}
							else {
								history.go(-1);
							}
						},'json');
								$.post('/user/setWatchPeriod',{movie_id: movie_id,'studio_id' : studio_id},function(res){
								});
						started = 1;
						ended = 0;
						logged = 0
					}
				}
			}  
		});
		setInterval(function(){
                        length =  player.currentTime() - tTime;
			updateViewLogIntervals();
		}, 60000);
		$("video").on('seeking', function() {
			if(log_seekStart === null && log_id >= 1) {
				log_seekStart = previousTime;
				length = log_seekStart-tTime;
				updateViewLogIntervals();
			}
		});

		$("video").on('seeked', function () {
			if (started === 1 && log_id >= 1 ) {
				log_id_temp=0;
				log_seekStart=null;
				length=0;
				updateViewLogIntervals();
				tTime=player.currentTime();
			}
		});
		
	});
	function updateViewLogIntervals (){
		if(length < 0 ){
			length = 0;
		}  
		if ((started === 1 && ended === 0 && typeof adavail != 'undefined' && (adavail === 0 || adavail === 2)) || (started === 1 && ended === 0 && typeof adavail == 'undefined')) {
			var curlength = player.currentTime();
			if(is_live != 0){
				percen = Math.round((curlength * 100)/videoFullLength);
			}
			$.post(video_view_log_url, {log_id_temp:log_id_temp,movie_id: movie_id, episode_id: stream_id, status: "halfplay", log_id: log_id,percent: percen, played_length: length,'studio_id' : studio_id ,'resume_time':player.currentTime(), 'played_from': check_played_from}, function(res) {
				log_id_temp = res['log_id_temp'];
				log_id = res['log_id'];
			}, 'json');
		}
	}
});