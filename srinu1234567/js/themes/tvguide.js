/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getchannel_detail(channel_id,image_path)
{
    //alert();'
     $('#player-loading').show();
    $(".allchannel").removeClass("active-channel");
    $('.allchannel').addClass("channel-front");
    $("#allchannel_"+channel_id).removeClass("channel-front");
    $("#phase_checking").val(channel_id);   
    $('#allchannel_'+channel_id).addClass("active-channel");
    $.ajax({
        url: HTTP_ROOT + '/tvguide/getChannelWiseDetail',
        type: "POST",
        data: {'channel_id': channel_id},
        dataType: 'json',
        success: function (data) {
           
            if(data.movie_id!=null && data.start_time!=null && data.end_time!=null){
            $("#checkdefault").val(0);
            var movie_id = data.movie_id;
            var start_time = data.start_time;
            var end_time = data.end_time; 
            $('#player-loading').show();
            $('#poster-loading').show();
            $.ajax({
                url: HTTP_ROOT + "/tvguide/getvideodtls",
                data: {movie_id: movie_id, start_time: start_time, end_time: end_time},
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.permalink != '') {
                        $('#poster-loading').hide();
                        $('#content_story').html(data.content_short_description);
                        $('#content_poster').attr('src', data.content_poster);
                        $('#content_title').html(data.content_title);
                        $.ajax({
                            type: 'POST',
                            url: HTTP_ROOT + '/TvGuidePlayer/' + data.permalink + '?start_time=' + data.start_time+'&old_value=2',
                            success: function (res) {
                                
                                $("#checkdefault").val(0);
                                $("#video_player").html("");
                                $('#video_player').html(res);
                                $('#player-loading').hide();
                            }
                        });
                    } else {
                        $("#checkdefault").val(1);
                        $('#poster-loading').hide();
                        //$('#player-loading').hide();
                        $('#content_story').html("");
                       // $('#content_poster').attr('src', THEME_URL + "/images/livetv-thumb.jpg");
                        var vjsscript = '<script data-cfasync="false" type="text/javascript" src="'+HTTP_ROOT+'/vast/js/video.js"></script><link href="'+HTTP_ROOT+'/vast/css/videojs.vast.css" rel="stylesheet" type="text/css"><link href="'+HTTP_ROOT+'/vast/css/video.js.css" rel="stylesheet" type="text/css"><script data-cfasync="false" type="text/javascript">    videojs.options.flash.swf = "'+HTTP_ROOT+'/js/video-js.swf";</script>';
                        $("#video_player").html("");
			$('#player-loading').hide();
                        $("#video_player").html(vjsscript+'<video id="video_block1" class="video-js vjs-default-skin vjs-16-9 " preload="none" width="auto" height="auto" ></video>');
                        var playerSetup = videojs('video_block1');
                        playerSetup.ready(function () {
                            var player = this;
                            //$('.vjs-loading-spinner').show();
                            $('.vjs-loading-spinner').hide();
                            $('.vjs-control-bar').show();
                            $('.vjs-control-bar').css('display','block');
                            $('.vjs-default-skin .vjs-control-bar').css('z-index','100');
                            $('.vjs-play-control').css('pointer-events','none');
                            $('.vjs-progress-control ').css('pointer-events','none');
                            $("#video_block1_html5_api").attr('poster', image_path);
                            $("#video_block1").attr('poster', image_path);
                            $( ".video-js" ).attr('style','padding-top:47%; height:50%;');
                            player = this;
                            player.load();
                            $(".vjs-mute-control").css("display","none"); 
                 $(".vjs-volume-control").css("display","none");
                 $(".vjs-fullscreen-control").css("display","none");
                        });
                    }
                },
            });
        }
        else
        {
       
        $("#checkdefault").val(1);
        //$('#player-loading').hide();   
        var vjsscript = '<script data-cfasync="false" type="text/javascript" src="'+HTTP_ROOT+'/vast/js/video.js"></script><link href="'+HTTP_ROOT+'/vast/css/videojs.vast.css" rel="stylesheet" type="text/css"><link href="'+HTTP_ROOT+'/vast/css/video.js.css" rel="stylesheet" type="text/css"><script data-cfasync="false" type="text/javascript">    videojs.options.flash.swf = "'+HTTP_ROOT+'/js/video-js.swf";</script>';
        $("#video_player").html("");
        $('#player-loading').hide();   
        $("#video_player").html(vjsscript+'<video id="video_block1" class="video-js vjs-default-skin vjs-16-9 " preload="none" width="auto" height="auto" ></video>');
        var playerSetup = videojs('video_block1');
        playerSetup.ready(function () {
            var player = this;
            $('.vjs-loading-spinner').hide();
            $('.vjs-control-bar').show();
            $('.vjs-control-bar').css('display','block');
            $('.vjs-default-skin .vjs-control-bar').css('z-index','100');
            $('.vjs-play-control').css('pointer-events','none');
            $('.vjs-progress-control ').css('pointer-events','none');
            $("#video_block1_html5_api").attr('poster', image_path);
            $("#video_block1").attr('poster', image_path);
            $( ".video-js" ).attr('style','padding-top:47%; height:50%;');
            player = this;
            player.load();
            $(".vjs-mute-control").css("display","none"); 
                 $(".vjs-volume-control").css("display","none");
                 $(".vjs-fullscreen-control").css("display","none");
        });
            }
        
        }
    });
   
           

}
