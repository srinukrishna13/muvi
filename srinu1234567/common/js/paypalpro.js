function paypalpro () {
    
    this.processCard = function() {
        $('#register_membership').html(JSLANGUAGE.wait);
        $('#register_membership').attr('disabled', 'disabled');
        var url = HTTP_ROOT+"/user/processCard";
        var card_name = $('#card_name').val();
        var card_number = $('#card_number').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();

        if($.trim($('#email_address').val())){
            var email = $('#email_address').val();
        }else{
            var email = $('#email').val();
        }
        var plan_id = 0;
        if ($('#plan_id').length) {
            plan_id = $('#plan_id').val();
        }else if($('#plandetail_id').length){
            plan_id = $('#plandetail_id').val();
        }

        var currency_id = 0;
        if ($('#currency_id').length) {
            currency_id = $('#currency_id').val();
        }

        var ppv_plan = $('#ppv_plan').val();
        if(ppv_plan){
           $('#paynowbtn').html(JSLANGUAGE.wait);
            $('#paynowbtn').attr('disabled', 'disabled');
        }
        if ($('#have_paypal_pro').is(":checked")) {
            var have_paypal_pro = 1;
            $("#paypalproPopup").modal('show');
        }else{
            var have_paypal_pro = 0;
            $("#loadingPopup").modal('show');
            if ($("#ppvModalPricingMain").length) {
                $("#ppvModalPricingMain").addClass('fade');
            }
        }
        var timeframe_id = $("#timeframe_id").val();
        $("#card_div").append("<input type='hidden' name='data[payment_method]' value='paypalpro' />");
        $.post(url, {'email': email, 'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv,'plan_id': plan_id,'ppv':ppv_plan,'havePaypal':have_paypal_pro, 'currency_id': currency_id, 'timeframe_id':timeframe_id}, function (data) {

            if (parseInt(data.isSuccess) === 1) {
                if(!have_paypal_pro){
                    $("#loadingPopup").modal('hide');
                    $("#successPopup").modal('show');
                    if (data.card) {
                        $("#card_div").append("<input type='hidden' name='data[havePaypal]' value='0' />");
                        for (var i in data.card) {
                            $("#card_div").append("<input type='hidden' name='data[" + i + "]' value='" + data.card[i] + "' />");
                        }
                        $("#card_div").append("<input type='hidden' name='data[payment_method]' value='paypalpro' />");
                    }
                    setTimeout(function () {
                        document.membership_form.action = HTTP_ROOT+"/user/" + action;
                        document.membership_form.submit();
                        return false;
                    }, 5000);
                }else{
                    $('#payment_method').val('paypalpro');
                    $("#card_div").append("<input type='hidden' name='data[havePaypal]' value='1' />");
                    document.membership_form.action = HTTP_ROOT+"/user/" + action;
                    document.membership_form.submit();
                }
            } else {
                $("#loadingPopup").modal('hide');
                if ($("#ppvModalPricingMain").length) {
                    $("#ppvModalPricingMain").removeClass('fade');
                }
                $('#register_membership').html(btn);
                $('#register_membership').removeAttr('disabled');
                if($("#paypal").length){
                    $("#paypal").removeAttr("disabled");
                }
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                }
            }
        }, 'json');
    }

    this.updateCard = function (){
        var card_name = $('#card_name').val();
        var card_number = $('#card_number').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();
        $("#loadingPopup").modal('show');
        var url = HTTP_ROOT+"/user/updateCard";
        $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv}, function (data) {
            var obj = JSON.parse(data);
            if (parseInt(obj.isSuccess) === 1) {
                $("#loadingPopup").modal('hide');
                $("#successPopup").modal('show');
                setTimeout(function () {
                    window.location.reload();
                    return false;
                }, 5000);
            }else{
                $("#loadingPopup").modal('hide');
                $("#errorPopup").modal('show');
                setTimeout(function () {
                    $("#errorPopup").modal('hide');
                }, 5000);
            }
        });
    }
    
    pciDigitalPayment = function() {
        var _url = HTTP_ROOT + '/userPayment/setHostedData';
        var session_url = (window.location != window.parent.location) ? document.referrer : document.location.href;
        var use_coupon = $.trim($("#coupon_use").val());
                var coupon_code = '';
                var coupon_currency_id = 0;
                if(use_coupon === "1"){
                    coupon_code =  $.trim($("#coupon").val());
                    coupon_currency_id =  $("#currency_id").val();
                }
                var payment_method =  $("#payment_method").val();
                
                var movie_id = $("#ppvmovie_id").val();
                var season_id = 0;
                var episode_id = 0;
                var isadv = 0;
                
                var is_show = $("#is_show").val();
                var is_season = $("#is_season").val();
                var is_episode = $("#is_episode").val();
                var content_types_id = $("#content_types_id").val();
                var timeframe_id = $("#timeframe_id").val();
                if($.trim($("#isadv").val())){
                    isadv = $("#isadv").val();
                }else if($.trim($("#is_adv_plan").val())){
                     isadv = $("#is_adv_plan").val();
                }
                var purchase_type = "";
                
                var season_title = '';
                var episode_title = '';
                
                if ($("#showtext").is(':checked')) {
                     purchase_type = "show";
                } else if (parseInt(is_season) && $("#seasontext").is(':checked')) {
                    season_id = $.trim($("#seasonval").val());
                    episode_id = 0;
                    purchase_type = "season";
                    
                    season_title = $("#seasonval option:selected").text();;
                } else if (parseInt(is_episode) && $("#episodetext").is(':checked')) {
                    season_id = $.trim($("#seasonval1").val());
                    episode_id = $.trim($("#episodeval").val());
                    purchase_type = "episode";
                    
                    season_title = $("#seasonval1 option:selected").text();
                    episode_title = $("#episodeval option:selected").text();
                }
        var plan_id = $("#plandetail_id").val();
        var permalink = $("#permalink").val();
        var coupon = $("#coupon").val();
        $.post(_url, {'session_url':session_url,'permalink':permalink,'plan_id':plan_id,'movie_id': movie_id, 'season_id': season_id, 'episode_id': episode_id, 'purchase_type': purchase_type, 'content_types_id': content_types_id, 'coupon_code' : coupon_code,'coupon_currency_id' : coupon_currency_id, 'payment_method': payment_method, 'isadv': isadv,'coupon':coupon, 'timeframe_id':timeframe_id}, function (data) {
            var data = JSON.parse(data);
            $("#pci_amount").val(data.amount);
            $("#pci_currency_code").val(data.currency_code);
            $("#invoice").val(data.cart_unique_id);
            $("#billing_first_name").val(data.first_name);
            $("#billing_last_name").val(data.last_name);
            $("#buyer_email").val(data.email);
            $("#first_name").val(data.first_name);
            $("#last_name").val(data.last_name);
            $("#return").val(data.return);
            $("#cancel_return").val(data.cancel_return);
            document.form_iframe.submit();
            setTimeout(function(){
                $('#loader-ppv').hide();
                $('#paynowbtn').parent().parent().parent().hide();
                $('#iframeContainer').show();
            },4000);
        });
    };
    
    pciPhysicalPayment = function() {
        var pg_amount = $('#pg_amount').val();
        var coupon = $('#coupon').val();
        var pg_currency_id = $('#pg_currency_id').val();
        var pg_shipping_cost = $('#pg_shipping_cost').val();
        var url = HTTP_ROOT + '/shop/getCurrency';
        $.post(url, {'currency_id': pg_currency_id,'coupon':coupon,'amount':pg_amount,'shipping_cost':pg_shipping_cost}, function (res) {
            var data = $.parseJSON(res);
            if(data.amount > 0){
            $("#pci_amount").val(data.amount);
            $("#pci_currency_code").val(data.currency_code);
            $("#invoice").val(data.cart_unique_id);
            $("#billing_first_name").val(data.first_name);
            $("#billing_last_name").val(data.last_name);
            $("#buyer_email").val(data.email);
            $("#first_name").val(data.first_name);
            $("#last_name").val(data.last_name);
            $("#return").val(data.return);
            $("#cancel_return").val(data.cancel_return);
            document.form_iframe.submit();
            setTimeout(function(){
                $('#pci_pay').hide();
                $('#payformdiv').hide();
                $('#iframeContainer').show();
                $('.loader_cart').hide();
            }, 7000);
            }else{
                if (data) {
                    for (var i in data) {
                        $("#payform").append("<input type='hidden' name='data[" + i + "]' value='" + data[i] + "' />");
                    }
                }
                setTimeout(function () {
                    document.payform.action = HTTP_ROOT + "/userPayment/saveOrder";
                    document.payform.submit();
                    return false;
                }, 5000);
            }
        });
    };
    
    this.pciPayment = function(goods_type) {
        
        if($.trim(goods_type) === 'digital_payment'){
            pciDigitalPayment();
        }else{
            pciPhysicalPayment();
        }
         
    };
    
};