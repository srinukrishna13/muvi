function sisp () {
    
    this.processCard = function() {
        var _url = "/user/SispProcessCard";
        var session_url = (window.location != window.parent.location) ? document.referrer : document.location.href;
        var use_coupon = $.trim($("#coupon_use").val());
                var coupon_code = '';
                var coupon_currency_id = 0;
                if(use_coupon === "1"){
                    coupon_code =  $.trim($("#coupon").val());
                    coupon_currency_id =  $("#currency_id").val();
                }
                var currency_id=$("#currency_id").val();
                var payment_method =  $("#payment_method").val();
                //card details starts
                  var card_name =  $("#card_name").val();
                  var card_number =  $.trim($("#card_number").val());
                  var exp_month =  $("#exp_month").val();
                  var exp_year =  $("#exp_year").val();
                  var security_code =  $.trim($("#security_code").val());
                  //end here 
                
                var movie_id = $("#ppvmovie_id").val();
                var season_id = 0;
                var episode_id = 0;
                var isadv = 0;
                
                var is_show = $("#is_show").val();
                var is_season = $("#is_season").val();
                var is_episode = $("#is_episode").val();
                var content_types_id = $("#content_types_id").val();
                var timeframe_id = $("#timeframe_id").val();
               if($.trim($("#isadv").val())){
                isadv = $("#isadv").val();
                }else if($.trim($("#is_adv_plan").val())){
                     isadv = $("#is_adv_plan").val();
                }
                var purchase_type = "";
                
                var season_title = '';
                var episode_title = '';
				
                if ($("#showtext").is(':checked')) {
                     purchase_type = "show";
                } else if (parseInt(is_season) && $("#seasontext").is(':checked')) {
                    season_id = $.trim($("#seasonval").val());
                    episode_id = 0;
                    purchase_type = "season";
                    
                    season_title = $("#seasonval option:selected").text();;
                } else if (parseInt(is_episode) && $("#episodetext").is(':checked')) {
                    season_id = $.trim($("#seasonval1").val());
                    episode_id = $.trim($("#episodeval").val());
                    purchase_type = "episode";
                    
                    season_title = $("#seasonval1 option:selected").text();
                    episode_title = $("#episodeval option:selected").text();
                }
        
        
        //var data = $('#membership_form').serialize();
        var plan_id = $("#plandetail_id").val();
        var permalink = $("#permalink").val();
        var coupon = $("#coupon").val();
        $('.loader').show();
        $.post(_url, {'session_url':session_url,'permalink':permalink,'plan_id':plan_id,'movie_id': movie_id, 'season_id': season_id, 'episode_id': episode_id, 'purchase_type': purchase_type, 'content_types_id': content_types_id, 'coupon_code' : coupon_code,'coupon_currency_id' : coupon_currency_id, 'payment_method': payment_method, 'isadv': isadv,'coupon':coupon, 'timeframe_id':timeframe_id,'card_name':card_name,'card_number':card_number,'exp_month':exp_month,'exp_year':exp_year,'security_code':security_code,'currency_id':currency_id}, function (data) {
            var res = JSON.parse(data);
            if (parseInt(res.isSuccess) === 1) {
            $.each(res, function(key, value){
            $("#membership_form").append("<input type='hidden' name='"+ key +"' value='" + value + "' />");
             });
              if (confirm(JSLANGUAGE.confirm_sisp_payment)) {
                setTimeout(function () {
                    document.membership_form.action = res.endpoint;
                    document.membership_form.submit();
                    return false;
                }, 3000);
            }else{
              location.reload();   
              return false;  
            }
            }else{
                $('#card-info-error').show().html('We are not able to process the transaction. Please try after sometime.');
            }
        });
    };
}