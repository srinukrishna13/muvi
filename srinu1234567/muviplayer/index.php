<?php
$opt = isset($_REQUEST['ad_type']) ? $_REQUEST['ad_type'] : 'vpaid';
?>

<!DOCTYPE html>
<!--[if lte IE 9]><html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <title>Muvi Studio Player</title>
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">

        <!-- DEMO required styles-->
        <link rel="stylesheet" type="text/css" href="styles/normalize.css"/>
        <link rel="stylesheet" type="text/css" href="styles/skeleton.css"/>
        <link rel="stylesheet" type="text/css" href="styles/video-js.css"/>
        <link rel="stylesheet" type="text/css" href="styles/messages.css"/>
        <link rel="stylesheet" type="text/css" href="styles/demo.css"/>

        <!-- Fonts -->
        <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

        <!-- CSS files -->
        <link rel="stylesheet" type="text/css" href="styles/ads-label.css"/>
        <link rel="stylesheet" type="text/css" href="styles/black-poster.css"/>
        <link rel="stylesheet" type="text/css" href="styles/videojs.vast.css"/>
        <link rel="stylesheet" type="text/css" href="styles/videojs.vpaid.css"/>


        <script type="text/javascript">
            // this is mandatory for IE8 !!!!!
            document.createElement('video');
            document.createElement('audio');
            document.createElement('track');
        </script>

        <!-- DEMO required scripts -->
        <script type="text/javascript" src="scripts/es5-shim.js"></script>
        <script type="text/javascript" src="scripts/ie8fix.js"></script>
        <script type="text/javascript" src="scripts/swfobject.js"></script>
        <script type="text/javascript" src="http://studio.muuvi.com/js/video.js"></script>
        <script type="text/javascript" src="scripts/videojs-vast-vpaid.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script type="text/javascript" src="scripts/demo_bundle.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function () {
                updateDemo();
            });


            function updateDemo() {
                var adPluginOpts = {
                    "plugins": {
                        "ads-setup": {
                            "adCancelTimeout": 20000, // Wait for ten seconds before canceling the ad.
                            "adsEnabled": true
                        }
                    }
                };

                var ad_src = '';
                var playerHeight = document.getElementById('video_block').clientHeight;
                var playerWidth = document.getElementById('video_block').clientWidth;

                ad_src = 'https://search.spotxchange.com/vast/2.00/79391?VPAID=1&content_page_url=' + encodeURIComponent(window.location.href) + '&cb=' + Math.random() + '&player_width=' + playerWidth + '&player_height=' + playerHeight;

                //  ad_src = 'https://search.spotxchange.com/vast/2.00/79391?VPAID=0&content_page_url='+encodeURIComponent(window.location.href)+'&cb='+ Math.random() +'&player_width='+playerWidth+'&player_height='+playerHeight;
                adPluginOpts.plugins["ads-setup"].adTagUrl = ad_src;
                player = videojs('#video_block', adPluginOpts);
            }
        </script>
    </head>
    <body>

        <h1 class="title main">Muvi Player</h1>

        <div class="container">
            <div class="row">
                <div class="twelve columns">
                    <div class="vjs-video-container">
                    <video id="video_block" class="video-js vjs-default-skin" controls autoplay preload="auto" poster="" width="auto" height="auto" data-setup='{"techOrder": ["html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "144" } }}'>
                        <source src="https://s3.amazonaws.com/muvistudio/Ramnavami_2010.mp4" data-res="144" type="video/mp4" />
                    </video>
                    </div>
                </div>        
            </div>
        </div>
    </body>

</html>
