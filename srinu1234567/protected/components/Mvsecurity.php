<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/vendor/autoload.php';
use GeoIp2\Database\Reader; 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Mvsecurity extends AppComponent {
    public function checkAnonymousIP($ip_address = ''){    
        $return = 0;
        try {
            $maxmindPath = $_SERVER["DOCUMENT_ROOT"] . "/db/maxmind/";
            $reader = new Reader($maxmindPath . 'GeoIP2-Anonymous-IP.mmdb');
            $record = $reader->anonymousIp($ip_address);

            if ($record->isAnonymous || $record->isAnonymousVpn)
                $return = $record->isAnonymous;
        }catch (Exception $e) {
            //echo $e->getMessage();exit();
            $return = 0;
        }
        return $return;
    }
    
    public function hasMaxmindSuspiciousIP($studio_id){
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'has_suspicious_ip');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == 1){
            $return = 1;
        }
        return $return;
    }    
    
    //Create a new connection to any database
    function createDbConnection($dbname = '', $db_username = '', $db_password = '', $host = '', $port = '') {
        //get another db connection using the db confidential        
        if ($dbname != '' && $db_username != '' && $db_password != '' && $host != '') {
            $dsn = "mysql:host=" . $host . ";dbname=" . $dbname;
            $dbcon = new CDbConnection($dsn, $db_username, $db_password);
            $dbcon->active = true;
        } else {

            $dbcon = Yii::app()->db;
        }
        return $dbcon;
    }     
}