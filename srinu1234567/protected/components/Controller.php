<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Aws\Ses\SesClient; //for sending email by amason ses sdk
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    public $plinkcounter = 1;

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    //Added for SEO
    public $pageTitle;
    public $pageKeywords;
    public $pageDescription;
    public $contenttypes;
    public $reservedDomain = array('devstudio', 'demo', 'studio', 'maatv', 'maaflix', 'iskcon', 'admin', 'mail', 'muvi', 'bootstrap', 'noacount', 'signup', 'classic', 'underconstruction', 'www');
    public $breadcrumbs = array();
    public $siteLogo;
    public $favIcon;
    public $studio;
    public $Footerlinks;
    public $iosapplinks;
    public $ioslinks;
    public $andriodapplinks;
    public $androidlinks;
    public $site_parent_theme;
    public $site_theme;
    public $site_theme_color;
    public $template;
    public $canonicalurl;
    public $extensions;
    public $has_blog;
    public $has_faqs;
    public $has_tvguide;
    public $copy_text;
    public $copyright;
    public $main_menu;
    public $mainmenu;
    public $usermenu;
    public $user_menu;
    public $user_id;
    public $is_subscribed;
    public $cancel_popup;
    public $siteurl;
    public $newsletter;
    public $newsletter_form;
    public $ogImage;
    public $ogUrl;
    public $is_twitter;
    public $contentDisplayName;
    public $ogTitle;
    public $ogDescription;
    public $preview_mode;
    public $dummy_data_completed;
    //Added for different payment gateways of different studios
    public $PAYMENT_GATEWAY_ID;
    public $PAYMENT_GATEWAY;
    public $GATEWAY_ID;
    public $PAYMENT_GATEWAY_API_USER;
    public $PAYMENT_GATEWAY_API_PASSWORD;
    public $PAYMENT_GATEWAY_API_SIGNATURE;
    public $IS_LIVE_API_PAYMENT_GATEWAY;
    public $IS_PRIMARY;
    public $IS_CURRENCY_CONVERSION;
    public $PAYMENT_GATEWAY_NON_3D_SECURE;
    public $IS_PCI_COMPLIANCE;
    public $IS_PAYPAL_EXPRESS;
    public $API_ADDONS;
    public $API_ADDONS_REDIRECT;
    public $API_ADDONS_OTHERGATEWAY;
    public $PAYPAL;
    //added for purchase subdcription popup
    public $subscription_popup = 0;
    public $is_mobileview;
    public $is_master_account = 0;
    public $master_logo;
    public $master_name;
    public $poster_sizes;
    public $Language;
    public $Original;
    public $StaticMessage;
    public $ServerMessage;
    public $JsMessage;
    public $enable_laguages;
    public $all_languages;
    public $language_code;
    public $language_id;
    public $default_language_code;
    public $widgets;
    public $alljs;
    public $success_msg;
    public $error_msg;
    public $notice_msg;  
    public $ishome;
    public $cookie_msg;
    public $website_msg;
    public $default_image;
    public $enterpriselogo = '';
    public $current_url;
    public $draft = 0;
    public $tracking_code; 
    public $SubscriptionPlans;
    public $add_to_favourite;
    public $manage_device;
    public $watch_history;
    public $reminder;
    public $notification;
    public $add_to_queue;
    public $add_to_playlist;
    public $autoplay_episode;
    public $review_comment_size;
    public $login_with;
    public $chkRegisterapi;
    public $chkPlayPermission;
    public $is_audio_enable;
    public $otp_enable;
    public $register_with_mobile;    
    public function init() {
        //place this before any script you want to calculate time
//		$time_start = microtime(true); 
        parent::init();
        if (!YII_DEBUG) {
            //Yii::app()->attachEventHandler('onError',array($this,'handleError'));
                Yii::app()->attachEventHandler('onException', array($this, 'handleError'));
        }
        $this->is_mobileview = '';
        $controllers = Yii::app()->controller->id;
        if($controllers == 'rest'){
                return true;
        }
        else if($controllers == 'category' && Yii::app()->controller->action->id == 'CheckUniqueFieldName'){
            return true;
        }
        
       // redirect SIGNUP uppercase url to signup lowercase url
       if(strtolower($_SERVER['REQUEST_URI']) == '/signup')
       {
           if( $_SERVER['REQUEST_URI'] != '/signup' ) {
           header('HTTP/1.1 301 Moved Permanently');
           header('Location: '.Yii::app()->getBaseUrl(TRUE).strtolower($_SERVER['REQUEST_URI']));
           exit();   
           }
       }
        
        $studio = Yii::app()->common->getStudiosId(1);
        $this->current_url = Yii::app()->getBaseUrl(true).rtrim(Yii::app()->request->url, '/');
        $ip_address = Yii::app()->getRequest()->getUserHostAddress();
        //$ip_address = '217.170.201.106';
        //To check Maxmind anonymous IP
        $muvi_ips = array('111.93.166.194');
        if(!in_array($ip_address, $muvi_ips)){
            $check_anonymous_ip = Yii::app()->mvsecurity->hasMaxmindSuspiciousIP($studio->id);
            if($check_anonymous_ip == 1)
            {
                $is_anonymous = Yii::app()->mvsecurity->checkAnonymousIP($ip_address);
                if($is_anonymous > 0){
                    $this->website_msg = 'Your IP '.$ip_address.' looks suspicious. Please contact admin/technical support team at support@vishwammedia.com.';
                    if (isset($studio) && !empty($studio)) {
                        $this->siteLogo = Yii::app()->common->getLogoFavPath($studio->id);
                        $this->favIcon = Yii::app()->common->getLogoFavPath($studio->id, "favicon");
                    }

                    $org_url = Yii::app()->getbaseUrl(true);
                    $org_url = str_replace('http://', '', $org_url);
                    if (substr_count($org_url, 'https://') > 0)
                        $org_url = str_replace('https://', '', $org_url);

                    $cur_url = $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
                    $last_char = substr($cur_url, -1);
                    if ($last_char == '/')
                        $cur_url = str_replace('/', '', $cur_url);
                    if ($org_url == $cur_url) {

                    } else {
                        $home_url = Yii::app()->getbaseUrl(true);
                        $last_char = substr($home_url, -1);
                        if ($last_char == '/')
                            $home_url = str_replace('/', '', $home_url);
                        $this->redirect($home_url);
                    }
                    Yii::app()->theme = 'unavailable';                
                    Yii::app()->request->setHostInfo('http://www.' . $studio->domain);
                    @define('WEBSITE_NAME', $studio->name);
                    $this->defaultAction = 'sdkIndex';
                    return true;
                }
            }
        }
                
        //if($ip_address == TEST_IP_OFFICE){ 
       
        
        $admintheme = array('admin', 'report', 'template');
        if (in_array($controllers, $admintheme)) {
            $redirect_url = Yii::app()->getBaseUrl(true);
            if (isset(Yii::app()->user->signup_step) && Yii::app()->user->signup_step > 0) {
                if (Yii::app()->user->signup_step == 1) {
                    $redirect_url .=$this->createUrl('signup/typeofcontent');
                } else if (Yii::app()->user->signup_step == 2) {
                    $redirect_url .=$this->createUrl('signup/paymentGateway');
                }
                $this->redirect($redirect_url);
            }
            Yii::app()->theme = 'admin';
		} else if($controllers == 'signup'){
			Yii::app()->theme = 'signup';
		}else{
            Yii::app()->theme = 'bootstrap';
        }
        $this->template = array();

        $in_preview = 0;        
        $studio_id = '';
        $con = Yii::app()->db;
        $this->site_theme = '';
        $this->site_theme_color = '';
        $this->site_parent_theme = '';
        $extensions = array();
        $this->has_blog = '';
        $this->has_faqs = '';
        $this->has_tvguide='';
        $this->copy_text = '';
        $this->copyright = '';
        $this->main_menu = '';
        $this->mainmenu = array();
        $this->usermenu = array();
        $this->user_menu = '';
        $this->cancel_popup = '';
        $this->is_subscribed = 0;
        $this->siteurl = '';
        $this->newsletter = '';
        $this->newsletter_form = '';
        $this->preview_mode = '';
        $this->dummy_data_completed = 1;
        $this->poster_sizes = array();
        $this->widgets = array();
        $this->alljs = '';
        $this->success_msg = '';
        $this->error_msg = '';
        $this->notice_msg = '';        
        $this->ishome = '';
        $this->iosapplinks=0;
        $this->ioslinks='';
        $this->androidlinks='';
        $this->andriodapplinks=0;
        $this->tracking_code=0; 
        //$this->IS_PCI_COMPLIANCE = 0;
        $this->PAYPAL = '';
        $std_id = $studio_id = $studio->id;
        $this->studio = $studio;
        $this->user_id = Yii::app()->user->id;  
        $this->cookie_msg = '';
        $this->website_msg = '';
        $this->default_image = '';
        $this->review_comment_size = 0;
        $this->login_with = '';
        $this->chkRegisterapi = '';
        $this->chkPlayPermission = '';
        $this->is_audio_enable = 0;
        $this->otp_enable = 0;
        $this->register_with_mobile = 0;        
		if(isset($_SESSION[$studio_id]['parent_studio_id']) && $_SESSION[$studio_id]['parent_studio_id']){}else{
			$StoreMapping = StoreMapping::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
			if(!empty($StoreMapping)){
					$_SESSION[$studio_id]['parent_studio_id'] = $StoreMapping['parent_studio_id'];
			}
		}
        //Added by RK for preview functionality
        if($controllers == 'player'){
            //echo "Controller -".microtime(true)."--".date("Y-m-d H:i:s")."<br/>";
            $this->reminder = 0;
            $this->notification = 0;
            $reminder_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'reminder');
            if (!empty($reminder_status)) {
                $this->reminder = $reminder_status['config_value'];
            }
            $this->autoplay_episode = 0;
            $autoplay_active = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'autoplay_next_episode');
            if (!empty($autoplay_active)) {
                $this->autoplay_episode = $autoplay_active['config_value'];
            }
            if ($this->reminder != 0) {
                $notification_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'notification');
                if (!empty($notification_status)) {
                    $this->notification = $notification_status['config_value'];
                }
            }
            
            $studio_language       = self::studio_language(0,true);
            $this->enable_laguages = $studio_language['studio_enable_languages'];
            $this->all_languages   = $studio_language['studio_languages'];
            $this->language_code   = $studio_language['language_code'];
            foreach ($studio_language['studio_languages'] as $key => $value) {
                if ($this->language_code == $value['code']) {
                    $this->language_id = $value['languageid'];
                }
            }
            $this->default_language_code = $studio_language['studio_languages'][0]['default_language'];
            $theme = $studio->theme;
            if(file_exists(ROOT_DIR."languages/".$theme."/".$this->language_code.".php")){
                $lang = include( ROOT_DIR."languages/".$theme."/".$this->language_code.".php");
            }elseif(file_exists(ROOT_DIR . 'languages/studio/'.$this->language_code.'.php')){
                $lang = include(ROOT_DIR . 'languages/studio/'.$this->language_code.'.php');
            }else{
                $lang = include(ROOT_DIR . 'languages/studio/en.php');
            }
            $lang = self::getFullLangTranslation($lang);
            $this->Language = $lang['all'];
            $this->StaticMessage = $lang['static_messages'];
            $this->ServerMessage = $lang['server_messages'];
            $this->Original = TranslateKeyword::model()->getAllMessages($studio_id);     
            
            $this->siteLogo = Yii::app()->common->getLogoFavPath($studio->id);
            $this->favIcon = Yii::app()->common->getLogoFavPath($studio->id, "favicon");    
            $this->siteurl = Yii::app()->getBaseUrl(true);
            return true;
        }         
                
        //This is used for payment test in live. don't remove this section. @Sunil
        if ($studio_id == 1753) {
            define('IS_DEMO_PAYMENT', 1);
        } else {
            define('IS_DEMO_PAYMENT', 0);
        }
		//Added by  Gayadhar
		//if(!(Yii::app()->request->isAjaxRequest)){

			//Added by RK for preview functionality
			if (isset($_REQUEST['preview']) && $_REQUEST['preview'] == 1) {
				$cookie = new CHttpCookie('in_preview_theme', 0);
				$cookie->expire = time() + 60 * 5;
				Yii::app()->request->cookies['in_preview_theme'] = $cookie; // will send the cookie  
				//$this->redirect(Yii::app()->getBaseUrl(true));
			} else if (isset($_REQUEST['preview']) && $_REQUEST['preview'] == 2) {
				unset(Yii::app()->request->cookies['in_preview_theme']);
				$studio->in_preview_theme = 0;
				$cookie = new CHttpCookie('for_preview', 2);
				$cookie->expire = time() + 60 * 10;
				Yii::app()->request->cookies['for_preview'] = $cookie;
				//$this->redirect(Yii::app()->getBaseUrl(true));
			}

			if ($studio_id > 0) {
				$referrer_link = $this->setReferrer();
				$vertical = Yii::app()->common->getCropDimension(1, $studio_id);
				$horizontal = Yii::app()->common->getCropDimension(2, $studio_id);

				$poster_sizes = array(
					'vertical' => array(
						'width' => $vertical['width'],
						'height' => $vertical['height']
					),
					'horizontal' => array(
						'width' => $horizontal['width'],
						'height' => $horizontal['height']
					),                
				);
				$this->poster_sizes = $poster_sizes;
			}
			//Find Studio Extensions        
			$extensions = StudioExtension::model()->findExtensions($studio_id);
			$this->extensions = $extensions;
			/* Remove storelink from session */
			unset($_SESSION['storelink']);
			//Base Url 
			define('BASE_URL', Yii::app()->getBaseUrl(true));

			$host_ip = Yii::app()->params['host_ip'];

			if(Yii::app()->common->isVisitorAllowedCountry()===false){
				$this->studio->is_country_restriction = 1;
			}
		//}
        if (SUB_DOMAIN == 'partners'){
            Yii::app()->theme = 'admin';
            $this->layout = 'partner';
        }else if ($studio_id == 0) {
            $domain = Yii::app()->common->getStudiodomain();
            Yii::app()->user->setFlash('error', 'Oops! Sorry requested domain is not availalbe. To avail this domain please Contact Us');
            //$this->redirect('http://www.studio.'.$domain);exit;
            Yii::app()->theme = 'noaccount';
            Yii::app()->request->setHostInfo('http://www.' . $domain);
            @define('WEBSITE_NAME', "Muvi");
            @define('IS_SUBSCRIBED', 0);
        } else if (SUB_DOMAIN == 'studio' || $this->studio->is_country_restriction != 1 || Yii::app()->common->isVisitorAllowedCountry()) {
            
            $this->has_blog = Yii::app()->common->IsBlogAvailable();         
            $this->has_faqs = Yii::app()->common->IsFAQsAvailable();  
            $this->has_tvguide= Yii::app()->common->IsTvguideAvailable(); 
            $this->siteurl = Yii::app()->getBaseUrl(true);            
            $this->preview_mode = Yii::app()->common->showleftslide();
            
            //Added By SNL: Check plan and payment gateway are exists or not
                //$plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($std_id);
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($std_id);

                $activate = 0; //No plan and payment gateway set by studio

                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                    $activate = 1;
                }//End of Payment Gateway
            
            //if (SUB_DOMAIN == 'studio') {
            //    $this->dummy_data_completed = Yii::app()->general->dummyDataCompleted($studio_id);
            //}

            //$mastersubdomain = array('d2c-popup-shop','idogic');//solving template issue #4334, by pass master domain
            $coming_settings = ComingsoonSetting::getComingSoonSetting($studio_id);            
            $enable_comingsoon = 0;
            if(isset($coming_settings) && count($coming_settings > 0)){  
                $enable_comingsoon = $coming_settings->status;
            }  
			if(!Yii::app()->request->isAjaxRequest){
				$this->generateCsrfToken();
			}
            if (SUB_DOMAIN != 'studio' && SUB_DOMAIN != 'devstudio' && $studio->domain != '') {
                $this->register_with_mobile = Yii::app()->common->mobileFlag($studio_id);
                $chk_loginwith = Yii::app()->common->checkApi($studio_id);
                if(!empty($chk_loginwith)){
                    $this->login_with = $chk_loginwith;
                }
                $chk_otp = Yii::app()->common->enableOtp($studio_id);
                $this->otp_enable = (!empty($chk_otp))?$chk_otp:0;
                $this->chkRegisterapi = Yii::app()->common->checkRegApi($studio_id);
                $this->chkPlayPermission = Yii::app()->common->checkPlayApi($studio_id);           
                //Added By Biswajit For Language Translation
                $studio_language       = self::studio_language(0,true);
                $this->enable_laguages = $studio_language['studio_enable_languages'];
                $this->all_languages   = $studio_language['studio_languages'];
                $this->language_code   = $studio_language['language_code'];
                foreach ($studio_language['studio_languages'] as $key => $value) {
                    if ($this->language_code == $value['code']) {
                        $this->language_id = $value['languageid'];
                    }
                }
                $this->default_language_code = $studio_language['studio_languages'][0]['default_language'];
                $theme = $studio->theme;
                if(file_exists(ROOT_DIR."languages/".$theme."/".$this->language_code.".php")){
                    $lang = include( ROOT_DIR."languages/".$theme."/".$this->language_code.".php");
                }elseif(file_exists(ROOT_DIR . 'languages/studio/'.$this->language_code.'.php')){
                    $lang = include(ROOT_DIR . 'languages/studio/'.$this->language_code.'.php');
                }else{
                    $lang = include(ROOT_DIR . 'languages/studio/en.php');
                }
                $lang = self::getFullLangTranslation($lang);
                $this->Language = $lang['all'];
                $this->StaticMessage = $lang['static_messages'];
                $this->ServerMessage = $lang['server_messages'];
                $this->Original = TranslateKeyword::model()->getAllMessages($studio_id);
                $user_id = Yii::app()->user->id;
                if($user_id){
                    $login_history_id = Yii::app()->session['login_history_id'];
                    if(!$login_history_id){
                        $login_history_cookie_name = "login_history_id_".$studio_id."_".$user_id;
                        Yii::app()->session['login_history_id'] =  $login_history_id = $_COOKIE[$login_history_cookie_name];
                    }
                    if(!$login_history_id){
                        LoginHistory::model()->logoutUser($studio_id,$user_id);
                    }else{
                        $is_login = LoginHistory::model()->checkUserLoginTime($user_id,$studio_id, $login_history_id);
                    } 
                }else{
                    if($login_history_id){
                        $history = LoginHistory::model()->findByPk($login_history_id);
                        $history->logout_at = date('Y-m-d H:i:s');
                        $history->save();
                    }
                }
                
		

                $this->add_to_favourite = 0;
                $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_favourite_list');
                if(!empty($fav_status)){
                    $this->add_to_favourite = $fav_status['config_value'];
                }
				$content = Yii::app()->general->content_count($studio_id);
				if ((isset($content) && ($content['content_count'] & 4))) {
					$this->add_to_queue = 0;
					$queue_active = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'user_queue_list');
					if (!empty($queue_active)) {
						$this->add_to_queue = $queue_active['config_value'];
					}
					$this->add_to_playlist = Yii::app()->custom->hasPlaylistenabled();
				}
				

				$this->watch_history = 0;
                $watch_status = StudioConfig::model()->getConfig($studio_id,'studio_watch_history');
                if(!empty($watch_status)){
                    $this->watch_history = $watch_status['config_value'];
                }
				$this->manage_device = 0;
                $manage_device_status = StudioConfig::model()->getConfig($studio_id,'restrict_no_devices');                
                if(!empty($manage_device_status) && $manage_device_status->config_value){                   
                    $this->manage_device = $manage_device_status['config_value'];
                }
                $this->reminder = 0;
                $this->notification = 0;
                $reminder_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'reminder');
                if(!empty($reminder_status)){
                    $this->reminder = $reminder_status['config_value'];
                }
				$this->autoplay_episode = 0;
				$autoplay_active = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'autoplay_next_episode');
				if (!empty($autoplay_active)) {
					$this->autoplay_episode = $autoplay_active['config_value'];
				}
                if($this->reminder != 0){
                    $notification_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'notification');
                    if(!empty($notification_status)){
                        $this->notification = $notification_status['config_value'];
                    }
                }
                $review_config = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'max_review_comment_size');
                if (empty($review_config)) {
                    $review_config = StudioConfig::model()->getconfigvalueForStudio(0, 'max_review_comment_size');
                }
                $this->review_comment_size = @$review_config['config_value'];
                $this->website_msg = Yii::app()->custom->getFlashMessages();
                if($this->studio->parent_theme == 'modern-byod'){
                    $this->main_menu = Yii::app()->custom->getMainMenuStructure('array'); 
                    $this->user_menu = Yii::app()->custom->getUserMenuStructure('array');
                }
                else{
                    $this->main_menu = Yii::app()->general->getMainMenu();
                    $this->user_menu = Yii::app()->general->getUserMenu();
                } 
                $this->mainmenu = Yii::app()->custom->getMainMenuStructure('array'); 
                $this->usermenu = Yii::app()->custom->getUserMenuStructure('array');                
                $this->ishome = Yii::app()->general->isHomepage();
                $this->default_image =  $this->tvguideDefaultImage();
                $this->widgets = Yii::app()->general->cmsblocks();
                //if ($studio->parent_theme == 'byod' || $studio->parent_theme == "physical") {                                  
				$this->copy_text = Yii::app()->general->powered_by_text();
				$this->copyright = Yii::app()->general->copyright_text($studio->copyright);                
				$this->cancel_popup = Yii::app()->general->getcancelPopUp();
                //}
                $this->newsletter = Yii::app()->general->newsletterform();   
                $this->newsletter_form = Yii::app()->general->newsletterformonly();
                $jslang = $lang['js_messages'];
                $jslang = json_encode($jslang);
                $jslang = addslashes($jslang);
                $this->JsMessage = $jslang;
                $this->alljs = Yii::app()->general->alljs($studio->id);
                $this->tracking_code=self::trackingCode();
                $this->SubscriptionPlans = Yii::app()->custom->SubscriptionPlans();
                $content = Yii::app()->general->content_count($studio_id);
                if ((isset($content) && ($content['content_count'] & 4))) {
                    $this->is_audio_enable = 1;
                }
                $showMessageForCookie = 0;
                $showcookieMessage = '';
                if (!isset($_COOKIE['showMessageForCookies'])) {
                    $studioManageCookie = new StudioManageCookie();

                    if (@IS_LANGUAGE == 1) {
                        $studioManageCookieDetails = $studioManageCookie->findByAttributes(array('studio_id' => $this->studio->id, 'is_enable' => 1, 'language_id' => $language_id), array('condition' => 'message !=""'));
                    } else {
                        $studioManageCookieDetails = $studioManageCookie->findByAttributes(array('studio_id' => $this->studio->id, 'is_enable' => 1), array('condition' => 'message !=""'));
                    }

                    if (!empty($studioManageCookieDetails) && isset($studioManageCookieDetails->message) && ($studioManageCookieDetails->message != '')) {
                        $showcookieMessage = $studioManageCookieDetails->message;
                        $showMessageForCookie = 1;
                    } else {
                        $showMessageForCookie = 0;
                    }
                    setcookie("showMessageForCookies", 1, time() + (10 * 365 * 24 * 60 * 60),'/',$studio->domain,isset($_SERVER["HTTPS"]),TRUE);
                }
                define('showMessageForCookie', $showMessageForCookie);
                define('showcookieMessage', $showcookieMessage);                
                $this->cookie_msg = Yii::app()->custom->getCookieMessage();                
                //print '<pre>';print_r($studio);exit;
                $this->setReferrerUser();
                //If studio need no login
                if ($studio->need_login == 0 && !isset(Yii::app()->user->id) && $controllers != 'embed') {
                    $usr = SdkUser::model()->findByAttributes(array('status' => 1, 'studio_id' => $studio_id, 'is_studio_admin' => 1));
                    if (isset($usr) && !empty($usr)) {
                        $model = new LoginForm;
                        $model->attributes = array('email' => $usr->email, 'password' => $usr->encrypted_password, 'rememberMe' => 0);
                        if ($model->dostudioownerlogin()) {
                            if(($controllers != 'partners') && !(isset($_REQUEST['mobileview'])))
                                $this->redirect(Yii::app()->getBaseUrl(true));
                        }
                    }
                }

                //When account has cancelled
                //print $studio_id ."-----". $studio->is_subscribed ."-----".$studio->status ."-----".$studio->is_deleted ."-----".$studio->is_default;exit;
                if ($studio_id > 0 && $studio->is_subscribed == 0 && $studio->status == 4 && $studio->is_deleted == 0 && $studio->is_default == 0) {
                    $domain = Yii::app()->common->getStudiodomain();
                    Yii::app()->theme = 'underconstruction';
					$this->defaultAction = 'sdkIndex';
                    Yii::app()->request->setHostInfo('http://' . $domain);
                    @define('WEBSITE_NAME', "Muvi");
                    @define('IS_SUBSCRIBED', 0);

                    if ($_SERVER['REQUEST_URI'] != '/') {
                        $this->redirect('http://' . $studio->domain);
                        exit;
                    }
                } else if ($studio_id > 0 && $studio->is_subscribed == 0 && $studio->status == 0 && $studio->is_deleted == 1 && $studio->is_default == 0) {//When account has deleted
                    $domain = Yii::app()->common->getStudiodomain();
                    Yii::app()->user->setFlash('error', 'Oops! Sorry requested domain is not availalbe. To avail this domain please Contact Us');
                    Yii::app()->theme = 'noaccount';
					$this->defaultAction = 'sdkIndex';
                    Yii::app()->request->setHostInfo('http://' . $domain);
                    @define('WEBSITE_NAME', "Muvi");
                    @define('IS_SUBSCRIBED', 0);
					 if ($_SERVER['REQUEST_URI'] != '/') {
                        $this->redirect('http://' . $studio->domain);exit;
                    }
                } else if($enable_comingsoon == 1 && (!isset(Yii::app()->session['has_valid_passcode']) || @$coming_settings->option_enabled == '1')){                    
                    if(!$this->ishome && Yii::app()->request->url != '/site/comingpasscode' && Yii::app()->request->url != '/site/comingsignup'){
                        $this->redirect(Yii::app()->getBaseUrl(true));
                    }                    
                    Yii::app()->theme = 'comingsoon';
                    Yii::app()->request->setHostInfo('http://www.' . $studio->domain);
                    @define('WEBSITE_NAME', $studio->name);
                    $this->defaultAction = 'comingsoon';
                    @define('IS_SUBSCRIBED', 0);                    
                } else {//Free trial or subscribed account
                  
                    $sql = "SELECT * FROM pages WHERE studio_id={$studio_id} AND status=1 AND permalink != 'terms-privacy-policy' AND (language_id={$this->language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM pages WHERE studio_id={$studio_id} AND status=1 AND permalink != 'terms-privacy-policy' AND language_id={$this->language_id})) ORDER BY id_seq ASC";
                    $pages = Yii::app()->db->createCommand($sql)->queryAll();
                    $this->Footerlinks = $pages;
                    //Add Applink By <suraja@muvi.com>
                    //get the link of the ios app store                    
                    $sql = "SELECT config_value FROM studio_config where studio_id=" . $studio_id . " and config_key ='appstore_link'";
                    $app_config = Yii::app()->db->createCommand($sql)->queryAll();
                    $this->ioslinks = $app_config[0]['config_value'];
                    //get the link of the android app store                   
                    $sql = "SELECT config_value FROM studio_config where studio_id=" . $studio_id . " and config_key ='playstore_link'";
                    $app_config = Yii::app()->db->createCommand($sql)->queryAll();
                    $this->androidlinks = $app_config[0]['config_value'];

                    //End Applink By <suraja@muvi.com>
                    $this->pageTitle = $studio->name;
                    @define('WEBSITE_NAME', $studio->name);
                    @define('IS_SUBSCRIBED', $studio->is_subscribed);
                    $this->defaultAction = 'sdkIndex';

                    if (!isset(Yii::app()->user->is_sdk) || Yii::app()->user->is_sdk < 1) {

                        if (Yii::app()->common->getOrgSubdomain() != 'studio' && ((isset(Yii::app()->request->cookies['in_preview_theme']->value) && Yii::app()->request->cookies['in_preview_theme']->value == 1))) {
                            $std = $studio;
                            $current_theme = $studio->preview_theme ? $studio->preview_theme : $studio->subdomain;
                            $current_theme_color = $studio->preview_theme_color;
                            $parent_theme = $studio->preview_parent_theme;
                            $in_preview = 1;
                            //Yii::app()->user->setFlash('success', 'You are in Preview Mode. <a href="' . Yii::app()->getBaseUrl(true) . '/?preview=2">Go back</a> to original template.');
                        }else {
                            $current_theme = $studio->theme ? $studio->theme : $studio->subdomain;
                            $current_theme_color = $studio->default_color;
                            $parent_theme = $studio->parent_theme;
                        }
                    } else {
                        $current_theme = $studio->theme ? $studio->theme : $studio->subdomain;
                        $current_theme_color = $studio->default_color;
                        $parent_theme = $studio->parent_theme;
                    }
                    //echo $current_theme;
                    $this->site_theme = $current_theme;
                    $this->site_theme_color = $current_theme_color;
                    $this->site_parent_theme = $parent_theme;
                    $this->alljs = Yii::app()->general->alljs($studio->id);

                    //Updated for preview functionality
                    
                    /*$contenttype = new StudioContentType();
                    $ctypes = $contenttype->findAllByAttributes(array('studio_id' => $this->studio->id, 'is_enabled' => 1), array('order' => 'id_seq ASC,id DESC'));
                    $this->contenttypes = $ctypes;
                    */
                    
					$menuItems = new MenuItem();
                    $ctypes = $menuItems->findAllByAttributes(array('studio_id' => $this->studio->id), array('order' => 'id DESC'));
                    $this->contenttypes = $ctypes;
                    
                    Yii::app()->theme = $current_theme;
                    $this->template = Yii::app()->common->getTemplateDetails($parent_theme);
                    define('THEME', $current_theme);

                    define('GA_PROFILE_ID', $studio->ga_profile_id);
                    defined('LUNCH_DT') ? '' : define('LUNCH_DT', $studio->lunch_dt ? $studio->lunch_dt : date('Y-m-d', strtotime($studio->created_dt)));
                    defined('SITE_NAME') ? '' : define('SITE_NAME', $studio->name);
                    defined('SITE_URL') ? '' : define('SITE_URL', $studio->domain);
                    defined('S3BUCKET_ID') ? '' : define('S3BUCKET_ID', $studio->s3bucket_id);
                    defined('LOGIN_REDIRECT') ? '' : define('LOGIN_REDIRECT', $studio->default_view ? $studio->default_view : 'index');
                    @define('IS_SUBSCRIBED', $studio->is_subscribed);
                    if (!isset($_SESSION['internetSpeed']) && !isset(Yii::app()->user->internetSpeed)) {
                       $bucketInfoDetails = Yii::app()->common->getBucketInfo('',$studio->id);
                        $internetSpeedImage = CDN_HTTP . $bucketInfoDetails['bucket_name'] . '.' . $bucketInfoDetails['s3url'] . '/check-download-speed.jpg';
                        define('internetSpeedImage', $internetSpeedImage);
                    }
                    if (isset($_SESSION['internetSpeed']) && isset(Yii::app()->user->internetSpeed)) {
                        unset($_SESSION['internetSpeed']);
                    }

                    $this->siteLogo = Yii::app()->common->getLogoFavPath($studio->id);
                    $this->favIcon = Yii::app()->common->getLogoFavPath($studio->id, "favicon");
                    //To check if user has subscribed
                    $this->is_subscribed = Yii::app()->common->isSubscribed(Yii::app()->user->id);
                }
            } else {
                $studio_language       = self::studio_language(0,false);
                $this->enable_laguages = $studio_language['studio_enable_languages'];
                $this->all_languages   = $studio_language['studio_languages'];
                $this->language_code   = $studio_language['language_code'];
                foreach ($studio_language['studio_languages'] as $key => $value) {
                    if ($this->language_code == $value['code']) {
                        $this->language_id = $value['languageid'];
                    }
                }
                $this->default_language_code = $studio_language['studio_languages'][0]['default_language'];
            }
        } else {
            if (isset($studio) && !empty($studio)) {
                $this->siteLogo = Yii::app()->common->getLogoFavPath($studio->id);
                $this->favIcon = Yii::app()->common->getLogoFavPath($studio->id, "favicon");
            }

            $org_url = Yii::app()->getbaseUrl(true);
            $org_url = str_replace('http://', '', $org_url);
            if (substr_count($org_url, 'https://') > 0)
                $org_url = str_replace('https://', '', $org_url);

            $cur_url = $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            $last_char = substr($cur_url, -1);
            if ($last_char == '/')
                $cur_url = str_replace('/', '', $cur_url);
            if ($org_url == $cur_url) {
                
            } else {
                $home_url = Yii::app()->getbaseUrl(true);
                $last_char = substr($home_url, -1);
                if ($last_char == '/')
                    $home_url = str_replace('/', '', $home_url);
                $this->redirect($home_url);
            }
            
            $studio_language       = self::studio_language(0,true);
            $this->language_code   = $studio_language['language_code'];
            $this->default_language_code = $studio_language['studio_languages'][0]['default_language'];
            $theme = $studio->theme;
            if(file_exists(ROOT_DIR."languages/".$theme."/".$this->language_code.".php")){
                $lang = include( ROOT_DIR."languages/".$theme."/".$this->language_code.".php");
            }elseif(file_exists(ROOT_DIR . 'languages/studio/'.$this->language_code.'.php')){
                $lang = include(ROOT_DIR . 'languages/studio/'.$this->language_code.'.php');
            }else{
                $lang = include(ROOT_DIR . 'languages/studio/en.php');
            }
            $lang = self::getFullLangTranslation($lang);
            $this->Language = $lang['all'];            
            
            Yii::app()->theme = 'unavailable';
            $this->website_msg = $this->Language['sorry_country_not_avaliable'];
            Yii::app()->request->setHostInfo('http://www.' . $studio->domain);
            @define('WEBSITE_NAME', $studio->name);
            $this->defaultAction = 'sdkIndex';
        }
        @define('STUDIO_USER_ID', $studio_id);

//	$time_end = microtime(true);
//	//dividing with 60 will give the execution time in minutes other wise seconds
//	$execution_time = ($time_end - $time_start);
        //execution time of the script
        //echo '<!--<b>Total Execution Time:</b> '.$execution_time.' Secs -->';
    }
    
    public function beforeRender($view) {

        /******* LOC for restricted user acces by IP and user name by Suraja** */
        $restricted_user = new Restriction();
        $restricted_userModel = Restriction::model()->findAll();

        foreach ($restricted_userModel as $restricted_userModel) {
            //var_dump($restricted_userModel->attributes); 
            $ip_list = $restricted_userModel->ip;
            $user_list = $restricted_userModel->email;
        }

        $explode_userlist = explode(',', $user_list);
        $explode_iplist = explode(',', $ip_list);
        $ip = gethostbyname($_SERVER['REMOTE_ADDR']);

        if (in_array($ip, $explode_iplist)) {
            Yii::app()->session['block_ga'] = 0;
        } else {
            Yii::app()->session['block_ga'] = 1;
        }
      
        $meta = Yii::app()->common->defaultmeta();

        if (!($this->pageTitle)) {
            $this->pageTitle = $meta['title'];
        } else {
            $app = $this->studio->name;
            $this->pageTitle = $this->pageTitle;
        }

        if (!($this->pageDescription)) {
            $this->pageDescription = $meta['description'];
        }

        if (!($this->pageKeywords)) {
            $this->pageKeywords = $meta['keywords'];
        }
        $user_id = Yii::app()->user->id;
        if (SUB_DOMAIN != 'studio' && SUB_DOMAIN != 'devstudio' && !isset(Yii::app()->user->is_studio_admin) && @$user_id != STUDIO_USER_ID && Yii::app()->user->id != 1038) {
            $userstudio = Studios::model()->findByPk($this->studio->id);
            
            if ($userstudio->domain != '') {
                $user_is_subscribed = Yii::app()->common->isSubscribed($user_id);
                
                if (intval($user_is_subscribed)) {
                    $payment_status = Yii::app()->common->isSubscribedAndPaymentSuccess($user_id);

                    if (isset($payment_status) && ($payment_status != 0)) {//Payment failed
                        $accontArray = array('profile', 'cardInformation', 'saveCard', 'deleteCard', 'makePrimaryCard');
                        if (!in_array(Yii::app()->controller->action->id, $accontArray)) {
                            $redirect_url .= '/user/cardInformation';
                            $this->redirect($redirect_url);
                        }
                    }
                }
            }
        }
        
        $is_subscribed = 0;
        $movie_paid = 0;
        if ($user_id > 0 && (Yii::app()->controller->id != 'site') && (Yii::app()->controller->action->id != 'index')) {

            if (isset($_REQUEST['item']) && $_REQUEST['item'] != '') {
                $pgconnection = Yii::app()->db;
                $sql = "select id from films where permalink = '" . $_REQUEST['item'] . "'";
                $obj = $pgconnection->createCommand($sql);
                $movie = $obj->queryAll();
                $movie_id = $movie[0]["id"];

                if ($movie_id > 0) {
                    $can_see_data['movie_id'] = $movie_id;
                    $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
                    $mov_warning = Yii::app()->common->getPlayMessage($mov_can_see);
                    $movie_payment_types = Yii::app()->common->getMoviePaymentType($movie_id);
                    $movie_paid = Yii::app()->common->checkIfPaid($movie_id);
                }
            }
            /*
              if($movie_paid > 0)
              {}
              else if(($user_id > 0 && !isset(Yii::app()->user->is_sdk) && Yii::app()->common->checkIfPlan() && !Yii::app()->common->isSubscribed($user_id)) && (@STUDIO_USER_ID!='55'))
              Yii::app()->user->setFlash('success', '<a class="ylw_link" href="'.Yii::app()->getBaseUrl(true).'/user/process'.'">Purchase a subscription</a> to watch '.WEBSITE_NAME.' shows and movies.');
             * 
             */
        }
//			if($_SERVER['REMOTE_ADDR'] =='117.247.67.108'){
//				echo Yii::app()->user->id."==".SUB_DOMAIN."--".Yii::app()->user->status;
//			}
        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 1 && (SUB_DOMAIN == 'devstudio' || SUB_DOMAIN == 'studio')) {
            $user = User::model()->findByPk(Yii::app()->user->id);
            if (isset($user) && !empty($user)) {
                $redirect_url = Yii::app()->getBaseUrl(true);
                $studio = Studios::model()->findByPk($user->studio_id);
                if (isset($studio->is_default) && $studio->is_default != 1 && isset($user->id) && $user->id > 1 && isset($user->is_sdk) && $user->is_sdk == 1 && isset($user->role_id) && $user->role_id == 1) {
                    //Check free trial period expired or not
                    if (isset($studio->status) && $studio->status == 1 && $studio->is_subscribed == 0) {//Free trial user
                        $start_date = date("Y-m-d", strtotime($studio->start_date));
                        $today_date = gmdate('Y-m-d');
                        if (strtotime($today_date) >= strtotime($start_date)) {
                            $accontArray = array('account', 'subscription', 'authenticateCard', 'purchaseSubscribe', 'managePayment', 'saveCard', 'deleteCard', 'makePrimaryCard');
                            if (!in_array(Yii::app()->controller->action->id, $accontArray)) {
                                if (!strstr($_SERVER['HTTP_HOST'], 'devstudio.')) {
                                    //$redirect_url .= '/payment/subscription';
                                    //$this->redirect($redirect_url);
                                    $this->subscription_popup = 1;
                                }
                            }
                        }
                    } elseif (isset($studio->status) && $studio->status == 1 && $studio->is_subscribed == 1) {//Subscribed user
                        if (isset($studio->payment_status) && ($studio->payment_status != 0)) {//Payment failed
                            $accontArray = array('account', 'managePayment', 'saveCard', 'deleteCard', 'makePrimaryCard');
                            if (!in_array(Yii::app()->controller->action->id, $accontArray)) {
                                $redirect_url .= '/payment/managePayment';
                                $this->redirect($redirect_url);
                            }
                        } else {//Payment success
                            $accontArray = array('subscription', 'authenticateCard', 'purchaseSubscribe', 'reactivate');
                            if (in_array(Yii::app()->controller->action->id, $accontArray)) {
                                $redirect_url .= '/admin/managecontent';
                                $this->redirect($redirect_url);
                            }
                        }
                    }

                    if (isset($studio->status) && $studio->status == 4 && $studio->is_subscribed == 0) {//Cancelled user want to reactivate
                        $accontArray = array('account', 'reactivate', 'authenticateCard', 'purchaseSubscribe', 'managePayment', 'saveCard', 'deleteCard', 'makePrimaryCard');
                        if (!in_array(Yii::app()->controller->action->id, $accontArray)) {
                            //$redirect_url .= '/payment/reactivate';
                            //$this->redirect($redirect_url);
                            $this->subscription_popup = 1;
                        }
                    }
                }
            }
        }

        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 1 && (SUB_DOMAIN == 'devstudio' || SUB_DOMAIN == 'studio') && Yii::app()->user->status == 2) {
            $bypassAction = array('domainname', 'typeofcontent', 'paymentgateway', 'createconsumer', 'signupterms', 'managecontent', 'addcontent', 'updatecontent', 'removecontent', 'checksubdomain', 'setupstudio', 'adddefaultcontenttype', 'updatetemplate', 'logout');
            $curaction = strtolower(Yii::app()->controller->action->id);
        }

        if (isset(Yii::app()->user->id) && intval(Yii::app()->user->id) && isset($this->studio->parent_theme) && trim($this->studio->parent_theme) == "singleshow-a") {
            $studio_id = Yii::app()->common->getStudiosId();

            $command = Yii::app()->db->createCommand()
                    ->select('confirmation_token')
                    ->from('sdk_users')
                    ->where('id = "' . Yii::app()->user->id . '" AND studio_id=' . $studio_id . ' AND fb_userid IS NULL');
            $users = $command->queryAll();

            if (isset($users[0]['confirmation_token']) && trim($users[0]['confirmation_token'])) {
                Yii::app()->theme = $this->studio->theme;
                $this->layout = 'email_verification';
            }
        }
        
        //to check whether the email is verified or not.
       

        return true;
    }

    public function studio_language($studio_id=0 ,$frontend = false) {
        if($studio_id == 0){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $studiolanguages = Yii::app()->db;
        $sql = "SELECT a.*, s.id AS studio_id, s.default_language FROM 
        (SELECT l.id AS languageid, l.name, l.code, sl.* FROM languages l LEFT JOIN studio_languages sl 
        ON (l.id = sl.language_id AND sl.studio_id={$studio_id}) WHERE l.code='en' OR (sl.studio_id={$studio_id})) AS a, studios s WHERE s.id={$studio_id} 
        ORDER BY FIND_IN_SET(a.code,s.default_language) DESC, FIND_IN_SET(a.code,'en') DESC, a.status DESC, a.name ASC";
        $studio_languages = $studiolanguages->createCommand($sql)->queryAll();
        $studio_enable_languages = array();
        $lang_array =array();
        if(!empty($studio_languages)){
            foreach($studio_languages as $key => $value){
                if($frontend){
                    if(($value['frontend_show_status'] != "0") && ($value['status'] != 0 || $value['code'] == "en")){
                        $value['original'] = $value['name'];
                        if(trim($value['translated_name']) !=""){
                            $value['name'] = $value['translated_name'];
                        }
                        $studio_enable_languages[] =  $value;
                        $lang_array[] = $value['code'];
                    }
                }else{
                    if ($value['status'] != 0 || $value['code'] == "en") {
                        $studio_enable_languages[] =  $value;
                        $lang_array[] = $value['code'];
                    }
                }
            }
        }
        if (isset(Yii::app()->request->cookies['Language']) && trim(Yii::app()->request->cookies['Language']) && @in_array(Yii::app()->request->cookies['Language']->value,$lang_array)) {
            $language_code = Yii::app()->request->cookies['Language']->value;
        } else if (isset($studio_languages[0]['id']) && trim($studio_languages[0]['id'])) {
            $language_code = $studio_languages[0]['default_language'];
        } else {
            $language_code = 'en';
        }
        
        $domain = Yii::app()->getBaseUrl(true);
        $domain = explode("//", $domain);
        $domain = $domain[1];

        if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
            setcookie('Language', $language_code, time() + (60 * 60 * 24 * 90), '/', $domain, isset($_SERVER["HTTPS"]), TRUE);
        }
        $language = array('studio_languages' => $studio_languages,'studio_enable_languages'=>$studio_enable_languages, 'language_code' => $language_code);
        
        return $language;
    }

    function setReferrer() {
        $referrer_link = "";

        if (isset($_GET['sr']) && trim($_GET['sr'])) {
            $referrer = urldecode($_GET['sr']);
        }else if (isset($_GET['SR']) && trim($_GET['SR'])) {
            $referrer = urldecode($_GET['src']);
        }  else if (isset($_GET['src']) && trim($_GET['src'])) {
            $referrer = urldecode($_GET['src']);
        } else if (isset($_GET['SRC']) && trim($_GET['SRC'])) {
            $referrer = urldecode($_GET['SRC']);
        } else {
            $referrer = @$_SERVER['HTTP_REFERER'];
        }

        $referrer = trim($referrer);

        if ($referrer) {
            if (@$_GET['sr'] || @$_GET['src'] || @$_GET['SRC']) {
                if ((strpos($_SERVER['HTTP_HOST'], 'muvi.com')) || (strpos($_SERVER['HTTP_HOST'], 'muvi.in'))) {
                    setcookie('REFERRER', $referrer, time() + (60 * 60 * 24 * 90), '/', DOMAIN_COOKIE, false, false);
                }
            } else {
                $parseurl = parse_url($referrer);

                if (stristr($parseurl['host'], "www.google.co") || stristr($referrer, "www.bing") || stristr($referrer, "search.yahoo")) {
                    $referrer = $parseurl['host'];
                } elseif (stristr($referrer, "utm_source") || stristr($referrer, "utm_medium")) {
                    parse_str($referrer['query'], $output);

                    if (isset($output['utm_source'])) {
                        $referrer = $output['utm_source'];
                    } elseif (isset($output['utm_medium'])) {
                        $referrer = $output['utm_medium'];
                    }
                }

                if ((strpos($referrer, "muvi.com") == false) && (strpos($referrer, "muvi.in") == false)) {
                    setcookie('REFERRER', $referrer, time() + (60 * 60 * 24 * 90), '/', DOMAIN_COOKIE, false, false);
                }
            }
        }

        return $referrer_link;
    }
    

    function setReferrerUser() {
        $referrer_link = "";


        if (isset($_GET['source']) && trim($_GET['source'])) {
            $referrer = urldecode($_GET['source']);
        } else if (isset($_GET['src']) && trim($_GET['src'])) {
            $referrer = urldecode($_GET['src']);
        } else if (isset($_GET['SRC']) && trim($_GET['SRC'])) {
            $referrer = urldecode($_GET['SRC']);
        } else {
            $referrer = @$_SERVER['HTTP_REFERER'];
        }

        $referrer = trim($referrer);
        $studio_id = Yii::app()->common->getStudiosId();
        //$studio = Studio::model()->findByPk($studio_id);
        $studio = $this->studio;
        $domain = $studio->domain;

        if ($referrer) {
            if (@$_GET['source'] || @$_GET['src'] || $_GET['SRC']) {
                if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
                    setcookie('SITE_REFERRER', $referrer, time() + (60 * 60 * 24 * 90), '/', $domain, false, false);
                }
            } else {
                $parseurl = parse_url($referrer);

                if (stristr($parseurl['host'], "www.google.co") || stristr($referrer, "www.bing") || stristr($referrer, "search.yahoo")) {
                    $referrer = $parseurl['host'];
                } elseif (stristr($referrer, "utm_source") || stristr($referrer, "utm_medium")) {
                    parse_str($referrer['query'], $output);

                    if (isset($output['utm_source'])) {
                        $referrer = $output['utm_source'];
                    } elseif (isset($output['utm_medium'])) {
                        $referrer = $output['utm_medium'];
                    }
                }
                if ((strpos($referrer, "muvi.com") == false) && (strpos($referrer, "muvi.in") == false) && (strpos($referrer, $domain) == false)) {
                    setcookie('SITE_REFERRER', $referrer, time() + (60 * 60 * 24 * 90), '/', $domain, false, false);
                }
            }
        }

        return $referrer_link;
    }

    public function setPaymentGatwayVariable($gateways) {
        if(isset($gateways) && count($gateways) > 1){
            foreach ($gateways as $row){
                $gateway['payment_gateways_id'][trim($row->short_code)] = $row->id;
                $gateway['gateway_id'][trim($row->short_code)] = $row->gateway_id;
                $gateway['short_code'][trim($row->short_code)] = trim($row->short_code);
                $gateway['api_username'][trim($row->short_code)] = trim($row->api_username);
                $gateway['api_password'][trim($row->short_code)] = trim($row->api_password);
                $gateway['api_signature'][trim($row->short_code)] = trim($row->api_signature);
                $gateway['api_mode'][trim($row->short_code)] = trim($row->api_mode);
                $gateway['is_primary'][trim($row->short_code)] = $row->is_primary;
                $gateway['is_currency_conversion'][trim($row->short_code)] = $row->is_currency_conversion;
                $gateway['non_3d_secure'][trim($row->short_code)] = $row->non_3d_secure;
                $gateway['is_pci_compliance'][trim($row->short_code)] = $row->is_pci_compliance;
                $gateway['is_paypal_express'][trim($row->short_code)] = 0;
                $gateway['api_addons'][trim($row->short_code)] = isset($row->api_addons) && trim($row->api_addons) ? json_decode($row->api_addons,true): '';
                if(trim($row->short_code) == 'paypalpro' && isset($row->is_pci_compliance) && $row->is_pci_compliance) {
                    if(trim($row->api_mode) == 'sandbox'){
                        $gateway['paypal_url']  = 'https://securepayments.sandbox.paypal.com/webapps/HostedSoleSolutionApp/webflow/sparta/hostedSoleSolutionProcess';
                    }else{
                        $gateway['paypal_url']  = 'https://securepayments.paypal.com/webapps/HostedSoleSolutionApp/webflow/sparta/hostedSoleSolutionProcess';
                    }
                  
                }
            }
            $this->PAYMENT_GATEWAY_ID = $gateway['payment_gateways_id'];
            $this->GATEWAY_ID = $gateway['gateway_id'];
            $this->PAYMENT_GATEWAY = $gateway['short_code'];
            $this->PAYMENT_GATEWAY_API_USER = $gateway['api_username'];
            $this->PAYMENT_GATEWAY_API_PASSWORD = $gateway['api_password'];
            $this->PAYMENT_GATEWAY_API_SIGNATURE = $gateway['api_signature'];
            $this->IS_LIVE_API_PAYMENT_GATEWAY = $gateway['api_mode'];
            $this->IS_PRIMARY = $gateway['is_primary'];
            $this->IS_CURRENCY_CONVERSION = $gateway['is_currency_conversion'];
            $this->PAYMENT_GATEWAY_NON_3D_SECURE = $gateway['non_3d_secure'];
            $this->IS_PCI_COMPLIANCE = $gateway['is_pci_compliance'];
            $this->IS_PAYPAL_EXPRESS = $gateway['is_paypal_express'];
            $this->API_ADDONS = isset($gateway['api_addons']) && !empty($gateway['api_addons']) ? $gateway['api_addons']['multicurrency']:'';
            $this->API_ADDONS_REDIRECT = isset($gateway['api_addons']['redirect']) && !empty($gateway['api_addons']['redirect']) ? $gateway['api_addons']['redirect']: 0;
            $this->API_ADDONS_OTHERGATEWAY = isset($gateway['api_addons']['othergateway']) && !empty($gateway['api_addons']['othergateway']) ? $gateway['api_addons']['othergateway']: '';
            if(isset($gateway['paypal_url']) && trim($gateway['paypal_url'])) {
                $this->PAYPAL = $gateway['paypal_url'];
            }
            
        }else{
            $this->PAYMENT_GATEWAY_ID[trim($gateways[0]->short_code)] = $gateways[0]->id;
            $this->GATEWAY_ID[trim($gateways[0]->short_code)] = $gateways[0]->gateway_id;
            $this->PAYMENT_GATEWAY[trim($gateways[0]->short_code)] = trim($gateways[0]->short_code);
            $this->PAYMENT_GATEWAY_API_USER[trim($gateways[0]->short_code)] = trim($gateways[0]->api_username);
            $this->PAYMENT_GATEWAY_API_PASSWORD[trim($gateways[0]->short_code)] = trim($gateways[0]->api_password);
            $this->PAYMENT_GATEWAY_API_SIGNATURE[trim($gateways[0]->short_code)] = trim($gateways[0]->api_signature);
            $this->IS_LIVE_API_PAYMENT_GATEWAY[trim($gateways[0]->short_code)] = trim($gateways[0]->api_mode);
            $this->IS_PRIMARY[trim($gateways[0]->short_code)] = $gateways[0]->is_primary;
            $this->IS_CURRENCY_CONVERSION[trim($gateways[0]->short_code)] = $gateways[0]->is_currency_conversion;
            $this->PAYMENT_GATEWAY_NON_3D_SECURE[trim($gateways[0]->short_code)] = $gateways[0]->non_3d_secure;
            $this->IS_PCI_COMPLIANCE[trim($gateways[0]->short_code)] = $gateways[0]->is_pci_compliance;
            $this->IS_PAYPAL_EXPRESS[trim($gateways[0]->short_code)] = 1;
            if($gateways[0]->is_primary){
                $this->IS_PAYPAL_EXPRESS[trim($gateways[0]->short_code)] = 0;
            }
            $addons = isset($gateways[0]->api_addons) && trim($gateways[0]->api_addons) ? json_decode($gateways[0]->api_addons,true): '';
            $this->API_ADDONS[trim($gateways[0]->short_code)] = isset($addons['multicurrency']) && !empty($addons['multicurrency']) ? $addons['multicurrency']: '';
            $this->API_ADDONS_REDIRECT[trim($gateways[0]->short_code)] = isset($addons['is_redirect']) && !empty($addons['is_redirect']) ? $addons['is_redirect']: 0;
            $this->API_ADDONS_OTHERGATEWAY[trim($gateways[0]->short_code)] = isset($addons['othergateway']) && !empty($addons['othergateway']) ? $addons['othergateway']: '';
            
            if(trim($gateways[0]->short_code) == 'paypalpro' && isset($gateways[0]->is_pci_compliance) && $gateways[0]->is_pci_compliance)  {
                if(trim($gateways[0]->api_mode) == 'sandbox'){
                    $paypal_url  = 'https://securepayments.sandbox.paypal.com/webapps/HostedSoleSolutionApp/webflow/sparta/hostedSoleSolutionProcess';
                }else{
                    $paypal_url  = 'https://securepayments.paypal.com/webapps/HostedSoleSolutionApp/webflow/sparta/hostedSoleSolutionProcess';
                }
            }
            if(isset($paypal_url) && trim($paypal_url)) {
                $this->PAYPAL = $paypal_url;
            }
            
            
        }
        
        return true;
    }

    //End added for SEO        

    function cropandupload($iWidth, $iHeight, $sTempFileName, $upload_path, $crop_upload_path) {
        $iJpgQuality = 100;
        $bucketInfo = Yii::app()->common->getBucketInfoForPoster(Yii::app()->user->studio_id);
        $bucketName = $bucketInfo['bucket_name'];
        $client = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);


        $result = $client->putObject(array(
            'ACL' => 'public-read',
            'Bucket' => $bucketName,
            'Key' => $upload_path,
            'SourceFile' => $sTempFileName
                )
        );
        list($img_width, $img_height, $img_type, $img_attr) = getimagesize($sTempFileName);
        if ($_POST['x1'] != "") {
            $x1 = $_POST['x1'];
        } else {
            $x1 = 0;
        };
        if ($_POST['y1'] != "") {
            $y1 = $_POST['y1'];
        } else {
            $y1 = 0;
        };
        if ($_POST['h'] != "") {
            $h = $_POST['h'];
        } else {
            $h = $img_height;
        };
        if ($_POST['w'] != "") {
            $w = $_POST['w'];
        } else {
            $w = $img_width;
        };
        // change file permission to 644
        @chmod($sTempFileName, 0644);

        if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
            $aSize = getimagesize($sTempFileName); // try to obtain image info
            $type = exif_imagetype($sTempFileName);
            //print_r($type);exit;

            if (!$aSize) {
                @unlink($sTempFileName);
                return;
            }

            // check for image type
            switch ($type) {
                case 2:
                    $sExt = '.jpg';
                    //echo "jpeg";exit;
                    // create a new image from file 
                    $vImg = @imagecreatefromjpeg($sTempFileName);
                    break;
                case 3:
                    $sExt = '.png';

                    // create a new image from file 
                    $vImg = @imagecreatefrompng($sTempFileName);
                    break;
                default:
                    @unlink($sTempFileName);
                    return;
            }

            // create a new true color image
            $vDstImg = @imagecreatetruecolor($iWidth, $iHeight);

            // copy and resize part of an image with resampling
            imagecopyresampled($vDstImg, $vImg, 0, 0, (int) $x1, (int) $y1, $iWidth, $iHeight, (int) $w, (int) $h);

            // define a result image filename
            $sResultFileName = $sTempFileName;

            // output image to file
            imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);




            $result = $client->putObject(array(
                'ACL' => 'public-read',
                'Bucket' => $bucketName,
                'Key' => $crop_upload_path,
                'SourceFile' => $sResultFileName,
                'headers' => array(
                    "Cache-Control" => "max-age=94608000",
                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                )                
                    )
            );
            @unlink($sTempFileName);
        }
    }

    /**
     * @method public getMovieDetails(int $movie_id) Returns the Details of a Movie With its image,cast and Crew
     * @param int $movie_id Id of the movie to get the details
     * @author GDR<support@muvi.com>
     * @return Json Json array with all the details of the Movie
     */
    function getMovieDetails($movie_id, $include_episode = 1) {
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        if ($movie_id) {
            $movie_final = array();
            $dbcon = Yii::app()->db;

            $sql = "SELECT f.id,f.name,s.id as movie_stream_id,f.content_type_id,f.uniq_id,f.ppv_plan_id, f.story,f.permalink,f.genre,f.release_date,f.censor_rating,f.content_types_id,s.full_movie,s.is_episode FROM films f,movie_streams s  WHERE f.id = s.movie_id AND f.id=" . $movie_id . " AND ((s.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(s.content_publish_date) = 0) OR (s.content_publish_date <=NOW()) ) ";
            $movieDetails = $dbcon->createCommand($sql)->queryAll();
            //$movieDetails = Film::model()->findByPk($movie_id);
            //echo "<pre>";print_r($movieDetails);exit;
            if ($movieDetails) {
                $current_date = date('m/d/Y', time());
                $final = array();
                $actors = array();
                $cast_detail = array();
                $castsarr = $this->getCasts($movie_id);
                if ($castsarr) {
                    $actor = $castsarr['actor'];
                    $actor = trim($actor, ',');
                    unset($castsarr['actor']);
                    $director = $castsarr['director'];
                    unset($castsarr['director']);
                }

                $cast_detail = $castsarr;

                if ($movieDetails[0]['content_types_id'] == 2 || $movieDetails[0]['content_types_id'] == 4) {
                    $thumb_image = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                } else {
                    $thumb_image = $this->getPoster($movie_id, 'films', 'thumb', $studio_id);
                }

                if ($thumb_image && strstr($thumb_image, '/no-image')) {
                    $original_image = $banner_image = $thumb_image;
                } elseif ($thumb_image) {
                    $banner_image = str_replace('/thumb/', '/standard/', $thumb_image);
                    if ($movieDetails[0]['content_types_id'] == 2 || $movieDetails[0]['content_types_id'] == 4) {
                        $original_image = str_replace('/thumb/', '/episode/', $thumb_image);
                    } else {
                        $original_image = str_replace('/thumb/', '/original/', $thumb_image);
                    }
                }
                $movieUrl = '';
                if ($movieDetails[0]['full_movie']) {
                    if (defined('S3BUCKET_ID')) {
                        $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
                    } elseif (isset(Yii::app()->user->s3bucket_id) && Yii::app()->user->s3bucket_id) {
                        $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
                    } else {
                        $bucketInfo = Yii::app()->common->getBucketInfo('', Yii::app()->common->getStudioId());
                    }
                    //$bucket = Yii::app()->params->video_bucketname;
                    $bucket = $bucketInfo['bucket_name'];
                    $s3url = $bucketInfo['s3url'];
                    $movieUrl = CDN_HTTP . $bucket . "." . $s3url . "/uploads/movie_stream/full_movie/" . $movieDetails[0]['movie_stream_id'] . "/" . urlencode($movieDetails[0]['full_movie']);
                }
                $trailerUrl = isset($movieDetails['trailer']) ? $movieDetails['trailer']['video_remote_url'] : "";
                $bannerImage = '';
                $MovieBanner = $this->getPoster($movie_id, 'topbanner', 'original');
                if ($MovieBanner) {
                    $bannerImage = $MovieBanner;
                }
                $episodes = '';
                if (($movieDetails[0]['content_types_id'] == 3) && $include_episode) {
                    $episodes = $this->getEpisodes($movie_id, '');
                    //echo '<pre>';print_r($episodes);exit;
                }
                $isConverted = 0;
                $is_convertedQuery = "SELECT * from movie_streams where is_converted =1 and full_movie !='' and movie_id=" . $movie_id;
                $count_Is_converted = Yii::app()->db->createCommand($is_convertedQuery)->execute();
                if ($count_Is_converted > 0) {
                    $isConverted = 1;
                }
                $final[0] = $movieDetails[0];
                $langcontent = Yii::app()->custom->getTranslatedContent($movie_id,0,$language_id);
                if (array_key_exists($movie_id, $langcontent['film'])) {
                    $final[0]['name'] = $langcontent['film'][$movie_id]->name;
                    $final[0]['story'] = $langcontent['film'][$movie_id]->story;
                    $final[0]['genre'] = $langcontent['film'][$movie_id]->genre;
                    $final[0]['language'] = $langcontent['film'][$movie_id]->language;
                    $final[0]['censor_rating'] = $langcontent['film'][$movie_id]->censor_rating;
                }
                $final[0]['movie_banner'] = @$bannerImage;
                $final[0]['thumb_image'] = @$thumb_image;
                $final[0]['banner_image'] = @$banner_image;
                $final[0]['original_image'] = @$original_image;
                $final[0]['actor'] = @$actor;
                $final[0]['director'] = @$director;
                $final[0]['cast_detail'] = @$castsarr;
                $final[0]['trailerUrl'] = $this->getTrailer($movie_id);
                $final[0]['trailerIsConverted'] = $this->getTrailerIsConverted($movie_id);
                $final[0]['movieUrl'] = @$movieUrl;
                $final[0]['episodes'] = @$episodes;
                $final[0]['isConverted'] = $isConverted;
                return json_encode($final);
                exit;
            } else {
                return json_encode(array('error' => "Movie not found!"));
            }
        } else {
            return json_encode(array('error' => "Movie id not Provided!"));
        }
    }

    /**
     * @method public getCasts(int $movie_id) Returns the list of Casts With there image n Details
     * @param int $movie_id Id of the movie to get the details
     * @author GDR<support@muvi.com>
     * @return Array Multidimensional array containg each cast details
     */
    function getCasts($movie_id = '', $actdirc = 1,$language_id=false,$studio_id=false) {
        if(!$language_id){
            $language_id = $this->language_id;
        }
        $default_lang_id = 20;
        if($studio_id == false){
            $studio_id = $this->studio->id;
        }
        if ($movie_id) {
            $sql = "SELECT mc.*,c.name,c.permalink FROM movie_casts mc, celebrities c WHERE c.parent_id=0 AND c.studio_id = " . $studio_id . " AND mc.celebrity_id = c.id AND mc.movie_id=" . $movie_id;
            $con = Yii::app()->db;
            $cast1 = $con->createCommand($sql)->queryAll();
            if ($cast1) {
                $i = 0;
                $actor = '';
                $director = '';
                //$val = $cast[0];
                foreach ($cast1 AS $key => $val) {
                    $celeb_name = Yii::app()->custom->getTranslatedCastName($val['celebrity_id'],$val['name'],$language_id,$studio_id);
                    if ($actdirc) {
                        if (!$director && in_array('director', json_decode($val['cast_type']))) {
                            $director = $celeb_name;
                        }
                        if (in_array('actor', json_decode($val['cast_type'])) && $i < 2) {
                            $i++;
                            $actor .= $celeb_name . ',';
                        }
                    }
                    $casts[$key]['celeb_image'] = $this->getPoster($val['celebrity_id'], 'celebrity', 'medium');
                    $casts[$key]['celeb_name'] = $celeb_name;
                    $casts[$key]['celeb_id'] = $val['celebrity_id'];
                    $casts[$key]['permalink'] = $val['permalink'];
                    $casts[$key]['cast_type'] = implode(',', json_decode($val['cast_type'], TRUE));
                }
                if ($actdirc) {
                    $casts['actor'] = @$actor;
                    $casts['director'] = @$director;
                }
                return $casts;
				
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @method public getPoster(int $movie_id) Returns the Poster of the movie
     * @param int $movie_id Id of the movie to get the details
     * @param string $poster_file_name Name of the Poster if its avl in the films table
     * @param string $type Which type of image you need @example medium thumb standard original 
     * @author GDR<support@muvi.com>
     * @return String Poster path
     */
    function getPoster_bak($id = '', $poster_file_name = '', $type = 'medium', $contentType = '') {
        /* $tagable = "Poster";
          if(strtolower($contentType)=='clips'){
          $tagable = 'Clips';
          $type='episode';
          } */
        $poster = $this->check_for_default_poster($id, "Movie", 'Poster', 'poster');
        if (!$poster) {
            return '/images/no-image.png';
        } else {
            $pgconnection = Yii::app()->db;
            $sql = "SELECT * FROM posters WHERE  id IN (" . $poster . ") AND rank=1 ORDER BY id DESC";
            //Poster.where("id IN (#{poster})").find(:all, :conditions => ["rank = 1"], :order => ["id desc"]).first
            $poster_image = $pgconnection->createCommand($sql)->queryAll();

            if (!empty($poster_image)) {
                if ($poster_image[0]['poster_file_name']) {
                    return "/system/posters/" . $poster_image[0]['id'] . "/" . $type . "/" . str_replace(" ", '%20', $poster_image[0]['poster_file_name']);
                } else {
                    return '/images/no-image.png';
                }
            } else {
                return ($poster_file_name == '') ? '/images/no-image.png' : "/system/posters/" . $id . "/" . $type . "/" . str_replace(" ", '%20', $poster_file_name);
            }
        }
    }

    /**
     * @method public getPoster(int $movie_id) Returns the Poster of the movie
     * @param int $movie_id Id of the movie to get the details
     * @param string $poster_file_name Name of the Poster if its avl in the films table
     * @param string $type Which type of image you need @example medium thumb standard original 
     * @author GDR<support@muvi.com>
     * @return String Poster path
     */
    /*
    function getPoster($object_id = '', $object_type = 'films', $size = 'original', $studio_id = 0) {
        if($studio_id == 0)
            $studio_id = Yii::app()->common->getStudiosId();
        $posters = Poster::model()->find('object_id=:object_id AND object_type=:object_type AND poster_file_name <> ""', array(':object_id' => $object_id, ':object_type' => $object_type), array('order' => 'id desc'));
        if (!$posters) {
            if ($object_type == 'topbanner') {
                return '';
            } else {
                if ($size == 'episode' || $studio_id == HOR_POSTER_STUDIO) {
                    return POSTER_URL . '/no-image-h.png';
                } else {
                    return POSTER_URL . '/no-image-a.png';
                }
            }
        } else {
            $pdata = $posters->attributes;
            if ($pdata['wiki_data'] != "") {
                return OLD_POSTER_URL . '/system/posters/' . $pdata['wiki_data'] . '/' . $size . '/' . urlencode($pdata['poster_file_name']);
            } else {
                if($studio_id == 0)
                    $studio_id = Yii::app()->common->getStudiosId();
                if($pdata['poster_file_name']!=''){
                $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                return $url.'/system/posters/' . $pdata['id'] . '/' . $size . '/' . urlencode($pdata['poster_file_name']);
                }
                else
                {
                    if ($object_type == 'moviestream') {
                  return POSTER_URL . '/no-image-h.png';
                } else {
                    return POSTER_URL . '/no-image-a.png';
                } 
                    
                }
                
                
                }
        }
    }
     * 
     */
    function getPoster($object_id = '', $object_type = 'films', $size = 'original', $studio_id = 0, $mapped_id = 0, $is_embed = 0) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        if ($is_embed) {
            $studio = Studio::model()->findByPk($studio_id);
        } else {
            $studio = $this->studio;
        }
        $poster_sizes = $this->poster_sizes;
        $get_horizontaltposter = Yii::app()->general->getdefaulthorizontalposter();
        $get_verticaltposter = Yii::app()->general->getdefaultverticalposter();
        $template_name = $studio->parent_theme;
        $theme_name = $studio->theme;
        $theme_path = Yii::app()->getBaseUrl(true) . '/themes/' . $theme_name . '/images/';
        $root_poster_path = Yii::app()->getBaseUrl(true) . '/img';
        $criteria = new CDbCriteria;
        $criteria->condition = 'object_id=:object_id AND object_type=:object_type AND poster_file_name <> ""';
        $criteria->params = array(
            ':object_id' => $object_id,
            ':object_type' => $object_type
        );
        $criteria->order = 'id desc';
        $criteria->limit = 1;
        $posters = Poster::model()->findAll($criteria);
        if (!$posters) {
            if ($object_type == 'topbanner') {
                return '';
            } else {
                if (($object_type == 'episode' || $object_type == 'moviestream') && $poster_sizes['horizontal']['width'] < $poster_sizes['horizontal']['height']) {
                    if (file_exists('themes/' . $theme_name . '/images/No-Image-Vertical.png')) {
                        return $theme_path . 'No-Image-Vertical.png';
                    } else {
                        if ($get_verticaltposter != '') {
                            return $get_verticaltposter;
                        } else {
                            return $root_poster_path . '/No-Image-Vertical.png';
                        }
                    }
                } else if ($object_type == 'films' && $poster_sizes['vertical']['width'] > $poster_sizes['vertical']['height']) {
                    if (file_exists('themes/' . $theme_name . '/images/No-Image-Horizontal.png')) {
                        return $theme_path . 'No-Image-Horizontal.png';
                    } else {

                        if ($get_horizontaltposter != '') {
                            return $get_horizontaltposter;
                        } else {
                            return $root_poster_path . '/No-Image-Horizontal.png';
                        }
                    }
                } else if ($size == 'episode' || $studio_id == HOR_POSTER_STUDIO) {
                    if (file_exists('themes/' . $theme_name . '/images/No-Image-Horizontal.png')) {
                        return $theme_path . 'No-Image-Horizontal.png';
                    } else {
                        if ($get_horizontaltposter != '') {
                            return $get_horizontaltposter;
                        } else {
                            return $root_poster_path . '/No-Image-Horizontal.png';
                        }
                    }
                } else {
                    if (file_exists('themes/' . $theme_name . '/images/No-Image-Vertical.png')) {
                        return $theme_path . 'No-Image-Vertical.png';
                    } else {
                        if ($get_verticaltposter != '') {
                            return $get_verticaltposter;
                        } else {
                            return $root_poster_path . '/No-Image-Vertical.png';
                        }
                    }
                }
            }
        } else {
            $posters = $posters[0];
            $pdata = $posters->attributes;
            if ($pdata['poster_file_name'] != '') {
                if($mapped_id && (isset($_SESSION[$studio_id]['parent_studio_id']) && $_SESSION[$studio_id]['parent_studio_id']))
                    $url = Yii::app()->common->getPosterCloudFrontPath($_SESSION[$studio_id]['parent_studio_id']);
                else    
                    $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                if ($size == 'roku' && $pdata['is_roku_poster'] != 1) {
                    return '';
                } else {
                    return $url . '/system/posters/' . $pdata['id'] . '/' . $size . '/' . urlencode($pdata['poster_file_name']);
                }
            } else {
                if ($object_type == 'moviestream') {
                    if ($get_horizontaltposter != '') {
                        return $get_horizontaltposter;
                    } else {
                        return $root_poster_path . '/No-Image-Horizontal.png';
                    }
                } else {
                    if ($get_verticaltposter != '') {
                        return $get_verticaltposter;
                    } else {
                        return $root_poster_path . '/No-Image-Vertical.png';
                    }
                }
            }
        }
    }

    function getProfilePicture($object_id = '', $object_type = 'films', $size = 'original', $studio_id = 0) {
        $posters = Poster::model()->find('object_id=:object_id AND object_type=:object_type', array(':object_id' => $object_id, ':object_type' => $object_type));
        $theme = (isset($this->studio->parent_theme) && trim($this->studio->parent_theme)) ? $this->studio->parent_theme : Studio::model()->findByPk($studio_id)->parent_theme;
        $local_poster_path = $this->siteurl . '/images';
        if (isset($posters) && !empty($posters)){
            $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
            $pdata = $posters->attributes;
            return $url . '/system/profile_images/' . $pdata['id'] . '/' . $size . '/' . urlencode($pdata['poster_file_name']);
        } else {
            if ($object_type == 'topbanner') {
                return '';
            } else {
               if ($theme =='traditional-byod' ){
                    return $local_poster_path .'/no-user-traditional.jpg';
                } 
                else if ($theme =='audio-streaming' ){
                    return $local_poster_path .'/no-user-audio.jpg';
                }else{
                return POSTER_URL . '/no-user.png';
                }
            }
        }
    }

    /**
     * @method public getBannerImage(int $movie_id) Returns the Poster of the movie
     * @param int $movie_id Id of the movie to get the details
     * @author GDR<support@muvi.com>
     * @return String Poster path
     */
    function getBannerImage($id = '') {
        $poster = $this->check_for_default_poster($id, "Movie", "Banner", "banner");
        if (!$poster) {
            return '';
        } else {
            $pgconnection = Yii::app()->db;
            $sql = "SELECT * FROM posters WHERE  id IN (" . $poster . ") AND rank=1 ORDER BY id DESC";
            //Poster.where("id IN (#{poster})").find(:all, :conditions => ["rank = 1"], :order => ["id desc"]).first
            $poster_image = $pgconnection->createCommand($sql)->queryAll();
            if (!empty($poster_image)) {
                if ($poster_image[0]['poster_file_name']) {
                    return "/system/posters/" . $poster_image[0]['id'] . "/original/" . str_replace(" ", '%20', $poster_image[0]['poster_file_name']);
                } else {
                    return '';
                }
            } else {
                return ($poster_file_name == '') ? '' : "/system/posters/" . $id . "/original/" . str_replace(" ", '%20', $poster_file_name);
            }
        }
    }

    /**
     * @method public getEpisodePoster(int $movie_id) Returns the Poster of the movie
     * @param int $movie_stream_id Movie Stream id for the episode.
     * @author GDR<support@muvi.com>
     * @return String Poster path
     */
    function getEpisodePoster($id = '') {
        $poster = $this->check_for_default_poster($id, "Movie", "Episode", "episode");
        if (!$poster) {
            return '/images/episode.jpg';
        } else {
            $pgconnection = Yii::app()->db;
            $sql = "SELECT * FROM posters WHERE  id IN (" . $poster . ") AND rank=1 ORDER BY id DESC";
            //Poster.where("id IN (#{poster})").find(:all, :conditions => ["rank = 1"], :order => ["id desc"]).first
            $poster_image = $pgconnection->createCommand($sql)->queryAll();
            if (!empty($poster_image)) {
                if ($poster_image[0]['poster_file_name']) {
                    return "/system/posters/" . $poster_image[0]['id'] . "/original/" . urlencode($poster_image[0]['poster_file_name']);
                } else {
                    return '/images/episode.jpg';
                }
            } else {
                return ($poster_file_name == '') ? '/images/episode.jpg' : "/system/posters/" . $id . "/original/" . urlencode($poster_file_name);
            }
        }
    }

    public function getEpisodestoShow($movie_id, $s_no = "", $contentTypePermalink = '') {

        $studio_id = Yii::app()->common->getStudiosId();
        $dbcon = Yii::app()->db;
        if ($s_no == "") {
            $sql = "select * from movie_streams where episode_title IS NOT NULL AND movie_id = " . $movie_id . " AND is_converted=1 ORDER BY episode_number ASC";
        } else {
            $sql = "select * from movie_streams where episode_number IS NOT NULL AND series_number IS NOT NULL AND is_converted=1 AND  series_number = " . $s_no . " AND movie_id = " . $movie_id . " ORDER BY episode_number ASC";
        }
        if (defined('S3BUCKET_ID')) {
            $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
        } elseif (isset(Yii::app()->user->s3bucket_id) && Yii::app()->user->s3bucket_id) {
            $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
        } else {
            $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
        }

        $max_sql = "select distinct(series_number) from movie_streams where series_number IS NOT NULL AND movie_id = " . $movie_id . " ORDER BY series_number";
        $epis = $dbcon->createCommand($sql)->queryAll();
        $s_nos = $dbcon->createCommand($max_sql)->queryAll();
        $episodes = array();
        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $movie_id));

        foreach ($epis as $k => $v) {
            $epi_can_see = true;
            $epi_url = CDN_HTTP . $bucketInfo['cloudfront_url'] . '/uploads/movie_stream/full_movie/' . $v['id'] . '/' . $v['full_movie'];
            $episode_poster = $this->getPoster($v['id'], 'moviestream', 'episode');

            if (Yii::app()->session['logged_user_id'] != '') {
                $log_sql = "Select * from video_logs where user_id = " . Yii::app()->session['logged_user_id'] . " and episode_id = " . $v['id'] . " and watch_status = 'complete' and EXTRACT(MONTH FROM created_at) = " . date('n');
                $log_obj = $dbcon->createCommand($log_sql);
                $log_data = $log_obj->queryAll();
                if (count($log_data) == 3) {
                    $epi_can_see = false;
                }
            }
            $episodes[] = array('id' => $v['id'], 'embed_id' => $v['embed_id'], 'movie_id' => $v['movie_id'], 'uniq_id' => $Films->uniq_id, 'content_type_id' => $Films->content_type_id, 'ppv_plan_id' => $Films->ppv_plan_id, 'episode_number' => $v['episode_number'], 'series_number' => $v['series_number'], 'episode_title' => $v['episode_title'], 'episode_story' => $v['episode_story'], 'episode_date' => $v['episode_date'], 'episode_path' => $epi_url, 'epi_can_see' => $epi_can_see, 'episode_poster' => $episode_poster, 'permalink' => $Films->permalink, 'contentTypePermalink' => @$contentTypePermalink);
        }

        if (count($s_nos) > 0) {
            $episodes[] = array('s_no' => $s_nos);
        } else {
            $episodes[] = array('s_no' => '');
        }
        return $episodes;
    }

    ///// returns episodes detail of movie /////////
    public function getEpisodes($movie_id, $s_no = "") {

        $studio_id = Yii::app()->common->getStudiosId();
        $dbcon = Yii::app()->db;
        if ($s_no == "") {
            $sql = "select * from movie_streams where episode_title IS NOT NULL AND movie_id = " . $movie_id . " AND is_converted=1 ORDER BY episode_number DESC";
        } else {
            $sql = "select * from movie_streams where episode_number IS NOT NULL AND series_number IS NOT NULL AND is_converted=1 AND  series_number = " . $s_no . " AND movie_id = " . $movie_id . " ORDER BY episode_number DESC";
        }
        if (defined('S3BUCKET_ID')) {
            $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
        } elseif (isset(Yii::app()->user->s3bucket_id) && Yii::app()->user->s3bucket_id) {
            $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
        } else {
            $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
        }

        $max_sql = "select distinct(series_number) from movie_streams where series_number IS NOT NULL AND movie_id = " . $movie_id . " ORDER BY series_number";
        $epis = $dbcon->createCommand($sql)->queryAll();
        $s_nos = $dbcon->createCommand($max_sql)->queryAll();
        $episodes = array();
        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $movie_id));

        foreach ($epis as $k => $v) {
            $epi_can_see = true;
            $epi_url = CDN_HTTP . $bucketInfo['cloudfront_url'] . '/uploads/movie_stream/full_movie/' . $v['id'] . '/' . $v['full_movie'];
            $episode_poster = $this->getPoster($v['id'], 'moviestream', 'episode');

            if (Yii::app()->session['logged_user_id'] != '') {
                $log_sql = "Select * from video_logs where user_id = " . Yii::app()->session['logged_user_id'] . " and episode_id = " . $v['id'] . " and watch_status = 'complete' and EXTRACT(MONTH FROM created_at) = " . date('n');
                $log_obj = $dbcon->createCommand($log_sql);
                $log_data = $log_obj->queryAll();
                if (count($log_data) == 3) {
                    $epi_can_see = false;
                }
            }
            $episodes[] = array('id' => $v['id'], 'embed_id' => $v['embed_id'], 'movie_id' => $v['movie_id'], 'uniq_id' => $Films->uniq_id, 'content_type_id' => $Films->content_type_id, 'content_types_id' => $Films->content_types_id, 'ppv_plan_id' => $Films->ppv_plan_id, 'episode_number' => $v['episode_number'], 'series_number' => $v['series_number'], 'episode_title' => $v['episode_title'], 'episode_story' => $v['episode_story'], 'episode_date' => $v['episode_date'], 'episode_path' => $epi_url, 'epi_can_see' => $epi_can_see, 'episode_poster' => $episode_poster, 'permalink' => $Films->permalink);
        }

        if (count($s_nos) > 0) {
            $episodes[] = array('s_no' => $s_nos);
        } else {
            $episodes[] = array('s_no' => '');
        }
        return $episodes;
    }

    function check_for_default_poster($tagger_id, $tagger_type, $taggable_type, $context) {
        $id = "";
        $poster = '';
        $pgconnection = Yii::app()->db1;
        $sql = "SELECT t2.taggable_id FROM taggings t2,taggings t1 WHERE t1.taggable_id=t2.taggable_id AND lower(t1.tagger_type)='" . $context . "' AND t2.taggable_type='" . $taggable_type . "' AND t2.tagger_id='" . $tagger_id . "' AND t2.tagger_type='" . $tagger_type . "' GROUP BY t2.taggable_id";
        $poster = $pgconnection->createCommand($sql)->queryAll();
        //Tagging.where("t2.taggable_type = '#{taggable_type}' and t2.tagger_id='#{tagger_id}' and t2.tagger_type='#{tagger_type}'").find(:all, :select => ["t2.taggable_id"], :joins => ["inner join taggings t2 on taggings.taggable_id = t2.taggable_id"], :conditions => ["lower(taggings.tagger_type)='#{context}'"], :group => ["t2.taggable_id"])
        if (!empty($poster)) {
            foreach ($poster AS $pos) {
                $id .= $pos['taggable_id'] . ",";
            }
            $id = trim($id, ',');
        }
        if ($id) {
            return $id;
        } else {
            return false;
        }
    }

    /**
     * @method public getCelebrityImage($celebrity_id,$id,$clebrity_image=') Returns the Poster of the movie
     * @param int $movie_id Id of the movie to get the details
     * @param int $celebrity_id Id of the clebrity 
     * @param string $clebrity_image Name of the Poster if its avl in the films table
     * @author GDR<support@muvi.com>
     * @return String Poster path
     */
    function getCelebrityImage($celebrity_id, $id, $clebrity_image = '') {
        if (!$clebrity_image) {
            //poster = Poster.check_for_default_poster(self.id, "Celebrity", "Poster", "profilepic")
            $poster = $this->check_for_default_poster($celebrity_id, "Celebrity", "Poster", "profilepic");
            if (!$poster) {
                return '/images/no-image.png';
            } else {
                $pgconnection = Yii::app()->db2;
                $sql = "SELECT * FROM posters WHERE  id IN (" . $poster . ") AND rank=1 ORDER BY id DESC";
                $poster_image = $pgconnection->createCommand($sql)->queryAll();
                if (!empty($poster_image)) {
                    if ($poster_image[0]['poster_file_name']) {
                        return "/system/posters/" . $poster_image[0]['id'] . "/thumb/" . str_replace(" ", '%20', $poster_image[0]['poster_file_name']);
                    } else {
                        return '/images/no-image.png';
                    }
                } else {
                    return ($clebrity_image == '') ? '/images/no-image.png' : "/system/posters/" . $celebrity_id . "/thumb/" . str_replace(" ", '%20', $clebrity_image);
                }
            }
        } else {
            return "/system/profile_pictures/" . $celebrity_id . "/thumb/" . str_replace(" ", '%20', $clebrity_image);
        }
    }

    /**
     * @method public getTrailer($movie_id,) Returns the trailer url for the Moive
     * @param int $movie_id Id of the movie to get the Trailer
     * @param int $return_type If its mentioned then it will return the complete information of trailer else just the URL.
     * @author GDR<support@muvi.com>
     * @return String Poster path
     */
    function getTrailer($movie_id = '', $return_type = 0,$studio_id = 0) {
        if ($movie_id) {
            $data = movieTrailer::model()->findAllByAttributes(array('movie_id' => $movie_id));
            if ($data) {
                if($data[0]->third_party_url != '' && $data[0]->trailer_file_name != ''){
                    return $data[0]->third_party_url;
                } else {
                    if ($data[0]->video_remote_url != '' && $data[0]->trailer_file_name != '') {
                        if (HOST_IP == '127.0.0.1') {
                            return $data[0]->video_remote_url;
                        } else {
                            $videoUrl = $data[0]->video_remote_url;
                            if($studio_id == 0){
                                $studio_id = Yii::app()->common->getStudiosId();
                            }
                            $cloudFront = Yii::app()->common->connectToAwsCloudFront($studio_id);
                            $expires = time() + 10000;

                            $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
                                'url' => $videoUrl,
                                'expires' => $expires,
                            ));
                            return $signedUrlCannedPolicy;
                        }
                    } else {
                        return '';
                    }
                } 
            }
            else {
                return '';
            }
        } else {
            return '';
        }
    }

    function getTrailerIsConverted($movie_id = '', $return_type = 0) {
        if ($movie_id) {
            $data = movieTrailer::model()->findAllByAttributes(array('movie_id' => $movie_id));
            if ($data) {
                return $data[0]->is_converted;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    function getMoiveUrl($movie_id = '') {
        if ($movie_id) {
            $movieUrl = '';
            $connectionwiki = Yii::app()->db;
            $data = $connectionwiki->createCommand("SELECT full_movie,id,wiki_data FROM movie_streams WHERE movie_id=" . $movie_id . " AND is_episode=0 AND full_movie!='' LIMIT 1")->queryAll();
            if ($data) {
                $s3dir = 'uploads/movie_stream/full_movie/';
                if ($data[0]["wiki_data"] != "") {
                    return CDN_HTTP . "muviassetsdev.s3.amazonaws.com/" . $s3dir . $data[0]["wiki_data"] . "/" . $data[0]["full_movie"];
                } elseif ($data[0]["full_movie"] != "") {
                    if (defined('S3BUCKET_ID')) {
                        $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
                    } elseif (isset(Yii::app()->user->s3bucket_id) && Yii::app()->user->s3bucket_id) {
                        $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
                    } else {
                        $bucketInfo = Yii::app()->common->getBucketInfo('', Yii::app()->common->getStudioId());
                    }
                    $movieUrl = CDN_HTTP . $bucketInfo['cloudfront_url'] . '/' . $s3dir . $data[0]["id"] . "/" . urlencode($data[0]["full_movie"]);
                    //$movieUrl = VIDEO_URL.'/'.$s3dir.$data[0]["id"]."/".urlencode($data[0]["full_movie"]);
                    return $movieUrl;
                } else {
                    return;
                }
            } else {
                return;
            }
        } else {
            return;
        }
    }

    function getvideoResolution($differentResolution = '', $videoPath = '', $resolutionType = '') {
        $multipleVideo = array();
        $diffvideoResolution = array();
        $diffvideoResolution['BEST'] = 'BEST';
        $diffvideoResolution[3840] = 2160;
        $diffvideoResolution[2560] = 1440;
        $diffvideoResolution[1920] = 1080;
        $diffvideoResolution[1210] = 720;
        $diffvideoResolution[900] = 640;
        $diffvideoResolution[480] = 480;
        $diffvideoResolution[317] = 360;
        $diffvideoResolution[242] = 240;
        $diffvideoResolution[108] = 144;
        if ($differentResolution != '') {
            $videoResolution = explode(",", $differentResolution);
            $vidoInfo = new SplFileInfo($videoPath);
            $videoExt = $vidoInfo->getExtension();
            $fileNameWithUrl = substr($videoPath, 0, -4);
            foreach ($videoResolution as $key => $value) {
                if ($key == 0) {
                    $trailerUrl = $videoPath;
                } else {
                    $trailerUrl = $fileNameWithUrl . "_" . $value . "." . $videoExt;
                }
                $multipleVideo[$diffvideoResolution[$value]] = $trailerUrl;
            }
            if($resolutionType == 'SD'){
                $isResolutionUnset = 0;
                foreach($multipleVideo as $multipleVideoKey => $multipleVideoVal){
                    if($multipleVideoKey >= 720){
                        unset($multipleVideo[$multipleVideoKey]);
                        $isResolutionUnset++;
                    }
                }
                if($isResolutionUnset > 0){
                    unset($multipleVideo['BEST']);
                }
            }
        } else {
            $multipleVideo[144] = $videoPath;
        }
        return $multipleVideo;
    }

    function getVideoToBePlayed($multipleVideo = '', $video_path = '', $internetSpeed = 0) {
        $defaultResolution = 144;
        if (isset(Yii::app()->user->internetSpeed) || isset($_SESSION['internetSpeed']) || $internetSpeed != 0) {
            if ($internetSpeed > 0) {
                $internetSpeedUser = $internetSpeed;
            } else if (isset(Yii::app()->user->internetSpeed)) {
                $internetSpeedUser = Yii::app()->user->internetSpeed;
            } else {
                $internetSpeedUser = $_SESSION['internetSpeed'];
            }
        }
        if (isset($internetSpeedUser)) {
            if ($internetSpeedUser >= internetSpeedSet7) {
                if (isset($multipleVideo[2160])) {
                    $video_path = $multipleVideo[2160];
                    $defaultResolution = 2160;
                } else if (isset($multipleVideo[1440])) {
                    $video_path = $multipleVideo[1440];
                    $defaultResolution = 1440;
                } else if (isset($multipleVideo[1080])) {
                    $video_path = $multipleVideo[1080];
                    $defaultResolution = 1080;
                } else if (isset($multipleVideo[720])) {
                    $video_path = $multipleVideo[720];
                    $defaultResolution = 720;
                } else if (isset($multipleVideo[640])) {
                    $video_path = $multipleVideo[640];
                    $defaultResolution = 640;
                } else if (isset($multipleVideo[480])) {
                    $video_path = $multipleVideo[480];
                    $defaultResolution = 480;
                } else if (isset($multipleVideo[360])) {
                    $video_path = $multipleVideo[360];
                    $defaultResolution = 360;
                } else if (isset($multipleVideo[240])) {
                    $video_path = $multipleVideo[240];
                    $defaultResolution = 240;
                } else {
                    $video_path = $multipleVideo[144];
                }
            }
            else if ($internetSpeedUser >= internetSpeedSet6) {
                if (isset($multipleVideo[1440])) {
                    $video_path = $multipleVideo[1440];
                    $defaultResolution = 1440;
                } else if (isset($multipleVideo[1080])) {
                    $video_path = $multipleVideo[1080];
                    $defaultResolution = 1080;
                } else if (isset($multipleVideo[720])) {
                    $video_path = $multipleVideo[720];
                    $defaultResolution = 720;
                } else if (isset($multipleVideo[640])) {
                    $video_path = $multipleVideo[640];
                    $defaultResolution = 640;
                } else if (isset($multipleVideo[480])) {
                    $video_path = $multipleVideo[480];
                    $defaultResolution = 480;
                } else if (isset($multipleVideo[360])) {
                    $video_path = $multipleVideo[360];
                    $defaultResolution = 360;
                } else if (isset($multipleVideo[240])) {
                    $video_path = $multipleVideo[240];
                    $defaultResolution = 240;
                } else {
                    $video_path = $multipleVideo[144];
                }
            }
            else if ($internetSpeedUser >= internetSpeedSet1) {
                if (isset($multipleVideo[1080])) {
                    $video_path = $multipleVideo[1080];
                    $defaultResolution = 1080;
                } else if (isset($multipleVideo[720])) {
                    $video_path = $multipleVideo[720];
                    $defaultResolution = 720;
                } else if (isset($multipleVideo[640])) {
                    $video_path = $multipleVideo[640];
                    $defaultResolution = 640;
                } else if (isset($multipleVideo[480])) {
                    $video_path = $multipleVideo[480];
                    $defaultResolution = 480;
                } else if (isset($multipleVideo[360])) {
                    $video_path = $multipleVideo[360];
                    $defaultResolution = 360;
                } else if (isset($multipleVideo[240])) {
                    $video_path = $multipleVideo[240];
                    $defaultResolution = 240;
                } else {
                    $video_path = $multipleVideo[144];
                }
            } else if ($internetSpeedUser >= internetSpeedSet2) {
                if (isset($multipleVideo[720])) {
                    $video_path = $multipleVideo[720];
                    $defaultResolution = 720;
                } else if (isset($multipleVideo[640])) {
                    $video_path = $multipleVideo[640];
                    $defaultResolution = 640;
                } else if (isset($multipleVideo[480])) {
                    $video_path = $multipleVideo[480];
                    $defaultResolution = 480;
                } else if (isset($multipleVideo[360])) {
                    $video_path = $multipleVideo[360];
                    $defaultResolution = 360;
                } else if (isset($multipleVideo[240])) {
                    $video_path = $multipleVideo[240];
                    $defaultResolution = 240;
                } else {
                    $video_path = $multipleVideo[144];
                }
            } else if ($internetSpeedUser >= internetSpeedSet3) {
                if (isset($multipleVideo[640])) {
                    $video_path = $multipleVideo[640];
                    $defaultResolution = 640;
                } else if (isset($multipleVideo[480])) {
                    $video_path = $multipleVideo[480];
                    $defaultResolution = 480;
                } else if (isset($multipleVideo[360])) {
                    $video_path = $multipleVideo[360];
                    $defaultResolution = 360;
                } else if (isset($multipleVideo[240])) {
                    $video_path = $multipleVideo[240];
                    $defaultResolution = 240;
                } else {
                    $video_path = $multipleVideo[144];
                }
            } else if ($internetSpeedUser >= internetSpeedSet4) {
                if (isset($multipleVideo[360])) {
                    $video_path = $multipleVideo[360];
                    $defaultResolution = 360;
                } else if (isset($multipleVideo[240])) {
                    $video_path = $multipleVideo[240];
                    $defaultResolution = 240;
                } else if (isset($multipleVideo[144])){
                    $video_path = $multipleVideo[144];
                    $defaultResolution = 144;
                } else if (isset($multipleVideo[480])){
                    $video_path = $multipleVideo[480];
                    $defaultResolution = 480;
                }
            } else {
                if ($internetSpeed == 0) {
                    if (isset($multipleVideo[240])) {
                        $video_path = $multipleVideo[240];
                        $defaultResolution = 240;
                    } else if (isset($multipleVideo[144])){
                        $video_path = $multipleVideo[144];
                        $defaultResolution = 144;
                    } else if (isset($multipleVideo[360])) {
                        $video_path = $multipleVideo[360];
                        $defaultResolution = 360;
                    } else if (isset($multipleVideo[480])){
                        $video_path = $multipleVideo[480];
                        $defaultResolution = 480;
                    }
                } else {
                    if ($internetSpeedUser >= internetSpeedSet5) {
                        if (isset($multipleVideo[240])) {
                            $video_path = $multipleVideo[240];
                            $defaultResolution = 240;
                        } else if (isset($multipleVideo[144])){
                            $video_path = $multipleVideo[144];
                            $defaultResolution = 144;
                        } else if (isset($multipleVideo[360])) {
                            $video_path = $multipleVideo[360];
                            $defaultResolution = 360;
                        } else if (isset($multipleVideo[480])){
                            $video_path = $multipleVideo[480];
                            $defaultResolution = 480;
                        }
                    } else { 
                        if (isset($multipleVideo[144])){
                            $video_path = $multipleVideo[144];
                            $defaultResolution = 144;
                        } else if (isset($multipleVideo[240])){
                            $video_path = $multipleVideo[240];
                            $defaultResolution = 240;
                        } else if (isset($multipleVideo[360])) {
                            $video_path = $multipleVideo[360];
                            $defaultResolution = 360;
                        } else if (isset($multipleVideo[480])){
                            $video_path = $multipleVideo[480];
                            $defaultResolution = 480;
                        }
                    }
                }
            }
        } else {
            if (isset($multipleVideo[240])) {
                $video_path = $multipleVideo[240];
                $defaultResolution = 240;
            } else if (isset($multipleVideo[144])){
                $video_path = $multipleVideo[144];
                $defaultResolution = 144;
            } else if (isset($multipleVideo[360])) {
                $video_path = $multipleVideo[360];
                $defaultResolution = 360;
            } else if (isset($multipleVideo[480])){
                $video_path = $multipleVideo[480];
                $defaultResolution = 480;
            }
        }
        return $video_path . "," . $defaultResolution;
    }

    function getEpisodeUrl($epi_id = '', $movie_id = '') {
        $mov_url = "";
        $studio_id = Yii::app()->common->getStudioId();
        if ($epi_id || $movie_id) {
            if (defined('S3BUCKET_ID')) {
                $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
            } elseif (isset(Yii::app()->user->s3bucket_id) && Yii::app()->user->s3bucket_id) {
                $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
            } else {
                $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
            }
            if ($epi_id) {
                $streamData = movieStreams::model()->findAllByAttributes(array('id' => $epi_id, 'studio_id' => $studio_id));
                if ($streamData && $streamData[0]->wiki_data) {
                    return CDN_HTTP . "muviassetsdev.s3.amazonaws.com/" . $s3dir . $streamData[0]->wiki_data . "/" . $streamData[0]->full_movie;
                } elseif ($streamData) {
                    return CDN_HTTP . $bucketInfo['cloudfront_url'] . '/uploads/movie_stream/full_movie/' . $streamData[0]->id . '/' . $streamData[0]->full_movie;
                } else {
                    return '';
                }
            } elseif ($movie_id) {
                $streamData = movieStreams::model()->findAllByAttributes(array('movie_id' => $movie_id, 'is_episode' => "1", 'studio_id' => Yii::app()->common->getStudioId()), array('order' => 'id ASC'));
                if ($streamData && $streamData[0]->wiki_data) {
                    $mov_url = CDN_HTTP . "muviassetsdev.s3.amazonaws.com/" . $s3dir . $streamData[0]->wiki_data . "/" . $streamData[0]->full_movie;
                } elseif ($streamData) {
                    $mov_url = CDN_HTTP . $bucketInfo['cloudfront_url'] . '/uploads/movie_stream/full_movie/' . $streamData[0]->id . '/' . $streamData[0]->full_movie;
                } else {
                    return '';
                }
                if ($mov_url != "") {
                    $arr = array("url" => $mov_url, "episode_id" => $streamData[0]->id);
                    return $arr;
                }
            } else {
                return;
            }
        } else {
            return;
        }
    }

    function getFullVideoPath($stream_id, $s3bucket_id = 0, $studio_id = 0,$arg=array()) {
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        if ($stream_id > 0) {
            $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
			if($arg){
				$streamData = (object)$arg;
              } else {
            $streamData = movieStreams::model()->findByPk($stream_id);
			}
           if ($streamData && (($streamData->is_demo == '1') || ($streamData->full_movie !=''))) {
                if($streamData->is_demo == 1)
                    $mov_url = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/small.mp4';
                else
                    $mov_url = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/movie_stream/full_movie/' . $stream_id . '/' . $streamData->full_movie;
            } else {
                return '';
            }
            if ($mov_url != '') {
                return $mov_url;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    function mandrilEmail($template_name, $template_content, $message) {
        //if(strstr($_SERVER['HTTP_HOST'], 'maaflix.com')){
        require_once 'mandrill-api-php/src/Mandrill.php';
        $mandrill = new Mandrill('Xb3a0BtRxPQIJTbJwn1Vlg');
        $async = false;
        try {
            $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async);
            return $result;
        } catch (Exception $e) {
            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/muvi_dam/protected/runtime/email.log', 'a+');
            fwrite($fp, '\n\t Exception occured on dt:-' . date('m-d-Y H:i:s') . "\n\t " . $e->getMessage() . "\n Template name-" . $template_name . '\n Template Content:-' . print_r($template_content, TRUE));
            fclose($fp);
            return false;
        }
        //}else{
        //    return false;
        //}
    }

    function sendMandrilEmail($template_name, $template_content, $message) {

        require_once 'mandrill-api-php/src/Mandrill.php';
        $mandrill = new Mandrill('Xb3a0BtRxPQIJTbJwn1Vlg');
        $async = false;
        try {
            $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async);
            return $result;
        } catch (Exception $e) {
            $fp = fopen(__DIR__ . '/../../protected/runtime/email.log', 'a+');
            fwrite($fp, '\n\t Exception occured on dt:-' . date('m-d-Y H:i:s') . "\n\t " . $e->getMessage() . "\n Template name-" . $template_name . '\n Template Content:-' . print_r($template_content, TRUE));
            fclose($fp);
            return false;
        }
    }

    /**
     * @method public episodeThumbnail() It will generate video thumbnail from the uploaded video and store 
     * @author GDR<support@muvi.com>
     * @return bool TRUE/FALSE
     */
    function episodeThumbnail($studio_id = '') {
        if (isset($_REQUEST['studio_id'])) {
            $studio_id = $_REQUEST['studio_id'];
        }
        if (!$studio_id) {
            echo "Oops Sorry you don't have access";
            exit;
        }
        // where ffmpeg is located, such as /usr/sbin/ffmpeg
        $ffmpeg = FFMPEG_PATH;

        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
        $dbcon = Yii::app()->db;
        //Get the Video url from s3
        $sql = "SELECT ms.full_movie,ms.movie_id,ms.id,ms.episode_number,ms.episode_title,f.name,f.content_types_id,ms.is_poster FROM movie_streams AS ms,films f WHERE f.id=ms.movie_id AND  ms.studio_id=" . $studio_id . " AND  ms.full_movie!='' AND f.content_types_id !=1 AND is_poster!=1";
        $data = $dbcon->createCommand($sql)->queryAll();
		
        //$bucket = Yii::app()->params->video_bucketname;
        $bucket = @$bucketInfo['bucket_name'];
        $s3url = @$bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
        $signedFolderPath = $folderPath['signedFolderPath'];
        foreach ($data as $key => $val) {
            $s3viewdir = "https://" . $bucket . '.' . $s3url . '/' . $signedFolderPath . 'uploads/movie_stream/full_movie/' . $val['id'] . "/" . urlencode($val['full_movie']);
            $uid = $val['movie_id'];
            if (strtolower($data[0]['content_types_id']) == 2) {
                $imgname = 'clips_' . $this->cleanSrting($val['name']) . '.jpg';
                $uid = $val['movie_id'];
            } else {
                $imgname = 'episode' . $val['episode_number'] . ".jpg";
            }

            $upload_dir = $_SERVER['DOCUMENT_ROOT'] . "/images/videoThumbnail/";
            $dir = $upload_dir . $val['id'] . "/";

            if (!is_dir($dir)) {
                mkdir($dir);
                @chmod($dir, 0775);
            }
            $dir .="original/";
            if (!is_dir($dir)) {
                mkdir($dir);
                chmod($dir, 0775);
            }
            $img_path = $dir . $imgname;
            //echo "image path===".$img_path;
            //$img_path = $upload_dir.'episode'.$val['episode_number'].".jpg";

            $second = 1;

            // get the duration and a random place within that
            $cmd = "$ffmpeg -i $s3viewdir 2>&1";
            if (preg_match('/Duration: ((\d+):(\d+):(\d+))/s', `$cmd`, $time)) {
                $total = ($time[2] * 3600) + ($time[3] * 60) + $time[4];
                $second = rand(1, ($total - 1));
            }
            //screenshot size
            $size = '560x312';

            // get the screenshot
            //$cmd = "$ffmpeg -ss $second -i  $s3viewdir $img_path -r 1 -y -s $size -vframes 1 -an -vcodec mjpeg";
            if (HOST_IP == '127.0.0.1') {
                $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";
            } else {
                $cmd = "sudo $ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";
            }
            //echo $cmd;
            //ffmpeg command
            //$cmd = "$ffmpeg -i $s3viewdir -deinterlace -an -ss $second -f mjpeg -t 1 -r 1 -y -s $size $img_path 2>&1";       
            //echo $cmd.'<br>';
            exec($cmd, $output, $return);
            //print_r($output);
            if ($return && is_file($img_path)) {
                $this->uploadThumbnail($val['id'], $imgname, $val['movie_id'], $img_path, $val['name'], $val['content_types_id'],$studio_id);
            }
        }
    }

    /**
     * @method public uploadthumbnail() Upload the episode thumbnail to s3 and save in DB
     * @param int $movie_stream_id Movie Stream id
     * @param string $posterName Name of the uploaded poster
     * @param int $movie_id Movie id
     * @param string $imgPath The uploaded path of the image
     * @param string $movieName Name of the Movie
     * @author GDR<support@muvi.com>
     * @return bool true/false
     */
    function uploadThumbnail($id = '', $posterName = '', $movie_id = '', $imgPath = '', $movieName = '', $content_type = '',$studio_id='') {
        if ($id) {
			if(!$studio_id){
            $studio_id = Yii::app()->common->getStudioId();
			}
            if ($content_type == 2) {
                $object_type = 'films';
                $object_id = $movie_id;
            } else {
                $object_type = 'moviestream';
                $object_id = $id;
            }
            $pdata = Poster::model()->find('object_id=:object_id AND object_type=:object_id ', array(':object_id' => $object_id, ':object_type' => $object_type));
            if ($pdata) {
                $pdata->poster_file_name = $posterName;
                $pdata->last_updated_by = @Yii::app()->user->id;
                $pdata->last_updated_date = gmdate('Y-m-d');
                $pdata->save();
                $uid = $pdata->id;
            } else {
                $poster = new Poster();
                $poster->poster_file_name = $posterName;
                $poster->object_type = $object_type;
                $poster->object_id = $object_id;
                $poster->created_by = @Yii::app()->user->id;
                $poster->created_date = gmdate('Y-m-d');
                $poster->ip = $_SERVER['REMOTE_ADDR'];
                $poster->save();
                $uid = $poster->id;
            }
            require_once "Image.class.php";
            require_once "Config.class.php";
            require_once "Uploader.class.php";
            spl_autoload_unregister(array('YiiBase', 'autoload'));
            require_once "amazon_sdk/sdk.class.php";
            spl_autoload_register(array('YiiBase', 'autoload'));

            if (!defined('BASEPATH'))
                define("BASEPATH", dirname(__FILE__) . "/..");
            $s3_config = Yii::app()->common->getS3Details($studio_id);
            $config = Config::getInstance();
            $config->setS3Key($s3_config['key']);
            $config->setS3Secret($s3_config['secret']);;
            $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/videoThumbnail'); //path for images uploaded
            $bucketInfoForPoster = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $config->setBucketName($bucketInfoForPoster['bucket_name']);
            $config->setAmount(250);  //maximum paralell uploads
            $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
            $theme = $this->studio->parent_theme;
            if($theme == "traditional-byod"){
                $config->setDimensions(array('episode' => "264x181", 'thumb' => "185x256", 'standard' => '280x400'));   //resize to these sizes
             }else{
                $config->setDimensions(array('episode' => "280x156", 'thumb' => "185x256", 'standard' => '280x400'));   //resize to these sizes
             }    //resize to these sizes
            //usage of uploader class - this simple :)
            //Here $uid is the movie_stream_id 
            $uploader = new Uploader($id);
            $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            $ret = $uploader->cropVideothumb($unsignedBucketPath . 'public/system/posters/', $posterName, $uid);
            $poster = array($ret);
            //echo "<pre>";print_r($poster);
            //Updating Poster table for episode having poster 
            $dbcon = Yii::app()->db;
            $upd_post = "UPDATE movie_streams SET is_poster=1 WHERE id=" . $id;
            $dbcon->createCommand($upd_post)->execute();
            return $ret;
        }
    }

    /**
     * @method public checkRemoteFile($url)
     * @param type $url
     * @return boolean
     * @author GDR<info@muvi.com>
     */
    function checkRemoteFile($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (curl_exec($ch) !== FALSE) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function accepts an array and joins all values in a string separated by
     * a comma except the last value which is preceeded by 'and' instead of a comma
     */
    function to_sentence($array = array()) {
        if (empty($array) || !is_array($array)) {
            return $array;
        }
        $cnt = count($array);
        $res = array();
        for ($i = 0; $i < $cnt; $i++) {
            if ($array[$i] != '') {
                array_push($res, trim($array[$i]));
            }
        }
        if ($cnt > 1) {
            array_shift($res);
        }
        switch (count($res)) {
            case 0 :
                $string = "";
                break;
            case 1:
                $string = array_pop($res);
                break;
            case 2:
                $string = implode(' and ', $res);
                break;
            default:
                //$last_string = array_pop( $res );
                $string = implode(', ', $res); // . ' and ' . $last_string;
                break;
        }

        return $string;
    }

    public function getUserDetail($user_id) {
        //$connectionmuvi = new CDbConnection('pgsql:host='.Yii::app()->params['pg_host'].';port='.Yii::app()->params['pg_port'].';dbname=muvi_data',Yii::app()->params['pg_username'],Yii::app()->params['pg_password']);
        $criteria = new CDbCriteria();
        $criteria->select = 'display_name';
        $criteria->condition = 'id=:user_id';
        $criteria->params = array(':user_id' => $user_id);
        $data = SdkUser::model()->find($criteria);
        if ($data) {
            $profile_image = $this->getPoster($user_id, 'profilepicture', 'thumb');
            $user_profile = array('display_name' => $data->display_name, 'profile_image' => $profile_image);
            return $user_profile;
        } else {
            return;
        }
    }

    function dateFormatOutputdateTime($date_time, $curdate = NULL, $type = NULL) {
        //echo $date_time."------".$curdate."<br/>";
        $curr = strtotime($curdate);
        $crted = strtotime($date_time);
        $diff_in_sec = ($curr - $crted);
        $diff_in_min = round(($curr - $crted) / 60);
        $diff_in_hr = round(($curr - $crted) / (60 * 60));
        if ($diff_in_sec < 60) {
            if ($diff_in_sec != 1) {
                //return $diff_in_sec." secs ago";
                return "just now";
            } else {
                //return $diff_in_sec." sec ago";
                return "just now";
            }
        } else if ($diff_in_min < 60) {
            if ($diff_in_min != 1) {
                return $diff_in_min . " mins ago";
            } else {
                return $diff_in_min . " min ago";
            }
        } else if ($diff_in_hr < 24) {
            if ($diff_in_hr != 1) {
                return $diff_in_hr . " hours ago";
            } else {
                return $diff_in_hr . " hour ago";
            }
        }
    }

    function facebook_style($date, $curdate = NULL, $type = NULL) {
        $checkDate = date("Y-m-d", strtotime($date));
        $checkCur = date("Y-m-d", strtotime($curdate));
        if ($checkDate == $checkCur) {
            if ($type == 'date') {
                return $this->dateFormatOutputdateTime($date, $curdate, 'date');
            } else {
                return $this->dateFormatOutputdateTime($date, $curdate, 'time');
            }
        }

        $timestamp = strtotime($date);
        $difference = strtotime($curdate) - $timestamp;
        //return $date." - ".$curdate;

        $periods = array("sec", "min", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        if ($difference > 0) { // this was in the past time
            $ending = "ago";
        } else { // this was in the future time
            $difference = -$difference;
            $ending = "to go";
        }
        for ($j = 0; $difference >= $lengths[$j]; $j++)
            $difference /= $lengths[$j];
        $difference = round($difference);
        if ($difference != 1)
            $periods[$j].= "s";
        $text = "$difference $periods[$j] $ending";
        return $text;
    }

    /**
     * @method generate_permalink() Generate Permalink from a String
     * @param type $string
     * @return type
     * @return string 
     * @author GDR<support@muvi.com>
     */
    function generate_permalink($string) {
        $name = $string;
        if ($string !== mb_convert_encoding(mb_convert_encoding($string, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32'))
            $string = mb_convert_encoding($string, 'UTF-8', mb_detect_encoding($string));
        $string = htmlentities($string, ENT_NOQUOTES, 'UTF-8');
        $string = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $string);
        $string = html_entity_decode($string, ENT_NOQUOTES, 'UTF-8');
        $string = preg_replace(array('`[^a-z0-9]`i', '`[-]+`'), '-', $string);
        $string = strtolower(trim($string, '-'));
        $dbcon = Yii::app()->db2;
        $data = $dbcon->createCommand('SELECT id FROM films WHERE permalink=\'' . $string . '\'')->execute();
        if ($data >= 1) {
            $name = $name . "-" . $this->plinkcounter++;
            $this->generate_permalink($name);
        }
        return $string;
    }

    /**
     * @method getpermalink() Generate Permalink from a String
     * @param type $string
     * @return type
     * @return string 
     * @author GDR<support@muvi.com>
     */
    function getPermalink($string) {
        $name = $string;
        if ($string !== mb_convert_encoding(mb_convert_encoding($string, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32'))
            $string = mb_convert_encoding($string, 'UTF-8', mb_detect_encoding($string));
        $string = htmlentities($string, ENT_NOQUOTES, 'UTF-8');
        $string = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $string);
        $string = html_entity_decode($string, ENT_NOQUOTES, 'UTF-8');
        $string = preg_replace(array('`[^a-z0-9]`i', '`[-]+`'), '-', $string);
        $string = strtolower(trim($string, '-'));
        return $string;
    }

    /**
     * @method cleanSrting() Remove all Special character from string except underscore
     * @param type $string
     * @return string 
     * @author GDR<support@muvi.com>
     */
    function cleanSrting($string) {
        $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\_]/', '', $string); // Removes special chars.
        return preg_replace('/_+/', '_', $string);
    }

    /**
     * 
     */
    function getMuvialias($contnetType) {
        if ($contnetType == 1) {
            return 'movie';
        } elseif ($contnetType == 2) {
            return 'clips';
        } elseif ($contnetType == 3) {
            return 'tv';
        } elseif ($contnetType == 4) {
            return 'livestreaming';
        } else {
            return '';
        }
    }

    /**
     * @method generateuniqNumber()
     */
    function generateUniqNumber() {
        $uniq = uniqid(rand());
        return md5($uniq . time());
    }

    /**
     * @method public uploadposter($files,$object_id,$object_type) Get the file information and upload the it to s3 with cropping
     * @param array $files File information
     * @param int $object_id Its movie_id/celebrity_id/
     * @param string $object_type Defines the type of objects films,topbanner,celebrity etc
     * @return bool true/false
     * @author GDR<support@muvi.com>
     */
    function uploadPoster($fileinfo, $object_id, $object_type, $dimension = array('thumb' => '185x256', 'standard' => '280x400'), $jcropPath = '') {
        if ($fileinfo) {
            $posterData = Poster::model()->find('object_id=:object_id AND object_type=:object_type', array('object_id' => $object_id, ':object_type' => $object_type));
            $fileName = Yii::app()->common->fileName($fileinfo['name']);
            if ($posterData) {
                $oldposter = $posterData->attributes;
                //Remove old poster images
                //Update poster data 
                $posterData->poster_file_name = $fileName;
                $posterData->is_poster = $fileName;
                $posterData->wiki_data = '';
                $posterData->last_updated_by = Yii::app()->user->id;
                $posterData->last_updated_date = gmdate('Y-m-d');
                $posterData->save();
                $uid = $oldposter['id'];
            } else {
                $poster = new Poster();
                $poster->object_id = $object_id;
                $poster->object_type = $object_type;
                $posterData->wiki_data = '';
                $poster->poster_file_name = $fileName;
                $poster->ip = $_SERVER['REMOTE_ADDR'];
                $poster->created_by = Yii::app()->user->id;
                $poster->created_date = gmdate('Y-m-d');
                $poster->save();
                $uid = $poster->id;
            }
        }
        $studio_id = Yii::app()->common->getStudioId();
        //$uid = $_REQUEST['movie_id'];
        require_once "Image.class.php";
        require_once "Config.class.php";
        require_once "Uploader.class.php";
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        require_once "amazon_sdk/sdk.class.php";
        spl_autoload_register(array('YiiBase', 'autoload'));
        defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
        $config = Config::getInstance();
        $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/posters'); //path for images uploaded
        $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);

        $config->setBucketName($bucketInfo['bucket_name']);
        $s3_config = Yii::app()->common->getS3Details($studio_id);
        $config->setS3Key($s3_config['key']);
        $config->setS3Secret($s3_config['secret']);
        $config->setAmount(250);  //maximum paralell uploads
        $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
        $config->setDimensions($dimension);   //resize to these sizes
        //usage of uploader class - this simple :)
        $uploader = new Uploader($uid);
        $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $ret = $uploader->uploadPoster($fileinfo, $unsignedBucketPath . "public/system/posters/", $jcropPath);
        $poster = array($ret);
        return $ret;
    }
	
    /**
     * @method public uploadLogo($files,$object_id,$object_type) Get the file information and upload the it to s3 with cropping
     * @param array $files File information
     * @param int $object_id Its movie_id/celebrity_id/
     * @param string $object_type Defines the type of objects films,topbanner,celebrity etc
     * @return bool true/false
     * @author GDR<support@muvi.com>
 */
    function uploadLogo($fileinfo, $dimension ,$parent_theme, $jcropPath = '',$type='logos') {
        if ($fileinfo) {
			$fileName = Yii::app()->common->fileName($fileinfo['name']);
			$studio_id = Yii::app()->common->getStudioId();
			require_once "Image.class.php";
			require_once "Config.class.php";
			require_once "Uploader.class.php";
			spl_autoload_unregister(array('YiiBase', 'autoload'));
			require_once "amazon_sdk/sdk.class.php";
			spl_autoload_register(array('YiiBase', 'autoload'));
			defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
			$config = Config::getInstance();
			$config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos'); //path for images uploaded
			$bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);

			$config->setBucketName($bucketInfo['bucket_name']);
			$s3_config = Yii::app()->common->getS3Details($studio_id);
			$config->setS3Key($s3_config['key']);
			$config->setS3Secret($s3_config['secret']);
			$config->setAmount(250);  //maximum paralell uploads
			$config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
			$config->setDimensions($dimension);   //resize to these sizes
			//usage of uploader class - this simple :)
			$uploader = new Uploader($parent_theme);
			$folderPath = Yii::app()->common->getFolderPath('', $studio_id);
			$unsignedBucketPath = $folderPath['unsignedFolderPath'];
			$ret = $uploader->uploadStudioLogo($fileinfo, $unsignedBucketPath .'public/'. $parent_theme . '/'.$type.'/', $jcropPath);
			$poster = array($ret);
			return $ret;
		}else{
			return FALSE;
		}
    }
    /**
     * @method public uploadtoImageGallery($files,$object_id,$object_type) Get the file information and upload the it to s3 with cropping
     * @param array $files File information
     * @param int $object_id Its movie_id/celebrity_id/
     * @param string $object_type Defines the type of objects films,topbanner,celebrity etc
     * @return bool true/false
     * @author GDR<support@muvi.com>
     */
    function uploadToImageGallery($fileinfo,$dimension = array('thumb' => '64x64'), $jcropPath = '') {
        if ($fileinfo) {
            $file_info= pathinfo ($fileinfo['name']);
            $fname = Yii::app()->common->fileName($file_info['filename']);
            $fileinfo['name'] = $fname."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_info['extension'];
            $crop_size_mb = 0;
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudioId();

            $imageMgt = new ImageManagement();
            $imageMgt-> image_size = $crop_size_mb;
            $imageMgt-> image_name = $fileinfo['name'];
            if(!empty($_REQUEST['image_key'])){
                $isExistImageKey = ImageManagement::model()->isExistImageKey($studio_id);
                if($isExistImageKey['success']==1){
                    $imageMgt->image_key = trim($_REQUEST['image_key']);
                }
            }
            $imageMgt-> s3_thumb_name = 'imagegallery/'.$studio_id.'/thumb/'.$fileinfo['name'];
            $imageMgt-> s3_original_name = 'imagegallery/'.$studio_id.'/original/'.$fileinfo['name'];
            $imageMgt-> studio_id = $studio_id;
            $imageMgt-> user_id = $user_id;
            $imageMgt-> creation_date = new CDbExpression("NOW()");
            $imageMgt->save();
            $uid = $imageMgt->id;
            //$uid = $_REQUEST['movie_id'];
            require_once "Image.class.php";
            require_once "Config.class.php";
            require_once "Uploader.class.php";
            spl_autoload_unregister(array('YiiBase', 'autoload'));
            require_once "amazon_sdk/sdk.class.php";
            spl_autoload_register(array('YiiBase', 'autoload'));
            defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
            $config = Config::getInstance();
            $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/imagegallery'); //path for images uploaded
            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);

            $config->setBucketName($bucketInfo['bucket_name']);
            $s3_config = Yii::app()->common->getS3Details($studio_id);
            $config->setS3Key($s3_config['key']);
            $config->setS3Secret($s3_config['secret']);
            $config->setAmount(250);  //maximum paralell uploads
            $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
            $config->setDimensions($dimension);   //resize to these sizes
            //usage of uploader class - this simple :)
            $uploader = new Uploader($uid);

            $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            $ret = $uploader->uploadToImageGallery($fileinfo, $unsignedBucketPath . "imagegallery/".$studio_id.'/',$jcropPath);
            $poster = array($ret);
            return $ret;
        }else{
            return false;
        }
    }
    
    function uploadAnyFile($fileinfo, $dimension ,$parent_theme, $jcropPath = '',$type='logos',$pgtype='',$remove_timestamp=0) {
        if ($fileinfo) {
            $file_info= pathinfo ($fileinfo['name']);
            $fname = Yii::app()->common->fileName($file_info['filename']);
            if(($pgtype=='banner') && $remove_timestamp){
                $fileinfo['name'] = $fname.'.'.$file_info['extension'];  
            }else{
                $fileinfo['name'] = $fname."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_info['extension'];            
            }                      
            $studio_id = Yii::app()->common->getStudioId();
            require_once "Image.class.php";
            require_once "Config.class.php";
            require_once "Uploader.class.php";
            spl_autoload_unregister(array('YiiBase', 'autoload'));
            require_once "amazon_sdk/sdk.class.php";
            spl_autoload_register(array('YiiBase', 'autoload'));
            defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
            $config = Config::getInstance();
            $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/'.$type); //path for images uploaded
            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);

            $config->setBucketName($bucketInfo['bucket_name']);
            $s3_config = Yii::app()->common->getS3Details($studio_id);
            $config->setS3Key($s3_config['key']);
            $config->setS3Secret($s3_config['secret']);
            $config->setAmount(250);  //maximum paralell uploads
            $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
            $config->setDimensions($dimension);   //resize to these sizes
            //usage of uploader class - this simple :)
            $uploader = new Uploader($parent_theme);
            $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            if($parent_theme != ""){
                $parent_theme = $parent_theme."/";
            }
            $target_folder = $unsignedBucketPath .'public/'. $parent_theme . $type.'/';
            $ret = $uploader->uploadImagetoAws($fileinfo, $target_folder, $jcropPath,$pgtype);
            return $ret;
        }else{
                return FALSE;
        }
    }    
    function getComingSoonPath($studio_id, $filename){
        $unsignedBucketPath = Yii::app()->common->getPosterCloudFrontPath($studio_id);
        $type = 'comingsoon';
        $std = new Studio();                
        $studio = $std->findByPk($studio_id);        
        $target_folder = $unsignedBucketPath .'/'. $studio->theme . '/'.$type.'/'.$filename;
        return $target_folder;
    } 

    /**
     * @method public	getmovieInfo(int $movie_id) Its returns the related movie information along with movie streams details and Content type details
     * @return array Movie detials as an arrray
     * @author GDR<support@muvi.com>
     */
    function getMovieInfo($movie_id, $studio_id = '') {
        if ($movie_id && $studio_id) {
            $dbcon = Yii::app()->db;
            $sql = "SELECT f.name,f.permalink,f.id,f.uniq_id,f.content_type_id,ms.full_movie,ms.id as movie_stream_id,f.content_types_id FROM films f,movie_streams ms,studio_content_types c WHERE f.id=ms.movie_id  AND f.id=" . $movie_id . " AND f.studio_id=" . $studio_id . " AND ms.is_episode=0";
            $data = $dbcon->createCommand($sql)->queryAll();
            if ($data) {
                return $data;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * @method public getEpisodeToPlay() Its will get the first playable episode for the show
     * @return array Episode Details
     * @author GDR<support@muvi.com>
     */
    function getEpisodeToPlay($movie_id,$studio_id='') {
        if ($movie_id) {
			if(!$studio_id){
				$studio_id = Yii::app()->common->getStudioId();
			}
            $data = movieStreams::model()->find(array('condition' => 'movie_id =' . $movie_id . ' AND is_episode =1 AND episode_parent_id=0 AND is_converted =1 AND studio_id =' . $studio_id . ' AND full_movie !=\'\' ', 'order' => 'id ASC', 'select' => 'id,full_movie,COUNT(DISTINCT(series_number)) AS total_series,GROUP_CONCAT(DISTINCT series_number order by series_number asc SEPARATOR \',\' ) AS series_number'));
			if($data){
				return @$data;
			}else{
				return FALSE;
			}
        } else {
            return FALSE;
        }
    }

    function getPosterPath($object_id = '', $object_type = 'films', $size = 'original') {
        $posters = Poster::model()->find('object_id=:object_id AND object_type=:object_type', array(':object_id' => $object_id, ':object_type' => $object_type));
        if (!$posters) {
            if ($object_type == 'topbanner') {
                return '';
            } else {
                //return POSTER_URL . '/no-image.png';
                return '';
            }
        } else {
            $pdata = $posters->attributes;
            if ($pdata['wiki_data'] != "") {
                $data['is_wiki_data'] = 1;
                $data['path'] = 'public/system/posters/' . $pdata['wiki_data'] . '/' . $size . '/' . $pdata['poster_file_name'];
                return $data;
            } else {
                $data['is_wiki_data'] = 0;
                $data['path'] = 'public/system/posters/' . $pdata['id'] . '/' . $size . '/' . $pdata['poster_file_name'];
                return $data;
            }
        }
    }

    function removeMovieTrailerEpisode($studio = Null, $movie_id = Null) {
        if (isset($studio) && !empty($studio) && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $studio->id;
            $s3bucket_id = $studio->s3bucket_id;

            //Getting all videos like movie, episode
            $getMovieStream = movieStreams::model()->findAll('movie_id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie_id, ':studio_id' => $studio_id));

            if ($movie_id && $getMovieStream) {
                $s3 = Yii::app()->common->connectToAwsS3($studio_id);
                $folderPath = Yii::app()->common->getFolderPath($studio->new_cdn_users);
                $signedBucketPath = $folderPath['signedFolderPath'];
                //Get bucket name
                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                $bucket = $bucketInfo['bucket_name'];

                foreach ($getMovieStream AS $key => $val) {
                    $streams = $val->attributes;
                    $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $streams['id'] . "/";

                    //Checking movie or episode
                    if ($streams['full_movie']) {

                        //Getting all video resolution file in that folder
                        $response = $s3->getListObjectsIterator(array(
                            'Bucket' => $bucket,
                            'Prefix' => $s3dir
                        ));

                        //Delete the videos from bucket
                        foreach ($response as $object) {
                            $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => $object['Key']
                            ));
                        }
                    }

                    if ($streams['is_episode'] != 1) {
                        //Remove Trailer
                        $this->removeTrailer($movie_id);

                        //Remove Poster 
                        $this->removePosters($movie_id, 'films');

                        //Remove Top Banner
                        $this->removePosters($movie_id, 'topbanner');

                        //Remove Data from films table 
                        Film::model()->deleteByPk($movie_id);
                    } else {
                        //Remove Poster
                        $this->removePosters($streams['id'], 'moviestream');
                    }

                    //Delete movie streams
                    movieStreams::model()->deleteByPk($streams['id']);
                }
            }
        }
    }

    /**
     * @method public removetrailer($movie_id) Remove trailer for the movie
     * @return bool true/false
     * @author GDR<support@muvi.com>
     */
    function removeTrailer($movie_id) {
        $trailer = movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id' => $movie_id));
        if ($trailer) {
            $tdata = $trailer->attributes;
            if ($tdata['trailer_file_name']) {
                $s3dir = 'uploads/trailers/' . $tdata['id'] . "/";
                $s3 = S3Client::factory(array(
                            'key' => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret,
                ));
                $bucket = Yii::app()->params->s3_bucketname;
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3dir . $tdata['trailer_file_name']
                ));
            }
            $trailer->delete();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @method public removeposter(int $object_id,$object_type) Remove all the poster againest a movie
     * @param int $object_id id of films,id of movie streams etc 
     * @param string $object_type films,moviestream,celebrity,topbanner etc
     * @return bool true/false
     * @author GDR<support@muvi.com>
     */
    function removePosters($object_id, $object_type) {
        $posters = Poster::model()->find('object_id=:object_id AND object_type=:object_type', array(':object_id' => $object_id, ':object_type' => $object_type));
        if (!$posters) {
            return TRUE;
        } else {
            $studio_id = Yii::app()->common->getStudioId();
            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucket_name = $bucketInfo['bucket_name'];
            $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            $pdata = $posters->attributes;
            $s3dir = $unsignedBucketPath . 'public/system/posters/' . $pdata['id'] . "/";
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $bucket = $bucket_name;
            if ($pdata['poster_file_name']) {
                $result = $s3->deleteMatchingObjects($bucket, $s3dir);
            }
            $posters->delete();
        }
    }
    function removeTvGuideEvent($stream_id)
    {
     
   $delete_event="delete from ls_schedule where movie_id=".$stream_id;
   Yii::app()->db->createCommand($delete_event)->execute();
    
    }
    function is_url_exist($url, $auth) {
        $arr = array('error' => 1);
        //$username = 'goquestmedia';$password = 'iu4jyc9ic3yy9x50';
        //$url = 'https://s3.amazonaws.com/vf-designers-2/Videofashion-Designers-s02-e01.mp4';
        //$url = 'http://d1yjifjuhwl7lc.cloudfront.net/Ramnavami_2010.mp4';
        if (@$url) {
            $username = '';
            $password = '';
            if ($auth) {
                $username = $auth['access_username'];
                $password = $auth['access_password'];
            }
            if ($username && $password) {
                ini_set('memory_limit', '1024M');
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                curl_setopt($ch, CURLOPT_USERAGENT, "Proventum Proxy/1.0 (Linux)");
                curl_setopt($ch, CURLOPT_TIMEOUT, 10); //times out after 10s 
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                $data = curl_exec($ch);
                $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                $arr['code'] = $code;
            } else {
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_NOBODY, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_exec($ch);
                $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                $arr['code'] = $code;
            }
            if ($code == 200) {
                $arr['error'] = 0;
                if (strpos($url, 'dropbox.com') !== FALSE) {
                    $extention = pathinfo($url);
                    $extention = $extention['extension'];
                    if(in_array(strtolower($extention), array('mp4','mov','mkv','flv','vob','m4v','avi','3gp','mpg','wmv'))){
                        $arr['is_video'] = 'video/';
                    }else{
                        $arr['is_video'] = 0;
                    }
                } else {
                    $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                    if ($content_type && (strstr($content_type, 'video/') || ($content_type == 'application/octet-stream') || ($content_type == 'binary/octet-stream'))) {
                        $arr['is_video'] = $content_type;
                    } else {
                        $arr['is_video'] = 0;
                    }
                }
            } elseif(in_array($code, array('125','150','226','225'))){
                $arr['error'] = 0;
                $extention = pathinfo($url);
                $extention = $extention['extension'];
                if(in_array(strtolower($extention), array('mp4','mov','mkv','flv','vob','m4v','avi','3gp','mpg','wmv'))){
                    $arr['is_video'] = 'video/';
                }else{
                    $arr['is_video'] = 0;
                }
            } else {
                $arr['error'] = 1;
            }
            curl_close($ch);
        }
        return $arr;
    }

    /**
     * @method public uploadMobileImages($files,$object_id,$object_type) Get the file information and upload the it to s3 with cropping
     * @param array $files File information
     * @param int $object_id Its movie_id/celebrity_id/
     * @param string $object_type Defines the type of objects films,topbanner,celebrity etc
     * @return bool true/false
     * @author SKM<support@muvi.com>
     */
    function uploadMobileImages($fileinfo, $studio_id, $app_type, $dimension = array(), $jcropPath = '', $type) {
        if ($fileinfo) {
            $imageData = MobileAppInfo::model()->find('studio_id=:studio_id AND app_type=:app_type', array('studio_id' => $studio_id, ':app_type' => $app_type));
            $uid = $studio_id;
        }
        //$uid = $_REQUEST['movie_id'];
        require_once "Image.class.php";
        require_once "Config.class.php";
        require_once "Uploader.class.php";
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        require_once "amazon_sdk/sdk.class.php";
        spl_autoload_register(array('YiiBase', 'autoload'));
        defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
        $config = Config::getInstance();
        $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/mobileapp'); //path for images uploaded
        $bucketInfo = Yii::app()->common->getBucketInfo("", Yii::app()->user->studio_id);
        $bucketCloudfrontUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
        $config->setBucketName($bucketInfo['bucket_name']);
        $s3_config = Yii::app()->common->getS3Details(Yii::app()->user->studio_id);
        $config->setS3Key($s3_config['key']);
        $config->setS3Secret($s3_config['secret']);
        $config->setAmount(250);  //maximum paralell uploads
        $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
        $config->setDimensions($dimension);   //resize to these sizes
        //usage of uploader class - this simple :)
        $uploader = new Uploader($uid);
        $folderPath = Yii::app()->common->getFolderPath('', Yii::app()->common->getStudioId());
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $ret = $uploader->uploadMobileImage($fileinfo, $unsignedBucketPath . "public/system/mobileapp/", $jcropPath, $type);
        $poster = array($ret);
        return $ret;
    }

   
    function sendStudioAdminEmails($studio_id, $template,$user_id, $VideoName = Null,$amount=0, $plan_name = Null,$card_id = Null, $coupon_code=NULL, $coupon_amount = NULL)
    {
        if(isset($template) && $template != ''){
            $std = new Studio();                
            $studio = $std->findByPk($studio_id);
            $usr = new SdkUser();
            $user = $usr->findByPk($user_id);
            $name = $user->display_name;
            $email = $user->email;
            $address = '';
            
            $address = $this->mb_unserialize($user->signup_location);
            $signup_location = json_decode(json_encode($address),TRUE);
            $signup_location = $signup_location['\0*\0_data']['\0CMap\0_d'];
            $address = $signup_location['geoplugin_city'].','.$signup_location['geoplugin_region'].','.$signup_location['geoplugin_countryName'];
            $agent = Yii::app()->common->getAgent();
            $studio_name = strtoupper($studio->name);
            $studio_usr = new User();
            $studio_user = $studio_usr->findByAttributes(array('studio_id'=> $studio_id));
            //5975 - check for cc emails ajit@muvi.com 
            $admin_cc = array();
            $admin_cc = Yii::app()->common->emailNotificationLinks($studio_id,'end_user_action');
            //END - check for cc emails ajit@muvi.com issue id - 5975
            $message = array();
            if($studio_name == 'ISKCON'){
                $studio_email = 'studio@muvi.com';
                $studio_name = 'Muvi';
                $message['to']=array($studio_user->email);
                           
            }else{
                
                $studio_email = $studio_user->email;
                $studio_name = $studio->name;
                $message['to'] = array($studio_email);
                             
            }
            if($studio->contact_us_email == ''){
                $studio_from_email = $studio_email;
            }else{
                $studio_from_email = $studio->contact_us_email;
            }
            $subject = '';
            switch ($template) {
                case 'admin_new_user_registration':
                    $params = array(
                       'agent'=> $agent,
                        'user_name'=> $name,
                       'user_email'=> $email,
                       'user_address'=> $address);
                            
                    $subject = 'New user registered!';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    
                    break;
                case 'admin_voucher_content':
                    $params = array(
                        'user_name'=>$name,
                        'user_email'=>$email,
                        'content_name'=>$VideoName,
                        'voucher_code'=>$coupon_code
                    );
                    $subject = 'Voucher Applied!';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    
                    break;
                case 'admin_new_paying_customer':
                    $params = array(
                        'agent'=> $agent,
                         'user_name' => $name,
                         'user_email'=> $email,
                        'user_address'=> $address);
                    $subject = 'New paying customer!';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    
                    break;
                case 'admin_user_reactivate':
                    $params = array(
                       'agent'=> $agent,
                        'user_name'=> $name,
                        'user_email'=> $email,
                       'user_address'=> $address
                    );
                    $subject = 'Subscription Reactivated!';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    
                    break;
                
                case 'admin_subscription_cancelled':
                    $query = "SELECT `user_subscriptions`.`cancel_note`, `cancel_reasons`.`reason` FROM `user_subscriptions` LEFT JOIN `cancel_reasons` ON user_subscriptions.cancel_reason_id = cancel_reasons.id WHERE user_subscriptions.user_id =".$user_id." AND user_subscriptions.status =0";
                    $usersub = Yii::app()->db->createCommand($query)->queryRow();
                    $params = array(
                        'agent'=> $agent,
                        'user_name'=> $name,
                        'user_email'=> $email,
                       'user_address'=> $address,
                        'user_reason'=> $usersub['reason'],
                        'user_custom_reason' => $usersub['cancel_note']
                    );
                    $subject = 'Subscription cancelled';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    break;
                 case 'admin_subscription_bundles_cancelled':
                    $query = "SELECT `user_subscription_bundles`.`cancel_note`, `cancel_reasons`.`reason` FROM `user_subscription_bundles` LEFT JOIN `cancel_reasons` ON user_subscription_bundles.cancel_reason_id = cancel_reasons.id WHERE user_subscription_bundles.user_id =".$user_id." AND user_subscription_bundles.status =0";
                    $usersub = Yii::app()->db->createCommand($query)->queryRow();
                    $params = array(
                        'agent'=> $agent,
                        'user_name'=> $name,
                        'user_email'=> $email,
                       'user_address'=> $address,
                        'user_reason'=> $usersub['reason'],
                        'user_custom_reason' => $usersub['cancel_note']
                    );
                    $subject = 'Subscription Bundles cancelled';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    break;
                
                case 'admin_monthly_payment':
                    $params = array(
                        'user_name' => $name,
                        'payment_amount'=> $amount,
                        'plan_name'=> $plan_name
                    );
                    $subject = 'Subscription Fee Charged Successfully!';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    
                    break;
                 case 'admin_bundle_monthly_payment':
                    $params = array(
                        'user_name' => $name,
                        'payment_amount'=> $amount,
                        'plan_name'=> $plan_name
                    );
                    $subject = 'Subscription Bundle Fee Charged Successfully!';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                
                    break;

                case 'admin_payment_ppv_content':
                    $params = array(
                        'agent' => $agent,
                       'user_name' => $name,
                        'payment_amount'=> $amount,
                        'content_name'=> $VideoName
                    );
                    $subject = 'Payment for PPV content!';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    
                    break;
                
                 case 'admin_payment_subscription_bundles_content':
                    $params = array(
                        'agent' => $agent,
                       'user_name' => $name,
                        'payment_amount'=> $amount,
                        'content_name'=> $VideoName
                    );
                    $subject = 'Payment for Subscription Bundles content!';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    
                    break;
                
                case 'admin_advance_purchase':
                    $params = array(
                        'user_email'=> $email,
                        'user_name' => $name,
                        'payment_amount'=> $amount,
                        'content_name'=> $VideoName,
                        'coupon_code'=> $coupon_code,
                        'coupon_amount'=> $coupon_amount
                    );
                    $subject = 'New Pre-Order!';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    
                    break;
                
                case 'studio_admin_payment_failed':
                    $query = "SELECT us.plan_id, sp.name, spr.price, sci.card_last_fourdigit FROM user_subscriptions us LEFT JOIN (subscription_plans sp, sdk_card_infos sci, subscription_pricing spr) ON (us.plan_id=sp.id AND sp.studio_id=us.studio_id AND sci.user_id=us.user_id AND sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.id = spr.subscription_plan_id AND spr.currency_id =us.currency_id) WHERE us.studio_id=".$studio_id." AND us.user_id=".$user_id." AND us.status =1";
                    $usersub = Yii::app()->db->createCommand($query)->queryRow();
                    $params = array(
                       'user_name'=> $name,
                        'user_email'=> $email,
                        'user_card'=> $usersub['card_last_fourdigit'],
                        'user_subscription_plan'=> $usersub['name'],
                        'user_subscription_amount'=> '$'.$usersub['price']
                    );
                    $subject = 'Payment failure';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    break;
                
                case 'admin_card_updated_and_charged':
                    $query = "SELECT us.plan_id, sp.name, sci.card_last_fourdigit FROM user_subscriptions us LEFT JOIN (subscription_plans sp, sdk_card_infos sci) ON (us.plan_id=sp.id AND sp.studio_id=us.studio_id AND sci.user_id=us.user_id AND sci.studio_id=us.studio_id AND sci.is_cancelled=0) WHERE us.studio_id=".$studio_id." AND us.user_id=".$user_id." AND us.status =1";
                    $usersub = Yii::app()->db->createCommand($query)->queryRow();
                    $params = array(
                        'user_name'=> $name,
                        'user_email'=> $email,
                        'user_card'=> $usersub['card_last_fourdigit'],
                        'user_subscription_plan'=> $usersub['name'],
                        'user_subscription_amount'=> $amount
                    );
                    $subject = 'Payment successful';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    break;
                
                case 'admin_subscription_cancelled_on_payment_failed':
                    $query = "SELECT us.plan_id, us.currency_id, sp.name, spr.price, sci.card_last_fourdigit FROM user_subscriptions us LEFT JOIN (subscription_plans sp, sdk_card_infos sci) ON (us.plan_id=sp.id AND sp.studio_id=us.studio_id AND sci.user_id=us.user_id AND sci.studio_id=us.studio_id AND sci.id=".$card_id.") LEFT JOIN (subscription_pricing spr) ON (us.plan_id=spr.subscription_plan_id AND spr.currency_id=us.currency_id) WHERE us.studio_id=".$studio_id." AND us.user_id=".$user_id;
                    $usersub = Yii::app()->db->createCommand($query)->queryRow();
                    $user_subscription_amount = Yii::app()->common->formatPrice($usersub['price'], $usersub['currency_id'], 1);
                    
                    $params = array(
                        'user_name' => $name,
                        'user_email' => $email,
                        'user_card' => $usersub['card_last_fourdigit'],
                        'user_subscription_plan' => $usersub['name'],
                        'user_subscription_amount'=> $user_subscription_amount 
                            );                   
                    $subject = 'Subscription cancelled on payment failure';
                    $message['subject'] = $subject;
                    $message['from_email'] = $studio_from_email;
                    $message['from_name'] =  $studio->name;
                    break;

                default:
                    break;
            }
          // return $this->sendMandrilEmail($template, $params, $message); 
             $subject=$message['subject'];
            $to=implode("",$message['to']);
            $from=$message['from_email'];
          //  $template=$template;
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/'.$template,array('params'=>$params),true);
           
            return $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from,$admin_cc,'','',$message['from_name']);      
        }
    }

    function sendStudioAdminContactusEmail($template_name, $template_content, $admin_message) {
        return $this->sendMandrilEmail($template_name, $template_content, $admin_message);
    }

    /**
     * @method public GetSocialUrls() Return the social login urls 
     * @return array array of urls
     * @author Gayadhar<support@muvi.com>
     */
    function getSocialUrls() {
        $session = new CHttpSession;
        $session->open();
        $socialAuth = Yii::app()->common->getSocialAuths($this->studio->id);
        if ($socialAuth) {
            if ($socialAuth['fb_app_id']) {
                require 'facebook-php-sdk/src/Facebook/autoload.php';
                $fb = new Facebook\Facebook([
                    'app_id' => $socialAuth['fb_app_id'],
                    'app_secret' => $socialAuth['fb_secret'],
                    'default_graph_version' => $socialAuth['fb_app_verson'],
                    //'persistent_data_handler' => new FacebookPersistentDataHandler(),
                    'default_access_token' => isset($_SESSION['facebook_access_token']) ? $_SESSION['facebook_access_token'] : $socialAuth['fb_app_id']
                ]);
                $helper = $fb->getRedirectLoginHelper();
                $permissions = ['email']; // Optional permissions
                $redirect_uri = Yii::app()->getBaseUrl(TRUE) . '/login/facebookAuth/';
                $loginUrl = $helper->getLoginUrl($redirect_uri, $permissions);
                foreach ($_SESSION as $k => $v) {
                    if (strpos($k, "FBRLH_") !== FALSE) {
                        if (!setcookie($k, $v)) {
                            //what??
                        } else {
                            $_COOKIE[$k] = $v;
                        }
                    }
                }
                $_SESSION['social']['fb_url'] = $loginUrl;
                $arr['fb_url'] = htmlspecialchars($loginUrl);
            }
            if ($socialAuth['gplus_client_id']) {
                require_once ('google-plus-sdk/Google/autoload.php');
                $redirect_uri = Yii::app()->getBaseUrl(TRUE) . '/login/googleplusAuth/';
                $client = new Google_Client();
                $client->setClientId($socialAuth['gplus_client_id']);
                $client->setClientSecret($socialAuth['gplus_client_secret']);
                $client->setRedirectUri($redirect_uri);
                $client->addScope("email");
                $client->addScope("profile");
                $service = new Google_Service_Oauth2($client);
                $authUrl = $client->createAuthUrl();
                $_SESSION['social']['gplus_url'] = $authUrl;
                $arr['gplus_url'] = htmlspecialchars($authUrl);
            }
            return $arr;
        } else {
            return '';
        }
    }

    function mb_unserialize($string) {
        $string = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $string);
        return unserialize($string);
    }

    function clearFacebookCookie() {
        foreach ($_SESSION as $k => $v) {
            if (strpos($k, "FBRLH_") !== FALSE) {
                $_COOKIE[$k] = '';
                setcookie($k, '', time() - 36000);
                setcookie($k, '', time() - 36000, '/');
            }
        }
        unset($_SESSION['social']);
        $_SESSION = array();
        session_destroy();
    }

    function getnew_secure_urlForEmbeded($resourceKey, $streamHostUrl = "", $isAjax = "", $browserName = "", $expireTime = 0, $studio_id = 0) {
        $this->layout = false;
        if ($studio_id == 0) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $cloudFront = Yii::app()->common->connectToAwsCloudFront($studio_id);
        if ($streamHostUrl == "") {
            $streamHostUrl = CDN_HTTP . 'd3ff6jhdmx1yhu.cloudfront.net';
        }
        if ($expireTime != 0) {
            $expires = time() + $expireTime;
        } else {
            $deviceName = '';
            if (Yii::app()->common->isMobile()) {
                $deviceName = 'mobile';
            } elseif ($browserName == "Safari") {
                $deviceName = 'safari';
            } elseif ($isAjax == 1) {
                $deviceName = 'ajax';
            } elseif ($isAjax == 2) {
                $deviceName = 'On embed first time load';
            } else {
                $deviceName = 'On video first time load';
            }
            $studioDetails = Studio::model()->getStudioBucketData($studio_id);
            $s3ExpDetails = S3BucketExpirationTime::model()->findByAttributes(array("s3bucket_id" => $studioDetails['s3bucket_id'], 'device_name' => $deviceName));
            if (isset($s3ExpDetails->expire_time) && $s3ExpDetails->expire_time != '') {
                $expires = time() + $s3ExpDetails->expire_time;
            } else if (Yii::app()->common->isMobile() || $browserName == "Safari") {
                $expires = time() + 50;
            } else if ($isAjax == 1) {
                $expires = time() + 20;
            } else {
                $expires = time() + 25;
            }
        }

        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));
        return $signedUrlCannedPolicy;
    }

    /**
     * @method public getContentSeasons() Get the multipart content seasons/Serise List
     * @author Gayadhar<support@muvi.com>
     * @return array season array
     */
    function getContentSeasons($content_id, $studio_id) {
        $seasons = array();
        if ($content_id) {
            $criteria = new CDbCriteria;
            $criteria->select = 'DISTINCT series_number as series_number,id'; // select fields which you want in output
            $criteria->condition = 'movie_id = ' . $content_id . " AND is_episode =1 AND studio_id=" . $studio_id;
            $criteria->group = 'series_number';
            $criteria->order = 'series_number ASC';
            $data = movieStreams::model()->findAll($criteria);
            if ($data) {
                foreach ($data AS $key => $val) {
                    $arr[] = $val->series_number;
                }
                $seasons = $arr;
            }
        }
        return $seasons;
    }

    /**
     * @method public handleError(type $paramName) This method is to handel exceptions or errors 
     * @author Gayadhar<support@muvi.com>
     * @return HTML Exception handeling page
     */
    public function handleError(CEvent $event) {
        if ($event instanceof CExceptionEvent) {
            //echo $event->message;
            $this->pageTitle = "MuviStudio | Page not available";
            $statusCode = $event->exception->statusCode;
            $params = '';
            if(isset($_POST) && count($_POST))
                $params .= serialize($_POST);
            if(isset($_GET) && count($_GET))
                $params .= serialize($_GET);
                
            $body = array(
                'code' => $event->exception->getCode(),
                'message' => $event->exception->getMessage(),
                'file' =>    @$event->exception->getFile(),
                'line' =>    @$event->exception->getLine(),
                'Referer' => @$_SERVER['HTTP_REFERER'],
                'Params' => @$params,
            );
            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/exception.log', "a+");
        } elseif ($event instanceof CErrorEvent) {
            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/custom_error.log', "a+");
            Yii::app()->theme = 'error';
            $this->layout = 'main';
            $this->render('//error/exception', array('statuscode' => $statusCode, 'body' => $body));
        }
        fwrite($fp, "\n\n\t Date:- " . date('dS M Y h:i A') . " \t Site URL: " . Yii::app()->getBaseUrl(TRUE) . $_SERVER["REQUEST_URI"] . ". \t Studio ID:-" . Yii::app()->common->getStudioId() . " \t  IP: ".CHttpRequest::getUserHostAddress()." \t\n Error body:-" . print_r($body, TRUE));
        fclose($fp);
        Yii::app()->theme = 'error';
        $this->layout = 'main';
        $this->render('//error/exception', array('statuscode' => $statusCode, 'body' => $body));
        $event->handled = TRUE;
    }

    public function getOwnerName($developer) {
        if ($developer) {
            $sql = "SELECT id,first_name FROM `owner` WHERE id=" . $developer;
            $user = Yii::app()->db->createCommand($sql)->queryAll();
            return ucfirst($user[0]['first_name']);
        } else
            return 'None';
    }
        function ticketDetails($ticket) {
        $std = new Studio();
        $stu = $std->findByPk($ticket[0]['studio_id']);
        $reported_by = $ticket[0]['studio_id'] == 1 ? 'Super Admin' : $stu['name'];
        $assigned_to = ['assigned_to'] == 0 ? 'None' : $this->getOwnerName($ticket[0]['assigned_to']);
        $model = new TicketForm;
        //$template = TicketController::tempaleFormat();
        $ticketnotes = $model->ticketNotes($ticket[0]['id']);
        $html = "Your support ticket is {$ticket[0]['id']} updated. <br />
         Ticket ID: {$ticket[0]['id']},Reported By: {$reported_by},Assigned To: " . $assigned_to . ",Priority: {$ticket[0]['priority']} <br /><br />";

        $html.= "Description:" . stripslashes(nl2br(htmlentities($ticket[0]['description'])));
        if (!empty($ticketnotes)) {
            $html.="<hr style='border-top:1px dotted #000;' />";
            foreach ($ticketnotes as $k=>$updates) {
                
                    $note_added_by = $updates['id_user']==1? 'Super Admin' : $std->findByPk($ticket[0]['studio_id'])->name;
                    $updated_on = date('M d, Y', strtotime($updates['updated_date']));
                    $html.=stripslashes(nl2br(htmlentities($updates['note'])));
                    $html.="<br />By: {$note_added_by} on {$updated_on}";
                    $html.="<hr style='border-top:1px dotted #000;' />";
                
            }
            
        }
        $url = "http://".DOMAIN_COOKIE; 
         $html.= "Add an update to the ticket by logging into <a target='_blank' href='" . $url ."'>Muvi</a>, or simply replying to this email <br /><br />" ;
        return $html . "</p>" ."<p><br/>Regards,<br /> Muvi</p>";
        exit;
    }
    function sendmailViaMandrill($html, $subject, $to, $attachment = '') {
        require_once 'mandrill-api-php/src/Mandrill.php';
        $args = array(
            'key' => "Xb3a0BtRxPQIJTbJwn1Vlg",
            'message' => array(
                "html" => $html,
                "text" => null,
                "from_email" => "support@muvi.com",
                "from_name" => "Muvi",
                "subject" => $subject,
                "to" => $to,
                "track_opens" => true,
                "track_clicks" => true,
                "auto_text" => true
            /* ,
              "attachments" => array(
              array(
              'content' => $attachment['file'],
              'type' => "application/pdf",
              'name' => $attachment['filename']
              )
              ) */
            )
        );
        $curl = curl_init('https://mandrillapp.com/api/1.0/messages/send.json');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($args));
        $time = "\n" . date("Y-m-d H:i:s");
        $response = curl_exec($curl);
        // Close the connection
        curl_close($curl);
        return 1;
    }

    // for sending email by amason ses sdk
   function sendmailViaAmazonsdk($html, $subject, $to, $from,$cc=array(),$bcc=array(),$reply_to=array(),$from_name) {
		require 'sendgrid-php/vendor/autoload.php';
		$sendgrid = new SendGrid(SENDGRID_APIKEY);
		$email = new SendGrid\Email();
        try{
			$reply_to = $reply_to ?$reply_to:$from; 
			$email->addTo($to)
				->addCc($cc)
				->addBcc($bcc)
				->setReplyTo($reply_to)
				->setFromName(@$from_name)
				->setFrom($from)
				->setSubject($subject)
				->setHtml($html);
			$result=$sendgrid->send($email);
			return $result;  
         } catch (Exception $e) {
             $fp = fopen(__DIR__ . '/../../protected/runtime/email.log', 'a+');
             fwrite($fp, '\n\t Exception occured on dt:-' . date('m-d-Y H:i:s') .'\t'.'Studio_id='.$this->studio->id. "\n\t " . $e->getMessage());
             fclose($fp);
             return false;
         }
    }
     function sendAttchmentMailViaAmazonsdk($to, $subject, $from, $html,$attachments,$cc='',$bcc='',$reply_to='', $studio_name){
		require 'sendgrid-php/vendor/autoload.php';
        $from_name = $studio_name;
        $sendgrid = new SendGrid(SENDGRID_APIKEY);
        $email    = new SendGrid\Email();
		//echo $to."==".$subject."---".$from."===".$html."===".$attachments;exit;
		try{
         $email->addTo($to)
             ->addCc($cc)
             ->addBcc($bcc)
             ->setReplyTo(@$reply_to)
             ->setFromName(@$from_name)
             ->setFrom($from)
             ->setSubject($subject)
             ->setHtml($html);
		   if($attachments){
			   $email->setAttachments(@$attachments);
		   } 
			$result=$sendgrid->send($email);
           return $result;  
         } catch (Exception $e) {
			 $fp = fopen(__DIR__ . '/../../protected/runtime/email.log', 'a+');
			 fwrite($fp, '\n\t Exception occured on dt:-' . date('m-d-Y H:i:s') . "\n\t " . $e->getMessage());
             fclose($fp);
             return false;
         }        
    }
    function mimeContentType($file)
	{
		if (file_exists($file) && $extension = pathinfo($file, PATHINFO_EXTENSION))
		{
			//taken from https://github.com/laravel/laravel/blob/3.0/application/config/mimes.php
			$mimes = array(
				'hqx'   => 'application/mac-binhex40',
				'cpt'   => 'application/mac-compactpro',
				'csv'   => array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream'),
				'bin'   => 'application/macbinary',
				'dms'   => 'application/octet-stream',
				'lha'   => 'application/octet-stream',
				'lzh'   => 'application/octet-stream',
				'exe'   => array('application/octet-stream', 'application/x-msdownload'),
				'class' => 'application/octet-stream',
				'psd'   => 'application/x-photoshop',
				'so'    => 'application/octet-stream',
				'sea'   => 'application/octet-stream',
				'dll'   => 'application/octet-stream',
				'oda'   => 'application/oda',
				'pdf'   => array('application/pdf', 'application/x-download'),
				'ai'    => 'application/postscript',
				'eps'   => 'application/postscript',
				'ps'    => 'application/postscript',
				'smi'   => 'application/smil',
				'smil'  => 'application/smil',
				'mif'   => 'application/vnd.mif',
				'xls'   => array('application/excel', 'application/vnd.ms-excel', 'application/msexcel'),
				'ppt'   => array('application/powerpoint', 'application/vnd.ms-powerpoint'),
				'wbxml' => 'application/wbxml',
				'wmlc'  => 'application/wmlc',
				'dcr'   => 'application/x-director',
				'dir'   => 'application/x-director',
				'dxr'   => 'application/x-director',
				'dvi'   => 'application/x-dvi',
				'gtar'  => 'application/x-gtar',
				'gz'    => 'application/x-gzip',
				'php'   => array('application/x-httpd-php', 'text/x-php'),
				'php4'  => 'application/x-httpd-php',
				'php3'  => 'application/x-httpd-php',
				'phtml' => 'application/x-httpd-php',
				'phps'  => 'application/x-httpd-php-source',
				'js'    => 'application/x-javascript',
				'swf'   => 'application/x-shockwave-flash',
				'sit'   => 'application/x-stuffit',
				'tar'   => 'application/x-tar',
				'tgz'   => array('application/x-tar', 'application/x-gzip-compressed'),
				'xhtml' => 'application/xhtml+xml',
				'xht'   => 'application/xhtml+xml',
				'zip'   => array('application/x-zip', 'application/zip', 'application/x-zip-compressed'),
				'mid'   => 'audio/midi',
				'midi'  => 'audio/midi',
				'mpga'  => 'audio/mpeg',
				'mp2'   => 'audio/mpeg',
				'mp3'   => array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
				'aif'   => 'audio/x-aiff',
				'aiff'  => 'audio/x-aiff',
				'aifc'  => 'audio/x-aiff',
				'ram'   => 'audio/x-pn-realaudio',
				'rm'    => 'audio/x-pn-realaudio',
				'rpm'   => 'audio/x-pn-realaudio-plugin',
				'ra'    => 'audio/x-realaudio',
				'rv'    => 'video/vnd.rn-realvideo',
				'wav'   => 'audio/x-wav',
				'bmp'   => 'image/bmp',
				'gif'   => 'image/gif',
				'jpeg'  => array('image/jpeg', 'image/pjpeg'),
				'jpg'   => array('image/jpeg', 'image/pjpeg'),
				'jpe'   => array('image/jpeg', 'image/pjpeg'),
				'png'   => 'image/png',
				'tiff'  => 'image/tiff',
				'tif'   => 'image/tiff',
				'css'   => 'text/css',
				'html'  => 'text/html',
				'htm'   => 'text/html',
				'shtml' => 'text/html',
				'txt'   => 'text/plain',
				'text'  => 'text/plain',
				'log'   => array('text/plain', 'text/x-log'),
				'rtx'   => 'text/richtext',
				'rtf'   => 'text/rtf',
				'xml'   => 'text/xml',
				'xsl'   => 'text/xml',
				'mpeg'  => 'video/mpeg',
				'mpg'   => 'video/mpeg',
				'mpe'   => 'video/mpeg',
				'qt'    => 'video/quicktime',
				'mov'   => 'video/quicktime',
				'avi'   => 'video/x-msvideo',
				'movie' => 'video/x-sgi-movie',
				'doc'   => 'application/msword',
				'docx'  => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'xlsx'  => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
				'word'  => array('application/msword', 'application/octet-stream'),
				'xl'    => 'application/excel',
				'eml'   => 'message/rfc822',
				'json'  => array('application/json', 'text/json'),
			);

			if (array_key_exists($extension, $mimes))
			{
				return (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
			}
		}
        }
        
    function log($message)
	{
		echo '<p class="error">' . strip_tags($message) . '</p>';
	}
    
    function uploadAttachments($filename, $fileinfo, $id_ticket) {
        require_once "Image.class.php";
        require_once "Config.class.php";
        require_once "Uploader.class.php";
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        require_once "amazon_sdk/sdk.class.php";
        spl_autoload_register(array('YiiBase', 'autoload'));
        defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
        $config = Config::getInstance();
        $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . 'images/attachment'); //path for images uploaded
        $bucket = Yii::app()->params->s3_ticket_attachments;
        $config->setMimeTypes(array('jpg', 'gif', 'png', 'jpeg','txt','xls','xlsx','pdf','doc','docx','bmp')); //allowed extensions
        //usage of uploader class - this simple :)
        $uploader = new Uploader($id_ticket);
        $ret = $uploader->uploadAttachments($filename, $fileinfo, "uploads/", $id_ticket, $bucket);
        
        return $ret;
    }

    function uploadEmailNotiImages($filename, $fileinfo, $studio_id, $upload_file_dir) {
        if ($fileinfo) {
            $uid = $studio_id;
        }
        require_once "Image.class.php";
        require_once "Config.class.php";
        require_once "Uploader.class.php";
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        require_once "amazon_sdk/sdk.class.php";
        spl_autoload_register(array('YiiBase', 'autoload'));
        defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
        $config = Config::getInstance();
        $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/email_noti_images'); //path for images uploaded
        $bucketInfoForPoster = Yii::app()->common->getBucketInfoForPoster(Yii::app()->user->studio_id);
        $config->setBucketName($bucketInfoForPoster['bucket_name']);
        $s3_config = Yii::app()->common->getS3Details(Yii::app()->user->studio_id);
        $config->setS3Key($s3_config['key']);
        $config->setS3Secret($s3_config['secret']);
        $config->setAmount(250);  //maximum paralell uploads
        $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
        //usage of uploader class - this simple :)
        $uploader = new Uploader($uid);
        $ret = $uploader->uploadEmailNotiImage($filename, $fileinfo, "email_noti_images/", $fileinfo["tmp_name"], 'email');
        $poster = array($ret);
        return $ret;
    }

    function getVideoDuration($filename) {
        $ffmpeg = FFMPEG_PATH;
        if (HOST_IP != '127.0.0.1' && HOST_IP != '52.0.64.95') {
            $ffmpeg = 'sudo '.FFMPEG_PATH;
        }
        $command = $ffmpeg . " -i " . $filename . " 2>&1 | grep Duration | awk '{print $2}' | tr -d ,";
        $output = shell_exec($command);
        $output12 = explode(".", $output);
        $duration = $output12[0] ? $output12[0] : null;
        return json_encode($duration);
    }

    public function ticketNotes($id_ticket, $limit = '') {
        $tickettingdb=$this->getAnotherDbconnection();
        $sql = "SELECT * FROM `ticket_notes` WHERE id_ticket=" . $id_ticket . " ORDER BY updated_date DESC";
        $post = $tickettingdb->createCommand($sql)->queryAll();
        $tickettingdb->active=flase;
        return $post;
    }

    /*     * * This function is for check permission for modules ** */
    /*     * * Now checked permission for Manage content and View Reports ** */
    /*     * * Author manas@muvi.com ** */

    function checkPermission() {
        $role = isset(Yii::app()->user->role_id) ? Yii::app()->user->role_id : 1;
        if ($role == 5 && !in_array(Yii::app()->controller->getId(), array('partner', 'payment'))) {//referrer/reseller/master
            $this->redirect(Yii::app()->getbaseUrl(true) . '/partner');
            exit;
        }
        if (in_array($role, array(3,4))) {//3 = Member , 4 = Partner            
            if (!Yii::app()->request->isAjaxRequest) {
                $data = User::model()->findAll(
                        array(
                            'select' => 'permission_id',
                            'condition' => 'id=:userid',
                            'params' => array(':userid' => Yii::app()->user->id)
                ));
                if ($role == 4){
                    $data[0]['permission_id']=($data[0]['permission_id'] == '')?5:$data[0]['permission_id'].',5';
                }
                if ($data[0]['permission_id'] == '') {
                    return true;
                } else {
                    $db = Yii::app()->db;
                    $sql = "select module,action from permission where id IN(" . $data[0]['permission_id'] . ")";
                    $query_permission = $db->createCommand($sql)->queryAll();
                    foreach ($query_permission as $key => $value) {
                        $permission[$value['module']] = $value['action'];
                    }
                    $modules = array_keys($permission);
                    if (in_array('all', $modules)) {
                        return true;
                    } else {
                        $accessaction = array('dashboard');
                        $accesscontroller = array();
                        $blockaction = array('blog', 'extensions', 'advanced', 'account', 'partners', 'videoad', 'emailNotifications','email_triggers', 'seo', 'advancedSetting');
                        if (in_array('content', $modules)) {
                            if($role == 4){
                                array_push($accesscontroller, 'admin', 'management');
                                array_push($blockaction, 'manageimage');
                            }else{
                                array_push($accesscontroller, 'admin', 'adminceleb', 'management', 'mrss', 'lstream');
                            }
                        }
                        if (in_array('report', $modules)) {
                            array_push($accesscontroller, 'report');
                        }
                        $currentcontroller = Yii::app()->controller->getId();
                        $currentaction = Yii::app()->controller->action->id;
                        if (in_array($currentaction, $accessaction)) {
                            return true;
                        } elseif (in_array($currentcontroller, $accesscontroller)) {
                            if (in_array($currentaction, $blockaction)) {
                                Yii::app()->user->setFlash('error', 'You don\'t have permission to access this page.');
                                if ($role == 4){$url = $this->createUrl('/partners/video');}else{$url = $this->createUrl('/admin/dashboard');}
                                $this->redirect($url);
                            } else {
                                return true;
                            }
                        } else {
                            Yii::app()->user->setFlash('error', 'You don\'t have permission to access this page.');
                            if ($role == 4){$url = $this->createUrl('/partners/video');}else{$url = $this->createUrl('/admin/dashboard');}
                            $this->redirect($url);
                        }
                    }
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    
    function timeFormat($seconds)
    {
        if($seconds > 0 && $seconds < 1){
            $seconds = $seconds+1;
        }
        // extract hours
        $hours = floor($seconds / (60 * 60));
        // extract minutes
        $divisor_for_minutes = $seconds % (60 * 60);
        $minutes = floor($divisor_for_minutes / 60);
        // extract the remaining seconds
        $divisor_for_seconds = $divisor_for_minutes % 60;
        $seconds = ceil($divisor_for_seconds);
        // return the final array
        $obj = array(
            "h" => (int) ($hours >= 10)?$hours:'0'.$hours,
            "m" => (int) ($minutes >= 10)?$minutes:'0'.$minutes,
            "s" => (int) ($seconds >= 10)?$seconds:'0'.$seconds,
         );
        $time = implode(':', $obj);
        return $time;
    }
     function calcdivpercen($start_time,$end_time,$start){
        $start = $start.":00:00";
        $total      = 240;
	if($start_time < $start){
		$start_time = $start;
	}
	$after_4hours = date("Y-m-d H:i:s", strtotime('+4 hours', strtotime($start)));
	if($end_time > $after_4hours){
		$end_time = $after_4hours;
	}
	$diff       = self::timediff($start_time,$end_time);
	$pro_start  = $start_time;
	$diff_time  = self::timediff($start_time,$start);
	$start_from = 0;
	if($diff_time != 0){
		$start_from = $diff_time;
	}
	$start_hour =  $total - $start_from;
	if($diff > $total){
		$start_from = $total - $diff_time;
		$to_hour 	= $total;
		$diff       = $to_hour - $start_from;

	}else{
	   $to_hour = $start_from + $diff;
	}
	$div_percent = $diff / $total;
	$div_percent = number_format($div_percent * 100,2);
        if($diff_time > 0){
            $before_percent = $diff_time / $total;
            $before_percent = number_format($before_percent * 100,2);
        }else{
            $before_percent = 0;
        }
        $percent["div_percent"] = $div_percent;
        $percent["before_percent"] = $before_percent;
        return $percent;
    }
    function timediff($start_time,$end_time){
	$timestamp1 = strtotime($start_time);
	$timestamp2 = strtotime($end_time);
	return $min = abs($timestamp2 - $timestamp1)/(60);
    }
    public function tvguideDefaultImage(){
        $studio = $this->studio;      
        $preview_image = $studio->tv_guide_default_preview_img;      
        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio->id);
        $base_url=Yii::app()->getBaseUrl(true);
        if ($preview_image == '') {
            $deafult_img = $base_url."/images/preview_img_with_content.jpg";
        } else {
            $deafult_img = $base_cloud_url . "tv_guide/".$preview_image;          
        }
        
        return $deafult_img;
    }
    public function getFullLangTranslation($lang){
        $static_msg    = TranslateKeyword::model()->getMessages('0');
        $js_msg        = TranslateKeyword::model()->getMessages('1');
        $server_msg    = TranslateKeyword::model()->getMessages('2');
        $ln   = @$lang["static_messages"];
        if(!empty($ln)){
            $static_mssg    = is_array($lang['static_messages'])? $lang['static_messages'] : array();
            $js_mssg        = is_array($lang['js_messages'])? $lang['js_messages'] : array();
            $server_mssg    = is_array($lang['server_messages'])? $lang['server_messages'] : array();
        }else{
            $static_mssg    = is_array($lang)? $lang : array();
            $js_mssg        = array();
            $server_mssg    = array();
        }
        $language['static_messages'] = self::compareDbwithFile($static_msg,$static_mssg);
        $language['js_messages']     = self::compareDbwithFile($js_msg,$js_mssg);
        $language['server_messages'] = self::compareDbwithFile($server_msg,$server_mssg);
        $language['all']             = array_merge($language['static_messages'],$language['js_messages'],$language['server_messages']);
        return $language;
        
    }
    public function getAllTranslation($lang,$studio_id){
        $ln   = @$lang["static_messages"];
        if(!empty($ln)){
            $static_mssg    = is_array($lang['static_messages'])? $lang['static_messages'] : array();
            $js_mssg        = is_array($lang['js_messages'])? $lang['js_messages'] : array();
            $server_mssg    = is_array($lang['server_messages'])? $lang['server_messages'] : array();
        }else{
            $static_mssg    = is_array($lang)? $lang : array();
            $js_mssg        = array();
            $server_mssg    = array();
        }
        $lang_from_files = array_merge($static_mssg,$js_mssg,$server_mssg);
        $lang_from_db    = TranslateKeyword::model()->getAllMessages($studio_id,'1');
        $language       = self::compareDbwithFileForApp($lang_from_db,$lang_from_files);
        return $language;
    }
    /*
    * @purpose Compare database table data with file data
    * @param type (Array,Array)
    * @return  Array of translated keys and value
    * @author Biswajit Parida<biswajit@muvi.com>
    */

    public function compareDbwithFile($db_msg,$file_msg){
        $message = array();
        if(count($db_msg) != count($file_msg)){
            $diff    = array_diff_key($db_msg,$file_msg);
            $message = array_merge($file_msg,$diff);
            asort($message);
            return $message;
        }else{
            return $file_msg;
        }
    }
     /*
    * @purpose Compare database table data with file data for mobile app
    * @param type (Array,Array)
    * @return  Array of translated keys and value
    * @author Biswajit Parida<biswajit@muvi.com>
    */
    
    public function compareDbwithFileForApp($db_msg,$file_msg){
        $message = array();
        $diff    = array_diff_key($db_msg,$file_msg);
        $message = array_merge($file_msg,$diff);
        $message = array_intersect_key($message,$db_msg);
        asort($message);
        return $message;
    }
    
    public function getAnotherDbconnection()
    {
        if(HOST_IP == '127.0.0.1' || HOST_IP=='52.0.64.95'){
            return Yii::app()->db;
        }else{        
            $dbname='studio';
            $dsn='mysql:host=studiomuvi.cagqnk2yhltv.us-east-1.rds.amazonaws.com;dbname='. $dbname;
            $username='studio';
            $password='#%!!studio%&!%##';
            $dbcon=new CDbConnection($dsn,$username,$password);
            $dbcon->active=true;
            return $dbcon;
        }
    }
    public function getAnotherWpstudioDbconnection()
    {
		$dbname='wpstudio';
		$username='studio';
		$password='#%!!studio%&!%##';
		if(HOST_IP=='52.0.64.95'){
			$dsn='mysql:host=localhost;dbname='. $dbname;
		}else{
			$dsn='mysql:host=studiomuvi.cagqnk2yhltv.us-east-1.rds.amazonaws.com;dbname='. $dbname;
		}
		$dbcon=new CDbConnection($dsn,$username,$password);
		$dbcon->active=true;
        return $dbcon;
    }
/**
 * @method public generatecsrfToken(type $paramName) It will generate unique CSRF Token 
 * @author Gayadhar<support@muvi.com>
 * @return String It will return a string of Token
 */
	function generateCsrfToken(){
		if(!@$_SESSION['csrfToken']){
			$randomtoken = base64_encode( openssl_random_pseudo_bytes(32));
			$_SESSION['csrfToken'] = $randomtoken;
			return true;
		}else{
			return false;
		}
	}

 function getDbConnection() //BY Suraja<suraja@muvi.com>
    {
        $dbname=Yii::app()->session['dbname'];
        $db_username=Yii::app()->session['db_username'];
        $db_password=Yii::app()->session['db_password'];
        $host=Yii::app()->session['host'];
        $port=Yii::app()->session['port'];               
        //get another db connection using the db confidential        
        
        if($dbname!='' && $db_username!='' && $db_password!='' && $host!='' && $port!='' )
        {
          
        $dsn="mysql:host=".$host.";dbname=".$dbname;
        $dbcon=new CDbConnection($dsn,$db_username,$db_password);
        $dbcon->active=true;             
        }
        else
        {
          
        $dbcon=Yii::app()->db;
        }
        return $dbcon;
    }
    function trackingCode() {
        $data = Yii::app()->general->getTrackingCode();
        foreach ($data as $trackcode) {
            $track_location = $trackcode['track_location'];
            $track_tag = $trackcode['track_tag'];
            $tracking_code = $trackcode['track_code'];
            if (($track_location == 'Below') ) {
                if ($track_tag == 'open head') {
                    $position = 'open_head_below';
                    $trackingcode[$position][] = $tracking_code;
                } else {
                    $position = 'open_body_below';
                    $trackingcode[$position][] = $tracking_code;
                }
            } else {
                 if ($track_tag == 'close head') {
                    $position = 'close_head_above';
                    $trackingcode[$position][] = $tracking_code;
                } else {
                    $position = 'close_body_above';
                    $trackingcode[$position][] = $tracking_code;
                }
            }
        }
        return $trackingcode;
    }
    public function getContentSeasonDetails($movie_id, $studio_id){
        $data = array();
        if($movie_id){
            $season_details = Yii::app()->db->createCommand()
                ->select('series_number,count(series_number) as total_episodes')
                ->from('movie_streams')
                ->where('studio_id='.$studio_id.' AND movie_id='.$movie_id.' AND episode_parent_id=0 AND is_episode = 1')
                ->group('series_number')    
                ->queryAll();
            $data['total_seasons'] = count($season_details);
            $total_episodes = 0;  
            if(!empty($season_details)){
                foreach($season_details as $seasons){
                    $total_episodes += $seasons['total_episodes'];
                    if($seasons['series_number'] < 1 || $seasons['series_number'] ==""){
                        $seasons['series_number'] = 0;
                    }
                    $episode[$seasons['series_number']] = $seasons['total_episodes'];
                }
            }
            $data['total_episodes_per_season'] = empty($episode)? array() : $episode;
            $data['total_episodes'] = $total_episodes;
        }
        return $data;
    }
}
