<?php
require 's3bucket/aws-autoloader.php';
use Aws\Common\Enum\DateFormat;
use Aws\S3\Model\MultipartUpload\UploadId;
use Aws\S3\S3Client;
class S3multipart {	

/**
 * 
 * @return type
 */	
	function isAllowed(){
		//wow, what a validator :P
		//WARNING: this is just a demonstration, convert it to your own need
		return ($_REQUEST['otherInfo']['user'] == 'user' && $_REQUEST['otherInfo']['pass'] == 'pass');
	}	
/**
 * @method public ListalllParts() List and return all the parts of a mulatipart upload
 * @author GDR<support@muvi.com>
 * @return array Array of parts
 */
	function ListallParts($keyName='',$uploadId='',$dest_bucket='',$region='',$s3url=''){
		if($dest_bucket == ''){
			$bucketInfo = Yii::app()->common->getBucketInfo(Yii::app()->user->s3bucket_id);
			$dest_bucket = $bucketInfo['bucket_name'];
			//$dest_bucket = Yii::app()->params->video_bucketname;
		}
                $s3 = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id,$region);
		if($region){
			$s3->setRegion($region);
		}
		$parts = $s3->getIterator('ListParts', array(
			'Bucket' =>$dest_bucket,
			'Key' => $keyName,
			'UploadId' => $uploadId,
		));
		$i=0;$arr=array();
		foreach ($parts as $key=>$part) {
			// Do something with the part data
			$arr[$i]['PartNumber'] =  $part['PartNumber'];
			$arr[$i]['ETag'] =  $part['ETag'];
			$arr[$i]['Size'] =  $part['Size'];
			$arr[$i++]['LastModified'] =  $part['LastModified'];
			//printf("%d: %s (%d bytes)\n", $part['PartNumber'], $part['ETag'], $part['Size']);
		}
		return $arr;
		//echo "<pre>";print_r($arr);exit;
	}
/**
 * @method Public createMultipartupload() Create the Authentication key and Upload id
 * @author GDR<support@muvi.com>
 * @return json A string of json data
 */	
	function createmultipartupload($dest_bucket='',$region='',$s3url =''){
            $new_cdn_user=Yii::app()->user->new_cdn_users;
		if($dest_bucket == ''){
			$bucketInfo = Yii::app()->common->getBucketInfo(Yii::app()->user->s3bucket_id);
			$dest_bucket = $bucketInfo['bucket_name'];
			//$dest_bucket = Yii::app()->params->video_bucketname;
		}
                $client = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id,$region);
		if($region){
			$client->setRegion($region);
		}
		if (!$this->isAllowed()) {
			header(' ', true, 403);
			die('You are not authorized');
		}
                $studio_id = Yii::app()->user->studio_id;
                $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
                $signedFolderPath = $folderPath['signedFolderPath'];
                $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                if(isset($_REQUEST['otherInfo']['uploadType']) && $_REQUEST['otherInfo']['uploadType']=='trailer'){
                        $key = $signedFolderPath.'uploads/trailers/'.$_REQUEST['otherInfo']['trailer_id'].'/'.$_REQUEST['fileInfo']['name'];
                }
                else if(isset($_REQUEST['otherInfo']['uploadType']) && $_REQUEST['otherInfo']['uploadType']=='physical'){
                        $key = $signedFolderPath.'uploads/physical/'.$_REQUEST['otherInfo']['trailer_id'].'/'.$_REQUEST['fileInfo']['name'];
                }
                else if(isset($_REQUEST['otherInfo']['uploadType']) && $_REQUEST['otherInfo']['uploadType']=='videobanner'){
                        $key = $unsignedFolderPathForVideo.'uploads/videobanner/'.$_REQUEST['otherInfo']['movie_stream_id'].'/'.$_REQUEST['fileInfo']['name'];
                } 
                else if(isset($_REQUEST['otherInfo']['uploadType']) && $_REQUEST['otherInfo']['uploadType']=='videogallery'){
                   /** **********check whether the file exist already or not************* */
                    $sql = "select * from video_management where studio_id=" . $studio_id . " and video_name='" . $_REQUEST['fileInfo']['name'] . "' and flag_deleted=0";
         
                    $res = Yii::app()->db->createCommand($sql)->queryAll();
                    $file_name_upload =$_REQUEST['fileInfo']['name'];
                    $file_name_list=explode('.',$file_name_upload);
                   
                    $check_video_availability = count($res);
                    // echo $check_video_availability;
                    if ($check_video_availability > 0) {

                   $file_name = $file_name_list[0]."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_name_list[1];
                    }
                    else {
                    /** **********check whether the file exist already or not************* */
                    $file_name=$file_name_upload;
                    }
                     $key = $unsignedFolderPathForVideo.'videogallery/'.$file_name;  
                    if($new_cdn_user==0)//existing user
                        {
                            $key = $unsignedFolderPathForVideo.'videogallery/'.$studio_id.'/'.$file_name;
                        }

                } 
                else if(isset($_REQUEST['otherInfo']['uploadType']) && $_REQUEST['otherInfo']['uploadType']=='audiogallery'){
                        /*
                        * modified :   Arvind 
                        * email    :   aravind@muvi.com
                        * reason   :   Audio Gallery :  
                        * functionality : audiogallery
                        * date     :   24-1-2017
                        */
                        $sql = "select * from audio_gallery where studio_id=" . $studio_id . " and audio_name='" . $_REQUEST['fileInfo']['name'] . "' and is_deleted=0";
                        $res = Yii::app()->db->createCommand($sql)->queryAll();
                        $file_name_upload =$_REQUEST['fileInfo']['name'];
                        $file_name_list=explode('.',$file_name_upload);
                        $check_audio_availability = count($res);
                        // echo $check_audio_availability;
                        if ($check_audio_availability > 0) {
                        $file_name = $file_name_list[0]."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_name_list[1];
                        }
						else{
                        //check whether the audio file exist or not
                        $file_name=$file_name_upload;
                        }
                        $key = $unsignedFolderPathForVideo.'audiogallery/'.$file_name;  
                        if($new_cdn_user==0){//existing user
                            $key = $unsignedFolderPathForVideo.'audiogallery/'.$studio_id.'/'.$file_name;
                        }
                }
				else if(isset($_REQUEST['otherInfo']['uploadType']) && $_REQUEST['otherInfo']['uploadType']=='filegallery'){
                   /** **********check whether the file exist already or not************* */
                    $sql = "select * from file_management where studio_id=" . $studio_id . " and file_name='" . $_REQUEST['fileInfo']['name'] . "' and flag_deleted=0";
         
                    $res = Yii::app()->db->createCommand($sql)->queryAll();
                    $file_name_upload =$_REQUEST['fileInfo']['name'];
                    $file_name_list=explode('.',$file_name_upload);
                   
                    $check_video_availability = count($res);
                    // echo $check_video_availability;
                    if ($check_video_availability > 0) {

                   $file_name = $file_name_list[0]."_".strtotime(date("d-m-Y H:i:s")).'.'.$file_name_list[1];
                    }
                else{
                    /** **********check whether the file exist already or not************* */
                    $file_name=$file_name_upload;
                    }
                     $key = $unsignedFolderPathForVideo.'filegallery/'.$file_name;
                }
                else{
                        $key = $signedFolderPath.'uploads/movie_stream/full_movie/'.$_REQUEST['otherInfo']['movie_stream_id'].'/'.$_REQUEST['fileInfo']['name'];
                }
		/* @var $multipartUploadModel UploadId */
		$model = $client->createMultipartUpload(array(
			'Bucket' =>$dest_bucket,
			'Key' => $key,
			'ContentType' => $_REQUEST['fileInfo']['type'],
			'Metadata' => $_REQUEST['fileInfo'],
			'ACL'     => 'public-read'
		));
		return array('uploadId' => $model->get('UploadId'),'key' => $model->get('Key'),);
	}
/**
 * @method Public signupLoadpart() Sign each part and returns the part details
 * @author GDR<support@muvi.com>
 * @return json A string of json data
 */	
	function signuploadpart($dest_bucket='',$region='',$s3url=''){
		if($dest_bucket == ''){
			$bucketInfo = Yii::app()->common->getBucketInfo(Yii::app()->user->s3bucket_id);
			$dest_bucket = $bucketInfo['bucket_name'];
			//$dest_bucket = Yii::app()->params->video_bucketname;
		}
		
                $client = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id,$region);
		if($region){
			$client->setRegion($region);
		}
		$command = $client->getCommand('UploadPart',
			array(
			'Bucket' =>$dest_bucket,
			'Key' => str_replace('\n', '', trim($_REQUEST['sendBackData']['key'])),
			'UploadId' => str_replace('\n','', trim($_REQUEST['sendBackData']['uploadId'])),
			'PartNumber' => trim($_REQUEST['partNumber']),
			'ContentLength' => $_REQUEST['contentLength'],
			'Body' => ''
		));
		$request = $command->prepare();
		$data = $client->dispatch('command.before_send', array('command' => $command));
		$request->removeHeader('User-Agent');
		$request->setHeader('x-amz-date', date(DateFormat::RFC2822));
		// This dispatch commands wasted a lot of my times :'(
		$client->dispatch('request.before_send', array('request' => $request));
		return array(
			'url' => $request->getUrl(),
			'authHeader' => (string) $request->getHeader('Authorization'),
			'dateHeader' => (string) $request->getHeader('x-amz-date'),
		);
	}
/**
 * @method Public completeMultipartupload() Sign each part and returns the part details
 * @author GDR<support@muvi.com>
 * @return json A string of json data
 */	
	function completemultipartupload($dest_bucket='',$region='',$s3url=''){
		if($dest_bucket == ''){
			//$dest_bucket = Yii::app()->params->video_bucketname;
			$bucketInfo = Yii::app()->common->getBucketInfo(Yii::app()->user->s3bucket_id);
			$dest_bucket = $bucketInfo['bucket_name'];
		}
		
                $client = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id,$region);
		if($region){
			$client->setRegion($region);
		}
		$parts = $this->ListallParts($_REQUEST['sendBackData']['key'], $_REQUEST['sendBackData']['uploadId'],$dest_bucket,$region,$s3url);
		$model = $client->completeMultipartUpload(array(
			'Bucket' =>$dest_bucket,
			'Key' => $_REQUEST['sendBackData']['key'],
			'UploadId' => $_REQUEST['sendBackData']['uploadId'],
			//'Parts' => $partsModel['Parts'],
			'Parts' => $parts,
		));
		return array('success' => true);			
	}
/**
 * @method Public abortMultipartupload() Sign each part and returns the part details
 * @author GDR<support@muvi.com>
 * @return json A string of json data
 */	
	function abortmultipartupload($dest_bucket='',$region='',$s3url=''){
		if($dest_bucket == ''){
			$bucketInfo = Yii::app()->common->getBucketInfo(Yii::app()->user->s3bucket_id);
			$dest_bucket = $bucketInfo['bucket_name'];
			//$dest_bucket = Yii::app()->params->video_bucketname;
		}
		
                $client = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id,$region);
		if($region){
			$client->setRegion($region);
		}
		$model = $client->abortMultipartUpload(array(
			'Bucket' =>$dest_bucket,
			'Key' => $_REQUEST['sendBackData']['key'],
			'UploadId' => $_REQUEST['sendBackData']['uploadId']
		));
		return array('success' => true);
	}
}

