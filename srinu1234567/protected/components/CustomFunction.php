<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class CustomFunction extends AppComponent{
    public function canLogin(){
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        if($user_id){
            $current_login_id = Yii::app()->session['login_history_id'];
            if(!$current_login_id){
                $login_history_cookie_name = "login_history_id_".$studio_id."_".$user_id;
                Yii::app()->session['login_history_id'] = $current_login_id = $_COOKIE[$login_history_cookie_name];
            }
        }
        $sconfig = self::hideSimultaneousLoginStatus($studio_id);
        $return = 1;

        $sdk_user = SdkUser::model()->findByAttributes(array('studio_id' => $studio_id, 'id'=>$user_id));        
        
        if($sconfig == 1 && $user_id > 0 && isset($sdk_user) && count($sdk_user) > 0){                                                   
            $sql = "SELECT id FROM `login_history` WHERE `studio_id`=".$studio_id." AND user_id=".$user_id." AND logout_at ='0000-00-00 00:00:00'";
            $qry = Yii::app()->db->createCommand($sql)->queryAll();
            $log_id = array();
            foreach($qry as $qr){
                $log_id[] =  $qr['id'];
            }
            if(!in_array($current_login_id,$log_id))
                $return = 0;
            else
                $return = 1;
        }
        return $return;
    }
    
    public function hasOtherLogins(){
        $user_id = Yii::app()->user->id;
        $current_login_id = Yii::app()->session['login_history_id'];
        $studio_id = Yii::app()->common->getStudiosId();
        $sconfig = self::hideSimultaneousLoginStatus($studio_id);
        $return = false;

        $sdk_user = SdkUser::model()->findByAttributes(array('studio_id' => $studio_id, 'id'=>$user_id));                
        /*if($sconfig == 1 && $user_id > 0 && isset($sdk_user) && count($sdk_user) > 0)
        {            
            $qry = LoginHistory::model()->findAllByAttributes(
                array('studio_id' => $studio_id, 'user_id'=>$user_id, 'logout_at' => '0000-00-00 00:00:00'),
                array(
                    'condition'=>'id != :current_login_id', 
                    'params'=>array(':current_login_id'=> $current_login_id)
                )
            );
            if(isset($qry) && count($qry) > 0){                
                foreach($qry as $login){
                    $login->logout_at = new CDbExpression("NOW()");
                    $login->save();
                }                
                $return = true;
            }
            else
                $return = false;
        }*/
        return $return;
    }
    
    public function hideSimultaneousLoginStatus($studio_id){
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'hide_simultaneous_login');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == 1){
            $return = 1;
        }
        return $return;
    }
    public function limitLogin($studio_id){
        $return = 1;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_login');
        if(isset($sconfig) && count($sconfig) > 0){
            $return = $sconfig->config_value;
        }
        return $return;
    }
    public function restrictDevices($studio_id,$config_key=''){
        
        $return = 0;
        if($config_key!=''){
            $sconfig = StudioConfig::model()->getConfig($studio_id,$config_key);
            if(isset($sconfig) && count($sconfig) > 0){
                $return = $sconfig->config_value;
            }
        }
        return $return;
    }
    public function checkAppSelected($studio_id){
        $return = 0;
        $app=Yii::app()->general->apps_count($studio_id);
		if($app){
          $return=1;  
        }
        return $return;      
    }
    public function getFlashMessages(){
        $messages = '';                 
        if (Yii::app()->user->hasFlash('success')) { 
            $messages.= '<div class="alert alert-success" role="alert"><a data-dismiss="alert" class="close" href="#" style="color: #000">×</a>';
            $messages.= Yii::app()->user->getFlash('success');
            $messages.= '</div>';
        }
        if (Yii::app()->user->hasFlash('error')) {
            $messages.= '<div class="alert alert-danger" role="alert"><a data-dismiss="alert" class="close" href="#" style="color: #000">×</a>';
            $messages.= Yii::app()->user->getFlash('error');
            $messages.= '</div>';                    
        }
        if (Yii::app()->user->hasFlash('notice')) {
            $messages.= '<div class="alert alert-notice" role="alert"><a data-dismiss="alert" class="close" href="#" style="color: #000">×</a>';
            $messages.= Yii::app()->user->getFlash('notice');
            $messages.= '</div>'; 
        } 
        return $messages;
    }
    
    public function getCookieMessage() {
        $message = '';
        if (showMessageForCookie == 1) {
            $message.= '<script>
                $(document).ready(function() {
                    $("#cookieModal").modal("show");
                });
            </script>';
            $message.= '<div id="cookieModal" class="modal fade login-popu">
                <div class="modal-dialog">
                    <div class="modal-content" style="position: relative;">  
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" style="line-height: 1;">&nbsp;</h4>
                        </div>                    
                        <div class="modal-body">                            
                            <div class="row-fluid">
                                <div class="col-md-12">';
            $message.= showcookieMessage;
            $message.= '        </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>          
                    </div>		
                </div>
            </div>';
        }
        return $message;
    }

    function getExtracontent($studio_id, $content_id = 0){
        if($content_id == 0){
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'content_extra_content');
        if(isset($sconfig) && count($sconfig) > 0){
            return $sconfig->config_value;
        }else
            return '';
        }
        else{
            return '';
        }
    }
    
    function processUploadImage($filedata, $croparea, $theme_folder = '', $cropDimension = '', $type = 'logos', $gallery_img_name = '', $gallery_img_path = '', $gallery_croparea = array(),$object_id=false,$pgtype='') {
        $studio_id = Yii::app()->common->getStudiosId();
        $controller = Yii::app()->controller;
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/' . $type . '/' . $theme_folder;
        if($theme_folder == ""){
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/' . $type;
        }
        if($object_id){
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/posters/' . $object_id;
        }
        
        if (isset($filedata) && !($filedata['error'])) {
            $file_info = pathinfo($filedata['name']);
            $extension = array('PNG', 'png', 'JPG', 'jpeg', 'JPEG', 'jpg', 'gif', 'GIF');
            if (!in_array($file_info['extension'], $extension)) {
                Yii::app()->user->setFlash('error', 'Please upload valid files formats(png,jpg,jpeg,gif).');
                $url = $controller->createUrl('/template');
                $controller->redirect($url);
                exit;
            }

            if ($croparea['w'] == '' && $croparea['h'] == '') {
                Yii::app()->user->setFlash('error', 'Please crop image to upload.');
                $url = $controller->createUrl('/template');
                $controller->redirect($url);
                exit;
            }
            $cdimension = array('thumb' => "64x64");
            $ret1 = $controller->uploadToImageGallery($filedata, $cdimension);
            $path = Yii::app()->common->jcropImage($filedata, $dir, $croparea);
            if($object_id){
                $ret = $controller->uploadPoster($filedata, $object_id, $type, $cropDimension, $path);
            }else{
                $ret = $controller->uploadAnyFile($filedata, $cropDimension, $theme_folder, $path, $type,@$pgtype);
            }
            Yii::app()->common->rrmdir($dir);
            return $ret;
        } else if ($filedata['name'] == '' && $gallery_img_name != '') {
            $file_info = pathinfo($gallery_img_name);
            $gallery_img_name = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
            $dimension['x1'] = $gallery_croparea['x13'];
            $dimension['y1'] = $gallery_croparea['y13'];
            $dimension['x2'] = $gallery_croparea['x23'];
            $dimension['y2'] = $gallery_croparea['y23'];
            $dimension['w'] = $gallery_croparea['w3'];
            $dimension['h'] = $gallery_croparea['h3'];

            $path = Yii::app()->common->jcroplibraryImage($gallery_img_name, $gallery_img_path, $dir, $dimension);
            $fileinfo['name'] = $gallery_img_name;
            $fileinfo['error'] = 0;
            if($object_id){
                $ret = $controller->uploadPoster($fileinfo, $object_id, $type, $cropDimension, $path);
            }else{
                $ret = $controller->uploadAnyFile($fileinfo, $cropDimension, $theme_folder, $path, $type,@$pgtype,1);
            }
            Yii::app()->common->rrmdir($dir);
            return $ret;
        } else {
            return false;
        }
    }
    public function showContentPricing($payment_type, $adv_payment, $price, $item = array()){
        $return_text = '';
        $controller = Yii::app()->controller;
        if ($payment_type > 0 || $adv_payment > 0) {
            if ($item['content_types_id'] == 3) {
                $show_subscribed = $show_unsubscribed = $season_subscribed = $season_unsubscribed = $episode_subscribed = $episode_unsubscribed = 0;

                $ppv_buy = Yii::app()->common->itemsBuyInMultiContentPPV();
                if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) {
                    $show_subscribed = $price['show_subscribed'];
                    $show_unsubscribed = $price['show_unsubscribed'];
                }

                if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) {
                    $season_subscribed = $price['season_subscribed'];
                    $season_unsubscribed = $price['season_unsubscribed'];
                }

                if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) {
                    $episode_subscribed = $price['episode_subscribed'];
                    $episode_unsubscribed = $price['episode_unsubscribed'];
                }

                if (isset(Yii::app()->user->id) && (Yii::app()->user->id > 0)) {
                    $is_subscribed = Yii::app()->common->isSubscribed(Yii::app()->user->id);
                    if ($is_subscribed) {
                        if (abs($show_subscribed) > 0.00001 || abs($season_subscribed) > 0.00001 || abs($episode_subscribed) > 0.00001) {
                            $return_text .= '<div class="well ppv_price">';
                            $return_text .= '<div><u>'.$controller->Language['members'].'</u></div>';
                            if (abs($show_subscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['entire_show'].':'. Yii::app()->common->formatPrice($show_subscribed, $price['currency_id']).'</div>';}
                            if (abs($season_subscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['per_season'].':'. Yii::app()->common->formatPrice($season_subscribed, $price['currency_id']).'</div>';}
                            if (abs($episode_subscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['per_episode'].':'. Yii::app()->common->formatPrice($episode_subscribed, $price['currency_id']).'</div>';}
                            $return_text .= '</div>';
                        }
                    } else {
                        if (abs($show_unsubscribed) > 0.00001 || abs($season_unsubscribed) > 0.00001 || abs($episode_unsubscribed) > 0.00001) {
                            $return_text .= '<div class="well ppv_price">';
                            $return_text .= '<div><u>'.$controller->Language['non_members'].'</u></div>';
                            if (abs($show_unsubscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['entire_show'].':'. Yii::app()->common->formatPrice($show_unsubscribed, $price['currency_id']).'</div>';}
                            if (abs($season_unsubscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['per_season'].':'. Yii::app()->common->formatPrice($season_unsubscribed, $price['currency_id']).'</div>';}
                            if (abs($episode_unsubscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['per_episode'].':'. Yii::app()->common->formatPrice($episode_unsubscribed, $price['currency_id']).'</div>';}
                            $return_text .= '</div>';
                        }
                    }
                } else { 
                    if (abs($show_subscribed) > 0.00001 || abs($season_subscribed) > 0.00001 || abs($episode_subscribed) > 0.00001 || abs($show_unsubscribed) > 0.00001 || abs($season_unsubscribed) > 0.00001 || abs($episode_unsubscribed) > 0.00001) {
                        $return_text .= '<div class="well ppv_price">';
                            if (abs($show_subscribed) > 0.00001 || abs($season_subscribed) > 0.00001 || abs($episode_subscribed) > 0.00001) {
                                $return_text .= '<div class="pull-left col-sm-6">';
                                    $return_text .= '<div><u>'.$controller->Language['members'].'</u></div>';
                                    if (abs($show_subscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['entire_show'].':'. Yii::app()->common->formatPrice($show_subscribed, $price['currency_id']).'</div>';}
                                    if (abs($season_subscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['per_season'].':'. Yii::app()->common->formatPrice($season_subscribed, $price['currency_id']).'</div>';}
                                    if (abs($episode_subscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['per_episode'].':'. Yii::app()->common->formatPrice($episode_subscribed, $price['currency_id']).'</div>';}
                                $return_text .= '</div>';
                            }

                            if (abs($show_unsubscribed) > 0.00001 || abs($season_unsubscribed) > 0.00001 || abs($episode_unsubscribed) > 0.00001) {
                                $return_text .= '<div class="pull-left col-sm-6">';
                                    $return_text .= '<div><u>'.$controller->Language['non_members'].'</u></div>';

                                    if (abs($show_unsubscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['entire_show'].':'. Yii::app()->common->formatPrice($show_unsubscribed, $price['currency_id']).'</div>';}
                                    if (abs($season_unsubscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['per_season'].':'. Yii::app()->common->formatPrice($season_unsubscribed, $price['currency_id']).'</div>';}
                                    if (abs($episode_unsubscribed) > 0.00001) { $return_text .= '<div>'.$controller->Language['per_episode'].':'. Yii::app()->common->formatPrice($episode_unsubscribed, $price['currency_id']).'</div>';}
                                $return_text .= '</div>';
                            }
                            $return_text .= '<div class="clearfix"></div>';
                        $return_text .= '</div>';
                    }
                }
            } else {
                $price_for_subscribed = $price['price_for_subscribed'];
                $price_for_unsubscribed = $price['price_for_unsubscribed'];

                if (isset(Yii::app()->user->id) && (Yii::app()->user->id > 0)) {
                    $is_subscribed = Yii::app()->common->isSubscribed(Yii::app()->user->id);
                    if ($is_subscribed) {
                        if (abs($price_for_subscribed) > 0.00001) {
                            $return_text .= '<div class="well ppv_price">
                                <div>'.$controller->Language['members'].':'. Yii::app()->common->formatPrice($price_for_subscribed, $price['currency_id']).'</div>
                            </div>';
                        }
                    } else {
                        if (abs($price_for_unsubscribed) > 0.00001) {
                            $return_text .= '<div class="well ppv_price">
                                <div>'.$controller->Language['non_members'].':'. Yii::app()->common->formatPrice($price_for_unsubscribed, $price['currency_id']).'</div>
                            </div>';
                        }
                    }
                } else { 
                    if (abs($price_for_subscribed) > 0.00001 || abs($price_for_unsubscribed) > 0.00001) {
                        $return_text .= '<div class="well ppv_price">';
                            if (abs($price_for_subscribed) > 0.00001) {
                                $return_text .= '<div>'.$controller->Language['members'].':'. Yii::app()->common->formatPrice($price_for_subscribed, $price['currency_id']).'</div>';
                            }
                            if (abs($price_for_unsubscribed) > 0.00001) {
                                $return_text .= '<div>'.$controller->Language['non_members'].':'. Yii::app()->common->formatPrice($price_for_unsubscribed, $price['currency_id']).'</div>';
                            }
                        $return_text .= '</div>';
                    }
                }
            }
        }
        return $return_text;
    }
    public function episodeSortOrder(){
        $studio_id = Yii::app()->common->getStudiosId();
        $return = 'desc';
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'episode_sort_order');
        if(isset($sconfig) && count($sconfig) > 0){
            $return = $sconfig->config_value;
        }
        return $return;
    }  
    
    public function episodesortordermultipart()
    {
        $studio_id = Yii::app()->common->getStudiosId();
        $return = 'asc';
        $multipart_cntorder=StudioConfig::model()->getConfig($studio_id, 'multipart_content_setting');
        if($multipart_cntorder){
        $content_order=$multipart_cntorder->config_value;
        $return=($content_order==0)?'desc':'asc';
        }
        return $return;
    }
    
    /*Added By RKS*/
    /*This function will save all values custom fields in registration or activate/reactivate page*/
    public function saveCustomFieldValues($studio_id, $user_id){
        foreach($_REQUEST['data'] as $key => $value){ 
            $parts = explode('custom_', $key);
            if(@$parts[1] != ''){
                $field = CustomField::model()->findFieldByName($studio_id, $parts[1]);                
                $old_val = CustomFieldValue::model()->findAllByAttributes(array('studio_id' => $studio_id, 'field_id' => $field->id, 'user_id' => $user_id));
                if(isset($old_val) && count($old_val)){
                    foreach($old_val as $old)
                        $old->delete();   
                }
                if(is_array($value)){
                    $c = 0;
                    foreach($value as $val){
                        $field_value = new CustomFieldValue;
                        $field_value->studio_id = $studio_id;
                        $field_value->field_id = $field->id;
                        $field_value->user_id = $user_id;
                        $field_value->value = $val;
                        $field_value->id_seq = $c;
                        $field_value->save(); 
                        $c++;
                    }
                }else{
                    $field_value = new CustomFieldValue;
                    $field_value->studio_id = $studio_id;
                    $field_value->field_id = $field->id;
                    $field_value->user_id = $user_id;
                    $field_value->value = $value;
                    $field_value->save();
                }
            }
        } 

        return true;
    } 
    public function getContentofCurrentLang($studio_id, $language_id) {
        $content = array();
        $sql = 'SELECT id,name,language,censor_rating,genre,story,language_id,parent_id FROM films WHERE studio_id=' . $studio_id . ' AND(language_id = ' . $language_id . ' OR parent_id=0 ) ORDER BY parent_id ASC';
        $films = Yii::app()->db->createCommand($sql)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
        foreach ($films as $film) {
            $id = $film->id;
            if ($film->parent_id > 0) {
                $id = $film->parent_id;
            }
            $content['film'][$id] = $film;
        }
        $esql = 'SELECT id,episode_title,episode_story,episode_language_id,episode_parent_id FROM movie_streams WHERE studio_id=' . $studio_id . ' AND is_episode=1 AND (episode_language_id = ' . $language_id . ' OR episode_parent_id=0 ) ORDER BY episode_parent_id ASC';
        $episodes = Yii::app()->db->createCommand($esql)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
        foreach ($episodes as $episode) {
            $id = $episode->id;
            if ($episode->episode_parent_id > 0) {
                $id = $episode->episode_parent_id;
            }
            $content['episode'][$id] = $episode;
        } 
       return $content;
    }
    public function getTranslatedContent($content_id,$is_episode=0, $language_id=20,$studio_id=false){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $content = array();
		if(!$content_id){
			return $content;
		}
        if($language_id !=20){
            if($is_episode == 1){
				$customfield = 'custom1,custom2,custom3,custom4,custom5,custom6';
                $stream_id = $content_id;
                $sql = "SELECT id,`movie_id`,`episode_title`,`episode_story`,episode_parent_id,{$customfield} FROM movie_streams WHERE episode_parent_id =".$content_id." AND studio_id=".$studio_id." AND episode_language_id=".$language_id;
                $episode = Yii::app()->db->createCommand($sql)->setFetchMode(PDO::FETCH_OBJ)->queryRow();
                if(!empty($episode)){
                    $movie_id = $episode->movie_id;
                }else{
                    $episode = movieStreams::model()->findByPk($stream_id,array('select'=>'id,movie_id,episode_title,episode_story,episode_parent_id'));
                    $movie_id = $episode->movie_id;
                }
                $id = @$episode->id;
                if ($episode->episode_parent_id > 0) {
                    $id = $episode->episode_parent_id;
                }
				if(!$movie_id){
					return $content;
				}
                $content['episode'][$id] = $episode;
                $fsql = "SELECT id,name,language,censor_rating,genre,story,parent_id FROM films WHERE parent_id =".$movie_id." AND studio_id=".$studio_id." AND language_id=".$language_id;
                $film = Yii::app()->db->createCommand($fsql)->setFetchMode(PDO::FETCH_OBJ)->queryRow();
                if(empty($film)){
                    $film = Film::model()->findByPk($movie_id,array('select'=>'id,name,language,censor_rating,genre,story,parent_id'));
                }
                $id = $film->id;
                if ($film->parent_id > 0) {
                    $id = $film->parent_id;
                }
                $content['film'][$id] = $film;
            }else{
				$customfield = 'custom1,custom2,custom3,custom4,custom5,custom6,custom7,custom8,custom9,custom10';
                $movie_id = $content_id;
                $fsql = "SELECT id,name,language,censor_rating,genre,story,parent_id,{$customfield} FROM films WHERE parent_id =".$movie_id." AND studio_id=".$studio_id." AND language_id=".$language_id;
                $film = Yii::app()->db->createCommand($fsql)->setFetchMode(PDO::FETCH_OBJ)->queryRow();
                if(empty($film)){
                    $film = Film::model()->findByPk($movie_id,array('select'=>'id,name,language,censor_rating,genre,story,parent_id'));
                }
                $id = $film->id;
                if ($film->parent_id > 0) {
                    $id = $film->parent_id;
                }
                $content['film'][$id] = $film;
            }
        }
        return $content;
    }
    public function hideSignup($studio_id){
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'hide_signup');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == '1'){
            $return = 1;
        }
        return $return;
    }   
	public function getCatImgSize($studio_id=false,$type='category_poster_size'){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $poster_size = CAT_POSTER_SIZE;
        $poster = StudioConfig::model()->getconfigvalueForStudio($studio_id,$type);
        if(!empty($poster)){
            $poster_config = $poster['config_value'];
            if($poster_config!=""){
                $poster_size = $poster_config;
            }
        }
        $poster_size = explode('x',$poster_size);
        return $poster_size;
    }
    public function getCatImgOption($studio_id=false,$type='category_poster_option'){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $poster = false;
        $posters = StudioConfig::model()->getconfigvalueForStudio($studio_id,$type);
        if(!empty($posters) && $posters['config_value']==1){
            $poster = true;
        }
        return $poster;
    }
    public function getLanguage_id($lang_code='en'){
        $lang_details = Language::model()->findByAttributes(array('code'=>$lang_code));
        if(!empty($lang_details)){
            $language_id = $lang_details->id;
        }else{
            $language_id = 20;
        }
        return $language_id;
    }
    public function updateCustomFieldValue($user_id,$studio_id){
        foreach($_REQUEST as $key => $value){ 
            $parts = explode('custom_', $key);
            if(@$parts[1] != ''){
                $field = CustomField::model()->findFieldByName($studio_id, $parts[1]);
                $cval = new CustomFieldValue();
                $criteria = array('field_id' => $field->id, 'studio_id' => $studio_id, 'user_id' => $user_id);                        
                $cvals = $cval->findAllByAttributes($criteria); 
                foreach($cvals as $val){
                    $val->delete();
                }        
                if(is_array($value)){                                                                                   
                    $c = 0;
                    foreach($value as $val){
                        $field_value = new CustomFieldValue;
                        $field_value->studio_id = $studio_id;
                        $field_value->field_id = $field->id;
                        $field_value->user_id = $user_id;
                        $field_value->value = $val;
                        $field_value->id_seq = $c;
                        $field_value->save(); 
                        $c++;
                    }
                }else{
                    $field_value = new CustomFieldValue;
                    $field_value->studio_id = $studio_id;
                    $field_value->field_id = $field->id;
                    $field_value->user_id = $user_id;
                    $field_value->value = $value;
                    $field_value->save();
                }
            }
        }
    }
    function getCustomFieldsDetail($studio_id, $user_id,$form_type){
        $sql = "SELECT c.field_name,cv.value FROM `custom_field` c JOIN custom_field_value cv ON c.id = cv.field_id WHERE c.studio_id=".$studio_id." AND cv.user_id=".$user_id." AND c.status =1";
        $res = Yii::app()->db->createCommand($sql)->queryAll();
        $field_value=array();
        foreach($res as $ress){
            $field_value[$ress['field_name']][] = $ress['value'];
        }
        return $field_value;
    }
    function getactiveLanguageList($studio_id,$original=false){
        $language = array();
        $con = Yii::app()->db;
        $sql = "SELECT a.*, s.id AS studio_id, s.default_language FROM 
        (SELECT l.id AS languageid, l.name, l.code, sl.translated_name,sl.status,sl.frontend_show_status FROM languages l LEFT JOIN studio_languages sl 
        ON (l.id = sl.language_id AND sl.studio_id={$studio_id}) WHERE  l.code='en' OR (sl.studio_id={$studio_id})) AS a, studios s WHERE s.id={$studio_id} 
        ORDER BY FIND_IN_SET(a.code,s.default_language) DESC, FIND_IN_SET(a.code,'en') DESC, a.status DESC, a.name ASC";
        
        $studio_languages = $con->createCommand($sql)->queryAll();
        if(!empty($studio_languages)){
            foreach($studio_languages as $sl){
                if(($sl['frontend_show_status'] != "0") && ($sl['status'] == 1 || $sl['code'] == 'en')){
                    $name = $sl['name'];
                    if(!$original){
                        if($sl['translated_name']!=""){
                            $name = $sl['translated_name'];
                        }
                    }
                    $language[$sl['code']] = $name;
                }
            }
        }
        return $language;
    } 
    function getCountryList(){
        $country = array();
        $lists = Yii::app()->db->createCommand()
                ->select('code,country')
                ->from('countries')
                ->queryAll();
        if(!empty($lists)){
            foreach($lists as $list){
                $country[$list['code']] = $list['country'];
            }
        }
        return $country;
    }
    public function hideFeaturedSectionsLandingPage(){
        $studio_id = Yii::app()->common->getStudiosId();
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'hide_contents_landingpage');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == '1'){
            $return = 1;
        }
        return $return;
    } 
    
    public function sendNoJSONData(){
        $studio_id = Yii::app()->common->getStudiosId();
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'send_nojson_data');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == '1'){
            $return = 1;
        }
        return $return;
    }  
    
    public function LimitedContentHomePageData(){
        $studio_id = Yii::app()->common->getStudiosId();
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'limited_homepage_content_data');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == '1'){
            $return = 1;
        }
        return $return;
    }  
    
    public function LoadHomepageContentsByAjax(){
        $studio_id = Yii::app()->common->getStudiosId();
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'load_homepage_contents_by_ajax');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == '1'){
            $return = 1;
        }
        return $return;
    }  
    
    public function hasVideoBanner($theme){
        $studio_id = Yii::app()->common->getStudiosId();
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'has_video_banner');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == '1'){
            return 1;
        }else
            return $theme->has_video_banner;
    } 
    
    public function videoBannerImagePathFormat($studio_id = 0){
        if($studio_id > 0)
            $dir = $_SERVER['DOCUMENT_ROOT'] . "/images/videobanners/" . $studio_id . "/";
        else
            $dir = $_SERVER['DOCUMENT_ROOT'] . "/images/videobanners";
        return $dir;
    }
    
    public function videoBannerS3ImagePathFormat($studio_id){
        $file_path = Yii::app()->common->getPosterCloudFrontPath($studio_id) . '/uploads/videobanner-image/' . $studio_id . '/';       
        return $file_path;
    }
    
    public function videoBannerImageServerPathFormat(){
        $file_path = 'uploads/videobanner-image/';
        return $file_path;
    }    
    
    public function videoBannerServerPathFormat(){
        $file_path = 'uploads/videobanner/';
        return $file_path;
    }  
    public function getTranslatedCastName($celebid,$celeb_name,$language_id=20,$studio_id=false){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $castname = $celeb_name;
        if($language_id!=20){
            $sql = "SELECT name FROM celebrities WHERE parent_id =".$celebid." AND studio_id=".$studio_id." AND language_id=".$language_id;
            $cast = Yii::app()->db->createCommand($sql)->queryRow();
            if(!empty($cast)){
              $castname =  $cast['name'];
            }
        }
        return $castname;
    }
    
    public function isBlackoutDay(){
        if(strtotime("now") < strtotime("29 November 2016"))
            return true;
        else
            return false;
    }
    
    public function getProductDetails($product_id, $fields) {
        $studio_id = Yii::app()->common->getStudiosId();
        $list = array();
        if(count($fields) > 0){
            if(count($fields) == 1)
                $flds = $fields[0];
            else
                $flds = explode(',', $fields);
            $sql = "SELECT ".$flds." FROM pg_product WHERE studio_id=" . $studio_id . " AND id = ".$product_id.' LIMIT 1';
            $command = Yii::app()->db->createCommand($sql);
            $list = $command->queryAll();
        }
        return $list;
    }
    
    public function SectionProducts($section_id, $studio_id, $default_currency_id = false) {
        if (!$default_currency_id) {
            $default_currency_id = Yii::app()->controller->studio->default_currency_id;
        }
        $sql = "SELECT FC.movie_id, P.* FROM featured_content FC, pg_product P WHERE FC.studio_id = " . $studio_id . " AND FC.section_id=" . $section_id . " AND FC.movie_id = P.id AND FC.is_episode = '2' ORDER BY id_seq ASC";
        $con = Yii::app()->db;
        $movieids = '';
        $standaloneproduct = $con->createCommand($sql)->queryAll();
        if ($standaloneproduct) {
            foreach ($standaloneproduct AS $key => $val) {
                $standaloneproduct[$key]['poster'] = PGProduct::getpgImage($val['id'], 'standard');
                $tempprice = Yii::app()->common->getPGPrices($val['id'], $default_currency_id);
                $standaloneproduct[$key]['price'] = Yii::app()->common->formatPrice($standaloneproduct[$key]['sale_price'], $default_currency_id);
                if (!empty($tempprice)) {
                    $standaloneproduct[$key]['price'] = Yii::app()->common->formatPrice($tempprice['price'], $default_currency_id);
                    $standaloneproduct[$key]['sale_price'] = $tempprice['price'];
                    $standaloneproduct[$key]['currency_id'] = $tempprice['currency_id'];
                }
            }
        }
        return $standaloneproduct;
    }
    public function AppSectionProducts($section_id, $studio_id, $default_currency_id = false){
        if(!$default_currency_id){
            $default_currency_id = Yii::app()->controller->studio->default_currency_id;
        }
        $sql = "SELECT FC.movie_id,FC.is_episode, P.* FROM app_featured_content FC, pg_product P WHERE FC.studio_id = " . $studio_id . " AND FC.section_id=" . $section_id . " AND FC.movie_id = P.id AND FC.is_episode = '2' ORDER BY id_seq ASC";        
        $con = Yii::app()->db;
        $movieids = '';
        $standaloneproduct = $con->createCommand($sql)->queryAll();
        if ($standaloneproduct) {
            foreach ($standaloneproduct AS $key => $val) {
                if (Yii::app()->common->isGeoBlockPGContent($val['id'])) {
                    $cont_name = $val['name'];
                    $story = $val['description'];
                    $permalink = $val['permalink'];
                    $is_episode = $val['is_episode'];
                    $short_story = substr(Yii::app()->common->htmlchars_encode_to_html($story), 0, 200);
                    $poster = PGProduct::model()->getPgImageForApp($val['id'],'standard',$studio_id);
                    $final_content[$key]['price'] = Yii::app()->common->formatPrice($val['sale_price'],$val['currency_id']);
                    $tempprice = Yii::app()->common->getPGPrices($val['id'], $default_currency_id);
                    if(!empty($tempprice)){
                        $final_content[$key]['sale_price'] = Yii::app()->common->formatPrice($tempprice['price'], $val['currency_id']);
                        $final_content[$key]['currency_id'] = $tempprice['currency_id'];
                    }
                    $formatted_price = Yii::app()->common->formatPrice($final_content[$key]['sale_price'], $final_content[$key]['currency_id']);                            
                }
                $final_content[$key] = array(
                    'movie_id' => $val['movie_id'],
                    'title' => $cont_name,
                    'permalink' => $permalink,
                    'poster_url' => $poster,
                    'data_type' => 4,
                    'uniq_id' => @$val['uniqid'],
                    'short_story' => $short_story,
                    'price' => @$formatted_price,
                    'status' => @$val['status'],
                    'is_episode' => $is_episode
                ); 
            }
        }
        return $final_content;
    }
    public function HasHomepagePSOnly(){
        $studio_id = Yii::app()->common->getStudiosId();
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'has_home_physical_fs_only');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == '1'){
            $return = 1;
        }
        return $return;
    }  
    public function HomePageLayout(){
        $studio_id = Yii::app()->common->getStudiosId();
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'homepage_layout');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == 1){
            $return = 1;
        }
        return $return;
    }  
    
    //Does homepage supports featured sections and list contents
    public function HomePageLayoutType(){
        $has_physical = Yii::app()->general->getStoreLink();
        $return = 0;
        $theme = Yii::app()->common->getTemplateDetails(Yii::app()->controller->studio->parent_theme);        
        if($has_physical == 1 && $theme->content_type == 1){
            $return = 1;
        }
        return $return;
    }   
    public function SubscriptionPlans(){
        $studio_id = Yii::app()->common->getStudiosId();
        $plans = array();
        $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
        if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
            $plans = $plan_payment_gateway['plans'];
        }
        return $plans;
    }
    
    public function getMainMenuStructure($format = 'array', $language_id = false, $studio_id = false){
        if($format == 'array'){
            $controller = Yii::app()->controller;
            if(!$studio_id){
                $studio_id = Yii::app()->common->getStudiosId();
                $site_url = $controller->siteurl;
            }else{
                $site_url = "";
            }
            if(!$language_id){
                $language_id = $controller->language_id;
            }
            $current = explode('/', Yii::app()->request->url);
            $current = @$current[1];            
            
            $elems = array();
            
            $mainmenu = Menu::model()->find('studio_id=:studio_id AND position=:position', array(':studio_id' => $studio_id, ':position' => 'top'));            
            $menu_id = @$mainmenu->id;                                  
                      
            $sql = "SELECT id, parent_id, permalink, link_type, title,language_parent_id,language_id FROM menu_items WHERE studio_id={$studio_id} AND (menu_id=".$menu_id." OR menu_id != '') AND (language_id={$language_id} OR language_parent_id=0 AND is_hidden=0 AND id NOT IN (SELECT language_parent_id FROM menu_items WHERE studio_id={$studio_id}  AND is_hidden=0 AND (menu_id=".$menu_id." OR menu_id != '') AND language_id={$language_id})) ORDER BY parent_id ASC, id_seq ASC";
            $topmenuitems = Yii::app()->db->createCommand($sql)->queryAll();            
            foreach($topmenuitems as $row){            
                $row['children'] = array();
                $allmenus = array();
                $allmenus[] = $row['permalink'];
                if($row['language_parent_id'] != 0 && $language_id !=20){
                    $row['id'] = $row['language_parent_id'];
                }
                $parent_menu_item_id = $row['id'];
                $vn = "row" . $row['id'];
                $sql = "SELECT permalink FROM menu_items WHERE studio_id={$studio_id} AND menu_id=".$menu_id." AND parent_id=".$parent_menu_item_id." AND (language_id={$language_id} OR language_parent_id=0 AND id NOT IN (SELECT language_parent_id FROM menu_items WHERE studio_id={$studio_id} AND menu_id=".$menu_id." AND parent_id=".$parent_menu_item_id." AND language_id={$language_id})) ORDER BY id_seq ASC";
                $topmenus = Yii::app()->db->createCommand($sql)->queryAll();
                if(count($topmenus) > 0){
                    //Code for active class
                    foreach($topmenus as $topmenu){
                        $allmenus[] = $topmenu['permalink'];
                    }  
                }

                $active = (in_array($current, $allmenus))?'active':'';
                $permalink = $site_url != "" ? $site_url.'/'.$row['permalink'] : $row['permalink'];
                if($row['link_type'] == 2)
                    $permalink = $row['permalink'];
                $formatted_row = array(
                    'title' => $row['title'],
                    'permalink' => $permalink,
                    'id' => $row['id'],
                    'parent_id' => $row['parent_id'],
                    'class' => $active,
                    'short_permalink' => $row['permalink'],
                    'link_type' => $row['link_type'],
                );

                ${$vn} = $formatted_row;
                if(!is_null($row['parent_id'])) {
                    $vp = "parent" . $row['parent_id'];
                    if(isset($data[$row['parent_id']])) {
                        ${$vp} = $data[$row['parent_id']];
                    }
                    else {
                        ${$vp} = array('id' => $row['parent_id'], 'parent_id' => null, 'children' => array());
                        $data[$row['parent_id']] = &${$vp};
                    }
                    ${$vp}['children'][] = &${$vn};
                    $data[$row['parent_id']] = ${$vp};
                }
                $data[$row['id']] = &${$vn};
            }
            $result = array_filter($data, function($elem) { return is_null($elem['parent_id']); });
            $result = $result[0]['children'];                        
        }else{
            $result = '';
        } 
        return $result;
    }   
	public function hasPlaylistenabled($studio_id = 0){
		if($studio_id < 1){
			$studio_id = Yii::app()->common->getStudiosId();
		}
		$add_to_playlist = 0;
		$playlist_active = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'user_playlist');
		if(count($playlist_active) > 0){
			$add_to_playlist = $playlist_active['config_value'];
		}
		return $add_to_playlist;
	}
    
    public function getUserMenuStructure($format = 'array', $language_id = false, $studio_id = false, $user_id = false,$translate = array()) {
        if($format == 'array'){
            $current = explode('/', Yii::app()->request->url);
            $current = @$current[2];

            $controller = Yii::app()->controller;
            if($controller->studio->need_login){
                $default_currency_id = NULL;
                $favourite    = 0;
                $playlist  = $this->hasPlaylistenabled();
                if(!$studio_id){
                $studio_id = Yii::app()->common->getStudiosId();
                    $site_url  = $controller->siteurl."/";
                    $favourite = $controller->add_to_favourite;
                    $watch_history = $controller->watch_history;
                    $theme = $controller->studio->parent_theme;
                    $manage_device = $controller->manage_device;
                }else{
                    $studio = Studio::model()->findByPk($studio_id,array('select'=>'default_currency_id,domain,parent_theme'));
                    $default_currency_id = $studio->default_currency_id;
                    $site_url = "";
                    $theme    = $studio->parent_theme;
                    $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_favourite_list');
                    if(!empty($fav_status)){
                        $favourite = $fav_status['config_value'];
                }
                }
                if(!$language_id){
                    $language_id = $controller->language_id;            
                }
                if(!$user_id){
                $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
                }
                $hide_signup = Yii::app()->custom->hideSignup($studio_id);            
                if ($user_id > 0) {
                    $is_subscribed = Yii::app()->common->isSubscribed($user_id);
                    //$plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id,$default_currency_id,'',$language_id);
                    $monetization_menu = Yii::app()->general->monetizationMenuSetting($studio_id);
                    $user = Yii::app()->common->getSDKUserInfo($user_id);
                    $profile_image = $controller->getProfilePicture($user->id, 'profilepicture', 'thumb');
                    $display_name = isset(Yii::app()->user->display_name) ? Yii::app()->user->display_name : $user->display_name;
                    $cards = SdkCardInfos::model()->findAllByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id), array('order' => 'is_cancelled ASC'));
                    $link_profile[] = array(
                        'title' => isset($translate['profile']) ? $translate['profile'] : $controller->Language['profile'],
                        'permalink' => $site_url.'user/profile',
                        'id' => 0,
                        'parent_id' => 1,
                        'class' => 'profile',
                        'short_permalink' => 'profile',
                        'children' => array()
                    );
                    if($favourite) {
                        $link_profile[] = array(
                            'title' => isset($translate['my_favourite']) ? $translate['my_favourite'] : $controller->Language['my_favourite'],
                            'permalink' => $site_url.'user/favourites',
                            'id' => 0,
                            'parent_id' => 0,
                            'class' => 'favourites',  
                            'short_permalink' => 'favourites',
                            'children' => array()
                        );
                    }
                    if($watch_history != 0) {
                        $link_profile[] = array(
                            'title' => isset($translate['watch_history']) ? $translate['watch_history'] : $controller->Language['watch_history'],
                            'permalink' => $site_url.'user/watchHistory',
                            'id' => 0,
                            'parent_id' => 0,
                            'class' => 'watchhistory',  
                            'short_permalink' => 'watchhistory',
                            'children' => array()
                        );
                    }
                    if($playlist != 0 ){
                        $link_profile[] = array(
                            'title' =>isset($translate['my_playlist']) ? $translate['my_playlist'] : $controller->Language['my_playlist'],
                            'permalink' => $site_url.'user/myplaylist',
                            'id' => 0,
                            'parent_id' => 0,
                            'class' => 'myplaylist',
                            'short_permalink' => 'myplaylist',
                            'children' => array()
                        );                                
                    }
                    if ($controller->manage_device != '0') {
                        $link_profile[] = array(
                            'title' => isset($translate['manage_device']) ? $translate['manage_device'] : $controller->Language['manage_device'],
                            'permalink' => $site_url.'user/manageDevice',
                            'id' => 0,
                            'parent_id' => 0,
                            'class' => 'managedevice',  
                            'short_permalink' => 'managedevice',
                            'children' => array()
                        );
                    }
               /* if(isset($plan_payment_gateway) && !empty($plan_payment_gateway)){
                        if ($user->is_deleted > 0) {                    
                            $link_profile[] = array(
                                'title' => isset($translate['reactivate']) ? $translate['reactivate'] : $controller->Language['reactivate'],
                                'permalink' => $site_url.'user/reactivate',
                                'id' => 0,
                                'parent_id' => 1,
                                'class' => 'reactivate',
                                'children' => array()
                            );                    
                        } else {
                            if ($is_subscribed > 0) {                       
                                $link_profile[] = array(
                                    'title' =>  isset($translate['cancel_subscription']) ? $translate['cancel_subscription'] : $controller->Language['cancel_subscription'],
                                    'permalink' => '#cancelsubscription',
                                    'id' => 0,
                                    'parent_id' => 1,
                                    'class' => 'cancelsubscription',
                                    'children' => array()
                                );                         
                            } else {
                                $link_profile[] = array(
                                    'title' =>isset($translate['activate']) ? $translate['activate'] : $controller->Language['activate'],
                                    'permalink' => $site_url.'user/activate',
                                    'id' => 0,
                                    'parent_id' => 1,
                                    'class' => 'activate',
                                    'children' => array()
                                );                                                 
                            }
                        }
                    }*/
                     if ($monetization_menu['menu'] & 1) {
                          $link_profile[] = array(
                                    'title' =>isset($translate['my_plan']) ? $translate['my_plan'] : $controller->Language['my_plan'],
                                    'permalink' => $site_url.'user/myplans',
                                    'id' => 0,
                                    'parent_id' => 1,
                                    'class' => 'myplans',
                              'short_permalink' => 'myplans',
                                    'children' => array()
                                );        
                    }
                    if(!empty($cards)){
                        $link_profile[] = array(
                            'title' =>isset($translate['card_info']) ? $translate['card_info'] : $controller->Language['card_info'],
                            'permalink' => $site_url.'user/cardinformation',
                            'id' => 0,
                            'parent_id' => 1,
                            'class' => 'cardinformation',
                            'short_permalink' => 'cardinformation',
                            'children' => array()
                        );                                
                    }            
                    $link_profile[] = array(
                        'title' =>isset($translate['purchase_history']) ? $translate['purchase_history'] : $controller->Language['purchase_history'],
                        'permalink' => $site_url.'user/PurchaseHistory',
                        'id' => 0,
                        'parent_id' => 1,
                        'class' => 'purchasehistory',
                        'short_permalink' => 'purchasehistory',
                        'children' => array()
                    );            
                    $link_profile[] = array(
                        'title' =>isset($translate['logout']) ? $translate['logout'] : $controller->Language['logout'],
                        'permalink' => $site_url.'user/logout',
                        'id' => 0,
                        'parent_id' => 1,
                        'class' => 'logout',
                        'short_permalink' => 'logout',
                        'children' => array()
                    );


                    $formatted_row[] = array(
                        'title' =>isset($translate['profile']) ? $translate['profile'] : $controller->Language['profile'],
                        'permalink' => $site_url.'user/profile',
                        'id' => 1,
                        'parent_id' => 0,
                        'class' => 'profile',
                        'short_permalink' => 'profile',
                        'profile_image' => $profile_image,
                        'display_name' => $display_name,                
                        'children' => $link_profile
                    );            


                } else {
                    if($hide_signup == 0){
                        $formatted_row[] = array(
                            'title' =>isset($translate['register']) ? $translate['register'] : $controller->Language['register'],
                            'permalink' => $site_url.'user/register',
                            'id' => 0,
                            'parent_id' => 0,
                            'class' => 'register',
                            'short_permalink' => 'register',
                            'children' => array()
                        );                            
                    }
                    $formatted_row[] = array(
                        'title' =>isset($translate['login']) ? $translate['login'] : $controller->Language['login'],
                        'permalink' => $site_url.'user/login',
                        'id' => 0,
                        'parent_id' => 0,
                        'class' => 'login',
                        'short_permalink' => 'login',
                        'children' => array()
                    );            
                }    
            }
            else
                $formatted_row = array();
        }
        else{
            $formatted_row = '';
        }
        return $formatted_row;
    }        
    public function getCastCrew($movie_id,$studio_id,$language_id){
        $sql = "SELECT m.`movie_id`,m.`celebrity_id`,m.`cast_type`,c.name,c.id,c.summary,c.permalink,c.language_id,c.parent_id FROM movie_casts m, celebrities c WHERE c.parent_id=0 AND c.studio_id = " . $studio_id . " AND m.celebrity_id = c.id AND m.movie_id=" . $movie_id;
        $movie_casts = Yii::app()->db->createCommand($sql)->queryAll();
        if($language_id == 20){
            return $movie_casts;
        }else{
            if(!empty($movie_casts)){
                $celeb_ids ="";
                foreach($movie_casts as $movie_cast){
                    $celeb_ids .= $movie_cast['celebrity_id'].",";
                }
                $celeb_ids = rtrim($celeb_ids,',');
                $sqls = "SELECT id,name,summary,parent_id FROM celebrities WHERE studio_id = ". $studio_id ." AND language_id=".$language_id." AND parent_id IN(".$celeb_ids.")";
                $castname_translated = Yii::app()->db->createCommand($sqls)->queryAll();
                $castname = array();
                if(!empty($castname_translated)){
                    foreach($castname_translated as $translated){
                        $castname['name'][$translated['parent_id']]= $translated['name'];
                        $castname['summary'][$translated['parent_id']]= $translated['summary'];
                        /*$castname['celebrity_id'][$translated['parent_id']]= $translated['id'];*/
                    }
                }
                $after_translate = array();
                foreach($movie_casts as $cast){
                    if (array_key_exists($cast['celebrity_id'], $castname['name'])) {
                        $cast['name'] = $castname['name'][$cast['celebrity_id']];
                    } if (array_key_exists($cast['celebrity_id'], $castname['summary'])) {
                        $cast['summary'] = $castname['summary'][$cast['celebrity_id']];
                    } /*if (array_key_exists($cast['celebrity_id'], $castname['celebrity_id'])) {
                        $cast['id'] = $castname['celebrity_id'][$cast['celebrity_id']];
                    }*/
                    $after_translate[] = $cast;
                }
                return $after_translate;
            }
        }
    }
    /**
     * @method getStaticPages Returns the list of pages with translation
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return Array the list of pages with translation
     */
    public function getStaticPages($studio_id,$language_id=20,$pages=array()){
        $translated_pages = array();
        $page_ids = array();
        if(!empty($pages)){
            foreach($pages as $page){
                $page_ids[] =  $page['id'];
            }
            if(!empty($page_ids)){
                $page_ids = implode(',',$page_ids);
                $sql = "SELECT id,parent_id,title from pages WHERE studio_id=".$studio_id." AND language_id=".$language_id." AND parent_id IN (".$page_ids.")";
                $page_translated = Yii::app()->db->createCommand($sql)->queryAll();
                $pagename = array();
                if(!empty($page_translated)){
                    foreach($page_translated as $translated){
                        $pagename[$translated['parent_id']]= $translated['title'];
                    }
                }
                $translated_pages = array();
                $spage = array();
                foreach($pages as $page){
                    if (array_key_exists($page['id'], $pagename)) {
                        $page['display_name'] = $pagename[$page['id']];
                    }
                    $translated_pages[] = $page;
                }
                return $translated_pages;
            }else{
                return $translated_pages;
            }
        }else{
            return $translated_pages;
        }
    }
    /**
     * @method getStaticPageDetails Returns the details of page with translation
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return Array the details of page with translation
     */
    public function getStaticPageDetails($studio_id,$language_id=20,$page=array()){
        $translated_page = array();
        if(!empty($page)){
            $page_id = $page['id'];
            $sql = "SELECT id,parent_id,content,title from pages WHERE studio_id=".$studio_id." AND language_id=".$language_id." AND parent_id =".$page_id;
            $page_translated = Yii::app()->db->createCommand($sql)->queryRow();
            if(!empty($page_translated)){
                $page['title']   = $page_translated['title'];
                $page['content'] = $page_translated['content'];
            }
            $translated_page = $page;
            return $translated_page;
        }else{
            return $translated_page;
        }
    }
    /**
     * @method getTranslatedMenuList Returns the list of Menus with translation
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return Array the list of getTranslatedMenuList with translation
     */
    public function getTranslatedMenuList($studio_id, $language_id=20, $menu_list=array()){
        $translated_menus = array();
        $menu_ids = array();
        if(!empty($menu_list)){
            foreach($menu_list as $list){
                $menu_ids[] =  $list['id'];
            }
            if(!empty($menu_ids)){
                $menu_ids = implode(',',$menu_ids);
                $sql = "SELECT id,language_parent_id,title from menu_items WHERE studio_id=".$studio_id." AND language_id=".$language_id." AND language_parent_id IN (".$menu_ids.")";
                $menu_translated = Yii::app()->db->createCommand($sql)->queryAll();
                $menuname = array();
                if(!empty($menu_translated)){
                    foreach($menu_translated as $translated){
                        $menuname[$translated['language_parent_id']]= $translated['title'];
                    }
                }
                foreach($menu_list as $list){
                    if (array_key_exists($list['id'], $menuname)) {
                        $list['display_name'] = $menuname[$list['id']];
                    }
                    $translated_menus[] = $list;
                }
                return $translated_menus;
            }else{
                return $translated_menus; 
            }
        }else{
           return $translated_menus; 
        }
    }    
    public function convertTimeToMiliseconds($time_string = '00:00:00.00'){
        $time   = explode(":", $time_string);
        $hour   = $time[0] * 60 * 60 * 1000;
        $minute = $time[1] * 60 * 1000;
        $second = explode(".", $time[2]);
        $sec    = $second[0] * 1000;
        $milisec= $second[1];
        $result = $hour + $minute + $sec + $milisec;
        return $result;
    }
    public function formatMilliseconds($milliseconds) {
        $seconds = floor($milliseconds / 1000);
        $minutes = floor($seconds / 60);
        $hours = floor($minutes / 60);
        $milliseconds = $milliseconds % 1000;
        $seconds = $seconds % 60;
        $minutes = $minutes % 60;

        $format = '%02u:%02u:%02u.%02u';
        $time = sprintf($format, $hours, $minutes, $seconds, $milliseconds);
        return $time;
    }    
    
	/**
	 * @This funstion is modified for #7537 
	 * @param string $studio_id studio id
	 * @param array $ids array of category ids
	 * output is get Contents from id not binary value
	 */
    function getContentsFromBinaryValue($studio_id, $ids){
		$cid = implode(',', $ids);
        $controller = Yii::app()->controller;
        $language_id = $controller->language_id;
        $sql = "SELECT id,category_name,permalink,binary_value,parent_id,language_id FROM content_category WHERE studio_id={$studio_id} AND id IN (".$cid.") AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY category_name,IF(parent_id>0,parent_id,id) ASC";
        $cat = Yii::app()->db->createCommand($sql)->queryAll();
        $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
        $favlist = UserFavouriteList::model()->getUsersFavouriteContentList($user_id,$studio_id);
        $favourites = $favlist['list'];
        foreach ($cat as $category) {
            $category_id = $category['id'];
            if ($category['parent_id'] > 0) {
                $category_id = $category['parent_id'];
            }
            $poster_category = $controller->getPoster($category_id, 'content_category');
            $category_name = $category['category_name'];
            $category_permalink = $controller->siteurl . '/' . $category['permalink'];
            $sql = "SELECT id,name,permalink,parent_id FROM films WHERE studio_id =" . $studio_id . " AND FIND_IN_SET(".$category['id'].",content_category_value) AND (parent_id=0 OR language_id=" . $language_id . ") ORDER BY name,parent_id ASC";
            $films = Yii::app()->db->createCommand($sql)->queryAll();
            $content_details = array();
            $poster_film = array();
            if (!empty($films)) {
                foreach ($films as $film) {
                    $id = $film['id'];
                    $fav_status = 1;
                    $login_status = 0;
                    if($user_id){
                        $fav_status = UserFavouriteList::model()->getFavouriteContentStatus($id,$studio_id,$user_id);
                        $fav_status = ($fav_status== 1)? 0 : 1;
                        $login_status = 1;
                    }
                    $poster = $controller->getPoster($id, 'films');
                    $permalink = $controller->siteurl . '/' . $film['permalink'];
                    $content_details[$id] = array(
                        'content_id'=> $id,
                        'poster' => $poster,
                        'content_permalink' => $permalink,
                        'is_episode' => 0,
                        'fav_status' =>$fav_status,
                        'login_status' =>$login_status
                    );
                }
               
            }

            $channels[$category_name] = array(
                'category_name' => $category_name,
                'permalink' => $category_permalink,
                'category_poster' => $poster_category,
                'category' => $content_details
            );
        }
        return $channels;
    }
    
    public function showRelated($studio_id){
        $return = 0;
        $sconfig = StudioConfig::model()->getConfig($studio_id, 'show_related');
        if(isset($sconfig) && count($sconfig) > 0 && $sconfig->config_value == 1){
            $return = 1;
        }
        return $return;
    }  
    public function getFilterTableData($key, $studio_id){
        $data = array();
        $sql = "SHOW COLUMNS FROM `films` LIKE '".$key."'";
        $res = Yii::app()->db->createCommand($sql)->queryRow();
        if(!empty($res)){
            $data['table'] = 'films';
            $data['field'] = $key;
        }else{
            $sql = "SHOW COLUMNS FROM `movie_streams` LIKE '".$key."'";
            $res = Yii::app()->db->createCommand($sql)->queryRow();
            if(!empty($res)){
                $data['table'] = 'movie_streams';
                $data['field'] = $key;
            }
        }
        if(empty($data)){
            $sql = "SELECT fcm.field_name FROM film_custom_metadata fcm JOIN custom_metadata_field cf ON cf.id = fcm.custom_field_id WHERE fcm.studio_id  = ".$studio_id." AND cf.f_name ='".$key."'";
            $res = Yii::app()->db->createCommand($sql)->queryRow();
            if(!empty($res)){
                $data['table'] = 'films';
                $data['field'] = $res['field_name'];
            }else{
                $sql = "SELECT mcm.field_name FROM movie_streams_custom_metadata mcm JOIN custom_metadata_field cf ON cf.id = mcm.custom_field_id WHERE mcm.studio_id  = ".$studio_id." AND cf.f_name ='".$key."'";
                $res = Yii::app()->db->createCommand($sql)->queryRow();
                if(!empty($res)){
                    $data['table'] = 'movie_streams';
                    $data['field'] = $res['field_name'];
                }
            }            
        }
        return $data;
    }   
    public function getConfigValueForStudio($studio_id){
        if(empty($studio_id)){
           $studio_id = Yii::app()->common->getStudioId();
        }
        $limitNotifaction = StudioConfig::model()->getconfigvalueForStudio($studio_id,'daily_notification_limit');
        if(!empty($limitNotifaction)){
            $limit = $limitNotifaction['config_value'];
        }
        else{
            $limit = 1;
        }
        return $limit;
    }
    
    
    public function getPopupBlockMessage() {
        $controller = Yii::app()->controller;
        $url = Yii::app()->getbaseUrl(true)."/shop/popupblocked";
        $message = '';
            $message.= '<script>
                $(document).ready(function() {
                    detectPopupBlocker();
                    function detectPopupBlocker() {
                        var test = window.open("' . $url . '","","width=100,height=100");
                        try {
                            test.close();
                        } catch (e) {
                            $("#popupModal").modal("show");
                        }
                    }
                });
            </script>';
            $message.= '<div id="popupModal" class="modal fade login-popu">
                <div class="modal-dialog">
                    <div class="modal-content" style="position: relative;">  
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" style="line-height: 1;">&nbsp;</h4>
                        </div>                    
                        <div class="modal-body">                            
                            <div class="row-fluid">
                                <div class="col-md-12">';
            $message.= $controller->Language['enable_popup'];
            $message.= '        </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>          
                    </div>		
                </div>
            </div>';
        return $message;
    }
}