<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 20/8/2016
 */

class CDS extends CApplicationComponent {
    /*** This function is for add new contacts/leads using hubspot ***/
    /*** Author ajit@muvi.com ***/
    /*** 20/8/2016 ***/
    function CheckStock($xml_data){ 
        //get the curl response
        $response = Self::XMLCurl($xml_data);      
        //parse the array values
        $doc = new DOMDocument();
        $doc->loadXML($response);
        $parseFile = json_decode(json_encode(simplexml_load_string($doc->getElementsByTagName('checkStockResponse')->item(0)->nodeValue, 'SimpleXMLElement', LIBXML_NOCDATA),true), true);
       
        foreach($parseFile['checkStockResult']['StockCheck'] as $key=>$val)
        {
            $array[$key]=$val;
        }
        return $array;
    }

    function CreateOrder($xml_data){        
        //get the curl response
        $response = Self::XMLCurl($xml_data);
        //parse the array values
        $doc = new DOMDocument();
        $doc->loadXML($response);
        return $doc->getElementsByTagName('createOrderResponse')->item(0)->nodeValue;
    }
    function CancelOrder($xml_data){        
        //get the curl response
        $response = Self::XMLCurl($xml_data);
        //print_r($response);exit;
        //parse the array values
        $doc = new DOMDocument();
        $doc->loadXML($response);
        return $doc->getElementsByTagName('cancelOrderResponse')->item(0)->nodeValue;
    }    
    function GetItemStatus($xml_data){        
        //get the curl response
        $response = Self::XMLCurl($xml_data);
        //print_r($response);exit;
        //parse the array values
        $doc = new DOMDocument();
        $doc->loadXML($response);
        $parseFile = json_decode(json_encode(simplexml_load_string($doc->getElementsByTagName('getOrderStatusResponse')->item(0)->nodeValue, 'SimpleXMLElement', LIBXML_NOCDATA),true), true);
        //print_r($parseFile);
        foreach($parseFile['OrderStatusBundle'] as $key=>$val)//return array order[] and MoreExists[]
        {
            foreach($val as $key1=>$val1 ){
            //echo $key1."=>".json_encode($val1);
            $array[$key1]=$val1;            
            }
        }
        return $array;        
    }     
    
    function XMLCurl($xml_data){
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => 2663));
        $wsdl_url = $webservice->url;
        //generate the xml data
        $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:app="http://app"><soapenv:Header/><soapenv:Body>';
        $xml_post_string .= $xml_data;
        $xml_post_string .= '</soapenv:Body></soapenv:Envelope>';
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: ".$wsdl_url, 
            "Content-length: ".strlen($xml_post_string),
        );       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $wsdl_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);        
        $response = curl_exec($ch); 
        curl_close($ch);
        return $response;
    }
}