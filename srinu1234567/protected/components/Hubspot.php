<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Hubspot extends CApplicationComponent {
    /*** This function is for add new contacts/leads using hubspot ***/
    /*** Author manas@muvi.com ***/
    function AddToHubspot($email,$firstname='',$lastname='',$phone='',$hubspot_owner_id='',$hs_lead_status='',$lead_type=''){
        $arr = array(
            'properties' => array(
                array(
                    'property' => 'email',
                    'value' => $email
                ),
                array(
                    'property' => 'firstname',
                    'value' => $firstname
                ),
                array(
                    'property' => 'lastname',
                    'value' => $lastname
                ),
                array(
                    'property' => 'phone',
                    'value' => $phone
                ),
                array(
                    'property' => 'hubspot_owner_id',
                    'value' => $hubspot_owner_id
                ),
                array(
                    'property' => 'hs_lead_status',
                    'value' => $hs_lead_status
                ),
                array(
                    'property' => 'lead_type',
                    'value' => $lead_type
                )
            )
        );
        $post_json = json_encode($arr);
        $hapikey = HUBSPOT_API_KEY;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;
        }else{
            $endpoint = 'http://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;
        }
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}