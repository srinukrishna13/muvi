<?php
header("Access-Control-Allow-Origin: *");
header('content-type: application/json; charset=utf-8');
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/PushNotification.php';
class RestController extends Controller {
    public $defaultAction = '';
    public $studio_id = '';

    public function init() {
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
                $route->enabled = false;
            }
        }
//      parent::init();
        $headerinfo = getallheaders();
        $authstatus = 0;
        foreach ($headerinfo AS $key => $val) {
            if($key == "authToken"){
                $authstatus = 1 ;
            }
            $_REQUEST[$key] = $val;
        }        
        if($authstatus != 1){
            $requestBody = file_get_contents('php://input');
            $requestBody = json_decode($requestBody);
            foreach ($requestBody AS $key => $val) {
                $_REQUEST[$key] = $val;
            }
        }        
        return true;
    }

    protected function beforeAction($action) {
        parent::beforeAction($action);
		$currentAction = strtolower($action->id);
		if($currentAction != 'getstudioauthkey'){
			$this->validateOauth($_REQUEST);
		}
		$redirect_url = Yii::app()->getBaseUrl(true);
        return true;
    }

    public function encrypt($value) {
        $enc = new bCrypt();
        return $enc->hash($value);
    }

    /**
     * @method private ValidateOauth() Validate the Oauth token if its registered to our database or not
     * @author GDR<support@muvi.com>
     * @return json Json data with parameters
     */
    function validateOauth() {
        if (isset($_REQUEST) && (isset($_REQUEST['authToken']) || isset($_REQUEST['authtoken']) || isset($_REQUEST['muvi_token']))) {
            if(isset($_REQUEST['muvi_token']) && trim($_REQUEST['muvi_token'])) {
                $muvi_token = explode('-',$_REQUEST['muvi_token']);
                $authToken = $muvi_token[1];
            }else{
                $authToken = (isset($_REQUEST['authtoken'])) ? trim($_REQUEST['authtoken']) : $_REQUEST['authToken'];
            }
            $referer = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $referer = parse_url($_SERVER['HTTP_REFERER']);
                $referer = $referer['host'];
            }
            $sql = "SELECT * FROM oauth_registration WHERE oauth_token='$authToken'";
            $data = Yii::app()->db->createCommand($sql)->queryRow();
            if ($data) {
                $data = (object) $data;
                if ($data->request_domain && ($data->request_domain != $referer)) {
                    $data['code'] = 409;
                    $data['status'] = "failure";
                    $data['msg'] = "Domain not registered with Muvi for API calls!";
                    echo json_encode($data);
                    exit;
                }
                if ($data->expiry_date && (strtotime($data->expiry_date) < strtotime(date('Y-m-d')))) {
                    $data['code'] = 410;
                    $data['status'] = "failure";
                    $data['msg'] = "Oauth Token expired!";
                    echo json_encode($data);
                    exit;
                }
                $this->studio_id = $data->studio_id;
                return TRUE;
            } else {
                $data['code'] = 408;
                $data['status'] = "failure";
                $data['msg'] = "Invalid Oauth Token!";
                echo json_encode($data);
                exit;
            }
        } else {
            $data['code'] = 407;
            $data['status'] = "failure";
            $data['msg'] = "Ouath Token required!";
            echo json_encode($data);
            exit;
        }
    }
	 /**
     * @method private login() Get login parameter from API request and Checks login and return response
     * @author GDR<support@muvi.com>
     * @return json Json data with parameters
     * @param string $authToken Ouath Token generated during registartion.
     * @param string $email Registered Email
     * @param string $password Login password for the user
     */
    function actionLogin($req = array()) {
        $studio_id=$this->studio_id;
        if ($req) {
            $_REQUEST = $req;
        }
        if (isset($_REQUEST) && isset($_REQUEST['email']) && isset($_REQUEST['password'])) {
            //$userData = SdkUser::model()->find('email=:email AND studio_id =:studio_id AND status=:status AND is_deleted !=1', array(':email' => $_REQUEST['email'], ':studio_id' => $this->studio_id, ':status' => 1));
            $lang_code   = (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") ? $_REQUEST['lang_code'] : 'en';
            $translate   = $this->getTransData($lang_code, $studio_id);
            $sql = "SELECT id, email, encrypted_password, display_name, nick_name, add_video_log FROM sdk_users WHERE email = '" . trim($_REQUEST['email']) . "' AND studio_id = '" . $studio_id . "' AND status = 1 AND is_deleted != 1 ORDER BY id DESC LIMIT 1 OFFSET 0";
            $connection = Yii::app()->db;
            $userData = (object) $connection->createCommand($sql)->queryRow();
            
            if (!empty($userData)) {
                $enc = new bCrypt();
                $pwd = trim(@$_REQUEST['password']);
                $device_type=$_REQUEST['device_type'];
                if (!$enc->verify(@$pwd, $userData->encrypted_password)) {
                    $data['code'] = 406;
                    $data['status'] = "failure";
                    $data['msg'] = "Email or Password is invalid!";
                } else {
                    $user_id = $userData->id;
                    $data['id'] = $user_id;
                    $data['email'] = $userData->email;
                    $data['display_name'] = $userData->display_name;
                    $data['nick_name'] = $userData->nick_name;
                    $data['studio_id'] = $studio_id;
                    $data['isFree']=$userData->is_free;
                    $data['profile_image'] = $this->getProfilePicture($user_id, 'profilepicture', 'thumb', $studio_id);
                    $isSubscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                    $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
                    $data['is_broadcaster'] = $userData->is_broadcaster;
					$data['language_list'] = Yii::app()->custom->getactiveLanguageList($studio_id);
					$custom_field_details = Yii::app()->custom->getCustomFieldsDetail($studio_id,$user_id);

                    if(!empty($custom_field_details)){
                        foreach($custom_field_details as $key=>$val){
                            $data["custom_".$key] = ($key == "languages") ? $val : $val[0];
                        }
                    }
                    if ($userData->add_video_log){
                            $ip_address = CHttpRequest::getUserHostAddress();
                            $user_id = $user_id;
                            $login_at = date('Y-m-d H:i:s');
                        $login_history = new LoginHistory();
                        $login_history->studio_id = $this->studio_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = $login_at;
                        $login_history->logout_at = "0000-00-00 00:00:00";
                        $login_history->last_login_check = $login_at;
                        $login_history->google_id = isset($_REQUEST['google_id']) ? @$_REQUEST['google_id'] : '';
                        $login_history->device_id = isset($_REQUEST['device_id']) ? @$_REQUEST['device_id'] : '';
                        $login_history->device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
                        $login_history->ip = $ip_address;
                        $login_history->save();
                        $data['login_history_id'] = $login_history->id;
                    }
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['msg'] = "Login Success";
                }
            } else {
                $data['code'] = 406;
                $data['status'] = "failure";
                $data['msg'] = "Email or Password is invalid!";
            }
        } else {
            $data['code'] = 406;
            $data['status'] = "failure";
            $data['msg'] = "Email or Password is invalid!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
    }
    
     /*By Biswajit das(biswajitdas@muvi.com)(for check whether user login or not)*/
    public function actionCheckIfUserLoggedIn(){
        $studio_id=$this->studio_id;
        $user_id=$_REQUEST['user_id'];
        $device_type=$_REQUEST['device_type'];
        $device_id=$_REQUEST['device_id'];
        $sql = "SELECT * FROM `login_history` WHERE `studio_id` = {$studio_id} AND `user_id` = {$user_id} AND `device_type`= '{$device_type}' AND `device_id`= '{$device_id}' AND  (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
        $login_data = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($login_data)){
            $data['code'] = 200;
            $data['is_login'] = '1';
        }
        else{
            $data['code'] = 200;
            $data['is_login'] = '0';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
     public function actionLogoutAll(){
        $studio_id=$this->studio_id;
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code, $studio_id);
        $json = '';
        $response = '';
        
        if (isset($_REQUEST) && isset($_REQUEST['email_id'])) {
            $userData = SdkUser::model()->find('email=:email AND studio_id =:studio_id AND status=:status AND is_deleted !=1', array(':email' => $_REQUEST['email_id'], ':studio_id' => $this->studio_id, ':status' => 1));
            if($userData){   
            $user_id = $userData->id;
            $sql="SELECT google_id FROM login_history WHERE studio_id={$studio_id} and user_id={$user_id} AND (device_type='1') AND (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
            $registration_ids = Yii::app()->db->createCommand($sql)->queryAll();
            $sql2="SELECT google_id FROM login_history WHERE studio_id={$studio_id} and user_id={$user_id} AND (device_type='2') AND (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
            $registration_ids2 = Yii::app()->db->createCommand($sql2)->queryAll();
            $reg_ids2= array();
            $reg_ids = array();
            if(!empty($registration_ids)){
                foreach($registration_ids as $ids){
                    $reg_ids [] = $ids['google_id'];
                }
            }
            if(!empty($registration_ids2)){
                foreach($registration_ids2 as $ids2){
                    $reg_ids2 [] = $ids2['google_id'];
                }
            }
            $title=$user_id;
            $message=$translate['logged_out_from_all_devices'];
            $push = new Push();
            $push->setTitle($title);
            $push->setMessage($message);
            //json data android
            $jsondata = $push->getPushAndroid();
            //json data ios
            $jsonda = $push->getPushIos();
            $response = $push->sendMultiple($reg_ids, $jsondata);
            $response2 = $push->sendMultipleNotify($reg_ids2, $jsonda);
            LoginHistory::model()->logoutUser($studio_id,$user_id);
            $data['code']=200;
            $data['status'] = 'success';
            $data['msg']=$translate['logged_out_from_all_devices'];
            }else{
                $data['code']=300;
                $data['status'] = 'failure';
                $data['msg']="Email is invalid!";
            }
        }
        else{
                $data['code']=300;
                $data['status'] = 'failure';
                $data['msg']="Email required!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }  
    public function actionUpdateGoogleid(){
        $studio_id=$this->studio_id;
        $user_id=$_REQUEST['user_id'];
        $google_id=$_REQUEST['google_id'];
        $device_id =$_REQUEST['device_id'];
        $loginD = new LoginHistory();                
        $loginData = $loginD->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id ,'device_id' =>$device_id,'logout_at'=>'0000-00-00 00:00:00'));  
        if ($loginData){
            $loginData->google_id = $google_id;
            $loginData->save();
            $data['code']=200;
            $data['status'] = 'success';
            $data['msg']="Successfully update.";
        }
        else{
            $data['code']=300;
            $data['status'] = 'Failure';
            $data['msg']="Invalid UserID OR DeviceID.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    /* By Biswajit Parida For updating login history when logout from mobile apps */
    public function actionLogout(){
        if(isset($_REQUEST['login_history_id']) && $_REQUEST['login_history_id']!=""){
            $login_history_id = $_REQUEST['login_history_id'];
            $login_history = LoginHistory::model()->findByPk($login_history_id);
            $login_history->logout_at = date('Y-m-d H:i:s');
            $login_history->save();
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = "Logout Successfully.";       
        }else{
            $data['code'] = 406;
            $data['status'] = "Failure";
            $data['msg'] = "Requests needed";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
    }
    /**
     * @method private getContentList() Get the list of contents based on its content_type permalink
     * @author GDR<support@muvi.com>VideoLogs

     * @return json Returns the list of data in json format
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionGetContentList() {
        if ($_REQUEST['permalink']) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
            if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != 'en')
                $language_id = Yii::app()->custom->getLanguage_id($_REQUEST['lang_code']);
            else
                $language_id = 20;
				$menuItemInfo = MenuItem::model()->find('studio_id=:studio_id AND permalink=:permalink', array(':studio_id' => $this->studio_id, ':permalink' => $_REQUEST['permalink']));
            if ($menuItemInfo) {
                $menuInfo = $menuItemInfo->attributes;
                $content_type = $contentTypeInfo['id'];
                $content_types_id = $contentTypeInfo['content_types_id'];
                $studio_id = $this->studio_id;
                $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                $offset = 0;
                if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                    $offset = ($_REQUEST['offset'] - 1) * $limit;
                }
                $pdo_cond_arr = array(':studio_id' => $this->studio_id, ':content_category_value' => $menuInfo['value']);

                $order = '';
                
                $cond = ' ';
                $pdo_cond = '';
				$_REQUEST['orderby'] = trim($_REQUEST['orderby']);
                $orderData = ContentOrdering::model()->find('studio_id=:studio_id AND category_value=:category_value ',array(':studio_id'=>$studio_id,':category_value'=>$menuInfo['value']));
                if (isset($_REQUEST['orderby']) && $_REQUEST['orderby']!='') {
                    $order = $_REQUEST['orderby'];
                    if ($_REQUEST['orderby'] == 'lastupload') {
                        $orderby = "  M.last_updated_date DESC";
                        $neworderby = " P.last_updated_date DESC ";
                    } else if ($_REQUEST['orderby'] == 'releasedate') {
                        $orderby = "  F.release_date DESC";
                        $neworderby = " P.release_date DESC ";
                    } else if ($_REQUEST['orderby'] == 'sortasc') {
                        $orderby = "  F.name ASC";
                        $neworderby = " P.name ASC ";
                    } else if ($_REQUEST['orderby'] == 'sortdesc') {
                        $orderby = "  F.name DESC";
                        $neworderby = " P.name DESC ";
                    }
                }else if(@$orderData->method){
                    if(@$orderData->orderby_flag==1){// most viewed
                        $mostviewed = 1;
                        $neworderby = " Q.cnt DESC";
                    }else if(@$orderData->orderby_flag==2){//Alphabetic A-Z
                        $orderby = "  trim(F.name) ASC";
                        $neworderby = " trim(P.name) ASC ";
                    }else if(@$orderData->orderby_flag==3){//Alphabetic Z-A
                        $orderby = "  trim(F.name) DESC";
                        $neworderby = " trim(P.name) DESC ";
                    }else {
                        $orderby = "  M.last_updated_date DESC";
                        $neworderby = " P.last_updated_date DESC ";
                    }                    
                }else if($orderData  && $orderData->ordered_stream_ids){
                        $orderby = " FIELD(movie_stream_id,".$orderData->ordered_stream_ids.")";
                        $neworderby = " FIELD(movie_stream_id,".$orderData->ordered_stream_ids.")";
                }else{
                    $orderby = "  M.last_updated_date DESC ";
                    $neworderby = " P.last_updated_date DESC ";
                }
                if (@$_REQUEST['genre']) {
					if(is_array($_REQUEST['genre'])){
						$cond .= " AND (";
						foreach ($_REQUEST['genre'] AS $gkey => $gval){
							if($gkey){
								$cond .= " OR "; 
							 }
							 $cond .= " (genre LIKE ('%" . $gval . "%'))";
						}
						$cond .= " ) ";
					}else{
						$cond .= " AND genre LIKE ('%" . $_REQUEST['genre'] . "%')";
					}
                }
				$cond .= ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) ';
                if($_REQUEST['prev_dev']){
                    $command = Yii::app()->db->createCommand()
                            ->select('SQL_CALC_FOUND_ROWS (0),M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted, M.full_movie,F.language AS content_language,F.censor_rating')
                        ->from('movie_streams M,films F')
                            ->where('M.movie_id = F.id AND M.studio_id=:studio_id AND (FIND_IN_SET(:content_category_value,F.content_category_value)) AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0  ' . $cond, $pdo_cond_arr)
                        ->order($orderby)
                        ->limit($limit, $offset);
                }else{
                    /* Add Geo block to listing by manas@muvi.com*/
                    if(isset($_REQUEST['country']) && $_REQUEST['country']){
                        $country = $_REQUEST['country'];
                    }else{
                        $visitor_loc = Yii::app()->common->getVisitorLocation();
                        $country = $visitor_loc['country'];
                    }
                    $sql_data1 = "SELECT M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id, M.is_episode,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted,M.last_updated_date,F.language AS content_language,F.censor_rating"
                            . " FROM movie_streams M,films F WHERE "
                            . "M.movie_id = F.id AND M.studio_id=" . $studio_id . " AND  (FIND_IN_SET(" . $menuInfo['value'] . ",F.content_category_value)) AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0 " . $cond;
                    $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = ".$studio_id." AND sc.country_code='{$country}'";
                    $sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (".$sql_data1.") AS t LEFT JOIN (".$sql_geo.") AS g ON t.movie_id = g.movieid) AS P ";                     
                    if($mostviewed){
                        $sql_data.=" LEFT JOIN (SELECT COUNT(v.movie_id) AS cnt,v.movie_id FROM video_logs v WHERE `studio_id`=".$studio_id." GROUP BY v.movie_id ) AS Q ON P.movie_id = Q.movie_id";
                    }            
                    $sql_data.=" WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY " . $neworderby. " LIMIT " . $limit . " OFFSET " . $offset;
                    $command = Yii::app()->db->createCommand($sql_data);
                }                
                $list = $command->queryAll();
				$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
				$is_payment_gateway_exist = 0;
				$payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
				if (isset($payment_gateway) && !empty($payment_gateway)) {
					$is_payment_gateway_exist = 1;
				}
				//Get Posters for the Movies 
				$movieids = '';
				$movieList = '';
				if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
					//Retrive the total counts based on deviceType 
					$countQuery  = Yii::app()->db->createCommand()
						->select('COUNT(DISTINCT F.id) AS cnt')
						->from('movie_streams M,films F')
                            ->where('M.movie_id = F.id AND M.studio_id=:studio_id AND (FIND_IN_SET(:content_category_value,F.content_category_value)) AND (IF(F.content_types_id =3,is_episode=1, is_episode=0)) AND (F.content_types_id =4 OR (M.is_converted =1 AND M.full_movie !=\'\')) AND F.parent_id=0 AND M.episode_parent_id=0 ' . $cond, $pdo_cond_arr);
					$itemCount = $countQuery->queryAll();
					$item_count = @$itemCount[0]['cnt'];

					$newList = array();
					foreach ($list AS $k => $v) {
						if ($v['content_types_id'] == 3) {
							$epSql = " SELECT id FROM movie_streams where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1 AND episode_parent_id=0 LIMIT 1";
							$epData = Yii::app()->db->createCommand($epSql)->queryAll();
							if (count($epData)) {
								$newList[] = $list[$k];
							}
                                                //Added by prakash on 1st feb 2017        
						} else if($v['content_types_id'] == 4){
							$epSql = " SELECT id FROM livestream where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . "  LIMIT 1";
							$epData = Yii::app()->db->createCommand($epSql)->queryAll();
							if (count($epData)) {
								$newList[] = $list[$k];
							}
						} else if (($v['content_types_id']==1 || $v['content_types_id']==2 ) && ($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
							$newList[] = $list[$k];
						}
					}
					$list = array();
					$list = $newList;
				}
				$domainName= $this->getDomainName();
				$livestream_content_ids = '';
				foreach ($list AS $k => $v) {
					$movieids .="'" . $v['movie_id'] . "',";
					if($v['content_types_id'] == 4){
						$livestream_content_ids .="'" .$v['movie_id']. "',";
					}
					if ($v['content_types_id'] == 2 || $v['content_types_id'] == 4) {
						$defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
					} else {
						$defaultPoster = POSTER_URL . '/' . 'no-image-a.png';
					}
					$movie_id = $v['movie_id'];
					$v['poster_url'] = $defaultPoster;
					$list[$k]['poster_url'] = $defaultPoster;
                                        $list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0)?$v['is_episode']:0;
					$is_episode=0;

					$arg['studio_id'] = $this->studio_id;
					$arg['movie_id'] = $v['movie_id'];
					$arg['season_id'] = 0;
					$arg['episode_id'] = 0;
					$arg['content_types_id'] = $v['content_types_id'];

					$isFreeContent = Yii::app()->common->isFreeContent($arg);
					$list[$k]['isFreeContent'] = $isFreeContent;
					$list[$k]['embeddedUrl'] = $domainName.'/embed/'.$v['movie_stream_uniq_id'];
					$is_ppv = $is_advance = 0;
					if ((intval($isFreeContent) == 0) && ($is_payment_gateway_exist == 1)) {
						if ($v['is_converted'] == 1) {
							$ppv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id, 0);
							if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
								$is_ppv = 1;
							} else {
								$ppv_plan = Yii::app()->common->getContentPaymentType($v['content_types_id'], $v['ppv_plan_id'], $this->studio_id);
								$is_ppv = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? 1 : 0;
							}
						} else  {
							$adv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id);
							$is_advance = (isset($adv_plan->id) && intval($adv_plan->id)) ? 1 : 0;
						}
					}

					if (intval($is_ppv)) {
						$list[$k]['is_ppv'] = $is_ppv;
					}
					if (intval($is_advance)) {
						$list[$k]['is_advance'] = $is_advance;
					}

					if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
						$director = '';
						$actor = '';
						$castsarr = $this->getCasts($v['movie_id'],'',$language_id,$this->studio_id);
						if ($castsarr) {
							$actor = $castsarr['actor'];
							$actor = trim($actor, ',');
							unset($castsarr['actor']);
							$director = $castsarr['director'];
							unset($castsarr['director']);
						}
						$list[$k]['actor'] = @$actor;
						$list[$k]['director'] = @$director;
						$list[$k]['cast_detail'] = @$castsarr;
					}
				}

				$movieids = rtrim($movieids, ',');
				$livestream_content_ids = rtrim($livestream_content_ids,',');
				$liveFeedUrls = array();
				if($livestream_content_ids){
					$StreamSql = " SELECT feed_url,movie_id,is_online FROM livestream where movie_id IN (".$livestream_content_ids .') AND studio_id=' . $this->studio_id . "  ";
					$streamData = Yii::app()->db->createCommand($StreamSql)->queryAll();
					foreach($streamData AS $skey=>$sval){
						$liveFeedUrls[$sval['movie_id']]['feed_url']= $sval['feed_url'];
						$liveFeedUrls[$sval['movie_id']]['is_online']= $sval['is_online'];
					}
				}
				if ($movieids) {
					$sql = "SELECT id,name,language,censor_rating,genre,story,parent_id FROM films WHERE parent_id IN (".$movieids.") AND studio_id=".$studio_id." AND language_id=".$language_id;
					$otherlang1 = Yii::app()->db->createCommand($sql)->queryAll();
					foreach ($otherlang1 as $key => $val) {
						$otherlang[$val['parent_id']] = $val;
					}

					$psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movieids . ") AND (object_type='films' OR object_type='tvapp') ";
					$posterData = Yii::app()->db->createCommand($psql)->queryAll();
					if ($posterData) {
						$posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
						foreach ($posterData AS $key => $val) {
							$posterUrl = '';
							if($val['object_type'] == 'films'){
								$posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/thumb/' . urlencode($val['poster_file_name']);
								$posters[$val['movie_id']] = $posterUrl;
								if(($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku'){
									$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
								}
							}else if(($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')){
								$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
							}
						}
					}
					//Get view count status
					$viewStatus = VideoLogs::model()->getViewStatus($movieids,$studio_id);
					if(@$viewStatus){
						$viewStatusArr = '';
						foreach ($viewStatus AS $key=>$val){
							$viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount']; 
							$viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount']; 
						}
					}
					foreach ($list as $key => $val) {
						if (isset($otherlang[$val['movie_id']])) {
							$list[$key]['name'] = $otherlang[$val['movie_id']]['name'];
							$list[$key]['story'] = $otherlang[$val['movie_id']]['story'];
							$list[$key]['genre'] = $otherlang[$val['movie_id']]['genre'];
							$list[$key]['censor_rating'] = $otherlang[$val['movie_id']]['censor_rating'];
						}
						if (isset($posters[$val['movie_id']])) {
							if(isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != ''){
								$list[$key]['poster_url_for_tv'] =$postersforTv[$val['movie_id']];
							}
							if (($val['content_types_id'] == 2) || ($val['content_types_id'] == 4)) {
									$posters[$val['movie_id']] = str_replace('/thumb/', '/episode/', $posters[$val['movie_id']]);
							}
							$list[$key]['poster_url'] = $posters[$val['movie_id']];
						}
						if(@$viewStatusArr[$val['movie_id']]){
							$list[$key]['viewStatus'] = $viewStatusArr[$val['movie_id']];
						}else{
							$list[$key]['viewStatus'] = array('viewcount'=>"0",'uniq_view_count'=>"0");
						}
						if($val['content_types_id']== 4){
							$list[$key]['feed_url'] = @$liveFeedUrls[$val['movie_id']]['feed_url'];
							$list[$key]['is_online'] = @$liveFeedUrls[$val['movie_id']]['is_online'];
						}
					}
				}
				$data['status'] = 200;
				$data['msg'] = $translate['btn_ok'];
				$data['movieList'] = @$list;
				if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] && is_numeric($_REQUEST['user_id'])) {
					$user = SdkUser::model()->countByAttributes(array('id' => $_REQUEST['user_id']));
					if ($user) {
						$favourite = Yii::app()->db->createCommand()
							->select('*')
							->from('user_favourite_list')
                                                     ->where('user_id=:user_id AND studio_id=:studio_id AND status=:status',array(':user_id' => $_REQUEST['user_id'], ':studio_id' => $this->studio_id, ':status' => 1))
							->queryAll();
						 $data['is_favorite'] = (count($favourite) > 0) ? 1 : 0;
					 }
				 }                                 
				$data['orderby'] = @$order;
				$data['item_count'] = @$item_count;
				$data['limit'] = @$page_size;
				$data['Ads'] = $this->getStudioAds();
            } else {
                $data['code'] = 412;
                $data['status'] = "Invalid Content Type";
                $data['msg'] = $translate['provided_permalink_invalid'];
            }
        } else {
            $data['code'] = 411;
            $data['status'] = "Invalid Content Type";
            $data['msg'] = $translate['content_type_permalink_required'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method getstudioAds() It will return the add server details if enabled 
     * @author Gayadhar<support@muvi.com>
     * @return array array of details
     */
    function getStudioAds() {
        //Find the Ad Channel id if enabled for the studio
        $arr = array();
        $studioAds = StudioAds::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio_id));
        if ($studioAds) {
            if ($studioAds->ad_network_id == 1) {
                $data['Ads']['network'] = 'spotx';
            } elseif ($studioAds->ad_network_id == 2) {
                $arr['network'] = 'yume';
            }
            $arr['channelId'] = $studioAds->channel_id;
        }
        return $arr;
    }

    /**
     * @method private getepisodeDetails() Get the details of Episode
     * @author GDR<support@muvi.com>
     * @return json Returns the list of episodes
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionEpisodeDetails() {
        if ($_REQUEST['permalink']) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $content_order = StudioConfig::model()->getConfig($this->studio_id, 'multipart_content_setting')->config_value;            
            $order_type=($content_order==0)?'desc':'asc';
            $movie = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.uniq_id AS muvi_uniq_id,f.ppv_plan_id, f.content_type_id,f.content_types_id,ms.id AS movie_stream_id,f.permalink')
                    ->from('films f ,movie_streams ms ')
                    ->where('ms.movie_id = f.id AND ms.is_episode=0 AND f.permalink=:permalink AND f.studio_id=:studio_id ', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryAll();
            if ($movie) {
                $movie = $movie[0];
                $movie_id = $movie['id'];
                $langcontent = Yii::app()->custom->getTranslatedContent($movie_id,0,$language_id,$this->studio_id);
                if (array_key_exists($movie_id, $langcontent['film'])) {
                    $movie['name']  = $langcontent['film'][$movie_id]->name;
                    $movie['story'] = $langcontent['film'][$movie_id]->story;
                    $movie['genre'] = $langcontent['film'][$movie_id]->genre;
                    $movie['censor_rating'] = $langcontent['film'][$movie_id]->censor_rating;
                }
                $cast = array();
                if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                    $director = '';
                    $actor = '';
                    $castsarr = $this->getCasts($movie_id,'',$language_id,$this->studio_id);
                    if ($castsarr) {
                        $actor = $castsarr['actor'];
                        $actor = trim($actor, ',');
                        unset($castsarr['actor']);
                        $director = $castsarr['director'];
                        unset($castsarr['director']);
                    }
                    $cast['actor'] = @$actor;
                    $cast['director'] = @$director;
                    $cast['cast_detail'] = @$castsarr;
                }

                //Checking PPV set or not
                $ppv = Yii::app()->common->getContentPaymentType($movie['content_types_id'], $movie['ppv_plan_id'], $this->studio_id);
                $ppvarr = array();

                if ((isset($ppv) && !empty($ppv))) {
                    $command = Yii::app()->db->createCommand()
                            ->select('default_currency_id')
                            ->from('studios')
                            ->where('id = ' . $this->studio_id);
                    $studio = $command->queryAll();

                    $price = Yii::app()->common->getPPVPrices($ppv->id, $studio[0]['default_currency_id'], $_REQUEST['country']);
                    (array) $currency = Currency::model()->findByPk($price['currency_id']);

                    $ppvarr['id'] = $ppv->id;
                    $ppvarr['title'] = $ppv->title;

                    $ppvarr['pricing_id'] = $price['id'];

                    $ppvarr['price_for_unsubscribed'] = $price['price_for_unsubscribed'];
                    $ppvarr['price_for_subscribed'] = $price['price_for_subscribed'];

                    $ppvarr['show_unsubscribed'] = $price['show_unsubscribed'];
                    $ppvarr['season_unsubscribed'] = $price['season_unsubscribed'];
                    $ppvarr['episode_unsubscribed'] = $price['episode_unsubscribed'];

                    $ppvarr['show_subscribed'] = $price['show_subscribed'];
                    $ppvarr['season_subscribed'] = $price['season_subscribed'];
                    $ppvarr['episode_subscribed'] = $price['episode_subscribed'];

                    $validity = PpvValidity::model()->findByAttributes(array('studio_id' => $this->studio_id));

                    $ppvarr['validity_period'] = $validity->validity_period;
                    $ppvarr['validity_recurrence'] = $validity->validity_recurrence;

                    if (isset($ppv->content_types_id) && (trim($ppv->content_types_id) == 3 || trim($ppv->content_types_id) == 6)) {
                        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
                        $ppvarr['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                        $ppvarr['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                        $ppvarr['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
                    }

                    $movie['is_ppv'] = 1;
                } else {
                    $movie['is_ppv'] = 0;
                }

                if ($movie['content_types_id'] != 3 && $movie['content_types_id'] != 6) {
                    $data['code'] = 415;
                    $data['status'] = "Invalid Content";
                    $data['msg'] = "Invalid content to get episode details";
                    echo json_encode($data);
                    exit;
                }
                $pdo_cond_arr = array(':movie_id' => $movie_id, ":is_episode" => 1);
                if (isset($_REQUEST['series_number']) && $_REQUEST['series_number']) {
                    $cond = " AND series_number=:series_number";
                    $pdo_cond_arr[':series_number'] = $_REQUEST['series_number'];
                }
                $forRokuCond = '';
                if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                    $forRokuCond = '  AND is_converted=:is_converted ';
                    $pdo_cond_arr[':is_converted'] = 1;
                }
                $orderby = ' episode_number '.$order_type;

                $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                $offset = 0;
                if (isset($_REQUEST['offset'])) {
                    $offset = ($_REQUEST['offset'] - 1) * $limit;
                }

                $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
                $bucket = $bucketInfo['bucket_name'];
                $s3url = $bucketInfo['s3url'];
                $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
                $signedFolderPath = $folderPath['signedFolderPath'];
                $video_url = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/";
                $list = array();
				$addQuery = '';
				$customFormObj = new CustomForms();
				$customData = $customFormObj->getFormCustomMetadat($this->studio_id, 4);
				if($customData){
					foreach ($customData AS $ckey => $cvalue){
						$addQuery = ", ".$cvalue['field_name'] ." AS ".addslashes($cvalue['f_id']);
						$customArr[$cvalue['f_id']] = $cvalue['f_display_name']; 
					}
				}
                $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),id,embed_id AS movie_stream_uniq_id,full_movie,episode_number,video_resolution,episode_title,series_number,episode_date,episode_story,IF((full_movie != "" AND is_converted = 1),CONCAT(\'' . $video_url . '\',id,\'/\',full_movie),"") AS video_url,thirdparty_url,rolltype,roll_after,video_duration'.$addQuery)
                        ->from('movie_streams')
                        ->where('movie_id=:movie_id AND is_episode=:is_episode AND episode_parent_id = 0 ' . $forRokuCond . $cond, $pdo_cond_arr)
                        ->order($orderby)
                        ->limit($limit, $offset);
                $list = $command->queryAll();

                $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                if ($list) {
                    $domainName= $this->getDomainName();
                    $_REQUEST['is_mobile'] = 1;
                    foreach ($list AS $k => $v) {
                        $langcontent = Yii::app()->custom->getTranslatedContent($v['id'],1,$language_id,$this->studio_id);
                        if (array_key_exists($v['id'], $langcontent['episode'])) {
                            $list[$k]['episode_title']  = $langcontent['episode'][$v['id']]->episode_title;
                            $list[$k]['episode_story'] = $langcontent['episode'][$v['id']]->episode_story;
                        }
                        $list[$k]['embeddedUrl'] = $domainName.'/embed/'.$v['movie_stream_uniq_id'];
                        $defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
                        $v['poster_url'] = $defaultPoster;
                        $list[$k]['poster_url'] = $this->getPoster($v['id'], 'moviestream', 'episode', $this->studio_id);
                        if(isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku'){
                            $posterUrlForTv = $this->getPoster($v['id'], 'moviestream', 'roku', $this->studio_id);
                            if($posterUrlForTv != ''){
                                $list[$k]['posterForTv'] = $posterUrlForTv;
                            }
                        }
                        $list[$k]['movieUrlForTv'] = '';
                        $list[$k]['video_url'] = '';
                        $list[$k]['resolution'] = array();
                        if($v['thirdparty_url'] != ""){
                            $info = pathinfo($v['thirdparty_url']);
                            if (@$info["extension"] == "m3u8"){ 
                                $list[$k]['movieUrlForTv'] = $v['thirdparty_url'];
                                $list[$k]['video_url'] = $v['thirdparty_url'];
                            } 
                            $list[$k]['thirdparty_url']=$this->getSrcFromThirdPartyUrl($v['thirdparty_url']);
                        } else if ($v['video_url'] != '') {
                            $list[$k]['movieUrlForTv'] = $v['video_url'];
                            $multipleVideo = $this->getvideoResolution($v['video_resolution'], $v['video_url']);
                            $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $v['video_url']);
                            $videoToBeplayed12 = explode(",", $videoToBeplayed);
                            $list[$k]['video_url'] = $videoToBeplayed12[0];
                            $list[$k]['resolution'] = $multipleVideo;
                            //Ad Details
                            $list[$k]['adDetails'] = array();
                            if ($v['rolltype'] == 1) {
                                $list[$k]['adDetails'][] = 0;
                            } elseif ($v['rolltype'] == 2) {
                                $roleafterarr = explode(',', $v['roll_after']);
                                foreach ($roleafterarr AS $ktime => $vtime) {
                                    $list[$k]['adDetails'][] = $this->convertTimetoSec($vtime);
                                }
                            } elseif ($v['rolltype'] == 3) {
                                $list[$k]['adDetails'][] = $this->convertTimetoSec($v['video_duration']);
                            }
                        }
                    }
                }
                $comments = Comment::model()->findAll('movie_id=:movie_id AND  (parent_id IS NULL OR parent_id =0)  ORDER BY id desc', array(':movie_id' => $movie_id));

                $data['code'] = 200;
                $data['msg'] = $translate['btn_ok'];
                $data['name'] = $movie['name'];
                $data['muvi_uniq_id'] = $movie['muvi_uniq_id'];
				$data['custom_fields'] = $customArr;
                $data['is_ppv'] = $movie['is_ppv'];
                if (!empty($ppvarr)) {
                    $data['ppv_pricing'] = $ppvarr;
                    $data['currency'] = $currency->attributes;
                }
                $data['permalink'] = $movie['permalink'];
                $data['item_count'] = $item_count;
                $data['limit'] = $limit;
                $data['offset'] = $offset;
                $data['comments'] = $comments;
                $data['episode'] = $list;
                $data['cast'] = $cast;
            } else {
                $data['code'] = 413;
                $data['status'] = "Invalid Permalink";
                $data['msg'] = $translate['content_permalink_invalid'];
            }
        } else {
            $data['code'] = 414;
            $data['status'] = "Invalid Content";
            $data['msg'] = $translate['content_permalink_required'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method private getContentDetails() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns details of the content
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionGetContentDetails() {
        if ($_REQUEST['permalink']) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate   = $this->getTransData($lang_code, $this->studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $movie = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.content_types_id,ms.id AS movie_stream_id,ms.full_movie,ms.is_converted,ms.video_resolution,ms.embed_id AS movie_stream_uniq_id, f.uniq_id AS muvi_uniq_id,f.ppv_plan_id, f.permalink,f.content_type_id,f.genre,f.release_date,f.censor_rating,f.story,ms.rolltype,ms.roll_after,ms.video_duration,ms.thirdparty_url,ms.is_episode,f.language AS content_language')
                    ->from('films f ,movie_streams ms ')
                    ->where('ms.movie_id = f.id AND ms.is_episode=0 AND ((ms.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(ms.content_publish_date) = 0) OR (ms.content_publish_date <=NOW())) AND  f.permalink=:permalink AND  f.studio_id=:studio_id AND f.parent_id = 0 AND ms.episode_parent_id=0 ', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryAll();            
        if(isset($_REQUEST['country']) && $_REQUEST['country']){
            $country = $_REQUEST['country'];
        }else{
            $visitor_loc = Yii::app()->common->getVisitorLocation();
            $country = $visitor_loc['country'];
        }
        if (Yii::app()->common->isGeoBlockContent(@$movie[0]['id'], @$movie[0]['movie_stream_id'],$this->studio_id,$country)) {//Auther manas@muvi.com
            if ($movie) {
                $movie = $movie[0];
                $thirdPartyUrl='';
                $domainName= $this->getDomainName();
                $movie['censor_rating'] = stripslashes($movie['censor_rating']);
                $movie_id = $movie['id'];
                $langcontent = Yii::app()->custom->getTranslatedContent($movie_id,0,$language_id,$this->studio_id);
                $arg['studio_id'] = $this->studio_id;
                $arg['movie_id'] = $movie_id;
                $arg['season_id'] = 0;
                $arg['episode_id'] = 0;
                $arg['content_types_id'] = $movie['content_types_id'];
                
                $isFreeContent = Yii::app()->common->isFreeContent($arg);
                $movie['isFreeContent'] = $isFreeContent;
                
                //get trailer info from movie_trailer table 
                $trailerThirdpartyUrl='';
                $trailerUrl='';
                $embedTrailerUrl='';                                  
                $trailerData=movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id'=>$movie_id));
                if($trailerData['trailer_file_name']!=''){
                    $embedTrailerUrl=$domainName.'/embedTrailer/'.$movie['muvi_uniq_id'];
                    if($trailerData['third_party_url']!=''){
                        $trailerThirdpartyUrl=$this->getSrcFromThirdPartyUrl($trailerData['third_party_url']);
                    }else if($trailerData['video_remote_url']!=''){
                        $trailerUrl = $this->getTrailer($movie_id,"",$this->studio_id);
                    }  
                }                
                //End of trailer part    
                //Checking PPV set or not
                $is_ppv = $is_advance = 0;
                if (intval($isFreeContent) == 0) {
                    
                    $payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                    
                    if (isset($payment_gateway) && !empty($payment_gateway)) {
                        if ($movie['is_converted'] == 1) {
                            $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id, 0);
                            if (isset($ppv->id) && intval($ppv->id)) {
                                $is_ppv = 1;
                            } else {
                                $ppv = Yii::app()->common->getContentPaymentType($movie['content_types_id'], $movie['ppv_plan_id'], $this->studio_id);
                                $is_ppv = (isset($ppv->id) && intval($ppv->id)) ? 1 : 0;
                            }
                        } else  {
                            $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id);
                            $is_advance = (isset($ppv->id) && intval($ppv->id)) ? 1 : 0;
                        }
                    }
                }
                
                $ppvarr = array();

                if (intval($is_ppv) || intval($is_advance)) {
                    $command = Yii::app()->db->createCommand()
                            ->select('default_currency_id,rating_activated')
                            ->from('studios')
                            ->where('id = ' . $this->studio_id);
                    $studio = $command->queryAll();

                    $price = Yii::app()->common->getPPVPrices($ppv->id, $studio[0]['default_currency_id'], $_REQUEST['country']);
                    (array) $currency = Currency::model()->findByPk($price['currency_id']);

                    $ppvarr['id'] = $ppv->id;
                    $ppvarr['title'] = $ppv->title;

                    $ppvarr['pricing_id'] = $price['id'];

                    $ppvarr['price_for_unsubscribed'] = $price['price_for_unsubscribed'];
                    $ppvarr['price_for_subscribed'] = $price['price_for_subscribed'];

                    $ppvarr['show_unsubscribed'] = $price['show_unsubscribed'];
                    $ppvarr['season_unsubscribed'] = $price['season_unsubscribed'];
                    $ppvarr['episode_unsubscribed'] = $price['episode_unsubscribed'];

                    $ppvarr['show_subscribed'] = $price['show_subscribed'];
                    $ppvarr['season_subscribed'] = $price['season_subscribed'];
                    $ppvarr['episode_subscribed'] = $price['episode_subscribed'];

                    $validity = PpvValidity::model()->findByAttributes(array('studio_id' => $this->studio_id));

                    $ppvarr['validity_period'] = $validity->validity_period;
                    $ppvarr['validity_recurrence'] = $validity->validity_recurrence;

                    if (isset($ppv->content_types_id) && trim($ppv->content_types_id) == 3) {
                        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
                        $ppvarr['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                        $ppvarr['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                        $ppvarr['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
                    }

                    if (intval($is_ppv)) {
                        $movie['is_ppv'] = $is_ppv;
                    }
                    if (intval($is_advance)) {
                        $movie['is_advance'] = $is_advance;
                    }
                } else {
                    $command = Yii::app()->db->createCommand()
                            ->select('rating_activated')
                            ->from('studios')
                            ->where('id = ' . $this->studio_id);
                    $studio = $command->queryAll();                    
                    $movie['is_ppv'] = 0;
                    $movie['is_advance'] = 0;
                }

                $castsarr = $this->getCasts($movie_id,'',$language_id,$this->studio_id);
                if ($castsarr) {
                    $actor = $castsarr['actor'];
                    $actor = trim($actor, ',');
                    unset($castsarr['actor']);
                    $director = $castsarr['director'];
                    unset($castsarr['director']);
                }
                
                $cast_detail = $castsarr;
                $movie['actor'] = @$actor;
                $movie['director'] = @$director;
                $movie['cast_detail'] = @$castsarr;
                $movie['embeddedUrl'] = "";
                
                //Get view status 
                $viewStatus = VideoLogs::model()->getViewStatus($movie_id,$this->studio_id);
                $movie['viewStatus']['viewcount'] = @$viewStatus[0]['viewcount']?@$viewStatus[0]['viewcount']:"0";
                $movie['viewStatus']['uniq_view_count'] = @$viewStatus[0]['u_viewcount']?@$viewStatus[0]['u_viewcount']:"0";
                if($movie['thirdparty_url'] != ""){
                    $info = pathinfo($movie['thirdparty_url']);
                    if (@$info["extension"] == "m3u8"){ 
                        $movieUrlToBePlayed = $movie['thirdparty_url'];
                        $movieUrl = $movie['thirdparty_url'];
                    } 
                    $thirdPartyUrl=$this->getSrcFromThirdPartyUrl($movie['thirdparty_url']);
                    $movie['embeddedUrl'] = $domainName.'/embed/'.$movie['movie_stream_uniq_id'];
                }else if ($movie['full_movie'] != '' && $movie['is_converted'] == 1) {
                    $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
                    $bucket = $bucketInfo['bucket_name'];
                    $s3url = $bucketInfo['s3url'];
                    $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
                    $signedFolderPath = $folderPath['signedFolderPath'];
                    $movieUrl = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/" . $movie['movie_stream_id'] . "/" . urlencode($movie['full_movie']);
                    $movie['movieUrlForTv'] = $movieUrl;
                    $_REQUEST['is_mobile'] = 1;
                    $multipleVideo = $this->getvideoResolution($movie['video_resolution'], $movieUrl);
					
                    $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $movieUrl);
                    $videoToBeplayed12 = explode(",", $videoToBeplayed);
                    $movieUrlToBePlayed = $videoToBeplayed12[0];
                    $movie['embeddedUrl'] = $domainName.'/embed/'.$movie['movie_stream_uniq_id'];
                }                
                $movie['movieUrlForTv'] = @$movieUrl;
                $movie['movieUrl'] = @$movieUrlToBePlayed;        
                //Added thirdparty url
                $movie['thirdparty_url']=$thirdPartyUrl;               
                if ($movie['content_types_id'] == 4) {
                    $Ldata = Livestream::model()->find("studio_id=:studio_id AND movie_id=:movie_id", array(':studio_id'=>$this->studio_id,':movie_id'=>$movie_id));
                    if($Ldata && $Ldata->feed_url){
                        $movie['embeddedUrl'] = $domainName.'/embed/livestream/'.$movie['muvi_uniq_id'];
                        $movie['feed_url'] = $Ldata->feed_url;
                    }					
		}
                $movie['resolution'] = @$multipleVideo;
                $MovieBanner = $this->getPoster($movie_id, 'topbanner', 'original', $this->studio_id);
                if ($movie['content_types_id'] == 2) {
                    $poster = $this->getPoster($movie_id, 'films', 'episode', $this->studio_id);
                } else {
                    $poster = $this->getPoster($movie_id, 'films', 'thumb', $this->studio_id);
                }
                if(isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku'){
                    $posterForTv = $this->getPoster($movie_id, 'films', 'roku', $this->studio_id);
                    if($posterForTv != ''){
                        $movie['posterForTv'] = $posterForTv;
                    }
                }
                $movie['banner'] = $MovieBanner;
                $movie['poster'] = @$poster;
                //$data = $this -> getMovieDetails($movie_id,0);
                //$item = json_decode($data,TRUE);

                $movie_name = $movie['name'];
                if ($movie['content_types_id'] == 3) {
                    $EpDetails = $this->getEpisodeToPlay($movie_id, $this->studio_id);
                    $seasons = $this->getContentSeasons($movie_id, $this->studio_id);
                }
                $comments = Comment::model()->findAll('movie_id=:movie_id AND  (parent_id IS NULL OR parent_id =0)  ORDER BY id desc', array(':movie_id' => $movie_id));
                $movie['adDetails'] = array();
                if ($movie['rolltype'] == 1) {
                    $movie['adDetails'][] = 0;
                } elseif ($movie['rolltype'] == 2) {
                    $roleafterarr = explode(',', $movie['roll_after']);
                    foreach ($roleafterarr AS $tkey => $tval) {
                        $movie['adDetails'][] = $this->convertTimetoSec($tval);
                    }
                } elseif ($movie['rolltype'] == 3) {
                    $movie['adDetails'][] = $this->convertTimetoSec($movie['video_duration']);
                }
                if (array_key_exists($movie_id, $langcontent['film'])) {
                    $movie['name']  = $langcontent['film'][$movie_id]->name;
                    $movie['story'] = $langcontent['film'][$movie_id]->story;
                    $movie['genre'] = $langcontent['film'][$movie_id]->genre;
                    $movie['censor_rating'] = $langcontent['film'][$movie_id]->censor_rating;
                }
                //trailer part
                $movie['trailerThirdpartyUrl']=$trailerThirdpartyUrl;
                $movie['trailerUrl']=$trailerUrl;
                $movie['embedTrailerUrl']=$embedTrailerUrl;
                $movie['is_episode'] = (isset($movie['is_episode']) && strlen($movie['is_episode']) > 0)?$movie['is_episode']:0;
               //end trailer part                 
               //favorite items
				if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] && is_numeric($_REQUEST['user_id'])) {
					$user = SdkUser::model()->countByAttributes(array('id' => $_REQUEST['user_id']));
					if ($user) {
						$favourite = Yii::app()->db->createCommand()
							->select('*')
							->from('user_favourite_list')
                                    ->where('user_id=:user_id AND content_id=:content_id AND studio_id=:studio_id AND status=:status',array(':user_id' => $_REQUEST['user_id'],':content_id' => $movie['id'], ':studio_id' => $this->studio_id,':status'=>1))
							->queryAll();
						$movie['is_favorite'] = (count($favourite) > 0) ? 1 : 0;
					}
				}
                $data['code'] = 200;
                $data['msg'] = $translate['btn_ok'];
                $data['movie'] = $movie;
                
                if (!empty($ppvarr)) {
                    if (intval($is_ppv)) {
                        $data['ppv_pricing'] = $ppvarr;
                    }
                    if (intval($is_advance)) {
                        $data['adv_pricing'] = $ppvarr;
                    }
                    $data['currency'] = $currency->attributes;
                }                
                $data['comments'] = $comments;
                if($studio[0]['rating_activated'] == 1){
                    $data['rating'] = $this->actionRating($movie_id);
					$reviews = ContentRating::model()->count('content_id=:content_id AND studio_id=:studio_id AND status=1', array(':content_id'=>$movie_id,':studio_id'=>$this->studio_id));
					$data['review'] = $reviews;
                }
                $data['epDetails'] = $EpDetails;
                if (@$seasons)
                    $data['seasons'] = $seasons;
            } else {
                $data['code'] = 413;
                $data['status'] = "Invalid Permalink";
                $data['msg'] = $translate['content_permalink_invalid'];
            }
        }else{
            $data['code'] = 414;
            $data['status'] = "Invalid Content";
            $data['msg'] = $translate['content_not_available_in_your_country'];
        }   
        } else {
            $data['code'] = 414;
            $data['status'] = "Invalid Content";
            $data['msg'] = $translate['content_permalink_required'];
        }
        $this->setHeader($data['code']);         
        echo json_encode($data);
        exit;
    }

     protected function actionRating($content_id) {
        $ratings = Yii::app()->db->createCommand()
                ->select('ROUND(AVG(rating),1) as rating')
                ->from('content_ratings')    
                ->where('content_id=:content_id AND studio_id=:studio_id AND status=1', array(':content_id'=>$content_id,':studio_id'=>$this->studio_id))
                ->queryRow();
        return $ratings['rating']==0?0:$ratings['rating'];
    }

    public function actionReviews() {
        if ($_REQUEST['content_id']) {
            $content_id = $_REQUEST['content_id'];
            $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 5;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            $sql = "SELECT r.*, u.display_name FROM content_ratings r LEFT JOIN sdk_users u ON r.user_id=u.id WHERE r.studio_id =". $this->studio_id .
                        " AND r.content_id=$content_id AND r.status=1 ORDER BY r.created_date DESC LIMIT $offset, $limit";
            $ratings = Yii::app()->db->createCommand($sql)->queryAll();
            $i = 0;
            foreach($ratings as $rating){
                $review[$i]['rating'] = $rating["rating"];
                $review[$i]['profile_pic'] = $this->getProfilePicture($rating['user_id'], 'profilepicture');
                $review[$i]['content'] = substr($rating['review'], 0, 50);
                $review[$i]['user_name'] = $rating['display_name'];
                $review[$i]['date'] = Yii::app()->common->YYMMDD($rating['created_date']);
                $i++;
            }
            $data['code'] = 200;
            $data['msg'] = 'OK';
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = count($ratings);
            $data['review'] = $review;
        }else {
            $data['code'] = 414;
            $data['status'] = "Invalid Content";
            $data['msg'] = "Content permalink is required!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public Savereview()
     * @author Manas Ranjan Sahoo <manas@muvi.com>
     * @return json message of success or failure 
     * @param int $content_id
     * @param int $rating
     * @param string $review_comment 
     * @param int $user_id
     */
    public function actionSavereview() {
        if (isset($_REQUEST['content_id']) && $_REQUEST['content_id'] > 0 && $_REQUEST['user_id'] > 0) {
            $rating = new ContentRating();
            $rate = $rating->findAllByAttributes(array('studio_id' => $this->studio_id, 'content_id' => $_REQUEST['content_id'], 'user_id' => $_REQUEST['user_id']));
            if (count($rate) > 0) {
                $data['code'] = 456;
                $data['status'] = "Already Reviewed";
                $data['msg'] = "Your have already added your review to this content.";
            } else {
                if (isset($_REQUEST['rating']) && ($_REQUEST['rating'] > 0) && ($_REQUEST['rating'] < 5)) {
                    $rating->content_id = $_REQUEST['content_id'];
                    $rating->rating = round($_REQUEST['rating'], 1);
                    $rating->review = nl2br(addslashes($_REQUEST['review_comment']));
                    $rating->created_date = new CDbExpression("NOW()");
                    $rating->studio_id = $this->studio_id;
                    $rating->user_ip = Yii::app()->getRequest()->getUserHostAddress();
                    $rating->user_id = $_REQUEST['user_id'];
                    $rating->status = 1;
                    $rating->save();
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['msg'] = "Your review is saved successfully.";
                } else {
                    $data['code'] = 455;
                    $data['status'] = "Failure";
                    $data['msg'] = "Please rate the content , and should be within 0 to 5";
                }
            }
        } else {
            $data['code'] = 406;
            $data['status'] = "Invalid Login Details";
            $data['msg'] = "Ensure you are logged in and you have filled all fields.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method private searchData() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns details of the content
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionsearchData() {
        if (isset($_REQUEST['q']) && $_REQUEST['q']) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate   = $this->getTransData($lang_code, $this->studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            if($language_id == 20){
                $langcond = ' AND (F.language_id = ' . $language_id . ' AND M.episode_language_id = ' . $language_id . ') ';
            }else{
                $langcond = ' AND (F.language_id = ' . $language_id . ' AND (IF (M.is_episode = 0, M.episode_language_id = 20 , M.episode_language_id=' . $language_id . '))) ';
            }
            $q = strtolower($_REQUEST['q']);
            $casts = Yii::app()->db->createCommand()
                    ->select('id,parent_id,language_id')
                    ->from('celebrities')
                    ->where('studio_id=:studio_id AND language_id=:language_id AND LOWER(name) LIKE  :search_string ', array(':studio_id' => $this->studio_id, ':search_string' => "%$q%", ':language_id' => $language_id))
                    ->queryAll();
            $cast_ids = array();
            $inCond = '';
            if (!empty($casts)) {
                foreach ($casts as $cast) {
                    if ($cast['parent_id'] > 0) {
                        $cast_ids[] = $cast['parent_id'];
                    } else {
                        $cast_ids[] = $cast['id'];
                    }
                }
            }
            if (!empty($cast_ids)) {
                $cast_ids = implode(',', $cast_ids);
                $movieIds = Yii::app()->db->createCommand()
                        ->select('GROUP_CONCAT(DISTINCT mc.movie_id) AS movie_ids')
                        ->from('movie_casts mc')
                        ->where('mc.celebrity_id IN (' . $cast_ids . ') ')
                        ->queryAll();
                if (@$movieIds[0]['movie_ids']) {
                    $movie_ids = $movieIds[0]['movie_ids'];
                    if ($language_id == 20) {
                        $film = Yii::app()->db->createCommand()
                                ->select('GROUP_CONCAT(DISTINCT f.id) AS movie_ids')
                                ->from('films f')
                                ->where('f.id IN (' . $movie_ids . ')')
                                ->queryAll();
                    } else {
                        $film = Yii::app()->db->createCommand()
                                ->select('GROUP_CONCAT(DISTINCT f.id) AS movie_ids')
                                ->from('films f')
                                ->where('f.parent_id IN (' . $movie_ids . ') AND language_id=' . $language_id)
                                ->queryAll();
                    }                   
                    if (!empty($film)) {
                        if (@$film[0]['movie_ids'])
                            $inCond = " OR F.id IN(" . $film[0]['movie_ids'] . ") ";
                    }
                }
            }
            //Build the search query 
            $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            $forRokuCond = '';
            if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                $forRokuCond = " AND (F.content_types_id =4 OR (IF( F.content_types_id =3 AND M.is_episode=0, M.is_converted =0 , M.is_converted =1 AND M.full_movie !=''))) "; 
            }
            $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
            $signedFolderPath = $folderPath['signedFolderPath'];
            $video_url = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/";

            $permalink = Yii::app()->getBaseUrl(TRUE) . '/';
            /* $command = Yii::app()->db->createCommand()
              ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration')
              ->from('movie_streams M,films F')
              ->where('M.movie_id = F.id AND  M.studio_id=:studio_id AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id))
              ->order('F.name,M.episode_title')
              ->limit($limit, $offset); */
            if ($_REQUEST['prev_dev']) {
                $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url')
                        ->from('movie_streams M,films F')
                        ->where('M.movie_id = F.search_parent_id '. $langcond .' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND   M.studio_id=:studio_id AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id))
                        ->order('F.name,M.episode_title')
                        ->limit($limit, $offset);
            } else {
                /* Add Geo block to listing by manas@muvi.com */
                if (isset($_REQUEST['country']) && $_REQUEST['country']) {
                    $country = $_REQUEST['country'];
                } else {
                    $visitor_loc = Yii::app()->common->getVisitorLocation();
                    $country = $visitor_loc['country'];
                }
                $sql_data1 = 'SELECT F.id as film_id,M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,F.parent_id,F.language_id,M.episode_parent_id,M.episode_language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url'
                        . ' FROM movie_streams M,films F WHERE '
                        . ' M.movie_id = F.search_parent_id  '. $langcond .' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND M.studio_id=' . $this->studio_id . ' AND (F.name LIKE \'%' . $q . '%\' OR M.episode_title LIKE \'%' . $q . '%\'' . $inCond . ' ) ' . $forRokuCond;
                $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $this->studio_id . " AND sc.country_code='{$country}'";
                $sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1 . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY P.name,P.episode_title LIMIT " . $limit . " OFFSET " . $offset;
                $command = Yii::app()->db->createCommand($sql_data);
                /* END */
            }
			if($this->studio_id == 3060 || $this->studio_id == 3755){
				$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/api.log', "a+");
				fwrite($fp, " \n\n\t Date:- " . date('dS M Y h:i A') . " \tSql: " .$sql_data);
				fclose($fp);
			}
            $list = $command->queryAll();
            $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();

            if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                //Retrive the total counts based on deviceType 
                $countQuery = Yii::app()->db->createCommand()
                        ->select('COUNT(*) as cnt')
                        ->from('movie_streams M,films F')
                        ->where('M.movie_id = F.search_parent_id  '. $langcond .' AND M.studio_id=:studio_id AND (F.content_types_id =4 OR (IF( F.content_types_id =3 AND M.is_episode=0, M.is_converted =0 , M.is_converted =1 AND M.full_movie !=\'\'))) AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id));
                $itemCount = $countQuery->queryAll();
                $item_count = @$itemCount[0]['cnt'];
                $newList = array();
                foreach ($list AS $k => $v) {
                    /* if($v['content_types_id'] != 4){ 
                      $is_convertedQuery = "SELECT * from movie_streams where is_converted =1 and full_movie !='' and movie_id=" . $v['movie_id'];
                      $count_Is_converted = Yii::app()->db->createCommand($is_convertedQuery)->execute();
                      if ($count_Is_converted > 0) {
                      $newList[] = $list[$k];
                      }
                      } */

                    if ($v['content_types_id'] == 3 && $v['is_episode'] == 0) {
                        $epSql = " SELECT id FROM movie_streams where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1  LIMIT 1";

                        $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                        if (count($epData)) {
                            $newList[] = $list[$k];
                        }
                        //Added by prakash on 1st Feb 2017
                    } else if ($v['content_types_id'] == 4) {
                        $epSql = " SELECT id FROM livestream where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . "  LIMIT 1";
                        $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                        if (count($epData)) {
                            $newList[] = $list[$k];
                        }
                    } else if (($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
                        $newList[] = $list[$k];
                    }
                }
                $list = array();
                $list = $newList;
            }
            if ($list) {
                $view = array();
                $cnt = 1;
                $movieids = '';
                $stream_ids = '';
                $domainName = $this->getDomainName();
                foreach ($list AS $k => $v) {
                    $list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0)?$v['is_episode']:0;
                    $list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
                    if($v['episode_parent_id'] > 0){
                        $v['movie_stream_id'] = $v['episode_parent_id'];
                        $list[$k]['movie_stream_id'] = $v['episode_parent_id'];
                    }
                    if ($v['is_episode']) {
                        $stream_ids .="'" . $v['movie_stream_id'] . "',";
                    } else {
                        $movieids .="'" . $v['movie_id'] . "',";
                        if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                            $director = '';
                            $actor = '';
                            $castsarr = $this->getCasts($v['movie_id'], '', $language_id, $this->studio_id);
                            if ($castsarr) {
                                $actor = $castsarr['actor'];
                                $actor = trim($actor, ',');
                                unset($castsarr['actor']);
                                $director = $castsarr['director'];
                                unset($castsarr['director']);
                            }
                            $list[$k]['actor'] = @$actor;
                            $list[$k]['director'] = @$director;
                            $list[$k]['cast_detail'] = @$castsarr;
                        }
                    }
                    if ($v['content_types_id'] == 4) {
                        $liveTV[] = $v['movie_id'];
                    }
                    if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                        if (@$v['thirdparty_url'] != "") {
                            $info = pathinfo(@$v['thirdparty_url']);
                            if (@$info["extension"] == "m3u8") {
                                $list[$k]['movieUrlForTV'] = @$v['thirdparty_url'];
                            }
                        } else {
                            $list[$k]['movieUrlForTV'] = @$v['video_url'];
                        }
                    }
                    if ($v['thirdparty_url'] != "") {
                        $list[$k]['thirdparty_url'] = $this->getSrcFromThirdPartyUrl($v['thirdparty_url']);
                    }
                }
                $viewcount = array();
                $movie_ids = rtrim($movieids, ',');
                $stream_ids = rtrim($stream_ids, ',');


                if ($movie_ids) {
                    $cast = MovieCast::model()->findAll(array("condition" => "movie_id  IN(" . $movie_ids . ')'));
                    foreach ($cast AS $key => $val) {
                        if (in_array('actor', json_decode($val['cast_type']))) {
                            $actor[$val['movie_id']] = @$actor[$val['movie_id']] . $val['celebrity']['name'] . ', ';
                        }
                    }
                    $psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movie_ids . ") AND (object_type='films' OR object_type='tvapp') ";
                    $posterData = Yii::app()->db->createCommand($psql)->queryAll();
                    if ($posterData) {
                        $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
                        foreach ($posterData AS $key => $val) {
                            $size = 'thumb';
                            if ($val['object_type'] == 'films') {
                                $posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/' . $size . '/' . urlencode($val['poster_file_name']);
                                $posters[$val['movie_id']] = $posterUrl;
                                if (($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
                                    $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
                                }
                            } else if (($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')) {
                                $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
                            }
                        }
                    }
                }
                $default_epp = POSTER_URL . '/' . 'no-image-h.png';
                $default_p = POSTER_URL . '/' . 'no-image-a.png';
                if (@$liveTV) {
                    $lcriteria = new CDbCriteria();
                    $lcriteria->select = "movie_id,IF(feed_type=1,'hls','rtmp') feed_type,feed_url,feed_method";
                    $lcriteria->addInCondition("movie_id", $liveTV);
                    $lcriteria->condition = "studio_id = $this->studio_id";
                    $lresult = Livestream::model()->findAll($lcriteria);
                    foreach ($lresult AS $key => $val) {
                        $liveTvs[$val->movie_id]['feed_url'] = $val->feed_url;
                        $liveTvs[$val->movie_id]['feed_type'] = $val->feed_type;
                        $liveTvs[$val->movie_id]['feed_method'] = $val->feed_method;
                    }
                }

                $is_payment_gateway_exist = 0;
                $payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($payment_gateway) && !empty($payment_gateway)) {
                    $is_payment_gateway_exist = 1;
                }

                //Get view status for content other then episode
                $viewStatus = VideoLogs::model()->getViewStatus($movie_ids, $this->studio_id);

                if ($viewStatus) {
                    $viewStatusArr = '';
                    foreach ($viewStatus AS $key => $val) {
                        $viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount'];
                        $viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount'];
                    }
                }
                //Get view status for Episodes 
                $viewEpisodeStatus = VideoLogs::model()->getEpisodeViewStatus($stream_ids, $this->studio_id);
                if ($viewEpisodeStatus) {
                    $viewEpisodeStatusArr = '';
                    foreach ($viewEpisodeStatus AS $key => $val) {
                        $viewEpisodeStatusArr[$val['video_id']]['viewcount'] = $val['viewcount'];
                        $viewEpisodeStatusArr[$val['video_id']]['uniq_view_count'] = $val['u_viewcount'];
                    }
                }
                $default_status_view = array('viewcount' => "0", 'uniq_view_count' => "0");
                foreach ($list AS $key => $val) {
                    $casts = '';
                    if ($val['is_episode'] == 1) {
                        if ($language_id != 20 && $val['episode_parent_id'] > 0) {
                            $val['movie_stream_id'] = $val['episode_parent_id'];
                        }
                        //Get Posters for Episode
                        $posterUrlForTv = $this->getPoster($val['movie_stream_id'], 'moviestream', 'roku', $this->studio_id);
                        if ($posterUrlForTv != '') {
                            $list[$key]['posterForTv'] = $posterUrlForTv;
                        }
                        $poster_url = $this->getPoster($val['movie_stream_id'], 'moviestream', 'episode', $this->studio_id);
                        $list[$key]['viewStatus'] = @$viewEpisodeStatusArr[$val['movie_stream_id']] ? @$viewEpisodeStatusArr[$val['stream_id']] : $default_status_view;
                    } else {
                        if ($language_id != 20 && $val['parent_id'] > 0) {
                            $val['movie_id'] = $val['parent_id'];
                        }
                        $poster_url = $posters[$val['movie_id']] ? $posters[$val['movie_id']] : $default_p;
                        if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {
                            $poster_url = str_replace('/thumb/', '/episode/', $poster_url);
                        }
                        $casts = $actor[$val['movie_id']] ? rtrim($actor[$val['movie_id']], ', ') : '';
                        $list[$key]['viewStatus'] = @$viewStatusArr[$val['movie_id']] ? @$viewStatusArr[$val['movie_id']] : $default_status_view;
                        if (isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != '') {
                            $list[$key]['posterForTv'] = $postersforTv[$val['movie_id']];
                        }
                    }
                    $list[$key]['poster_url'] = $poster_url;
                    if ($val['content_types_id'] == 4) {
                        $list[$key]['feed_type'] = $liveTvs[$val['movie_id']]['feed_type'];
                        $list[$key]['feed_url'] = $liveTvs[$val['movie_id']]['feed_url'];
                        $list[$key]['feed_method'] = $liveTvs[$val['movie_id']]['feed_method'];
                    }
                    $list[$key]['actor'] = $casts;

                    $arg['studio_id'] = $this->studio_id;
                    $arg['movie_id'] = $v['movie_id'];
                    $arg['season_id'] = 0;
                    $arg['episode_id'] = 0;
                    $arg['content_types_id'] = $v['content_types_id'];

                    $isFreeContent = Yii::app()->common->isFreeContent($arg);
                    $list[$key]['isFreeContent'] = $isFreeContent;

                    //Checking PPV set or advance set or not
                    $is_ppv = $is_advance = 0;
                    if ((intval($isFreeContent) == 0) && ($is_payment_gateway_exist == 1)) {
                        if ($v['is_converted'] == 1) {
                            $ppv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id, 0);
                            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                $is_ppv = 1;
                            } else {
                                $ppv_plan = Yii::app()->common->getContentPaymentType($v['content_types_id'], $v['ppv_plan_id'], $this->studio_id);
                                $is_ppv = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? 1 : 0;
                            }
                        } else {
                            $adv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id);
                            $is_advance = (isset($adv_plan->id) && intval($adv_plan->id)) ? 1 : 0;
                        }
                    }

                    $list[$key]['is_ppv'] = $is_ppv;
                    $list[$key]['is_advance'] = $is_advance;
                }
                $list[$key]['adDetails'] = array();
                if ($val['rolltype'] == 1) {
                    $list[$key]['adDetails'][] = 0;
                } elseif ($val['rolltype'] == 2) {
                    $roleafterarr = explode(',', $val['roll_after']);
                    foreach ($roleafterarr AS $tkey => $tval) {
                        $list[$key]['adDetails'][] = $this->convertTimetoSec($tval);
                    }
                } elseif ($val['rolltype'] == 3) {
                    $list[$key]['adDetails'][] = $this->convertTimetoSec($val['video_duration']);
                }
                /* if(isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku'){
                  $newList = array();
                  foreach ($list AS $k => $v) {
                  if(($content_types_id != 4) && ($v['movieUrlForTV'] != "")){
                  $newList[] = $list[$k];
                  }
                  }
                  $list = array();
                  $list = $newList;
                  } */
            }


            $searchLogs = new SearchLog();
            $newlisting = array();
            $listings = $list;
            if (!empty($listings)) {
                foreach ($listings as $listnew) {
                    $searchLogs->setIsNewRecord(true);
                    $searchLogs->id = null;
                    $searchLogs->studio_id = $this->studio_id;
                    $searchLogs->search_string = $_REQUEST['q'];
                    $searchLogs->object_category = "content";
                    $searchLogs->object_id = $lists['movie_id'];
                    $searchLogs->user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
                    $searchLogs->created_date = gmdate('Y-m-d H:i:s');
                    $searchLogs->ip = $_SERVER['REMOTE_ADDR'];
                    $searchLogs->save();
                    $content_id = $list_id;
                    $is_episode = $listnew['is_episode'];
                    if ($is_episode == 1) {
                        $content_id = $stream_id;
                    }
                }
            } else {
                $searchLogs->studio_id = $this->studio_id;
                $searchLogs->search_string = $_REQUEST['q'];
                $searchLogs->user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
                $searchLogs->created_date = gmdate('Y-m-d H:i:s');
                $searchLogs->ip = $_SERVER['REMOTE_ADDR'];
                $searchLogs->save();
            }
            $data['code'] = 200;
            $data['msg'] = $translate['btn_ok'];
            $data['search'] = $list;
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = $item_count;
            $data['Ads'] = $this->getStudioAds();
        } else {
            $data['code'] = 416;
            $data['status'] = "Search Parameter required";
            $data['msg'] = $translate['search_parameter_required'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method private forgotPassword() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns array of message and response code based on the action success/failure
     * @param string $email	Valid email of the user
     * @param string $oauthToken Auth token
     * 
     */
    public function actionforgotPassword() {
        $studio_id = $this->studio_id;
        $language_id = 20;
        if (isset($_REQUEST) && count($_REQUEST) > 0 && $_REQUEST['email'] && $_REQUEST['email'] != '') {
            $email = trim($_REQUEST['email']);
            $user = SdkUser::model()->find('studio_id=:studio_id AND email=:email', array(':studio_id'=>$this->studio_id, ':email'=>$email));
            $studio = Studio::model()->findByPk($this->studio_id);
            if ($user) {
                $user_id = $user->id;
                $to = array($email);
                $enc = new bCrypt();
                $reset_tok = $enc->hash($email);
                $user->reset_password_token = $reset_tok;
                $user->save();

                $user_name = $user->display_name;
                $to_name = $user_name;
                $from = $studio->contact_us_email;
                $from_name = $studio->name;
                $site_url = 'http://' . $studio->domain;
                $siteLogo = $logo_path = Yii::app()->common->getLogoFavPath($this->studio_id);
                $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" /></a>';
                $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';
                if ($studio->fb_link != '')
                    $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
                else
                    $fb_link = '';
                if ($studio->tw_link != '')
                    $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
                else
                    $twitter_link = '';
                if ($studio->gp_link != '')
                    $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';
                else
                    $gplus_link = '';

                $reset_link = $site_url . '/user/resetpassword?user_id=' . $user_id . '&auth=' . $reset_tok . '&email=' . $email;
                $rlink = '<a href="' . $reset_link . '">' . $reset_link . '</a>';

                $studio_email = Yii::app()->common->getStudioEmail($this->studio_id);
                $subject = 'Reset password for your ' . $studio->name . ' account';
                $command = Yii::app()->db->createCommand()
                    ->select('display_name,studio_id')
                    ->from('sdk_users u')
                    ->where(' u.status = 1 AND u.email = "' .$email.'" AND studio_id='.$this->studio_id);
                $dataem = $command->queryAll();
                $FirstName = $dataem[0]['display_name'];
                $link = $rlink;
                $StudioName = $studio->name;
                $EmailAddress = $studio_email;
                $email_type = 'forgot_password';
                $content = NotificationTemplates::model()->find('studio_id=:studio_id AND type=:type', array(':studio_id'=>$this->studio_id, ':type'=>$email_type));                
                if ($content) {                
                } else {
                    $content = NotificationTemplates::model()->find('studio_id=:studio_id AND type=:type', array(':studio_id'=>0, ':type'=>$email_type));
                }
                $temps = $content;
                //Subject
                $subject = $temps['subject'];
                eval("\$subject = \"$subject\";");
                $content = (string) $temps["content"];
                $breaks = array("<br />", "<br>", "<br/>");
                $content = str_ireplace($breaks, "\r\n", $content);
                $content = htmlentities($content);
                eval("\$content = \"$content\";");
                $content = htmlspecialchars_decode($content);
                
                $params = array('website_name' => $studio->name,
                    'logo' => $logo,
                    'site_link' => $site_link,
                    'reset_link' => $rlink,
                    'username' => $user_name,
                    'fb_link' => @$fb_link,
                    'twitter_link' => @$twitter_link,
                    'gplus_link' => @$gplus_link,
                    'supportemail' => $studio_email,
                    'website_address' => $studio->address,
                    'content'=> $content
                );
                $template_name = 'sdk_reset_password_user';

                Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/' . $template_name, array('params' => $params), true);
                $return_param = $this->sendmailViaAmazonsdk($thtml, $subject, $user->email, $EmailAddress,"","","",$StudioName);                //$return_param = $this->mandrilEmail($template_name, $params, $message);
                $response = 'success';
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['msg'] = "Please check your email to reset your password.";
            }else {

                $data['code'] = 418;
                $data['status'] = "Email didn't exists with us! ";
                $data['msg'] = "Email you have entered is not present!";
            }
        } else {
            $data['code'] = 417;
            $data['status'] = "A valid Email required";
            $data['msg'] = "Please provide a valid Email!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getStudioPlanLists() It will check if plan exists for a studio. If exists then return the plan lists
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     */
    public function actionGetStudioPlanLists() {
        $studio_id = $this->studio_id;
        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
        $country = @$_REQUEST['country'];
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        
        $ret = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id, $default_currency_id, $country, $language_id);
        $data['code'] = 200;
        $data['status'] = "OK";
        if ($ret && !empty($ret)) {
            
            if (isset($ret['plans']) && !empty($ret['plans'])) {
                $plans = $ret['plans'];
                $default_plan_id = @$plans[0]->id;
                
                foreach ($plans AS $key => $val) {
                    if ($val->is_default) {
                        $default_plan_id = $val->id;
                    }
                        
                    $plan[$key] = $val->attributes;
                    $plan[$key]['price'] = $val->price;

                    (array) $currency = Currency::model()->findByPk($val->currency_id);
                    $plan[$key]['currency'] = isset($currency->attributes) && !empty($currency->attributes) ? $currency->attributes : array();
                }
                $data['plans'] = $plan;
                $data['default_plan'] = $default_plan_id;
            } else {
                $data['code'] = 457;
                $data['plans'] = '';
                $data['gateways'] = '';
                $data['msg'] = "No subscription plan found!";
            }
            //$paymentGate = $ret['gateways'];
            //echo "<pre>";print_r($paymentGate);exit;
            //$data['gateways'] = $paymentGate[0]->attributes;
            //$data['msg'] = "Payment is enabled & plan exists!";
        } else {
            $data['code'] = 424;
            $data['plans'] = '';
            $data['gateways'] = '';
            $data['msg'] = "Payment gateway is not enabled for this studio.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public authUserPaymentInfo() It will users card details and return proper message
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param string $nameOnCard Name as on Credit card
     * @param String $cvv CVV as on card
     * @param String $expiryMonth Expiry month as on Card
     * @param String $expiryYear Expiry Year as on Card
     * @param String $cardNumber Card Number 
     * @param String $email user email : Specifically require for stripe
     */
    public function actionAuthUserPaymentInfo() {
        $data = '';
        $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
        if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
            $gateway_code = $plan_payment_gateway['gateways'][0]->short_code;
            $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
        } else {
            $res['status'] = 'Failure';
            $res['code'] = 419;
            $res['msg'] = 'No active payment gateway';
            $data = json_encode($res);
        }

        if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
            $_REQUEST['card_name'] = $_REQUEST['nameOnCard'];
            $_REQUEST['card_number'] = $_REQUEST['cardNumber'];
            $_REQUEST['exp_month'] = ltrim((string) $_REQUEST['expiryMonth'], '0');
            $_REQUEST['exp_year'] = $_REQUEST['expiryYear'];

            $_REQUEST['isAPI'] = 1; //Only for stripe

            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::processCard($_REQUEST);
            
            $res = json_decode($data, true);
            if (isset($res['isSuccess']) && intval($res['isSuccess'])) {
                if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypalpro') {
                    if (isset($_REQUEST['plan_id']) && intval($_REQUEST['plan_id'])) {
                        //$default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
                        $studioData = Yii::app()->db->createCommand("SELECT default_currency_id FROM studios WHERE id = {$this->studio_id}")->queryRow();
						$default_currency_id = $studioData['default_currency_id'];
						
                        $plan_details = SubscriptionPlans::model()->getPlanDetails($_REQUEST['plan_id'],$this->studio_id, $default_currency_id, $_REQUEST['country']);
                        if (isset($plan_details) && !empty($plan_details) && isset($plan_details['trial_period']) && intval($plan_details['trial_period']) == 0) {
                            $price = $plan_details['price'];
                            $currency_id = $plan_details['currency_id'];
                            $currency = Currency::model()->findByPk($currency_id);
                            
                            //Prepare an array to charge from given card
                            $user['currency_id'] = $currency->id;
                            $user['currency_code'] = $currency->code;
                            $user['currency_symbol'] = $currency->symbol;
                            $user['amount'] = $price;
                            $user['token'] = @$res['card']['token'];
                            $user['profile_id'] = @$res['card']['profile_id'];
                            $user['card_holder_name'] = $_REQUEST['card_name'];
                            $user['card_type'] = @$res['card']['card_type'];
                            $user['exp_month'] = $_REQUEST['exp_month'];
                            $user['exp_year'] = $_REQUEST['exp_year'];
                            $user['studio_name'] = Studios::model()->findByPk($this->studio_id)->name;
                            $trans_data = $payment_gateway::processTransactions($user);
                            
                            if (intval($trans_data['is_success'])) {
                                $data = json_decode($data, true);
                                
                                $data['transaction_status'] = $trans_data['transaction_status'];
                                $data['transaction_invoice_id'] = $trans_data['invoice_id'];
                                $data['transaction_order_number'] = $trans_data['order_number'];
                                $data['transaction_dollar_amount'] = $trans_data['dollar_amount'];
                                $data['transaction_amount'] = $trans_data['amount'];
                                $data['transaction_response_text'] = $trans_data['response_text'];
                                $data['transaction_is_success'] = $trans_data['is_success'];
                                
                                $data = json_encode($data);
                            } else {
                                $data = json_encode($trans_data);
                            }
                        }
                    }
                }
            }
        } else {
            $res['status'] = 'Failure';
            $res['code'] = 419;
            $res['msg'] = 'No active payment gatway';
            $data = json_encode($res);
        }

        $data1 = json_decode($data, TRUE);
        $this->setHeader(@$data1['code'] == 100 ? 200 : @$data1['code']);
        echo json_encode($data1);
        exit;
    }

    /**
     * @method public registerUser() Register the user's details 
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param string $nameOnCard Name as on Credit card
     * @param String $cvv CVV as on card
     * @param String $expiryMonth Expiry month as on Card
     * @param String $expiryYear Expiry Year as on Card
     * @param String $cardNumber Card Number 
     * @param String $name Full Name of the User
     * @param String $email Email address of the User
     * @param String $password Login Password 
     * @param String $planId Login Password 
     * @param String $card_last_fourdigit Last 4 digits of the card 
     * @param String $auth_num Authentication Number
     * @param String $token Payment Token
     * @param String $cardType Type of the card
     * @param String $referenceNo Reference Number if any
     * @param String $response_text Response text as returned by Payment gateways
     * @param String $status Status of the payment gateways
     * @param String $profileId User Profile id created in the payment gateways
     * 
     */
    public function actionRegisterUser() {
        $_REQUEST['data'] = $_REQUEST;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            if ($this->actionCheckEmailExistance($_REQUEST)) {
                $studio_id = $this->studio_id;
                $name = trim($_REQUEST['data']['name']);
                $email = trim($_REQUEST['data']['email']);
                $password = $_REQUEST['data']['password'];

                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                
                $gateway_code = '';
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                }

                $_REQUEST['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
                $_REQUEST['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
                $_REQUEST['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];
                
                if (isset($_REQUEST['transaction_is_success']) && intval($_REQUEST['transaction_is_success'])) {
                    $_REQUEST['data']['transaction_data']['transaction_status'] = $_REQUEST['transaction_status'];
                    $_REQUEST['data']['transaction_data']['invoice_id'] = $_REQUEST['transaction_invoice_id'];
                    $_REQUEST['data']['transaction_data']['order_number'] = $_REQUEST['transaction_order_number'];
                    $_REQUEST['data']['transaction_data']['dollar_amount'] = $_REQUEST['transaction_dollar_amount'];
                    $_REQUEST['data']['transaction_data']['amount'] = $_REQUEST['transaction_amount'];
                    $_REQUEST['data']['transaction_data']['response_text'] = $_REQUEST['transaction_response_text'];
                    $_REQUEST['data']['transaction_data']['is_success'] = $_REQUEST['transaction_is_success'];
                }
                
                $ret = SdkUser::model()->saveSdkUser($_REQUEST, $studio_id);

                if ($ret) {
                    if (@$ret['error']) {
                        $data['code'] = 420;
                        $data['status'] = 'Failure';
                        $data['msg'] = 'Invalid Plan!';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    }
                    // Send welcome email to user
                    if (@$ret['is_subscribed']) {
                        $file_name = '';
                        
                        if (isset($_REQUEST['data']['transaction_data']['is_success']) && intval($_REQUEST['data']['transaction_data']['is_success'])) {
                            $studio = Studios::model()->findByPk($studio_id);
                            $user['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
                            $user['card_holder_name'] = $_REQUEST['data']['card_name'];
                            $user['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];
                            
                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $_REQUEST['data']['transaction_data']['invoice_id']);
                        }
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email_with_subscription', $file_name);
                    } else {
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email');
                    }
                    $_REQUEST['sdk_user_id'] = $ret['user_id'];
                   /* if ($_REQUEST['livestream'] == 1) {
                        $this->registerStreamUser($_REQUEST);
                    }*/
                    $this->actionLogin($_REQUEST);
                } else {
                    $data['code'] = 421;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'Error in registration';
                    $this->setHeader($data['code']);
                    echo json_encode($data);
                    exit;
                }
            } else {
                $data['code'] = 421;
                $data['status'] = 'Failure';
                $data['msg'] = 'Error in registration';
                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            }
        }
    }
    
    //multipart user regiistration part-1 start
    function actionRegisterUserDetails(){
        $_REQUEST['data'] = $_REQUEST;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            if ($this->actionCheckEmailExistance($_REQUEST)){
                $studio_id = $this->studio_id;
                $name = trim($_REQUEST['data']['name']);
                $email = trim($_REQUEST['data']['email']);
                $password = $_REQUEST['data']['password'];

                $ret = SdkUser::model()->saveUserDetails($_REQUEST, $studio_id);
                if ($ret) {
                    if (@$ret['error']) {
                        $data['code'] = 420;
                        $data['status'] = 'Failure';
                        $data['msg'] = 'Invalid Plan!';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    }else{
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email');
                    }
                    $_REQUEST['sdk_user_id'] = $ret['user_id'];
                    if ($_REQUEST['livestream'] == 1) {
                        $this->registerStreamUser($_REQUEST);
                    }
                    $this->actionLogin($_REQUEST);
                }else {
                    $data['code'] = 421;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'Error in registration';
                    $this->setHeader($data['code']);
                    echo json_encode($data);
                    exit;
                }
            }else {
                $data['code'] = 421;
                $data['status'] = 'Failure';
                $data['msg'] = 'Error in registration';
                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            }
        }
    }
    //multipart user registration part-1 end
    
    //multipart user registration part-2 start
    function actionRegisterUserPayment(){
        $_REQUEST['data'] = $_REQUEST;
        $studio_id = $this->studio_id;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $user = new SdkUser;
            $users = $user->findAllByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $studio_id));
            if (isset($users) && !empty($users)) {
                
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                
                $gateway_code = '';
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                }

                $_REQUEST['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
                $_REQUEST['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
                $_REQUEST['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];
                
                if (isset($_REQUEST['transaction_is_success']) && intval($_REQUEST['transaction_is_success'])) {
                    $_REQUEST['data']['transaction_data']['transaction_status'] = $_REQUEST['transaction_status'];
                    $_REQUEST['data']['transaction_data']['invoice_id'] = $_REQUEST['transaction_invoice_id'];
                    $_REQUEST['data']['transaction_data']['order_number'] = $_REQUEST['transaction_order_number'];
                    $_REQUEST['data']['transaction_data']['dollar_amount'] = $_REQUEST['transaction_dollar_amount'];
                    $_REQUEST['data']['transaction_data']['amount'] = $_REQUEST['transaction_amount'];
                    $_REQUEST['data']['transaction_data']['response_text'] = $_REQUEST['transaction_response_text'];
                    $_REQUEST['data']['transaction_data']['is_success'] = $_REQUEST['transaction_is_success'];
                }
                $ret = SdkUser::model()->saveUserPayment($_REQUEST, $studio_id);
                
                if ($ret) {
                    if (@$ret['error']) {
                        $data['code'] = 420;
                        $data['status'] = 'Failure';
                        $data['msg'] = 'Invalid Plan!';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    }
                    // Send welcome email to user
                    if (@$ret['is_subscribed']) {
                        
                        $file_name = '';
                        if (isset($_REQUEST['data']['transaction_data']['is_success']) && intval($_REQUEST['data']['transaction_data']['is_success'])) {
                            $studio = Studios::model()->findByPk($studio_id);
                            $user['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
                            $user['card_holder_name'] = $_REQUEST['data']['card_name'];
                            $user['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];
                            
                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $_REQUEST['data']['transaction_data']['invoice_id']);
                        }
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email_with_subscription', $file_name);
                        $admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $ret['user_id'], '', 0);
                        
                        $data['code'] = 200;
                        $data['msg'] = 'OK';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    }
                    
                }else {
                    $data['code'] = 421;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'Error in processing payment!';
                    $this->setHeader($data['code']);
                    echo json_encode($data);
                    exit;
                }
            }else {
                $data['code'] = 421;
                $data['status'] = 'Failure';
                $data['msg'] = 'Error in processing payment!';
                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            }
        }
    }
    //multipart user registration part-2 end

    /**
     * @method public registerstreamUser($request) It will register the stream user and create a channel id
     * @return json Return the json array of results
     * @author Gayadhar<support@muvi.com>
     */
    function registerStreamUser($request) {
        $lsuser = new LivestreamUsers;
        $lsuser->name = $request['name'];
        $lsuser->nick_name = @$request['nick_name']?@$request['nick_name']:$request['name'] ;
        $lsuser->email = $request['email'];
        $lsuser->sdk_user_id = $request['sdk_user_id'];
        $lsuser->studio_id = $this->studio_id;
        $lsuser->ip = Yii::app()->request->getUserHostAddress();
        $lsuser->status = 1;
        $lsuser->created_date = new CDbExpression("NOW()");
        $return = $lsuser->save();
        $lsuser_id = $lsuser->id;
        $push_url = RTMP_URL . $this->studio_id . '/' . $lsuser->channel_id;
        $hls_path = HLS_PATH . $this->studio_id;
        if (!is_dir($hls_path)) {
            mkdir($hls_path, 0777, TRUE);
            chmod($hls_path, 0777);
        }
        if (!is_dir($hls_path . '/' . $lsuser->channel_id)) {
            mkdir($hls_path . '/' . $lsuser->channel_id, 0777, TRUE);
            chmod($hls_path . '/' . $lsuser->channel_id, 0777);
        }
        $hls_path = $hls_path . '/' . $lsuser->channel_id;
        $hls_url = HLS_URL . $this->studio_id . '/' . $lsuser->channel_id . '/' . $lsuser->channel_id . '.m3u8';
        $lsuser->pull_url = $hls_url;
        $lsuser->push_url = $push_url;
        $lsuser->save();
        $_REQUEST['rtmp_path'] = $push_url;
        return true;
    }

    /**
     * @method public checkEmailExistance($email) It will check if an email exists with the studio or not
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param String $email Email to be validated
     */
    public function actionCheckEmailExistance($req = array()) {
        if ($req) {
            $_REQUEST = $req;
        }
        $arr['isExists'] = 0;
        $arr['code'] = 200;
        $arr['msg'] = 'OK';
        if (isset($_REQUEST) && isset($_REQUEST['email'])) {
            $user = new SdkUser;
            $users = $user->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id));
            if (isset($users) && !empty($users)) {
                $arr['isExists'] = 1;
                $arr['code'] = 422;
                $arr['msg'] = 'Email already exists for the studio';
            }
        } else {
            $arr['isExists'] = 0;
            $arr['code'] = 423;
            $arr['msg'] = '';
        }
        if ($req && $arr['code'] == 200) {
                return TRUE;
            }
            $this->setHeader($arr['code']);
            echo json_encode($arr);
            exit;
        }

    /**
     * @method private isPPVSubscribed() Check the ppv subscription taken by user or not
     * @author SMK<support@muvi.com>
     * @return json Returns the list of data in json format
     * @param string $movie_id uniq id of film or muvi
     * @param int $season_id season (optional)
     * @param string $episode_id uniq id of stream (optional)
     * @param string $purchase_type show/episode for multipart only (optional)
     * @param string $oauthToken Auth token
     * 
     */
    function actionIsPPVSubscribed() {
        if ($_REQUEST['movie_id'] && $_REQUEST['user_id']) {
            $studio_id = $this->studio_id;
            $user_id = $_REQUEST['user_id'];
            
            $lang_code   = @$_REQUEST['lang_code'];
            $translate   = $this->getTransData($lang_code,$studio_id);
            
            if (@$user_id) {
                $usercriteria = new CDbCriteria();
                $usercriteria->select = "is_developer";
                $usercriteria->condition = "id=$user_id";
                $userData = SdkUser::model()->find($usercriteria);
            }
            $data = array();

            $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
            $movie_id = Yii::app()->common->getMovieId($movie_code);

            $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
            $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }
            
            if (@$userData->is_developer) {
                $mov_can_see = 'allowed';
            } else {
                $can_see_data['movie_id'] = $movie_id;
                $can_see_data['season'] = @$season;
                $can_see_data['stream_id'] = $stream_id;
                $can_see_data['purchase_type'] = @$_REQUEST['purchase_type'];
                $can_see_data['studio_id'] = $studio_id;
                $can_see_data['user_id'] = $user_id;
                $can_see_data['country'] = @$_REQUEST['country'];
                
                $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
            }
            
            if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
                $data['code'] = 425;
                $data['status'] = "False";
                $data['msg'] = $translate['activate_subscription_watch_video'];
            } else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
                $data['code'] = 426;
                $data['status'] = "False";
                $data['msg'] = $translate['reactivate_subscription_watch_video'];
            } else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
                $data['code'] = 427;
                $data['status'] = "False";
                $data['msg'] = $translate['video_restiction_in_your_country'];
            } else if ($mov_can_see == 'advancedpurchased') {
                $data['code'] = 431;
                $data['status'] = "False";
                $data['msg'] = $translate['already_purchase_this_content'];
            } else if ($mov_can_see == 'access_period') {
                $data['code'] = 428;
                $data['status'] = "False";
                $data['msg'] = $translate['access_period_expired'];
            } else if ($mov_can_see == 'watch_period') {
                $data['code'] = 428;
                $data['status'] = "False";
                $data['msg'] = $translate['watch_period_expired'];
            } else if ($mov_can_see == 'maximum') {
                $data['code'] = 428;
                $data['status'] = "False";
                $data['msg'] = $translate['crossed_max_limit_of_watching'];
            } else if ($mov_can_see == 'already_purchased') {
                $data['code'] = 428;
                $data['status'] = "False";
                $data['msg'] = $translate['already_purchase_this_content'];
            } else if ($mov_can_see == 'allowed') {
                $data['code'] = 429;
                $data['status'] = "OK";
                $data['msg'] = 'You will be allow to watch this video.';
            } else if ($mov_can_see == 'unpaid') {
                $data['code'] = 430;
                $data['status'] = "False";
                $data['msg'] = 'Unpaid';

            $data['member_subscribed'] = 0;
            $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
            if ($is_subscribed) {
                $data['member_subscribed'] = 1;
            }
            }
        } else {
            $data['code'] = 411;
            $data['status'] = "Invalid Content Type";
            $data['msg'] = "Muvi id required";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }    
    
    function actionisContentAuthorized() {
        if ($_REQUEST['movie_id'] && $_REQUEST['user_id']) {
            $studio_id = $this->studio_id;
            $user_id = $_REQUEST['user_id'];
            $lang_code   = @$_REQUEST['lang_code'];
            $translate   = $this->getTransData($lang_code,$studio_id);
            if (@$user_id) {
                $usercriteria = new CDbCriteria();
                $usercriteria->select = "is_developer";
                $usercriteria->condition = "id=$user_id";
                $userData = SdkUser::model()->find($usercriteria);
            }
            $data = array();

            $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
            $movie_id = Yii::app()->common->getMovieId($movie_code);

            $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
            $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }
            
            if (@$userData->is_developer) {
                $mov_can_see = 'allowed';
            } else {
                $can_see_data['movie_id'] = $movie_id;
                $can_see_data['season'] = @$season;
                $can_see_data['stream_id'] = $stream_id;
                $can_see_data['purchase_type'] = @$_REQUEST['purchase_type'];
                $can_see_data['studio_id'] = $studio_id;
                $can_see_data['user_id'] = $user_id;
                $can_see_data['country'] = @$_REQUEST['country'];
                
                $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
            }

            if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
                $data['code'] = 425;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['activate_subscription_watch_video'];
            } else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
                $data['code'] = 426;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['reactivate_subscription_watch_video'];
            } else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
                $data['code'] = 427;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['video_restiction_in_your_country'];
            } else if ($mov_can_see == 'advancedpurchased') {
                $data['code'] = 431;
                $data['status'] = "False";
                $data['msg'] = $translate['already_purchase_this_content'];
            } else if ($mov_can_see == 'access_period') {
                $data['code'] = 428;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['access_period_expired'];
            } else if ($mov_can_see == 'watch_period') {
                $data['code'] = 428;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['watch_period_expired'];
            } else if ($mov_can_see == 'maximum') {
                $data['code'] = 428;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['crossed_max_limit_of_watching'];
            } else if ($mov_can_see == 'already_purchased') {
                $data['code'] = 428;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['already_purchase_this_content'];
            } else if ($mov_can_see == 'allowed') {
                $data['code'] = 429;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "OK";
                $data['msg'] = 'You will be allow to watch this video.';
            } else if ($mov_can_see == 'unpaid') {
                $data['code'] = 430;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = 'Unpaid';

                $data['member_subscribed'] = 0;
                $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                if ($is_subscribed) {
                    $data['member_subscribed'] = 1;
                }
            }
        } else {
            $data['code'] = 411;
            $data['status'] = "Invalid Content Type";
            $data['msg'] = "Muvi id required";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionvalidateCouponCode() {
        $data = array();
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);
        
        $couponeDetails = Yii::app()->common->couponCodeIsValid($_REQUEST["couponCode"], $user_id, $studio_id, $_REQUEST["currencyId"]);
        if ($couponeDetails != 0) {
            if ($couponeDetails != 1) {
                $data['code'] = 432;
                $data['status'] = 'Valid';
                $data['msg'] = '';
                $data['discount'] = $couponeDetails['discount_amount'];
                if ($couponeDetails["discount_type"] == 1) {
                    $data['discount_type'] = '%';
                } else {
                    if ($_REQUEST["currencyId"] != 0) {
                        $currencyDetails = Currency::model()->findByPk($_REQUEST["currencyId"]);
                        $data['discount_type'] = $currencyDetails['symbol'];
                    } else {
                        $data['discount_type'] = '$';
                    }
                }
            } else {
                $data['code'] = 433;
                $data['status'] = 'Invalid';
                $data['msg'] = $translate['coupon_already_used'];
            }
        } else {
            $data['code'] = 433;
            $data['status'] = 'Invalid';
            $data['msg'] = 'Coupon code not valid.';
            $data['msg'] = $translate['invalid_coupon'];
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionPpvpayment() {
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);

        $data = array();
        $data['movie_id'] = @$_REQUEST['movie_id'];
        $data['season_id'] = @$_REQUEST['season_id'];
        $data['episode_id'] = @$_REQUEST['episode_id'];
        $data['card_holder_name'] = @$_REQUEST['card_name'];
        $data['card_number'] = @$_REQUEST['card_number'];
        $data['exp_month'] = @$_REQUEST['exp_month'];
        $data['exp_year'] = @$_REQUEST['exp_year'];
        $data['cvv'] = @$_REQUEST['cvv'];
        $data['profile_id'] = @$_REQUEST['profile_id'];
        $data['card_type'] = @$_REQUEST['card_type'];
        $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
        $data['token'] = @$_REQUEST['token'];
        $data['email'] = @$_REQUEST['email'];
        $data['coupon_code'] = @$_REQUEST['coupon_code'];
        $data['is_advance'] = @$_REQUEST['is_advance'];
        $data['currency_id'] = @$_REQUEST['currency_id'];
        
        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
        $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
        
        $data['studio_id'] = $studio_id;
        $data['user_id'] = $user_id;
        
        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
        $video_id = $Films->id;
        $plan_id = 0;

        //Check studio has plan or not
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);

        //Check ppv plan or advance plan set or not by studio admin
        $is_ppv = $is_advance = 0;
        if (intval($data['is_advance']) == 0) {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id, 0);
            if (isset($plan->id) && intval($plan->id)) {
                $is_ppv = 1;
            } else {
                $plan = Yii::app()->common->getContentPaymentType($Films->content_types_id, $Films->ppv_plan_id, $this->studio_id);
                $is_ppv = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
            }
        } else  {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id);
            $is_advance = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
        }
        
        if (isset($plan) && !empty($plan)) {
            $plan_id = $plan->id;
            $price = Yii::app()->common->getPPVPrices($plan_id, $data['currency_id'], $_REQUEST['country']);
            $currency = Currency::model()->findByPk($price['currency_id']);
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;

            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');

            $end_date = '';
            if (intval($is_advance) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;
                        
                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
            }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }

            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                    if (intval($is_episode)) {
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $price['season_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for season
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $price['show_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for show
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $price['price_for_unsubscribed'];
                }
            }

            $couponCode = '';
            //Calculate coupon if exists
            if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                $getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
                $data['amount'] = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
            }
                
            $gateway_code = '';
            
            if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                $data['card_id'] = @$card->id;
                $data['token'] = @$card->token;
                $data['profile_id'] = @$card->profile_id;
                $data['card_holder_name'] = @$card->card_holder_name;
                $data['card_type'] = @$card->card_type;
                $data['exp_month'] = @$card->exp_month;
                $data['exp_year'] = @$card->exp_year;

                $gateway_info = StudioPaymentGateways::model()->findAllByAttributes(array('studio_id' => $studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
                $gateway_code = $gateway_info[0]->short_code;
            } else {
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $gateway_info = $plan_payment_gateway['gateways'];
                }
            }
            
            $this->setPaymentGatwayVariable($gateway_info);
            
            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);

                $VideoDetails = $VideoName;
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if ($data['season_id'] == 0) {
                        $VideoDetails .= ', All Seasons';
                    } else {
                        $VideoDetails .= ', Season ' . $data['season_id'];
                        if ($data['episode_id'] == 0) {
                            $VideoDetails .= ', All Episodes';
                        } else {
                            $episodeName = Yii::app()->common->getEpisodename($data['episode_id']);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', ' . $episodeName;
                        }
                    }
                }

                if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypal') {
                    
                } else if (abs($data['amount']) < 0.01) {
                    
                    if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                        $data['gateway_code'] = $gateway_code;
                        $crd = Yii::app()->billing->setCardInfo($data);
                        $data['card_id'] = $crd;
                    }
                    
                    $data['amount'] = 0;
                    $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$video_id,$start_date,$end_date,$data['coupon_code'], $is_advance, $gateway_code);
                    
                    $ppv_subscription_id = $set_ppv_subscription;
                    $trans_data=array();
                    $trans_data['transaction_status'] = 'succeeded';
                    $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
                    $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
                    $trans_data['dollar_amount'] =$data['amount'];
                    $trans_data['amount'] = $data['amount'];
                    $trans_data['response_text'] = '';
                    $transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$video_id,$trans_data, $ppv_subscription_id, $gateway_code, $is_advance);
                    
                    $ppv_apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, $is_advance, $VideoName, $VideoDetails);
                    
                    $res['code'] = 200;
                    $res['status'] = "OK";
                } else {
                    $data['gateway_code'] = $gateway_code;
                    $data['paymentDesc'] = $VideoDetails.' - '.$data['amount'];
                    
                    if($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && intval($data['havePaypal'])){
                        
                    }
                    
                    if(isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez'){
                        //echo json_encode($data);exit;
                    }
                    $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                    $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $trans_data = $payment_gateway::processTransactions($data);
                    
                    $is_paid = $trans_data['is_success'];
                    if (intval($is_paid)) {
                        $data['amount'] = $trans_data['amount'];
                        if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                            $crd = Yii::app()->billing->setCardInfo($data);
                            $data['card_id'] = $crd;
                        }
                        
                        $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$video_id,$start_date,$end_date,$data['coupon_code'], $is_advance, $gateway_code);
                        $ppv_subscription_id = $set_ppv_subscription;
                        
                        $transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$video_id,$trans_data, $ppv_subscription_id, $gateway_code, $is_advance);
                        $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $is_advance, $VideoName, $VideoDetails);
                        
                        $res['code'] = 200;
                        $res['status'] = "OK";
                    } else {
                        $res['code'] = 411;
                        $res['status'] = "Error";
                        $res['msg'] = $translate['error_transc_process'];
                        $res['response_text'] = $trans_data['response_text'];
                    }
                }
            } else {
                $res['code'] = 411;
                $res['status'] = "Error";
                $res['msg'] = "Studio has no payment gateway";
            }
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Studio has no PPV plan";
        }

        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @method public GetBannerList() It will return the list of top banners for the studio
     * @param string $authToken Authentication Token
     * @author Gayadhar<support@muvi.com>
     * @return json Json array
     */
    function actionGetBannerList() {
        $banners = StudioBanner::model()->findAllByAttributes(array('studio_id' => $this->studio_id, 'banner_section' => 6, 'is_published' => 1), array('order' => 'id_seq ASC'));
        if ($banners) {
            $data['code'] = 200;
            $data['status'] = 'OK';
            $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
            foreach ($banners AS $key => $banner) {
                $banner_src = $banner->image_name;
                $banner_title = $banner->title;
                $banner_id = $banner->id;
                $data['banner_url'][] = $posterCdnUrl . "/system/studio_banner/" . $this->studio_id . "/original/" . urlencode($banner_src);
            }
            $bnr = new BannerText();
            $banner_text = $bnr->findByAttributes(array('studio_id' => $this->studio_id), array('order' => 'id DESC'));
            $data['text'] = (isset($banner_text->bannercontent) && $banner_text->bannercontent != '') ? html_entity_decode($banner_text->bannercontent) : '';
            $data['join_btn_txt'] = (isset($banner_text->show_join_btn) && $banner_text->show_join_btn > 0) ? html_entity_decode($banner_text->join_btn_txt) : '';
        } else {
            $data['code'] = 434;
            $data['status'] = 'Failure';
            $data['msg'] = 'No banner uploaded';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public GetBannerSectionList() It will return the list of top banners for the studio
     * @param string $authToken Authentication Token
     * @author Gayadhar<support@muvi.com>
     * @return json Json array
     */
    function actionGetBannerSectionList($nojson = 1) {
        $studio_id = $this->studio_id;
        $sql = "SELECT t.id FROM studios s,templates t WHERE s.parent_theme =t.code AND s.id =" . $studio_id;
        $res = Yii::app()->db->createCommand($sql)->queryRow();
        $template_id = $res['id'];
        $banner_secs = BannerSection::model()->with(array('banners' => array('alias' => 'bn')))->findAll(array('condition' => 't.template_id=' . $template_id . ' and bn.studio_id = ' . $studio_id, 'order' => 'bn.id_seq ASC, bn.id DESC'));
        if ($banner_secs) {            
            $bnr = new BannerText();
            $banner_text = $bnr->findByAttributes(array('studio_id' => $studio_id), array('order' => 'id DESC'));
            $data['code'] = 200;
            $data['status'] = 'OK';
            $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
            if ($template_id == 10) {
                $count = 0;
                foreach ($banner_secs AS $key => $banner) {
                    $bnr = $banner->banners;
                    $banner_src = trim($bnr[0]->image_name);
                    $data['banner_url'][] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . urlencode($banner_src);
                    $data['banners'][$count]['original'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . urlencode($banner_src);
                    $data['banners'][$count]['mobile_view'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/mobile_view/" . urlencode($banner_src);
                    $count++;
                }
            } else {
                $banners = $banner_secs[0]->banners;
                foreach ($banners AS $key => $banner) {
                    $banner_src = trim($banner->image_name);
                    $data['banner_url'][] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . urlencode($banner_src);
                    $data['banners'][$key]['original'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . urlencode($banner_src);
                    $data['banners'][$key]['mobile_view'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/mobile_view/" . urlencode($banner_src);
                }
            }
            $data['text'] = (isset($banner_text->bannercontent) && $banner_text->bannercontent != '') ? html_entity_decode($banner_text->bannercontent) : '';
            $data['join_btn_txt'] = (isset($banner_text->show_join_btn) && $banner_text->show_join_btn > 0) ? html_entity_decode($banner_text->join_btn_txt) : '';
        } else {
            $data['code'] = 434;
            $data['status'] = 'Failure';
            $data['msg'] = 'No banner uploaded';
        }
        if ($nojson == 1) {
            $this->setHeader($data['code']);
            echo json_encode($data);
            exit;
        } else {
            return $data;
        }
    }

    /**
     * @method public socialAuth() It will authenticate the social Login 
     * @author Gayadhar<support@muvi.com>
     * @param type $authToken AuthToken for the Studio
     * @param string $fb_userId The user id return by the User
     * @param string $email Email return by the app
     * @param String $name Name as returned by the application
     * @return json Json string
     */
    function actionSocialAuth() {
        $req = $_REQUEST;
        $studio_id = $this->studio_id;
        if (@$req['fb_userid']) {
            if (@$req['email']) {
                $device_type = trim(@$req['device_type']);
                $google_id = trim(@$req['google_id']);
                $device_id = isset($req['device_type']) ? @trim(@$req['device_id']) : '2'; 
                $userData = SdkUser::model()->find('(email=:email OR fb_userid=:fb_userid) AND studio_id=:studio_id', array(':email' => $req['email'], ':fb_userid' => $req['fb_userid'], ':studio_id' => $studio_id));
                if ($userData && ($userData->is_deleted || !$userData->status)) {
                    $data['code'] = 436;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'Account suspended, Contact admin to login';
                } elseif ($userData) {
                    if ($userData['fb_userid'] != $req['fb_userid']) {
                        $userData->fb_userid = $req['fb_userid'];
                        $userData->save();
                    }
                    if ($userData->add_video_log) {
                        $ip_address = CHttpRequest::getUserHostAddress();
                        $user_id = $userData->id;
                        $login_history = new LoginHistory();
                        $login_history->studio_id = $studio_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = new CDbExpression("NOW()");
                        $login_history->logout_at = "0000-00-00 00:00:00";
                        $login_history->last_login_check = new CDbExpression("NOW()");
                        $login_history->google_id = $google_id;
                        $login_history->device_id = $device_id;
                        $login_history->device_type = $device_type;
                        $login_history->ip = $ip_address;
                        $login_history->save();
						$login_history_id = $login_history->id;
                    } 
                    
                    $data['id'] = $userData->id;
                    $data['email'] = $userData->email;
                    $data['display_name'] = $userData->display_name;
                    $data['studio_id'] = $studio_id;
                    $data['is_newuser'] = 0;
                    $data['profile_image'] = $this->getProfilePicture($userData->id, 'profilepicture', 'thumb');
                    $isSubscribed = Yii::app()->common->isSubscribed($userData->id, $this->studio_id);
                    $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
					$data['login_history_id'] = @$login_history_id;
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['msg'] = "Login Success";
                } else {
                    $email = $res['data']['email'] = @$req['email'];
                    $name = $res['data']['name'] = @$req['name'];
                    $res['data']['fb_userid'] = @$req['fb_userid'];

                    $res['data']['password'] = @$req['password'] ? $req['password'] : '';
                    
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                    
                    $gateway_code = '';
                    if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                        $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                        $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                    }

                    $res['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
                    $res['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
                    $res['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];
                    
                    $ret = SdkUser::model()->saveSdkUser($res, $studio_id);
                    if ($ret) {
                        if (@$ret['error']) {
                            $data['code'] = 420;
                            $data['status'] = 'Failure';
                            $data['msg'] = 'Invalid Plan!';
                            $this->setHeader($data['code']);
                            echo json_encode($data);
                            exit;
                        }
                        // Send welcome email to user
                        if (@$ret['is_subscribed']) {
                            $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email_with_subscription');
                        } else {
                            $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email');
                        }
                        $data['id'] = $ret['user_id'];
                        $data['email'] = $email;
                        $data['display_name'] = $name;
                        $data['studio_id'] = $studio_id;
                        $data['is_newuser'] = 1;
                        $data['profile_image'] = $this->getProfilePicture($ret['user_id'], 'profilepicture', 'thumb');
                        $isSubscribed = Yii::app()->common->isSubscribed($ret['user_id'], $this->studio_id);
                        $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
						
						$ip_address = CHttpRequest::getUserHostAddress();
                        $user_id = $userData->id;
						$login_history = new LoginHistory();
						$login_history->studio_id = $studio_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = new CDbExpression("NOW()");
						$login_history->logout_at = "0000-00-00 00:00:00";
                        $login_history->last_login_check = new CDbExpression("NOW()");
                        $login_history->google_id = $google_id;
                        $login_history->device_id = $device_id;
                        $login_history->device_type = $device_type;
                        $login_history->ip = $ip_address;
						$login_history->save();
						$data['login_history_id'] = $login_history->id;
						
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['msg'] = "Login Success";
                    } else {
                        $data['code'] = 421;
                        $data['status'] = 'Failure';
                        $data['msg'] = 'Error in registration';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    }
                }
            } else {
                $data['code'] = 417;
                $data['status'] = 'Failure';
                $data['msg'] = 'A valid Email required';
            }
        } else {
            $data['code'] = 435;
            $data['status'] = 'Failure';
            $data['msg'] = 'Facebook User id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getPpvDetails() It will check and return PPV details for a content
     * @author Gayadhar<support@muvi.com>
     * @param type $authToken AuthToken for the Studio
     * @param string $content_id Content id 
     * @param string $stream_id Stream Id 
     * @return json Json string
     */
    public function actionGetPpvDetails() {
        $studio_id = $this->studio_id;
        $content_id = $_REQUEST['content_id'];
        $stream_id = $_REQUEST['stream_id'];
        if ($content_id && $stream_id) {
            $command = Yii::app()->db->createCommand()
                    ->select('f.ppv_plan_id,f.content_type_id,f.content_types_id')
                    ->from('films f ,movie_streams m')
                    ->where('f.id=m.movie_id AND f.id=:content_id AND f.studio_id=:studio_id AND m.id = :stream_id', array(':content_id' => $content_id, ':studio_id' => $studio_id, ":stream_id" => $stream_id));
            $movie = $command->queryAll();
            if ($movie) {
                if ($movie[0]['ppv_plan_id']) {
                    $ppvPlans = PpvPlans::model()->findByAttributes(array('id' => $movie[0]['ppv_plan_id'], 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 0, 'is_advance_purchase' => 0));
                } else {
                    $ppvPlans = PpvPlans::model()->findByAttributes(array('studio_content_types_id' => $movie[0]['content_type_id'], 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 1, 'is_advance_purchase' => 0));
                }
                if (@$ppvPlans) {
                    $data['code'] = 200;
                    $data['status'] = 'OK';
                    $data['msg'] = "PPV Details";
                    if ($movie[0]['content_types_id'] != 3) {
                        $data['ppvPlans']['price_for_unsubscribed'] = $ppvPlans->price_for_unsubscribed;
                        $data['ppvPlans']['price_for_subscribed'] = $ppvPlans->price_for_subscribed;
                    } else {
                        $data['ppvPlans']['show_unsubscribed'] = $ppvPlans->show_unsubscribed;
                        $data['ppvPlans']['show_subscribed'] = $ppvPlans->show_subscribed;
                        $data['ppvPlans']['season_unsubscribed'] = $ppvPlans->season_unsubscribed;
                        $data['ppvPlans']['season_subscribed'] = $ppvPlans->season_subscribed;
                        $data['ppvPlans']['episode_subscribed'] = $ppvPlans->episode_subscribed;
                    }
                } else {
                    $data['code'] = 439;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'PPV not enabled for this content';
                }
            } else {
                $data['code'] = 438;
                $data['status'] = 'Failure';
                $data['msg'] = 'No content availbe for the information';
            }
        } else {
            $data['code'] = 437;
            $data['status'] = 'Failure';
            $data['msg'] = 'Content id and Stream id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getVideoDetails() It will give details of the video
     * @author Gayadhar<support@muvi.com>
     * @param type $authToken AuthToken for the Studio
     * @param string $content_uniq_id Uniq id of 
     * @param string $stream_uniq_id Stream Id 
     * @return json Json string
     */
     function actionGetVideoDetails() {
        $studio_id = $this->studio_id;
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);
        $content_uniq_id = @$_REQUEST['content_uniq_id'];
        $stream_uniq_id = @$_REQUEST['stream_uniq_id'];
        $internetSpeed = 0;
        if (isset($_REQUEST['internet_speed']) && $_REQUEST['internet_speed'] != '') {
            $internetSpeed = $_REQUEST['internet_speed'];
        }        
        if ($content_uniq_id && $stream_uniq_id) {
            $cond = array(':uniq_id' => $content_uniq_id, ':stream_uniq_id' => $stream_uniq_id);
            $command = Yii::app()->db->createCommand()
                    ->select('F.id,F.name,F.content_types_id,S.id AS stream_id,S.rolltype AS rollType,S.roll_after AS rollAfter,S.video_resolution,S.enable_ad AS adActive,S.full_movie,S.embed_id,S.is_demo,S.is_converted,F.uniq_id,S.content_key,S.encryption_key,S.thirdparty_url,S.video_management_id,S.is_offline, S.is_episode')
                    ->from('films F, movie_streams S')
                    ->where('F.id=S.movie_id AND F.uniq_id=:uniq_id AND S.embed_id=:stream_uniq_id ', $cond);
            $contentDetails = $command->queryAll();
            if ($contentDetails) {
                    $language_nameArr = array();
                    $domainName= $this->getDomainName();
                    $movie = $contentDetails[0];                     
                    $thirdparty_url='';
                    $fullmovie_path='';
                    $embedUrl='';
                    $trailerThirdpartyUrl='';
                    $trailerUrl='';
                    $embedTrailerUrl='';  
					$adActive = $contentDetails[0]['adActive'];
					$rollType = $contentDetails[0]['rollType'];
					$roll_after = $contentDetails[0]['rollAfter'];
                    $user_id = (isset($_REQUEST['user_id'])&& $_REQUEST['user_id']!='')?$_REQUEST['user_id']:0;                    
                    $stream_id=($movie['stream_id']>0)?$movie['stream_id']:0;
                    $parameters = array();
                    $parameters['movie_id'] = $movie['id'];
                    $parameters['stream_id'] = $stream_id;
                    $parameters['studio_id'] = $studio_id;
                    $activePlayCount = VideoLogs::model()->activePlayLog($parameters);
                    $streamingRestriction = 0;
                    $getDeviceRestrictionSetByStudio = 0;
                    if($user_id>0){
                        $playLengthArr = VideoLogs::model()->getvideoDurationPlayed($studio_id, $user_id, $movie['id'], $stream_id);
                        $getDeviceRestriction = StudioConfig::model()->getdeviceRestiction($studio_id, $user_id);
                        if($getDeviceRestriction != '' && $getDeviceRestriction > 0){
                            $streamingRestriction = 1;
                        } 
                        if($getDeviceRestriction != ""){
                            $getDeviceRestrictionSetByStudioData = StudioConfig::model()->getConfig($studio_id, 'restrict_streaming_device');
                            if(@$getDeviceRestrictionSetByStudioData->config_value != ""){
                                $getDeviceRestrictionSetByStudio = $getDeviceRestrictionSetByStudioData->config_value;
                            }
                        } 
                    } 
                    $played_length=!empty($playLengthArr['played_length'])?$playLengthArr['played_length']:'';
                    //get trailer info from movie_trailer table                                      
                    $movie_id=$movie['id'];
                    $trailerData=movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id'=>$movie_id));
                    if($trailerData['trailer_file_name']!=''){
                        $embedTrailerUrl=$domainName.'/embedTrailer/'.$movie['uniq_id'];
                        if($trailerData['third_party_url']!=''){
                            $trailerThirdpartyUrl=$this->getSrcFromThirdPartyUrl($trailerData['third_party_url']);
                        }else if($trailerData['video_remote_url']!=''){
                            $trailerUrl = $this->getTrailer($movie_id,"",$this->studio_id);
                        }  
                    }                    
                    //End of trailer part
                    if($movie['content_types_id'] == 4){
                        $Ldata = Livestream::model()->find("studio_id=:studio_id AND movie_id=:movie_id", array(':studio_id'=>$this->studio_id,':movie_id'=>$movie['id']));
                        if($Ldata && $Ldata->feed_url){
                            $embedUrl= $domainName.'/embed/livestream/'.$movie['uniq_id'];
                            $fullmovie_path = $Ldata->feed_url;
                            if ($Ldata->feed_type == 2) {
                                    if (strpos($Ldata->feed_url, 'rtmp://'.nginxserverip.'/live') !== false) {
                                    $fullmovie_path = str_replace("rtmp://".nginxserverip."/live", nginxserverlivecloudfronturl, $Ldata->feed_url) . "/index.m3u8";
                                    } else if (strpos($Ldata->feed_url, 'rtmp://'.nginxserverip.'/record') !== false) {
                                    $fullmovie_path = str_replace("rtmp://".nginxserverip."/record", nginxserverrecordcloudfronturl, $Ldata->feed_url) . "/index.m3u8";
                                    }
                                }
                            }
                    }else{
                        if($movie['full_movie']!=''){
                            $embedUrl = $domainName.'/embed/'.$movie['embed_id'];
                           if($movie['thirdparty_url']!=''){
                                $thirdparty_url=$this->getSrcFromThirdPartyUrl($movie['thirdparty_url']);
                           }else{                            
                              //Check thirdparty url is not present then pass video url with details                            
                                $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
                                $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] . "/" . $bucketInfo['signedFolderPath'] . "uploads/movie_stream/full_movie/" . $movie['stream_id'] . "/" . $movie['full_movie'];
                                if($movie['is_demo'] == 1){
                                        $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] . "/" . $bucketInfo['signedFolderPath'] . "uploads/small.mp4";
                                }
                                $movie['movieUrlForTv'] = $fullmovie_path;                                
                                $multipleVideo = $this->getvideoResolution($movie['video_resolution'], $fullmovie_path);
                                $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $fullmovie_path, $internetSpeed);
                                $videoToBeplayed12 = explode(",", $videoToBeplayed);
                                $fullmovie_path = $videoToBeplayed12[0];
                                $data['videoResolution'] = $videoToBeplayed12[1];
                                $clfntUrl = 'https://' . $bucketInfo['cloudfront_url'];
                                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                                $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, "", "", 60000,$studio_id);
                                //added by prakash for signed url on 29th march 2017
                                $multipleVideoSignedArr=array();
                                foreach($multipleVideo as $resKey=>$videoPath){
                                    $subArr=array();
                                    if($resKey){
                                        $subArr['resolution']=$resKey;
                                        $rel_path_multiple = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $videoPath));
                                        $subArr['url']=$this->getnew_secure_urlForEmbeded($rel_path_multiple, $clfntUrl, "", "", 60000,$studio_id);
                                        $multipleVideoSignedArr[]=$subArr;
                                    }                                                               
                                }                                                               
                                $data['videoDetails'] = $multipleVideoSignedArr;                                
                                $data['newvideoUrl'] = $multipleVideoForUnsigned[144];
                                $offline = 0;
                                $app = Yii::app()->general->apps_count($studio_id); 
                                if(empty($app) || (isset($app) && ($app['apps_menu'] & 2)) || (isset($app) && ($app['apps_menu'] & 4))){
                                    $offline_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'offline_view');
                                    if (($offline_val['config_value'] == 1 || $offline_val == '') && ($movie['is_offline'] == 1) && (@$movie['content_types_id'] == 1 || (@$movie['content_types_id'] == 3 && @$movie['is_episode'] == 1))) {
                                        $offline = 1;
                                    }
                                }
                                $data['is_offline'] = $offline;
                                if($movie['content_key'] && $movie['encryption_key']){
                                    $folderPath = Yii::app()->common->getFolderPath("",$this->studio_id);
                                    $signedBucketPath = $folderPath['signedFolderPath'];
                                    $fullmovie_path = 'https://'.$bucketInfo['bucket_name'].'.'.$bucketInfo['s3url'].'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$movie['stream_id'].'/stream.mpd';
                                    $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($this->studio_id,'drm_cloudfront_url');
                                    if($getStudioConfig){
                                        if(@$getStudioConfig['config_value'] != ''){
                                            $fullmovie_path = 'https://'.$getStudioConfig['config_value'].'/uploads/movie_stream/full_movie/'.$movie['stream_id'].'/stream.mpd';
                                        }
                                    }
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL,MS3_URL);
                                    curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=200988,058524ac8dc0459cb9e4d497136563ba&contentId=urn:marlin:kid:" . $movie['encryption_key'] . "&contentKey=" . $movie['content_key'] . "&ms3Scheme=true&contentURL=" . $fullmovie_path);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
                                    $server_output = curl_exec ($ch);
                                    $data['studio_approved_url'] = $server_output;
                                    curl_close ($ch);
									//get LicenceUrl									
									$licence_url=$this->generateExplayWideVineToken($movie['content_key'],$movie['encryption_key']);									
									$data['licenseUrl']=$licence_url?$licence_url:'';
                                }                            
                           }
                        }
                    }
                /**
                *  srt/vtt subtitle files for api.
                * @author Prangya.
                */
                if(isset($movie['video_management_id']) && ($movie['video_management_id']>0)){
                     $video_management_id = $movie['video_management_id'];
                        $cond_subtitle = array(':video_management_id' => $video_management_id);
                        $command = Yii::app()->db->createCommand()
                          ->select('vs.id AS subId,vs.filename,ls.name,ls.code,vs.video_gallery_id,') 
                          ->from('video_subtitle vs,language_subtitle ls')
                          -> where('vs.language_subtitle_id = ls.id AND vs.video_gallery_id=:video_management_id',$cond_subtitle);
                        $getVideoSubtitle = $command->queryAll(); 
                        if($getVideoSubtitle) {
                            $i = 0;
                            foreach ($getVideoSubtitle as $getVideoSubtitlekey => $getVideoSubtitlevalue) {
                                $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                                $signedFolderPath = $folderPath['signedFolderPath'];
                                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                                $bucketname = $bucketInfo['bucket_name'];
                                $clfntUrl = HTTP . $bucketInfo['cloudfront_url'];
                                $s3url = Yii::app()->common->connectToAwsS3($studio_id);
                                $filePth = $clfntUrl . "/" . $bucketInfo['signedFolderPath'] . "subtitle/" . $getVideoSubtitlevalue['subId'] . "/" . $getVideoSubtitlevalue['filename'];
                                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
                                $language_nameArr[$i]['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 1, "", 60000,$studio_id);
                                $language_nameArr[$i]['code'] = $getVideoSubtitlevalue['code'];
								if (@$translate[$getVideoSubtitlevalue['name']]!=''){
									$language_nameArr[$i]['language'] = @$translate[$getVideoSubtitlevalue['name']];  
								} else {
									$language_nameArr[$i]['language'] = $getVideoSubtitlevalue['name'];
								}
                                $i ++;
                            } 
                        }
                    }
                 // end of subtitle part.
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = "Video Details";
                $data['videoUrl'] = $fullmovie_path;
                $data['thirdparty_url']=$thirdparty_url;
				$data['emed_url'] = $embedUrl ;                 
                $data['trailerUrl'] = $trailerUrl;
                $data['trailerThirdpartyUrl']=$trailerThirdpartyUrl;
				$data['embedTrailerUrl'] = $embedTrailerUrl ; 
                $data['played_length'] = $played_length ; 
                $data['subTitle'] = $language_nameArr;
                $data['streaming_restriction'] = $streamingRestriction;
                $data['no_streaming_device'] = $getDeviceRestrictionSetByStudio;
                $data['msg'] = @$translate['restrict-streaming-device'];
                $data['no_of_views'] = @$activePlayCount;
                $ads['adActive'] = $adActive;
                $ads['adNetwork'] = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($this->studio_id);
                $start = $mid = $end = 0;
                if ($rollType == '7' || $rollType == '5' || $rollType == '3' || $rollType == '1') {
                    $start = 1;
                }
                if ($rollType == '7' || $rollType == '3' || $rollType == '6' || $rollType == '2') {
                    $mid = 1;
                }
                if ($rollType == '7' || $rollType == '5' || $rollType == '6' || $rollType == '4') {
                    $end = 1;
                }
                $adsTime['start'] = $start;
                $adsTime['mid'] = $mid;
                $adsTime['end'] = $end;
                $roll_after = explode(',', $roll_after);
                if (in_array("end", $roll_after)) {
                    array_pop($roll_after);
                }
                if (in_array("0", $roll_after)) {
                    $roll_after = array_reverse($roll_after);
                    array_pop($roll_after);
                    $roll_after = array_reverse($roll_after);
                }
                //convert ads streaming time to seconds
                $time_seconds ="";$i =0;
                while($i<count($roll_after)){
                    if(count($roll_after)>1)
                        $time_seconds .= $this->getHHMMSSToseconds($roll_after[$i++]).',';
                    else
                        $time_seconds .= $this->getHHMMSSToseconds($roll_after[$i++]);
                }
                $adsTime['midroll_values'] = $time_seconds;
				if($ads['adNetwork'][1]['ad_network_id']!=null && $ads['adNetwork'][1]['ad_network_id']!=3){
				   $ads['adsTime'] = $adsTime;
				}
                $data['adsDetails'] = $ads;               
            } else {
                $data['code'] = 438;
                $data['status'] = 'Failure';
                $data['msg'] = 'No content availbe for the information';
            }
        } else {
            $data['code'] = 437;
            $data['status'] = 'Failure';
            $data['msg'] = 'Content id and Stream id required';
        }
        $this->setHeader($data['code']);       
        echo json_encode($data);
        exit;
    }

    /**
     * @method public GetImageForDownload() It will give the s3 url image to check internet speed of user
     * @author Sruti kanta<support@muvi.com>
     * @return json Json string
     */
    function actionGetImageForDownload() {
        $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
        $image_url = CDN_HTTP . $bucketInfo['bucket_name'] . "." . $bucketInfo['s3url'] . "/check-download-speed.jpg";
        $data['code'] = 200;
        $data['status'] = 'OK';
        $data['image_url'] = $image_url;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public startUpstream() It will start the upstream automatically and will generate the HLS url 
     * @param string $authToken Authtoken
     * @param string $channel_id Authtoken
     * @param string $title Title of the stream
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionStartUpstream() {
        if (@$_REQUEST['movie_id']) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 1;
                $lsuser->stream_start_time = gmdate('Y-m-d H:i:s');
                $lsuser->stream_update_time = gmdate('Y-m-d H:i:s');
                $lsuser->save();
                $data['code'] = 200;
                $data['status'] = 'OK';
            } else {
                $data['code'] = 441;
                $data['status'] = 'Failure';
                $data['msg'] = 'Invalid content id';
            }
        } else {
            $data['code'] = 440;
            $data['status'] = 'Failure';
            $data['msg'] = 'A movie id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getliveUserlist() It will return the list of users who are currently live streaming 
     * @param string $authToken Authtoken
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionGetLiveUserlist() {
        $lsuser = LivestreamUsers::model()->findAll('is_online=1 AND studio_id =:studio_id', array(':studio_id' => $this->studio_id));
        if ($lsuser) {
            $data['code'] = 200;
            $data['status'] = 'OK';
            $data['msg'] = 'Available users list';
            foreach ($lsuser AS $key => $val) {
                $data['users'][$key]['name'] = $val->name;
                $data['users'][$key]['nick_name'] = $val->nick_name;
                $data['users'][$key]['title'] = $val->channel_title;
                $data['users'][$key]['hls_path'] = $val->pull_url;
                $data['users'][$key]['rtmp_path'] = $val->push_url;
            }
        } else {
            $data['code'] = 200;
            $data['status'] = 'OK';
            $data['msg'] = 'No online user available';
            $data['users'] = array();
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getChannelOnlineStatus() It will return the list of users who are currently live streaming 
     * @param string $authToken Authtoken
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actiongetChannelOnlineStatus() {
		if ($_REQUEST['permalink']) {
            $movie = Yii::app()->db->createCommand()->select('f.content_types_id,ls.is_online')->from('films f ,livestream ls ')
                    ->where('ls.movie_id = f.id AND f.content_types_id=4 AND f.permalink=:permalink AND  f.studio_id=:studio_id', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryRow();
			if($movie){
                	$data['code'] = 200;
					$data['status'] = 'OK';
                    $data['is_online'] = $movie["is_online"] ? (int)$movie["is_online"] : 0;
                    $data['msg'] = $movie["is_online"] ? "Online" : 'Offline';
            } else {
                $data['code'] = 441;
				$data['status'] = 'Failure';
				$data['msg'] = 'Invalid permalink';
			}
        } else {
			$data['code'] = 440;
			$data['status'] = 'Failure';
			$data['msg'] = 'permalink required';
		}
        $this->setHeader($data['code']);
        echo json_encode($data);exit;
    }

    /**
     * @method public stopUpstream() It will stop the upstream 
     * @param string $authToken Authtoken
     * @param string $channel_id Channel id of the streaming
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionStopUpstream() {
        if (@$_REQUEST['movie_id']) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 0;
                $lsuser->stream_start_time = '';
                $lsuser->stream_update_time = '';
                $lsuser->save();
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = 'Success!';
            } else {
                $data['code'] = 441;
                $data['status'] = 'Failure';
                $data['msg'] = 'Invalid movie id';
            }
        } else {
            $data['code'] = 440;
            $data['status'] = 'Failure';
            $data['msg'] = 'A movie id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public channelLiveStatus() It will stop the upstream 
     * @param string $authToken Authtoken
     * @param string $channel_id Channel id of the streaming
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionChannelStatus() {
        if (@$_REQUEST['movie_id']) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 1;
                $lsuser->stream_update_time = gmdate('Y-m-d H:i:s');
                $lsuser->save();
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = "Stream is online now";
            } else {
                $data['code'] = 441;
                $data['status'] = 'Failure';
                $data['msg'] = 'Invalid movie id';
            }
        } else {
            $data['code'] = 440;
            $data['status'] = 'Failure';
            $data['msg'] = 'A movie id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getPorfileDetails() It will fetch the profile details of the logged in user
     * @param string $authToken Authtoken
     * @param string $email
     * @param int $user_id
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionGetProfileDetails() {
        if (@$_REQUEST['email'] && $_REQUEST['user_id']) {
            $userData = SdkUser::model()->find('email=:email AND studio_id =:studio_id AND status=:status AND id=:user_id', array(':email' => $_REQUEST['email'], ':studio_id' => $this->studio_id, ':status' => 1, ':user_id' => $_REQUEST['user_id']));
            if ($userData) {
                $data['id'] = $userData->id;
                $data['email'] = $userData->email;
                $data['display_name'] = $userData->display_name;
                $data['studio_id'] = $userData->studio_id;
                $data['isFree']= $userData->is_free;
                $data['profile_image'] = $this->getProfilePicture($userData->id, 'profilepicture', 'thumb', $this->studio_id);
				$data['mobile_number'] = @$userData->mobile_number;
                
				$isSubscribed = Yii::app()->common->isSubscribed($userData->id, $this->studio_id);
                $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
                $custom_field_details = Yii::app()->custom->getCustomFieldsDetail($this->studio_id,$userData->id);
				
                if(!empty($custom_field_details)){
                    foreach($custom_field_details as $key=>$val){
                        if($key == "languages"){
                            $data["custom_".$key] = $val;
                        }else{
                            $data["custom_".$key] = $val[0];
                        }
                    }
                }
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = 'Success!';
            } else {
                $data['code'] = 442;
                $data['status'] = 'Failure';
                $data['msg'] = 'No user found!';
            }
        } else {
            $data['code'] = 443;
            $data['status'] = 'Failure';
            $data['msg'] = 'Email & user id required!';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method getFbUserstatus() Returns the Facebook user status 
     * @param String $authToken A authToken
     * @param String $fbuser_id Facebook User id
     * @author Gayadhar<support@muvi.com>
     * @return Json Facebook user status
     */
    function actionGetFbUserStatus() {
        $userData = SdkUser::model()->find('(fb_userid=:fb_userid) AND studio_id=:studio_id', array(':fb_userid' => $_REQUEST['fb_userid'], ':studio_id' => $this->studio_id));
        if ($userData && ($userData->is_deleted || !$userData->status)) {
            $data['code'] = 436;
            $data['status'] = 'Failure';
            $data['msg'] = 'Account suspended, Contact admin';
        } elseif ($userData) {
            $data['id'] = $userData->id;
            $data['email'] = $userData->email;
            $data['display_name'] = $userData->display_name;
            $data['nick_name'] = $userData->nick_name;
            $data['studio_id'] = $this->studio_id;
            $data['is_newuser'] = 0;
            $data['profile_image'] = $this->getProfilePicture($userData->id, 'profilepicture', 'thumb');
            $isSubscribed = Yii::app()->common->isSubscribed($userData->id, $this->studio_id);
            $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = "Success";
        } else {
            $data['is_newuser'] = 1;
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = "Success";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method converttimetoSec(string $str) convert a string of HH:mm:ss to second
     * @author Gayadhar<support@muvi.com>
     * @return int 
     */
    function convertTimetoSec($str_time) {
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        return $time_seconds;
    }

    public function actionVideoLogs() {
        $data = array();

        if (isset($_REQUEST['ip_address']) && isset($_REQUEST['movie_id'])) {
            $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : '';
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $studio_id = $this->studio_id;
                $user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
                if($user_id){
                    $add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
                }else{
                    $add_video_log = 1;
                }
                $isAllowed = 0;
                $device_id = 0;
                if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
                    $isAllowed = 1;
                    $device_id = $_REQUEST['user_id'];
                }
                if ($add_video_log || $isAllowed) {
                    $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
                    $movie_id = Yii::app()->common->getMovieId($movie_code);

                    $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
                    $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

                    //Get stream Id
                    $stream_id = 0;
                    if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
                    } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
                    } else {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
                    }
                    $parameters = array();
                    $parameters['movie_id'] = $movie_id;
                    $parameters['stream_id'] = $stream_id;
                    $parameters['studio_id'] = $studio_id;
                    $activePlayCount = VideoLogs::model()->activePlayLog($parameters);

                    $played_length = $_REQUEST['played_length'];
                    $video_length = isset($_REQUEST['video_length']) ? $_REQUEST['video_length'] : '0';
                    $watch_status = $_REQUEST['watch_status'];
                    $device_type = $_REQUEST['device_type'];
                    $content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
                    $restrictDeviceId = 0;
                    if($user_id > 0 && @$_REQUEST['is_streaming_restriction'] == 1) {
                        $restrictStreamId = (isset($_REQUEST['restrict_stream_id'])) ? $_REQUEST['restrict_stream_id'] : 0;
                        if(@$_REQUEST['is_active_stream_closed'] == 1){
                            RestrictDevice::model()->deleteData($restrictStreamId);
                        } else {
                            $postData = array();
                            $postData['id'] = $restrictStreamId;
                            $postData['studio_id'] = $studio_id;
                            $postData['user_id'] = $user_id;
                            $postData['movie_stream_id'] = $stream_id;
                            $restrictDeviceId = RestrictDevice::model()->dataSave($postData);
                        }
                    }
                    $video_log = new VideoLogs();
                    $log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;
                    
                    $trailer_id='';
                    if($content_type == 2){
                        $trailerData = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
                        $trailer_id=$trailerData->id;
                        $stream_id=0;
                    }
                    $video_log->trailer_id = $trailer_id;
                    
                    if ($log_id > 0) {
                        $video_log = VideoLogs::model()->findByPk($log_id);
                        $video_log->updated_date = new CDbExpression("NOW()");
                    } else {
                        $video_log->created_date = new CDbExpression("NOW()");
                        $video_log->ip = $ip_address;
                        $video_log->video_length = $video_length;
                        $video_log->device_type = $device_type;
                    }
                    $video_log->played_length = $played_length;
                    $video_log->movie_id = $movie_id;
                    $video_log->video_id = $stream_id;
                    $video_log->user_id = $user_id;
                    $video_log->device_id = $device_id;
                    $video_log->content_type = $content_type;
                    $video_log->studio_id = $studio_id;
                    $video_log->watch_status = $watch_status;
                    $video_log->resume_time = $played_length;
                    if ($video_log->save()) {
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['log_id'] = $video_log->id;
                        $data['restrict_stream_id'] = $restrictDeviceId;
                        $data['no_of_views'] = @$activePlayCount;
                    } else {
                        $data['code'] = 500;
                        $data['status'] = "Error";
                        $data['msg'] = "Internal Server Error";
                    }
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Adding log is not allowed!";
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionVideoLogNew() {
        $data = array();

        if (isset($_REQUEST['ip_address']) && isset($_REQUEST['movie_id'])) {
            $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : '';
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $studio_id = $this->studio_id;
                $user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
                if($user_id){
                    $add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
                }else{
                    $add_video_log = 1;
                }
                $isAllowed = 0;
                $device_id = 0;
                if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
                    $isAllowed = 1;
                    $device_id = $_REQUEST['user_id'];
                }
                if ($add_video_log || $isAllowed) {
                    $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
                    $movie_id = Yii::app()->common->getMovieId($movie_code);

                    $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
                    $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

                    //Get stream Id
                    $stream_id = 0;
                    if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
                    } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
                    } else {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
                    }
                    $parameters = array();
                    $parameters['movie_id'] = $movie_id;
                    $parameters['stream_id'] = $stream_id;
                    $parameters['studio_id'] = $studio_id;
                    $activePlayCount = VideoLogs::model()->activePlayLog($parameters);

                    $content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
                    $restrictDeviceId = 0;
                    if($user_id > 0 && @$_REQUEST['is_streaming_restriction'] == 1) {
                        $restrictStreamId = (isset($_REQUEST['restrict_stream_id'])) ? $_REQUEST['restrict_stream_id'] : 0;
                        if(@$_REQUEST['is_active_stream_closed'] == 1){
                            RestrictDevice::model()->deleteData($restrictStreamId);
                        } else {
                            $postData = array();
                            $postData['id'] = $restrictStreamId;
                            $postData['studio_id'] = $studio_id;
                            $postData['user_id'] = $user_id;
                            $postData['movie_stream_id'] = $stream_id;
                            $restrictDeviceId = RestrictDevice::model()->dataSave($postData);
                        }
                    }
                    $dataForSave = array();
                    $dataForSave = $_REQUEST;
                    $dataForSave['movie_id'] = $movie_id;
                    $dataForSave['stream_id'] = $stream_id;
                    $dataForSave['studio_id'] = $studio_id;
                    $dataForSave['user_id'] = $user_id;
                    $dataForSave['ip_address'] = $ip_address;
                    $dataForSave['trailer_id'] = "";
                    $dataForSave['status'] = $_REQUEST['watch_status'];
                    $dataForSave['content_type'] = $content_type;
                    if($content_type == 2){
                        $trailerData = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
                        $dataForSave['trailer_id']=$trailerData->id;
                        $dataForSave['stream_id']="";
                    }
                    $video_log_id = VideoLogs::model()->dataSave($dataForSave);
                    if ($video_log_id) {
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['log_id'] = @$video_log_id[1];
                        $data['log_temp_id'] = @$video_log_id[0];
                        $data['restrict_stream_id'] = $restrictDeviceId;
                        $data['no_of_views'] = @$activePlayCount;
                    } else {
                        $data['code'] = 500;
                        $data['status'] = "Error";
                        $data['msg'] = "Internal Server Error";
                    }
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Adding log is not allowed!";
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionBufferLogs() {
        $data = array();
        if (isset($_REQUEST['ip_address']) && isset($_REQUEST['movie_id'])) {
            $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : '';
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $studio_id = $this->studio_id;
                $user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
                if($user_id){
                    $add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
                }else{
                    $add_video_log = 1;
                }
                $isAllowed = 0;
                $device_id = 0;
                if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
                    $isAllowed = 1;
                    $device_id = $_REQUEST['user_id'];
                }
                if ($add_video_log || $isAllowed) {
                    $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
                    $movie_id = Yii::app()->common->getMovieId($movie_code);

                    $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
                    $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

                    //Get stream Id
                    $stream_id = 0;
                    if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
                    } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
                    } else {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
                    }

                    $device_type = $_REQUEST['device_type'];
                    $resolution = (isset($_REQUEST['resolution'])) ? $_REQUEST['resolution'] : "";
                    $start_time = (isset($_REQUEST['start_time'])) ? $_REQUEST['start_time'] : 0;
                    $end_time = (isset($_REQUEST['end_time'])) ? $_REQUEST['end_time'] : 0;
                    $log_unique_id = (isset($_REQUEST['log_unique_id']) && trim(($_REQUEST['log_unique_id']))) ? $_REQUEST['log_unique_id'] : '';
                    $log_id = (isset($_REQUEST['log_id']) && trim(($_REQUEST['log_id']))) ? $_REQUEST['log_id'] : '';
                    $content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
                    $played_time = $end_time - $start_time;
                    $bandwidthType = 1;
                    if (@$_REQUEST['downloaded_bandwidth'] != '') {
                        $bandwidth_used = @$_REQUEST['downloaded_bandwidth'];
                        $bandwidthType = 2;
                    } else if(strtolower(@$_REQUEST['video_type']) == 'mped_dash'){
                         //mped_dash
                         $bandwidth_used = @$_REQUEST['totalBandwidth'];
                    } else{
                        if($content_type == 2){
                            $movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
                        } else{
                            $movie_stream_data = movieStreams::model()->findByPk($stream_id);
                        }
                        $res_size = json_decode($movie_stream_data->resolution_size, true);
                        $video_duration = explode(':', $movie_stream_data->video_duration);
                        $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
                        $size = $res_size[$resolution];
                        $bandwidth_used = ($size / $duration) * $played_time;
                    }

                    if (isset($log_id) && $log_id) {
                        //$buff_log = BufferLogs::model()->findByAttributes(array('unique_id' => $log_unique_id, 'studio_id' => $studio_id, 'user_id' => $user_id, 'movie_id' => $movie_id, 'resolution' => $resolution));
                        $buff_log = BufferLogs::model()->findByPk($log_id);

                        if (isset($buff_log) && !empty($buff_log)) {
                            $buff_log->end_time = $end_time;
                            $buff_log->buffer_size = $bandwidth_used;
                            $buff_log->played_time = $end_time - $buff_log->start_time;
                            $buff_log->save();
                            $buff_log_id = $log_id;
                            $unique_id = $log_unique_id;
                        }
                    } else {

                        if (isset($_REQUEST['location']) && $_REQUEST['location'] == 1) {
                            if ($device_type == 4) {
                                $buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id));
                            } else {
                                $buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id));
                            }
                            $city = $buff_log->city;
                            $region = $buff_log->region;
                            $country = $buff_log->country;
                            $country_code = $buff_log->country_code;
                            $continent_code = $buff_log->continent_code;
                            $latitude = $buff_log->latitude;
                            $longitude = $buff_log->longitude;
                        } else {
                            $ip_address = Yii::app()->request->getUserHostAddress();
                            $geo_data = new EGeoIP();
                            $geo_data->locate($ip_address);
                            $city = @$geo_data->getCity();
                            $region = @$geo_data->getRegion();
                            $country = @$geo_data->getCountryName();
                            $country_code = @$geo_data->getCountryCode();
                            $continent_code = @$geo_data->getContinentCode();
                            $latitude = @$geo_data->getLatitude();
                            $longitude = @$geo_data->getLongitude();
                        }
                        $unique_id = md5(uniqid(rand(), true));
                        $buff_log = new BufferLogs();
                        $buff_log->studio_id = $studio_id;
                        $buff_log->unique_id = $unique_id;
                        $buff_log->user_id = $user_id;
                        $buff_log->device_id = $device_id;
                        $buff_log->movie_id = $movie_id;
                        $buff_log->video_id = $stream_id;
                        $buff_log->resolution = $resolution;
                        $buff_log->start_time = $start_time;
                        $buff_log->end_time = $end_time;
                        $buff_log->played_time = $played_time;
                        $buff_log->buffer_size = $bandwidth_used;
                        $buff_log->city = $city;
                        $buff_log->region = $region;
                        $buff_log->country = $country;
                        $buff_log->country_code = $country_code;
                        $buff_log->continent_code = $continent_code;
                        $buff_log->latitude = $latitude;
                        $buff_log->longitude = $longitude;
                        $buff_log->device_type = $device_type;
                        $buff_log->content_type = $content_type;
                        $buff_log->ip = $ip_address;
                        $buff_log->bandwidth_type = $bandwidthType;
                        $buff_log->created_date = date('Y-m-d H:i:s');
                        $buff_log->save();
                        $buff_log_id = $buff_log->id;
                    }

                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['location'] = 1;
                    $data['log_id'] = $buff_log_id;
                    $data['log_unique_id'] = $unique_id;
                    $data['request_data'] = @$_REQUEST['request_data'];
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Adding log is not allowed!";
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionUpdateBufferLogs() {
        $data = array();
        
        if (isset($_REQUEST['ip_address']) && isset($_REQUEST['movie_id'])) {
            $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : '';
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $studio_id = $this->studio_id;
                $user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
                if($user_id){
                    $add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
                }else{
                    $add_video_log = 1;
                }
                $isAllowed = 0;
                $device_id = 0;
                if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
                    $isAllowed = 1;
                    $device_id = $_REQUEST['user_id'];
                }
                if ($add_video_log || $isAllowed) {
                    $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
                    $movie_id = Yii::app()->common->getMovieId($movie_code);

                    $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
                    $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

                    //Get stream Id
                    $stream_id = 0;
                    if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
                    } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
                    } else {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
                    }

                    $device_type = $_REQUEST['device_type'];
                    $resolution = $_REQUEST['resolution'];
                    $start_time = $_REQUEST['start_time'];
                    $end_time = $_REQUEST['end_time'];
                    $content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
                    $movie_stream_data = movieStreams::model()->findByPk($stream_id);
                    if($content_type == 2){
                        $movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
                    }
                    $res_size = json_decode($movie_stream_data->resolution_size, true);
                    $video_duration = explode(':', $movie_stream_data->video_duration);
                    $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);

                    $size = $res_size[$resolution];
                    $played_time = $end_time - $start_time;
                    $bandwidth_used = ($size / $duration) * $played_time;

                    $unique_id = md5(uniqid(rand(), true));
                    if (isset($_REQUEST['location']) && $_REQUEST['location'] == 1) {
                        if ($device_type == 4) {
                            $buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id));
                        } else {
                            $buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id));
                        }
                        
                        $city = $buff_log->city;
                        $region = $buff_log->region;
                        $country = $buff_log->country;
                        $country_code = $buff_log->country_code;
                        $continent_code = $buff_log->continent_code;
                        $latitude = $buff_log->latitude;
                        $longitude = $buff_log->longitude;
                    } else {
                        $ip_address = Yii::app()->request->getUserHostAddress();
                        $geo_data = new EGeoIP();
                        $geo_data->locate($ip_address);
                        $city = @$geo_data->getCity();
                        $region = @$geo_data->getRegion();
                        $country = @$geo_data->getCountryName();
                        $country_code = @$geo_data->getCountryCode();
                        $continent_code = @$geo_data->getContinentCode();
                        $latitude = @$geo_data->getLatitude();
                        $longitude = @$geo_data->getLongitude();
                    }
                    
                    $buff_log = new BufferLogs();
                    $buff_log->studio_id = $studio_id;
                    $buff_log->unique_id = $unique_id;
                    $buff_log->user_id = $user_id;
                    $buff_log->device_id = $device_id;
                    $buff_log->movie_id = $movie_id;
                    $buff_log->video_id = $stream_id;
                    $buff_log->resolution = $resolution;
                    $buff_log->start_time = $start_time;
                    $buff_log->end_time = $end_time;
                    $buff_log->played_time = $played_time;
                    $buff_log->buffer_size = $bandwidth_used;
                    $buff_log->city = $city;
                    $buff_log->region = $region;
                    $buff_log->country = $country;
                    $buff_log->country_code = $country_code;
                    $buff_log->continent_code = $continent_code;
                    $buff_log->latitude = $latitude;
                    $buff_log->longitude = $longitude;
                    $buff_log->device_type = $device_type;
                    $buff_log->content_type = $content_type;
                    $buff_log->ip = $ip_address;
                    $buff_log->created_date = date('Y-m-d H:i:s');
                    $buff_log->save();
                    $buff_log_id = $buff_log->id;

                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['log_id'] = $buff_log_id;
                    $data['log_unique_id'] = $unique_id;
                    $data['location'] = 1;
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Adding log is not allowed!";
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method getmenuList() Returns the list of content Categories
     * @param String $authToken A authToken
     * @author Gayadhar<support@muvi.com>
     * @return Json Facebook user status
     */
    function actionGetMenuList() {
        $list = array();
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code); 
        $studio_id   = $this->studio_id;
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code, $studio_id);        
		$menuType = 'top'; // For web only
		if((@$_REQUEST['menutype'] || @$_REQUEST['menuType'])){
			$menuType = @$_REQUEST['menutype']?$_REQUEST['menutype']:$_REQUEST['menuType'];
		}
        $sql = "SELECT mi.id,mi.link_type,mi.parent_id,mi.title as display_name,mi.permalink FROM menu_items mi,menus m WHERE mi.menu_id = m.id AND m.position='{$menuType}' AND mi.studio_id={$this->studio_id} AND language_parent_id = 0 ORDER BY parent_id,id_seq ASC"; 
        $list = Yii::app()->db->createCommand($sql)->queryAll();
        if ($language_id != 20) {
            $list = Yii::app()->custom->getTranslatedMenuList($this->studio_id, $language_id, $list);
        }
		$finalArr = array();
        if ($list) {
            foreach ($list AS $key => $val) {
                if ($val['link_type'] == 1) {
                    if (strstr($val['permalink'], 'http://') || strstr($val['permalink'], 'httpS://')) {
						$list[$key]['web_url'] = $val['permalink'];
                    } else {
                        $list[$key]['web_url'] = 'http://' . $defaultdomain . '/page/' . $val['permalink'];
					}
                } elseif ($val['permalink'] == '#') {
                    $list[$key]['web_url'] = 'http://' . $defaultdomain;
                } else {
                    if (strstr($val['permalink'], 'http://') || strstr($val['permalink'], 'httpS://')) {
						$list[$key]['web_url'] = $val['permalink'];
                    } else {
                        $list[$key]['web_url'] = 'http://' . $defaultdomain . '/' . $val['permalink'];
					}
				}
				if($val['parent_id']==0){
						$parentArr[]= $list[$key];
					}else{
						$childArr[$val['parent_id']][] = $list[$key]; 
					}
				}
				$finalArr = @$parentArr;
				foreach (@$parentArr AS $key=>$val){
					if(@$childArr[$val['id']]){
						$finalArr = array_merge($finalArr,$childArr[$val['id']]);
					}
				}
		}
			$sql = "SELECT domain, p.link_type,p.id, p.display_name, p.permalink, IF(p.link_type='external', p.external_url, concat('http://', domain, '/page/',p.permalink)) AS url FROM 
                    (SELECT id_seq, studio_id, external_url,id, link_type, title as display_name, permalink FROM pages WHERE studio_id={$this->studio_id} AND parent_id=0 AND status=1 AND permalink != 'terms-privacy-policy') AS p LEFT JOIN studios AS s ON p.studio_id=s.`id` ORDER BY p.id_seq ASC ";
            $pages = Yii::app()->db->createCommand($sql)->queryAll();
            if ($language_id != 20) {
                $pages = Yii::app()->custom->getStaticPages($this->studio_id, $language_id, $pages);
            }
            $defaultdomain = @$pages[0]['domain'];
            if (trim($defaultdomain) == "") {
                $studio_details = Studios::model()->findByPk($this->studio_id, array('select' => 'domain'));
                $defaultdomain = $studio_details->domain;
            }
            $default = array(0 => 0, 'link_type' => 'internal', 'display_name' => $translate['contact_us'], 'permalink' => 'contactus', 'url' => 'http://' . $defaultdomain . '/contactus');
            array_push($pages, $default);
			
            $data['status'] = 'OK';
            $data['code'] = 200;
            $data['msg'] = $translate['success'];
            $data['total_menu'] = count(@$list);
            $data['menu'] = $finalArr;
            $data['footer_menu'] = $pages;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
/**
* @method updateUserProfile() Returns the profile details along with profile image
* @param String $authToken A authToken
* @param array $data profile data in array format
* @param array $image optional image info 
* @author Gayadhar<support@muvi.com>
* @return Json user details 
*/
function actionUpdateUserProfile() {
        if ($_REQUEST['user_id']) {
            if ($_FILES && ($_FILES['error'] == 0)) {
                $ret = $this->uploadProfilePics($_REQUEST['user_id'], $this->studio_id, $_FILES['file']);
            }
            $sdkUsers = SdkUser::model()->findByPk($_REQUEST['user_id']);
            if ($sdkUsers) {
                if (@$_REQUEST['nick_name']) {
                    $sdkUsers->nick_name = $_REQUEST['nick_name'];
                }
                if (@$_REQUEST['name']) {
                    $sdkUsers->display_name = $_REQUEST['name'];
                }
                if (@$_REQUEST['mobile_number'] && is_numeric($_REQUEST['mobile_number'])) {
                    $sdkUsers->mobile_number = @$_REQUEST['mobile_number'];
                }
                if (@$_REQUEST['password']) {
                    $enc = new bCrypt();
                    $pass = $enc->hash($_REQUEST['password']);
                    $sdkUsers->encrypted_password = $pass;
                }
                $sdkUsers->last_updated_date = new CDbExpression("NOW()");
                $sdkUsers->save();
                $is_user = Yii::app()->custom->updateCustomFieldValue($_REQUEST['user_id'], $this->studio_id);

                $data['status'] = 'OK';
                $data['code'] = 200;
                $data['msg'] = 'Success';
                $data['name'] = $sdkUsers->display_name;
                $data['email'] = $sdkUsers->email;
                $data['nick_name'] = $sdkUsers->nick_name;
                $data['mobile_number'] = $sdkUsers->mobile_number;
                $data['profile_image'] = $this->getProfilePicture($_REQUEST['user_id'], 'profilepicture', 'thumb', $this->studio_id);
            } else {
                $data['code'] = 448;
                $data['status'] = "Error";
                $data['msg'] = "User does not exist";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "User id required";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function uploadProfilePics($user_id,$studio_id,$fileinfo){
		if(!$studio_id){$studio_id = $this->studio_id;}

		//Checking for existing object
		$old_poster = new Poster;
		$old_posters = $old_poster->findAllByAttributes(array('object_id' => $user_id, 'object_type' => 'profilepicture'));
		if(count($old_posters) > 0){
			foreach ($old_posters as $oldposter) {
				$oldposter->delete();
			}
		}
		$ip_address = CHttpRequest::getUserHostAddress(); 
		$poster = new Poster();
		$poster->object_id = $user_id;
		$poster->object_type = 'profilepicture';
		$poster->poster_file_name = $fileinfo['name'];
		$poster->ip = $ip_address;
		$poster->created_by = $user_id;
		$poster->created_date = new CDbExpression("NOW()");
		$poster->save();
		$poster_id = $poster->id;                    
		
		$fileinfo['name'] = Yii::app()->common->fileName($fileinfo['name']);
		$_FILES['Filedata'] = $fileinfo;
		$dir = $_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'/images/public/system/profile_images/'.$poster_id;
		if (!file_exists($dir)) {
			mkdir($dir, 0777);
		}
		$dir = $dir.'/original';
		if (!file_exists($dir)) {
			mkdir($dir, 0777);
		}
		move_uploaded_file($fileinfo['tmp_name'], $dir.'/'.$fileinfo['name']);
		
		//$uid = $_REQUEST['movie_id'];
		require_once "Image.class.php";
		require_once "Config.class.php";
		require_once "ProfileUploader.class.php";
		spl_autoload_unregister(array('YiiBase', 'autoload'));
		require_once "amazon_sdk/sdk.class.php";
		spl_autoload_register(array('YiiBase', 'autoload'));

		define( "BASEPATH",dirname(__FILE__) . "/.." );
		$config = Config::getInstance();
		$config->setUploadDir($_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'images/public/system/profile_images'); //path for images uploaded
		$bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
		$config->setBucketName($bucketInfo['bucket_name']);
		$s3_config = Yii::app()->common->getS3Details($studio_id);
		$config->setS3Key($s3_config['key']);
		$config->setS3Secret($s3_config['secret']);
		$config->setAmount( 250 );  //maximum paralell uploads
		$config->setMimeTypes( array( "jpg" , "gif" , "png",'jpeg' ) ); //allowed extensions
		$config->setDimensions( array('small'=>"150x150",'thumb'=>"100x100") );   //resize to these sizes
		//usage of uploader class - this simple :)
		$uploader = new ProfileUploader($poster_id);
		$folderPath = Yii::app()->common->getFolderPath("",$studio_id);
		$unsignedBucketPath = $folderPath['unsignedFolderPath'];
		$ret = $uploader->uploadprofileThumb($unsignedBucketPath."public/system/profile_images/");
		return true;
	}
/**
* @method GetStatByType() Return the 
* @param String $authToken A authToken
* @param array $filterType filter on specific type like genre, language,rating etc
* @author Gayadhar<support@muvi.com>
* @return Json user details 
*/
function actionGetStatByType() {
		if($_REQUEST['filterType']){
			$filterType =strtolower(trim($_REQUEST['filterType'])); 
			if($filterType == 'genre'){
				$genreArr = array();
            $gsql = 'SELECT genre, COUNT(*) AS cnt FROM films WHERE studio_id ='.$this->studio_id .' AND genre is not NULL AND genre !=\'\' AND genre !=\'null\' GROUP BY genre';
				$con = Yii::app()->db;
				$res = $con->createCommand($gsql)->queryAll();
				if($res){
						$data['status'] = 'OK';
						$data['code'] = 200;
						$data['msg'] = 'Success';
                foreach($res As $key=>$val) {
                    $expArr = json_decode($val['genre'],TRUE);
                    if(is_array($expArr)) {
                        foreach ($expArr AS $k=>$v){
                            $mygenre = strtolower(trim($v));
                            $data['content_statistics'][$mygenre] += $val['cnt'];
							}
						}
					}
            } else {
					$data['code'] = 451;
					$data['status'] = "Error";
					$data['msg'] = "No valid genre found with your data";
				}
        } else {
				$data['code'] = 450;
				$data['status'] = "Error";
				$data['msg'] = "A valid filter type is required";
			}
    } else {
			$data['code'] = 449;
            $data['status'] = "Error";
            $data['msg'] = "Please mention a filter type like genre,language,rating etc";
		}
		$this->setHeader($data['code']);
        echo json_encode($data);exit;
	}
        
/**
* @method addVideoViewLogs() It will add the video view logs, Increase the video view count,Icrease the played duration, Increase the buffer size
* @param String $authToken A authToken
* @param Integer user_id 
* @param Integer movie_id 
* @param Integer stream_id
* @param String	played_duration
* @param String	buffer_length
* @author Gayadhar<support@muvi.com>
* @return Json Success/failuer
*/
	function actionAddVideoViewLogs(){
		$req = $_REQUEST;
		if(@$req['movie_id']){
			$movie_id =  isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : 0;
			$stream_id = isset($_REQUEST['stream_id']) ? $_REQUEST['stream_id'] : 0;
			$video_log = new VideoLogs();
			$log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;
			if ($log_id > 0) {
				$video_log = VideoLogs::model()->findByPk($log_id);
				$video_log->updated_date = new CDbExpression("NOW()");
				$video_log->played_length = @$_REQUEST['played_duration']?@$_REQUEST['played_duration']:0;
			} else {
				$video_log->created_date = new CDbExpression("NOW()");
				$video_log->ip = $ip_address;
				$video_log->video_length = @$_REQUEST['buffer_length']?@$_REQUEST['buffer_length']:0;
			}
			$video_log->movie_id = $movie_id;
			$video_log->video_id = $stream_id;
			$video_log->user_id = @$_REQUEST['user_id']?@$_REQUEST['user_id']:0;
			$video_log->studio_id = $this->studio_id;
			$video_log->watch_status = @$_REQUEST['status']?@$_REQUEST['status']:'start';
			if ($video_log->save()) {
				$data['code'] = 200;
				$data['status'] = "Success";
				$data['msg'] = "Video Log updated successfully";
				$data['log_id'] = $video_log->id;
			} else {
				$data['code'] = 452;
				$data['status'] = "Error";
				$data['msg'] = "Error in saving data";
			}
		}else{
			$data['code'] = 453;
            $data['status'] = "Error";
            $data['msg'] = "Movie id is required";
		}
		$this->setHeader($data['code']);
        echo json_encode($data);exit;
	}
	
	/* Functions to set header with status code. eg: 200 OK ,400 Bad Request etc.. */

    private function setHeader($status) {
		$status = 200;
		ob_clean();
		ob_flush();
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type = "application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
        header('X-Powered-By: ' . "Muvi <support@muvi.com>");
    }

    private function _getStatusCodeMessage($status) {
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            407 => 'AuthToken Required',
            406 => 'Invalid Login Details',
            408 => 'Invalid authToken',
            409 => 'AuthToken not registerd with Muvi',
            410 => 'Expired authToken',
            411 => 'Content type permalink required!',
            412 => 'Invalid Content type permalink!',
            413 => 'Content permalink is invalid!',
            414 => 'Content permalink required!',
            415 => 'Invalid Content!',
            416 => 'Search Parameter required!',
            417 => 'A valid Email required!',
            418 => 'Invalid Email!',
            419 => 'No active payment gateways!',
            420 => 'Invalid Plan!',
            421 => 'Error in processing payment!',
            422 => 'Email already exists with the studio!',
            423 => 'A valid email address required!',
            424 => 'Payment is not enabled for this studio!',
            425 => 'Please activate your subscription to watch video.',
            426 => 'Please reactivate your subscription to watch video.',
            427 => 'This video is not allowed to play in your country.',
            428 => 'Sorry, you have crossed the maximum limit of watching this content.',
            429 => 'You will be allow to watch this video.',
            430 => 'Please purchase this video to watch it.',
            55 => 'We are not able to process your credit card. Please try another card.',
            432 => 'Coupon Valid',
            433 => 'Coupon Invalid',
            434 => 'No banner uploaded',
            435 => 'Facebook user id required',
            436 => 'Account suspended, Contact admin to login',
            437 => 'Content id and Stream id required',
            438 => 'No content availbe for the information',
            439 => 'PPV not enabled for this content',
            440 => 'A channel id required',
            441 => 'Invalid Channel id',
            442 => 'No user found!',
            443 => 'Email & user id required!',
            444 => 'IP address required!',
            445 => 'IP address is not allowed!',
            446 => 'Adding log is not allowed!',
            447 => 'No menu Found',
            448 => 'User id required to update profile',
            449 => 'Filter type like genre,language,rating etc',
            450 => 'A valid filterType is require',
            451 => 'Genre not found in your data',
            452 => 'Error in saving data',
            453 => 'Movie id is required',
            454 => 'Account is Blocked in this country',
            455 => 'Registration is not Enabled',
            456 => 'Card can not be saved',
            457 => 'No subscription plans found!',
			458 => 'Required params are missing!',
			459 => 'Invalid Login credential',
			460 => 'No studio available with the given credential!',
			461 => 'Invalid Login credential',
			462 => 'AuthToken not available, Contact admin to generate the token',
            463 => 'Your IP looks suspicious',
            464 => 'Cast permalink is invalid!',
            465 => 'Cast permalink required!',
            466 => 'Invalid Cast!',
            467 => 'Insufficient Data',
            468 => 'Banned user',
            469 => 'Rating disabled',
			471 => 'Please ener image key'
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    public function actiongetEmbedUrl() {
        $movieStreamData = movieStreams::model()->find('studio_id=:studio_id AND id=:id', array(':studio_id' => $this->studio_id, ':id' => $_REQUEST['movie_stream_id']));
        if($movieStreamData){   
            $data['status'] = 'OK';
            $data['code'] = 200;
            $data['msg'] = 'Success';
			$domainName = Yii::app()->getBaseUrl(TRUE);
            $sql = "SELECT is_embed_white_labled, domain FROM studios WHERE id=".$this->studio_id;
            $stData = Yii::app()->db->createCommand($sql)->queryAll();
			if(@$stData[0]['is_embed_white_labled']){
				 $domainName = @$stData[0]['domain']? 'http://'.@$stData[0]['domain']:$domainName; 
			}				
			$data['emed_url'] = $domainName . '/embed/'.$movieStreamData->embed_id;
        } else {
            $data['code'] = 447;
            $data['status'] = "Error";
            $data['msg'] = "No data found!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public CheckGeoBlock() Get IP Address from API request and check if allowed in current country
     * @author RKS<support@muvi.com>
     * @return json Json data with parameters
     * @param string $authToken Ouath Token generated during registartion.
     * @param string $ip_address IP Address of the users device
     */
    
    public function actionCheckGeoBlock(){
        $ip_address = isset($_REQUEST['ip'])?$_REQUEST['ip']:'No IP';
        if($this->studio_id == 1977){
            //Keeping the log of API calls for Maxmind
            $log = new ApiRequestIp();
            $log->ip_address = $ip_address;
            $log->studio_id = $this->studio_id;
            $log->save();  
        }
        if(isset($_REQUEST) && count($_REQUEST) > 0 && isset($_REQUEST['ip'])){
            $ip_address = $_REQUEST['ip'];
            $has_error = 0;                                   
            
            $check_anonymous_ip = Yii::app()->mvsecurity->hasMaxmindSuspiciousIP($this->studio_id);
            if($check_anonymous_ip == 1)
            {                
                $is_anonymous = Yii::app()->mvsecurity->checkAnonymousIP($ip_address);
                if($is_anonymous > 0){
                    $has_error = 1;
                    $data['code'] = 463;
                    $data['status'] = "OK";
                    $data['msg'] = "Your IP looks suspicious. Please contact admin/technical support team at support@vishwammedia.com.";                    
                }
            }
            if($has_error == 0){
                $visitor_loc = Yii::app()->common->getVisitorLocation($ip_address);
                $country = $visitor_loc['country'];
                $std_countr = StudioCountryRestriction::model();
                $studio_restr = $std_countr->findByAttributes(array('studio_id' => $this->studio_id, 'country_code' => $country));
                if(count($studio_restr) > 0){
                    $data['code'] = 454;
                    $data['msg'] = "Account is Blocked in this country"; 
                }else{
                    $data['code'] = 200;                
                    $data['msg'] = "Website can be accessed from your country";  
                }
                $data['status'] = "OK";
                $data['country'] = $country;
            }
        }else{
            $data['code'] = 444;
            $data['status'] = "failure";
            $data['msg'] = "IP address required!";              
        }
        $this->setHeader($data['code']);
        echo json_encode($data);        
    } 
    
    
    public function actionIsRegistrationEnabled() {
		$studio_id = $this->studio_id;
        $studioData = Studio::model()->findByPk($this->studio_id);
        $is_registration = $studioData->need_login;
        $chromecast = 0;
        $offline = 0;
        $app = Yii::app()->general->apps_count($studio_id); 
        if(empty($app) || (isset($app) && ($app['apps_menu'] & 2)) || (isset($app) && ($app['apps_menu'] & 4))){
            $chromecast_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'chromecast');
            if ($chromecast_val['config_value'] == 1 || $chromecast_val == '') {
                $chromecast = 1;
            }
            $offline_val= StudioConfig::model()->getconfigvalueForStudio($studio_id,'offline_view');
            if($offline_val['config_value'] == 1 || $offline_val ==''){
                $offline=1;
            }
        }
        if (intval($is_registration)) {
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['isMylibrary'] = (int) $studioData->mylibrary_activated;
			$data['isRating'] = (int) $studioData->rating_activated;
            $data['is_login'] = 1;
            $data['has_favourite'] = $this->CheckFavouriteEnable($studio_id);
            $general = new GeneralFunction;
            $data['signup_step'] = $general->signupSteps($this->studio_id);
			$device_status = StudioConfig::model()->getConfig($studio_id,'restrict_no_devices');
            $data['isRestrictDevice'] = (isset($device_status['config_value']) && ($device_status['config_value'] != 0)) ? 1 : 0;
            $getDeviceRestrictionSetByStudioData = StudioConfig::model()->getConfig($studio_id, 'restrict_streaming_device');
            $getDeviceRestrictionSetByStudio = 0;
            if(@$getDeviceRestrictionSetByStudioData->config_value != "" && @$getDeviceRestrictionSetByStudioData->config_value > 0){
                $getDeviceRestrictionSetByStudio = 1;
            }
            $data['is_streaming_restriction'] = $getDeviceRestrictionSetByStudio;
        } else {
            $data['code'] = 455;
            $data['status'] = "failure";
            $data['is_login'] = 0;
        }
        $data['chromecast'] = $chromecast;
        $data['is_offline']=$offline;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    public function actiongetWebsiteSettings() {
        $studioData = Studio::model()->findByPk($this->studio_id);
        $is_registration = $studioData->need_login;
        if (intval($is_registration)) {
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['isMylibrary'] = (int) $studioData->mylibrary_activated;
            $data['is_login'] = 1;
            $data['has_favourite'] = $this->CheckFavouriteEnable($studio_id);
            $general = new GeneralFunction;
            $data['signup_step'] = $general->signupSteps($this->studio_id);
        } else {
            $data['code'] = 455;
            $data['status'] = "failure";
            $data['is_login'] = 0;
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }    

    function getDomainName(){
        $sql = "SELECT is_embed_white_labled, domain FROM studios WHERE id=".$this->studio_id;
        $stData = Yii::app()->db->createCommand($sql)->queryAll();
        $domainName = Yii::app()->getBaseUrl(TRUE);
        if(@$stData[0]['is_embed_white_labled']){
                 $domainName = @$stData[0]['domain']? 'http://'.@$stData[0]['domain']:$domainName; 
        }
        return $domainName;
    }
    
    function actionGetCardsListForPPV() {
        $studio_id = $this->studio_id;
        $user_id = @$_REQUEST['user_id'];
        $data = array();
        
        if ($user_id) {
            $can_save_card = 0;
            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                if(isset($gateways[0]) && !empty($gateways[0])){
                    $can_save_card = (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) ? $gateways[0]['paymentgt']['can_save_card'] : 0;
                }
            }
            
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['can_save_card'] = $can_save_card;
            
            $card_sql = "SELECT tot.card_id, tot.card_name, tot.card_holder_name, tot.card_last_fourdigit, tot.card_type FROM (SELECT card_uniq_id AS card_id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
            $cards = Yii::app()->db->createCommand($card_sql)->queryAll();

            if (!empty($cards)) {
                $data['cards'] = $cards;
            } else {
                $data['msg'] = "No card found!";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "User id required";
            
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    function actiongetCategoryList(){
        $studio_id = $this->studio_id;
        $lang_code   = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate   = $this->getTransData($lang_code, $studio_id);
        $catlists = ContentCategories::model()->findAll('studio_id=:studio_id AND parent_id = 0 ORDER BY id DESC', array(':studio_id' => $studio_id));
        if($language_id !=20){
            $translated = CHtml::listData(ContentCategories::model()->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select'=>'parent_id,title')),'parent_id','title');
            if(!empty($catlists)){
                foreach($catlists as $cats){
                    if (array_key_exists($cats->id, $translated)) {
                       $cats->title = $translated[$cats->id];
                    }
                    $cat[] = $cats;
                }
            }
        }else{
            $cat = $catlists;
        }
        if(!empty($cat)){
            $cat_img_size = Yii::app()->custom->getCatImgSize($studio_id);
			foreach($cat as $k => $cat_data) {
				$list[$k]['category_id'] = $cat_data['id'];
				$list[$k]['category_name'] = $cat_data['category_name'];        
				$list[$k]['permalink'] = $cat_data['permalink'];
				$list[$k]['category_img_url'] = $this->getPoster($cat_data['id'], 'content_category','original',$studio_id);    
				$list[$k]['cat_img_size'] = @$cat_img_size;    
			}
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['category_list'] = $list;
        }else{
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "No category added yet";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    function actiongetGenreList() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
		$customGenre = CustomMetadataField::model()->find("studio_id=:studio_id AND f_id='genre' ", array(':studio_id' => $studio_id));
		if ($customGenre) {
            $allgenre= json_decode($customGenre->f_value, TRUE);
            
              
               if((isset($allgenre[$lang_code])) && is_array($allgenre[$lang_code]))
               {
                $data['genre_list']= $allgenre[$lang_code]; 
               }
               else if((isset($allgenre['en'])) && is_array($allgenre['en']))
               {
                $data['genre_list']= $allgenre['en'];    
               }
               else {
                 $data['genre_list']=  $allgenre ; 
               }
               
		} else {
            //$generes = MovieTag::model()->findAll('studio_id=:studio_id AND taggable_type=:taggable_type ORDER BY name', array(':studio_id' => $studio_id, ':taggable_type' => 1));         
            $criteria = new CDbCriteria();
            $criteria->select = 't.id,t.name';
            $criteria->condition = 't.studio_id=:studio_id AND t.taggable_type=:taggable_type';
            $criteria->params = array(':studio_id' => $studio_id, ':taggable_type' => 1);
            $criteria->group = 't.name';
            $criteria->order = 't.name ASC';
            $generes = MovieTag::model()->findAll($criteria);

			if ($generes) {
				foreach ($generes as $gen_data) {
					$data['genre_list'][] = $gen_data['name'];
				}
			}
		}
		if (@$data['genre_list']) {
			$data['code'] = 200;
			$data['status'] = "OK";
		} else {
			$data['code'] = 448;
			$data['status'] = "Error";
			$data['msg'] = "no_genre_found";
		}
		$this->setHeader($data['code']);
		echo json_encode($data);
		exit;
	}
    
    /**
     * @method public CreatePpvPayPalPaymentToken() create a payment token for paypal express checkout
     * @author SKM<sanjeev@muvi.com>
     * @return json Json data with parameters
    */
    public function actionCreatePpvPayPalPaymentToken() {
        $request_data = $_REQUEST;
        $plan_id = $request_data['plan_id'];
        $user_id = $request_data['user_id'];
        $studio_id = $request_data['studio_id'];
        $authToken = $request_data['authToken'];
        $studio = Studio::model()->findByPk($studio_id);
        
        if(isset($plan_id) && $plan_id > 0){
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
            }
            $VideoDetails = '';
            $Films = Film::model()->findByAttributes(array('studio_id'=>$studio_id, 'uniq_id'=>$request_data['movie_id']));
            $video_id = $Films->id;
            //Check studio has plan or not
            $is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);
            
            $is_bundle = @$request_data['is_bundle'];
            $timeframe_id = @$request_data['timeframe_id'];
            $isadv = @$request_data['is_advance_purchase'];
            $timeframe_days = 0;
            $end_date = '';
            
            //Get plan detail which user selected at the time of registration
            $plan = PpvPlans::model()->findByPk($plan_id);
            $request_data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;
            
            if (intval($is_bundle) && intval($timeframe_id)) {
                (array)$price = Yii::app()->common->getTimeFramePrice($plan_id, $request_data['timeframe_id'], $studio_id);
                $price = $price->attributes;

                $timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;
            } else {
                $plan_details = PpvPricing::model()->findByAttributes(array('ppv_plan_id'=>$plan_id,'status'=>1));
                $price = Yii::app()->common->getPPVPrices($plan->id, $plan_details->currency_id);
            }
            $currency = Currency::model()->findByPk($price['currency_id']);
            $data['currency_id'] = $currency->id;
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;
            
            
            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');

            if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
                $time = strtotime($start_date);
                $expiry = $timeframe_days.' '."days";
                $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
            } else {
                if (intval($isadv) == 0) {
                    $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                    $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;
                        
                    $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                    $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

                    $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                    $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';

                    $time = strtotime($start_date);
                    if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                        $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                    $data['view_restriction'] = $limit_video;
                    $data['watch_period'] = $watch_period;
                    $data['access_period'] = $access_period;
            }
            }
            
            if ($is_bundle) {//For PPV bundle
                    $data['season_id'] = 0;
                    $data['episode_id'] = 0;
                    
                if (intval($is_subscribed_user)) {
                    if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                        $data['amount'] = $amount = $price['show_subscribed'];
                    } else {
                        $data['amount'] = $amount = $price['price_for_subscribed'];
                    }
                } else {
                    if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                        $data['amount'] = $amount = $price['show_unsubscribed'];
                    } else {
                        $data['amount'] = $amount = $price['price_for_unsubscribed'];
                    }
                }
            } else {
                //Set different prices according to schemes
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                    //Check which part wants to sell by studio admin
                    $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                    $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                    $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                    $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                    if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id'])!= '0') { //Find episode amount
                        if (intval($is_episode)) {
                            $streams = movieStreams::model()->findByAttributes(array('studio_id'=>$studio_id, 'embed_id'=>$data['episode_id']));
                            $data['episode_id'] = $streams->id;
                            $data['episode_uniq_id'] = $streams->embed_id;

                            if (intval($is_subscribed_user)) {
                                $data['amount'] = $amount = $price['episode_subscribed'];
                            } else {
                                $data['amount'] = $amount = $price['episode_unsubscribed'];
                            }
                        }
                    } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                        if (intval($is_season)) {
                            $data['episode_id'] = 0;

                            if (intval($is_subscribed_user)) {
                                $data['amount'] = $amount = $price['season_subscribed'];
                            } else {
                                $data['amount'] = $amount = $price['season_unsubscribed'];
                            }

                            //$end_date = '';//Make unlimited time limit for season
                        }
                    } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                        if (intval($is_show)) {
                            $data['season_id'] = 0;
                            $data['episode_id'] = 0;
                            if (intval($is_subscribed_user)) {
                                $data['amount'] = $amount = $price['show_subscribed'];
                            } else {
                                $data['amount'] = $amount = $price['show_unsubscribed'];
                            }

                            //$end_date = '';//Make unlimited time limit for show
                        }
                    }
                } else {//Single part videos
                    $data['season_id'] = 0;
                    $data['episode_id'] = 0;

                    if (intval($is_subscribed_user)) {
                        $data['amount'] = $amount = $price['price_for_subscribed'];
                    } else {
                        $data['amount'] = $amount = $price['price_for_unsubscribed'];
                    }
                }
            }
            $couponCode = '';
            //Calculate coupon if exists
            if((isset($request_data['coupon']) && $request_data['coupon'] != '') || (isset($request_data['coupon_instafeez']) && $request_data['coupon_instafeez'] != '')){
                $coupon = (isset($request_data['coupon']) && $request_data['coupon'] != '') ? $request_data['coupon'] : $request_data['coupon_instafeez'];
                $getCoup = Yii::app()->common->getCouponDiscount($coupon, $amount,$studio_id,$user_id,$data['currency_id']);
                $data['amount'] = $amount = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
            }
            if (intval($is_bundle)) {
                $VideoName = $plan->title;
            } else {
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);
            }

            $VideoDetails = $VideoName;
            
            if (!$is_bundle && isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                if($data['season_id'] == 0){
                    $VideoDetails .= ', All Seasons';
                }else{
                    $VideoDetails .= ', Season '.$data['season_id'];
                    if($data['episode_id'] == 0){
                        $VideoDetails .= ', All Episodes';
                    }else{
                        $episodeName = Yii::app()->common->getEpisodename($streams->id);
                        $episodeName = trim($episodeName);
                        $VideoDetails .= ', '.$episodeName;
                    }
                }
            }
            
            $data['studio_id'] = $studio_id;

            $data['paymentDesc'] = $VideoDetails.' - '.$data['amount'];
            
            $log_data = array (
                    'studio_id'=>$studio_id,
                    'user_id'=>$user_id,
                    'plan_id'=>$plan_id,
                    'uniq_id'=>$request_data['movie_id'],
                    'amount'=>$data['amount'],
                    'episode_id'=>$request_data['episode_id'],
                    'episode_uniq_id'=>$request_data['episode_uniq_id'],
                    'season_id'=>$request_data['season_id'],
                    'permalink'=>$request_data['permalink'],
                    'currency_id'=>$data['currency_id'],
                    'couponCode'=>$request_data['coupon'],
                    'is_bundle'=>@$request_data['is_bundle'],
                    'timeframe_id'=>@$request_data['timeframe_id'],
                    'isadv'=>$isadv
                );
            $timeparts = explode(" ",microtime());
            $currenttime = bcadd(($timeparts[0]*1000),bcmul($timeparts[1],1000));
            $reference_number = explode('.', $currenttime);
            $unique_id = $reference_number[0];
            $log = new PciLog();
            $log->unique_id = $unique_id;
            $log->log_data = json_encode($log_data);
            $log->save();
            if (strpos($studio->domain, 'idogic.com') !== false || strpos($studio->domain, 'muvi.com') !== false) {
                $pre = 'http://www.';
            }else{
                $pre = 'https://www.';
            }
            
            $muvi_token = $unique_id."-".$authToken;
            
            $data['returnURL'] = $pre.$studio->domain."/rest/ppvSuccess?muvi_token=".$muvi_token;
            $data['cancelURL'] = $pre.$studio->domain."/rest/ppvPayPalCancel?muvi_token=".$muvi_token;
            $data['user_id'] = $user_id;
            $payment_gateway_controller = 'ApipaypalproController';
            Yii::import('application.controllers.wrapper.'.$payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $resArr = $payment_gateway::createToken($data);
            if($resArr['ACK'] == 'Success') {
                $res['code'] = 200;
                $res['status'] = "OK";
                $res['url'] =  $resArr['REDIRECTURL'];
                $res['token'] =  $resArr['TOKEN'];
            }else{
                $res['code'] = 458;
                $res['status'] = "Error";
                $res['msg'] = "Transaction error!";
            }
        }else{
            $res['code'] = 457;
            $res['status'] = "Error";
            $res['msg'] = "No plans found!";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);exit;
    }
    /**
     * @method public PpvSuccess() handles paypal retun token and do a expresscheckout
     * @author SKM<sanjeev@muvi.com>
     * @return json Json data with parameters
    */
    public function actionPpvSuccess() {
        $muvi_token = explode('-',$_REQUEST['muvi_token']);
        $unique_id = $muvi_token[0];
        if(isset($unique_id) && trim($unique_id)){
            $log_data = PciLog::model()->findByAttributes(array('unique_id'=>$unique_id));
            $data = json_decode($log_data['log_data'],true);
            $studio_id = $data['studio_id'];
            $user_id = $data['user_id'];
            
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
            }
            
            $payment_gateway_controller = 'ApipaypalproController';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $trans_data = $payment_gateway::processCheckoutDetails($_REQUEST['token']);
            $userdata['TOKEN'] = $trans_data['TOKEN'];
            $userdata['PAYERID'] = $trans_data['PAYERID'];
            $userdata['AMT'] = $trans_data['AMT'];
            $userdata['CURRENCYCODE'] = $trans_data['CURRENCYCODE'];
            $resArray = $payment_gateway::processDoPayment($userdata);
            
            if(isset($resArray) && $resArray['ACK'] == 'Success') {
                $plan_id = $data['plan_id'];
                $ip_address = CHttpRequest::getUserHostAddress();
                $isadv = $data['isadv'];
                $planModel = new PpvPlans();
                $plan = $planModel->findByPk($plan_id);
                
                $start_date = Date('Y-m-d H:i:s');
                $end_date = '';
                if (intval($isadv) == 0) {
                    $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                    $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                    $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                    $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

                    $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                    $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';

                    $time = strtotime($start_date);
                    if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                        $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                    $data['view_restriction'] = $limit_video;
                    $data['watch_period'] = $watch_period;
                    $data['access_period'] = $access_period;
                }

                $Films = Film::model()->findByAttributes(array('studio_id'=>$studio_id, 'uniq_id'=>$data['uniq_id']));
                $video_id = $Films->id;
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);
                $VideoDetails = $VideoName;
                $streams = movieStreams::model()->findByAttributes(array('studio_id'=>$studio_id, 'embed_id'=>$data['episode_id']));
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if($data['season_id'] == 0){
                        $VideoDetails .= ', All Seasons';
                    }else{
                        $VideoDetails .= ', Season '.$data['season_id'];
                        if($data['episode_id'] == 0){
                            $VideoDetails .= ', All Episodes';
                        }else{
                            $episodeName = Yii::app()->common->getEpisodename($streams->id);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', '.$episodeName;
                        }
                    }
                }
                $data['amount'] = $trans_data['AMT'];
                $ppv_subscription_id = Yii::app()->billing->setPpvSubscription($plan,$data,$video_id,$start_date,$end_date,$data['couponCode'], $isadv, $gateway_code);
                $trans_data['transaction_status'] = $resArray['PAYMENTSTATUS'];
                $trans_data['invoice_id'] = $resArray['CORRELATIONID'];
                $trans_data['order_number'] = $resArray['TRANSACTIONID'];
                $trans_data['amount'] = $trans_data['AMT'];
                $trans_data['dollar_amount'] = $trans_data['AMT'];
                $trans_data['response_text'] = json_encode($trans_data);
                $transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$video_id,$trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                $trans_data['amount'] = $trans_data['AMT'];
                $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
                PciLog::model()->deleteByPk($log_data['id']);
                $res['code'] = 200;
                $res['status'] = "OK";
            }else{
                $res['code'] = 458;
                $res['status'] = "Error";
                $res['msg'] = "Transaction error!";
            }
        }else{
            $res['code'] = 400;
            $res['status'] = "Error";
            $res['msg'] = "Bad Request";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);exit;
    }
    /**
     * @method public PpvPayPalCancel() handles transaction cancled by user
     * @author SKM<sanjeev@muvi.com>
     * @return json Json data with parameters
    */
    public function actionPpvPayPalCancel() {
        $muvi_token = explode('-',$_REQUEST['muvi_token']);
        $unique_id = $muvi_token[0];
        $log_data = AppPayPalLog::model()->findByAttributes(array('unique_id'=>$unique_id));
        AppPayPalLog::model()->deleteByPk($log_data['id']);
        $data['code'] = 460;
        $data['status'] = "Error";
        $data['msg'] = "Transaction canceled!";
        $this->setHeader($data['code']);
        echo json_encode($data);exit;
    }
    /**@method  return scr part from thirdparty url
     * @author SKM<prakash@muvi.com>
     * @return string
     */
    public function getSrcFromThirdPartyUrl($thirdPartyUrl=''){
        if($thirdPartyUrl!=''){
            preg_match('/src="([^"]+)"/',$thirdPartyUrl, $match);
            if(!empty($match[1])){
                return $match[1];
            }else{
                preg_match("/src='([^']+)'/",$thirdPartyUrl, $match);
                if(!empty($match[1])){
                   return $match[1]; 
                }else{
                   return $thirdPartyUrl; 
                }
            }          
        }else{
            return $thirdPartyUrl;
        }
    }  
    
    /**@method Get Marlin BB Offline content
     * @author SKP<srutikant@muvi.com>
     * @return Json data with parameters
     */
    public function actiongetMarlinBBOffline(){
        $studio_id = $this->studio_id;
        $stream_unique_id = @$_REQUEST['stream_unique_id'];
        if($stream_unique_id != ''){
            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $stream_unique_id));
            if($streams){
                 if(@$streams->encryption_key != '' && @$streams->content_key != '' &&  @$streams->is_offline == 1){
                    $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id,'drm_cloudfront_url');
                    $fileName = substr($streams->full_movie, 0, -4).'.mlv';
                    if(@$getStudioConfig['config_value'] != ''){
                        $fullmovie_path = 'https://'.$getStudioConfig['config_value'].'/uploads/movie_stream/full_movie/'.$streams->id.'/'.$fileName;
                    } else{
                        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
                        $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
                        $signedBucketPath = $folderPath['signedFolderPath'];
                        $fullmovie_path = 'https://'.$bucketInfo['bucket_name'].'.'.$bucketInfo['s3url'].'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$streams->id.'/'.$fileName;
                    }
                    if($streams->is_multibitrate_offline == 1){ 
                        $multOfflineVal = $this->getvideoResolution($streams->video_resolution, $fullmovie_path);
                        foreach($multOfflineVal as $resKey => $videoPath){
                            $subArr['resolution'] = $resKey;
                            $subArr['url'] = $videoPath;
                            $token['multiple_resolution'][] = $subArr;
                        }
                    }
                    $token['file'] = $fullmovie_path;
                    $token['token'] = file_get_contents('https://bb-gen.test.expressplay.com/hms/bb/token?customerAuthenticator=200988,058524ac8dc0459cb9e4d497136563ba&actionTokenType=1&rightsType=BuyToOwn&outputControlOverride=urn:marlin:organization:intertrust:wudo,ImageConstraintLevel,0&cookie=MY_TEST01&contentId='.$streams->content_key.'&contentKey='.$streams->encryption_key);
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['data'] = $token;

                }else{
                    $data['code'] = 438;
                    $data['status'] = "Error";
                    $data['msg'] = "No such content found";

                }
            }else{
                $data['code'] = 438;
                $data['status'] = "Error";
                $data['msg'] = "No such content found";

            }
        }else{
            $data['code'] = 438;
            $data['status'] = "Error";
            $data['msg'] = "Stream unique id not found";

        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }  
    
   
    /*
     * 6204: Featured Content API
     * ajit@muvi.com
    */
    public function actionHomepage(){
        $data['BannerSectionList']=$this->actiongetBannerSectionList(0);
        $data['SectionName']=$this->actiongetSectionName(0);
        $this->setHeader($data['SectionName']['code']);
        echo json_encode($data);
        exit;
    }
    
    public function actiongetSectionName($nojson=1) {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $sections = FeaturedSection::model()->findAll('studio_id=:studio_id AND is_active=:is_active AND parent_id = 0 ORDER BY id_seq', array(':studio_id' => $studio_id, ':is_active' => 1));
        if ($language_id != 20) {
            $translated = CHtml::listData(FeaturedSection::model()->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select' => 'parent_id,title')), 'parent_id', 'title');
            if (!empty($sections)) {
                foreach ($sections as $section) {
                    if (array_key_exists($section->id, $translated)) {
                        $section->title = $translated[$section->id];
					}  
                    $sectionsdetail[] = $section;
                }
            }
        } else {
            $sectionsdetail = $sections;
        }
        if($sectionsdetail){
            foreach ($sectionsdetail as $key => $sec) {
                $fsections[$key]['studio_id'] = $sec->studio_id;
                $fsections[$key]['language_id'] = $sec->language_id;
                $fsections[$key]['title'] = $sec->title;
                $fsections[$key]['section_id'] = $sec->id;
                $total_content = FeaturedContent::model()->countByAttributes(array(
                    'section_id' => $sec->id,
                    'studio_id' => $studio_id
                ));
                $fsections[$key]['total'] =  $total_content;
            }
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['section'] = @$fsections;
        }else{
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "Section not found";            
        }
        
		if($nojson==1){
			$this->setHeader($data['code']);
            echo json_encode($data);
            exit;
		}else{
			return $data;
		}
    }

   public function actiongetFeaturedContent(){
        $studio_id = $this->studio_id;
        $section_id = $_REQUEST['section_id'];
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $controller = Yii::app()->controller;
        $sql = "SELECT * FROM homepage WHERE studio_id =" . $studio_id . " AND section_id=" . $section_id . " ORDER BY id_seq ASC";
        $con = Yii::app()->db;
        $movieids = '';
        $fecontents = $con->createCommand($sql)->queryAll();    
		$keycounter = 0;
        if ($fecontents) {
            $domainName= $this->getDomainName();
            $restriction = StudioContentRestriction::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
             if(isset($_REQUEST['country']) && $_REQUEST['country']){
				$country = $_REQUEST['country'];
			}else{
				$visitor_loc = Yii::app()->common->getVisitorLocation();
				$country = $visitor_loc['country'];
			}
			foreach ($fecontents AS $key => $val) {
                $langcontent = Yii::app()->custom->getTranslatedContent($val['movie_id'], $val['is_episode'], $language_id, $studio_id);
                $content[$keycon]['is_episode'] = $val['is_episode'];
                if($val['is_episode']==2){
                    $sql = "SELECT P.* FROM pg_product P WHERE P.id = '".$val['movie_id']."'";
                    $standaloneproduct = $con->createCommand($sql)->queryAll();
                    if ($standaloneproduct) {
                        foreach ($standaloneproduct AS $key => $val1) {
                            if (Yii::app()->common->isGeoBlockPGContent($val1['id'])) {
                                $cont_name = $val1['name'];
                                $story = $val1['description'];
                                $permalink = $val1['permalink'];
                                $short_story = substr(Yii::app()->common->htmlchars_encode_to_html($story), 0, 80);
                                $tempprice = Yii::app()->common->getPGPrices($val['id'], Yii::app()->controller->studio->default_currency_id);
                                if(!empty($tempprice)){
                                    $standaloneproduct[$key]['sale_price'] = $tempprice['price'];
                                    $standaloneproduct[$key]['currency_id'] = $tempprice['currency_id'];
                                }
                                $poster = PGProduct::getpgImage($val1['id'], 'standard');
                                $formatted_price = Yii::app()->common->formatPrice($standaloneproduct[$key]['sale_price'], $standaloneproduct[$key]['currency_id']);                            

                                $final_content[$key] = array(
                                    'movie_id' => $movie_id,
                                    'title' => $cont_name,
                                    'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                                    'poster' => $poster,
                                    'data_type' => 4,
                                    'uniq_id' => $movie_uniq_id,
                                    'short_story' => utf8_encode($short_story),
                                    'price' => $formatted_price,
                                    'status' => $val['status'],
                                    'is_episode' => 2
                                );                            
                            }                                               
                        }
                    }                    
                }else{
					/*if($geoData && in_array($val['movie_id'], $geoData)){
						continue;
					}*/
					if (Yii::app()->common->isGeoBlockContent(@$val['movie_id'], 0, $studio_id, $country)) {
						$arg['studio_id'] = $studio_id;
						$arg['movie_id'] = $val['movie_id'];
						$arg['season_id'] = 0;
						$arg['episode_id'] = 0;
						$arg['content_types_id'] = $content_types_id = $val['content_types_id'];
						$isFreeContent = Yii::app()->common->isFreeContent($arg);
						$embededurl = $domainName . '/embed/' . $val['embed_id'];
						if ($val['is_episode'] == 1) {
                            if (array_key_exists($val['movie_id'], $langcontent['episode'])) {
                                $val['episode_title'] = $langcontent['episode'][$val['movie_id']]->episode_title;
                                $val['episode_story'] = $langcontent['episode'][$val['movie_id']]->episode_story;
                            }                            
							$cont_name = ($val['episode_title'] != '') ? $val['episode_title'] : "SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
							$story = $val['episode_story'];
							$release = $val['episode_date'];
							$poster = $controller->getPoster($val['stream_id'], 'moviestream', 'episode', $this->studio_id);
						} else {
                            if (array_key_exists($val['movie_id'], $langcontent['film'])) {
                                $val['name'] = $langcontent['film'][$val['movie_id']]->name;
                                $val['story'] = $langcontent['film'][$val['movie_id']]->story;
                                $val['genre'] = $langcontent['film'][$val['movie_id']]->genre;
                                $val['censor_rating'] = $langcontent['film'][$val['movie_id']]->censor_rating;
                                $val['language'] = $langcontent['film'][$val['movie_id']]->language;
                            }                            
							$cont_name = $val['name'];
							$story = $val['story'];
							$release = $val['release_date'];
							$stream = movieStreams::model()->findByPk($val['stream_id']);
							if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {
								$poster = $controller->getPoster($val['movie_id'], 'films', 'episode', $this->studio_id);
							} else {
								$poster = $controller->getPoster($val['movie_id'], 'films', 'standard', $this->studio_id);
							}
						}
						$geo = GeoblockContent::model()->find('movie_id=:movie_id', array(':movie_id' => $val['movie_id']));

						//Get view count status
						$viewStatus = VideoLogs::model()->getViewStatus($val['movie_id'], $studio_id);
						$viewStatusArr = array();
						if (@$viewStatus) {
							foreach ($viewStatus AS $valarr) {
								$viewStatusArr['viewcount'] = $valarr['viewcount'];
								$viewStatusArr['uniq_view_count'] = $valarr['u_viewcount'];
							}
						}
						if (isset($stream['content_publish_date']) && @$stream['content_publish_date'] && (strtotime($stream['content_publish_date']) > time())) {
							
						} else {
							$final_content[$keycounter]['is_episode'] = $val['is_episode'];
							$final_content[$keycounter]['movie_stream_uniq_id'] = $val['embed_id'];
							$final_content[$keycounter]['movie_id'] = $val['movie_id'];
							$final_content[$keycounter]['movie_stream_id'] = $val['stream_id'];
							$final_content[$keycounter]['muvi_uniq_id'] = $val['uniq_id'];
							$final_content[$keycounter]['content_type_id'] = $val['content_type_id'];
							$final_content[$keycounter]['ppv_plan_id'] = $val['ppv_plan_id'];
							$final_content[$keycounter]['permalink'] = $val['fplink'];
							$final_content[$keycounter]['name'] = $cont_name;
							$final_content[$keycounter]['full_movie'] = $stream->full_movie;
							$final_content[$keycounter]['story'] = $story;
							$final_content[$keycounter]['genre'] = json_decode($val['genre']);
							$final_content[$keycounter]['censor_rating'] = ($val['censor_rating'] != '') ? implode(',', json_decode($val['censor_rating'])) . '' : '';
							$final_content[$keycounter]['release_date'] = $release;
							$final_content[$keycounter]['content_types_id'] = $val['content_types_id'];
							$final_content[$keycounter]['is_converted'] = $val['is_converted'];
							$final_content[$keycounter]['last_updated_date'] = '';
							$final_content[$keycounter]['movieid'] = $geo->movie_id;
							$final_content[$keycounter]['geocategory_id'] = $geo->geocategory_id;
							$final_content[$keycounter]['category_id'] = $restriction->category_id;
							$final_content[$keycounter]['studio_id'] = $studio_id;
							$final_content[$keycounter]['country_code'] = $restriction->country_code;
							$final_content[$keycounter]['ip'] = $restriction->ip;
							$final_content[$keycounter]['poster_url'] = $poster;
							$final_content[$keycounter]['isFreeContent'] = $isFreeContent;
							$final_content[$keycounter]['embeddedUrl'] = $embededurl;
							$final_content[$keycounter]['viewStatus'] = $viewStatusArr;
							$keycounter++;
						}
					}
                }
            }
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['section'] = (array)$final_content;
        }else{
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "Featured content not found";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);exit;
    }
    
    
    /*
     * END 6204
    */   
    
    /* API to initilise the sdk 
     * By <suraja@muvi.com>,ticket #6247
     */
     public function actioninitialiseSdk(){
        $studio_id = $this->studio_id;
        //check whether the hask key exists for the studio or not ?
       $apiKeyData = OuathRegistration::model()->find('studio_id=:studio_id AND status=:status',array(':studio_id'=>$studio_id,':status'=>'1'));
       if(@$apiKeyData){
       $hashkey=$apiKeyData->hask_key;
       $packg_name=$apiKeyData->package_name;
       if(empty(trim($hashkey))){           
           //generate the new hask key and send it.
           $key = md5(time());
           $hashkey=str_replace(" ","",$packg_name);
           //$hask_key=$hashkey.$key;          
           $apiKeyData->hask_key=$key;
           $apiKeyData->save();
           $data['code'] = 200;
           $data['status'] = "OK";
           $data['hashkey'] = $key;
           $data['pkgname'] = $packg_name;
           $data['msg'] = "Hashkey created successfully";     
        }else{
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['hashkey'] = $hashkey;
            $data['pkgname'] = $packg_name;
            $data['section'] = "Hashkey already exists";           
        }
       }
       
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    public function actionTextTranslation(){
        $studio_id = $this->studio_id;
        if(isset($_REQUEST['lang_code']) && $_REQUEST['lang_code']!=""){
            $lang_code = $_REQUEST['lang_code'];
            $studio = Studio::model()->findByPk($studio_id,array('select'=>'theme'));
            $theme = $studio->theme;
            if(file_exists(ROOT_DIR."languages/".$theme."/".trim($lang_code).".php")){
               $lang = include( ROOT_DIR."languages/".$theme."/".trim($lang_code).".php");
            }elseif(file_exists(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php')){
                $lang = include(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php');
            }else{
                $lang = include(ROOT_DIR . 'languages/studio/en.php');
            }
            $language = Yii::app()->controller->getAllTranslation($lang,$studio_id);
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['translation'] = $language;
        }else{
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "Language code not found.";       
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    public function actionGetLanguageList(){
        $studio_id= $this->studio_id;
        $data['code'] = 200;
        $data['status'] = "OK";
        $con = Yii::app()->db;
        $sql = "SELECT a.*, s.id AS studio_id, s.default_language FROM 
        (SELECT l.id AS languageid, l.name, l.code, sl.translated_name,sl.status,sl.frontend_show_status FROM languages l LEFT JOIN studio_languages sl 
        ON (l.id = sl.language_id AND sl.studio_id={$studio_id}) WHERE  l.code='en' OR (sl.studio_id={$studio_id})) AS a, studios s WHERE s.id={$studio_id} 
        ORDER BY FIND_IN_SET(a.code,s.default_language) DESC, FIND_IN_SET(a.code,'en') DESC, a.status DESC, a.name ASC";
        
        $studio_languages = $con->createCommand($sql)->queryAll();
        $lang_list= array();
        $i=0;
        foreach($studio_languages as $lang){
            if(($lang['frontend_show_status'] != "0") && ($lang['status'] == 1 || $lang['code'] == 'en')){
                if(trim($lang['translated_name']) !=""){
                    $lang['name'] = $lang['translated_name'];
                }
                $lang_list[$i]['code'] = $lang['code'];
                $lang_list[$i]['language'] = $lang['name'];
                $i++;
            }
        }
        $data['lang_list'] = $lang_list;
        $data['default_lang'] = $studio_languages[0]['default_language'];
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    //API for purchase history Listing by suniln
      public function actionPurchaseHistory() {
          $user_id = @$_REQUEST['user_id'];
        $studio_id = $this->studio_id;
        if ($user_id > 0) {
                $subscribed_user_data = Yii::app()->common->isSubscribed($user_id, $studio_id, 1);
                $userdate['expire_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date . '-1 Days'));
                $userdate['nextbilling_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date));
                //Pagination codes
            $limit = (isset($_REQUEST['limit']) && intval($_REQUEST['limit'])) ? $_REQUEST['limit'] : 10;
			$offset = (isset($_REQUEST['offset']) && $_REQUEST['offset']) ? $_REQUEST['offset'] : 0;
			$page = $offset < 1 ? 1 : $offset;
			$offset = ($page - 1) * ($limit);
            
            $sql = "select SQL_CALC_FOUND_ROWS id,transaction_date,currency_id,amount,user_id,studio_id,plan_id,currency_id,transaction_date,payer_id,invoice_id,subscription_id,ppv_subscription_id,movie_id,order_number,transaction_type,transaction_status from transactions WHERE studio_id=" . $studio_id . " AND user_id=" . $user_id . "  ORDER BY id DESC LIMIT " . $limit . " OFFSET " . $offset;
           
                $transaction = Yii::app()->db->createCommand($sql)->queryAll();
                $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS() cnt')->queryScalar();
                $planModel = new PpvPlans();
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $studio_id);
                foreach ($transaction as $key => $details) {
                    $transactions[$key]['invoice_id']=$details['invoice_id'];
                    $transactions[$key]['transaction_date']=date('F d, Y', strtotime($details['transaction_date']));
                    $transactions[$key]['amount']=$details['amount'];
                    $transactions[$key]['transaction_status']=$translate['success'];
                    $currency=Currency::model()->findByPk($details['currency_id']);
                    $transactions[$key]['currency_symbol'] =$currency->symbol ;
                    $transactions[$key]['currency_code'] =$currency->code;
                    $transactions[$key]['id']=$this->getUniqueIdEncode($details['id']);
                if($details['transaction_type']==1){
                    $trans_type = 'Monthly subscription';
                    $subscription_id=$details['subscription_id'];
                    $startdate = UserSubscription::model()->findByAttributes(array('id'=>$subscription_id))->start_date;
                    $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($startdate);
                    if($currdatetime<=$expirytime && strtotime($startdate)!=''){
                       $statusppv=$translate['active'];  
                    }
                    else{
                       $statusppv='N/A';  
                    }
                    $transactions[$key]['statusppv']=$statusppv;
                    $transactions[$key]['Content_type'] = 'digital';
                }
                elseif($details['transaction_type']==2){
                    
                    $ppv_subscription_id = $details['ppv_subscription_id'];
                    $end_date = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->end_date;
                    $season_id = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->season_id;
                    $episode_id = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->episode_id;
                    $movie_id = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->movie_id;
                    $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                    $content_types_id = Film::model()->findByAttributes(array('id' => $movie_id))->content_types_id;
                    $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                    $movie_names = $movie_name;
                    if ($season_id != 0 && $episode_id != 0) {
                        $movie_names = $movie_name . " ->".$translate['season']." #" . $season_id . " ->" . $episode_name;
                    }
                    if ($season_id == 0 && $episode_id != 0) {
                        $movie_names = $movie_name . " -> " . $episode_name;
                    }
                    if ($season_id != 0 && $episode_id == 0) {
                        $movie_names = $movie_name . " ->".$translate['season']." #" . $season_id;
                    }
                    $transactions[$key]['movie_name'] = $movie_names;
                     $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($end_date);
                   $statusppv='N/A';
                      if($content_types_id==1){
                    if($currdatetime<=$expirytime && (strtotime($end_date)!='' || $end_date!='0000-00-00 00:00:00')){
                       $statusppv=$translate['active'];
                    }
                    else{
                       $statusppv='N/A';  
                    }
                     }
                     
                    if ($content_types_id == 3) {
                        if ($currdatetime <= $expirytime && strtotime($end_date) != '') {
                            $statusppv = $translate['active'];
                    }
                        if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
                            $statusppv = $translate['active'];
                    }
                        if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
                            $statusppv = $translate['active'];
                    }
                    }
                   $transactions[$key]['statusppv']=$statusppv;
                    if(strtotime($end_date)!='' && $end_date!='0000-00-00 00:00:00'){
                    $expirydateppv= date('F d, Y', strtotime($end_date));
                    $transactions[$key]['expiry_dateppv'] = $expirydateppv;
                    }else{
                    $expirydateppv='N/A'; 
                    $transactions[$key]['expiry_dateppv'] = $expirydateppv;
                    }
                    $trans_type = $translate['Pay_Per_View'] . "-";
                    $trans_type .= $movie_names;
                    $transactions[$key]['Content_type'] = 'digital';
                   
                    
                }
                elseif($details['transaction_type']==3){
                    $title = $planModel->findByPk($details['plan_id'])->title;
                    $transactions[$key]['movie_name'] = $title;
                    $trans_type = $translate['pre_order'] . "<br>";
                    $trans_type .= $title;
                    $transactions[$key]['Content_type'] = 'digital';
                }
               elseif($details['transaction_type'] == 4){
                    $trans_type='physical';
                    $orderquery = "SELECT d.id,d.order_id,d.name,d.price,d.quantity,d.product_id,d.item_status,d.cds_tracking,d.tracking_url,o.order_number,o.cds_order_status FROM pg_order_details d, pg_order o WHERE d.order_id=o.id AND o.transactions_id=".$details['id']." ORDER BY id DESC";
                    $order = Yii::app()->db->createCommand($orderquery)->queryAll();
                    $transactions[$key]['details'] = $order;
                    $transactions[$key]['order_item_id'] = $order[0]['id'];
                    $transactions[$key]['order_id'] = $order[0]['order_id'];
                    $transactions[$key]['order_number'] = $order[0]['order_number'];                    
                    $transactions[$key]['cds_order_status'] = $order[0]['cds_order_status'];
                    $transactions[$key]['cds_tracking'] = $order[0]['cds_tracking'];
                    $transactions[$key]['tracking_url'] = $order[0]['tracking_url'];
                    $transactions[$key]['Content_type'] = 'physical';
                }
                elseif($details['transaction_type']==5){
                    $title = $planModel->findByPk($details['plan_id'])->title;
                    $ppv_subscription_id=$details['ppv_subscription_id'];
                    $ppvplanid = PpvSubscription::model()->findByAttributes(array('id'=>$ppv_subscription_id))->timeframe_id;
                    $ppvtimeframes = Ppvtimeframes::model()->findByAttributes(array('id'=>$ppvplanid))->validity_days ;
                    $transaction_dates=$details['transaction_date'];
                    $expirydate= date('F d, Y', strtotime($transaction_dates. ' + '.$ppvtimeframes.' days'));
                    $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($transaction_dates. ' + '.$ppvtimeframes.' days');
                    if($currdatetime<=$expirytime){
                       $statusbundles=$translate['active'];
                    }
                    else{
                       $statusbundles='N/A';  
                    }
                    if($expirydate == ""){
                        $expirydate = "N/A";
                    }
                    $transactions[$key]['expiry_date'] =$expirydate;
                    $transactions[$key]['movie_name'] = $title;
                    $transactions[$key]['Content_type'] = 'digital';
                    $trans_type = 'Pay-per-view Bundle'."-";
                    $transactions[$key]['statusppv']=$statusbundles;
                    $trans_type.= $title;
                }else{
                    $trans_type = '';
                }                    

                $transactions[$key]['transaction_for'] = $trans_type;
            }
            if($transactions==""){
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['section'] = array();    
            }else{
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['section'] = $transactions;    
            }
        } else {
            $data['code'] = 449;
            $data['status'] = "Error";
            $data['msg'] = "User Authentication Error.";
        }
         
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
            
    }
    //get movie name by suniln
     public function getFilmName($movie_id){
        $movie_name = Yii::app()->db->createCommand("SELECT name FROM films WHERE id=".$movie_id)->queryROW();
        return $movie_name['name'];
    }
    //get episode name by suniln
     public function getEpisodeName($episode_id){
        $movie_name = Yii::app()->db->createCommand("SELECT episode_title FROM movie_streams WHERE id=".$episode_id)->queryROW();
        return $movie_name['episode_title'];
    }
    function getUniqueIdEncode($str){
       for($i=0; $i<5;$i++){
            //apply base64 first and then reverse the string
            $str=strrev(base64_encode($str));
        }
            return $str;
    }
    function getUniqueIdDecode($str){
         for($i=0; $i<5;$i++){
            //apply reverse the string first and then base64
            $str=base64_decode(strrev($str));
        }
            return $str;
    }
    //API for Order Details/transactions Details Page By suniln 
       public function actionTransaction() {
        $user_id = @$_REQUEST['user_id'];
        $studio_id= $this->studio_id; 
        if ($user_id > 0 && $studio_id > 0) {
             $id = $this->getUniqueIdDecode(@$_REQUEST['id']);
             $transaction = Yii::app()->general->getTransactionDetails($id,$user_id,1);
             $transaction_details['payment_method'] = $transaction['payment_method'];
             $transaction_details['transaction_status'] = $transaction['transaction_status'];
             $transaction_details['transaction_date'] = $transaction['transaction_date'];
             $transaction_details['amount'] = $transaction['amount'];
             $transaction_details['currency_symbol'] = $transaction['currency_symbol'];
             $transaction_details['currency_code'] = $transaction['currency_code'];
             $transaction_details['invoice_id'] = $transaction['invoice_id'];
             $transaction_details['order_number'] = $transaction['order_number'];
             $transaction_details['plan_name'] = $transaction['plan_name'];
             $transaction_details['plan_recurrence'] = $transaction['plan_recurrence'];
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['section'] = $transaction_details; 
        } else {
           $data['code'] = 450;
           $data['status'] = "Error";
           $data['msg'] = "User Authentication Error.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    //api for invoice details
    function actionGetInvoicePDF(){
        $user_id = @$_REQUEST['user_id'];
        $device_type=@$_REQUEST['device_type'];
       $transaction_id=$this->getUniqueIdDecode(@$_REQUEST['id']);
		if($user_id){
			if(isset($transaction_id) && $transaction_id){
				$studio_id= $this->studio_id; 
                                $name = Yii::app()->pdf->downloadUserinvoice($studio_id,$transaction_id,$user_id,'','',$device_type);
                                $data['code'] = 200;
                                $data['status'] = "OK";
                                $data['section'] = $name; 
                                
			}else{
				$data['code'] = 451;
                                $data['status'] = "Error";
                                $data['msg'] = "Transaction ID error"; 
}
		}else{
			 $data['code'] = 452;
                         $data['status'] = "Error";
                         $data['msg'] = "User Authentication Error.";
		}
                echo json_encode($data);exit;
        }
    function actionDeleteInvoicePath(){
        $file=@$_REQUEST['filepath'];
        if(unlink(ROOT_DIR . 'docs/' .$file)){
           $data['code'] = 200;
           $data['status'] = "OK";
           $data['msg'] = "Successfully Delete";
        }
        else{
                         $data['code'] = 453;
                         $data['status'] = "Error";
                         $data['msg'] = "Successfully not Delete";
        }
        echo json_encode($data);exit;
    }
/**
 * @purpose get the cast and crew
 * @param movie_id Mandatory
 * @Note Applicable only for single part, multipart content. Not for live streams and episodes
 * @Author <ajit@muvi.com>
 */
    function actiongetCelibrity(){
		$lang_code = isset($_REQUEST['lang_code']) ? $_REQUEST['lang_code'] : 'en';
		$studio_id   = $this->studio_id;
		$language_id = Yii::app()->custom->getLanguage_id($lang_code);
		$translate = $this->getTransData($lang_code, $studio_id);
		$movie_uniqueid = @$_REQUEST['movie_id'];
		if ($movie_uniqueid) {
			$Films = Film::model()->find(array('select' => 'id', 'condition' => 'studio_id=:studio_id and uniq_id=:uniq_id', 'params' => array(':studio_id' => $studio_id, ':uniq_id' => $movie_uniqueid)));
			$movie_id = $Films->id;
			if ($movie_id) {
				/* $sql = "SELECT C.id,C.name,C.birthdate,C.birthplace,C.summary,C.twitterid,C.permalink,C.alias_name,C.meta_title,C.meta_description,C.meta_keywords FROM celebrities C WHERE C.id IN(SELECT celebrity_id FROM movie_casts MC WHERE MC.movie_id = " . $movie_id . ") AND C.studio_id = " . $studio_id;
				  $command = Yii::app()->db->createCommand($sql);
				  $list = $command->queryAll(); */

				$movie_casts = Yii::app()->custom->getCastCrew($movie_id, $studio_id, $language_id);
				if ($movie_casts) {
					$celebrity = array();
					foreach ($movie_casts as $key => $val) {
						//get celebrity type
                        //$cast_name = MovieCast::model()->find(array('select' => 'cast_type', 'condition' => 'celebrity_id=:celebrity_id', 'params' => array(':celebrity_id' => $val['id'])));
                        //$movie_casts_roles = json_decode($cast_name->cast_type,true);
                        $movie_casts_roles = json_decode($val['cast_type'],true);
                        $castRoles = array();
                        foreach($movie_casts_roles as $movie_casts_role){
                            $castRole = $movie_casts_role;
                            if($language_id != 20){
                                if(strtolower($movie_casts_role)=='actor' || strtolower($movie_casts_role)=='actors'){
                                    $castRole = $translate['actors']==''?$castRole:$translate['actors'];
                                } if(strtolower($movie_casts_role)=='director' || strtolower($movie_casts_role)=='directors'){
                                    $castRole = $translate['directors']==''?$castRole:$translate['directors'];
								}
                            }
                            $castRoles[] = $castRole;
                        }
                        $getCastsRole = '["'.implode('","',$castRoles).'"]';
						$celebrity[$key]['name'] = $val['name'];
						$celebrity[$key]['permalink'] = $val['permalink'];
						$celebrity[$key]['summary'] = $val['summary'];
                        $celebrity[$key]['cast_type'] = $getCastsRole;
						$img = $this->getPoster($val['celebrity_id'], 'celebrity', 'thumb', $studio_id);
						if (getimagesize($img) == false) {
							$img = str_replace('/thumb/', '/medium/', $img);
						}
						$celebrity[$key]['celebrity_image'] = $img;
					}
					$data['code'] = 200;
					$data['status'] = "OK";
					$data['celibrity'] = $celebrity;
				} else {
					$data['code'] = 448;
					$data['status'] = "Failure";
					$data['msg'] = $translate['no_celebrity_found'];
				}
			} else {
				$data['code'] = 448;
				$data['status'] = "Failure";
				$data['msg'] = $translate['no_record_found'];
			}
		} else {
			$data['code'] = 448;
			$data['status'] = "Failure";
			$data['msg'] = $translate['required_data_not_found'];
		}
		$this->setHeader($data['code']);
		echo json_encode($data);
		exit;
	}

	function actiongetEmojis(){
        $emo = Emojis::model()->findAll();        
        $device_id = $_REQUEST['deviceId'];
            if(!empty($emo) && !empty($device_id)){
            foreach($emo as $key => $val){                
                $set[$key]['code'] = $val['code'];
                $set[$key]['name'] = $val['emoji_name'];
                $code = ltrim($val['code'], ':');
                $name = $code.'.png';
                $path = 'emoji-'.$device_id.'x/'.$name;
                $set[$key]['url'] = str_replace($name, $path,$val['emoji_url']) ;
            }
                $data['code'] = 200;
                $data['status'] = "OK";
            $data['emojis'] = $set;
            }else{
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Emoji not found.";            
            }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
/**
 * @method public assignBroadCastToContent() It will create content against broadcaster under a category. 
 * @author Gayadhar<support@muvi.com>
 * @param string $content_name Name of the BroadCast
 * @param string $category_id Id of the content category
 * @param int $user_id Logged in user id
 * @param text $description Description of the content
 * @return HTML 
 */	
	function actionAssignBroadCastToContent(){
		$required_params = array('category_id'=>1,'content_name'=>1,'user_id'=>1);
		if(@$_REQUEST['category_id']){
			unset($required_params['category_id']);
		}
		if(@$_REQUEST['content_name']){
			unset($required_params['content_name']);
		}
		if(@$_REQUEST['user_id']){
			unset($required_params['user_id']);
		}
		if(empty($required_params)){
			$studio_id = $this->studio_id;
			$arr['succ']= 0;
			$Films = new Film();
			$data['name'] = $_REQUEST['content_name'];
			$data['story'] = @$_REQUEST['description'];
			$catData = Yii::app()->db->CreateCommand("SELECT binary_value,category_name,permalink FROM content_category WHERE id={$_REQUEST['category_id']} AND studio_id={$this->studio_id}")->queryRow();
			$data['content_category_value'] = array($catData['binary_value']);
			$data['parent_content_type'] = 2;
			$data['content_types_id'] = 4;
			$movie = $Films->addContent($data,$this->studio_id);
			$movie_id = $movie['id'];
			$arr['uniq_id'] = $movie['uniq_id'];
			
            //Live stream set up
            $streamUrl = $movie['permalink'].'-'.strtotime(date("Y-m-d H:i:s"));
            $length = 8;
            $userName = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
            $password = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
            $enc = new bCrypt();
            $passwordEncrypt = $enc->hash($password);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://'.nginxserverip.'/auth.php');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=save&is_active=1&stream_name=".$streamUrl."&user_name=".$userName."&password=".$passwordEncrypt."&serverUrl=".Yii::app()->getBaseUrl(TRUE)."/conversion/stopLiveStreaming?movie_id".$movie_id);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = trim(curl_exec($ch));
            curl_close($ch);
            if($server_output != 'success'){
                Film::model()->deleteByPk($movie_id);
                $data['code'] = 458;
                $data['status'] = "Failure";
                $data['msg'] = "Due to some reason live stream content was unable to create. Please try again"; 
                $this->setHeader($data['code']);
                echo json_encode($data);exit;
            }

			//Adding permalink to url routing 
			$urlRouts['permalink'] = $movie['permalink'];
			$urlRouts['mapped_url'] = '/movie/show/content_id/'.$movie_id;
			$urlRoutObj = new UrlRouting();
			$urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,$studio_id);
			
			//Insert Into Movie streams table
			$MovieStreams = new movieStreams();
			$MovieStreams->studio_id = $studio_id;
			$MovieStreams->movie_id = $movie_id;
			$MovieStreams->is_episode = 0;
			$MovieStreams->created_date = gmdate('Y-m-d H:i:s');
			$MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
			$embed_uniqid = Yii::app()->common->generateUniqNumber();
			$MovieStreams->embed_id = $embed_uniqid; 
			$publish_date = NULL;
			$MovieStreams->content_publish_date = $publish_date;
			$MovieStreams->save();

			//Upload Poster
			//$this->processContentImage(4,$movie_id,$MovieStreams->id);
			
			//Adding the feed infos into live Stream table
            $feedData['feed_url'] = 'rtmp://'.nginxserverip.'/live/'.$streamUrl;
            $feedData['stream_url'] = 'rtmp://'.nginxserverip.'/live';
            $feedData['stream_key'] = $streamUrl."?user=".$userName."&pass=".$password;
			$feedData['feed_type'] = 2;
            $feedData['is_recording'] = 0;
			$feedData['method_type'] = 'push';
            $feedData['start_streaming'] = 1;
			
			$livestream = new Livestream();
			$livestream->saveFeeds($feedData,$studio_id,$movie_id,$_REQUEST['user_id']);
			if(HOST_IP !='127.0.0.1'){
				$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
				$solrArr['content_id'] = $movie_id;
				$solrArr['stream_id'] = $MovieStreams->id;
				$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
				$solrArr['is_episode'] = 0;
				$solrArr['name'] = $movie['name'];
				$solrArr['permalink'] = $movie['permalink'];
				$solrArr['studio_id'] =  $studio_id;
				$solrArr['display_name'] = 'livestream';
				$solrArr['content_permalink'] =  $movie['permalink'];
				$solrObj = new SolrFunctions();
				$ret = $solrObj->addSolrData($solrArr);
			}  
			$data = array();
			$data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = 'Broadcast Content created successfully.';
            $data['feed_url'] = $feedData['stream_url']."/".$feedData['stream_key'];
            $data['stream_url'] = $feedData['stream_url'];
            $data['stream_key'] = $feedData['stream_key'];
            $data['movie_id'] = $movie_id;
			$data['uniq_id'] = $movie['uniq_id'];
			$data['movie_stream_uniq_id'] = $embed_uniqid;
		}else{
			$data['code'] = 458;
            $data['status'] = "Failure";
            $data['msg'] = implode(',',$required_params).": required param(s) missing";    
		}
		$this->setHeader($data['code']);
        echo json_encode($data);exit;
	}
        
    /**
     * @purpose get the total no of videos and total duration of videos
     * @param authToken Mandatory
     * @Author Biswajit Parida<biswajit@muvi.com>
     */
    public function actionGetTotalNoOfVideo(){
        $mapped_videos = 0;
        $mapped_duration = $total_duration =  0;
        $studio_id = $this->studio_id;
        $sql = 'SELECT SQL_CALC_FOUND_ROWS (0), vm.*,ms.id as movie_stream_id,ms.is_converted as movieConverted,mt.is_converted as trailerConverted FROM video_management vm LEFT JOIN movie_streams ms ON (vm.id=ms.video_management_id) LEFT JOIN movie_trailer mt ON (vm.id=mt.video_management_id) WHERE  vm.studio_id='.$studio_id.' GROUP BY vm.id  order by vm.video_name ASC';
        $allvideo = Yii::app()->db->createCommand($sql)->queryAll();
        $total_videos = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        if(!empty($allvideo)){
            foreach($allvideo as $videos){
                $duration = Yii::app()->custom->convertTimeToMiliseconds($videos['duration']);
                if(trim($duration) == ""){
                    $duration = 0;
                }
                $total_duration += $duration;
                if(($videos['movieConverted'] == 1)){
                    $mapped_videos++;
                    $mapped_duration += $duration;
                }elseif(($videos['trailerConverted'] == 1)){
                    $mapped_videos++;
                    $mapped_duration += $duration;
                }
            }
        }
        //$total_duration  = Yii::app()->custom->formatMilliseconds($total_duration);
        //$mapped_duration = Yii::app()->custom->formatMilliseconds($mapped_duration);
        $data = array(
            'total_videos'    => $total_videos,
            'total_duration'  => $total_duration,
            'mapped_videos'   => $mapped_videos,
            'mapped_duration' => $mapped_duration
        );
       echo json_encode($data);exit;
    }
    /**
     * @purpose get the categories of a content
     * @param authToken,content_category_value Mandatory, lang_code Optional
     * @Author Biswajit Parida<biswajit@muvi.com>
     */
    public function actionGetCategoriesOfContent(){
        $lang_code   = isset($_REQUEST['lang_code']) ? $_REQUEST['lang_code'] : 'en';
        $language_id = Yii::app()->custom->getLanguage_id($lang_code); 
        $studio_id   = $this->studio_id;
        $sql = "SELECT id,binary_value,category_name,parent_id,language_id FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
        $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
        $contentCategories = CHtml::listData($contentCategory, 'binary_value', 'category_name');
        $content_category_value = $_REQUEST['content_category_value']; 
        $categories = Yii::app()->Helper->categoryNameFromCommavalues($content_category_value, $contentCategories);
        echo $categories;
        exit;
    }

/**
 * @method public GetStudioAuthKey() It will validate the login credential & return the authToken for that studio. 
 * @author Gayadhar<support@muvi.com>
 * @param string $email Loggin email address
 * @param string $password studio login password
 * @return JSON will return the authToken for the respective studio
 */	
	function actionGetStudioAuthkey(){
		if($_REQUEST['email'] && $_REQUEST['password']){
			$userData = User::model()->find('email=:email AND role_id =:role_id',array(':email'=>$_REQUEST['email'],'role_id'=>1));
			if($userData){
				$enc = new bCrypt();
				if($enc->verify($_REQUEST['password'], $userData->encrypted_password)){
					$sql = "SELECT * FROM oauth_registration WHERE studio_id={$userData->studio_id} and status=1";
					$oauthData = Yii::app()->db->createCommand($sql)->queryRow();
					if ($oauthData){
						$referer = '';
						if (isset($_SERVER['HTTP_REFERER'])) {
							$referer = parse_url($_SERVER['HTTP_REFERER']);
							$referer = $referer['host'];
						}
						$oauthData = (object) $oauthData;
						if ($oauthData->request_domain && ($oauthData->request_domain != $referer)) {
							$data['code'] = 409;
							$data['status'] = "failure";
							$data['msg'] = "Domain not registered with Muvi for API calls!";
							echo json_encode($data);exit;
						}
						if ($data->expiry_date && (strtotime($data->expiry_date) < strtotime(date('Y-m-d')))) {
							$data['code'] = 410;
							$data['status'] = "failure";
							$data['msg'] = "Oauth Token expired!";
							echo json_encode($data);exit;
						}
						$data['code'] = 200;
						$data['authToken'] = $oauthData->oauth_token;
						$data['status'] = "OK";
						$data['msg'] = "Success";
					}else{
						$data['code'] = 462;
						$data['status'] = "Failure";
						$data['msg'] = "AuthToken not available, Contact admin to generate the token";   
					}
				}else{
					$data['code'] = 461;
					$data['status'] = "Failure";
					$data['msg'] = "Invalid login credential";   
				}
			}else{
				$data['code'] = 460;
				$data['status'] = "Failure";
				$data['msg'] = "No studio available with the given credential!"; 
			}
		}else{
			$data['code'] = 459;
            $data['status'] = "Failure";
            $data['msg'] = "Invalid Login credential";    
		}
		$this->setHeader($data['code']);
        echo json_encode($data);exit;
	}
/**
 * @purpose get the categories of a content
 * @param authToken,user_id Mandatory
 * @<ajit@muvi.com>
 */
	public function actionMyLibrary(){
        $user_id   = $_REQUEST['user_id'];
        $studio_id   = $this->studio_id;
		$lang_code = (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") ? $_REQUEST['lang_code'] : 'en';
		$translate = $this->getTransData($lang_code, $studio_id);
        if(isset($user_id) && intval($user_id) && $user_id !=0 ){            
            $transactions = Yii::app()->db->createCommand()
                ->select('plan_id,subscription_id,ppv_subscription_id,transaction_type')
                ->from('transactions')
                ->where('studio_id = '.$studio_id.' AND user_id='.$user_id.' AND transaction_type IN (2,5,6)')
                ->order('id DESC')    
                ->queryAll();
            
            $planModel = new PpvPlans();
			$library = array();$newarr= array();$free=array();
            foreach ($transactions as $key => $details) {
                $ppv_subscription_id=$details['ppv_subscription_id'];
                $ppv_subscription = PpvSubscription::model()->findByAttributes(array('id'=>$ppv_subscription_id));
                if(Film::model()->exists('id=:id',array(':id' => $ppv_subscription->movie_id))){
                if($details['transaction_type']==2){                    
                    $film = Film::model()->findByAttributes(array('id'=>$ppv_subscription->movie_id));
                    $movie_id = $ppv_subscription->movie_id;
                    $end_date = $ppv_subscription->end_date ? $ppv_subscription->end_date=='1970-01-01 00:00:00'?'0000-00-00 00:00:00':$ppv_subscription->end_date:'0000-00-00 00:00:00';                                       
                    $season_id = $ppv_subscription->season_id ;
                    $episode_id = $ppv_subscription->episode_id ; 
                    $movie_name = isset($movie_id)?$this->getFilmName($movie_id):"";                    
                    $content_types_id = $film->content_types_id;
                    $episode_name = isset($episode_id)?$this->getEpisodeName($episode_id):"";
                    $movie_names=$movie_name;
                    if($season_id!=0 && $episode_id!=0){
                      $movie_names=$movie_name." -> ".$translate['season']." ".$season_id." ->".$episode_name; 
                    }
                    if($season_id==0 && $episode_id!=0){
                     $movie_names=$movie_name." -> ".$episode_name;  
                    }
                    if($season_id!=0 && $episode_id==0){
                      $movie_names=$movie_name." -> ".$translate['season']." ".$season_id;  
                    }                    
                    $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($end_date);
                    $statusppv='N/A';
                    if($content_types_id==1){
                        if($currdatetime<=$expirytime && (strtotime($end_date)!='' || $end_date!='0000-00-00 00:00:00')){
                           $statusppv='Active';  
                        }
                        $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                        if($end_date=='0000-00-00 00:00:00'){
                          $statusppv='Active';  
                    }
                    }
                    if($content_types_id==4){
                        if($currdatetime<=$expirytime && (strtotime($end_date)!='' || $end_date!='0000-00-00 00:00:00')){
                            $statusppv='Active'; 
                        }
                        $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                        if($end_date=='0000-00-00 00:00:00'){
                          $statusppv='Active';  
                    } 
                     }
                    if($content_types_id==3){
                        $datamovie_stream = Yii::app()->db->createCommand()
                            ->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
                            ->from('movie_streams ms ')
                            ->where('ms.id='.$episode_id)
                            ->queryRow();  
                        $embed_id=$datamovie_stream['embed_id'];
                        if($currdatetime<=$expirytime && strtotime($end_date)!='0000-00-00 00:00:00'){
                           $statusppv='Active';  
                        }
                    if($season_id!=0 && $episode_id==0){
                        $statusppv='Active'; 
                    } 
                    if($end_date=='0000-00-00 00:00:00'){
                      $statusppv='Active';  
                    }
                        if($movie_id!=0 && $season_id==0 && $episode_id==0 ){
                         $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                        }
                        if($movie_id!=0 && $season_id!=0 && $episode_id==0){
                          $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink.'?season='.$season_id;
                        }
                        if($movie_id!=0 && $season_id!=0 && $episode_id!=0){
                          $baseurl=Yii::app()->getBaseUrl(true).'/player/'.$film->permalink.'/stream/'.$embed_id;
                        }
                    } 
                    if($statusppv=='Active'){
                        if($content_types_id == 2){
                           $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                        }else{
                           $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                    }
                       if($content_types_id==3){
							if($episode_id!=0){
						 $stm = movieStreams::model()->findByAttributes(array('id'=>$episode_id));
							}else if($season_id!=0 && $episode_id==0){
						 $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id,'is_episode'=>0));
							}else{
						 $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
							}
                        }else{
                            $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
                        }
                        $library[$key]['name'] = $movie_names;
                        $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                        $library[$key]['movie_id'] = $movie_id;
                        $library[$key]['movie_stream_id'] = $stm->id;
                        $library[$key]['is_episode'] = ($episode_id!=0)?'1':isset($stm->is_episode)?$stm->is_episode:0;
                        $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                        $library[$key]['content_types_id'] = $film->content_types_id;
                        $library[$key]['content_type_id'] = $film->content_type_id;
                        $library[$key]['baseurl'] = $baseurl;
                        $library[$key]['poster_url'] = $poster;
                        $library[$key]['video_duration'] = $stm->video_duration;   
                        $library[$key]['genres'] = json_decode($film->genre);
                        $library[$key]['permalink'] = $film->permalink;
                        $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                        $library[$key]['full_movie'] = $stm->full_movie;
                        $library[$key]['story'] = $film->story;;
                        $library[$key]['release_date'] = $film->release_date;
                        $library[$key]['is_converted'] = $stm->is_converted;
                        $library[$key]['isFreeContent'] = 0;
                        $library[$key]['season_id'] = $season_id;
                    }
                }elseif($details['transaction_type']==5){                    
                    $ppvplanid = PpvSubscription::model()->find(array('select'=>'timeframe_id','condition' => 'id=:id','params' => array(':id' =>$ppv_subscription_id)))->timeframe_id;                    
                    $ppvtimeframes = Ppvtimeframes::model()->find(array('select'=>'validity_days','condition' => 'id=:id','params' => array(':id' =>$ppvplanid)))->validity_days;
                    $ppvbundlesmovieid = Yii::app()->db->createCommand()
                        ->SELECT('content_id')
                        ->from('ppv_advance_content')
                        ->where('ppv_plan_id='.$details['plan_id'])
                        ->queryAll();
                    $expirydate= date('M d, Y', strtotime($transaction_dates. ' + '.$ppvtimeframes.' days'));
                    $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($transaction_dates. ' + '.$ppvtimeframes.' days');
                    if($currdatetime<=$expirytime){
                        foreach($ppvbundlesmovieid as $keyb=>$contentid){
                            $movie_id=$contentid['content_id']; 
                            $film = Film::model()->find(array('condition' => 'id=:id','params' => array(':id' =>$movie_id)));
                            if($film->content_types_id == 2){
                               $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                            }else{
                               $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                        }                      
                        $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
                        
                        $library[$key]['name'] = $film->name;
                        $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                        $library[$key]['movie_id'] = $movie_id;
                        $library[$key]['movie_stream_id'] = $stm->id;
                        $library[$key]['is_episode'] = isset($stm->is_episode)?$stm->is_episode:0;
                        $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                        $library[$key]['content_types_id'] = $film->content_types_id;
                        $library[$key]['content_type_id'] = $film->content_type_id;
                        $library[$key]['baseurl'] = $baseurl;
                        $library[$key]['poster_url'] = $poster;
                        $library[$key]['video_duration'] = $stm->video_duration;   
                        $library[$key]['genres'] = json_decode($film->genre);
                        $library[$key]['permalink'] = $film->permalink;
                        $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                        $library[$key]['full_movie'] = $stm->full_movie;
                        $library[$key]['story'] = $film->story;;
                        $library[$key]['release_date'] = $film->release_date;
                        $library[$key]['is_converted'] = $stm->is_converted;
                        $library[$key]['isFreeContent'] = 0;
                        $library[$key]['season_id'] = $stm->series_number;
                    }                    
                    }                    
                }elseif($details['transaction_type']==6){ 

                    $ppv_subscription_id=$details['ppv_subscription_id'];
                    $PpvSubscription = Yii::app()->db->createCommand("select end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id from ppv_subscriptions WHERE id=".$ppv_subscription_id)->queryRow();
                    $PpvSubscription = Yii::app()->db->createCommand()
                        ->SELECT('end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id')
                        ->from('ppv_subscriptions')
                        ->where('id='.$ppv_subscription_id)
                        ->queryRow();
                    $end_date = $PpvSubscription['end_date']?$PpvSubscription['end_date']=='1970-01-01 00:00:00'?'0000-00-00 00:00:00':$PpvSubscription['end_date']:'0000-00-00 00:00:00' ;
                    $start_date = $PpvSubscription['start_date'] ;
                    $season_id = $PpvSubscription['season_id'] ;
                    $episode_id = $PpvSubscription['episode_id'] ;
                    $movie_id = $PpvSubscription['movie_id'] ;
                    $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($end_date);
                    $movie_name = isset($movie_id)?$this->getFilmName($movie_id):"";
                    $episode_name = isset($episode_id)?$this->getEpisodeName($episode_id):"";
                    $film = Film::model()->find(array('condition' => 'id=:id','params' => array(':id' =>$movie_id)));
                    $statusvoucher='N/A';
                    if($currdatetime<=$expirytime && $end_date!='0000-00-00 00:00:00' && strtotime($end_date)!=""){
                        $statusvoucher='Active';  
                    }
                    if($end_date!='0000-00-00 00:00:00' && strtotime($end_date)!=""){
                       $statusvoucher='Active'; 
                    }
                    if($end_date=='0000-00-00 00:00:00'){
                        $statusvoucher='Active';  
                    }  
                    if($statusvoucher=='Active'){
                        $datamovie_stream = Yii::app()->db->createCommand()
                            ->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
                            ->from('movie_streams ms ')
                            ->where('ms.id='.$episode_id)
                            ->queryRow();  
                        $embed_id=$datamovie_stream['embed_id'];
                        $vmovie_names=$movie_name;

                        if($movie_id!=0 && $season_id==0 && $episode_id==0 ){
                            $vmovie_names=$movie_name;
                            $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink;
                    }
                        if($movie_id!=0 && $season_id!=0 && $episode_id!=0){
                            $vmovie_names=$movie_name." -> ".$translate['season']." ".$season_id." ->".$episode_name; 
                            $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink.'/stream/'.$embed_id;
                }
                    if($movie_id!=0 && $season_id!=0 && $episode_id==0){
                        $vmovie_names=$movie_name." -> ".$translate['season']." ".$season_id;
                        $baseurl=Yii::app()->getBaseUrl(true).'/'.$film->permalink.'?season='.$season_id; 
                    }
                    if($film->content_types_id == 2){
                        $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                        }else{
                        $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                    }
                        
                        if($film->content_types_id==3){
                                if($episode_id!=0){
							$stm = movieStreams::model()->findByAttributes(array('id'=>$episode_id));
							   }else if($season_id!=0 && $episode_id==0){
							$stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id,'is_episode'=>0));
							   }else{
							$stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
							   }
                        }else{
                        $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$movie_id));
                        }

                        $library[$key]['name'] = $vmovie_names;
                        $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                        $library[$key]['movie_id'] = $movie_id;
                        $library[$key]['movie_stream_id'] = $stm->id;
                        $library[$key]['is_episode'] = isset($datamovie_stream['is_episode'])?$datamovie_stream['is_episode']:0;
                        $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                        $library[$key]['content_types_id'] = $film->content_types_id;
                        $library[$key]['content_type_id'] = $film->content_type_id;
                        $library[$key]['baseurl'] = $baseurl;
                        $library[$key]['poster_url'] = $poster;
                        $library[$key]['video_duration'] = $stm->video_duration;   
                        $library[$key]['genres'] = json_decode($film->genre);
                        $library[$key]['permalink'] = $film->permalink;
                        $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                        $library[$key]['full_movie'] = $stm->full_movie;
                        $library[$key]['story'] = $film->story;;
                        $library[$key]['release_date'] = $film->release_date;
                        $library[$key]['is_converted'] = $stm->is_converted;
                        $library[$key]['isFreeContent'] = 0;
                        $library[$key]['season_id'] = $season_id;
                    }
                    }
                            }
				}
            $newarr = array_merge($library,$free);
            if($newarr){
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['mylibrary'] = $newarr;
            }else{
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = $translate['content_not_found'];                        
            }
        }else{
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = $translate['user_not_found'];           
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    /**
     * @method actionGetMenus() Returns the list of menus in tree structure
     * @param String $authToken A authToken
     * @author Ratikanta<support@muvi.com>
     * @return Returns the list of menus in tree structure
     */
    function actionGetMenus() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $mainmenu = Yii::app()->custom->getMainMenuStructure('array', $language_id,$studio_id); 
        $user_id  = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) ? $_REQUEST['user_id'] : 0;
        $usermenu = Yii::app()->custom->getUserMenuStructure('array', $language_id,$studio_id,$user_id,$translate);
        $sql = "SELECT domain, p.link_type,p.id, p.display_name, p.permalink, IF(p.link_type='external', p.external_url, concat('http://', domain, '/page/',p.permalink)) AS url FROM 
                    (SELECT id_seq, studio_id, external_url,id, link_type, title as display_name, permalink FROM pages WHERE studio_id={$studio_id} AND parent_id=0 AND status=1 AND permalink != 'terms-privacy-policy') AS p LEFT JOIN studios AS s ON p.studio_id=s.`id` ORDER BY p.id_seq ASC ";
        $pages = Yii::app()->db->createCommand($sql)->queryAll();
        if ($language_id != 20) {
            $pages = Yii::app()->custom->getStaticPages($studio_id, $language_id, $pages);
        }
        $org_menu = array('mainmenu' => $mainmenu, 'usermenu' => $usermenu,'footer_menu'=>$pages);
        if ($org_menu) {
            $data['status'] = 'OK';
            $data['code'] = 200;
            $data['msg'] = 'Success';
            $data['menus'] = $org_menu;
        } else {
            $data['code'] = 447;
            $data['status'] = "Error";
            $data['msg'] = "No Menu found!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }        
    function getTransData($lang_code='en', $studio_id){
        $studio = Studio::model()->findByPk($studio_id,array('select'=>'theme'));
        $theme = $studio->theme;
        if(!$lang_code)
            $lang_code = 'en';
        
        if(file_exists(ROOT_DIR."languages/".$theme."/".trim($lang_code).".php")){
           $lang = include( ROOT_DIR."languages/".$theme."/".trim($lang_code).".php");
        }elseif(file_exists(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php')){
            $lang = include(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php');
        }else{
            $lang = include(ROOT_DIR . 'languages/studio/en.php');
        }
        $language = Yii::app()->controller->getAllTranslation($lang,$studio_id);
        return $language;
    }
    public function actionGetMonetizationDetails(){
        $_REQUEST['data'] = $_REQUEST;
        $studio_id = $this->studio_id;
        $data = array();
        
        $userid = trim($_REQUEST['data']['user_id']);
        $movieid = trim($_REQUEST['data']['movie_id']);
        $streamid = trim($_REQUEST['data']['stream_id']);
        $seasonid = trim($_REQUEST['data']['season']);
        $purchase_type = trim($_REQUEST['data']['purchase_type']);
        
                
        if (isset($userid) && $userid > 0 && isset($movieid) && !empty($movieid)) {
            $data['movie_id'] = $movie_code = isset($movieid) ? $movieid : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where('f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];

            $content_types_id = $data['content_types_id'] = $films['content_types_id'];
            $data['stream_id'] = $stream_code = isset($streamid) ? $streamid : '0';
            $season = !empty($seasonid) ? $seasonid : 0;
            
            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code,'',$studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season,$studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id,'','',$studio_id);
            }
            
            $season_id = $season;
            if ($purchase_type == 'episode' && $season == 0) {
                $season_id = movieStreams::model()->findByPk($stream_id)->series_number;
            }
            
            $film = Film::model()->findByPk($movie_id);
            
            if (intval($content_types_id) == 3){
                $content = "'".$movie_id.":".$season_id.":".$stream_id."'";
            }else{
                $content = $movie_id;
            }
            
            $monitization_data = array();
            $monitization_data['studio_id'] = $studio_id;
            $monitization_data['user_id'] = $user_id;
            $monitization_data['movie_id'] = $movie_id;
            $monitization_data['season'] = @$season_id;
            $monitization_data['stream_id'] = @$stream_id;
            $monitization_data['content'] = @$content;
            $monitization_data['film'] = $film;
            $monitization_data['purchase_type'] = $purchase_type;

            $monetization = Yii::app()->common->getMonetizationsForContent($monitization_data);
            
            $df_plans = array('ppv'=>0,'pre_order'=>0,'voucher'=>0,'ppv_bundle'=>0);
            
            $monetization_data = $monetization['monetization'];
            foreach($monetization_data as $key => $value){
                if (array_key_exists($key, $df_plans)) {
                    $df_plans[$key] = 1;
                }
            }
            
            $data['status'] = 'OK';
            $data['code'] = 200;
            $data['msg'] = 'Success';
            $data['monetizations'] = $monetization_data;
            $data['monetization_plans'] = $df_plans;
            $this->setHeader($data['code']);
            echo json_encode($data);
            exit;
        }
        
    }
    
    public function actionGetPpvPlan(){
        $_REQUEST['data'] = $_REQUEST;
        
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        $season_id = @trim($_REQUEST['data']['season']);
        $stream_id = @trim($_REQUEST['data']['stream_id']);
        
        $data = array();
        
       
        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id;
            
            $movie_code = isset($movie_id) ? $movie_id : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where('f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            
            $content_types_id = $films['content_types_id'];
             
            $stream_code = isset($stream_id) ? $stream_id : '0';
            $season = isset($season_id) ? $season_id : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code,'',$studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, '', $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id,'','',$studio_id);
            }
           
            if (intval($content_types_id) == 3) {
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = $data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = $data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = $data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
                
                $EpDetails = $this->getEpisodeToPlay($movie_id,$studio_id);
                  
                if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
                    $series = explode(',', $EpDetails->series_number);
                    sort($series);

                    $max_season = $data['max_season'] = $series[count($series) - 1];
                    $min_season = $data['min_season'] = $series[0];

                    $series_number = intval($season) ? $season : $series[0];
                   
                    $episql = "SELECT * FROM movie_streams WHERE studio_id=" . $studio_id . " AND is_episode=1 AND movie_id=" . $movie_id . " AND series_number=" . $series_number . " AND episode_parent_id=0 ORDER BY episode_number ASC";
                    $episodes = Yii::app()->db->createCommand($episql)->queryAll();
                    $ep_data = array();
                    $episode_val = array();
                    if (isset($episodes) && !empty($episodes)) {
                        for($i=0;$i<count($episodes);$i++){
                            $episode_val['id'] = $episodes[$i]['id'];
                            $episode_val['movie_id'] = $episodes[$i]['movie_id'];
                            $episode_val['episode_title'] = $episodes[$i]['episode_title'];
                            $episode_val['embed_id'] = $episodes[$i]['embed_id'];
                            $ep_data[$i] = $episode_val;
                        }
                    }
                }
            }
            
            
            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            $connection = Yii::app()->db;
            
            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                
                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                $is_subscribed = Yii::app()->common->isSubscribed($user_id,$studio_id);
                if ($is_subscribed) {
                    $member_subscribed = 1;
                }

                $card_sql = "SELECT tot.* FROM (SELECT id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
                $cards = $connection->createCommand($card_sql)->queryAll();
                
                //If it has been on advance purchase
                $plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id, 0);
                if (empty($plan)) {
                    //Check ppv plan set or not by studio admin
                    $plan = Yii::app()->common->getContentPaymentType($content_types_id, $films['ppv_plan_id'],$studio_id);
                }
                
                $ppv_id = $plan->id;
                $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
                $price = Yii::app()->common->getPPVPrices($ppv_id, $default_currency_id);
                $currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;
                if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
                    $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
                    $season_price_data['ppv_pricing_id'] = $price['id'];
                    $season_price_data['studio_id'] = $studio_id;
                    $season_price_data['currency_id'] = $price['currency_id'];
                    $season_price_data['season'] = (intval($season)) ? $season : 1;
                    $season_price_data['is_subscribed'] = $is_subscribed;
                    $default_season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
                    if ($default_season_price != -1) {
                        $price['default_season_price'] = $default_season_price;
                    }
                }
                
                $currency = Currency::model()->findByPk($currency_id);
                $validity = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $data['is_coupon_exists'] = self::isCouponExists();
                
                $data['movie_code'] = $movie_code;
                $data['content_types_id'] = $content_types_id;
                $data['is_show'] = $is_show;
                $data['is_season'] = $is_season;
                $data['is_episode'] = $is_episode;
                $data['max_season'] = $max_season;
                $data['min_season'] = $min_season;
                $data['series_number'] = $series_number;
                $data['series_number'] = $member_subscribed;
                $data['episode'] = $ep_data;
                $data['can_save_card'] = $gateways[0]['paymentgt']['can_save_card'];
                $data['gateway_short_code'] = $gateways[0]['paymentgt']['short_code'];
                $data['card'] = $cards;
                $data['price'] = $price;
                $data['currency_id'] = $currency->id;
                $data['currency_symbol'] = $currency->symbol;
                $data['ppv_buy'] = $currency->symbol;
                $data['ppv_validity'] = $validity;
                $data['user_id'] = $user_id;
                $data['film'] = $films;
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = 'Success';
                
            }else{
                $data['code'] = 400;
                $data['status'] = "Failure";
                $data['msg'] = "Payment Gateway not exist.";
            }
        }else{
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "User id or Movie id not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    function actionGetPreOrderPlan() {
        $_REQUEST['data'] = $_REQUEST;
        
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        
        $data = array();
        
        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id;

            $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $content_type_id = $films['content_types_id'];
            
            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            $connection = Yii::app()->db;
            
            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                
                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                if ($is_subscribed) {
                    $member_subscribed = 1;
                }

                $card_sql = "SELECT tot.* FROM (SELECT id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
                $cards = $connection->createCommand($card_sql)->queryAll();
                
                //If it has been on advance purchase
                $plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id, 0);
                if (isset($plan) && !empty($plan)) {
                    $ppv_id = $plan->id;
                    $data['isadv'] = 1;

                    $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
                    $price = Yii::app()->common->getPPVPrices($ppv_id, $default_currency_id);
                    $currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;

                    $currency = Currency::model()->findByPk($currency_id);

                    $validity = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                    $data['is_coupon_exists'] = self::isCouponExists();
                    $data['movie_id'] = $movie_code;
                    $data['films'] = $films;
                    $data['currency_id'] = $currency->id;
                    $data['currency_symbol'] = $currency->symbol;
                    $data['content_type_id'] = $content_type_id;
                    $data['card'] = $card;
                    $data['member_subscribed'] = $member_subscribed;
                    $data['can_save_card'] = $gateways[0]['paymentgt']['can_save_card'];
                    $data['gateway_short_code'] = $gateways[0]['paymentgt']['short_code'];
                    $data['preorder_validity'] = $validity;
                    $data['status'] = 'OK';
                    $data['code'] = 200;
                    $data['msg'] = 'Success';
                }else{
                    $data['code'] = 400;
                    $data['status'] = "Failure";
                    $data['msg'] = "Pre-Order plan not exist.";
                }
                
            }else{
                $data['code'] = 400;
                $data['status'] = "Failure";
                $data['msg'] = "Payment Gateway not exist.";
            }
        }else{
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "User id or Movie id not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    function actionGetPpvBundlePlan() {
        $_REQUEST['data'] = $_REQUEST;
        
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        
        $data = array();
        
        //For login or registered user
        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id; 
            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                $movie_code = isset($movie_id) ? $movie_id : '0';
                $command = Yii::app()->db->createCommand()
                        ->select('f.id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                        ->from('films f ')
                        ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
                $films = $command->queryRow();
                $movie_id = $films['id'];
                $content_types_id = $data['content_types_id'] = $films['content_types_id'];
                $is_ppv_bundle = 1;
                $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
                
                $plan = Yii::app()->common->getAllPPVBundle($movie_id, $content_types_id, $films['ppv_plan_id'],'',$studio_id);
                if(isset($plan) && !empty($plan)){
                    
                    $ppv_id = $plan[0]->id;
                    if (isset($plan[0]->is_timeframe) && intval($plan[0]->is_timeframe)) {
                        $timeframe_prices = Yii::app()->common->getAllTimeFramePrices($ppv_id, $default_currency_id, $studio_id);
                        $currency_id = (isset($timeframe_prices[0]['currency_id']) && trim($timeframe_prices[0]['currency_id'])) ? $timeframe_prices[0]['currency_id'] : $default_currency_id;
                    }

                    $currency = Currency::model()->findByPk($currency_id);
                    $data['is_coupon_exists'] = self::isCouponExists();

                    $card_sql = "SELECT tot.* FROM (SELECT id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
                    $connection = Yii::app()->db;
                    $cards = $connection->createCommand($card_sql)->queryAll();
                    $data['movie_id'] = $movie_code;
                    $data['films'] = $films;
                    $data['is_ppv_bundle'] = $is_ppv_bundle;
                    $data['currency_id'] = $currency->id;
                    $data['currency_symbol'] = $currency->symbol;
                    $data['content_type_id'] = $content_types_id;
                    $data['card'] = $cards;
                    $data['can_save_card'] = $gateways[0]['paymentgt']['can_save_card'];
                    $data['gateway_short_code'] = $gateways[0]['paymentgt']['short_code'];
                    $data['timeframe_prices'] = $timeframe_prices;
                    $data['bundle_exp_date'] = $plan[0]->expiry_date;
                    $data['bundle_description'] = $plan[0]->expiry_date;
                    $data['bundle_time_frame'] = $plan[0]->is_timeframe;
                    $data['status'] = 'OK';
                    $data['code'] = 200;
                    $data['msg'] = 'Success';
                }else{
                    $data['code'] = 400;
                    $data['status'] = "Failure";
                    $data['msg'] = "PPV Bundle Plan not exist.";
                }
            }else{
                $data['code'] = 400;
                $data['status'] = "Failure";
                $data['msg'] = "Payment Gateway not exist.";
            }
        }else{
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "User id or Movie id not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    function isCouponExists() {
        $studio_id = $this->studio_id;
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        
        $isCouponExists = 0;
        if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 32)) {
            $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
            //$sql = "SELECT c.id FROM coupon c, coupon_currency cc WHERE c.studio_id = {$studio_id} AND c.status=1 AND c.id=cc.coupon_id AND cc.currency_id={$default_currency_id}";
            $sql = "SELECT c.id FROM coupon c WHERE c.studio_id = {$studio_id} AND c.status=1";
            $coupon = Yii::app()->db->createCommand($sql)->queryAll();
            
            if (isset($coupon) && !empty($coupon)) {
                $isCouponExists = 1;
            }
        }
        
        return $isCouponExists;
    }
    
    public function actionGetVoucherPlan() {
        $_REQUEST['data'] = $_REQUEST;
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        $season_id = @trim($_REQUEST['data']['season']);
        $stream_id = @trim($_REQUEST['data']['stream_id']);
        
        $data = array();
        
        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id;

            $movie_code = isset($movie_id) ? $movie_id : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $content_types_id = $films['content_types_id'];
            $stream_code = isset($stream_id) ? $stream_id : '0';
            $season = !empty($season_id) ? $season_id : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code,'',$studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season,'',$studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id,'','',$studio_id);
            }
            
            if(intval($stream_id)) {
                if (intval($content_types_id) == 3) {
                    $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                    $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                    $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                    $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                    $EpDetails = $this->getEpisodeToPlay($movie_id,$studio_id);
                    
                    if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
                        $series = explode(',', $EpDetails->series_number);
                        sort($series);

                        $max_season = $series[count($series) - 1];
                        $min_season = $series[0];

                        $episql = "SELECT * FROM movie_streams WHERE studio_id=" . $studio_id . " AND is_episode=1 AND movie_id=" . $movie_id . " AND series_number=" . $min_season . " ORDER BY episode_number ASC";
                        $episodes = Yii::app()->db->createCommand($episql)->queryAll();
                         
                        $episode_data = array();
                        $episode_val = array();
                        if (isset($episodes) && !empty($episodes)) {
                            for($i=0;$i<count($episodes);$i++){
                                $episode_val['id'] = $episodes[$i]['id'];
                                $episode_val['movie_id'] = $episodes[$i]['movie_id'];
                                $episode_val['episode_title'] = $episodes[$i]['episode_title'];
                                $episode_val['embed_id'] = $episodes[$i]['embed_id'];
                                $episode_data[$i] = $episode_val;
                            }
                            
                        }
                    }
                }
                
                $sql_mstream = Yii::app()->db->createCommand()
                    ->select('ms.id,ms.movie_id,ms.episode_number,ms.series_number,ms.episode_title')
                    ->from('movie_streams ms ')
                    ->where(' ms.id=:ms_id AND ms.movie_id=:movie_id', array(':ms_id' => $stream_id, ':movie_id' => $movie_id));
                $stream_data = $sql_mstream->queryAll();
                $content_name = "";
                $search_str = "";
                $con_type = "";
                if($stream_data[0]['movie_id']!=""){
                    $search_str .= $stream_data[0]['movie_id'];
                    $content_name .= $films['name'];
                }if($stream_data[0]['series_number']!=0 && intval($content_types_id) == 3){
                    $search_str .= ":".$stream_data[0]['series_number'];
                    $content_name .= "-Season-".$stream_data[0]['series_number'];
                }if($stream_data[0]['episode_number']!="" && intval($content_types_id) == 3){
                    if($stream_data[0]['series_number']==0){
                        $search_str .= ":0:".$stream_data[0]['id'];
                    }else{
                        $search_str .= ":".$stream_data[0]['id'];
                        $content_name .= "-Season-".$stream_data[0]['episode_title'];
                    }
                }
				
                $content = "'".$search_str."'";
                $voucher_exist = Yii::app()->common->isVoucherExists($studio_id,$content);
                if($voucher_exist){
                    $data['status'] = 'OK';
                    $data['code'] = 200;
                    $data['msg'] = 'Success';
                    $data['user_id'] = $user_id;
                    $data['content_types_id'] = $content_types_id;
                    $data['is_show'] = $is_show;
                    $data['is_season'] = $is_season;
                    $data['is_episode'] = $is_episode;
                    $data['max_season'] = $max_season;
                    $data['min_season'] = $min_season;
                    $data['episodes'] = $episode_data;
                    $data['film'] = $films;
                }else{
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "Voucher not exist for this content";
                }
            } 
        }else{
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "User id or Movie id not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    public function actionValidateVoucher() {
        $_REQUEST['data'] = $_REQUEST;
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        $season_id = @trim($_REQUEST['data']['season']);
        $stream_id = @trim($_REQUEST['data']['stream_id']);
        $voucher_code = @trim($_REQUEST['data']['voucher_code']);
        $purchase_type = @trim($_REQUEST['data']['purchase_type']);
        $lang_code   = @$_REQUEST['lang_code'];
        
        $data = array();
        
        
        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id;
            $translate   = $this->getTransData($lang_code,$studio_id);
            $movie_code = isset($movie_id) ? $movie_id : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $content_types_id = $films['content_types_id'];
            $stream_code = isset($stream_id) ? $stream_id : '0';
            $season = !empty($season_id) ? $season_id : 0;
            
            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code,'',$studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season,'',$studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id,'','',$studio_id);
            }
            
            $content_id = "";
            if($content_types_id == 3 && $stream_id!="" && $purchase_type == 'episode'){
                if($movie_id != ""){
                    $content_id .= $movie_id;
                }if($season_id != ""){
                    $content_id .= ":".$season;
                }if($stream_id != ""){
                    $content_id .= ":".$stream_id;
                }
            }else if($content_types_id == 3 && $purchase_type == 'season'){
                if($movie_id != ""){
                    $content_id .= $movie_id;
                }if($season_id != ""){
                    $content_id .= ":".$season_id;
                }
            }else{
                $content_id .= $movie_id;
            }
            
            if(isset($voucher_code) && $voucher_code && isset($content_id)){
                $voucherDetails = Yii::app()->common->voucherCodeValid($voucher_code,$user_id,$content_id,$studio_id);
                switch ($voucherDetails) {
                    case 1:
                        $data['movie_id'] = $movie_code;
                        $data['season'] = $season;
                        $data['stream_id'] = $stream_code;
                        $data['content_types_id'] = $content_types_id;
                        $data['status'] = 'OK';
                        $data['code'] = 200;
                        $data['msg'] = 'Success';
                        break;
                    case 2:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['invalid_voucher'];
                        $data['code'] = 400;
                        $data['status'] = "Failure";
                        break;
                    case 3:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['voucher_already_used'];
                        $data['code'] = 458;
                        $data['status'] = "Failure";
                        break;
                    case 4:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['free_content'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                    case 5:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['already_purchased_content'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                    case 6:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['access_period_expired'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                    case 7:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['watch_period_expired'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                    case 8:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['crossed_max_limit_of_watching'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                }
            }else{
                $data['msg'] = 'Voucher Code not exist!';
                $data['code'] = 400;
                $data['status'] = "Failure";
            }
            
        }else{
            $data['msg'] = 'User id or Movie id not found.';
            $data['code'] = 400;
            $data['status'] = "Failure";
        }
            $this->setHeader($data['code']);
            echo json_encode($data);
            exit;
        }
       
    public function actionVoucherSubscription() {
        
        $_REQUEST['data'] = $_REQUEST;
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        $season_id = @trim($_REQUEST['data']['season']);
        $stream_id = @trim($_REQUEST['data']['stream_id']);
        $voucher_code = @trim($_REQUEST['data']['voucher_code']);
        $purchase_type = @trim($_REQUEST['data']['purchase_type']);
        $is_preorder = @trim($_REQUEST['data']['is_preorder']);
        $lang_code   = @$_REQUEST['lang_code'];
        $data = array();
        
        
        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $data = array();
            $studio_id = $this->studio_id;
            $translate   = $this->getTransData($lang_code,$studio_id);
            $movie_code = isset($movie_id) ? $movie_id : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $content_types_id = $films['content_types_id'];
            $stream_code = isset($stream_id) ? $stream_id : '0';
            $season = !empty($season_id) ? $season_id : 0;
            
            
            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code,'',$studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season,'',$studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id,'','',$studio_id);
            }
            
            $content_id = "";
            if($content_types_id == 3 && $stream_id!="" && $purchase_type == 'episode'){
                if($movie_id != ""){
                    $content_id .= $movie_id;
                }if($season_id != ""){
                    $content_id .= ":".$season;
                }if($stream_id != ""){
                    $content_id .= ":".$stream_id;
                }
            }else if($content_types_id == 3 && $purchase_type == 'season'){
                if($movie_id != ""){
                    $content_id .= $movie_id;
                }if($season_id != ""){
                    $content_id .= ":".$season_id;
                }
            }else{
                $content_id .= $movie_id;
            }
            $isadv = (isset($is_preorder)) ? intval($is_preorder) : 0;
            
            $getVoucher = Yii::app()->common->voucherCodeValid($voucher_code, $user_id, $content_id, $studio_id);
            if ($getVoucher == 2) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['invalid_voucher'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else if ($getVoucher == 3) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['voucher_already_used'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            }else if ($getVoucher == 4) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['free_content'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else if ($getVoucher == 5) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['already_purchased_content'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else {
                $sql = "SELECT id FROM voucher WHERE studio_id={$studio_id} AND voucher_code='" . $voucher_code . "'";
                $voucher_id = Yii::app()->db->createCommand($sql)->queryRow();
                
                $start_date = Date('Y-m-d H:i:s');
                $end_date = '';
                if (!empty($voucher_id)) {
                    if (intval($isadv) == 0) {
                        $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'limit_video');
                        $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;
                        
                        $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'watch_period');
                        $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

                        $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'access_period');
                        $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';
                        
                        $time = strtotime($start_date);
                        if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                            $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                        }
                        
                        $data['view_restriction'] = $limit_video;
                        $data['watch_period'] = $watch_period;
                        $data['access_period'] = $access_period;
                    }
                    $data['movie_id'] = $movie_code;
                    $data['content_types_id'] = $content_types_id;
                    $data['voucher_id'] = $voucher_id['id'];
                    $data['content_id'] = $content_id;
                    $data['user_id'] = $user_id;
                    $data['studio_id'] = $studio_id;
                    $data['isadv'] = $isadv;
                    $data['start_date'] = @$start_date;
                    $data['end_date'] = @$end_date;
                    
                    $VideoName = CouponOnly::model()->getContentInfo($content_id,$isadv);
                    $res = Yii::app()->billing->setVoucherSubscription($data);
					
                    if ($isadv) {
                        $data['status'] = 'OK';
                        $data['code'] = 200;
                        $data['msg'] = 'Success';
                    }else{
                        $data['status'] = 'OK';
                        $data['code'] = 200;
                        $data['msg'] = 'Success';
                    }
                } else {
                    $data['isError'] = $getVoucher;
                    $data['msg'] = $translate['invalid_voucher'];
                    $data['code'] = 400;
                    $data['status'] = "Failure";
                }
            }
        } else {
            $data['isError'] = $getVoucher;
            $data['msg'] = $translate['invalid_voucher'];
            $data['code'] = 400;
            $data['status'] = "Failure";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    function actionGetppvPlans() {
        $_REQUEST['data'] = $_REQUEST;
        $studio_id = $this->studio_id;
        $data = array();
        
        $userid = trim($_REQUEST['data']['user_id']);
        $movieid = trim($_REQUEST['data']['movie_id']);
        $streamid = trim($_REQUEST['data']['stream_id']);
        $seasonid = trim($_REQUEST['data']['season']);
        $purchase_type = trim($_REQUEST['data']['purchase_type']);

        //For login or registered user
        if (isset($userid) && $userid > 0 && isset($movieid) && !empty($movieid)) {
            $studio_id = $this->studio_id;
            $user_id = $userid;
            
            $movie_code = isset($movieid) ? $movieid : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id, f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where('f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            
            $data = array();

            $stream_id = $stream_code = !empty($streamid) ? $streamid : '0';
            $multipart_season = $season = !empty($seasonid) ? $seasonid : 0;
            $purchase_type = @$purchase_type;
                    
            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code,'',$studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season,'',$studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id,'','',$studio_id);
            }
            $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
            $can_see_data = array();
            $can_see_data['movie_id'] = $movie_id;
            $can_see_data['stream_id'] = @$stream_id;
            $can_see_data['season'] = @$season;
            $can_see_data['purchase_type'] = @$purchase_type;
            $can_see_data['studio_id'] = $studio_id;
            $can_see_data['user_id'] = $user_id;
            $can_see_data['default_currency_id'] = $default_currency_id;
            $can_see_data['is_monetizations_menu'] = 1;
            
            //print_r($can_see_data);exit;
            
            $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
            $error = array();
            if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
                $data['code'] = 425;
                $data['status'] = "False";
                $data['msg'] = 'Please activate your subscription to watch video.';
            } else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
                $data['code'] = 426;
                $data['status'] = "False";
                $data['msg'] = 'Please reactivate your subscription to watch video.';
            } else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
                $data['code'] = 427;
                $data['status'] = "False";
                $data['msg'] = 'This video is not allowed to play in your country.';
            } else if ($mov_can_see == 'advancedpurchased') {
                $data['code'] = 431;
                $data['status'] = "False";
                $data['msg'] = 'You have already purchased this content earlier.';
            } else if ($mov_can_see == 'allowed') {
                $data['code'] = 429;
                $data['status'] = "OK";
                $data['msg'] = 'You will be allow to watch this video.';
            } else {
                if (array_key_exists ( 'playble_error_msg' , $mov_can_see )) {
                    if ($mov_can_see['playble_error_msg'] == 'access_period') {
                        $data['code'] = 428;
                        $data['status'] = "False";
                        $data['msg'] = 'Sorry, your access period for this content has been expired';
                    } else if ($mov_can_see['playble_error_msg'] == 'watch_period') {
                        $data['code'] = 428;
                        $data['status'] = "False";
                        $data['msg'] = 'Sorry, your watch period for this content has been expired';
                    } else if ($mov_can_see['playble_error_msg'] == 'maximum') {
                        $data['code'] = 428;
                        $data['status'] = "False";
                        $data['msg'] = 'Sorry, you have crossed the maximum limit of watching this content.';
                    }
                }else{
                
                    $monetization = $mov_can_see;
                    $content_types_id = $films['content_types_id'];

                    $data['isadv'] = $isadv = isset($_REQUEST['isadv']) ? $_REQUEST['isadv'] : 0;
                    $data['is_ppv_bundle'] = $is_ppv_bundle = isset($_REQUEST['is_ppv_bundle']) ? $_REQUEST['is_ppv_bundle'] : 0;

                    if (intval($content_types_id)== 3) {
                        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                        $data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                        $data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                        $data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
                    }

                    $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
                    if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                        $gateways = $gateway['gateways'];
                        $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();
                    }
                    $is_preorder_content = @$_POST['isadv'];
                    $not_refresh_on_close = (isset($_POST['onCloseNotRefresh']) && $_POST['onCloseNotRefresh'] == 1 ) ? true : false;
                    
                    $data['monetization'] = $monetization;
                    $data['films'] = $films;
                    $data['stream_id'] = $stream_id;
                    $data['multipart_season'] = $multipart_season;
                    $data['purchase_type'] = $purchase_type;
                    $data['is_preorder_content'] = $is_preorder_content;
                    $data['not_refresh_on_close'] = $not_refresh_on_close;
                    $data['status'] = 'OK';
                    $data['code'] = 200;
                    $data['msg'] = 'Success';
                    
                }
            }
            
        } else {
            $data['msg'] = 'User id or Movie id not found.';
            $data['code'] = 400;
            $data['status'] = "Failure";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
    public function actionGetStaticPagedetails(){
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        if(isset($_REQUEST['permalink']) &&  $_REQUEST['permalink'] != ""){
            $permalink  = $_REQUEST['permalink'];
            $sql   = "SELECT * FROM pages where studio_id=".$studio_id." AND permalink ='".$permalink."' AND status=1 AND parent_id=0";
            $page  = Yii::app()->db->createCommand($sql)->queryRow();
            if(!empty($page)){
                if ($language_id != 20) {
                    $page = Yii::app()->custom->getStaticPageDetails($studio_id, $language_id, $page);
                }
                $page['content'] = html_entity_decode($page['content']);
                $data['status'] = 'OK';
                $data['code'] = 200;
                $data['msg'] = 'Success';
                $data['page_details'] = $page;
            }else{
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Content not found.";          
            }
        }else{
            $data['code'] = 448;
            $data['status'] = "Failure";
            $data['msg'] = "Required parameters not found";            
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }   
    public function actionContactUs(){
        $studio_id   = $this->studio_id;
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code, $studio_id);        
        if(isset($_REQUEST['email']) && trim($_REQUEST['email'])!=""){
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $studio      = Studio::model()->findByPk($studio_id, array('select'=>'name,contact_us_email,domain'));
            $subject     = $studio->name . ' :: New Contact Us';
            $site_url    = "https://".$studio->domain;
            $siteLogo    = Yii::app()->common->getLogoFavPath($studio_id);
            $logo        = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" /></a>';
            $site_link   = '<a href="' . $site_url . '">' . $studio->name . '</a>';
            $msg         = htmlspecialchars(@$_REQUEST['message']);
            $useremail   = @$_REQUEST['email'];
            $username = '';
            if (isset($_REQUEST['name']) && strlen(trim($_REQUEST['name'])) > 0){
                $username = htmlspecialchars($_REQUEST['name']);
            }
            $template_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                "message" => $msg
            );
            if ($studio->contact_us_email) {
                $admin_to [] = $studio_email = $studio->contact_us_email;
                if (strstr($studio_email, ',')) {
                    $admin_to     = explode(',', $studio_email);
                    $studio_email = $admin_to[0];
                }
            } else {
                $studio_user   = User::model()->findByAttributes(array('studio_id' => $studio_id));
                $studio_email  = $studio_user->email;                
                $linked_emails = EmailNotificationLinkedEmail::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'notification_from_email'));
                if ($linked_emails->notification_from_email) {
                    $studio_email = $linked_emails->notification_from_email;
                }
                $admin_to[] = $studio_email;
            }
            $studio_name   = $StudioName = $studio->name;
            $EndUserName   = $username;
            $admin_subject = $subject;
            $admin_from    = $useremail;
            $email_type    = 'contact_us';
            $admin_cc      = Yii::app()->common->emailNotificationLinks($studio_id, $email_type); 
            if(empty($admin_cc) || $admin_cc !=""){
                $admin_cc  = array();
            }
            $user_to       = array($useremail);
            $user_from     = $studio_email;
            $content  = "SELECT * FROM `notification_templates` WHERE type= '".$email_type."' AND studio_id=".$studio_id." AND (language_id = ".$language_id." OR parent_id=0 AND id NOT IN (SELECT parent_id FROM `notification_templates` WHERE type= '".$email_type."' AND studio_id=" . $studio_id." AND language_id = ".$language_id."))";
            $data     = Yii::app()->db->createCommand($content)->queryAll();
            if(empty($data)) {                
                $temp = New NotificationTemplates;
                $data = $temp->findAllByAttributes(array('studio_id' => 0, 'type' => $email_type));
            }
            $user_subject = $data[0]['subject'];
            eval("\$user_subject = \"$user_subject\";");
            $user_content = htmlspecialchars($data[0]['content']);
            eval("\$user_content = \"$user_content\";");
            $user_content = htmlspecialchars_decode($user_content); 
            $template_user_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                'content'=>$user_content
            );
            Yii::app()->theme = 'bootstrap';
            $admin_html = Yii::app()->controller->renderPartial('//email/studio_contactus', array('params' => $template_content), true);
            $user_html = Yii::app()->controller->renderPartial('//email/studio_contactus_user', array('params' => $template_user_content), true);
            $returnVal_admin = $this->sendmailViaAmazonsdk($admin_html, $admin_subject, $admin_to, $admin_from,$admin_cc,'','', $username);
            $returnVal_user = $this->sendmailViaAmazonsdk($user_html, $user_subject, $user_to, $user_from,'','','', $studio_name);
            $contact['status']      = 'OK';
            $contact['code']        = 200;
            $contact['msg']         = 'Success';
            $contact['success_msg'] = $translate['thanks_for_contact'];
        }else{
            $contact['code'] = 448;
            $contact['status'] = "Failure";
            $contact['msg'] = "Failure";
            $contact['error_msg'] = $translate['oops_invalid_email'];
        }
        $this->setHeader($contact['code']);
        echo json_encode($contact);
        exit;
    }    
    
    /*APIs for managing favourite contents*/
    function CheckFavouriteEnable($studio_id){
        $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_favourite_list');
        if(!empty($fav_status)){
            $result = $fav_status['config_value'];
        }
        else{
            $result = 0;
        }
        return $result;
    }
    
    public function actionGetFilteredContent() {
        if (isset($_REQUEST)) {
            $list = array();
            $cat = array();
            $order = true;
            $studio_id = $this->studio_id;
            $lang_code = @$_REQUEST['lang_code'];
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") ? $_REQUEST['user_id'] : 0;
            $is_episode = (isset($_REQUEST['is_episode']) && $_REQUEST['is_episode'] != "") ? $_REQUEST['is_episode'] : "";
            $translate = $this->getTransData($lang_code, $studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $content_category_value = 0;
            $condition = "";
            $sort_by = 'F.name';
            $sort_cond = $sort_by;
            $order_by = 'ASC';
            $cond = '';
            $group_by = (isset($_REQUEST['group_by']) && $_REQUEST['group_by'] != '') ? $_REQUEST['group_by'] : '';
            if (isset($_REQUEST['category']) && $_REQUEST['category'] != "") {
                $categories = $_REQUEST['category'];
                if (!is_array($categories)) {
                    $categories = explode(",", $categories);
                }
                $permalink = "";
                if (!empty($categories)) {
                    $cond = " AND permalink IN ";
                    $total = count($categories);
                    for ($i = 0; $i < $total; $i++) {
                        $comma = " ";
                        if ($total != ($i + 1)) {
                            $comma = ",";
                        }
                        $permalink .= "'" . $categories[$i] . "'";
                        $permalink .= $comma;
                    }
                    $cond .= " (" . $permalink . ") ";
                }
                $sql = "SELECT GROUP_CONCAT(id) as content_category_value FROM content_category WHERE parent_id =0 AND studio_id=" . $studio_id . " " . $cond;
                $res = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($res)) {
                    $content_category_value = $res['content_category_value'];
                }
            }
            if ($group_by == "category") {
                $sql = "SELECT id,category_name,permalink,binary_value FROM content_category WHERE parent_id =0 AND studio_id=" . $studio_id . "  " . $cond . " ORDER BY category_name ASC";
                $cat = Yii::app()->db->createCommand($sql)->queryAll();
            }
            if ($content_category_value) {
                foreach (explode(',', $content_category_value) as $key => $value) {
                    $condition1 .= " FIND_IN_SET($value, F.content_category_value) OR ";
                }
                $condition = " AND (" . rtrim($condition1, 'OR ') . ") ";
            }
            $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            if (isset($_REQUEST['order'])) {
                $order_by = $_REQUEST['order'];
            }
            if (isset($_REQUEST['sort'])) {
                $sort = explode(',', $_REQUEST['sort']);
                $sort_cond = array();
                foreach ($sort as $fvalue) {
                    $fvalue = explode(':', $fvalue);
                    $sort_value = $fvalue[0];
                    $sort_order = "";
                    if (count($fvalue) > 1) {
                        $order = false;
                        $sort_order = " " . $fvalue[1];
                    }
                    $fields = $this->GetTableName($sort_value);
                    $ts = ($fields['table'] == "movie_streams") ? "M" : "F";
                    if (trim($fields['field']) != "") {
                        $field_name = $ts . "." . $fields['field'];
                        if ((trim($sort_order) == "" && !($order))) {
                            $sort_order = " " . $order_by;
                        }
                        $sort_cond[] = $field_name . $sort_order;
                    }
                }
                $sort_by = implode(',', $sort_cond);
            }
            if ($order == true) {
                $order = $sort_by . " " . $order_by;
            } else {
                $order = $sort_by;
            }
            $filter_cond = "";
            foreach ($_REQUEST as $key => $value) {
                $get_filter_details = $this->GetTableName($key);
                if (!empty($get_filter_details)) {
                    if (trim($value) != "") {
                        $ts = ($get_filter_details['table'] == "movie_streams") ? "M" : "F";
                        $field_name = $ts . "." . $get_filter_details['field'];
                        $fdata = explode(',', trim($value));
                        $total = count($fdata);
                        for ($i = 0; $i < $total; $i++) {
                            $andor = ($i == 0) ? ' AND (' : ' OR';
                            $filter_cond .= $andor . ' ' . $field_name . ' LIKE \'%' . $fdata[$i] . "%' ";
                        }
                        $filter_cond .= ") ";
                    }
                }
            }
            if(isset($_REQUEST['live_field']) && in_array($_REQUEST['live_field'],array('start_time'))){
                $date_today = date('Y-m-d H:i:s', strtotime(date('m/d/Y')));
                $filter_cond .= " AND ".$_REQUEST['live_field']." >='".$date_today."'";
            }
            if(isset($_REQUEST['range_field']) && in_array($_REQUEST['range_field'],array('release_date'))){
                $date_today = date('Y-m-d', strtotime(date('m/d/Y')));
                $date_to = date('Y-m-d', strtotime(date('m/d/Y') . "-1 day"));
                $date_from = date('Y-m-d', strtotime(date('m/d/Y') . "-8 day"));
                $filter_cond .= " AND ".$_REQUEST['range_field']." >='".$date_from."' AND ".$_REQUEST['range_field']." <='".$date_to."'";
            }
            if(isset($_REQUEST['today_field']) && in_array($_REQUEST['today_field'],array('release_date'))){
                $date_today = date('Y-m-d', strtotime(date('m/d/Y')));
                $filter_cond .= " AND ".$_REQUEST['today_field']." ='".$date_today."'";
            }
            if (isset($_REQUEST['cast']) && ($_REQUEST['cast'] != 'null')) {
                $q = $_REQUEST['cast'];
                $casts = Yii::app()->db->createCommand()
                        ->select('id,parent_id,language_id')
                        ->from('celebrities')
                        ->where('studio_id=:studio_id AND language_id=:language_id AND LOWER(name) LIKE  :search_string ', array(':studio_id' => $studio_id, ':search_string' => "$q", ':language_id' => $language_id))
                        ->queryAll();
                $cast_ids = array();
                if (!empty($casts)) {
                    foreach ($casts as $cast) {
                        if ($cast['parent_id'] > 0) {
                            $cast_ids[] = $cast['parent_id'];
                        } else {
                            $cast_ids[] = $cast['id'];
                        }
                    }
                }
                if (!empty($cast_ids)) {
                    $movieIds = Yii::app()->db->createCommand()
                            ->select('GROUP_CONCAT(DISTINCT mc.movie_id) AS movie_ids')
                            ->from('movie_casts mc')
                            ->where('mc.celebrity_id IN (' . implode(',', $cast_ids) . ') ')
                            ->queryAll();
                    if (@$movieIds[0]['movie_ids']) {
                        $filter_cond .= ' AND F.id IN (' . $movieIds[0]['movie_ids'] . ') ';
                    } else {
                        $filter_cond .= ' AND F.id =0 ';
                    }
                }
            }
            $episode_cond = "";
            if ($is_episode != "") {
                $episode_cond = " AND M.is_episode=" . $is_episode;
            }
            if (!empty($cat)) {
                foreach ($cat as $category) {
                    $category_id = $category['id'];
                    $poster_category = $this->getPoster($category_id, 'content_category');
                    $category_name = $category['category_name'];
                    $category_permalink = $this->siteurl . '/' . $category['permalink'];
                    $binary_value = $category['binary_value'];
                    $sql = "SELECT id,name,story,permalink,parent_id FROM films WHERE studio_id =" . $studio_id . " AND FIND_IN_SET($category_id,content_category_value) AND parent_id=0 ORDER BY name,parent_id ASC";
                    $films = Yii::app()->db->createCommand($sql)->queryAll();
                    $content_details = array();
                    $poster_film = array();
                    if (!empty($films)) {
                        foreach ($films as $film) {
                            $id = $film['id'];
                            $name = $film['name'];
                            $story = $film['story'];
                            $langcontent = Yii::app()->custom->getTranslatedContent($id, 0, $language_id, $studio_id);
                            if (array_key_exists($id, $langcontent['film'])) {
                                $name = $langcontent['film'][$id]->name;
                                $story = $langcontent['film'][$id]->story;
                            }
                            $fav_status = 1;
                            $login_status = 0;
                            if ($user_id) {
                                $fav_status = UserFavouriteList::model()->getFavouriteContentStatus($id, $studio_id, $user_id);
                                $fav_status = ($fav_status == 1) ? 0 : 1;
                                $login_status = 1;
                            }
                            $poster = $this->getPoster($id, 'films');
                            $permalink = $this->siteurl . '/' . $film['permalink'];
                            $content_details[] = array(
                                'content_id' => $id,
                                'name' => $name,
                                'story' => $story,
                                'poster' => $poster,
                                'content_permalink' => $permalink,
                                'is_episode' => 0,
                                'fav_status' => $fav_status,
                                'login_status' => $login_status
                            );
                        }
                    }

                    $channels[] = array(
                        'category_name' => $category_name,
                        'permalink' => $category_permalink,
                        'link' => $category['permalink'],
                        'category_poster' => $poster_category,
                        'content_list' => $content_details
                    );
                }
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['lists'] = $channels;
                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            } else {
                $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.language,F.content_types_id,F.language_id,M.episode_language_id')
                        ->from('movie_streams M,films F')
                        ->where('M.movie_id = F.id AND F.studio_id =' . $this->studio_id . ' AND F.parent_id = 0 AND M.episode_parent_id =0 ' . $condition . ' ' . $filter_cond . '' . $episode_cond)
                        ->order($order)
                        ->limit($limit, $offset);
                $contents = $command->QueryAll();
                $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                $customData = array();
                $list = array();
                if (!empty($contents)) {
                    foreach ($contents as $content) {
                        $is_episode = $content['is_episode'];
                        $content_id = $content['movie_id'];
                        if ($is_episode == 1) {
                            $content_id = $content['movie_stream_id'];
                        }
                        $list[] = Yii::app()->general->getContentData($content_id, $is_episode, $customData, $language_id, $studio_id, $user_id, $translate);
                    }
                }
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['lists'] = $list;
                if (!empty($list)) {
                    $data['total_content'] = $item_count;
                } else {
                    $data['msg'] = "No content found";
                }
            }
        } else {
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "Invalid data sent.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function GetTableName($key){
        $request_array = array('authToken','lang_code','category','sort','order','limit','offset','Host','User-Agent','Accept','Accept-Encoding','Cookie','Content-Type','Content-Length');
        $film_array    = array('name','language','genre','rating','country','censor_rating','release_date');
        $movie_stream_array  = array('episode_title','episode_story','episode_date');
        if(!in_array($key,$request_array)){
            if(in_array($key, $film_array)){
                $get_filter_details = array(
                    'table' => 'films',
                    'field' => $key
                );
            }elseif(in_array($key, $movie_stream_array)){
                $get_filter_details = array(
                    'table' => 'movie_streams',
                    'field' => $key
                );
            }else{
                $get_filter_details = Yii::app()->custom->getFilterTableData($key, $this->studio_id);
            }
        }
        return $get_filter_details;
    }
    public function actionCheckFavourite(){
        $studio_id=$this->studio_id;
        $favenable=$this->CheckFavouriteEnable($studio_id);
        $data['code'] = 200;
        $data['status'] = "Success";
        $data['msg'] = $favenable;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }   
    public function actionViewFavourite(){
        $user_id = $_REQUEST['user_id'];
        $studio_id = $this->studio_id;
        $lang_code   = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
        $fav_status = $this->CheckFavouriteEnable($studio_id);
        $offset = 0;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
            $offset = ($_REQUEST['offset'] - 1) * $limit;
        }
        if(($fav_status !=0)){
            if($user_id > 0){
                $favlist = UserFavouriteList::model()->getUsersFavouriteContentList($user_id,$studio_id,$page_size,$offset,$language_id);
                $item_count = @$favlist['total'];
                $list = @$favlist['list'];
                $data['status'] = 200;
                $data['msg'] = 'OK';
                $data['movieList'] = @$list;
                $data['item_count'] = @$item_count;
                $data['limit'] = @$page_size;
            }else{
                $data['code'] = 411;
                $data['status'] = "Failure";
                $data['msg']="User Id not found";
            }
        }else{
            $data['code'] = 412;
            $data['status'] = "Failure";
            $data['msg']= "Add to favourite is not enable";
        }
        $this->setHeader($data['code']);        
        echo json_encode($data);
        exit;
    }
    public function actionAddtoFavlist() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code,$studio_id);
        if ((isset($_REQUEST['movie_uniq_id']) && ($_REQUEST['movie_uniq_id'] != '')) && (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !='')) {
            $uniq_id      = $_REQUEST['movie_uniq_id'];
            $content_type = isset($_REQUEST['content_type']) ? $_REQUEST['content_type'] : '0';
            $films        = new Film;
            $field        = 'uniq_id';
            if(($content_type == '1') || ($content_type == 1)){
                $films    = new movieStreams;
                $field    = 'embed_id';
            }
            $user_id      = @$_REQUEST['user_id'];
            if($user_id > 0){
                $data          = $films->findByAttributes(array($field => $uniq_id, 'studio_id' => $studio_id), array('select' => 'id'));
                if(!empty($data)){
                    $content_id    = $data->id;
                    $user_fav_stat = UserFavouriteList::model()->findByAttributes(array('content_id' => $content_id, 'content_type' => $content_type, 'studio_id' => $studio_id, 'user_id' => $user_id));
                    if (empty($user_fav_stat)) {
                        $usr_fav = new UserFavouriteList;
                        $usr_fav->status          = '1';
                        $usr_fav->content_id      = $content_id;
                        $usr_fav->content_type    = $content_type;
                        $usr_fav->studio_id       = $studio_id;
                        $usr_fav->user_id         = $user_id;
                        $usr_fav->date_added      = new CDbExpression("NOW()");
                        $usr_fav->last_updated_at = new CDbExpression("NOW()");
                    } else {
                        $usr_fav = UserFavouriteList::model()->findByPk($user_fav_stat->id);
                        if ($user_fav_stat->status == '0') {
                            $usr_fav->status          = '1';
                            $usr_fav->last_updated_at = new CDbExpression("NOW()");
                        }
                        else{
                            $res['code']   = 448;
                            $res['status'] = "Failure";
                            $res['msg']    = $translate['already_added_favourite'];
                            $this->setHeader($res['code']);
                            echo json_encode($res);
                            exit;
                        }
                    }
                    $usr_fav->save();
                    $res['code']   = 200;
                    $res['status'] = "Success";
                    $res['msg']    = $translate['added_to_fav'];
                }else{
                    $res['code']   = 406;
                    $res['status'] = "Failure";
                    $res['msg']    = $translate['required_data_not_found'];
                }
            }else{
                $res['code']   = 401;
                $res['status'] = "Failure";
                $res['msg']    = $translate['need_to_sign_in'];
            }
        } else {
            $res['code']   = 406;
            $res['status'] = "Failure";
            $res['msg']    = $translate['required_data_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
    public function actionDeleteFavList() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code,$studio_id);
        if ((isset($_REQUEST['movie_uniq_id']) && ($_REQUEST['movie_uniq_id'] != '')) && (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !='')) {
            $uniq_id      = $_REQUEST['movie_uniq_id'];
            $content_type = isset($_REQUEST['content_type'])? $_REQUEST['content_type'] : '0';
            $user_id      = @$_REQUEST['user_id'];
            if($user_id > 0){
                $films        = new Film;
                $field        = 'uniq_id';
                if(($content_type == '1') || ($content_type == 1)){
                    $films    = new movieStreams;
                    $field    = 'embed_id';
                }
                $data         = $films->findByAttributes(array($field => $uniq_id, 'studio_id' => $studio_id), array('select' => 'id'));
                if(!empty($data)){
                    $content_id    = $data['id'];
                    $params        = array(':content_id' => $content_id, ':content_type' => $content_type, ':studio_id' => $studio_id, ':user_id' => $user_id);
                    $user_fav_stat = UserFavouriteList::model()->deleteAll('content_id = :content_id AND content_type = :content_type AND studio_id = :studio_id AND user_id = :user_id', $params);
                    $res['code']   = 200;
                    $res['status'] = "Success";
                    $res['msg']    = $translate['content_remove_favourite'];
                }else{
                    $res['code']   = 406;
                    $res['status'] = "Failure";
                    $res['msg']    = $translate['required_data_not_found'];
                }
                
            }else{
                $res['code']   = 401;
                $res['status'] = "Failure";
                $res['msg']    = $translate['need_to_sign_in'];
            }
        }
        else{
            $res['code']   = 406;
            $res['status'] = "Failure";
            $res['msg']    = $translate['required_data_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
    /*Added by Biswajit Das(biswajitdas@muvi.com) for registering device for sending push notification*/
    public function actionRegisterMyDevice(){
        $studio_id = $this->studio_id;
        $lang_code=@$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);
            
        if ((isset($_REQUEST['device_id']) && ($_REQUEST['device_id'] != '')) && (isset($_REQUEST['fcm_token']) && $_REQUEST['fcm_token'] !='')) {
            $device_id = $_REQUEST['device_id'];
            $notification = new RegisterDevice;
            $registerdevice = $notification->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id));
            if(empty($registerdevice)){
                $register_device = new RegisterDevice;
                $register_device->studio_id = $studio_id;
                $register_device->device_type = isset($_REQUEST['device_type'])?@$_REQUEST['device_type']:'1';
                $register_device->device_id = isset($_REQUEST['device_id'])?@$_REQUEST['device_id']:'';
                $register_device->fcm_token = isset($_REQUEST['fcm_token'])?@$_REQUEST['fcm_token']:'';
                $register_device->save();
            }else{
                $registerdevice->fcm_token = isset($_REQUEST['fcm_token'])?@$_REQUEST['fcm_token']:'';
                $registerdevice->save();
            }
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['device_registerd'];
        }else{
            $res['code'] = 400;
            $res['status'] = "Failure";
            $res['msg'] = $translate['device_not_registerd'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
    public function actionNotificationLists(){
        $studio_id = $this->studio_id;
        $device_id = $_REQUEST['device_id'];
        $lang_code=@$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);
        $diff_days = isset($_REQUEST['before_days']) && $_REQUEST['before_days'] > 0?$_REQUEST['before_days']:7;
        $sql = "SELECT * FROM `push_notification_log` WHERE `studio_id` = '{$studio_id}' AND `device_id` = '{$device_id}' AND (`status` = '1' OR `status` = '2')";
        $sql.= " AND `created_date` >= DATE_SUB(CURDATE(), INTERVAL $diff_days DAY)";
        $all_notification = Yii::app()->db->createCommand($sql)->queryAll();
        if(!empty($all_notification)){
            $unread_cnt = 0;
            foreach($all_notification as $all_notif){
                if($all_notif['status'] == 1)
                  $unread_cnt++;  
            }
            
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['notifyList'] = @$all_notification;
            $res['count'] =count($all_notification);
            $res['count_unread'] = @$unread_cnt;
        }else{
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['no_notification'];
        }
        
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
    public function actionReadNotification(){
        $studio_id = $this->studio_id;
        $device_id = $_REQUEST['device_id'];
        $message_unique_id = $_REQUEST['message_unique_id'];
        $lang_code=@$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);
        if((isset($device_id) && $device_id !='') && (isset($message_unique_id) && $message_unique_id !='') ){
            $notification = new PushNotificationLog;
            $notificationstatus = $notification->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id ,'message_unique_id'=>$message_unique_id,'status' => '1'));
            $notificationstatus->status = '2';   
            $notificationstatus->save();
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['notification_read'];
        }
        else{
            $res['code'] = 400;
            $res['status'] = "Failure";
            $res['msg'] = 'Device Id not found';
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
    public function actionGetNoOfUnreadNotification(){
        $studio_id = $this->studio_id;
        $device_id = $_REQUEST['device_id'];
        $status = '1';
        $notification = new PushNotificationLog;
        $notificationstatus = $notification->findAllByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id,'status'=>$status));
        if(!empty($notificationstatus)){
            $no_of_notify = count($notificationstatus);
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['count'] = $no_of_notify;
        }
        else{
            $data['code'] = 400;
            $data['status'] = "Success";
            $data['count'] = 0;
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }  
    public function actionReadAllNotification() {
        $studio_id = $this->studio_id;
        $device_id = $_REQUEST['device_id'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($device_id) && $device_id != '')) {
            $notification = new PushNotificationLog;
            $attr = array('status' => '2');
            $condition = "studio_id=:studio_id AND device_id =:device_id AND status =:status";
            $params = array(':studio_id' => $studio_id, ':device_id' => $device_id, ':status' => '1');
            $notificationstatus = $notification->updateAll($attr, $condition, $params);
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['all_notification_read'];
        } 
        else {
            $res['code'] = 400;
            $res['status'] = "Failure";
            $res['msg'] = 'Device Id not found';
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    } 
	/**
         * API for Device Management Listing
     */
    public function actionManageDevices() {
		$studio_id = $this->studio_id;
		$lang_code = @$_REQUEST['lang_code'];
		$translate = $this->getTransData($lang_code, $studio_id);
        if (isset($_REQUEST['user_id'])) {
            $user_id = $_REQUEST['user_id'];
            $sql = Yii::app()->db->createCommand()
                       ->select(' * ')
                       ->from("device_management")
                       ->where('studio_id =:studio_id AND user_id=:user_id AND flag<=1',array(':studio_id' => $studio_id,':user_id'=>$user_id))
                       ->order ('id ASC')
                       ->queryAll();
            if ($sql) {
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = @$translate['device_list'];
                $res['device_list'] = $sql;
            } else {
                $res['code'] = 452;
                $res['status'] = 'Failure';
                $res['msg'] = @$translate['no_devices_available'];
            }
        } else {
            $res['code'] = 450;
            $res['status'] = "Failure";
            $res['msg'] = @$translate['user_information_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
	/**
	 * This function is used to request studio admin to remove device
	 */
    public function actionRemoveDevice() {
		$studio_id = $this->studio_id;
		$lang_code = @$_REQUEST['lang_code'];
		$translate = $this->getTransData($lang_code, $studio_id);
        if (isset($_REQUEST['device']) && ($_REQUEST['device'])) {
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                $user_id = $_REQUEST['user_id'];
                $device = $_REQUEST['device'];
				$device_details =Device::model()->findBySql('SELECT * FROM `device_management` WHERE `studio_id`=:studio_id AND `user_id`=:user_id AND `device`=:device AND flag=0 LIMIT 1',array(':studio_id'=>$studio_id,':user_id'=>$user_id,':device'=>$device));				
                if (!empty($device_details)) {
                    $id = $device_details['id'];					
					$device_info=$device_details['device_info']; 					
					Device::model()->updateByPk($id, array('flag' => 1,'deleted_date'=>date('Y-m-d H:i:s')));					
                    /*if (NotificationSetting::model()->isEmailNotificationRemoveDevice($studio_id, 'device_management')) {
						$user_data = SdkUser::model()->findByPk($user_id, array('select' => 'email'));							
						$email = $user_data['email'];                      
						$mailcontent = array('studio_id' => $studio_id,'device' => $device,'device_info'=>$device_info, 'email'=>$email);						
                        Yii::app()->email->mailRemoveDevice($mailcontent);
                    }*/
                    $res['code'] = 200;
                    $res['status'] = "Success";
                    $res['msg'] = @$translate['remove_device_request_succ'];                   
                } else {
                    $res['code'] = 450;
                    $res['status'] = "Failure";
                    $res['msg'] = @$translate['device_not_found_deleted'];
                }
            } else {
                $res['code'] = 450;
                $res['status'] = "Failure";
                $res['msg'] = @$translate['user_information_not_found'];
            }
        } else {
            $res['code'] = 450;
            $res['status'] = "Failure";
            $res['msg'] = @$translate['device_information_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
		
    }
	/**
	 * This function is used to check a device will add or not
	 */
    public function actionCheckDevice() {
        $studio_id = $this->studio_id;
			$lang_code = @$_REQUEST['lang_code'];
			$translate = $this->getTransData($lang_code, $studio_id);
			if (isset($_REQUEST['device']) && $_REQUEST['device']!='') {
                if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']>0) {
                    $user_id = @$_REQUEST['user_id'];
                    $device = @$_REQUEST['device'];
					$google_id=@$_REQUEST['google_id'];
					$device_type=isset($_REQUEST['device_type'])?@$_REQUEST['device_type']:'1';
                    if (Yii::app()->custom->restrictDevices($this->studio_id,'restrict_no_devices')) {
                        $chkDeviceLimit = Yii::app()->custom->restrictDevices($studio_id, 'limit_devices');
                        $all_devices = Yii::app()->db->createCommand()
                           ->select(' count(*) AS cnt, SUM(IF(device=:device, 1, 0)) AS exist ')
                           ->from("device_management")
                           ->where('studio_id =:studio_id AND user_id=:user_id AND flag<=1', array(':studio_id' => $studio_id,':user_id'=>$user_id, ':device' => $device))
                           ->queryrow();
                        if ($all_devices['exist'] == 0) {
							if ($all_devices['cnt'] < $chkDeviceLimit) {
								$this->actionAddDevice();
							} else {
								$data['code'] = 450;
								$data['status'] = "failure";
								$data['msg'] = @$translate['exceed_no_devices'];
							}
                        }else{
							//update device_management table for google_id
							$sql = "UPDATE device_management SET google_id = :google_id WHERE studio_id =:studio_id AND user_id=:user_id AND device=:device AND flag=0";
							$command = Yii::app()->db->createCommand($sql);
							$command->execute(array(':google_id'=>$google_id,':studio_id' => $studio_id,':user_id' => $user_id,':device'=>$device));
							$data['code'] = 200;
							$data['status'] = "Success";
							$data['msg'] = @$translate['successful'];
						}
                } else {
                    $data['code'] = 450;
                    $data['status'] = "Failure";
                    $data['msg'] = @$translate['device_restriction_not_enable'];
                }
            } else {
                $data['code'] = 450;
                $data['status'] = "Failure";
                $data['msg'] = @$translate['user_information_not_found'];
            }
        } else {
            $data['code'] = 450;
            $data['status'] = "Failure";
            $data['msg'] = @$translate['device_id_not_found'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
	/**
	 * This function is used to add new devices
	 */
    public function actionAddDevice() {
		$lang_code = @$_REQUEST['lang_code'];
		$translate = $this->getTransData($lang_code, $this->studio_id);
        if (isset($_REQUEST['device']) && ($_REQUEST['device'])) {
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']>0) {
                $devicelist = new Device();
                $devicelist->user_id = @$_REQUEST['user_id'];
                $devicelist->studio_id = $this->studio_id;
                $devicelist->device = @$_REQUEST['device'];
                $devicelist->device_info = @$_REQUEST['device_info'];				
                $devicelist->device_type = isset($_REQUEST['device_type'])?@$_REQUEST['device_type']:'1';
				$devicelist->google_id=isset($_REQUEST['google_id'])?@$_REQUEST['google_id']:'';
                $devicelist->created_by = @$_REQUEST['user_id'];
                $devicelist->created_date=date('Y-m-d H:i:s');
                $devicelist->save();
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = @$translate['device_added_success'];              
                
            } else {
                $res['code'] = 450;
                $res['status'] = "Failure";
                $res['msg'] = @$translate['user_information_not_found'];
            }
        } else {
            $res['code'] = 450;
            $res['status'] = "Failure";
            $res['msg'] = @$translate['device_id_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
    /*
     * @purpose Add content to calender
     * @return  Json String
     * @author Biswajit<biswajit@muvi.com>
    */
    public function actionBookEvent(){
        $studio_id = $this->studio_id;
        $curr_time = gmdate('Y-m-d H:i:s');
        if(isset($_REQUEST['movie_id']) && $_REQUEST['movie_id'] !=""){
            $lang_code   = @$_REQUEST['lang_code'];
            $time_offset = isset($_REQUEST['timezone_offset']) && $_REQUEST['timezone_offset'] != "" ? $_REQUEST['timezone_offset'] : 0;
            $translate   = $this->getTransData($lang_code, $studio_id);
            $movie_id    = $_REQUEST['movie_id'];
            $is_episode  = (isset($_REQUEST['is_episode']) && $_REQUEST['is_episode'] !="") ? $_REQUEST['is_episode'] : 0;
            $user_id     = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !="") ? $_REQUEST['user_id'] : 0;
            $start_time  = (isset($_REQUEST['start_time']) && $_REQUEST['start_time'] !="") ? $_REQUEST['start_time'] : 0;
            if($user_id > 0){
                if($start_time == 0){
                    if($is_episode == 0){
                        $movie_details  = Film::model()->findByPk($movie_id, array('select'=> 'start_time'));
                        $start_time     = $movie_details->start_time;
                    }elseif($is_episode == 1){
                        $ep_details     = movieStreams::model()->findByPk($movie_id, array('select' => 'movie_id'));
                        $movie_details  = Film::model()->findByPk($ep_details->movie_id, array('select'=> 'start_time'));
                        $start_time     = $movie_details->start_time;
                    }
                }
                $start_time = date("Y-m-d H:i:s", strtotime($time_offset." minutes", strtotime($start_time)));
                if(strtotime($start_time)> strtotime($curr_time)){
                    $book_event = BookedContent::model()->findByAttributes(array('studio_id'=>$studio_id, 'user_id'=> $user_id, 'content_type'=> $is_episode, 'content_id' => $movie_id ));
                    if(empty($book_event)){
                        $booked_content = new BookedContent;
                        $booked_content->user_id      = $user_id;
                        $booked_content->studio_id    = $studio_id;
                        $booked_content->content_type = $is_episode;
                        $booked_content->content_id   = $movie_id;
                        $booked_content->booked_time  = $start_time;
                        $booked_content->save();
                    }else{
                        $book_event->booked_time  = $start_time;
                        $book_event->notification_status = '0';
                        $book_event->save();
                    }
                    $res['code'] = 200;
                    $res['status'] = "Success";
                    $res['msg'] = $translate['content_saved_to_calender'];
                }else{
                    $res['code'] = 406;
                    $res['status'] = "Failure";
                    $res['msg'] = $translate['booking_time_should_greater_than_current'];
                }
            }else{
                $res['code'] = 401;
                $res['status'] = "Failure";
                $res['msg'] = $translate['need_to_sign_in'];
            }
        }else{
            $res['code'] = 406;
            $res['status'] = "Failure";
            $res['msg'] = $translate['required_data_not_found'];
        }
        echo json_encode($res);exit;
    } 
    /*
     * @purpose Check Calender to get contents added for current time 
     * @return  Json String
     * @author Biswajit<biswajit@muvi.com>
    */
    public function actionCheckCalender(){
        if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !=""){
            $date       = gmdate('Y-m-d H:i');
            $studio_id  = $this->studio_id;
            $lang_code  = @$_REQUEST['lang_code'];
            $translate  = $this->getTransData($lang_code,$studio_id);
            $user_id    = $_REQUEST['user_id'];
            $reminder   = 0;
            $reminder_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'reminder');
            if(!empty($reminder_status)){
                $reminder = $reminder_status['config_value'];
            }
            $reminder_before = 0;
            if($reminder){
                $reminder_before_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'reminder_before');
                if(!empty($reminder_before_status)){
                    $reminder_before = $reminder_before_status['config_value'];
                }
                if($reminder_before){
                    $time = new DateTime($date);
                    $time->add(new DateInterval('PT' . $reminder_before . 'M'));
                    $date = $time->format('Y-m-d H:i');
                }
                $command = Yii::app()->db->createCommand()
                    ->select('BC.id, BC.content_id, BC.content_type')
                    ->from('booked_content BC')
                    ->where('BC.studio_id='.$studio_id.' AND BC.user_id='.$user_id.' AND BC.notification_status="0" AND BC.booked_time LIKE "%'.$date.'%" ');
                $contents = $command->QueryAll();
                $films     = array();
                $episodes  = array();
                $movielist = array();
                if(!empty($contents)){
                    foreach($contents as $content){
                        if($content['content_type'] == 0){
                            $films[]    = $content['content_id'];
                        }else{
                            $episodes[] = $content['content_id'];
                        }
                        $booked_content = BookedContent::model()->findByPk($content['id']);
                        $booked_content->notification_status  = '1';
                        $booked_content->save();
                    }
                    $film_content    = array();
                    $episode_content = array();
                    if(!empty($films)){
                        $film = implode(',', $films);
                        $command = Yii::app()->db->createCommand()
                            ->select('F.id, F.name, F.story, F.permalink, M.is_episode')
                            ->from('films F, movie_streams M')
                            ->where('F.id = M.movie_id AND F.studio_id='.$studio_id.' AND F.parent_id=0 AND M.is_episode=0 AND F.id IN('.$film.')');
                        $film_content = $command->QueryAll();
                    }
                    if(!empty($episodes)){
                        $episode = implode(',', $episodes);
                        $command = Yii::app()->db->createCommand()
                            ->select('M.id, M.episode_title as name, M.episode_story as story, CONCAT(F.permalink, "/stream/", M.embed_id) as permalink, M.is_episode')
                            ->from('movie_streams M, films F')
                            ->where('F.id = M.movie_id AND F.studio_id='.$studio_id.' AND M.episode_parent_id=0 AND F.parent_id=0 AND M.is_episode=1 AND M.id IN('.$episode.')');
                        $episode_content = $command->QueryAll();
                    }
                    $movielist = array_merge($film_content,$episode_content);
                    $msgs = $translate['playing_now'];
                    if($reminder_before > 0){
                        $time = $reminder_before;
                        $msgs = $translate['start_in_minutes'];
                        $msgs = htmlentities($msgs);
                        eval("\$msgs = \"$msgs\";");
                        $msgs = htmlspecialchars_decode($msgs);
                    }
                    
                    $res['code'] = 200;
                    $res['status'] = "Success"; 
                    $res['movielist'] = $movielist;
                    $res['reminder_message'] = $msgs;
                }else{
                    $res['code'] = 200;
                    $res['status'] = "Success";
                    $res['msg'] = "No Content Found"; 
                    $res['movielist'] = $movielist;
                }
            }else{
                $res['code'] = 406;
                $res['status'] = "Failure";
                $res['msg'] = "Reminder Feature is not available"; 
            }
        }else{
            $res['code'] = 401;
            $res['status'] = "Failure";
            $res['msg'] = "Need to sign in"; 
        }
        echo json_encode($res);exit;
    }
	/*
     * @purpose Generate Widevine Licence Url
	 * @param string $conentKey , string $encryptionKey
     * @return  string 
     * @author Prakash<support@muvi.com>
    */
	private function generateExplayWideVineToken($conentKey = '', $encryptionKey = '') {
        if ($conentKey && $encryptionKey) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, WV_URL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=" . DRM_LICENSE_KEY . "&errorFormat=json&kid=" . $encryptionKey . "&contentKey=" . $conentKey . "&securityLevel=1&hdcpOutputControl=0&expirationTime=%2B36000");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = curl_exec($ch);            
            curl_close($ch);
            return ($server_output)?$server_output:false;
        } else {
            return false;
        }
    }
   /**
    * @method loadFeaturedContent() It will load the featured content a store 
    * @param int $limit Limit the number of section
    * @param int $offset Offset
    * @param String $themes Description
    * @author Gayadhar<support@muvi.com>
    * @return Json Json data array. 
     */
        
    public function actionloadFeaturedSections() {
        $offset = isset($_REQUEST['dataoffset']) ? $_REQUEST['dataoffset'] : 0;
        $limit = isset($_REQUEST['viewlimit']) ? $_REQUEST['viewlimit'] : 10;
        $studio_id = $this->studio_id;
        $themes = $_REQUEST['themes'] ? $_REQUEST['themes'] : '';
        $controller = Yii::app()->controller;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $studio_id);
        $user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0 ? $_REQUEST['user_id'] : 0;
        // query criteria
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'contents' => array(// this is for fetching data
                'together' => false,
                'condition' => 'contents.studio_id = ' . $studio_id,
                'order' => 'contents.id_seq ASC'
            ),
        );
        $cond = 't.is_active = 1 AND t.studio_id = ' . $studio_id . ' AND language_id=' . $language_id;
        $criteria->condition = $cond;
        $criteria->order = 't.id_seq ASC, t.id ASC';
        $criteria->limit = $limit;
        $criteria->offset = $offset;
        $sections = FeaturedSection::model()->findAll($criteria);
        $return = array();
        if ($sections) {
            $addQuery = '';
            $customFormObj = new CustomForms();
            $customData = $customFormObj->getFormCustomMetadat($studio_id);
            if ($customData) {
                foreach ($customData AS $ckey => $cvalue) {
                    $addQuery = ",F." . $cvalue['field_name'];
                    //$customArr[$cvalue['f_id']] = $cvalue['f_display_name']; 
                    $customArr[] = array(
                        'f_id' => trim($cvalue['f_id']),
                        'field_name' => trim($cvalue['field_name']),
                        'field_display_name' => trim($cvalue['f_display_name'])
                    );
                }
            }
            $visitor_loc = Yii::app()->common->getVisitorLocation();
            $country = $visitor_loc['country'];
            $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $studio_id . " AND sc.country_code='{$country}'";

            foreach ($sections as $section) {
                if(count($section->contents)){
                    $streamIds = '';
                    $movieIds = '';
                  $idSeq = '';
                    $idSeqplay = '';
                    $final_content = array();
                    $final_physical = array();
                    $Seq = array();
                    $total_contents = 0;
                    $is_playlist = 0;
                    if ($section->content_type == 1) {
                        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
                        $final_content = Yii::app()->custom->SectionProducts($section->id, $studio_id, $default_currency_id);
                    } else {
                        foreach ($section->contents as $featured) {
                            if ($featured->is_episode == 1) {
                                $streamIds .="," . $featured->movie_id;
                                $idSeq .= "," . $featured->id_seq;
                            } elseif ($featured->is_episode == 4) {
                                $playlistId .= "," . $featured->movie_id;
                                $is_playlist = 1;
                                $is_episode = $featured->is_episode;
                                $idSeqplay .= "," . $featured->id_seq;
                            }else {
                                $movieIds .= "," . $featured->movie_id;
                                $idSeq .= "," . $featured->id_seq;
                            }
                        }
                        $cond = '';
                        if (@$movieIds || @$streamIds) {
                            $cond = " AND (";
                            if (@$movieIds) {
                                $movieIds = ltrim($movieIds, ',');
                                $cond .="(F.id IN(" . $movieIds . ") AND is_episode=0) OR ";
                                $orderBy = " ORDER BY FIELD(P.movie_id," . $movieIds . ")";
                            }
                            if ($streamIds) {
                                $streamIds = ltrim($streamIds, ',');
                                $cond .=" (M.id IN(" . $streamIds . "))";
                                $orderBy = " ORDER BY FIELD(P.movie_stream_id," . $streamIds . ")";
                            } else {
                                $cond = str_replace(' OR ', ' ', $cond);
                            }
                            $cond .= ")";
                        }

                        $sql = "SELECT  PM.*, l.id as livestream_id,l.feed_type,l.feed_url,l.feed_method FROM "
                            . "( SELECT M.movie_id,M.is_converted,M.video_duration,M.is_episode,M.embed_id,M.is_downloadable,M.id AS movie_stream_id,F.permalink,F.content_category_value,F.name,F.mapped_id,F.censor_rating,M.mapped_stream_id,F.uniq_id,F.content_type_id,F.ppv_plan_id,M.full_movie,F.story,F.genre,F.release_date, F.content_types_id,M.episode_title,M.episode_date,M.episode_story,M.episode_number,M.series_number, M.last_updated_date " . $addQuery
                                . "FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=" . $studio_id . " " . $cond . " ) AS PM "
                                . " LEFT JOIN livestream l ON PM.movie_id = l.movie_id";
                        $sql_data = "SELECT P.* FROM (SELECT t.*,g.* FROM (" . $sql . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P ";
                        $where = " WHERE P.geocategory_id IS NULL OR P.country_code='{$country}'";
                        $sql_data .= $where . " " . $orderBy;
                        $list = Yii::app()->db->createCommand($sql_data)->queryAll();
                         if ($is_playlist == 1) {
                            $Seqid = ltrim($idSeq, ',');
                            $Seq = explode(',', $Seqid);
                        }
                        $fobject = new Film();
                        $final_content = $fobject->getContentListData($list, $margs, $customArr, $this->studio_id, $language_id, $themes, $translate, $Seq);
                        if ($is_playlist == 1) {
                            $movieIds = ltrim($playlistId, ',');
                            $idSeqplay = ltrim($idSeqplay, ',');
                            $seqId = explode(',', $idSeqplay);
                            $command1 = Yii::app()->db->createCommand()
                                    ->select('id,playlist_name,playlist_permalink')
                                    ->from('user_playlist_name u')
                                    ->where('u.studio_id=' . $studio_id . ' AND u.user_id = 0 And u.id IN(' . $movieIds . ') ORDER BY FIELD(u.id,' . $movieIds . ')');
                            $data = $command1->QueryAll();
                            $tot = count($final_content);
                            $k = 0;
                            if ($data) {
                                foreach ($data as $res) {
                                    $playlist_id = $res['id'];
                                    $playlist_name = $res['playlist_name'];
                                    $permalink = $res['playlist_permalink'];
                                    $poster = $controller->getPoster($playlist_id, 'playlist_poster');
                                    $final_content[$seqId[$k]] = array(
                                        'movie_id' => $playlist_id,
                                        'title' => utf8_encode($playlist_name),
                                        'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                                        'poster' => $poster,
                                        'is_episode' => $is_episode,
                                        'seq_feat' => $seqId[$k]
                                    );
                                    $k++;
                                    $tot ++;
                                }
                            }
                        }
                        ksort($final_content);
                        $final_content = array_values(array_filter($final_content));
                    }
                        $return[] = array(
                            'id' => $section->id,
                            'title' => utf8_encode($section->title),
                            'content_type' => $section['content_type'],
                            'total' => count($final_content),
                            'contents' => $final_content
                        );
                    
                }
            }
        }
        $res['code'] = 200;
        $res['status'] = "OK";
        $res['section'] = $return;
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /*
     * @purpose for filter custom manage metadata 
     * @author Sweta<sweta@muvi.com>
    */
    public function actionGetCustomFilter(){
        $lists = array();
        $studio_id = $this->studio_id;
        $command = Yii::app()->db->createCommand()
            ->select('f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value as options')
            ->from('custom_metadata_field f')
            ->where('f.f_type IN(2,3) AND f.studio_id='.$studio_id.' ORDER BY id ASC');
        $filterdata = $command->queryAll();
        if(!empty($filterdata)){
           foreach($filterdata as $listdata){
                 $options = json_decode($listdata['options'],TRUE);
               if((isset($options[$lang_code])) && is_array($options[$lang_code]))
               {
                $options= $options[$lang_code]; 
               }
                else if((isset($options['en'])) && is_array($options['en']))
               {
                $options= $options['en'];    
               }
               else {
                 $options=  $options ; 
               }
               /*  if (!is_array($options) && $listdata['options']) {
                    if (!in_array(trim($listdata['options']), array('null', 'NULL', '[', '""'))) {
                        $options = explode(",", $listdata['options']);
                    }
                }*/
              $listdata['options'] =  $options;     
              $lists[] = $listdata; 
           }
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['filter_list'] = $lists;
        }else{
            $data['code'] = 452;
            $data['status'] = 'Failure';
            $data['msg'] = 'No data found';
        }
        echo json_encode($data);
        exit;
    }
	
	/**
	* @method SetSubscriptionForOtherDevices Set the user subscription of User when purchase subscription from other device like Roku, Apple etc
	* @return json Returns the response in json format
	* @param string $email Email of the user
	* @param string $plan_id Unique Plan id of the Subscription Plan
	* @param int $device_type Device type For Roku
	* @param int $out_source_user_id Roku user id
	* @author Ashis<ashis@muvi.com>
	*/
	public function actionSetSubscriptionForOtherDevices(){
		$email = @$_REQUEST['email'];
		$plan_id = @$_REQUEST['plan_id'];
		$device_type = @$_REQUEST['device_type'];
		$out_source_user_id = @$_REQUEST['out_source_user_id'];
		
		if (!empty($email) && !empty($plan_id) && !empty($device_type)) {
			$user = SdkUser::model()->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id, 'status'=>1));
			if (isset($user) && !empty($user)) {
				$planData = SubscriptionPlans::model()->findByAttributes(array('unique_id'=>$plan_id, 'studio_id'=>$this->studio_id, 'status'=>1));
				if (isset($planData) && !empty($planData)) {
					$usModel = new UserSubscription;
					$subscriptionData = $usModel->findByAttributes(array('studio_id'=>$this->studio_id, 'plan_id'=>$planData->id, 'user_id'=>$user->id, 'device_type'=>$device_type,'status'=>1));
					if (isset($subscriptionData) && !empty($subscriptionData)) {
						$res['code'] = 652;
						$res['message'] = 'User has already taken subscription.';
						$res['status'] = 'FAILURE';
					}else{
						$usModel->studio_id = $this->studio_id;
						$usModel->user_id = $user->id;
						$usModel->plan_id = $planData->id;
						$usModel->device_type = @$device_type;
						$usModel->status = 1;
						$usModel->created_by = $user->id;
						$usModel->created_date = new CDbExpression("NOW()");
						$usModel->save();

						if (trim($out_source_user_id)) {
							$users = SdkUser::model()->findByPk($user->id);
							$users->out_source_user_id = $out_source_user_id;
							$users->save();
						}
						$res['code'] = 200;
						$res['message'] = 'SUCCESS';
						$res['status'] = 'OK';
					}
				} else {
					$res['code'] = 653;
					$res['message'] = 'No subscription plan found.';
					$res['status'] = 'FAILURE';
				}
			}else{
				$res['code'] = 654;
				$res['message'] = 'User does not exist.';
				$res['status'] = 'FAILURE';
			}
		} else {
			$res['code'] = 655;
			$res['message'] = 'Either email or plan_id or device_type is missing.';
			$res['status'] = 'FAILURE';
		}
		echo json_encode($res);exit;

	}
     /*
     * @purpose add the content to playlist
	 * *allPlaylist (get all the playlist)
     * @return  string 
     * @author Biswajitdas<biswajitdas@muvi.com>
     */

    public function actionallPlaylist() {
        $studio_id = $this->studio_id;
        if ((isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") ? $_REQUEST['user_id'] : 0;
			$lang_code = @$_REQUEST['lang_code'];
			$language_id = Yii::app()->custom->getLanguage_id($lang_code);
			$list = Yii::app()->common->getAllplaylist($studio_id, $user_id,0,0,0,$language_id);
            $res['code'] = 405;
            $res['status'] = "Success";
            $res['msg'] = $list;
            echo json_encode($res);
            exit;
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = 'Need To signin';
            echo json_encode($res);
            exit;
        }
    }
    public function actionCreateNewPlayList(){
        $studio_id = $this->studio_id;
        $lang_code = $_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if((isset($_REQUEST['playlistname']) && ($_REQUEST['playlistname'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))):
            $playlistnme = $_REQUEST['playlistname'];
            $user_id = $_REQUEST['user_id'];
            $playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'playlist_name' => $playlistnme));
            if(empty($playlist)){
                $playlistNew = new UserPlaylistName;
                $playlistNew->user_id = $user_id;
                $playlistNew->studio_id = $studio_id;
                $playlistNew->playlist_name = $playlistnme;
                $playlistNew->save();
                $playlist_id = $playlistNew->id;
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = $translate['playlist_created'];
                $res['playlist_id'] = $playlist_id;
            }else{
                $res['code'] = 405;
                $res['status'] = "Failure";
                $res['msg'] = $translate['playlist_already_exist'];
            }
        else:
            $res['code'] = 407;
            $res['status'] = "Failure";
            $res['msg'] = $translate['invalid_data'];
        endif;  
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
    public function actionaddToPlaylist() {
        $studio_id = $this->studio_id;
        if ((isset($_REQUEST['playlistname']) && ($_REQUEST['playlistname'] != '')) || (isset($_REQUEST['playlist_id']) && ($_REQUEST['playlist_id'] > 0)) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $playlistnme = @$_REQUEST['playlistname'];
            $playlist_id = (isset($_REQUEST['playlist_id']) && $_REQUEST['playlist_id'] > 0) ? $_REQUEST['playlist_id'] : 0;
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") ? $_REQUEST['user_id'] : 0;
            $movie_id = $_REQUEST['content_id'];
            $is_episode = $_REQUEST['is_episode'];
            $lang_code = $_REQUEST['lang_code'];
            $is_content = $_REQUEST['is_content'];
            $que_id = (isset($_REQUEST['que_id']) && $_REQUEST['que_id'] != "") ? $_REQUEST['que_id'] : '';
            $translate = $this->getTransData($lang_code, $studio_id);
            if ($playlist_id  == 0) {
                $playlistNew = new UserPlaylistName;
                $playlistNew->user_id = $user_id;
                $playlistNew->studio_id = $studio_id;
                $playlistNew->playlist_name = $playlistnme;
                $playlistNew->save();
                $playlist_id = $playlistNew->id;
            }
            $playlist_name = Yii::app()->common->getAllPlaylistName($studio_id, $user_id, 0);
            $total_playlist = count($playlist_name);
            if ($movie_id != '' && $is_content == 1) {
                $result = self::AddContentPlaylist($user_id, $movie_id, $is_episode, $playlist_id, $lang_code, $que_id, $total_playlist);
            } elseif ($que_id != '') {
                $result = self::AddQueuePlaylist($user_id, $playlist_id, $lang_code, $que_id, $total_playlist);
            } else {
                $res['code'] = 400;
                $res['status'] = "Success";
                $res['msg'] = $translate['playlist_created'];
                $res['total'] = $total_playlist;
                $result = $res;
            }
            echo json_encode($result);
            exit;
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = 'Need To signin';
            echo json_encode($res);
            exit;
        }
    }

    public function actionDeletePlaylist() {
        $studio_id = $this->studio_id;
        $lang_code = $_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['playlist_id']) && ($_REQUEST['playlist_id'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $playlist_id = $_REQUEST['playlist_id'];
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") ? $_REQUEST['user_id'] : 0;
            $playlist = UserPlaylistName::model()->findByPk($playlist_id);
            if (empty($playlist)) {
                $res['code'] = 400;
                $res['status'] = "Failure";
                $res['msg'] = 'Invalid playlist';
                echo json_encode($res);
                exit;
            }elseif($playlist->user_id != $user_id) {
                $res['code'] = 400;
                $res['status'] = "Failure";
                $res['msg'] = 'Invalid User';
                echo json_encode($res);
                exit;
            }else{
            $param = array(':playlist_id' => $playlist_id, ':user_id' => $user_id, ':studio_id' => $studio_id);
            $playItem = UserPlaylist::model()->deleteAll('playlist_id = :playlist_id AND user_id = :user_id AND studio_id = :studio_id', $param);
            $playlist = UserPlaylistName::model()->deleteAll('user_id = :user_id AND id = :playlist_id AND studio_id = :studio_id', $param);
            $playlist_name = Yii::app()->common->getAllPlaylistName($studio_id, $user_id, 0);
            $total_playlist = count($playlist_name);
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['playlist_deleted'];
            $res['total'] = $total_playlist;
            echo json_encode($res);
            exit;
            }
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = 'Need To signin';
            echo json_encode($res);
            exit;
        }
    }
    public function actionDeleteContent() {
        $lang_code = $_REQUEST['lang_code'];
        $studio_id = $this->studio_id;
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['playlist_id']) && ($_REQUEST['playlist_id'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $content_id = $_REQUEST['content_id'];
            $playlist_id = $_REQUEST['playlist_id'];
            $user_id = $_REQUEST['user_id'];
            $playlist = UserPlaylist::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'content_id' => $content_id, 'playlist_id' => $playlist_id));
            if (!empty($playlist)) {
                $params = array(':playlist_id' => $playlist_id, ':user_id' => $user_id, ':studio_id' => $studio_id, ':content_id' => $content_id);
                $userList = UserPlaylist::model()->deleteAll('content_id = :content_id AND playlist_id = :playlist_id AND studio_id = :studio_id AND user_id = :user_id', $params);
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = $translate['content_remove_playlist'];
            } else {
                $res['code'] = 405;
                $res['status'] = "Failure";
                $res['msg'] = 'No content found';
            }
        } else {
            $res['code'] = 407;
            $res['status'] = "Failure";
            $res['msg'] = 'Invalid data';
        }
        echo json_encode($res);
        exit;
    }
	
	public function actionGetAudioPlaylist(){
		$studio_id = $this->studio_id;
		if((isset($_REQUEST['playlist_id'])&&($_REQUEST['playlist_id'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))){
			$playlist_id = $_REQUEST['playlist_id'];
			$user_id     = @$_REQUEST['user_id'];
			$lang_code   = @$_REQUEST['lang_code']; 
            $translate   = $this->getTransData($lang_code, $studio_id);
			$language_id = Yii::app()->custom->getLanguage_id($lang_code);
			$command = Yii::app()->db->createCommand()
					->select('content_id,content_type')
					->from('user_playlist p')
					->where('p.user_id = ' . $user_id . ' AND p.studio_id=' . $studio_id . ' AND p.playlist_id ='.$playlist_id);
			$playItem = $command->queryAll();
			$playlist = array();
			$k = 0;
			$base_cloud_url = Yii::app()->common->getAudioGalleryCloudFrontPath($studio_id);
			foreach ($playItem as $item){
				$movie_id = $item['content_id'];
				$is_episode = $item['content_type'];
				
				$command1 = Yii::app()->db->createCommand()
					->select('f.name,f.permalink,ms.episode_title,ms.movie_id,ms.full_movie,ms.is_episode,ms.embed_id,f.uniq_id')
					->from('films f,movie_streams ms')
					->where('f.id=ms.movie_id AND ms.id=' . $movie_id . ' AND ms.is_episode = '.$is_episode.' AND ms.is_converted = 1');
				$list = $command1->QueryRow();
				if(!empty($list)){
					$audio_id = $list['movie_id'];
					if($is_episode == '1'){
						$title   = $list['episode_title'];
						$audioid = $movie_id;
						$type = 'moviestream';
						$uniq_id = $list['embed_id'];
					}else{
						$title = $list['name'];
						$audioid = $audio_id;
						$type = 'films';
						$uniq_id = $list['uniq_id'];
					}
					$is_favourite = Yii::app()->common->isContentFav($studio_id, $user_id, $audioid, $is_episode);
					$playlist[$k]['content_id'] = $movie_id;
					$playlist[$k]['movie_id'] = $audio_id;
					$playlist[$k]['is_episode'] = $is_episode;
					$playlist[$k]['url'] = $base_cloud_url . $list['full_movie'];
					$playlist[$k]['audio_poster'] = $this->getPoster($audioid, $type);
					$playlist[$k]['title'] = $title;
					$playlist[$k]['permalink'] = $list['permalink'];
					$playlist[$k]['is_favourite'] = $is_favourite;
					$playlist[$k]['uniq_id'] = $uniq_id;
					$cast_details = $this->getCasts($audio_id, '', $language_id, $studio_id);
					$celeb_name = array();
					if (!empty($cast_details)) {
						foreach ($cast_details as $casts) {
							if (trim($casts['celeb_name']) != "") {
								$celeb_name[] = $casts['celeb_name'];
							}
						}
					}
					$casts = implode(',', $celeb_name);
					$playlist[$k]['cast'] = $casts;
					$k++;
				}
			}
			if ($playlist) {
				$res['code'] = 200;
                $res['message'] = 'success';
                $res['data'] = $playlist;
            } else {
				$res['code'] = 405;
                $res['message'] = 'error';
                $res['data'] = $playlist;
            }
		}else{
				$res['code'] = 400;
                $res['message'] = $translate['invalid_playlist'];
                $res['status'] = 'failure';
		}
		echo json_encode($res);
        exit;
	}
	
	public function actionPlayListNameEdit() {
        $studio_id = $this->studio_id;
        $lang_code = $_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['playlist_name']) && ($_REQUEST['playlist_name'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $playlist_id = $_REQUEST['playlist_id'];
            $playlist_name = $_REQUEST['playlist_name'];
            $user_id = $_REQUEST['user_id'];
            $playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'id' => $playlist_id));
            if (!empty($playlist)) {
                $playlistUp = $playlist->findByPK($playlist->id);
                $playlistUp->playlist_name = $playlist_name;
                $playlistUp->save();
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = $translate['playlist_updated'];
            } else {
                $res['code'] = 405;
                $res['status'] = "Failure";
                $res['msg'] = $translate['no_playlist_found'];
            }
        } else {
            $res['code'] = 407;
            $res['status'] = "Failure";
            $res['msg'] = $translate['playlist_name_not_blank'];
        }
        echo json_encode($res);
        exit;
    }
	function AddContentPlaylist($user_id, $movie_id, $is_episode, $playlist_id, $lang_code, $que_id,$total_playlist) {
		$studio_id = $this->studio_id;
		$translate  = $this->getTransData($lang_code, $studio_id);
		$playlistitem =UserPlaylist::model()->findByAttributes(array('studio_id'=>$studio_id,'user_id'=>$user_id,'playlist_id'=>$playlist_id,'content_id'=>$movie_id,'content_type'=>$is_episode));
			if(empty($playlistitem)){
				$playlistItem = new UserPlaylist;
				$playlistItem->user_id = $user_id;
				$playlistItem->studio_id = $studio_id;
				$playlistItem->playlist_id = $playlist_id;
				$playlistItem->content_id = $movie_id;
				$playlistItem->content_type = $is_episode;
				$playlistItem->save();
				$msg =$translate['content_added_to_playlist'];
				$res['code'] = 200;
				$res['status'] = "Success";
				$res['msg'] = $translate['content_added_to_playlist']; 
            $res['total'] =$total_playlist;
        } else {
				$res['code'] = 400;
				$res['status'] = "Success";
				$res['msg'] = $translate['content_added_to_playlist']; 
			}
		return $res;
	}
    
    function AddQueuePlaylist($user_id, $playlist_id, $lang_code, $que_id,$total_playlist) {
		$studio_id = $this->studio_id;
		$translate  = $this->getTransData($lang_code, $studio_id);
		$user_id_que = 0;
		$queue = QueueList::model()->findByAttributes(array('studio_id'=>$studio_id,'user_id'=>$user_id_que,'id'=>$que_id));
		$old_data = unserialize($queue->queuelist_data);
		$playlistitem = Yii::app()->db->createCommand()
				->select('content_id')
				->from('user_playlist u')
				->where('u.studio_id=' . $studio_id . ' AND u.playlist_id = ' . $playlist_id . ' AND u.user_id = '.$user_id.'')
				->queryAll();
		if(count($old_data) > 0){
			foreach ($old_data as $data){
				$movie_id = $data['content_id'];
				$is_episode = $data['is_episode'];
				$match = array_search($movie_id, array_column($playlistitem, 'content_id'));
				if (empty($match) && $match  !== 0) {
					$playlistItem = new UserPlaylist;
					$playlistItem->user_id = $user_id;
					$playlistItem->studio_id = $studio_id;
					$playlistItem->playlist_id = $playlist_id;
					$playlistItem->content_id = $movie_id;
					$playlistItem->content_type = $is_episode;
					$playlistItem->save();
					$msg = $translate['content_added_to_playlist']; 
				}else{
					$msg = $translate['content_added_to_playlist'];
				}
			}
			
			$res['code'] = 200;
			$res['status'] = "Success";
			$res['msg'] = $msg;
            $res['total'] =$total_playlist;

        } else {
			$res['code'] = 400;
			$res['status'] = "Error";
			$res['msg'] = $translate['invalid_data'];
		}
		return $res;
	}
	public function actionUpdateQueueList(){
		$studio_id  = $this->studio_id;
		$user_id    = isset($_REQUEST['user_id'])?$_REQUEST['user_id']:0;
		$new_data   = $_REQUEST['queue_data'];
		$lang_code  = @$_REQUEST['lang_code'];
		$translate  = $this->getTransData($lang_code, $studio_id);
		$is_del = 0;
		$is_init = 1;
		$tot = '';
		$is_exist = 0;
 		if((isset($_REQUEST['queue_id'])) && ($_REQUEST['queue_id'] > 0)){
			$queue = QueueList::model()->findByAttributes(array('studio_id'=>$studio_id,'user_id'=>$user_id,'id'=>$_REQUEST['queue_id']));
			if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'add'){
                $old_data = unserialize($queue->queuelist_data);
                if (count($new_data) > 0) {
                    $old_data = array();
                    $old_data = $new_data;
                }
                $queue->queuelist_data = serialize($old_data);
                $queue->save();
                $queuelist_id = $queue->id;
             }elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete'){
				$data = unserialize($queue->queuelist_data);
				$content_id = $_REQUEST['content_id'];
				$match_key = array_search($content_id, array_column($data, 'content_id'));
				$total = count($data);
				unset($data[$match_key]);
				$old_data = array_values($data);
				$tot = $total-1;
				$queue->queuelist_data = serialize($old_data);
				$queue->save();
				$queuelist_id = $queue->id;
				$is_del = 1;
			}elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'add_new'){
				$old_data = unserialize($queue->queuelist_data);
				if (count($new_data) > 0) {
					$total = count($old_data);
					$k = 0;
					$match_new = array_search($new_data[$k]['content_id'], array_column($old_data, 'content_id'));
					if ($old_data) {
					if (empty($match_new) && $match_new  !== 0) {
						$tot = $total + 1;
						$old_data[$total] = $new_data[$k];
						$msg = $translate['added_to_queue'];
					}else{
						$tot = $total;
						$msg = $translate['added_to_queue'];
						$new_data = '';
						$is_exist = 1;
					}
						$queue->queuelist_data = serialize($old_data);
				}
				$queue->save();
				$queuelist_id = $queue->id;
				}
			}elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'clear_que'){
				$old_data = unserialize($queue->queuelist_data);
				if (count($old_data) > 0) {
					$queue->delete();
					$msg = 'delete';
					$old_data = '';
					$is_del = 1;
				}
			}
		}else{
			if ($_REQUEST['action'] == 'add_new'){
				$msg = $translate['added_to_queue'];
				$is_init = 0;
			}
			$old_data = $new_data;
			$queue = new QueueList;
			$queue->studio_id = $studio_id;
			$queue->queuelist_data = serialize($new_data);
			$queue->user_id = $user_id;
			$queue->save();
			$queuelist_id = $queue->id;
		}
		$res['code']	 = 200;
		$res['queue_id'] = $queuelist_id;
		$res['data']     = $old_data;
		$res['index']	 = $tot;
		$res['msg']      = $msg;
		$res['is_del']	 = $is_del;
		$res['is_init']  = $is_init;
		$res['is_exist'] = $is_exist;
 		echo json_encode($res);exit;
	}
	public function actionAddToQue(){
		$studio_id  = $this->studio_id;
		$movie_id   = $_REQUEST['content_id'];
		$is_episode = $_REQUEST['is_episode'];
		$lang_code  = @$_REQUEST['lang_code']; 
        $translate  = $this->getTransData($lang_code, $studio_id);
		$language_id = Yii::app()->custom->getLanguage_id($lang_code);
		$command1 = Yii::app()->db->createCommand()
				->select('f.name,f.permalink,ms.episode_title,ms.movie_id,ms.full_movie,ms.is_episode,ms.embed_id,f.uniq_id')
				->from('films f,movie_streams ms')
				->where('f.id=ms.movie_id AND ms.id=' . $movie_id . ' AND ms.is_episode = ' . $is_episode . '');
		$list = $command1->QueryRow();
		$playlist = array();
		$base_cloud_url = Yii::app()->common->getAudioGalleryCloudFrontPath($studio_id);
		if(!empty($list)){
			$k = 0; 
			$audio_id = $list['movie_id'];
			if($is_episode == '1'){
				$title   = $list['episode_title'];
				$audioid = $movie_id;
				$type = 'moviestream';
				$uniq_id = $list['embed_id'];
			}else{
				$title = $list['name'];
				$audioid = $audio_id;
				$type = 'films';
				$uniq_id = $list['uniq_id'];
			}
			$is_favourite = Yii::app()->common->isContentFav($studio_id, $user_id, $audioid, $is_episode);
			$playlist[$k]['content_id'] = $movie_id;
			$playlist[$k]['movie_id'] = $audio_id;
			$playlist[$k]['is_episode'] = $is_episode;
			$playlist[$k]['url'] = $base_cloud_url . $list['full_movie'];
			$playlist[$k]['audio_poster'] = $this->getPoster($audioid, $type);
			$playlist[$k]['title'] = $title;
			$playlist[$k]['permalink'] = $list['permalink'];
			$playlist[$k]['is_favourite'] = $is_favourite;
			$playlist[$k]['uniq_id'] = $uniq_id;
			$cast_details = $this->getCasts($audio_id, '', $language_id, $studio_id);
			$celeb_name = array();
			if (!empty($cast_details)) {
				foreach ($cast_details as $casts) {
					if (trim($casts['celeb_name']) != "") {
						$celeb_name[] = $casts['celeb_name'];
					}
				}
			}
			$casts = implode(',', $celeb_name);
			$playlist[$k]['cast'] = $casts;
			if ($playlist) {
				$res['code'] = 200;
				$res['message'] = 'success';
				$res['data'] = $playlist;
			} else {
				$res['code'] = 405;
				$res['message'] = 'error';
				$res['data'] = $playlist;
			}
		}
		else{
			$res['code'] = 400;
			$res['message'] = $translate['invalid_data'];
			$res['status'] = 'failure';
		}
		echo json_encode($res);
        exit;
	}
	/*
     * @purpose for cast listing page 
     * @author Sweta<sweta@muvi.com>
    */
    public function actionGetCastList(){
            $studio_id = $this->studio_id;
            $command = Yii::app()->db->createCommand()
                ->select('c.id,c.name,c.summary,c.permalink')
                ->from('celebrities c')
                ->where("c.parent_id='0' AND c.studio_id='$studio_id' ORDER BY id ASC");
            $celeblist = $command->queryAll();
            foreach($celeblist as $key => $celeb){
                $celeb_image = $this->getPoster($celeb['id'], 'celebrity'); 
                $celeblist[$key]['content_id'] = $celeb['id'];
                $celeblist[$key]['celeb_image'] = $celeb_image;
            }
            if ($celeblist) {
                    $data['code'] = 200;
                    $data['status'] = "Success";
                    $data['cast_list'] = $celeblist;
                }
                else {
                    $data['code'] = 452;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'No data found';
                }
            echo json_encode($data);
            exit;
    }
    /*
     * @purpose for reminder listing page 
     * @author Sweta<sweta@muvi.com>
    */
    public function actionViewBookedContent(){
        $user_id     = @$_REQUEST['user_id'];
        if($user_id > 0){
            $lang_code   = @$_REQUEST['lang_code'];
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $reminder_status = StudioConfig::model()->getconfigvalueForStudio($this->studio_id, 'reminder');
            if(!empty($reminder_status) && $reminder_status['config_value'] == 1){
                $booklist = Yii::app()->db->createCommand()->select('c.id,c.content_id,c.content_type')->from('booked_content c')->where("c.studio_id=".$this->studio_id." ORDER BY id ASC")->queryAll();
                $final_list = array();
                if(!empty($booklist)){
                    foreach($booklist as  $list){
                        $final_list[] = Yii::app()->general->getContentData($list['content_id'], $list['content_type'], array(), $language_id, $this->studio_id, $user_id, '', 1);
                    }
                }
                $data['code'] = 200;
                $data['status'] = "Success";
                $data['book_list'] = $final_list;
                $data['count'] = count($final_list);
            }else{
                $data['code'] = 412;
                $data['status'] = "Failure";
                $data['msg'] = "Reminder is not enable";
            }
        }else{
            $data['code'] = 411;
            $data['status'] = "Failure";
            $data['msg'] = "User Id not found";
        }
        echo json_encode($data);
        exit;
    }
	public function actionGetAPIServer() {
        if (isset($_REQUEST['ip']) && $_REQUEST['ip']!="") {
            $ip = $_REQUEST['ip'];
        } else {
            $ip = CHttpRequest::getUserHostAddress();
        }
        $geo_data = new EGeoIP();
        $geo_data->locate($ip);
        $latitude = $geo_data->getLatitude();
        $longitude = $geo_data->getLongitude();
        $ref = array($latitude, $longitude);
        $command = Yii::app()->db->createCommand()->select('*')->from('api_server');
        $locations = $command->queryAll();
        foreach ($locations AS $key => $val) {
            $apiserver[$val['id_server']][] = $val['url'];
            $apiserver[$val['id_server']][] = $val['latitude'];
            $apiserver[$val['id_server']][] = $val['longitude'];
            $server[$val['id_server']] = $val;
        }
        if ($latitude && $longitude) {
            $distances = array_map(function($apiserver) use($ref) {
                $a = array_slice($apiserver, -2);
                return Yii::app()->common->getDistance($a, $ref);
            }, $apiserver);
            asort($distances);
            $bucketids = array_keys($distances);
            if ($bucketids[0] > 0)
                $s3bucket_id = $bucketids[0];
        } else {
            $s3bucket_id = 5;
        }
        $res['code'] = 200;
        $res['status'] = "OK";
        $res['url'] = $server[$s3bucket_id]['url'];
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
	/** author:suraja@muvi.com
     *  use:for updating the chat stream details
     *
     */
    public function actionUpdateUserChatDeatils() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['chatbanstatus']=2; //when no data avaialable
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['userid']) && is_numeric($_REQUEST['userid']) && $_REQUEST['userid']!='' ) {
            //$getuser = SdkUser::model()->findByAttributes(array('id' => $_REQUEST['userid'], 'studio_id' => $this->studio_id));
             $command = Yii::app()->db->createCommand()
                ->select('id')
                ->from('sdk_users')
                ->where('id=:userid and studio_id=:studio_id', array(':studio_id' => $this->studio_id,':userid' => $_REQUEST['userid']));
                $getuser = $command->queryRow();
            if (isset($getuser) && count($getuser) > 0) {
              
                if (isset($_REQUEST['movieid']) && is_numeric($_REQUEST['movieid']) && $_REQUEST['movieid']!='') {
                     $movie = Film::model()->with(array("live_stream"=>array("id")))->find("t.id=".$_REQUEST['movieid']);
                    // echo $command1->live_stream[0]->id;;exit;
                    if(isset($movie) && count($movie) > 0){
                        $stream = Yii::app()->db->createCommand()
                                ->select('id,is_banned')
                                ->from('chat_stream')
                                ->where('sdkuser_id=:userid and movie_id=:movie_id and stream_id=:stream_id', array(':movie_id' => $_REQUEST['movieid'],':userid' => $_REQUEST['userid'],':stream_id'=>$movie->live_stream[0]->id));
                       
                        $getstream = $stream->queryRow();
                        if ($getstream['id']) {
                            $chatstream = ChatStream::model()->findByPk($getstream['id']);
                            $chatstream->studio_id = $this->studio_id;
                            $chatstream->ip_address = CHttpRequest::getUserHostAddress();                           
                        } else {
                            $chatstream = new ChatStream();
                            $chatstream->sdkuser_id = $_REQUEST['userid'];
                            $chatstream->studio_id = $this->studio_id;
                            $chatstream->movie_id = $_REQUEST['movieid'];
                            $chatstream->stream_id = $movie->live_stream[0]->id;
                            $chatstream->ip_address = CHttpRequest::getUserHostAddress();
                            $chatstream->created_date = new CDbExpression("NOW()");
                            $chatstream->save();                        
                        }

                        $chatid = $chatstream->id;
                        if ($chatid) {
                            $data['code'] = 200;
                            $data['status'] = "Ok";
                            $data['chatbanstatus']=$chatstream->is_banned;
                            $data['msg'] = "Chat details updated";
                        }
                    }
                }
            }
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    /** author:suraja@muvi.com
     *  use:for updating the chat ban/unban status
     *
     */
   public function actionUpdateChatBanStatus() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['chatbanstatus']=2; //when no data avaialable
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['userid']) && is_numeric($_REQUEST['userid']) && $_REQUEST['userid'] != '') {

            $command = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('sdk_users')
                    ->where('id=:userid and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':userid' => $_REQUEST['userid']));
            $getuser = $command->queryRow();
            if (isset($getuser) && count($getuser) > 0) {
                if (isset($_REQUEST['movieid']) && is_numeric($_REQUEST['movieid']) && $_REQUEST['movieid'] != '') {
                    $movie = Film::model()->with(array("live_stream" => array("id")))->find("t.id=" . $_REQUEST['movieid']);

                    if (isset($movie) && count($movie) > 0) {
                        if (isset($_REQUEST['devicetype']) && is_numeric($_REQUEST['devicetype']) && isset($_REQUEST['isbanned']) && is_numeric($_REQUEST['isbanned'])) {
                            $stream = Yii::app()->db->createCommand()
                                    ->select('id,is_banned')
                                    ->from('chat_stream')
                                    ->where('sdkuser_id=:userid and movie_id=:movie_id and stream_id=:stream_id', array(':movie_id' => $_REQUEST['movieid'], ':userid' => $_REQUEST['userid'], ':stream_id' => $movie->live_stream[0]->id));

                            $getstream = $stream->queryRow();
                            
                            if ($getstream['id']) {
                                $command = Yii::app()->db->createCommand();
                                $result = $command->update('chat_stream', array(
                                    'device_type' => $_REQUEST['devicetype'],
                                    'is_banned' => $_REQUEST['isbanned']
                                        ), 'movie_id=:movie_id AND sdkuser_id=:sdkuser_id', array(':movie_id' => $_REQUEST['movieid'], ':sdkuser_id' => $_REQUEST['userid']));


                                if ($result) {
                                    $data['code'] = 200;
                                    $data['status'] = "Ok";
                                    $data['chatbanstatus']=$_REQUEST['isbanned'];
                                    $data['msg'] = "Chat ban status updated";
                                }
                            }
                            else
                            {
                                $data['chatbanstatus']=$getstream['isbanned'];
                            }
                            
                        }
                    }
                }
            }
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }	
	public function actionAddContentRating() {
        $studio_id  = $this->studio_id;
        $is_enable_rating = Yii::app()->db->createCommand()
                ->select('rating_activated')
                ->from('studios')
                ->where('id =:studio_id', array(':studio_id'=>$this->studio_id))
                ->queryRow();
       if($is_enable_rating['rating_activated']){                 
           if (!empty($_REQUEST['content_id']) && intval($_REQUEST['content_id']) && !empty($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {            
                $rate = new ContentRating();
                $rate = $rate::model()->find('studio_id=:studio_id AND content_id=:content_id AND user_id=:user_id', array(':studio_id' => $studio_id,'content_id'=>$_REQUEST['content_id'],'user_id'=>$_REQUEST['user_id']));
                if(count($rate) > 0){
                    $data['rating'] = 0;
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['msg'] = "Review already added";            
                }else{
                    if(!empty($_REQUEST['rating']) || !empty($_REQUEST['review'])){
                        $review = !empty($_REQUEST['review'])?urldecode($_REQUEST['review']):'';
                        $rating = new ContentRating();
                        $rating->content_id = $_REQUEST['content_id'];
                        $rating->rating = $_REQUEST['rating'];
                        $rating->review = nl2br(addslashes($review));
                        $rating->created_date = new CDbExpression("NOW()");
                        $rating->studio_id = $studio_id;
                        $rating->user_ip = CHttpRequest::getUserHostAddress();
                        $rating->user_id = $_REQUEST['user_id'];
                        $rating->status = 1;
                        $rating->save();
                        $data['code'] = 200;
                        $data['status'] = "Ok";
                        $data['msg'] = "Review saved successfully";
                    }else{
                        $data['rating'] = 1;
                        $data['code'] = 200;
                        $data['status'] = "Ok";
                        $data['msg'] = "Review not added yet";
                    }              
                }
            } else {
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Required Parameter Missing";
            }
       } else{
           $data['code'] = 469;
           $data['status'] = "Failure";
           $data['msg'] = "Rating disabled";
       }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionViewContentRating() {
		$studio_id = $this->studio_id;
		$is_enable_rating = Yii::app()->db->createCommand()
				->select('rating_activated')
				->from('studios')
				->where('id =:studio_id', array(':studio_id' => $this->studio_id))
				->queryRow();
		if ($is_enable_rating['rating_activated']) {
			if (!empty($_REQUEST['content_id']) && intval($_REQUEST['content_id'])) {
				$showrating = 1;
				if (!empty($_REQUEST['user_id'])) {
					$condition = " AND user_id = " . $_REQUEST['user_id'];
					$urating = Yii::app()->db->createCommand()
							->select('CR.rating,CR.review,CR.created_date,U.display_name')
							->from('content_ratings CR,sdk_users U')
							->where('CR.user_id = U.id AND CR.content_id=:content_id AND CR.studio_id=:studio_id' . $condition, array(':content_id' => $_REQUEST['content_id'], ':studio_id' => $studio_id))
							->queryAll();
					if (count($urating) > 0) {
						$showrating = 0;
					}
				}
				$rating = Yii::app()->db->createCommand()
						->select('CR.id,CR.rating,CR.review,CR.created_date,U.display_name,CR.status')
						->from('content_ratings CR,sdk_users U')
						->where('CR.user_id = U.id AND CR.content_id=:content_id AND CR.studio_id=:studio_id', array(':content_id' => $_REQUEST['content_id'], ':studio_id' => $studio_id))
						->order('CR.id DESC')
						->queryAll();
				if (count($rating) > 0) {
					foreach ($rating as $key => $val) {
						$review[$key]['display_name'] = $val['display_name'];
						$review[$key]['created_date'] = $val['created_date'];
						$review[$key]['rating'] = $val['rating'];
						$review[$key]['review'] = $val['review'];
						$review[$key]['status'] = $val['status'];
					}
				} else {
					$review = array();
				}
				$data['code'] = 200;
				$data['status'] = "Ok";
				$data['rating'] = $review;
				$data['showrating'] = $showrating;
				$data['msg'] = "Review found";
			} else {
				$data['code'] = 448;
				$data['status'] = "Failure";
				$data['msg'] = "Required Parameter Missing";
			}
		} else {
			$data['code'] = 469;
			$data['status'] = "Failure";
			$data['msg'] = "Rating disabled";
		}
		$this->setHeader($data['code']);
		echo json_encode($data);
		exit;
	}

	public function actionCheckRating() {
        $studio_id  = $this->studio_id;
        if (!empty($_REQUEST['content_id']) && intval($_REQUEST['content_id']) && !empty($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {
            
            $rate = new ContentRating();
            $rate = $rate::model()->find('studio_id=:studio_id AND content_id=:content_id AND user_id=:user_id', array(':studio_id' => $studio_id,'content_id'=>$_REQUEST['content_id'],'user_id'=>$_REQUEST['user_id']));
            if(count($rate) > 0){
                $data['rating'] = 0;                           
            }else{
                $data['rating'] = 1;               
            }  
            $data['code'] = 200;
            $data['status'] = "Ok";
            $data['msg'] = "Review saved successfully"; 
        } else {
            $data['code'] = 448;
            $data['status'] = "Failure";
            $data['msg'] = "Required Parameter Missing";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
	/** author:suraja@muvi.com
     *  use: report abuse a chat
     *
     */
    public function actionReportabuseChat() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['reportedby']) && is_numeric($_REQUEST['reportedby']) && isset($_REQUEST['reportedto']) && is_numeric($_REQUEST['reportedto'])) {
            $command1 = Yii::app()->db->createCommand()
                    ->select('id, email, display_name')
                    ->from('sdk_users')
                    ->where('id=:userid and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':userid' => $_REQUEST['reportedby']));
            $getreportedby = $command1->queryRow();

            $command2 = Yii::app()->db->createCommand()
                    ->select('id, email, display_name')
                    ->from('sdk_users')
                    ->where('id=:userid and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':userid' => $_REQUEST['reportedto']));
            $getreportedto = $command2->queryRow();

            if (isset($getreportedby) && isset($getreportedto) && count($getreportedby) > 0 && count($getreportedto) > 0) {

                if (isset($_REQUEST['movieid']) && is_numeric($_REQUEST['movieid']) && $_REQUEST['movieid'] != '') {
                    $movie = Film::model()->with(array("live_stream" => array("id")))->find("t.id=" . $_REQUEST['movieid']);
                    if (isset($movie) && count($movie) > 0) {                        
                        $chatabuse = new ChatReportAbuse();
                        $chatabuse->report_by = $_REQUEST['reportedby'];
                        $chatabuse->reported_to = $_REQUEST['reportedto'];                       
                        $chatabuse->movie_id = $_REQUEST['movieid'];
                        $chatabuse->stream_id = $movie->live_stream[0]->id;
                        $chatabuse->message = @$_REQUEST['message'];
                        $chatabuse->ip = CHttpRequest::getUserHostAddress();
                        $chatabuse->created_date = new CDbExpression("NOW()");
                        $chatabuse->save();
                        $abuseid = $chatabuse->id;
                        if ($abuseid) {
                            $getStudioAdmin = User::model()->with(array('studio'=>array('select'=>'name')))->find('role_id=1 AND is_sdk=1 AND studio_id=:studio_id', array(':studio_id'=>$this->studio_id));
                            if(count($getStudioAdmin) > 0){
                                $req = array(
                                    'name'=>$getStudioAdmin->first_name,
                                    'studio_name'=>$getStudioAdmin->studio['name'],
                                    'from'=>$getreportedby['email'],
                                    'to'=>$getStudioAdmin->email,
                                    'reported_by_name'=>$getreportedby['display_name'],
                                    'reported_to_name'=>$getreportedto['display_name'],
                                    'sent_to'=>'admin',
                                 );
                                $adminEmail = Yii::app()->email->reportAbouseChat($req);
                            }
                            $req2 = array(
                                'name' => $getreportedby['display_name'],
                                'studio_name' => $getStudioAdmin->studio['name'],
                                'from' => $getStudioAdmin->email,
                                'to' => $getreportedby['email'],
                                'reported_by_name' => $getreportedby['display_name'],
                                'reported_to_name' => $getreportedto['display_name'],
                                'sent_to' => 'reportedby',
                            );
                            $adminEmail = Yii::app()->email->reportAbouseChat($req2);
                            $data['code'] = 200;
                            $data['status'] = "Ok";
                            $data['msg'] = "Abuse reported successfully";
                        }
                      
                    }
                }
            }
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    } 
	public function actionGetCastDetail() {
        if (!empty($_REQUEST['permalink'])) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
            if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != strtolower('en')){
                $language_id = Yii::app()->custom->getLanguage_id(strtolower($_REQUEST['lang_code']));
            }else{
                $language_id = 20;
            }
            $celb = Celebrity::model()->find('permalink=:permalink AND studio_id=:studio_id AND(language_id=:language_id OR parent_id=0 AND id NOT IN (SELECT parent_id FROM celebrities WHERE studio_id=:studio_id AND permalink=:permalink AND language_id=:language_id))', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id,':language_id' => $language_id));
            if ($celb) {
                if($celb->parent_id > 0){
                    $celb->id = $celb->parent_id;
                }
                $celeb_image = $this->getPoster($celb->id, 'celebrity', 'medium', $this->studio_id);
                $data['status'] = 200;
                $data['name'] = $celb->name;
                $data['summary'] = $celb->summary;
                $data['cast_image'] = $celeb_image;

                $cast_sql = "select films.id from films inner join movie_streams on films.id = movie_streams.movie_id inner join movie_casts on films.id = movie_casts.movie_id where movie_casts.celebrity_id = ".$celb->id." AND (movie_casts.movie_id IS NOT NULL and films.id IS NOT NULL and  movie_streams.studio_id = ".$this->studio_id." ) GROUP BY films.id,films.release_date ORDER BY films.release_date desc";
                $movies = Yii::app()->db->createCommand($cast_sql)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
                $data['movieList'] = array();
                if(count($movies) > 0){
                    $studio_id = $this->studio_id;
                    $page_size = $limit = !empty($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                    $offset = 0;
                    if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                        $offset = ($_REQUEST['offset'] - 1) * $limit;
                    }
                    $ids = array();
                    foreach($movies as $key=>$value){
                        $ids[] = $value->id;
                    }
                    $idsStr = implode(',',$ids);
                    $pdo_cond_arr = array(':studio_id' => $this->studio_id, ':movie_id' => $idsStr);

                $order = '';
                $cond = ' ';
                $_REQUEST['orderby'] = trim($_REQUEST['orderby']);
                $orderby = "  M.last_updated_date DESC ";
                $neworderby = " P.last_updated_date DESC ";
                if (isset($_REQUEST['orderby']) && $_REQUEST['orderby']!='') {
                    $order = $_REQUEST['orderby'];
                    if ($_REQUEST['orderby'] == strtolower('lastupload')) {
                        $orderby = "  M.last_updated_date DESC";
                        $neworderby = " P.last_updated_date DESC ";
                    } else if ($_REQUEST['orderby'] == strtolower('releasedate')) {
                        $orderby = "  F.release_date DESC";
                        $neworderby = " P.release_date DESC ";
                    } else if ($_REQUEST['orderby'] == strtolower('sortasc')) {
                        $orderby = "  F.name ASC";
                        $neworderby = " P.name ASC ";
                    } else if ($_REQUEST['orderby'] == strtolower('sortdesc')) {
                        $orderby = "  F.name DESC";
                        $neworderby = " P.name DESC ";
                    }
                }
                if (@$_REQUEST['genre']) {
                    if(is_array($_REQUEST['genre'])){
                        $cond .= " AND (";
                        foreach ($_REQUEST['genre'] AS $gkey => $gval){
                            if($gkey){
                                    $cond .= " OR "; 
                             }
                             $cond .= " (genre LIKE ('%" . $gval . "%'))";
                        }
                        $cond .= " ) ";
                    }else{
                        $cond .= " AND genre LIKE ('%" . $_REQUEST['genre'] . "%')";
                    }
                }
		$cond .= ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) ';
                if($_REQUEST['prev_dev']){
                    $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted, M.full_movie')
                        ->from('movie_streams M,films F')
                        ->where('M.movie_id = F.id AND M.studio_id=:studio_id AND F.id IN(:movie_id) AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0  ' . $cond, $pdo_cond_arr)
                        ->order($orderby)
                        ->limit($limit, $offset);
                }else{
                    /* Add Geo block to listing by manas@muvi.com*/
                    if(isset($_REQUEST['country']) && $_REQUEST['country']){
                        $country = $_REQUEST['country'];
                    }else{
                        $visitor_loc = Yii::app()->common->getVisitorLocation();
                        $country = $visitor_loc['country'];
                    }
                    $sql_data1 = "SELECT M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id, M.is_episode,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted,M.last_updated_date"
                            . " FROM movie_streams M,films F WHERE "
                            . "M.movie_id = F.id AND M.studio_id=".$studio_id." AND F.id IN ($idsStr) AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0 " . $cond;
                    $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = ".$studio_id." AND sc.country_code='{$country}'";
                    $sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (".$sql_data1.") AS t LEFT JOIN (".$sql_geo.") AS g ON t.movie_id = g.movieid) AS P ";                     
                    if($mostviewed){
                        $sql_data.=" LEFT JOIN (SELECT COUNT(v.movie_id) AS cnt,v.movie_id FROM video_logs v WHERE `studio_id`=".$studio_id." GROUP BY v.movie_id ) AS Q ON P.movie_id = Q.movie_id";
                    }            
                    $sql_data.=" WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY " . $neworderby. " LIMIT " . $limit . " OFFSET " . $offset;
                    $command = Yii::app()->db->createCommand($sql_data);
                }                
                $list = $command->queryAll();
				$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
				$is_payment_gateway_exist = 0;
				$payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
				if (isset($payment_gateway) && !empty($payment_gateway)) {
					$is_payment_gateway_exist = 1;
				}
				//Get Posters for the Movies 
				$movieids = '';
				$movieList = '';
				if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
					//Retrive the total counts based on deviceType 
					$countQuery  = Yii::app()->db->createCommand()
						->select('COUNT(DISTINCT F.id) AS cnt')
						->from('movie_streams M,films F')
						->where('M.movie_id = F.id AND M.studio_id=:studio_id AND F.id IN(:movie_id) AND (IF(F.content_types_id =3,is_episode=1, is_episode=0)) AND (F.content_types_id =4 OR (M.is_converted =1 AND M.full_movie !=\'\')) AND F.parent_id=0 AND M.episode_parent_id=0 ' . $cond, $pdo_cond_arr);
					$itemCount = $countQuery->queryAll();
					$item_count = @$itemCount[0]['cnt'];

					$newList = array();
					foreach ($list AS $k => $v) {
						if ($v['content_types_id'] == 3) {
							$epSql = " SELECT id FROM movie_streams where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1 AND episode_parent_id=0 LIMIT 1";
							$epData = Yii::app()->db->createCommand($epSql)->queryAll();
							if (count($epData)) {
								$newList[] = $list[$k];
							}
                                                //Added by prakash on 1st feb 2017        
						} else if($v['content_types_id'] == 4){
							$epSql = " SELECT id FROM livestream where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . "  LIMIT 1";
							$epData = Yii::app()->db->createCommand($epSql)->queryAll();
							if (count($epData)) {
								$newList[] = $list[$k];
							}
						} else if (($v['content_types_id']==1 || $v['content_types_id']==2 ) && ($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
							$newList[] = $list[$k];
						}
					}
					$list = array();
					$list = $newList;
				}
				$domainName= $this->getDomainName();
				$livestream_content_ids = '';
				foreach ($list AS $k => $v) {
					$movieids .="'" . $v['movie_id'] . "',";
					if($v['content_types_id'] == 4){
						$livestream_content_ids .="'" .$v['movie_id']. "',";
					}
					if ($v['content_types_id'] == 2 || $v['content_types_id'] == 4) {
						$defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
					} else {
						$defaultPoster = POSTER_URL . '/' . 'no-image-a.png';
					}
					$movie_id = $v['movie_id'];
					$v['poster_url'] = $defaultPoster;
					$list[$k]['poster_url'] = $defaultPoster;
                                        $list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0)?$v['is_episode']:0;
					$is_episode=0;

					$arg['studio_id'] = $this->studio_id;
					$arg['movie_id'] = $v['movie_id'];
					$arg['season_id'] = 0;
					$arg['episode_id'] = 0;
					$arg['content_types_id'] = $v['content_types_id'];

					$isFreeContent = Yii::app()->common->isFreeContent($arg);
					$list[$k]['isFreeContent'] = $isFreeContent;
					$list[$k]['embeddedUrl'] = $domainName.'/embed/'.$v['movie_stream_uniq_id'];
					$is_ppv = $is_advance = 0;
					if ((intval($isFreeContent) == 0) && ($is_payment_gateway_exist == 1)) {
						if ($v['is_converted'] == 1) {
							$ppv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id, 0);
							if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
								$is_ppv = 1;
							} else {
								$ppv_plan = Yii::app()->common->getContentPaymentType($v['content_types_id'], $v['ppv_plan_id'], $this->studio_id);
								$is_ppv = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? 1 : 0;
							}
						} else  {
							$adv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id);
							$is_advance = (isset($adv_plan->id) && intval($adv_plan->id)) ? 1 : 0;
						}
					}

					if (intval($is_ppv)) {
						$list[$k]['is_ppv'] = $is_ppv;
					}
					if (intval($is_advance)) {
						$list[$k]['is_advance'] = $is_advance;
					}

					if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
						$director = '';
						$actor = '';
						$castsarr = $this->getCasts($v['movie_id'],'',$language_id,$this->studio_id);
						if ($castsarr) {
							$actor = $castsarr['actor'];
							$actor = trim($actor, ',');
							unset($castsarr['actor']);
							$director = $castsarr['director'];
							unset($castsarr['director']);
						}
						$list[$k]['actor'] = @$actor;
						$list[$k]['director'] = @$director;
						$list[$k]['cast_detail'] = @$castsarr;
					}
				}

				$movieids = rtrim($movieids, ',');
				$livestream_content_ids = rtrim($livestream_content_ids,',');
				$liveFeedUrls = array();
				if($livestream_content_ids){
					$StreamSql = " SELECT feed_url,movie_id,is_online FROM livestream where movie_id IN (".$livestream_content_ids .') AND studio_id=' . $this->studio_id . "  ";
					$streamData = Yii::app()->db->createCommand($StreamSql)->queryAll();
					foreach($streamData AS $skey=>$sval){
						$liveFeedUrls[$sval['movie_id']]['feed_url']= $sval['feed_url'];
						$liveFeedUrls[$sval['movie_id']]['is_online']= $sval['is_online'];
					}
				}
				if ($movieids) {
					$sql = "SELECT id,name,language,censor_rating,genre,story,parent_id FROM films WHERE parent_id IN (".$movieids.") AND studio_id=".$studio_id." AND language_id=".$language_id;
					$otherlang1 = Yii::app()->db->createCommand($sql)->queryAll();
					foreach ($otherlang1 as $key => $val) {
						$otherlang[$val['parent_id']] = $val;
					}

					$psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movieids . ") AND (object_type='films' OR object_type='tvapp') ";
					$posterData = Yii::app()->db->createCommand($psql)->queryAll();
					if ($posterData) {
						$posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
						foreach ($posterData AS $key => $val) {
							$posterUrl = '';
							if($val['object_type'] == 'films'){
								$posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/thumb/' . urlencode($val['poster_file_name']);
								$posters[$val['movie_id']] = $posterUrl;
								if(($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku'){
									$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
								}
							}else if(($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')){
								$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
							}
						}
					}
					//Get view count status
					$viewStatus = VideoLogs::model()->getViewStatus($movieids,$studio_id);
					if(@$viewStatus){
						$viewStatusArr = '';
						foreach ($viewStatus AS $key=>$val){
							$viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount']; 
							$viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount']; 
						}
					}
					foreach ($list as $key => $val) {
						if (isset($otherlang[$val['movie_id']])) {
							$list[$key]['name'] = $otherlang[$val['movie_id']]['name'];
							$list[$key]['story'] = $otherlang[$val['movie_id']]['story'];
							$list[$key]['genre'] = $otherlang[$val['movie_id']]['genre'];
							$list[$key]['censor_rating'] = $otherlang[$val['movie_id']]['censor_rating'];
						}
						if (isset($posters[$val['movie_id']])) {
							if(isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != ''){
								$list[$key]['poster_url_for_tv'] =$postersforTv[$val['movie_id']];
							}
							if (($val['content_types_id'] == 2) || ($val['content_types_id'] == 4)) {
                                                            $posters[$val['movie_id']] = str_replace('/thumb/', '/episode/', $posters[$val['movie_id']]);
							}
							$list[$key]['poster_url'] = $posters[$val['movie_id']];
						}
						if(@$viewStatusArr[$val['movie_id']]){
							$list[$key]['viewStatus'] = $viewStatusArr[$val['movie_id']];
						}else{
							$list[$key]['viewStatus'] = array('viewcount'=>"0",'uniq_view_count'=>"0");
						}
						if($val['content_types_id']== 4){
							$list[$key]['feed_url'] = @$liveFeedUrls[$val['movie_id']]['feed_url'];
							$list[$key]['is_online'] = @$liveFeedUrls[$val['movie_id']]['is_online'];
						}
					}
				}
                                
				$data['msg'] = $translate['btn_ok'];
				$data['movieList'] = @$list;
                                if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] && is_numeric($_REQUEST['user_id'])) {
                                $user = SdkUser::model()->countByAttributes(array('id' => $_REQUEST['user_id']));
                                if($user) {
                                $favourite = Yii::app()->db->createCommand()
                                                     ->select('*')
                                                     ->from('user_favourite_list')
                                                     ->where('user_id=:user_id AND studio_id=:studio_id AND status=:status',array(':user_id' => $_REQUEST['user_id'], ':studio_id' => $this->studio_id, ':status' => 1))
                                                     ->queryAll();

                                 $data['is_favorite'] = (count($favourite) > 0) ? 1 : 0;
                                 }
                                 }
                                 
				$data['orderby'] = @$order;
				$data['item_count'] = @$item_count;
				$data['limit'] = @$page_size;
				$data['Ads'] = $this->getStudioAds();
            } 
            }  else{
                $data['code'] = 464;
                $data['status'] = "Failure";
                $data['msg'] = "Cast permalink is invalid!";
            }
        }  else {
            $data['code'] = 465;
            $data['status'] = "Failure";
            $data['msg'] = "Cast permalink required!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    public function actionMuvikartpayment() {
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);
        $data = array();
        $data['profile_id'] = @$_REQUEST['profile_id'];
        $data['card_type'] = @$_REQUEST['card_type'];
        $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
        $data['token'] = @$_REQUEST['token'];
        $data['email'] = @$_REQUEST['email'];
        $data['currency_id'] = @$_REQUEST['currency_id'];
        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
        $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
        $data['coupon_code'] = @$_REQUEST['coupon_code'];
        $data['amount'] = @$_REQUEST['amount'];
        $data['dollar_amount'] = @$_REQUEST['amount'];
        $data['studio_id'] = $studio_id;
        $data['user_id'] = $user_id;
        $_REQUEST["cart_item"] = json_decode($_REQUEST["cart_item"], true);
        $_REQUEST["ship"] = json_decode($_REQUEST["ship"], true);
        $data['cart_item'] = $_REQUEST["cart_item"];
        $data['ship'] = $_REQUEST["ship"];
        $gateway_info[0] = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'status' => 1, 'is_primary' => 1));
        if ($gateway_info[0]->short_code != '') {//check Payment Gatway Exists
             $this->setPaymentGatwayVariable($gateway_info);
             
            if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                $data['card_id'] = @$card->id;
                $data['token'] = @$card->token;
                $data['profile_id'] = @$card->profile_id;
                $data['card_holder_name'] = @$card->card_holder_name;
                $data['card_type'] = @$card->card_type;
                $data['exp_month'] = @$card->exp_month;
                $data['exp_year'] = @$card->exp_year;
            }
            
            $couponCode = '';
            //Calculate coupon if exists
            if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                $getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
                $data['amount'] = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
                $data['coupon_code'] = $couponCode;
                $data['discount_type'] = 0; //$getCoup["discount_type"];
                $data['discount'] = $getCoup["coupon_amount"];
            }
             
            if ($data['amount'] > 0) {
                $currency = Currency::model()->findByPk($data['currency_id']);
                $data['currency_id'] = $currency->id;
                $data['currency_code'] = $currency->code;
                $data['currency_symbol'] = $currency->symbol;
                $timeparts = explode(" ", microtime());
                $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
                $reference_number = explode('.', $currenttime);
                $data['order_reference_number'] = $reference_number[0];
                $payment_gateway_controller = 'Api' . $gateway_info[0]->short_code . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $data['gateway_code'] = $gateway_info[0]->short_code;
                $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                $trans_data = $payment_gateway::processTransactions($data); //charge the card
                if (intval($trans_data['is_success'])) {
                    if (intval($data['is_save_this_card'] == 1)) {
                        $card_info = json_decode(@$_REQUEST['card'], true);
                        $sciModel = New SdkCardInfos;
                        $card = $data;
                        $card['card_last_fourdigit'] = @$card_info['card']['card_last_fourdigit'];
                        $card['token'] = @$card_info['card']['token'];
                        $card['card_type'] = @$card_info['card']['card_type'];
                        $card['auth_num'] = @$card_info['card']['auth_num'];
                        $card['profile_id'] = @$card_info['card']['profile_id'];
                        $card['reference_no'] = @$card_info['card']['reference_no'];
                        $card['response_text'] = @$card_info['card']['response_text'];
                        $card['status'] = @$card_info['card']['status'];
                        $card['gateway_code'] = $data['gateway_code'];
                        $sciModel->insertCardInfos($studio_id, $user_id, $card, $ip);
                    }
                    //Save a transaction detail
                    $transaction = new Transaction;
                    $transaction_id = $transaction->insertTrasactions($studio_id, $user_id, $data['currency_id'], $trans_data, 4, $ip, $gateway_info[0]->short_code);
                    $data['transactions_id'] = $transaction_id;
                    /* Save to order table */
                    $data['hear_source'] = $_REQUEST['hear_source'];
                    $pgorder = new PGOrder();
                    $orderid = $pgorder->insertOrder($studio_id, $user_id, $data, $_REQUEST["cart_item"], $_REQUEST['ship'], $ip);
                    /* Save to shipping address */
                    $pgshippingaddr = new PGShippingAddress();
                    $pgshippingaddr->insertAddress($studio_id, $user_id, $orderid, $_REQUEST['ship'], $ip);
                    /* Save to Order Details */
                    $pgorderdetails = new PGOrderDetails();
                    $pgorderdetails->insertOrderDetails($orderid, $_REQUEST["cart_item"]);
                    
                    
                    //send email
                    $req['orderid'] = $orderid;
                    $req['emailtype'] = 'orderconfirm';
                    $req['studio_id'] = $studio_id;
                    $req['studio_name'] = @$data['studio_name'];
                    $req['currency_id'] = $data['currency_id'];
                    // Send email to user
                    Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $studio_id);
                    if ($isEmailToStudio) {
                        Yii::app()->email->pgEmailTriggers($req);
                    }
                    if($transaction_id){
                        $command = Yii::app()->db->createCommand();
                        $command->delete('pg_cart', 'user_id=:user_id', array(':user_id' => $user_id)); 
                    } 
                    //create order in cds
                    $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
                    if ($webservice) {
                        if ($webservice->inventory_type == 'CDS') {
                            $pgorder = new PGOrder();
                            $pgorder->CreateCDSOrder($studio_id, $orderid);
                            //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
                        }
                    }
                    $res['code'] = 200;
                    $res['status'] = "OK";
                }else{
                    $res['code'] = 411;
                    $res['status'] = "Error";
                    $res['msg'] = $translate['error_transc_process'];
                    $res['response_text'] = $trans_data['response_text'];
                }
                
            }else {
                $trans_data = array();
                $trans_data['transaction_status'] = 'succeeded';
                $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
                $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
                $trans_data['dollar_amount'] = $data['dollar_amount'];
                $trans_data['paid_amount'] = $data['amount'];
                $trans_data['bill_amount'] = $data['amount'];
                $trans_data['response_text'] = json_encode($trans_data);
                $isData = self::physicalDataInsert($data, $_REQUEST, $trans_data, $ip, $this->PAYMENT_GATEWAY[$gateway_info[0]->short_code]);
                if($isData){
                    $res['code'] = 200;
                    $res['status'] = "OK";
                }
            }
        }else{
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Studio has no payment gateway";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
        exit;
    }
    
	/**
     * @method private Muvikartpaymentnew() Payment for muvicart new
     * @author satyajit rout<satyajit@muvi.com>
     * @return json Returns the list of data in json format success or corrospending error code
     */    
    public function actionMuvikartpaymentnew() {
        if (isset($_REQUEST['user_id']) && isset($_REQUEST['ship_id']) && isset($_REQUEST['cart_item_id'])) {
            $studio_id = $this->studio_id;
            $is_muvi_kart_enable = Yii::app()->general->getStoreLink($studio_id, 1);
            if (isset($is_muvi_kart_enable) && intval($is_muvi_kart_enable)) {
                $ip = CHttpRequest::getUserHostAddress();
                $data['cart_item_id'] = $_REQUEST['cart_item_id'];
                $data['ship_id'] = $_REQUEST['ship_id'];
                $cart_item = PGCart::model()->findByPk($data['cart_item_id']);
                if (count($cart_item) > 0) {
                    $currency_id = (isset($_REQUEST['currency_id'])) ? $_REQUEST['currency_id'] : Studio::model()->findByPk($studio_id)->default_currency_id;
                    $user_id = $_REQUEST['user_id'];
                    $lang_code = @$_REQUEST['lang_code'];
                    $translate = $this->getTransData($lang_code, $studio_id);
                    $data = array();
                    $data['currency_id'] = $currency_id;
                    $data['email'] = @$_REQUEST['email'];
                    $data['currency_id'] = $currency_id;
                    $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
                    $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
                    $data['coupon_code'] = @$_REQUEST['coupon_code'];
                    $data['studio_id'] = $studio_id;
                    $data['user_id'] = $user_id;
                    $data['cart_item_id'] = $_REQUEST['cart_item_id'];
                    $data['ship_id'] = $_REQUEST['ship_id'];
                    $cart_item = PGCart::model()->findByPk($data['cart_item_id']);

                    $_REQUEST["cart_item"] = json_decode($cart_item->cart_item, true);
                    $amount_cal = 0;
                    foreach ($_REQUEST["cart_item"] as $key => $val) {
                        //  $amount_cal += ($val['price'] * $val['quantity']);
                        $amount_cal += $val['price'];
                    }
                    if (isset($_REQUEST['shipp_cost'])) {
                        $amount_cal = $amount_cal + $_REQUEST['shipp_cost'];
                        $data['shipping_cost'] = $_REQUEST['shipp_cost'];
                    }
                    $ship_item = PGSavedAddress::model()->findByPk($data['ship_id']);
                    $ship_arr = array('first_name' => $ship_item->first_name,
                        'address' => $ship_item->address,
                        'address2' => $ship_item->address2,
                        'city' => $ship_item->city,
                        'state' => $ship_item->state,
                        'country' => $ship_item->country,
                        'zip' => $ship_item->zip,
                        'phone_number' => $ship_item->phone_number,
                        'email' => $ship_item->email,
                    );
                    $_REQUEST["ship"] = $ship_arr;
                    $data['amount'] = $amount_cal;
                    $data['dollar_amount'] = $amount_cal;
                    $data['cart_item'] = $_REQUEST["cart_item"];
                    $data['ship'] = $_REQUEST["ship"];

                    $gateway_info[0] = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'status' => 1, 'is_primary' => 1));
                    if ($gateway_info[0]->short_code != '') {//check Payment Gatway Exists
                        $this->setPaymentGatwayVariable($gateway_info);

                        if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                            $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                            $data['card_last_fourdigit'] = @$card->card_last_fourdigit;
                            $data['card_id'] = @$card->id;
                            $data['token'] = @$card->token;
                            $data['profile_id'] = @$card->profile_id;
                            $data['card_holder_name'] = @$card->card_holder_name;
                            $data['card_type'] = @$card->card_type;
                            $data['exp_month'] = @$card->exp_month;
                            $data['exp_year'] = @$card->exp_year;
                        } else {
                            $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
                            $data['token'] = @$_REQUEST['token'];
                            $data['profile_id'] = @$_REQUEST['profile_id'];
                            $data['card_type'] = @$_REQUEST['card_type'];
                        }
                        $couponCode = '';
                        //Calculate coupon if exists
                        if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                            $getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
                            $data['amount'] = $getCoup["amount"];
                            $couponCode = $getCoup["couponCode"];
                            $data['coupon_code'] = $couponCode;
                            $data['discount_type'] = 0; //$getCoup["discount_type"];
                            $data['discount'] = $getCoup["coupon_amount"];
                        }

                        if ($data['amount'] > 0) {
                            $currency = Currency::model()->findByPk($data['currency_id']);
                            $data['currency_id'] = $currency->id;
                            $data['currency_code'] = $currency->code;
                            $data['currency_symbol'] = $currency->symbol;
                            $timeparts = explode(" ", microtime());
                            $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
                            $reference_number = explode('.', $currenttime);
                            $data['order_reference_number'] = $reference_number[0];
                            $payment_gateway_controller = 'Api' . $gateway_info[0]->short_code . 'Controller';
                            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                            $payment_gateway = new $payment_gateway_controller();
                            $data['gateway_code'] = $gateway_info[0]->short_code;
                            $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                            $trans_data = $payment_gateway::processTransactions($data); //charge the card
                            if (intval($trans_data['is_success'])) {
                                if (intval($data['is_save_this_card'] == 1)) {
                                    $sciModel = New SdkCardInfos;
                                    $card = $data;
                                    $card['card_last_fourdigit'] = $_REQUEST['card_last_fourdigit'];
                                    $card['token'] = $_REQUEST['token'];
                                    $card['card_type'] = $_REQUEST['card_type'];
                                    $card['auth_num'] = $_REQUEST['auth_num'];
                                    $card['profile_id'] = $_REQUEST['profile_id'];
                                    $card['reference_no'] = $_REQUEST['reference_no'];
                                    $card['response_text'] = $_REQUEST['response_text'];
                                    $card['status'] = $_REQUEST['status'];
                                    $sciModel->insertCardInfos($studio_id, $user_id, $card, $ip);
                                }
                                //Save a transaction detail
                                $transaction = new Transaction;
                                $transaction_id = $transaction->insertTrasactions($studio_id, $user_id, $data['currency_id'], $trans_data, 4, $ip, $gateway_info[0]->short_code);
                                $data['transactions_id'] = $transaction_id;
                                /* Save to order table */
                                $data['hear_source'] = $_REQUEST['hear_source'];
                                $pgorder = new PGOrder();
                                $orderid = $pgorder->insertOrder($studio_id, $user_id, $data, $_REQUEST["cart_item"], $_REQUEST['ship'], $ip);
                                /* Save to shipping address */
                                $pgshippingaddr = new PGShippingAddress();
                                $pgshippingaddr->insertAddress($studio_id, $user_id, $orderid, $_REQUEST['ship'], $ip);
                                /* Save to Order Details */
                                $pgorderdetails = new PGOrderDetails();
                                $pgorderdetails->insertOrderDetails($orderid, $_REQUEST["cart_item"]);


                                //send email
                                $req['orderid'] = $orderid;
                                $req['emailtype'] = 'orderconfirm';
                                $req['studio_id'] = $studio_id;
                                $req['studio_name'] = @$data['studio_name'];
                                $req['currency_id'] = $data['currency_id'];
                                // Send email to user
                                Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
                                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $studio_id);
                                if ($isEmailToStudio) {
                                    Yii::app()->email->pgEmailTriggers($req);
                                }
                                //create order in cds
                                $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
                                if ($webservice) {
                                    if ($webservice->inventory_type == 'CDS') {
                                        $pgorder = new PGOrder();
                                        $pgorder->CreateCDSOrder($studio_id, $orderid);
                                        //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
                                    }
                                }
                                PGCart::model()->deleteByPk($data['cart_item_id']);
                                $res['code'] = 200;
                                $res['status'] = "OK";
                                $res['id'] = $this->getUniqueIdEncode($transaction_id);
                                $res['orderid'] = $orderid;
                            } else {
                                $res['code'] = 411;
                                $res['status'] = "Error";
                                $res['msg'] = $translate['error_transc_process'];
                                $res['response_text'] = $trans_data['response_text'];
                            }
                        } else {
                            $trans_data = array(
                                'invoice_id' => Yii::app()->common->generateUniqNumber(),
                                'order_number' => Yii::app()->common->generateUniqNumber(),
                                'transaction_status' => 'Success',
                                'paid_amount' => 0.00,
                                'dollar_amount' => 0.00
                            );

                            $isData = self::physicalDataInsert($data, $_REQUEST, $trans_data, $ip, $this->PAYMENT_GATEWAY[$gateway_info[0]->short_code]);
                            if ($isData) {
                                $res['code'] = 200;
                                $res['status'] = "OK";
                                $res['id'] = $this->getUniqueIdEncode($isData);
                            }
                        }
                    } else {
                        $res['code'] = 411;
                        $res['status'] = "Error";
                        $res['msg'] = "Studio has no payment gateway";
                    }
                } else {
                    $res['code'] = 411;
                    $res['status'] = "Error";
                    $res['msg'] = "You don't have any item in your cart";
                }
            } else {
                $res['code'] = 412;
                $res['status'] = "Error";
                $res['msg'] = "Studio has not enable muvi Kart";
            }
        } else {
            $res['code'] = 413;
            $res['status'] = "Error";
            $res['msg'] = "Pleae provide required parameter";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
        exit;
    }
	
    public function physicalDataInsert($datap, $pay, $trans_data, $ip, $short_code) {
        //Save a transaction detail
        $transaction = new Transaction;
        $transaction_id = $transaction->insertTrasactions($this->studio_id, $pay['user_id'], $datap['currency_id'], $trans_data, 4, $ip, $short_code);
        $datap['transactions_id'] = $transaction_id;
        /* Save to order table */
        $datap['hear_source'] = $pay['hear_source'];
        $pgorder = new PGOrder();
        $orderid = $pgorder->insertOrder($this->studio_id, $pay['user_id'], $datap, $datap["cart_item"], $datap['ship'], $ip);
        /* Save to shipping address */
        $pgshippingaddr = new PGShippingAddress();
        $pgshippingaddr->insertAddress($this->studio_id, $pay['user_id'], $orderid, $datap['ship'], $ip);
        /* Save to Order Details */
        $pgorderdetails = new PGOrderDetails();
        $pgorderdetails->insertOrderDetails($orderid, $datap["cart_item"]);
        if ($pay['card_options'] != '') {
            $data = array('isSuccess' => 1);
            $data = json_encode($data);
        }
        //send email
        $req['orderid'] = $orderid;
        $req['emailtype'] = 'orderconfirm';
        $req['studio_id'] = $this->studio_id;
        $req['studio_name'] = $datap['studio_name'];
        $req['currency_id'] = $datap['currency_id'];
        // Send email to user
        Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio_id);
        if ($isEmailToStudio) {
            Yii::app()->email->pgEmailTriggers($req);
        }
        if($transaction_id){
        $command = Yii::app()->db->createCommand();
        $command->delete('pg_cart', 'user_id=:user_id', array(':user_id' => $datap['user_id'])); 
        }   
        //create order in cds
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio_id));
        if ($webservice) {
            if ($webservice->inventory_type == 'CDS') {
                $pgorder->CreateCDSOrder($this->studio_id, $orderid);
                //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
            }
        }
        return 1;
    }
    /**
     * @method private GetAppMenu() Get the list of Mobile & TV Apps Menu 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return json Returns the list of menu in json format
     * 
     */
    public function actionGetAppMenu(){
        $lang_code   = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate   = $this->getTransData($lang_code, $this->studio_id);
        $app_menu    = AppMenu::model()->findByAttributes(array('studio_id' => $this->studio_id), array('select' => 'id'));
        if(!empty($app_menu)){
            $menu_items = AppMenuItems::model()->getAllMenus($this->studio_id, $app_menu->id, $language_id);
            if(!empty($menu_items)){
                $data['code'] = 200;
                $data['status'] = 'OK'; 
                $data['menu_items'] = $menu_items;
            }else{
                $data['code'] = 204;
                $data['status'] = 'Failure';
                $data['msg'] = $translate['no_data'];
            }
        }else{
            $data['code'] = 204;
            $data['status'] = 'Failure';
            $data['msg'] = $translate['no_data'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    /**
     * @method private GetAppHomePage() Get home page for  Mobile & TV Apps 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return json Returns the list of banners and featured sections in json format
     * 
     */
    public function actionGetAppHomePage(){
        $lang_code   = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $home_layout = StudioConfig::model()->getConfig($this->studio_id, 'home_layout');
        $banners = AppBanners::model()->findAllByAttributes(array('studio_id' => $this->studio_id), array('order' => 'id_seq ASC','select' => 'id,image_name,banner_url'));
        $sections = $appbanner = array();
        $banner_text = "";
        if (!empty($banners)) {
            $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
            $i = 0;
            foreach ($banners AS $key => $banner) {
                $banner_src = $banner->image_name;
                $banner_id  = $banner->id;
                $appbanner[$i]['image_path'] = $posterCdnUrl . "/system/studio_banner/" . $this->studio_id . "/original/" . urlencode(trim($banner_src));
                $appbanner[$i]['banner_url'] =  $banner->banner_url;
                $i++;
            }
            $banner_text = AppBannerText::model()->getBannerText($this->studio_id, $language_id, 'banner_text')->banner_text;
        }
        if(!empty($home_layout) && ($home_layout['config_value'] == 1)){
            $featured = 0;
        }else{
            $featured = 1;
            $sections = AppFeaturedSections::model()->getAllFeaturedSections($this->studio_id, $language_id);
        }
        $data['code'] = 200;
        $data['status'] = 'OK';
        $data['BannerSectionList'] = @$appbanner;
        $data['banner_text'] = @$banner_text;
        $data['is_featured'] = $featured;
        $data['SectionName'] = @$sections;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    /**
     * @method private GetAppFeaturedContent() Get featured contents of a particular section for  Mobile & TV Apps 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return json Returns the list of featured contents in json format
     * @param  section_id integer
     * 
     */
    public function actionGetAppFeaturedContent(){
        $lang_code   = @$_REQUEST['lang_code'];
        $user_id     = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) ? $_REQUEST['user_id'] : 0;
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate   = $this->getTransData($lang_code, $this->studio_id);
        if(isset($_REQUEST['section_id']) && $_REQUEST['section_id'] !=""){
            $domainName= $this->getDomainName();
            $contents = AppFeaturedContent::model()->getAppFeatured($_REQUEST['section_id'], $this->studio_id, $language_id, $user_id, $domainName);
            if(!empty($contents)){
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['section'] = $contents;
            }else{
                $data['code'] = 204;
                $data['status'] = 'Failure';
                $data['msg'] = $translate['content_not_found'];
            }
        }else{
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = $translate['section_not_found'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
	 //@vi - get HH:MM:SS values to seconds || modified for Ads streaming 
    //Date : 07-2017-21
    public function getHHMMSSToseconds($time = ''){
        if($time!=''){        
             $timeExploded = explode(':', $time);
             return $timeExploded[0] * 3600 + $timeExploded[1] * 60 + $timeExploded[2];
		}
    }
	function actiongetImages(){
        if(!empty($_REQUEST['image_key'])){
            $studio_id = $this->studio_id;
            $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
            $image_root_url = CDN_HTTP . $bucketInfo['cloudfront_url'].'/'.$bucketInfo['unsignedFolderPath'];
            $image_key = trim($_REQUEST['image_key']);
            $limit = !empty($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            $command = Yii::app()->db->createCommand()
                    ->select("id,CONCAT('$image_root_url',s3_thumb_name) AS thumb_name, CONCAT('$image_root_url',s3_original_name) AS original_name, image_key")
                    ->from('image_management')
                    ->where("flag_deleted=0 AND studio_id=:studio_id AND image_key !='' AND image_key LIKE :image_key", array(':studio_id' => $studio_id, ':image_key'=>'%'.$image_key.'%'))
                    ->queryAll();
            
            $image_key = array_filter(preg_split( "/( |_)/", $image_key ));
            $command2 = array();
            if(count($image_key)>1){
                $implodeText = ' image_key LIKE \'%'.implode('%\' OR image_key LIKE \'%', $image_key).'%\'';
                $command2 = Yii::app()->db->createCommand()
                    ->select("id,CONCAT('$image_root_url',s3_thumb_name) AS thumb_name, CONCAT('$image_root_url',s3_original_name) AS original_name, image_key")
                    ->from('image_management')
                    ->where("flag_deleted=0 AND studio_id=:studio_id AND image_key !='' AND $implodeText", array(':studio_id' => $studio_id))
                    ->queryAll();
            }
            $mergeArr = array_merge($command, $command2);
            $input = array_map("unserialize", array_unique(array_map("serialize", $mergeArr)));
            $page = $offset < 1 ? 1 : $offset;
            $start = ($offset - 1) * ($limit + 1);
            $offset = $limit + 1;
            $result = array_slice($input, $start, $offset);
             
            $res['code'] = 200;
            $res['data'] = $input;
        } else {
            $res['code'] = 471;
            $res['msg'] = "Please ener image key"; 
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
    }
	
    public function actionGetAllCast() {
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $this->studio_id);
        if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != strtolower('en')) {
            $language_id = Yii::app()->custom->getLanguage_id(strtolower($_REQUEST['lang_code']));
        } else {
            $language_id = 20;
        }
        $studio_id = $this->studio_id;
        $limit = !empty($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
        $offset = 0;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
            $offset = ($_REQUEST['offset'] - 1) * $limit;
        }
        $_REQUEST['orderby'] = trim($_REQUEST['orderby']);
        $orderby = " created_date DESC";
        if (isset($_REQUEST['orderby']) && $_REQUEST['orderby'] != '') {
            if ($_REQUEST['orderby'] == strtolower('lastupdate')) {
                $orderby = " last_updated_date DESC";
            } else if ($_REQUEST['orderby'] == strtolower('sortasc')) {
                $orderby = " name ASC";
            } else if ($_REQUEST['orderby'] == strtolower('sortdesc')) {
                $orderby = " name DESC";
            }
        }
        $command = Yii::app()->db->createCommand()
                ->select('name, permalink, summary')
                ->from('celebrities')
                ->where("studio_id=:studio_id AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM celebrities WHERE studio_id={$studio_id} AND language_id={$language_id}))", array(':studio_id'=>$studio_id))
                ->order($orderby)
                ->limit($limit, $offset)
                ->queryAll(); 
        $res['code']=200;
        $res['data']=$command;
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }	
}
