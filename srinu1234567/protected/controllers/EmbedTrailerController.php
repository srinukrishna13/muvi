<?php
require 's3bucket/aws-autoloader.php';
use Aws\CloudFront\CloudFrontClient;
class EmbedTrailerController extends Controller{
	
    public function actionIndex(){ 
        $data = Yii::app()->db->createCommand()
                ->select('f.id as filmId,f.name,f.studio_id,f.uniq_id,f.meta_description,mt.id,mt.video_remote_url,mt.trailer_file_name,mt.third_party_url,mt.video_resolution,s.s3bucket_id,s.name AS studioName')
                ->from('films f ,movie_trailer mt, studios s')
                ->where('f.id = mt.movie_id AND f.studio_id=s.id AND mt.is_converted=1 AND f.uniq_id=:uniq_id', array(':uniq_id'=>$_REQUEST['uniq_id']))
                ->queryAll();

        if($data){ 
            if (Yii::app()->common->isGeoBlockContent(@$data[0]['filmId'], 0,@$data[0]['studio_id'])) {
                $trailer = $data[0]['third_party_url'];
                if ($trailer!='') {
                    Yii::app()->theme = 'bootstrap';
                    $this->layout=false;
                    $this->renderPartial('thirdpartylist', array('third_party_url' =>$trailer));  
                }else{
                    Yii::app()->theme = 'admin';
                    $this->layout=false;
                    $embed_id = $data[0]['embed_id']; 
                    $movie_id = $data[0]['filmId'];
                    $page_title = $data[0]['name'];
                    $page_desc = $data[0]['meta_description'];
                    $studio_id = $data[0]['studio_id'];
                    $studio_name = $data[0]['studioName'];
                    $v_logo = Yii::app()->general->getPlayerLogo($studio_id);
                    define('S3BUCKET_ID',$data[0]['s3bucket_id']);
                    $multipleVideo = array();
                    $defaultResolution = 144;
                    $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
                    $fullmovie_path = $data[0]['video_remote_url'];
                    $multipleVideo = $this->getvideoResolution($data[0]['video_resolution'], $fullmovie_path);
                    if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)){
                        $userintSpeed = 10;
                        if(isset($_REQUEST['user_internet_speed']) && ($_REQUEST['user_internet_speed'] != 0)){
                            $userintSpeed = $_REQUEST['user_internet_speed'];
                        }
                        $videoToBeplayed =$this->getVideoToBePlayed($multipleVideo, $fullmovie_path,$userintSpeed);
                    } else if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 2)){
                        $videoToBeplayed =$this->getVideoToBePlayed($multipleVideo, $fullmovie_path,$_REQUEST['user_internet_speed']);
                    } else{
                        $videoToBeplayed =$this->getVideoToBePlayed($multipleVideo, $fullmovie_path);
                    }

                    $device_type = isset($_REQUEST['device_type']) && intval($_REQUEST['device_type']) ? $_REQUEST['device_type'] : 1;
                    $videoToBeplayed12 = explode(",",$videoToBeplayed);
                    $fullmovie_path = $videoToBeplayed12[0];
                    $defaultResolution = $videoToBeplayed12[1];
                    $internetSpeedImage = CDN_HTTP.$bucketInfo['bucket_name'].'.'.$bucketInfo['s3url'].'/check-download-speed.jpg';
                    if(HOST_IP != '127.0.0.1'){
                        $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                        $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                        $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl,0,"",6000000,$studio_id);
                    }
                    $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id,'embed_watermark');
                    $embedWaterMArk = 0;
                    if($getStudioConfig){
                        if(@$getStudioConfig['config_value'] == 1){
                            $embedWaterMArk = 1;
                        }
                    }      
                    $item_poster = $this->getPoster($data[0]['filmId'],'films','original',$studio_id);
                    $waterMarkOnPlayer = Yii::app()->general->playerWatermark($studio_id,@$_REQUEST['watermark_value']);
                    //rendering page
                    $this->render('index',array('embed_id' => $embed_id,'device_type'=>$device_type,'movie_id' => $movie_id,'embedWaterMArk' => $embedWaterMArk,'defaultResolution' => $defaultResolution,
                                                    'internetSpeedImage'=>$internetSpeedImage, 'multipleVideo' => $multipleVideo,
                                                    'fullmovie_path' => $fullmovie_path,'v_logo'=>$v_logo,
                                                    'movieData'=>@$data[0],'studio_id'=>$studio_id,
                                                    'waterMarkOnPlayer' => $waterMarkOnPlayer, 'page_title' => $page_title,
                                                    'page_desc' => $page_desc,'studio_name' => $studio_name, 'item_poster' => $item_poster
                                                ));

                    exit;
                }
            } else{
                        $content = '<div style="height:250px;text-align:center; color:red;"><h3>This content is not available to stream in your country</h3></div>';
                        $this->render('//layouts/geoblock_error',array('content'=>$content));
                        exit;
            }
        } else{
            $this->render('404');
        }		
    }
}

