<?php
if((strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'awss3/saves3bucketdetails') ||
        (strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'awss3/movefileasperstudios') ||
        (strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'awss3/createstudiocdnmanualy')){
    require 'aws/aws-autoloader.php';
}
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
require_once( $_SERVER["DOCUMENT_ROOT"].'/SolrPhpClient/Apache/Solr/Service.php' );
class Awss3Controller extends Controller{
    public $defaultAction = 's3BucketSetup';
    public $headerinfo='';
    public $layout = 'admin';
    protected function beforeAction($action){
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        
        parent::beforeAction($action);
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        }
        Yii::app()->theme = 'admin';
        //$title_page = Yii::app()->controller->action->id;
        // $title_page = preg_replace('/(?<!\ )[A-Z]/', ' $0', $title_page);
        //echo $Word; exit;
        $this->pageTitle = Yii::app()->name . ' | Manage S3 Bucket';
        return true;
    } 
    function actions3BucketSetup(){
        $this->breadcrumbs=array('Settings'=>array('awss3/s3BucketSetup'), 'S3 Bucket Info');
        $this->pageTitle="Muvi | S3 Bucket Info";
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        $theme_folder = $std->theme;
        $studioS3BucketId = $std->studio_s3bucket_id;
        $data = "";
        if($studioS3BucketId != 0){
            $stdS3bucket = new StudioS3buckets();
            $data = $stdS3bucket->findByPk($studioS3BucketId);
        }
        $this->render('s3_bucket_setup',array('data' => $data,'theme_folder' => $theme_folder));
    }
        
    function actionsaveS3BucketDetails(){
        $studio_id = Yii::app()->common->getStudiosId();
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        $theme_folder = $std->theme;
        $url=$this->createUrl('awss3/s3BucketSetup');
        $fileInfo = new SplFileInfo($_FILES["pem_file_path"]["name"]);
        $fileExt = $fileInfo->getExtension();
        if($_REQUEST["id"] == ''){
            if($fileExt = 'pem'){
                $pemFilePathfirstPath = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'pem/' ;
                $pemFilePath = $pemFilePathfirstPath . $theme_folder . '/';
                $fileName = Yii::app()->common->fileName($_FILES["pem_file_path"]["name"]);
                $sTempFileName = $pemFilePath.$fileName;
                if(!is_dir($pemFilePathfirstPath)) {
                    mkdir($pemFilePathfirstPath);
                    chmod($pemFilePathfirstPath,0777);
                }
                if(!is_dir($pemFilePath)) {
                    mkdir($pemFilePath);
                    chmod($pemFilePath,0777);
                }
                if(move_uploaded_file($_FILES['pem_file_path']['tmp_name'], $sTempFileName)){
                    chmod($sTempFileName,0777);
                    $checkS3Details = $this->checkaccess($_REQUEST['bucket_name'],$_REQUEST['access_key'],$_REQUEST['secret_key']);
                    if($checkS3Details == 1){
                        $cloudFrontDetails = $this->createCloudFontUrlForNewBucket($_REQUEST['access_key'],$_REQUEST['secret_key'],$_REQUEST['bucket_name'],$studio_id);
                        $cloudFrontDetails = explode(":::",$cloudFrontDetails);
                        $stdS3bucket = new StudioS3buckets();
                        $stdS3bucket->bucket_name = $_REQUEST['bucket_name'];
                        $stdS3bucket->s3url = $_REQUEST['s3url'];
                        $stdS3bucket->access_key = $_REQUEST['access_key'];
                        $stdS3bucket->secret_key = $_REQUEST['secret_key'];
                        $stdS3bucket->pem_file_path = $fileName;
                        $stdS3bucket->s3cmd_file_name = $_REQUEST['s3cmd_file_name'];
                        $stdS3bucket->key_pair_id = $_REQUEST['key_pair_id'];
                        $stdS3bucket->created_date = new CDbExpression("NOW()");
                        $stdS3bucket->modified_date = new CDbExpression("NOW()");
                        $stdS3bucket->save();
                        $stdS3bucketId = $stdS3bucket->id;
                        Yii::app()->session['studio_s3bucket_id'] = $stdS3bucketId;
                        //Studio bucket id update
                        $std->studio_s3bucket_id = $stdS3bucketId;
                        $std->signed_url = $cloudFrontDetails[1];
                        $std->unsigned_url = $cloudFrontDetails[0];
                        $std->video_url = $cloudFrontDetails[2];
                        $std->cdn_status = 1;
                        $std->save();
                        Yii::app()->user->setFlash('success', 'Data saved successfully.');
                    }else{
                        Yii::app()->user->setFlash('error', $checkS3Details);
                    }
                }else{
                    Yii::app()->user->setFlash('error', 'Data could not saved.');
                }
            }else{
                Yii::app()->user->setFlash('error', 'Please upload a .pem file.');
            }
        }else{
            $createCloudFontUrl = 0;
            $stdS3bucket = new StudioS3buckets();
            $stdS3bucket = $stdS3bucket->findByPk($_REQUEST['id']);
            if($_REQUEST['bucket_name'] != $stdS3bucket->bucket_name){
                $createCloudFontUrl = 1;
            }
            if($_REQUEST['access_key'] != $stdS3bucket->access_key){
                $createCloudFontUrl = 1;
            }
            if($_REQUEST['secret_key'] != $stdS3bucket->secret_key){
                $createCloudFontUrl = 1;
            }
            if($createCloudFontUrl == 1){
                $checkS3Details = $this->checkaccess($_REQUEST['bucket_name'],$_REQUEST['access_key'],$_REQUEST['secret_key']);
                if($checkS3Details == 1){
                    $cloudFrontDetails = $this->createCloudFontUrlForNewBucket($_REQUEST['access_key'],$_REQUEST['secret_key'],$_REQUEST['bucket_name'],$studio_id);
                    $cloudFrontDetails = explode(":::",$cloudFrontDetails);
                    $std->signed_url = $cloudFrontDetails[1];
                    $std->unsigned_url = $cloudFrontDetails[0];
                    $std->video_url = $cloudFrontDetails[2];
                    $std->save();
                }else{
                    Yii::app()->user->setFlash('error', $checkS3Details);
                    $this->redirect($url);exit;
                }
            }
            $stdS3bucket->bucket_name = $_REQUEST['bucket_name'];
            $stdS3bucket->s3url = $_REQUEST['s3url'];
            $stdS3bucket->access_key = $_REQUEST['access_key'];
            $stdS3bucket->secret_key = $_REQUEST['secret_key'];
            $stdS3bucket->s3cmd_file_name = $_REQUEST['s3cmd_file_name'];
            $stdS3bucket->key_pair_id = $_REQUEST['key_pair_id'];
            if($_FILES["pem_file_path"]["name"] !=''){
                if($fileExt = 'pem'){
                    $pemFilePathfirstPath = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'pem/' ;
                    $pemFilePath = $pemFilePathfirstPath . $theme_folder . '/';
                    $fileName = Yii::app()->common->fileName($_FILES["pem_file_path"]["name"]);
                    $sTempFileName = $pemFilePath.$fileName;
                    if(!is_dir($pemFilePathfirstPath)) {
                        mkdir($pemFilePathfirstPath);
                        chmod($pemFilePathfirstPath,0777);
                    }
                    if(!is_dir($pemFilePath)) {
                        mkdir($pemFilePath);
                        chmod($pemFilePath,0777);
                    }
                    if(move_uploaded_file($_FILES['pem_file_path']['tmp_name'], $sTempFileName)){ 
                        if($fileName != $stdS3bucket->pem_file_path){
                            @unlink($pemFilePath.$stdS3bucket->pem_file_path);
                        }
                        $stdS3bucket->pem_file_path = $fileName;
                    }
                    else{
                        Yii::app()->user->setFlash('error', 'Data could not saved.');
                        $this->redirect($url);exit;
                    }
                }else{
                    Yii::app()->user->setFlash('error', 'Please upload a .pem file.');
                    $this->redirect($url);exit;
                }
            }
            $stdS3bucket->modified_date = new CDbExpression("NOW()");
            $stdS3bucket->save();
            Yii::app()->user->setFlash('success', 'Data updated successfully.');
        }
        $this->redirect($url);
    }
    function createCloudFontUrlForNewBucket($accessKey,$secretKey,$bucketName,$studio_id){
        $client = S3Client::factory(array(
                            'key'    => $accessKey,
                            'secret' => $secretKey,
        ));
        $key         = $studio_id.'/EncodedVideo/blank.txt';
        $key1        = $studio_id.'/public/blank.txt';
        $key2        = $studio_id.'/RawVideo/blank.txt';
        //$key3        = 'MuviSync/blank.txt';
        $source_file = Yii::app()->getbaseUrl(true).'/blank.txt';
        $acl         = 'public-read';
        $bucket      = $bucketName;
        $client->upload($bucket, $key, $source_file, $acl);
        $client->upload($bucket, $key1, $source_file, $acl);
        $client->upload($bucket, $key2, $source_file, $acl);
        //$client->upload($bucket, $key3, $source_file, $acl);
        $result = Yii::app()->aws->createCloudFontUrlForNewSignUp($accessKey,$secretKey,$bucketName,$studio_id,1);

        return $result;
    }
    
    function checkaccess($bucket,$accessKey,$secretKey){
        try{
            $s3Client = S3Client::factory(array(
                'key'    => $accessKey,
                'secret' => $secretKey,
            ));
            //Get buckets list
            $buckets = $s3Client->listBuckets([]);
            //Go through every bucket
            foreach ($buckets['Buckets'] as $key=>$obj){
                //Check if the bucket I'm going to use exists
                if ($buckets['Buckets'][$key]['Name'] === $bucket){
                    //If exists, return true, everything is fine
                    return 1;
                }
            }
            //Bucket doesn't exists but access key id and secret are correct.
            return "Bucket does not exist";
        } catch (\Aws\S3\Exception\S3Exception $e) {
            //Exception ocurred, 
            //"SignatureDoesNotMatch" for bad secret access key
            //"InvalidAccessKeyId" for invalid access key id
            return $e->getMessage();
        }
    }
    
    public function actionMoveFileAsPerStudios(){
        $this->layout = false;
        if(isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] != ''){
            $studio_id = $_REQUEST['studio_id'];
            $studioDetails = Studios::model()->findByPk($studio_id);
            if($studioDetails->new_cdn_users == 0){
                $s3bucket_id = $studioDetails->studio_s3bucket_id;
                if($s3bucket_id == 0 ){
                    $accessKey =  Yii::app()->params->s3_key;
                    $secretKey = Yii::app()->params->s3_secret;
                }else{
                    $studioS3buckets = StudioS3buckets::model()->findByPk($s3bucket_id);
                    $accessKey = $studioS3buckets->access_key;
                    $secretKey = $studioS3buckets->secret_key;
                }
                $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
                $bucketName = $bucketInfo['bucket_name'];
                if(($studioDetails->studio_s3bucket_id !=0) || ($studioDetails->is_subscribed ==1)){
                    if($studioDetails->studio_s3bucket_id !=0){
                        $studioDetails->cdn_status = 1;
                        $cloudFrontDetails = $this->createCloudFontUrlForNewBucket($accessKey,$secretKey,$bucketName,$studio_id);
                    }else{
                        $cloudFrontDetails = Yii::app()->aws->createCloudFontUrlForNewSignUp($accessKey,$secretKey,$bucketName,$studio_id);
                        $studioDetails->cdn_status = 0;
                    }
                    print_r($cloudFrontDetails);
                    $cloudFrontDetails = explode(":::",$cloudFrontDetails);
//                    $studioDetails->signed_url = $cloudFrontDetails[1];
//                    $studioDetails->unsigned_url = $cloudFrontDetails[0];
//                    $studioDetails->video_url = $cloudFrontDetails[2];
//                    $studioCdnDetails = New StudioCdnDetails;
//                    $studioCdnDetails->studio_id =$studio_id;
//                    $studioCdnDetails->signed_url_id = $cloudFrontDetails[4];
//                    $studioCdnDetails->unsigned_url_id = $cloudFrontDetails[3];
//                    $studioCdnDetails->video_url_id = $cloudFrontDetails[5];
//                    $studioCdnDetails->save();
                }
//                $studioDetails->new_cdn_users = 1;
//                $studioDetails->save();
                $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
                $bucketName = $bucketInfo['bucket_name'];
                $s3url = $bucketInfo['s3url'];
                $s3cfg = $bucketInfo['s3cmd_file_name'];
                $signedBucketPathForview = $bucketInfo['signedFolderPath'];
                $arr = array();
                $arr['signedFolderPath'] = $studio_id."/EncodedVideo/";
                $arr['unsignedFolderPath'] = $studio_id."/public/";
                $arr['unsignedFolderPathForVideo'] = $studio_id."/RawVideo/";
                
                $folderPath = $arr;
                $signedBucketPath = $folderPath['signedFolderPath'];
                $unsignedFolderPath = $folderPath['unsignedFolderPath'];
                $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
                $bucketHttpOldUrl = 'http://'.$bucketName.'.'.$s3url.'/';
                $bucket_oldurl = "s3://".$bucketName."/";
                $bucketHttpNewUrl = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath;
                $bucket_newurl = "s3://".$bucketName."/".$signedBucketPath;
                $bucket_newurlForUnsigned = "s3://".$bucketName."/".$unsignedFolderPath;
                $bucket_newurlForunsignedVideo = "s3://".$bucketName."/".$unsignedFolderPathForVideo;
                $posterBucket = "s3://".Yii::app()->params->s3_bucketname."/";
                $moveData = '';
                $client = Yii::app()->common->connectToAwsS3($studio_id);
                
                $moveData .= "mkdir /var/www/html/moveData/".$studio_id." \n";
                $moveData .= "chmod 0777 /var/www/html/moveData/".$studio_id." \n";
                
                //For movie
                $movieDetails = movieStreams::model()->findAllByAttributes(array('studio_id'=>$studio_id));
                if($movieDetails){
                        $moveData .= "mkdir /var/www/html/moveData/".$studio_id."/movie \n";
                        $moveData .= "chmod 0777 /var/www/html/moveData/".$studio_id."/movie \n";
                        $moveData .= "cd /var/www/html/moveData/".$studio_id."/movie \n";
                    foreach($movieDetails as $movieDetailsKey => $movieDetailsVal){
                        if($movieDetailsVal->full_movie != ''){
                            $moveData .= "mkdir /var/www/html/moveData/".$studio_id."/movie/".$movieDetailsVal->id." \n";
                            $moveData .= "chmod 0777 /var/www/html/moveData/".$studio_id."/movie/".$movieDetailsVal->id." \n";
                            $moveData .= "cd /var/www/html/moveData/".$studio_id."/movie/".$movieDetailsVal->id." \n";
                            
                            $moveData .= "/usr/local/bin/s3cmd get --recursive --config /usr/local/bin/.s3cfg ".$bucket_oldurl."uploads/movie_stream/full_movie/".$movieDetailsVal->id."/ \n";
                            $moveData .= "/usr/local/bin/s3cmd sync --recursive --config /usr/local/bin/.".$s3cfg." --acl-public --add-header='content-type':'video/mp4' /var/www/html/moveData/".$studio_id."/movie/".$movieDetailsVal->id."/ ".$bucket_newurl."uploads/movie_stream/full_movie/".$movieDetailsVal->id."/ \n";
                            $moveData .= "rm -rf /var/www/html/moveData/".$studio_id."/movie/".$movieDetailsVal->id."/ \n";
                        }
                    }
                }
                //For Trailer
                $trailerDetails = Yii::app()->db->createCommand()
		->select('mt.*')
		->from('movie_trailer mt, films f')
		->where('f.id = mt.movie_id AND mt.trailer_file_name != "" AND f.studio_id='.$studio_id)
		->queryAll();
                if($trailerDetails){
                    foreach($trailerDetails as $trailerDetailsKey => $trailerDetailsVal){
                        if(strpos(TRAILER_URL,$trailerDetailsVal->video_remote_url)){
                            $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.".$s3cfg." --acl-public ".$posterBucket."uploads/trailers/".$trailerDetailsVal['id']."/ ".$bucket_newurl."uploads/trailers/".$trailerDetailsVal['id']."/ \n";   
                        }else{
                            $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.".$s3cfg." --acl-public ".$bucket_oldurl."uploads/trailers/".$trailerDetailsVal['id']."/ ".$bucket_newurl."uploads/trailers/".$trailerDetailsVal['id']."/ \n";   
                        }
                    }
                }
                
                //For Logo
                if($studioDetails->logo_file != ''){
                    $destination_path = $unsignedFolderPath .'public/'.  $studioDetails->theme . '/logos/' . $studioDetails->logo_file;
                    $source_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $studioDetails->theme . '/' . $studioDetails->logo_file;

                    $acl = 'public-read';
                    if (file_exists($source_path)){
                        $result = $client->putObject(array(
                            'Bucket' => $bucketName,
                            'Key' => $destination_path,
                            'SourceFile' => $source_path,
                            'ACL' => $acl,
                        ));
                    }
                    $s3FullPath = $unsignedFolderPath.'public/'.$studioDetails['theme'].'/logos/'.$studioDetails['logo_file'];
                    $info = $client->doesObjectExist(Yii::app()->params->s3_bucketname, $s3FullPath);
                    if($info){
                        $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.s3cfg --acl-public ".$posterBucket."public/".$studioDetails->theme."/ ".$bucket_newurlForUnsigned."public/".$studioDetails->theme."/ \n";
                    }
                    
                    @unlink($source_path);
                    
                    $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.s3cfg --acl-public ".$posterBucket."public/".$studio_id."/playerlogos/ ".$bucket_newurlForUnsigned. "public/" . $studio_id . "/playerlogos/ \n";
                }
                
                //For Fav Icon
                if($studioDetails->favicon_name != ''){
                    $favicon_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $studioDetails->theme. '/' . $studioDetails->favicon_name;
                    $destination_path = $unsignedFolderPath .'public/'. $studioDetails->theme . '/favicon/' . $studioDetails->favicon_name;

                    $acl = 'public-read';
                    if (file_exists($favicon_path)){
                        $result = $client->putObject(array(
                            'Bucket' => $bucketName,
                            'Key' => $destination_path,
                            'SourceFile' => $favicon_path,
                            'ACL' => $acl,
                        ));
                    }
                    $s3FullPath = $unsignedFolderPath.'public/'.$studioDetails['theme'].'/favicon/'.$studioDetails['logo_file'];
                    $info = $client->doesObjectExist(Yii::app()->params->s3_bucketname, $s3FullPath);
                    if($info){
                        $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.s3cfg --acl-public ".$posterBucket."public/".$studioDetails->theme."/ ".$bucket_newurlForUnsigned."public/".$studioDetails->theme."/ \n";
                    }
                    @unlink($favicon_path);
                }
                
                
                //For studio banner
                $studioBanner = StudioBanner::model()->findByAttributes(array('studio_id'=>$studio_id));
                if($studioBanner){
                    $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.s3cfg --acl-public ".$posterBucket."public/system/studio_banner/".$studio_id."/ ".$bucket_newurlForUnsigned."public/system/studio_banner/".$studio_id."/ \n";
                }
                
                $posterIdArray = array();
                
                //Poster of Movie Streams
                $movieStrDetails = Yii::app()->db->createCommand()
                ->select('p.id as ids')
                ->from('posters p,movie_streams m ')
                ->where('m.id = p.object_id AND p.object_type="moviestream" AND m.studio_id='.$studio_id)
                ->queryAll();
                if($movieStrDetails){
                    foreach($movieStrDetails as $movieStrDetailsKey => $movieStrDetailsVal){
                        $posterIdArray[] = $movieStrDetailsVal['ids'];
                    }
                }
                
                //Poster and top banner of Films
                $filmsDetails = Yii::app()->db->createCommand()
                ->select('p.id as ids')
                ->from('posters p,films f ')
                ->where('f.id = p.object_id AND (p.object_type="films" or p.object_type="topbanner") AND f.studio_id='.$studio_id)
                ->queryAll();
                if($filmsDetails){
                    foreach($filmsDetails as $filmsDetailsKey => $filmsDetailsVal){
                        $posterIdArray[] = $filmsDetailsVal['ids'];
                    }
                }
                
                //Images of Celeb
                $celebrityDetails = Yii::app()->db->createCommand()
                ->select('p.id as ids')
                ->from('posters p,celebrities c ')
                ->where('c.id = p.object_id AND p.object_type="celebrity" AND c.studio_id='.$studio_id)
                ->queryAll();
                if($celebrityDetails){
                    foreach($celebrityDetails as $celebrityDetailsKey => $celebrityDetailsVal){
                        $posterIdArray[] = $celebrityDetailsVal['ids'];
                    }
                }
                
                //Profile picture of user
                $profilePicture = Yii::app()->db->createCommand()
                ->select('p.id as ids')
                ->from('posters p,sdk_users su')
                ->where('su.id = p.object_id AND p.object_type="profilepicture" AND su.studio_id='.$studio_id)
                ->queryAll();
                if($profilePicture){
                    foreach($profilePicture as $profilePictureKey => $profilePictureVal){
                        $posterIdArray[] = $profilePictureVal['ids'];
                    }
                }
                
                //Move Poster
                if($posterIdArray){
                    foreach($posterIdArray as $posterIdArrayKey => $posterIdArrayVal){
                        $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.s3cfg --acl-public ".$posterBucket."public/system/posters/".$posterIdArrayVal."/ ".$bucket_newurlForUnsigned."public/system/posters/".$posterIdArrayVal."/ \n";
                    }
                }
                
                //For Image Gallery
                $imageGalleryDetails = ImageManagement::model()->findByAttributes(array('studio_id'=>$studio_id));
                if($imageGalleryDetails){
                    $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.s3cfg --acl-public ".$posterBucket."imagegallery/".$studio_id."/ ".$bucket_newurlForUnsigned."imagegallery/ \n";
                }
                
                //For video Gallery
                $videoGalleryDetails = VideoManagement::model()->findAllByAttributes(array('studio_id'=>$studio_id));
                if($videoGalleryDetails){
                    $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/.s3cfg --acl-public ".$bucket_oldurl."videogallery/".$studio_id."/videogallery-image/ ".$bucket_newurlForunsignedVideo."videogallery/videogallery-image/ \n";
                    $moveData .= "mkdir /var/www/html/moveData/".$studio_id."/videogallery \n";
                    $moveData .= "chmod 0777 /var/www/html/moveData/".$studio_id."/videogallery \n";
                    $moveData .= "cd /var/www/html/moveData/".$studio_id."/videogallery \n";
                    foreach($videoGalleryDetails as $videoGalleryDetailsKey => $videoGalleryDetailsVal){
                        $moveData .= "/usr/local/bin/s3cmd get --config /usr/local/bin/.s3cfg ".$bucket_oldurl."videogallery/".$studio_id."/".$videoGalleryDetailsVal['video_name']." \n";
                        $moveData .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/.".$s3cfg." --acl-public /var/www/html/moveData/".$studio_id."/videogallery/".$videoGalleryDetailsVal['video_name']." ".$bucket_newurlForunsignedVideo."videogallery/".$videoGalleryDetailsVal['video_name']." \n";
                        $moveData .= "rm /var/www/html/moveData/".$studio_id."/videogallery/".$videoGalleryDetailsVal['video_name']." \n";
                    }
                }
                
                //Sh File Creation
                if($moveData != ''){
                    $file = 'moveFiles_'.$studio_id.'.sh';
                    $handle = fopen($_SERVER['DOCUMENT_ROOT'] ."/moveStudioData/".$file, 'w') or die('Cannot open file:  '.$file);
                    $cf='echo "${file%???}"';
                    $file_data = "file=`echo $0`\n".
                    "cf='".$file."'\n\n".
                    $moveData.
                    "# remove directory\n".
                    "rm -rf /var/www/html/moveData/$studio_id\n".
                    "# remove the process copy file\n".
                    "rm /var/www/html/moveData/$file\n";
                    fwrite($handle, $file_data);
                    fclose($handle);
                    $filePath = $_SERVER['DOCUMENT_ROOT'] ."/moveStudioData/".$file;
                    $s3 = S3Client::factory(array(
                            'key'    => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret,
                    ));
                    if($s3->upload('muvistudio', "moveStudioData/".$file, fopen($filePath, 'rb'), 'public-read')){
                        echo "Sh file created successfully";
                        unlink($filePath);
                    }else{
                        
                        $studioDetails = Studios::model()->findByPk($studio_id);
                        $studioDetails->signed_url = "";
                        $studioDetails->unsigned_url = "";
                        $studioDetails->video_url = "";
                        $studioDetails->cdn_status = 0;
                        $studioDetails->new_cdn_users = 0;
                        $studioDetails->save();
                        
                        $studioCdnDetails = StudioCdnDetails::findByAttributes()->findByPk($studio_id);
                        $studioCdnDetails->delete();
                    }
                }
            }
        }else{
            echo "Pls provide studio_id";
        }
    }
    
    public function actiondeleteFileAsPerStudios(){
        $this->layout = false;
        if(isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] != '' 
                && isset($_REQUEST['cloudFrontDetails']) && $_REQUEST['cloudFrontDetails'] != ''){
            $studio_id = $_REQUEST['studio_id'];
            $studioDetails = Studios::model()->findByPk($studio_id);
            if($studioDetails->new_cdn_users == 0){
                $s3bucket_id = $studioDetails->studio_s3bucket_id;
                $accessKey =  Yii::app()->params->s3_key;
                $secretKey = Yii::app()->params->s3_secret;
                $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
                $bucketName = $bucketInfo['bucket_name'];
                if(($studioDetails->studio_s3bucket_id !=0) || ($studioDetails->is_subscribed ==1)){
                    $cloudFrontDetails = explode(":::",$_REQUEST['cloudFrontDetails']);
                    $studioDetails->signed_url = $cloudFrontDetails[1];
                    $studioDetails->unsigned_url = $cloudFrontDetails[0];
                    $studioDetails->video_url = $cloudFrontDetails[2];
                    $studioCdnDetails = New StudioCdnDetails;
                    $studioCdnDetails->studio_id =$studio_id;
                    $studioCdnDetails->signed_url_id = $cloudFrontDetails[4];
                    $studioCdnDetails->unsigned_url_id = $cloudFrontDetails[3];
                    $studioCdnDetails->video_url_id = $cloudFrontDetails[5];
                    $studioCdnDetails->save();
                }
                $studioDetails->new_cdn_users = 1;
                $studioDetails->save();
                $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
                $bucketName = $bucketInfo['bucket_name'];
                $s3url = $bucketInfo['s3url'];
                $s3cfg = $bucketInfo['s3cmd_file_name'];
                $signedBucketPathForview = $bucketInfo['signedFolderPath'];
                $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
                $signedBucketPath = $folderPath['signedFolderPath'];
                $unsignedFolderPath = $folderPath['unsignedFolderPath'];
                $bucketHttpOldUrl = 'http://'.$bucketName.'.'.$s3url.'/';
                $bucket_oldurl = "s3://".$bucketName."/";
                $bucketHttpNewUrl = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath;
                $bucket_newurl = "s3://".$bucketName."/".$signedBucketPath;
                $bucket_newurlForUnsigned = "s3://".$bucketName."/".$unsignedFolderPath;
                $posterBucket = "s3://".Yii::app()->params->s3_bucketname."/";
                $moveData = '';
                $client = Yii::app()->common->connectToAwsS3($studio_id);
                
                //For movie
                $movieDetails = movieStreams::model()->findAllByAttributes(array('studio_id'=>$studio_id));
                if($movieDetails){
                    foreach($movieDetails as $movieDetailsKey => $movieDetailsVal){
                        if($movieDetailsVal->full_movie != ''){
                            $moveData .= "/usr/local/bin/s3cmd del --r --config /usr/local/bin/.".$s3cfg." --acl-public ".$bucket_oldurl."uploads/movie_stream/full_movie/".$movieDetailsVal->id."/  \n";
                        }
                    }
                }
                //For Trailer
                $trailerDetails = Yii::app()->db->createCommand()
		->select('mt.*')
		->from('movie_trailer mt, films f')
		->where('f.id = mt.movie_id AND mt.trailer_file_name != "" AND f.studio_id='.$studio_id)
		->queryAll();
                if($trailerDetails){
                    foreach($trailerDetails as $trailerDetailsKey => $trailerDetailsVal){
                        if(strpos(TRAILER_URL,$trailerDetailsVal->video_remote_url)){
                            $moveData .= "/usr/local/bin/s3cmd del -r --config /usr/local/bin/.".$s3cfg." ".$posterBucket."uploads/trailers/".$trailerDetailsVal['id']."/ \n";   
                        }else{
                            $moveData .= "/usr/local/bin/s3cmd del -r --config /usr/local/bin/.".$s3cfg." ".$bucket_oldurl."uploads/trailers/".$trailerDetailsVal['id']."/ \n";   
                        }
                        $trailer_url = CDN_HTTP.$bucketInfo['cloudfront_url'].'/'.$signedBucketPathForview.'uploads/trailers/'.$trailerDetailsVal['id'].'/'.$trailerDetailsVal['trailer_file_name'];
                        $updateTrailer = movieTrailer::model()->findByPk($trailerDetailsVal['id']);
                        $updateTrailer->video_remote_url = $trailer_url;
                        $updateTrailer->save();
                    }
                }
                
                //For Logo
                if($studioDetails->logo_file != ''){
                    $s3FullPath = $unsignedFolderPath.'public/'.$studioDetails['theme'].'/logos/'.$studioDetails['logo_file'];
                    $info = $client->doesObjectExist(Yii::app()->params->s3_bucketname, $s3FullPath);
                    if($info){
                        $moveData .= "/usr/local/bin/s3cmd del -r --config /usr/local/bin/.s3cfg ".$posterBucket."public/".$studioDetails->theme." \n";
                    }
                }
                
                //For Fav Icon
                if($studioDetails->favicon_name != ''){
                    $s3FullPath = $unsignedFolderPath.'public/'.$studioDetails['theme'].'/favicon/'.$studioDetails->favicon_name;
                    $info = $client->doesObjectExist(Yii::app()->params->s3_bucketname, $s3FullPath);
                    if($info){
                        $moveData .= "/usr/local/bin/s3cmd del -r --config /usr/local/bin/.s3cfg ".$posterBucket."public/".$studioDetails->theme." \n";
                    }
                }
                
                //For studio banner
                $studioBanner = StudioBanner::model()->findByAttributes(array('studio_id'=>$studio_id));
                if($studioBanner){
                    $moveData .= "/usr/local/bin/s3cmd del -r --config /usr/local/bin/.s3cfg ".$posterBucket."public/system/studio_banner/".$studio_id." \n";
                }
                
                $posterIdArray = array();
                
                //Poster of Movie Streams
                $movieStrDetails = Yii::app()->db->createCommand()
                ->select('p.id as ids')
                ->from('posters p,movie_streams m ')
                ->where('m.id = p.object_id AND p.object_type="moviestream" AND m.studio_id='.$studio_id)
                ->queryAll();
                if($movieStrDetails){
                    foreach($movieStrDetails as $movieStrDetailsKey => $movieStrDetailsVal){
                        $posterIdArray[] = $movieStrDetailsVal['ids'];
                    }
                }
                
                //Poster and top banner of Films
                $filmsDetails = Yii::app()->db->createCommand()
                ->select('p.id as ids')
                ->from('posters p,films f ')
                ->where('f.id = p.object_id AND (p.object_type="films" or p.object_type="topbanner") AND f.studio_id='.$studio_id)
                ->queryAll();
                if($filmsDetails){
                    foreach($filmsDetails as $filmsDetailsKey => $filmsDetailsVal){
                        $posterIdArray[] = $filmsDetailsVal['ids'];
                    }
                }
                
                //Images of Celeb
                $celebrityDetails = Yii::app()->db->createCommand()
                ->select('p.id as ids')
                ->from('posters p,celebrities c ')
                ->where('c.id = p.object_id AND p.object_type="celebrity" AND c.studio_id='.$studio_id)
                ->queryAll();
                if($celebrityDetails){
                    foreach($celebrityDetails as $celebrityDetailsKey => $celebrityDetailsVal){
                        $posterIdArray[] = $celebrityDetailsVal['ids'];
                    }
                }
                
                //Profile picture of user
                $profilePicture = Yii::app()->db->createCommand()
                ->select('p.id as ids')
                ->from('posters p,sdk_users su')
                ->where('su.id = p.object_id AND p.object_type="profilepicture" AND su.studio_id='.$studio_id)
                ->queryAll();
                if($profilePicture){
                    foreach($profilePicture as $profilePictureKey => $profilePictureVal){
                        $posterIdArray[] = $profilePictureVal['ids'];
                    }
                }
                
                //Move Poster
                if($posterIdArray){
                    foreach($posterIdArray as $posterIdArrayKey => $posterIdArrayVal){
                        $moveData .= "/usr/local/bin/s3cmd del -r --config /usr/local/bin/.s3cfg ".$posterBucket."public/system/posters/".$posterIdArrayVal." \n";
                    }
                }
                
                //For Image Gallery
                $imageGalleryDetails = ImageManagement::model()->findAllByAttributes(array('studio_id'=>$studio_id));
                if($imageGalleryDetails){
                    $moveData .= "/usr/local/bin/s3cmd del -r --config /usr/local/bin/.s3cfg ".$posterBucket."imagegallery/".$studio_id." \n";
                    foreach($imageGalleryDetails as $imageGalleryDetailsKey => $imageGalleryDetailsVal){
                        $s3_thumb_name = explode("imagegallery/".$studio_id,$imageGalleryDetailsVal['s3_thumb_name']);
                        $s3_original_name = explode("imagegallery/".$studio_id,$imageGalleryDetailsVal['s3_original_name']);
                        $updateImageGallery = ImageManagement::model()->findByPk($imageGalleryDetailsVal['id']);
                        $updateImageGallery->s3_thumb_name = "imagegallery".$s3_thumb_name[1];
                        $updateImageGallery->s3_original_name = "imagegallery".$s3_original_name[1];
                        $updateImageGallery->save();
                    }
                }
                
                //For video Gallery
                $videoGalleryDetails = VideoManagement::model()->findByAttributes(array('studio_id'=>$studio_id));
                if($videoGalleryDetails){
                    $moveData .= "/usr/local/bin/s3cmd del -r --config /usr/local/bin/.s3cfg ".$bucket_oldurl."videogallery/".$studio_id."/ \n";
                }
                
                //Sh File Creation
                if($moveData != ''){
                    $file = 'deleteFiles_'.$studio_id.'.sh';
                    $handle = fopen($_SERVER['DOCUMENT_ROOT'] ."/moveStudioData/".$file, 'w') or die('Cannot open file:  '.$file);
                    $cf='echo "${file%???}"';
                    $file_data = "file=`echo $0`\n".
                    "cf='".$file."'\n\n".
                    $moveData.
                    "# remove directory\n".
                    "rm -rf /var/www/html/moveData/$studio_id\n".
                    "# remove the process copy file\n".
                    "rm /var/www/html/moveData/$file\n";
                    fwrite($handle, $file_data);
                    fclose($handle);
                    $filePath = $_SERVER['DOCUMENT_ROOT'] ."/moveStudioData/".$file;
                    $s3 = S3Client::factory(array(
                            'key'    => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret,
                    ));
                    if($s3->upload('muvistudio', "moveStudioData/".$file, fopen($filePath, 'rb'), 'public-read')){
                        echo "Sh file created successfully";
                        unlink($filePath);
                    }else{
                        
                        $studioDetails = Studios::model()->findByPk($studio_id);
                        $studioDetails->signed_url = "";
                        $studioDetails->unsigned_url = "";
                        $studioDetails->video_url = "";
                        $studioDetails->cdn_status = 0;
                        $studioDetails->new_cdn_users = 0;
                        $studioDetails->save();
                        
                        $studioCdnDetails = StudioCdnDetails::findByAttributes()->findByPk($studio_id);
                        $studioCdnDetails->delete();
                    }
                }
            }
        }else{
            echo "Pls provide studio_id";
        }
    }
    
    public function actioncreateStudioCDNManualy(){
        $this->layout = false;
        if(isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] != '' ){
            $studio_id = $_REQUEST['studio_id'];
            $studioDetails = Studios::model()->findByPk($studio_id);
            if($studioDetails->new_cdn_users == 1){
                $s3bucket_id = $studioDetails->studio_s3bucket_id;
                $accessKey =  Yii::app()->params->s3_key;
                $secretKey = Yii::app()->params->s3_secret;
                $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
                $bucketName = $bucketInfo['bucket_name'];
                $cloudFrontDetails = Yii::app()->aws->createCloudFontUrlForNewSignUp($accessKey,$secretKey,$bucketName,$studio_id);
                print_r($cloudFrontDetails);
                $cloudFrontDetails = explode(":::",$cloudFrontDetails);
                $studioDetails->signed_url = $cloudFrontDetails[1];
                $studioDetails->unsigned_url = $cloudFrontDetails[0];
                $studioDetails->video_url = $cloudFrontDetails[2];
                $studioDetails->cdn_status = 0;
                $studioDetails->save();
                $studioCdnDetails = New StudioCdnDetails;
                $studioCdnDetails->studio_id =$studio_id;
                $studioCdnDetails->signed_url_id = $cloudFrontDetails[4];
                $studioCdnDetails->unsigned_url_id = $cloudFrontDetails[3];
                $studioCdnDetails->video_url_id = $cloudFrontDetails[5];
                $studioCdnDetails->save();
            }
        }else{
            echo "Pls provide studio_id";
        }
    } 
    
}