<?php

class ApicouponController extends Controller{


    function __construct() {
        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 0);
    }

    /**
     * 
      /**
     *
     * Initialize the 3rd party call
     * @author Sanjeev Kumar Malla
     */
    function initializeConnection($gateway_code) {
        if ($this->IS_LIVE_API_PAYMENT_GATEWAY[$gateway_code] != 'sandbox') {
            $location = "https://ws.cartadeldocente.istruzione.it/VerificaVoucherDocWEB/VerificaVoucher";
        } else {
            $location = "https://ws.cartadeldocente.istruzione.it/VerificaVoucherDocWEB/VerificaVoucher";
        }
        return $location;
    }

    function processCoupon($arg = array()) {
        $res = array();

		$location = self::initializeConnection($arg['gateway_code']);
		$data = array(
			'checkReq' => array(
				'tipoOperazione' => '2',
				'codiceVoucher' => $arg['ext_coupon']
			)
		);

		$password = file_get_contents($this->PATH_TO_LIB . "password.txt");

		$config = array(
			'location' => $location,
			'local_cert' => $this->PATH_TO_LIB . "Certificate.pem",
			'passphrase' => trim($password),
			'stream_context' => stream_context_create(array('ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
		))));
		$wsdl = $this->PATH_TO_LIB . "VerificaVoucher.wsdl";
		try {
			$client = new SoapClient($wsdl, $config);
			$result = $client->Check($data);
		} catch (Exception $ex) {
			$result = $ex;
			$res['isSuccess'] = 0;
		}

		$result = json_decode(json_encode($result),true);
		if((isset($result['detail']['FaultVoucher']) && !empty($result['detail']['FaultVoucher']) && trim($result['detail']['FaultVoucher']['exceptionCode'])) || (isset($result['faultstring']) && trim($result['faultstring']))){
			$res['isSuccess'] = 0;
			$res['error_code'] = $result['detail']['FaultVoucher']['exceptionCode'];
			$res['Message'] = $result['detail']['FaultVoucher']['exceptionMessage'];
			$res['transaction_status'] = 'failed';
			$res['response_text'] = json_encode($result);
		}else{
			$res['isSuccess'] = 1;
			$res['voucher_code'] = $arg['ext_coupon'];
			$res['Message'] = $result['checkResp']['nominativoBeneficiario'];
			$res['transaction_status'] = 'Success';
			$res['response_text'] = json_encode($result);
		}
        return $res;
    }
}