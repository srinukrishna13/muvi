<?php

require_once 'FirstDataApi/FirstData.php';

class ApifirstdataController extends Controller {

    /**
     * Constructor
     * @author Sunil Kund <sunil@muvi.com>
     */
    public function __construct() {
        
    }

    /**
     *
     * Initialize the api
     * @return FirstData Object
     * @author Sunil Kund <sunil@muvi.com>
     */
    function initializePaymentGateway() {
        $gateway_code = 'firstdata';
        if ($this->IS_LIVE_API_PAYMENT_GATEWAY[$gateway_code] != 'sandbox') {
            $firstData = new FirstData($this->PAYMENT_GATEWAY_API_USER[$gateway_code], $this->PAYMENT_GATEWAY_API_PASSWORD[$gateway_code]);
        } else {
            $firstData = new FirstData($this->PAYMENT_GATEWAY_API_USER[$gateway_code], $this->PAYMENT_GATEWAY_API_PASSWORD[$gateway_code], true);
        }
        return $firstData;
    }
    
    function validatePaymentGatewayCredentials($API_USER = NUll, $API_PASSWORD = NULL, $IS_LIVE_MODE = NULL) {
        $res = array();
        if (isset($API_USER) && isset($API_PASSWORD) && isset($IS_LIVE_MODE)) {
            if ($IS_LIVE_MODE != 'sandbox') {
                $firstData = new FirstData($API_USER, $API_PASSWORD);
            } else {
                $firstData = new FirstData($API_USER, $API_PASSWORD, true);
            }
            $firstData->process();
            if ($firstData->isError()) {
                if ($firstData->getErrorCode() == 401) {
                    $res['isSuccess'] = 0;
                    $res['code'] = $firstData->getErrorCode();
                    $res['Message'] = $firstData->getErrorMessage();
                }else{
                    $res['isSuccess'] = 1;
                } 
            } else {
                $res['isSuccess'] = 1;
            }
        } else {
            $res['isSuccess'] = 0;
            $res['Message'] = 'Please submit all credential details';
        }
        
        return $res;
    }

    /**
     * 
     * Validate the card and make a zero transaction
     * @param array $arg
     * @return json string
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processCard($arg = array()) {
        $firstData = self::initializePaymentGateway();
        if (isset($arg['card_number']) && !empty($arg['card_number'])) {
            $data = array();

            $data['amount'] = 0;
            $data['card_number'] = $arg['card_number'];
            $data['card_holder_name'] = $arg['card_name'];
            $data['exp'] = ($arg['exp_month'] < 10) ? '0' . $arg['exp_month'] . substr($arg['exp_year'], -2) : $arg['exp_month'] . substr($arg['exp_year'], -2);
            $data['cvv'] = $arg['cvv'];

            $firstData = self::initializePaymentGateway();

            $firstData->setTransactionType(FirstData::TRAN_PREAUTH);
            $firstData->setCreditCardNumber($data['card_number'])
                    ->setCreditCardName($data['card_holder_name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);
            if ($data['cvv']) {
                $firstData->setCreditCardVerification($data['cvv']);
            }

            $card_last_fourdigit = str_replace(substr($data['card_number'], 0, strlen($data['card_number']) - 4), str_repeat("#", strlen($data['card_number']) - 4), $data['card_number']);

            $firstData->process();

            //print '<pre>';print_r($firstData);exit;

            if ($firstData->isError()) {
                $res['isSuccess'] = 0;
                $res['code'] = $firstData->getErrorCode();
                $res['response_text'] = serialize($firstData);
                $res['Message'] = 'Invalid Card details entered. Please check again';

                //$req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
            } else {
                $respons_type = $firstData->getBankResponseType();

                if ($respons_type == 'D') {
                    //Charge forcible in card validation and refund an amount
                    $data['amount'] = 0.1;
                    $data['type'] = $firstData->getCreditCardType();
                    $firstData = self::preAuthWithTransaction($data);
                }

                if ($firstData->getBankResponseType() == 'S') {
                    $res['isSuccess'] = 1;
                    $res['TransactionRecord'] = $firstData->getTransactionRecord();
                    $res['code'] = $firstData->getBankResponseCode();
                    $res['card']['profile_id'] = '';
                    $res['card']['status'] = $res['Message'] = $firstData->getBankResponseMessage();
                    $res['card']['reference_no'] = $firstData->getTransactionTag();
                    $res['card']['card_type'] = $firstData->getCreditCardType();
                    $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                    $res['card']['auth_num'] = $firstData->getAuthNumber();
                    $res['card']['token'] = $firstData->getTransArmorToken();

                    $res['card']['response_text'] = serialize($firstData);
                    
                    
                    if ($respons_type == 'D' && $firstData->getBankResponseType() == 'S') {
                        //Refund the athuenticate charge
                        $data['token'] = $firstData->getTransArmorToken();
                        $data['cardtype'] = $firstData->getCreditCardType();
                        $refund = self::refundTransaction($data);
                    }
                }
            }
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        return json_encode($res);
    }
    
    /**
     * 
     * If card is not validated with zero transaction, make a minimum amount transaction (say $0.1) with validation
     * @param array $data
     * @return object
     * @author Sunil Kund <sunil@muvi.com>
     */
    function preAuthWithTransaction($data = array()) {
        if (isset($data) && !empty($data)) {
            $firstData = self::initializePaymentGateway();

            $firstData->setTransactionType(FirstData::TRAN_PREAUTH);
            $firstData->setCreditCardType($data['type'])
                    ->setCreditCardNumber($data['number'])
                    ->setCreditCardName($data['name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount'])
                    ->setReferenceNumber("Signup Charge");
            if ($data['cvv']) {
                $firstData->setCreditCardVerification($data['cvv']);
            }
            
            $firstData->process();

            return $firstData;
        } else {
            return '';
        }
    }
    
    /**
     * 
     * Refund an amount to user's account by using token provided from FirstData
     * @param array $data
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function refundTransaction($data = array()) {
        $res = array();
        if (isset($data) && !empty($data)) {
            $firstData = self::initializePaymentGateway();
            
            $firstData->setTransactionType(FirstData::TRAN_REFUND);
            $firstData->setCreditCardType($data['cardtype'])
                    ->setCreditCardNumber($data['number'])
                    ->setTransArmorToken($data['token'])
                    ->setCreditCardName($data['name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);

            $firstData->process();

            // Check
            if ($firstData->isError()) {
                $res['code'] = $firstData->getErrorCode();
                $res['Message'] = $firstData->getErrorMessage();
            } else {
                $res['code'] = $firstData->getBankResponseCode();
                $res['Message'] = $firstData->getBankResponseMessage();
            }
        }
        return $res;
    }
    
    /**
     * 
     * Make tranasction
     * @param object $card_info, object $user_sub
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processTransaction($card_info, $user_sub) {
        $res = array();
        $data = array();
        $data['token'] = $card_info->token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
        $data['name'] = $card_info->card_holder_name;
        $data['card_type'] = $card_info->card_type;
        $data['exp'] = ($card_info->exp_month < 10) ? '0' . $card_info->exp_month . substr($card_info->exp_year, -2) : $card_info->exp_month . substr($card_info->exp_year, -2);
        $data['billing_amount'] = $user_sub->amount;

        $firstData = self::initializePaymentGateway();
        
        $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
        $firstData->setTransArmorToken($data['token'])
                ->setCreditCardType($data['card_type'])
                ->setCreditCardName($data['name'])
                ->setCreditCardExpiration($data['exp'])
                ->setAmount($data['billing_amount']);

        $firstData->process();

        // Check
        if ($firstData->isError()) {
            $res['is_success'] = 0;
            $res['amount'] = 0;
            $res['response_text'] = serialize($firstData);
        } else {
            $respons_type = $firstData->getBankResponseType();
            
            if ($firstData->getBankResponseType() == 'S') {
                $res['is_success'] = 1;
                $res['transaction_status'] = $firstData->getBankResponseMessage();
                $res['invoice_id'] = $firstData->getTransactionTag();
                $res['order_number'] = $firstData->getAuthNumber();
                $res['amount'] = $firstData->getAmount();
                $res['paid_amount'] = $firstData->getAmount();
                $res['response_text'] = serialize($firstData);
            } else if ($respons_type == 'D') {
                $res['is_success'] = 0;
                $res['amount'] = 0;
                $res['response_text'] = serialize($firstData);
            }
        }
        
        return $res;
    }
    
    /**
     * 
     * Make tranasction
     * @param array $user
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processTransactions($user) {
        $amount = $user['amount'];
        $currency = strtoupper($user['currency_code']);
        $res = array();
        $data = array();
        $data['token'] = $user['token']; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
        $data['name'] = $user['card_holder_name'];
        $data['card_type'] = $user['card_type'];
        $data['exp'] = ($user['exp_month'] < 10) ? '0' . $user['exp_month'] . substr($user['exp_year'], -2) : $user['exp_month'] . substr($user['exp_year'], -2);
        $data['billing_amount'] = $amount;
        
        $firstData = self::initializePaymentGateway();

        $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
        $firstData->setTransArmorToken($data['token'])
                ->setCreditCardType($data['card_type'])
                ->setCreditCardName($data['name'])
                ->setCreditCardExpiration($data['exp'])
                ->setCurrency($currency)
                ->setAmount($data['billing_amount']);

        $firstData->process();

        // Check
        if ($firstData->isError()) {
            $res['is_success'] = 0;
            $res['amount'] = 0;
            $res['response_text'] = serialize($firstData);
        } else {
            $respons_type = $firstData->getBankResponseType();
            
            if ($firstData->getBankResponseType() == 'S') {
                $res['is_success'] = 1;
                $res['transaction_status'] = $firstData->getBankResponseMessage();
                $res['invoice_id'] = $firstData->getTransactionTag();
                $res['order_number'] = $firstData->getAuthNumber();
                $res['amount'] = $user['amount'];
                $res['paid_amount'] = $firstData->getAmount();
                $res['dollar_amount'] = $user['dollar_amount'];
                $res['currency_code'] = $user['currency_code'];
                $res['currency_symbol'] = $user['currency_symbol'];
                $res['response_text'] = serialize($firstData);
            } else if ($respons_type == 'D') {
                $res['is_success'] = 0;
                $res['amount'] = 0;
                $res['response_text'] = serialize($firstData);
            }
        }
        
        return $res;
    }
    
    /**
     * 
     * Cancel Customer's Account
     * @param object $user_sub
     * @return Null
     * @author Sunil Kund <sunil@muvi.com>
     */
    function cancelCustomerAccount($usersub = Null, $card_info = Null) {
        return null;
    }
    
    /**
     * 
     * Save Customer's card detail
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function saveCard($usersub, $arg) {
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    
    /**
     * 
     * Make default card where subscription will charge
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function defaultCard($usersub, $card) {
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    
    /**
     * 
     * Delete Customer's card
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function deleteCard($usersub, $card = Null) {
        $res['isSuccess'] = 1;
        
        return json_encode($res);
    }
    
    function sampleIntegrationTransaction($user = array()) {
        $firstData = self::initializePaymentGateway();
        $res = array();
        
        if (isset($user['card_number']) && !empty($user['card_number'])) {
            $data = array();
            $data['amount'] = $user['amount'];
            $data['card_number'] = $user['card_number'];
            $data['card_holder_name'] = $user['card_name'];
            $data['exp'] = ($user['exp_month'] < 10) ? '0' . $user['exp_month'] . substr($user['exp_year'], -2) : $user['exp_month'] . substr($user['exp_year'], -2);
            $data['cvv'] = $user['cvv'];
            
            $firstData->setTransactionType(FirstData::TRAN_PREAUTH);
            $firstData->setCreditCardNumber($data['card_number'])
                ->setCreditCardName($data['card_holder_name'])
                ->setCreditCardExpiration($data['exp'])
                ->setAmount($data['amount']);
            
            if ($data['cvv']) {
                $firstData->setCreditCardVerification($data['cvv']);
            }

            $firstData->process();
            if ($firstData->isError()) {
                $res['isSuccess'] = 0;
                $res['error_message'] = "User authentication failed due to invalid authentication values.";
            } else {
                if ($firstData->getBankResponseType() == 'S') {
                    $res['isSuccess'] = 1;
                } else {
                    $res['isSuccess'] = 0;
                    $res['error_message'] = (trim($firstData->getErrorMessage()) && $firstData->getErrorMessage() != 'Approved') ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                }
            }
        } else {
            $res['isSuccess'] = 0;
            $res['error_message'] = 'We are not able to process your credit card. Please try another card.';
        }

        return $res;
    }
    
    /**
     * 
     * Update user profile
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function updatePaymentProfile($user = array(),$sub = array(),$email)
    {
        $res = array();
        $res['isSuccess'] = 1;
        return $res;
    }
}

?>