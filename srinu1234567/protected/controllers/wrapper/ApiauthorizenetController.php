<?php
require_once 'authorizenet/vendor/autoload.php';

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class ApiauthorizenetController extends Controller {

    /**
     * Constructor
     * @author Sunil Kund <sunil@muvi.com>
     */
    public function __construct() {
        
    }

    /**
     *
     * Initialize the api
     * @return 
     * @author Sunil Kund <sunil@muvi.com>
     */
    function initializePaymentGateway() {
        if ($this->IS_LIVE_API_PAYMENT_GATEWAY['authorizenet'] != 'sandbox') {
            if (!defined('AUTHORIZENET_SANDBOX')) {
                define("AUTHORIZENET_SANDBOX", false);
            }
        } else {
            if (!defined('AUTHORIZENET_SANDBOX')) {
                define("AUTHORIZENET_SANDBOX", true);
            }
        }
    }
    
    
    function validatePaymentGatewayCredentialsTest($API_USER = NUll, $API_PASSWORD = NULL, $IS_LIVE_MODE = NULL) {
        echo $API_USER."--".$API_PASSWORD."--".$IS_LIVE_MODE;
        if ($IS_LIVE_MODE != 'sandbox') {
            $post_url = 'https://api.authorize.net/xml/v1/request.api';
            $data = array(
                "authenticateRequest" => array(
                    "merchantAuthentication" => array(
                        "name" => $API_USER,
                        "transactionKey" => $API_PASSWORD
                    )
                )
            );
            $json = json_encode($data);
        } else {
            $post_url = 'https://apitest.authorize.net/xml/v1/request.api';
            $data = array(
                "authenticateTestRequest" => array(
                    "merchantAuthentication" => array(
                        "name" => $API_USER,
                        "transactionKey" => $API_PASSWORD
                    )
                )
            );
            $json = json_encode($data);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        print_r($response);exit;
        if ($error = curl_error($ch)) {
            print $error;
            return FALSE;
        }
        curl_close($ch);
        
        if (strpos($response,'"resultCode":"Ok"') !== false && strpos($response, '"text":"Successful."') !== false) {
            $res['isSuccess'] = 1;
        }else if (strpos($response,'"resultCode":"Error"') !== false) {
            $res['isSuccess'] = 0;
            $res['Message'] = 'Invalid credentials have given!';
        }
        return $res;
    }
    
    
    function validatePaymentGatewayCredentials($API_USER = NUll, $API_PASSWORD = NULL, $IS_LIVE_MODE = NULL) {
        echo $API_USER."--".$API_PASSWORD."--".$IS_LIVE_MODE;
        if ($IS_LIVE_MODE != 'sandbox') {
            $post_url = 'https://api.authorize.net/xml/v1/request.api';
            $xml=<<<XML
<authenticateRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
<merchantAuthentication>
<name>$API_USER</name>
<transactionKey>$API_PASSWORD</transactionKey>
</merchantAuthentication>
</authenticateRequest>
XML;
        } else {
            $post_url = 'https://apitest.authorize.net/xml/v1/request.api';
            $xml=<<<XML
<authenticateTestRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
<merchantAuthentication>
<name>$API_USER</name>
<transactionKey>$API_PASSWORD</transactionKey>
</merchantAuthentication>
</authenticateTestRequest>
XML;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, "Content-Type:application/xml");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        print_r($response);exit;
        if ($error = curl_error($ch)) {
            print $error;
            return FALSE;
        }
        curl_close($ch);
        
        if (strpos($response,'"resultCode":"Ok"') !== false && strpos($response, '"text":"Successful."') !== false) {
            $res['isSuccess'] = 1;
        }else if (strpos($response,'"resultCode":"Error"') !== false) {
            $res['isSuccess'] = 0;
            $res['Message'] = 'Invalid credentials have given!';
        }
        return $res;
    }
    
    /**
     * 
     * Validate the card and make a 0.01 transaction and cancel immidietly once card validated
     * @param array $arg
     * @return json string
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processCard($arg = array()) {
        self::initializePaymentGateway();

        if (isset($arg['card_number']) && !empty($arg['card_number'])) {
            $data['amount'] = 0.01;//As Authorizenet is not processed with 0 amount, so here we transact a min amount and once authentication of card has done, cancel or refund that amount.
            $data['card_number'] = $arg['card_number'];
            $data['exp'] = ($arg['exp_month'] < 10) ? '0' . $arg['exp_month'] . substr($arg['exp_year'], -2) : $arg['exp_month'] . substr($arg['exp_year'], -2);
            $data['expirationDate'] = ($arg['exp_month'] < 10) ? substr($arg['exp_year'], -2).'-'.'0' . $arg['exp_month']  : substr($arg['exp_year'], -2).'-'.$arg['exp_month'];
            $data['email'] = $arg['email'];
            
            $auth = new AuthorizeNetAIM($this->PAYMENT_GATEWAY_API_USER['authorizenet'], $this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);
            $auth->setFields(
                array(
                'amount' => $data['amount'],
                'card_num' => $data['card_number'],
                'exp_date' => $data['exp']
                )
            );
            $response = $auth->authorizeAndCapture();//Authenticate to card and process transaction

            if ($response->approved) {
                $card_type = $response->card_type;
                
                /*$void = new AuthorizeNetAIM($this->PAYMENT_GATEWAY_API_USER, $this->PAYMENT_GATEWAY_API_PASSWORD);
                $void->setFields(
                    array(
                    'amount' => $data['amount'],
                    'card_num' => $data['card_number'],
                    'trans_id' => $response->transaction_id,
                    )
                );
                $void_response = $void->Void();//Cancel the previous trasanction once verified
                
                if ($void_response->approved) {*/
                    $request = new AuthorizeNetCIM($this->PAYMENT_GATEWAY_API_USER['authorizenet'], $this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);
                    
                    // Create new customer profile
                    $customerProfile                     = new AuthorizeNetCustomer;
                    $customerProfile->description        = "Creation of New customer";
                    $customerProfile->merchantCustomerId = time();
                    $customerProfile->email              = $data['email'];

                    // Add payment profile.
                    $paymentProfile = new AuthorizeNetPaymentProfile;
                    $paymentProfile->customerType = "individual";
                    $paymentProfile->payment->creditCard->cardNumber = $data['card_number'];
                    $paymentProfile->payment->creditCard->expirationDate = $data['expirationDate'];
                    $customerProfile->paymentProfiles[] = $paymentProfile;
 
                    $response = $request->createCustomerProfile($customerProfile);
                    if ($response->isOk()) {
                        $customerProfileId = $response->getCustomerProfileId();
                        $paymentProfileId = $response->getCustomerPaymentProfileIds();
                        $card_last_fourdigit = str_replace(substr($arg['card_number'], 0, strlen($arg['card_number']) - 4), str_repeat("#", strlen($arg['card_number']) - 4), $arg['card_number']);

                        $res['isSuccess'] = 1;
                        $res['card']['status'] = 'approved';
                        $res['card']['profile_id'] = $customerProfileId;//Authorize customer id which is used for recurring subscription
                        $res['card']['card_type'] = $card_type; //Card type
                        $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                        $res['card']['token'] = $paymentProfileId; //Authorize card id which is used to get card detail
                        $res['card']['response_text'] = json_encode($response);
                    } else {
                        $res['isSuccess'] = 0;
                        $res['code'] = 404;
                        $res['response_text'] = json_encode($response);
                        $res['Message'] = 'Invalid Card details entered. Please check again';
                    }
                /*} else {
                    $res['isSuccess'] = 0;
                    $res['code'] = 404;
                    $res['response_text'] = json_encode($void_response);
                    $res['Message'] = 'Invalid Card details entered. Please check again';
                }*/
            } else {
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = json_encode($response);
                $res['Message'] = 'Invalid Card details entered. Please check again';
            }
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }
        
        return json_encode($res);
    }
    
    /**
     * 
     * Make tranasction
     * @param object $card_info, object $user_sub
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processTransaction($card_info = Null, $user_sub = Null) {
        self::initializePaymentGateway();
        
        $res = array();
        
        $request = new AuthorizeNetCIM($this->PAYMENT_GATEWAY_API_USER['authorizenet'], $this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);
        
        // Create Auth & Capture Transaction
        $transaction = new AuthorizeNetTransaction;
        $transaction->amount = $user_sub->amount;
        $transaction->customerProfileId = $user_sub->profile_id;
        $transaction->customerPaymentProfileId = $card_info->token;
        $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction);
        
        if ($response->isOk()) {
            $transactionResponse = $response->getTransactionResponse();
            if ($transactionResponse->approved) {
                $res['is_success'] = 1;
                $res['transaction_status'] = $transactionResponse->approved;
                $res['invoice_id'] = $transactionResponse->transaction_id;
                $res['order_number'] = $transactionResponse->authorization_code;
                $res['amount'] = $transactionResponse->amount;
                $res['paid_amount'] = $transactionResponse->amount;
                $res['response_text'] = json_encode($transactionResponse);
            } else {
                $res['is_success'] = 0;
                $res['amount'] = 0;
                $res['response_text'] = json_encode($transactionResponse);
            }
        } else {
            $res['is_success'] = 0;
            $res['amount'] = 0;
            $res['response_text'] = json_encode($response);
        }
        
        return $res;
    }
    
    /**
     * 
     * Make tranasction
     * @param array $user
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processTransactions($user = Null) {
        self::initializePaymentGateway();
        
        $amount = $user['amount'];
        $currency = strtolower($user['currency_code']);
        $res = array();
        
        $request = new AuthorizeNetCIM($this->PAYMENT_GATEWAY_API_USER['authorizenet'], $this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);
        
        // Create Auth & Capture Transaction
        $transaction = new AuthorizeNetTransaction;
        $transaction->amount = $amount;
        $transaction->customerProfileId = $user['profile_id'];
        $transaction->customerPaymentProfileId = $user['token'];
        $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction);
        
        if ($response->isOk()) {
            $transactionResponse = $response->getTransactionResponse();
            if ($transactionResponse->approved) {
                $res['is_success'] = 1;
                $res['transaction_status'] = $transactionResponse->approved;
                $res['invoice_id'] = $transactionResponse->transaction_id;
                $res['order_number'] = $transactionResponse->authorization_code;
                $res['amount'] = $user['amount'];
                $res['paid_amount'] = $transactionResponse->amount;
                $res['dollar_amount'] = $user['dollar_amount'];
                $res['currency_code'] = $user['currency_code'];
                $res['currency_symbol'] = $user['currency_symbol'];
                $res['response_text'] = json_encode($transactionResponse);
            } else {
                $res['is_success'] = 0;
                $res['amount'] = 0;
                $res['response_text'] = json_encode($transactionResponse);
            }
        } else {
            $res['is_success'] = 0;
            $res['amount'] = 0;
            $res['response_text'] = json_encode($response);
        }
        
        return $res;
    }
    
    /**
     * 
     * Cancel Customer's Account
     * @param object $usersub
     * @return object
     * @author Sunil Kund <sunil@muvi.com>
     */
    function cancelCustomerAccount($usersub = Null, $card_info = Null) {
        self::initializePaymentGateway();
        //$fp = fopen($_SERVER['DOCUMENT_ROOT']."/".SUB_FOLDER.'protected/runtime/paymentgateway.log', "a+");
        
        $data = '';
        if (isset($usersub->profile_id) && !empty($usersub->profile_id)) {
            $request = new AuthorizeNetCIM($this->PAYMENT_GATEWAY_API_USER['authorizenet'], $this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);
            
            // Delete payment profile.
            $response = $request->deleteCustomerPaymentProfile($usersub->profile_id, $card_info->token);
            
            //fwrite($fp, "Dt: ".date('d-m-Y H:i:s').'-----'.$usersub->profile_id.'-----'. $card_info->token);
            
            if ($response->isOk()) {
                // Delete the profile id
                $cstresponse = $request->deleteCustomerProfile($usersub->profile_id);
                if ($cstresponse->isOk()) {
                    $data = $cstresponse;
                }
            }
        }
        
        return $data;
    }
    
    /**
     * 
     * Save Customer's card detail
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function saveCard($usersub, $arg) {
        self::initializePaymentGateway();
        
        if (isset($usersub->profile_id) && !empty($usersub->profile_id)) {
            $arg['exp'] = ($arg['exp_month'] < 10) ? '0' . $arg['exp_month'] . substr($arg['exp_year'], -2) : $arg['exp_month'] . substr($arg['exp_year'], -2);
            $arg['expirationDate'] = ($arg['exp_month'] < 10) ? $arg['exp_year'].'-'.'0' . $arg['exp_month']  : $arg['exp_year'].'-'.$arg['exp_month'];
            
            $existingcustomerprofileid = $usersub->profile_id;
            
            // Common setup for API credentials  
            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
            $merchantAuthentication->setName($this->PAYMENT_GATEWAY_API_USER['authorizenet']);
            $merchantAuthentication->setTransactionKey($this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);
            
            // Create the payment data for a credit card
            $creditCard = new AnetAPI\CreditCardType();
            $creditCard->setCardNumber($arg['card_number']);
            $creditCard->setExpirationDate($arg['expirationDate']);
            $creditCard->setCardCode($arg['cvv']);
            
            $paymentCreditCard = new AnetAPI\PaymentType();
            $paymentCreditCard->setCreditCard($creditCard);
            
            // Create a new Customer Payment Profile
            $paymentprofile = new AnetAPI\CustomerPaymentProfileType();
            $paymentprofile->setCustomerType('individual');
            $paymentprofile->setPayment($paymentCreditCard);

            // Submit a CreateCustomerPaymentProfileRequest to create a new Customer Payment Profile
            $paymentprofilerequest = new AnetAPI\CreateCustomerPaymentProfileRequest();
            $paymentprofilerequest->setMerchantAuthentication($merchantAuthentication);
            $paymentprofilerequest->setCustomerProfileId( $existingcustomerprofileid );
            $paymentprofilerequest->setPaymentProfile( $paymentprofile );
            
            if ($this->IS_LIVE_API_PAYMENT_GATEWAY['authorizenet'] != 'sandbox') {
                $paymentprofilerequest->setValidationMode('liveMode');
            } else {
                $paymentprofilerequest->setValidationMode('testMode');
            }
            
            $controller = new AnetController\CreateCustomerPaymentProfileController($paymentprofilerequest);
            if ($this->IS_LIVE_API_PAYMENT_GATEWAY['authorizenet'] != 'sandbox') {
                $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
            } else {
                $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
            }
            
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") ) {
                $allresp = explode(',', $response->getValidationDirectResponse());
                $card_last_fourdigit = str_replace(substr($arg['card_number'], 0, strlen($arg['card_number']) - 4), str_repeat("#", strlen($arg['card_number']) - 4), $arg['card_number']);

                $res['isSuccess'] = 1;
                $res['card']['code'] = 200;
                $res['card']['status'] = 'succeeded';
                $res['card']['card_type'] = isset($allresp['51']) ? $allresp['51'] : ''; //Card type
                $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                $res['card']['token'] = $response->getCustomerPaymentProfileId(); //Stripe card id which is used to get card detail
                $res['card']['response_text'] = serialize($response);
            } else {
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = serialize($response);
                $res['Message'] = $response->getMessages()->getMessage()[0]->getText();
            }
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 404;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }
        
        return $res;
    }
    
    /**
     * 
     * Make default card where subscription will charge
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function defaultCard($usersub, $card) {
        self::initializePaymentGateway();
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    
    /**
     * 
     * Delete Customer's card
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function deleteCard($usersub, $card = Null) {
        self::initializePaymentGateway();
        
        if (isset($card) && !empty($card)) {
            // Common setup for API credentials
            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
            $merchantAuthentication->setName($this->PAYMENT_GATEWAY_API_USER['authorizenet']);
            $merchantAuthentication->setTransactionKey($this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);

            // An existing payment profile ID for this Merchant name and Transaction key
            $customerprofileid = $usersub->profile_id;
            $customerpaymentprofileid = $card->token;

            $request = new AnetAPI\DeleteCustomerPaymentProfileRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setCustomerProfileId($customerprofileid);
            $request->setCustomerPaymentProfileId($customerpaymentprofileid);
            $controller = new AnetController\DeleteCustomerPaymentProfileController($request);
            
            if ($this->IS_LIVE_API_PAYMENT_GATEWAY['authorizenet'] != 'sandbox') {
                $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
            } else {
                $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
            }
            
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                $res['isSuccess'] = 1;
             } else {
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = serialize($response);
                $res['Message'] = $response->getMessages()->getMessage()[0]->getText();
            }
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 404;
            $res['Message'] = 'We are not able to delete your credit card. Please try again.';
        }
        
        return json_encode($res);
    }
    
    function sampleIntegrationTransaction($user = array()) {
        $res = array();
        
        if (isset($user['card_number']) && !empty($user['card_number'])) {
            self::initializePaymentGateway();
            $user['amount'] = Yii::app()->billing->currencyConversion('USD', $user['currency_code'], $user['amount']);
            $data['amount'] = $user['amount'];//As Authorizenet is not processed with 0 amount, so here we transact a min amount and once authentication of card has done, cancel or refund that amount.
            $data['card_number'] = $user['card_number'];
            $data['exp'] = ($user['exp_month'] < 10) ? '0' . $user['exp_month'] . substr($user['exp_year'], -2) : $user['exp_month'] . substr($user['exp_year'], -2);
            $data['expirationDate'] = ($user['exp_month'] < 10) ? substr($user['exp_year'], -2).'-'.'0' . $user['exp_month']  : substr($user['exp_year'], -2).'-'.$user['exp_month'];
            $data['email'] = $user['email'];
            
            $request = new AuthorizeNetCIM($this->PAYMENT_GATEWAY_API_USER['authorizenet'], $this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);
                
            // Create new customer profile
            $customerProfile                     = new AuthorizeNetCustomer;
            $customerProfile->description        = "Creation of Test customer";
            $customerProfile->merchantCustomerId = time();
            $customerProfile->email              = $data['email'];

            // Add payment profile.
            $paymentProfile = new AuthorizeNetPaymentProfile;
            $paymentProfile->customerType = "individual";
            $paymentProfile->payment->creditCard->cardNumber = $data['card_number'];
            $paymentProfile->payment->creditCard->expirationDate = $data['expirationDate'];
            $customerProfile->paymentProfiles[] = $paymentProfile;

            $cpresponse = $request->createCustomerProfile($customerProfile);
            if ($cpresponse->isOk()) {
                $customerProfileId = $cpresponse->getCustomerProfileId();
                $paymentProfileId = $cpresponse->getCustomerPaymentProfileIds();
                
                $auth = new AuthorizeNetAIM($this->PAYMENT_GATEWAY_API_USER['authorizenet'], $this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);
                $auth->setFields(
                    array(
                    'amount' => $data['amount'],
                    'card_num' => $data['card_number'],
                    'exp_date' => $data['exp']
                    )
                );
                $response = $auth->authorizeAndCapture();
                if ($response->approved) {
                    // Delete payment profile.
                    $delResponse = $request->deleteCustomerPaymentProfile($customerProfileId, $paymentProfileId);

                    if ($delResponse->isOk()) {
                        // Delete the profile id
                        $cstresponse = $request->deleteCustomerProfile($customerProfileId);
                        if ($cstresponse->isOk()) {
                            $data = $cstresponse;
                        }
                    }
                } else {
                    $res['isSuccess'] = 0;
                    $res['error_message'] = $response->response_reason_text;
                }

                $res['isSuccess'] = 1;
            } else {
                $res['isSuccess'] = 0;
                $res['error_message'] = "User authentication failed due to invalid authentication values.";
            }
        } else {
            $res['isSuccess'] = 0;
            $res['error_message'] = 'We are not able to process your credit card. Please try another card.';
        }
        
        return $res;
    }
    
    /**
     * 
     * Update user profile
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function updatePaymentProfile($user = array(),$sub = array(),$email = '')
    {
        $res = array();
        self::initializePaymentGateway();
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($this->PAYMENT_GATEWAY_API_USER['authorizenet']);
        $merchantAuthentication->setTransactionKey($this->PAYMENT_GATEWAY_API_PASSWORD['authorizenet']);
        
        //$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/PaymentProfile.log', "a+");
        //fwrite($fp, "Dt: " . date('d-m-Y H:i:s') . "-- Studio ID:-" . $user['studio_id']);
        
        $updatecustomerprofile = new AnetAPI\CustomerProfileExType();
        $updatecustomerprofile->setCustomerProfileId($sub->profile_id);
        $updatecustomerprofile->setDescription("Updated existing Profile Request");
        $updatecustomerprofile->setEmail($email);
        
        //fwrite($fp, "\n".print_r($updatecustomerprofile, TRUE));
        
        $request = new AnetAPI\UpdateCustomerProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setProfile($updatecustomerprofile);
        //fwrite($fp, "\n".print_r($request, TRUE));
        
        $controller = new AnetController\UpdateCustomerProfileController($request);
        if ($this->IS_LIVE_API_PAYMENT_GATEWAY['authorizenet'] != 'sandbox') {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
        } else {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        }
        
        //fwrite($fp, "\n".print_r($response, TRUE));
        
        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
        {
            $res['isSuccess'] = 1;
        }else{
            $res['isSuccess'] = 0;
        }
        return $res;
    }
}

?>