<?php
class NotificationController extends Controller
{
    public function actionIdealNotification() {
        
        /* Write all the response from Adyen to a log fle*/
        $logfile = dirname(__FILE__).'/adyen.txt';
        $msg = "\n----------Log File: NotificationController: actionIdealNotification()----------\n";
        $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
        $data_log = serialize($_REQUEST);
        $msg.= $data_log."\n";
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
        /*end*/
        
        if(isset($_REQUEST) && !empty($_REQUEST) && trim($_REQUEST['eventCode']) == 'AUTHORISATION'){
            $log_data = PciLog::model()->findByAttributes(array('unique_id' => $_REQUEST['merchantReference']));
            $data = json_decode($log_data['log_data'], true);
            if(isset($data) && !empty($data)){
                $studio_id = $data['studio_id'];
                $user_id = $data['user_id'];
                $gateway_code = $data["payment_method"];
                $plan_id = $data['plan_id'];
                $ip_address = CHttpRequest::getUserHostAddress();
                $planModel = new PpvPlans();
                $plan = $planModel->findByPk($plan_id);

                if (isset($data['is_advance_purchase']) && intval($data['is_advance_purchase'])) {
                    $isadv = $data['is_advance_purchase'];
                } else {
                    $isadv = $data['isadv'];
                }
                $Films = Film::model()->findByAttributes(array('studio_id' => $data['studio_id'], 'uniq_id' => $data['movie_id']));
                $video_id = $Films->id;


                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);
                $VideoDetails = $VideoName;
                $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if ($data['season_id'] == 0) {
                        $VideoDetails .= ', All Seasons';
                    } else {
                        $VideoDetails .= ', Season ' . $data['season_id'];
                        if ($data['episode_id'] == 0) {
                            $VideoDetails .= ', All Episodes';
                        } else {
                            $episodeName = Yii::app()->common->getEpisodename($streams->id);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', ' . $episodeName;
                        }
                    }
                }
                //Save ppv subscription detail
                $ppvsubscription = new PpvSubscription;
                $ppvsubscription->user_id = $user_id;
                $ppvsubscription->studio_id = $studio_id;
                $ppvsubscription->ppv_plan_id = $plan_id;
                $ppvsubscription->studio_payment_gateway_id = $data['studio_payment_gateway_id'];
                $ppvsubscription->token = Yii::app()->common->generateUniqNumber();
                $ppvsubscription->amount = $data['amount'];
                $ppvsubscription->start_date = $data['start_date'];
                $ppvsubscription->end_date = $data['end_date'];
                $ppvsubscription->status = 1;
                $ppvsubscription->movie_id = $Films->id;
                $ppvsubscription->season_id = $data['season_id'];
                $ppvsubscription->episode_id = $data['episode_id'];
                $ppvsubscription->is_advance_purchase = $isadv;
                $ppvsubscription->is_ppv_bundle = $data['is_bundle'];
                $ppvsubscription->timeframe_id = $data['timeframe_id'];
                $ppvsubscription->currency_id = $data['currency_id'];
                $ppvsubscription->coupon_code = $data['coupon'];
                $ppvsubscription->ip = $ip_address;
                $ppvsubscription->created_by = $user_id;
                $ppvsubscription->created_date = new CDbExpression("NOW()");
                $ppvsubscription->view_restriction = @$data['view_restriction'];
                $ppvsubscription->watch_period = @$data['watch_period'];
                $ppvsubscription->access_period = @$data['access_period'];
                $ppvsubscription->save();
                $ppv_subscription_id = $ppvsubscription->id;

                //Save a transaction detail
                $transaction = new Transaction;
                $transaction->user_id = $user_id;
                $transaction->studio_id = $studio_id;
                $transaction->plan_id = $plan_id;
                $transaction->currency_id = $data['currency_id'];
                $transaction->transaction_date = new CDbExpression("NOW()");
                $transaction->payment_method = $gateway_code;
                $transaction->transaction_status = $_REQUEST['eventCode'];
                $transaction->invoice_id = $_REQUEST['additionalData_iDealTransactionId'];
                $transaction->order_number = $_REQUEST['pspReference'];
                $transaction->amount = $data['amount'];
                $transaction->dollar_amount = $data['amount'];
                $transaction->user_reference = $data['shopper_reference'];
                $transaction->payer_id = $_REQUEST['additionalData_iDealConsumerIBAN'];
                $transaction->ppv_subscription_id = $ppv_subscription_id;
                $transaction->movie_id = $Films->id;
                $transaction->ip = $ip_address;
                $transaction->created_date = new CDbExpression("NOW()");
                $tType = 2;
                if (isset($isadv) && intval($isadv)) {
                    $tType = 3;
                } elseif (isset($data['is_bundle']) && intval($data['is_bundle'])) {
                    $tType = 5;
                }

                $transaction->transaction_type = $tType;
                $transaction->save();
                $transaction_id = $transaction->id;
                $couponDetails = Coupon::model()->findByAttributes(array('coupon_code' => @$data['coupon']));
            if (isset($couponDetails) && !empty($couponDetails)) {
                if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                        $qry = "UPDATE coupon SET used_by='" . $couponDetails->used_by . "," . $user_id . "',used_date = NOW() WHERE coupon_code='" . $data['coupon'] . "'";
                } else {
                        $qry = "UPDATE coupon SET used_by=" . $user_id . ",used_date = NOW() WHERE coupon_code='" . $data['coupon'] . "'";
                }
                    Yii::app()->db->createCommand($qry)->execute();
            }
            
                PciLog::model()->deleteByPk($log_data['id']);
                $trans_data['amount'] = $data['amount'];
                $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
                
            }
        }
        echo "accepted";
    }
}