<?php

class PartnersController extends Controller {

    public $defaultAction = 'index';
    public $headerinfo = '';
    public $layout = 'partners';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = 'admin';
        
        if(in_array($_SERVER['SERVER_NAME'], array('www.muvi.com','muvi.com'))){
            $this->redirect('https://partners.muvi.com');
            exit();
        }
        if (!(Yii::app()->user->id) || !isset(Yii::app()->user->role_id)) {
            $b4Login = array('index');
            if (!in_array(Yii::app()->controller->action->id, $b4Login)) {
                Yii::app()->user->setFlash('error', 'Login to access the page.');
                $this->redirect(Yii::app()->getbaseUrl(true) . '/partners');
                exit();
            }
        } else {
            if(isset(Yii::app()->user->role_id)){
                $b4Login = array('index');
                if (in_array(Yii::app()->controller->action->id, $b4Login)) {
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/partners/video');
                    exit();
                }
            }else{
                Yii::app()->user->logout();
                Yii::app()->session->clear();
                Yii::app()->session->destroy();
                Yii::app()->user->setFlash('error', 'Login to access the page.');
                $this->redirect(Yii::app()->getbaseUrl(true) . '/partners');
                exit();
            }    
        }
        return true;
    }

    function actionIndex() {
        $this->render('index');       
    }

    function actionLogout() {
        Yii::app()->user->logout();
        Yii::app()->session->clear();
        Yii::app()->session->destroy();

        $this->redirect(Yii::app()->getbaseUrl(true) . '/partners');
    }

    public function actionAccount() {
        $studio = $this->studio;
        
        $this->pageTitle = ucwords($studio->name)." | Account Info";
        $this->breadcrumbs = array('Settings' => array('partners/account'), 'Account Info');

        (array) $user = User::model()->findByPk(Yii::app()->user->id);

        $this->render('account', array('userdata' => $user->attributes, 'studio' => $studio));
    }

    public function actionSavepassword() {
        $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : '';
        $new_password = isset($_REQUEST['new_password']) ? $_REQUEST['new_password'] : '';
        $conf_password = isset($_REQUEST['conf_password']) ? $_REQUEST['conf_password'] : '';
        $studio = $this->studio;
        $studio_id = $studio->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $enc = new bCrypt();

        $model = new User;
        $model = User::model()->findByAttributes(array('id' => Yii::app()->user->id, 'studio_id' => $studio_id));

        if ($enc->verify($password, $model->encrypted_password) && $new_password != '' && strcmp($new_password, $conf_password) == 0) {
            $email = $model->email;
            $pass = $model->encrypt($new_password);
            $model->encrypted_password = $pass;
            $model->save();

            $usr = new SdkUser();
            $usr = $usr->findByAttributes(array('studio_id' => $studio_id, 'email' => $email));
            if ($usr) {
                $usr->encrypted_password = $pass;
                $usr->save();
            }

            Yii::app()->user->setFlash('success', 'Your password has been updated successfully!');
            $url = $this->createUrl('partners/account');
            $this->redirect($url);
            exit;
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in updating your password.');
            $url = $this->createUrl('partners/account');
            $this->redirect($url);
            exit;
        }
    }

    public function actionVideo() {
        $studio = $this->studio;
        
        $this->pageTitle = ucwords($studio->name)." | Content Analytics";
        $this->breadcrumbs = array('Analytics', 'Content Analytics');
        $this->headerinfo = "Content Analytics";
        
        $page_size = 20;

        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        $default_currency = $studio->default_currency_id;
        $currency = Yii::app()->common->getStudioCurrency();
        $c = new Currency();
        $currency_details = $c->findByPk($default_currency);
        
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $content = Yii::app()->general->getPartnersContentIds();
        $watchedHour = VideoLogs::model()->getTotalWatchedHour($studio->id, $content['movie_id'],0,$dt,1,$searchKey,$deviceType);
        $bandwidth = VideoLogs::model()->getTotalBandwidth($studio->id, $dt,$content['movie_id'],1,$searchKey,$deviceType);
	$bufferDuration = BufferLogs::model()->getTotalBufferDuration($studio->id,$content['movie_id'],0,$dt,$searchKey,$deviceType,1);
        $netWatchedHour = VideoLogs::model()->getNetWatchedHour($studio->id,$dt,$content['movie_id'],1,$searchKey,$deviceType);
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        $total_bandwidth = Yii::app()->aws->getLocationBandwidthSize($studio->id, $start_date, $end_date, $deviceType, 1,$content['movie_id'],1);
        
        $json_data['watched_hour'] = $netWatchedHour['watched_hour'];
        $json_data['bandwidth'] = $total_bandwidth['raw_bandwidth'];;
        $json_data['buffer_duration'] = $bufferDuration;
        
        if (isset($_REQUEST['dt'])) {
            echo json_encode($json_data);
            exit;
        } else {
            $allWatchedHour = VideoLogs::model()->getAllWatchedHour($studio->id,$content['movie_id'],1);
            $allBandwidth = VideoLogs::model()->getAllBandwidth($studio->id,$content['movie_id'],1);
            $viewsDetails='';//$viewsDetails = $this->actionGetMonthlyViews();
            foreach ($allBandwidth as $row) {
                $date = date('Y-m', strtotime($row['created_date']));
                $x[] = $date;
                if (in_array($date, $x)) {
                    $bandwidthArr[$date] +=(float) $row['total_buffered_size'] / 1024 / 1024;
                } else {
                    $bandwidthArr[$date] = (float) $row['total_buffered_size'] / 1024 / 1024;
                }
            }
            foreach ($allWatchedHour as $row) {
                $date = date('Y-m', strtotime($row['created_date']));
                $x[] = $date;
                if (in_array($date, $x)) {
                    $watchArr[$date] +=number_format((float) (($row['played_length'])), 2, '.', '');
                } else {
                    $watchArr[$date] = number_format((float) (($row['played_length'])), 2, '.', '');
                }
            }

            $lunchDate = $this->studio->created_dt;
            $lmonth = date('n', strtotime($lunchDate));
            $lyear = date('Y', strtotime($lunchDate));
            $arr = array();
            for ($i = date('Y'); $i >= $lyear; $i--) {
                if ($i == date('Y')) {
                    $j = date('n');
                } else {
                    $j = 12;
                }
                for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                    $mont = $j < 10 ? '0' . $j : $j;
                    $arr['watch'][] = isset($watchArr[$i . "-" . $mont]) ? $watchArr[$i . "-" . $mont] : 0;
                    $arr['bandwidth'][] = isset($bandwidthArr[$i . "-" . $mont]) ? $bandwidthArr[$i . "-" . $mont] : 0;
                    $xdata[] = date('F', mktime(0, 0, 0, $j, 10));
                }
            }
            $watchgraph[] = array('name' => ucfirst('Watch Hours'), 'data' => array_reverse($arr['watch']));
            $bandwidthgraph[] = array('name' => ucfirst('Bandwidth Consumed'), 'data' => array_reverse($arr['bandwidth']));
            $graphData = json_encode($watchgraph);
            $bandwidthGraphData = json_encode($bandwidthgraph);
            $this->render('video', array('watchedHour' => $watchedHour, 'bandwidth' => $bandwidth, 'viewsDetails' => $viewsDetails, 'xdata' => json_encode(array_reverse($xdata)), 'graphData' => $graphData, 'bandwidthGraphData' => $bandwidthGraphData,'bufferDuration'=>$bufferDuration ,'page_size' => $page_size,'lunchDate'=>$lunchDate,'default_currency'=>$default_currency,'currency'=>$currency,'currency_details'=>$currency_details,'movie_id'=>$content['movie_id']));
            //$this->render('video', array('watchedHour' => $watchedHour,'viewsDetails' => $viewsDetails, 'xdata' => json_encode(array_reverse($xdata)), 'graphData' => $graphData, 'page_size' => $page_size,'lunchDate'=>$lunchDate,'movie_id'=>$content['movie_id']));
        }        
    }

    public function actionGetMonthlyViews() {
        Yii::import('application.controllers.ReportController');
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        }else{
            $currency_id = $studio->default_currency_id;
        }
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $content = Yii::app()->general->getPartnersContentIds();
        
           $movie_idd=$content['movie_id'];
            $video_data= VideoLogs::model()->getVideoAllViewDetails($dt,$offset,$page_size,$movie_idd,1,$searchKey,$deviceType);
            $studio_id=Yii::app()->user->studio_id;
            $partner_id= Yii::app()->user->id;
            $partnersharing="select percentage_revenue_share from partners_contents where studio_id='".$studio_id."' AND user_id='".$partner_id."' group by user_id";
            $datapartnersharing = Yii::app()->db->createCommand($partnersharing)->queryAll();
            $percentageshare=$datapartnersharing[0]['percentage_revenue_share'];
        foreach($video_data['data'] as $key=>$pricedata){
            $contentTypes=$pricedata['content_type'];
         if($pricedata['content_types_id']==3)
         {
         $movie_id=$pricedata['movie_id'];
         $video_id=$pricedata['video_id'];
         $transaction_data = VideoLogs::model()->getPpvTransactionDetails($dt,$currency_id,$studio_id,$video_id);
         $bandwidth_data = VideoLogs::model()->getVideoBandwidthDetailsvideo($dt,$video_id,$deviceType,$contentTypes,$movie_id);
         $trailer_data = VideoLogs::model()->getTrailerdata($dt,$movie_id,$studio_id);
 //added for amount field
        // $get_episod_session_id = Coupon::model()->getepisod_and_session($dt,$movie_id,$studio_id);
         
        // print_r($get_episod_session_id);exit;
          $ppv_coupon_data = Coupon::model()->getppvCoupondata_multicontent($dt,$movie_id,$studio_id,($pricedata['episod_id'] != '' && $pricedata['episod_id'] != 0)?$pricedata['episod_id']:0);
          $amount = 0;
         foreach($ppv_coupon_data as $ct => $val){
               if($val[coupon_code] != ''){
                   $coupon_val = Coupon::model()->getppvCouponvalue($val[coupon_code]);
                  if($coupon_val['discount_type'] == 1){
                      $price = ($val['amount']*100)/(100-$coupon_val['discount_amount']);
                      //$price = Yii::app()->common->formatPrice($price,$currency_id)
                      $val[amount] = $price;
                  }else{
                      $val[amount] = $price = $val['amount'] + $coupon_val['cash_amount'];
                  }
                  
               }
               $amount += $val[amount];  
           }
         $amount = Yii::app()->common->formatPrice($amount,$currency_id);
         $video_data['data'][$key]['total_amount'] = $amount; 
//added for amount field end
         $trailer_data['viewcount'];
         $video_data['data'][$key]['trailercount'] = $trailer_data['viewcount'];
         $video_data['data'][$key]['bandwidthdata'] = $bandwidth_data['bandwidth'];        
         $video_data['data'][$key]['transactions'] = $transaction_data['transactioncount'];
         $video_data['data'][$key]['revenue'] = trim($transaction_data['transactionamt'])? Yii::app()->common->formatPrice(($transaction_data['transactionamt']*($percentageshare/100)),$currency_id) : Yii::app()->common->formatPrice(0,$currency_id); 
         }
         else
         {
         $movie_id=$pricedata['movie_id'];
         $transaction_data = VideoLogs::model()->getPpvTransactionDetailsmovie($dt,$currency_id,$studio_id,$movie_id);
         $bandwidth_data = VideoLogs::model()->getVideoBandwidthDetails($dt,$movie_id,$deviceType,$contentTypes);
         $trailer_data = VideoLogs::model()->getTrailerdata($dt,$movie_id,$studio_id);
//added for amount field 
         $ppv_coupon_data = Coupon::model()->getppvCoupondata($dt,$movie_id,$studio_id);
         $amount = 0;
         foreach($ppv_coupon_data as $ct => $val){
               if($val[coupon_code] != ''){
                   $coupon_val = Coupon::model()->getppvCouponvalue($val[coupon_code]);
                  if($coupon_val['discount_type'] == 1){
                      $price = ($val['amount']*100)/(100-$coupon_val['discount_amount']);
                      //$price = Yii::app()->common->formatPrice($price,$currency_id)
                      $val[amount] = $price;
                  }else{
                      $val[amount] = $price = $val['amount'] + $coupon_val['cash_amount'];
                  }
                  
               }
               $amount += $val[amount];  
           }
         $amount = Yii::app()->common->formatPrice($amount,$currency_id);
         $video_data['data'][$key]['total_amount'] = $amount; 
//added for amount field end
         $video_data['data'][$key]['trailercount'] = $trailer_data['viewcount'];
         $video_data['data'][$key]['bandwidthdata'] = $bandwidth_data['bandwidth'];
         $video_data['data'][$key]['transactions'] = $transaction_data['transactioncount'];
         $video_data['data'][$key]['revenue'] = trim($transaction_data['transactionamt'])? Yii::app()->common->formatPrice(($transaction_data['transactionamt']*($percentageshare/100)),$currency_id) : Yii::app()->common->formatPrice(0,$currency_id); 
         }
        if($pricedata['ppv_plan_id']!=0){ 
        if($pricedata['content_types_id']!=3){
        $price = VideoLogs::model()->videoPriceDetails($pricedata['movie_id'],$pricedata['ppv_plan_id'],$currency_id);
        if(isset($price) && !empty($price)){
          $video_data['data'][$key]['subscribe_price']= $price[0]['price_for_subscribed']; 
          $video_data['data'][$key]['nonsubscribe_price']= $price[0]['price_for_unsubscribed']; 
          $video_data['data'][$key]['is_advance_purchase']= $price[0]['is_advance_purchase']; 
        }
        }
        elseif($pricedata['content_types_id']==3){
        $price = VideoLogs::model()->videoPriceDetailsMultipart($pricedata['movie_id'],$pricedata['ppv_plan_id'],$currency_id);
      
        if(isset($price) && !empty($price)){
          $video_data['data'][$key]['subscribe_price']= $price[0]['episode_subscribed']; 
          $video_data['data'][$key]['nonsubscribe_price']= $price[0]['episode_unsubscribed']; 
          $video_data['data'][$key]['is_advance_purchase']= $price[0]['is_advance_purchase']; 
        } 
        }
        }
         else
         {
         $price = VideoLogs::model()->videoPriceDetailsadvance($pricedata['movie_id'],$currency_id);
        if((isset($price) && !empty($price)) && (($price[0]['is_advance_purchase']==1))){
          $video_data['data'][$key]['subscribe_price']= $price[0]['price_for_subscribed']; 
          $video_data['data'][$key]['nonsubscribe_price']= $price[0]['price_for_unsubscribed']; 
          $video_data['data'][$key]['is_advance_purchase']= $price[0]['is_advance_purchase']; 
        }    
        }
       
        
        }       
        $datappv = Yii::app()->general->monetizationMenuSetting($studio_id);
        //$views = VideoLogs::model()->getViewsDetails($dt,$offset,$page_size,$content['movie_id'],1,$searchKey,$deviceType);
        
        //print_r($video_data);
        //exit;
       /* $views = '';
        $content_data = VideoLogs::model()->getContentData($studio_id,$searchKey,$content['movie_id'],$offset,$page_size,1);
        $season_data = VideoLogs::model()->getSeasonData($studio_id,$searchKey,$content['movie_id'],$offset,$page_size,1);
        $episode_data = VideoLogs::model()->getEpisodeData($studio_id,$searchKey,$content['movie_id'],$offset,$page_size,1);
        $count = $content_data['count'] + $season_data['count'] + $episode_data['count'];
        $finalArray = array();
        $i = 0;
         $studio_id=Yii::app()->user->studio_id;
            $partner_id= Yii::app()->user->id;
            $partnersharing="select percentage_revenue_share from partners_contents where studio_id='".$studio_id."' AND user_id='".$partner_id."' group by user_id";
            $datapartnersharing = Yii::app()->db->createCommand($partnersharing)->queryAll();
          $percentageshare=$datapartnersharing[0]['percentage_revenue_share'];
        if(isset($content_data['data']) && !empty($content_data['data'])){
            foreach($content_data['data'] as $content){
                $j = 1;
                if($content['content_types_id'] == 3){
                    $type_str = '';
                    if($j){
                        $price = ReportController::getPrice($content['is_converted'],$content['movie_id'],$content['content_types_id'],$content['ppv_plan_id'],$currency_id);
                        $transaction_data = VideoLogs::model()->getPpvContentTransactionDetailsContent($dt,$currency_id,$content['movie_id'],0,0,1);
                        $content['transactions'] = $transaction_data['count']['transactioncount'];
                        foreach ($transaction_data['type'] as $type){
                            if(isset($type['is_advance_purchase']) && ($type['is_advance_purchase']==1)){
                                if (!preg_match('/APV/', $type_str)) {
                                    $type_str .= 'APV-';
                                }
                            }else{
                                if (!preg_match('/PPV/', $type_str)) {
                                    $type_str .= 'PPV-';
                                }
                            }
                        }
                        $content['transaction_type'] = trim($type_str) ? rtrim($type_str,'-') : 'Other';
                        $content['price'] = Yii::app()->common->formatPrice($price['show_unsubscribed'],$currency_id).'/'.Yii::app()->common->formatPrice($price['show_subscribed'],$currency_id);
                        $content['revenue'] = trim($transaction_data['count']['amount'])? Yii::app()->common->formatPrice(($transaction_data['count']['amount']*($percentageshare/100)),$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                        $finalArray[$i] = $content;
                        $j = 0;
                        $i++;
                    }
                    if(isset($season_data['data']) && !empty($season_data['data'])){
                        foreach ($season_data['data'] as $season) {
                            $type_str = '';
                            $price = ReportController::getPrice($content['is_converted'],$content['movie_id'],$content['content_types_id'],$content['ppv_plan_id'],$currency_id);
                            $transaction_data = VideoLogs::model()->getPpvContentTransactionDetailsContent($dt,$currency_id,$content['movie_id'],$season['series_number'],0,1);
                            foreach ($transaction_data['type'] as $type){
                                if(isset($type['is_advance_purchase']) && ($type['is_advance_purchase']==1)){
                                    if (!preg_match('/APV/', $type_str)) {
                                        $type_str .= 'APV-';
                                    }
                                }else{
                                    if (!preg_match('/PPV/', $type_str)) {
                                        $type_str .= 'PPV-';
                                    }
                                }
                            }
                            $finalArray[$i]['movie_id'] = $content['movie_id'];
                            $finalArray[$i]['name'] = $content['name'].' - Season - '.$season['series_number'];
                            $finalArray[$i]['content_types_id'] = $content['content_types_id'];
                            $finalArray[$i]['series_number'] = $season['series_number'];
                            $finalArray[$i]['episode_title'] = '';
                            $finalArray[$i]['episode_number'] = '';
                            $finalArray[$i]['transactions'] = $transaction_data['count']['transactioncount'];
                            $finalArray[$i]['transaction_type'] = trim($type_str) ? rtrim($type_str,'-') : 'Other';
                            $finalArray[$i]['price'] = Yii::app()->common->formatPrice($price['season_unsubscribed'],$currency_id).'/'.Yii::app()->common->formatPrice($price['season_subscribed'],$currency_id);
                            $finalArray[$i]['revenue'] = trim($transaction_data['count']['amount'])? Yii::app()->common->formatPrice(($transaction_data['count']['amount']*($percentageshare/100)),$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                            $i++;
                        }
                    }
                    if(isset($episode_data['data']) && !empty($episode_data['data'])){
                        foreach ($episode_data['data'] as $episode) {
                            $type_str = '';
                            $price = ReportController::getPrice($content['is_converted'],$content['movie_id'],$content['content_types_id'],$content['ppv_plan_id'],$currency_id);
                            $transaction_data = VideoLogs::model()->getPpvContentTransactionDetailsContent($dt,$currency_id,$content['movie_id'],$episode['series_number'],$episode['stream_id'],1);
                            
                            
                            foreach ($transaction_data['type'] as $type){
                                if(isset($type['is_advance_purchase']) && ($type['is_advance_purchase']==1)){
                                    if (!preg_match('/APV/', $type_str)) {
                                        $type_str .= 'APV-';
                                    }
                                }else{
                                    if (!preg_match('/PPV/', $type_str)) {
                                        $type_str .= 'PPV-';
                                    }
                                }
                            }
                            $finalArray[$i]['movie_id'] = $content['movie_id'];
                            $finalArray[$i]['name'] = $content['name'].' - Season - '.$episode['series_number'].' - Episode - '.$episode['episode_title'];
                            $finalArray[$i]['content_types_id'] = $content['content_types_id'];
                            $finalArray[$i]['series_number'] = $episode['series_number'];
                            $finalArray[$i]['episode_title'] = $episode['episode_title'];
                            $finalArray[$i]['episode_number'] = $episode['episode_number'];
                            $finalArray[$i]['transactions'] = $transaction_data['count']['transactioncount'];
                            $finalArray[$i]['transaction_type'] = trim($type_str) ? rtrim($type_str,'-') : 'Other';
                            $finalArray[$i]['price'] = Yii::app()->common->formatPrice($price['episode_unsubscribed'],$currency_id).'/'.Yii::app()->common->formatPrice($price['episode_subscribed'],$currency_id);
                            $finalArray[$i]['revenue'] = trim($transaction_data['count']['amount'])? Yii::app()->common->formatPrice(($transaction_data['count']['amount']*($percentageshare/100)),$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                            $i++;
                        }
                    }
                    
                }else{
                    $type_str = '';
                    $price = ReportController::getPrice($content['is_converted'],$content['movie_id'],$content['content_types_id'],$content['ppv_plan_id'],$currency_id);
                    $transaction_data = VideoLogs::model()->getPpvContentTransactionDetails($dt,$currency_id,$content['movie_id'],0,0,1);
                    $content['transactions'] = $transaction_data['count']['transactioncount'];
                    foreach ($transaction_data['type'] as $type){
                        if(isset($type['is_advance_purchase']) && ($type['is_advance_purchase']==1)){
                            if (!preg_match('/APV/', $type_str)) {
                                $type_str .= 'APV-';
                            }
                        }else{
                            if (!preg_match('/PPV/', $type_str)) {
                                $type_str .= 'PPV-';
                            }
                        }
                    }
                    $content['transaction_type'] = trim($type_str) ? rtrim($type_str,'-') : 'Other';
                    $content['price'] = Yii::app()->common->formatPrice($price['price_for_unsubscribed'],$currency_id).'/'.Yii::app()->common->formatPrice($price['price_for_subscribed'],$currency_id);
                    $content['revenue'] = trim($transaction_data['count']['amount'])? Yii::app()->common->formatPrice(($transaction_data['count']['amount']*($percentageshare/100)),$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                    $finalArray[$i] = $content;
                }
                $i++;
            }
        }
        
        $video_table_data = $finalArray;
        $column = 'name';
        $sort = 'SORT_ASC';
        if(isset($_REQUEST['column']) && trim($_REQUEST['column'])){
            $column = $_REQUEST['column'];
        }
        if(isset($_REQUEST['sort']) && trim($_REQUEST['sort'])){
            $sort = $_REQUEST['sort'];
        }
        
        if($sort  == 'desc'){
            usort($video_table_data, ReportController::make_comparer([$column, SORT_DESC]));
        }else{
            usort($video_table_data, ReportController::make_comparer([$column, SORT_ASC]));
        }
        if(trim($deviceType)){
            $video_table_data = ReportController::filterArray($video_table_data, $deviceType, 'top_device');
        }
        $sort_data['column'] = $column;
        $sort_data['sort'] = $sort; 
        * 
        */
        
        if (isset($_REQUEST['dt'])) {
            if (isset($_REQUEST['dt'])) {
                $dt = stripcslashes($_REQUEST['dt']);
                $dt = json_decode($dt);
            } else if (!$dt) {
                $dt = '';
            }
            $this->renderPartial('videoViewData',array('viewsDetails' => $video_data['data'],'transaction_data'=>$transactionData,'trailer_data'=>$trailerData,'video_count'=>$video_count,'bandwidth_data'=>$bandwidthData,'dateRange' => $dt,'currency_id'=>$currency_id,'studio_id'=>$studio_id,'monetization'=>$datappv,'count'=>$video_data['count'],'percentageshare'=>$percentageshare));
        } else {
            return $views;
        }
            
       /* if (isset($_REQUEST['dt'])) {
            if (isset($_REQUEST['dt'])) {
                $dt = stripcslashes($_REQUEST['dt']);
                $dt = json_decode($dt);
            } else if (!$dt) {
                $dt = '';
            }
           
           
            $this->renderPartial('videoViewData',array('viewsDetails' => $video_table_data,'count'=>$count,'dateRange' => $dt,'currency_id'=>$currency_id,'sort_data'=>$sort_data,'percentageshare'=>$percentageshare));
        } else {
            return $views;
        }*/
        exit();
    }

    public function formatKBytes($bytes, $precision = 2) { 
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;
            
        if (($bytes >= 0) && ($bytes < $kilobyte)) {
            return $bytes . ' B';
        } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
            return round($bytes / $kilobyte, $precision) . ' KB';
        } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
            return round($bytes / $megabyte, $precision) . ' MB';
        } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
            return round($bytes / $gigabyte, $precision) . ' GB';
        } elseif ($bytes >= $terabyte) {
            return round($bytes / $terabyte, $precision) . ' TB';
        } else {
            return $bytes . ' B';
        }
    }

    public function actionGetVideoReport() {
        Yii::import('application.controllers.ReportController');
        $studio_id = $this->studio->id;
        $monetization = Yii::app()->general->monetizationMenuSetting($studio_id);
        if((isset($monetization) && (($monetization['menu'] & 2) || ($monetization['menu'] & 16)))){
          $headArr[0] = array('Name','Price','Transactions','Type','Revenue');   
        }
        else{
          $headArr[0] = array('Name','Duration','Total Views','Unique Views','Bandwidth');   
        }
       
        $sheetName = array('Video');
        $sheet = 1;
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        //$deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $studio = $this->studio;
        $partner_content = Yii::app()->general->getPartnersContentIds();
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        }else{
            $currency_id = $studio->default_currency_id;
        }
        //$dt = stripcslashes($_REQUEST['dt']);
       // $dt = json_decode($dt);
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
           $datep = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        $video_data = array();
        $i = 0;
          $content = Yii::app()->general->getPartnersContentIds();
        
           $movie_idd=$content['movie_id'];
             $video_data = VideoLogs::model()->getVideoViewDetailsReport($dt,$movie_idd,1,$searchKey=0,$deviceType=0);
            
            //print_r($video_data);exit;
            //$video_data= VideoLogs::model()->getVideoAllViewDetails($dt,$offset,$page_size,$movie_idd,1,$searchKey,$deviceType);
            $studio_id=Yii::app()->user->studio_id;
            $partner_id= Yii::app()->user->id;
            $partnersharing="select percentage_revenue_share from partners_contents where studio_id='".$studio_id."' AND user_id='".$partner_id."' group by user_id";
            $datapartnersharing = Yii::app()->db->createCommand($partnersharing)->queryAll();
            $percentageshare=$datapartnersharing[0]['percentage_revenue_share'];
        foreach($video_data as $key=>$pricedata){
            $contentTypes=$pricedata['content_type'];
         if($pricedata['content_types_id']==3)
         {
         $movie_id=$pricedata['movie_id'];
         $video_id=$pricedata['video_id'];
         $transaction_data = VideoLogs::model()->getPpvTransactionDetails($datep,$currency_id,$studio_id,$video_id);
         $bandwidth_data = VideoLogs::model()->getVideoBandwidthDetailsvideo($datep,$video_id,$deviceType,$contentTypes,$movie_id,1);
         $trailer_data = VideoLogs::model()->getTrailerdata($datep,$movie_id,$studio_id);
         $trailer_data['viewcount'];
         $video_data[$key]['trailercount'] = $trailer_data['viewcount'];
         $video_data[$key]['bandwidthdata'] = $bandwidth_data['bandwidth'];         
         $video_data['data'][$key]['transactions'] = $transaction_data['transactioncount'];
         $video_data[$key]['revenue'] = $transaction_data['transactionamt'];
         }
         else
         {
         $movie_id=$pricedata['movie_id'];
         $transaction_data = VideoLogs::model()->getPpvTransactionDetailsmovie($datep,$currency_id,$studio_id,$movie_id);
         $bandwidth_data = VideoLogs::model()->getVideoBandwidthDetails($datep,$movie_id,$deviceType,$contentTypes,1);
         $trailer_data = VideoLogs::model()->getTrailerdata($datep,$movie_id,$studio_id);
         $video_data[$key]['trailercount'] = $trailer_data['viewcount'];
         $video_data[$key]['bandwidthdata'] = $bandwidth_data['bandwidth'];
         $video_data[$key]['transactions'] = $transaction_data['transactioncount'];
         $video_data[$key]['revenue'] = $transaction_data['transactionamt'];
         }
        if($pricedata['ppv_plan_id']!=0){ 
        if($pricedata['content_types_id']!=3){
        $price = VideoLogs::model()->videoPriceDetails($pricedata['movie_id'],$pricedata['ppv_plan_id'],$currency_id);
        if(isset($price) && !empty($price)){
          $video_data[$key]['subscribe_price']= $price[0]['price_for_subscribed']; 
          $video_data[$key]['nonsubscribe_price']= $price[0]['price_for_unsubscribed']; 
          $video_data[$key]['is_advance_purchase']= $price[0]['is_advance_purchase']; 
        }
        }
        elseif($pricedata['content_types_id']==3){
        $price = VideoLogs::model()->videoPriceDetailsMultipart($pricedata['movie_id'],$pricedata['ppv_plan_id'],$currency_id);
      
        if(isset($price) && !empty($price)){
          $video_data[$key]['subscribe_price']= $price[0]['episode_subscribed']; 
          $video_data[$key]['nonsubscribe_price']= $price[0]['episode_unsubscribed']; 
          $video_data[$key]['is_advance_purchase']= $price[0]['is_advance_purchase']; 
        } 
        }
        }
         else
         {
         $price = VideoLogs::model()->videoPriceDetailsadvance($pricedata['movie_id'],$currency_id);
        if((isset($price) && !empty($price)) && (($price[0]['is_advance_purchase']==1))){
          $video_data[$key]['subscribe_price']= $price[0]['price_for_subscribed']; 
          $video_data[$key]['nonsubscribe_price']= $price[0]['price_for_unsubscribed']; 
          $video_data[$key]['is_advance_purchase']= $price[0]['is_advance_purchase']; 
        }    
        }
        } 
        $data[0] = array();
        if(isset($video_data) && !empty($video_data)){
            foreach ($video_data as $views){
                 if($views['content_type']!=2){ 
                if(trim($views['name'])){
                     if($views['content_type']==2){
                                  $trailer='(Trailer)';  
                                }
                                else{
                                   $trailer=''; 
                                }
                                
                                if(@$views['content_types_id']==3){
                                   $content_name=$views['name']."-> Season ".$views['series_number']." -> ". ($views['episode_title']?$views['episode_title']:'Episode#-'.$views['episode_number']).$trailer;
                                }else{
                                      $content_name=wordwrap($views['name']).$trailer;
                                }
                }else{
                    $content_name = "Content is removed.";
                }
               $subscribe=trim($views['subscribe_price'])? Yii::app()->common->formatPrice($views['subscribe_price'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                $nonsubscribe=trim($views['nonsubscribe_price'])? Yii::app()->common->formatPrice($views['nonsubscribe_price'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                if($views['content_type']==2){ $transactiontype=0; }else{$transactiontype=$views['transactions']? $transactiontype=$views['transactions'] : 0; }
                if($views['content_type']==2){ $price='N/A'; }else{ if(isset($views['subscribe_price']) && !empty($views['subscribe_price'])){ $price=$subscribe.'/'.$nonsubscribe; }else { $price='N/A';} }
                if($views['content_type']==2){ $type='N/A'; }else{ if(isset($views['is_advance_purchase']) && ($views['is_advance_purchase'])==0){ $type='PPV';}elseif(isset($views['is_advance_purchase']) &&($views['is_advance_purchase'])==1){ $type='PO';}else { $type='N/A';} }
                if($views['content_type']==2){ $revenue= Yii::app()->common->formatPrice(0,$currency_id); }else{ $revenue=$views['revenue']? Yii::app()->common->formatPrice($views['revenue'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id); }
                $duration=$this->timeFormat($views['duration']);
                $viewcount=$views['viewcount'] ? $views['viewcount'] : 0;
                $uniquecount=$views['u_viewcount'] ? $views['u_viewcount'] : 0;
                 $bandwidth_consumed = $this->formatKBytes($views['bandwidthdata']*1024,2);
                 $trailercount=$views['trailercount'] ? $views['trailercount'] : 0;
                if((isset($monetization) && (($monetization['menu'] & 2) || ($monetization['menu'] & 16)))){
                $data[0][] = array(
                    $content_name,
                    $price,
                    $transactiontype,
                    $type,
                    $revenue,
                );
   }
   else{
       $data[0][] = array(
                    $content_name,
                    $duration,
                    $viewcount,
                    $uniquecount,
                    $bandwidth_consumed
                );
   }
            }
            }
        }
        
        $filename = 'video_report_' . date('Ymd_His');
        if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'csv') {
            $type = 'xls';
        } elseif (isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf') {
            $type = 'pdf';
        }
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, $type);
        exit;
    }

    public function actionVideoDetails() {
        $content = Yii::app()->general->getPartnersContentIds();
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $studio_id = $this->studio->id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if ((isset($_REQUEST['start']) && trim($_REQUEST['start'])) && (isset($_REQUEST['end']) && trim($_REQUEST['end']))) {
            $dt = array('start'=>$_REQUEST['start'],'end'=>$_REQUEST['end']);
            $dt = (object) $dt;
        }else if(!$dt){
            $dt = '';
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $videoTableData = BufferLogs::model()->getVideoDetails($studio_id,$content['movie_id'],0,$dt,$searchKey,$deviceType,$offset,$page_size);
        if(isset($videoTableData['data']) && empty($videoTableData['data'])){
            $videoTableData = BufferLogs::model()->getVideoDetailsOfDevice($studio_id,$content['movie_id'],0,$dt='',$searchKey,$deviceType,$offset,$page_size);
        }
        $std_id = isset($videoTableData['data'][0]['studio_id'])?$videoTableData['data'][0]['studio_id']:$studio_id;
        if($studio_id == $std_id){
            $is_self = 1;
            if(isset($videoTableData['count']) && !$videoTableData['count']){
                $contentDetails = BufferLogs::model()->getContentDetails($studio_id,0,0);
            }
        }else{
            $is_self = 0;
        }
        
        $hour_bandwidth = BufferLogs::model()->getHourBandwidth($studio_id,$content['movie_id'],0,$dt,$searchKey,$deviceType);
        $bufferDuration = BufferLogs::model()->getTotalBufferDuration($studio_id,$content['movie_id'],0,$dt,$searchKey,$deviceType,1);    
        
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        
        if (isset($_REQUEST['dt'])) {
            $json_data['bandwidth'] = $hour_bandwidth['bandwidth'];
            $json_data['buffer_duration'] = $bufferDuration;
            echo json_encode($json_data);
        }else{
            $allBandwidth = VideoLogs::model()->getAllBandwidthMovie($studio_id,$content['movie_id'],0,$searchKey,$deviceType);
            foreach ($allBandwidth as $row) {
                $date = date('Y-m',strtotime($row['created_date']));
                $x[] = $date;
                if(in_array($date, $x)){
                    $bandwidthArr[$date] +=(float)$row['total_buffered_size']/1024/1024;
                }else{
                    $bandwidthArr[$date] =(float)$row['total_buffered_size']/1024/1024;
                }
            }
            $lunchDate = Yii::app()->user->lunch_date;
            $lmonth = date('n', strtotime($lunchDate));
            $lyear = date('Y', strtotime($lunchDate));
            $arr = array();
            for ($i = date('Y'); $i >= $lyear; $i--) {
                if ($i == date('Y')) {
                    $j = date('n');
                } else {
                    $j = 12;
                }
                for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                    $mont = $j < 10 ? '0' . $j : $j;
                    $arr['bandwidth'][] = isset($bandwidthArr[$i . "-" . $mont]) ? $bandwidthArr[$i . "-" . $mont] : 0;
                    $xdata[] = date('F', mktime(0, 0, 0, $j, 10));
                }
            }
            $bandwidthgraph[] =  array('name' => ucfirst('Bandwidth Consumed'), 'data' => array_reverse($arr['bandwidth']));
            $bandwidthGraphData = json_encode($bandwidthgraph);
            $user_start = Yii::app()->common->getStudioUserStartDate();
            $this->render('videoDetails',array('videoTableData' => $videoTableData,'hour_bandwidth'=>$hour_bandwidth, 'xdata' => json_encode(array_reverse($xdata)),'bandwidthGraphData' => $bandwidthGraphData,'is_self'=>$is_self,'bufferDuration'=>$bufferDuration,'dateRanges'=>$dt,'lunchDate'=>$user_start));
        }
    }

    public function actionGetSearchedVideoDetails() {
        $key = $_REQUEST['search_value'];
        $movie_id = $_REQUEST['id'];
        $studio_id = $this->studio->id;
        $videoTableData = BufferLogs::model()->getSearchedVideoDetails($studio_id, $movie_id, $key);
        $this->renderPartial('bandwidthTable', array('videoTableData' => $videoTableData));
        exit;
    }
    
    public function actionGetSearchedViews() {
        $key = $_REQUEST['search_value'];
        $content = Yii::app()->general->getPartnersContentIds();
        $views = VideoLogs::model()->getSearchedViewsDetails($key,$content['movie_id'],1);
        $this->renderPartial('videoViewData', array('viewsDetails' => $views));
        exit;
    }
    
    public function actionGetMonthlyBandwidth(){
        $studio_id = $this->studio->id;
        $content = Yii::app()->general->getPartnersContentIds();
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $videoTableData = BufferLogs::model()->getVideoDetails($studio_id,$content['movie_id'],0,$dt,$searchKey,$deviceType,$offset,$page_size,1);
        if(isset($videoTableData['data']) && empty($videoTableData['data'])){
            $videoTableData = BufferLogs::model()->getVideoDetailsOfDevice($studio_id,$content['movie_id'],0,$dt,$searchKey,$deviceType,$offset,$page_size);
        }
        $hour_bandwidth = BufferLogs::model()->getHourBandwidth($studio_id,$content['movie_id'],0,$dt,$searchKey,$deviceType);
        $this->renderPartial('bandwidthTable',array('videoTableData' => $videoTableData,'hour_bandwidth'=>$hour_bandwidth));
    }
    
    public function actionGetVideoDetailsReport()
    {
        $dt = $_REQUEST['dt'];
        $studio_id = $this->studio->id;
        $headArr[0] = array('Date & Time','Video','Resolution','Geography','Bandwidth','User');
        $sheetName = array('Video Details');
        $sheet = 1;
        $movie_id = $_REQUEST['id'];
        $video_id = $_REQUEST['video_id'] ? $_REQUEST['video_id'] : 0;
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $content = Yii::app()->general->getPartnersContentIds();
        $videoData = BufferLogs::model()->getVideoDetailsReport($dt,$studio_id,$content['movie_id'],$video_id,$searchKey,$deviceType);
        $data[0] = array();
        foreach($videoData as $video){
            $full_address = $this->mb_unserialize($video['location']);
            $signup_location = json_decode(json_encode($full_address),TRUE);
            $signup_location = $signup_location['\0*\0_data']['\0CMap\0_d'];
            $country = $signup_location['geoplugin_countryName'];
            if(isset($video['content_type']) && $video['content_type'] == 2){
                $video_name = $video['name'].' - Trailer';
            }else{
                $video_name =  $video['name'];
            }
            $data[0][] = array(
                $video['created_date'],
                $video_name,
                $video['resolution'],
                $country,
                $video['buffer_size'],
                trim($video['display_name'])?$video['display_name']:'N/A',
            );
        }
        $filename = 'video_details_report_'.date('Ymd_His');
        if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'csv'){
           $type = 'xls'; 
        }
        elseif(isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf'){
           $type = 'pdf'; 
        }
        Yii::app()->general->getCSV($headArr,$sheet,$sheetName,$data,$filename,$type);
        exit;
    }
    
    public function actionAnalytics()
    {
        $this->pageTitle = ucwords($studio->name)." | Video Analytics";
        $this->breadcrumbs = array('Video' => array('report/video'), 'Analytics');
        $content = Yii::app()->general->getPartnersContentIds();
        $page_size = 20;
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $studio_id = $this->studio->id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if ((isset($_REQUEST['start']) && trim($_REQUEST['start'])) && (isset($_REQUEST['end']) && trim($_REQUEST['end']))) {
            $dt = array('start'=>$_REQUEST['start'],'end'=>$_REQUEST['end']);
            $dt = (object) $dt;
        }else if(!$dt){
            $dt = '';
        }
        
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $videoTableData = BufferLogs::model()->getVideoDetails($studio_id,$content['movie_id'],0,$dt,$searchKey,$deviceType,$offset,$page_size);
        if(isset($videoTableData['data']) && empty($videoTableData['data'])){
            $videoTableData = BufferLogs::model()->getVideoDetailsOfDevice($studio_id,$content['movie_id'],0,$dt='',$searchKey,$deviceType,$offset,$page_size);
        }
        $std_id = isset($videoTableData['data'][0]['studio_id'])?$videoTableData['data'][0]['studio_id']:$studio_id;
        if($studio_id == $std_id){
            $is_self = 1;
            if(isset($videoTableData['count']) && !$videoTableData['count']){
                $contentDetails = BufferLogs::model()->getContentDetails($studio_id,$content['movie_id'],0);
            }
        }else{
            $is_self = 0;
        }
        
        $hour_bandwidth = BufferLogs::model()->getHourBandwidth($studio_id,$content['movie_id'],0,$dt,$searchKey,$deviceType);
        $bufferDuration = BufferLogs::model()->getTotalBufferDuration($studio_id,$content['movie_id'],0,$dt,$searchKey,$deviceType);    
        if (isset($_REQUEST['dt'])) {
            $json_data['bandwidth'] = $hour_bandwidth['bandwidth'];
            $json_data['buffer_duration'] = $bufferDuration;
            echo json_encode($json_data);
        }else{
            $allBandwidth = VideoLogs::model()->getAllBandwidthMovie($studio_id,$content['movie_id'],0,$searchKey,$deviceType);
            foreach ($allBandwidth as $row) {
                $date = date('Y-m',strtotime($row['created_date']));
                $x[] = $date;
                if(in_array($date, $x)){
                    $bandwidthArr[$date] +=(float)$row['total_buffered_size']/1024/1024;
                }else{
                    $bandwidthArr[$date] =(float)$row['total_buffered_size']/1024/1024;
                }
            }
            //$lunchDate = Yii::app()->user->lunch_date;
            $lunchDate = $allBandwidth[0]['movie_date'];
            $lmonth = date('n', strtotime($lunchDate));
            $lyear = date('Y', strtotime($lunchDate));
            $arr = array();
            for ($i = date('Y'); $i >= $lyear; $i--) {
                if ($i == date('Y')) {
                    $j = date('n');
                } else {
                    $j = 12;
                }
                for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                    $mont = $j < 10 ? '0' . $j : $j;
                    $arr['bandwidth'][] = isset($bandwidthArr[$i . "-" . $mont]) ? $bandwidthArr[$i . "-" . $mont] : 0;
                    $xdata[] = date('F', mktime(0, 0, 0, $j, 10));
                }
            }
            $bandwidthgraph[] =  array('name' => ucfirst('Bandwidth Consumed'), 'data' => array_reverse($arr['bandwidth']));
            $bandwidthGraphData = json_encode($bandwidthgraph);
            $user_start = Yii::app()->common->getStudioUserStartDate();
            $this->render('analytics',array('videoTableData' => $videoTableData,'hour_bandwidth'=>$hour_bandwidth['bandwidth'],'xdata' => json_encode(array_reverse($xdata)),'bandwidthGraphData' => $bandwidthGraphData,'is_self'=>$is_self,'bufferDuration'=>$bufferDuration,'dateRanges'=>$dt,'lunchDate'=>$user_start,'page_size'=>$page_size));
        }
    }
}
