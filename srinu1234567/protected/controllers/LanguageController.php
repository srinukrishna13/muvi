<?php
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
class LanguageController extends Controller
{
    public $layout = 'admin';
    public $headerinfo='';
    protected function beforeAction($action){
        parent::beforeAction($action);
        Yii::app()->theme = 'admin'; 
        if(!(Yii::app()->user->id))
        {
            $this->redirect(array('/index.php/'));
        }else{
             $this->checkPermission();
        }
                
        return true;
    }
    function actionManageLanguage() {
        $this->breadcrumbs=array('Settings','Language', 'Manage Language');
        $this->pageTitle="Muvi | Manage Language";
        $this->headerinfo = "Manage Language";
        $this->layout='admin';
        
        $studio_id = Yii::app()->common->getStudiosId();
        
        $con = Yii::app()->db;
        $sql = "SELECT a.*, s.id AS studio_id, s.default_language FROM 
        (SELECT l.id AS languageid, l.name, l.code, sl.* FROM languages l LEFT JOIN studio_languages sl 
        ON (l.id = sl.language_id AND sl.studio_id={$studio_id}) WHERE l.code='en' OR (sl.studio_id={$studio_id})) AS a, studios s WHERE s.id={$studio_id} 
        ORDER BY FIND_IN_SET(a.code,s.default_language) DESC, FIND_IN_SET(a.code,'en') DESC, a.status DESC, a.name ASC";
        
        $studio_languages = $con->createCommand($sql)->queryAll();
        $this->render('managelanguage', array('studio_languages' => $studio_languages));
    }
    
    function actionGetLanguages() {
        $this->layout=false;
        
        $q = trim($_REQUEST['term']);
        $studio_id = Yii::app()->common->getStudiosId();
        
        $sql = "SELECT * FROM languages WHERE LOWER(code) != 'en' AND id NOT IN
        (SELECT language_id FROM studio_languages WHERE studio_id={$studio_id}) 
        AND (LOWER(name) LIKE '%".strtolower($q)."%' )";
        
        $con = Yii::app()->db;
        $list = $con->createCommand($sql)->queryAll();
        if (isset($list) && empty($list)) {
            $list[0]['id'] = '';
            $list[0]['name'] = 'No Language found!';
            $list[0]['code'] = '';
        }
        
        echo json_encode($list);exit;
    }
    
    function actionSaveLanguage() {
        if (isset($_REQUEST['language_id']) && trim($_REQUEST['language_id'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            
            $slModel = New StudioLanguage;
            $slModel->studio_id = $studio_id;
            $slModel->language_id = trim($_REQUEST['language_id']);
            $slModel->status = 1;
            $slModel->created_by = $user_id;
            $slModel->created_date = date('Y-m-d H:i:s');
            $slModel->save();
            
            Yii::app()->user->setFlash('success','Language has been added successfully.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while adding the language.');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    
    function actionEnableLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            $slModel->status = 1;
            $slModel->updated_by = $user_id;
            $slModel->updated_date = date('Y-m-d H:i:s');
            $slModel->save();
            Yii::app()->user->setFlash('success','Language has been enabled successfully.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while enabling the language.');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    
    function actionDisableLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $code = $_REQUEST['code'];
            $domain = Yii::app()->getBaseUrl(true);
            $domain = explode("//",$domain);
            $domain = $domain[1];
            if($_COOKIE['Language'] == $code){
                if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
                    setcookie('Language', "en", time() + (60 * 60 * 24 * 90), '/', $domain, false, false);
                }
            }
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            $slModel->status = 0;
            $slModel->updated_by = $user_id;
            $slModel->updated_date = date('Y-m-d H:i:s');
            $slModel->save();
            Yii::app()->user->setFlash('success','Language has been disabled successfully.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while disabling the language.');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    function actionFrontendShowStatusOfLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $code = $_REQUEST['code'];
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            if(!empty($slModel)){
                $studioLanguage = StudioLanguage::model()->findByPk($slModel->id);
                $studioLanguage->frontend_show_status = $slModel->frontend_show_status == '1' ? '0' : '1';
                $studioLanguage->updated_by = $user_id;
                $studioLanguage->updated_date = date('Y-m-d H:i:s');
                $studioLanguage->save();
            }else{
                $studioLanguage = new StudioLanguage;
                $studioLanguage->studio_id = $studio_id;
                $studioLanguage->language_id = 20;
                $studioLanguage->status = 1;
                $studioLanguage->created_by = $user_id;
                $studioLanguage->updated_by = $user_id;
                $studioLanguage->frontend_show_status = '0';
                $studioLanguage->created_date = date('Y-m-d H:i:s');
                $studioLanguage->updated_date = date('Y-m-d H:i:s');
                $studioLanguage->save();
            }
            Yii::app()->user->setFlash('success','Language show hide status updated.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while disabling the language.');
        }
    
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    function actionMakeprimaryLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language']) && isset($_REQUEST['code']) && trim($_REQUEST['code'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            if (isset($slModel) && !empty($slModel)) {
                $slModel->status = 1;
                $slModel->frontend_show_status = '1';
                $slModel->updated_by = $user_id;
                $slModel->updated_date = date('Y-m-d H:i:s');
                $slModel->save();
            }
            
            $studio = Studio::model()->findByPk($studio_id);
            $studio->default_language = trim($_REQUEST['code']);
            $studio->save();
            
            Yii::app()->user->setFlash('success','Language has been set as primary successfully');
        } else {
            Yii::app()->user->setFlash('error','Oops! Sorry, Language can not be made as primary!');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    
    function actionDeleteLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $code = $_REQUEST['code'];
            $domain = Yii::app()->getBaseUrl(true);
            $domain = explode("//",$domain);
            $domain = $domain[1];
            if($_COOKIE['Language'] == $code){
                if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
                    setcookie('Language', "en", time() + (60 * 60 * 24 * 90), '/', $domain, false, false);
                }
            }
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            $slModel->delete();
            Yii::app()->user->setFlash('success','Language has been deleted successfully.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while deleting the language.');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    } 
    public function actionTranslation(){
        $this->breadcrumbs  = array('Settings','Language','Translation');
        $this->pageTitle    = "Muvi | Translation";
        $this->headerinfo   = "Translation";
        $this->layout       = 'admin';
        $studio_id          = Yii::app()->user->studio_id;
        $enable_lang        = $this->language_code;
        $trans_key          = TranslateKeyword::model()->findAll(array('condition'=>'studio_id=:studio_id OR studio_id=0 ORDER BY trans_value ASC','params'=>array(':studio_id'=>$studio_id)));
        $studio_lang_path   = ROOT_DIR.'languages/studio/en.php';
        $studio_lang_merge  = $this->mergeArray($studio_lang_path);
        $trans_word         = array();
        if(!empty($trans_key)){
            foreach($trans_key as $trans_words){
                $trans_word[$trans_words->trans_key] = $trans_words->trans_value;
            }
        }
        if(count($trans_word) !=  count($studio_lang_merge)){
            $this->UpdateMainLanguageFile();
        }
        $total_words_encode = count($trans_word);
        $enable_lang_name   = Language::model()->findByAttributes(array('code'=>$enable_lang));
        $enable_lang_name   = $enable_lang_name['name'];
        $studio_details     = $this->studio;
        $theme              = $studio_details->theme;
        $name               = $studio_details->name;
        $main_language_path = ROOT_DIR.'languages/studio/'.$enable_lang.'.php';
        $path               = ROOT_DIR."languages/".$theme;
        $file_path          = $path."/".$enable_lang.".php";
        $english_path       = $path."/en.php";
        if(file_exists($file_path)){
            $trans_val = $this->mergeArray($file_path);
        }elseif(file_exists($main_language_path)){
            $trans_val = $this->mergeArray($main_language_path);
        }else{
            $trans_val = $trans_word;
        }
        $this->render('translation',array('trans_key'=>$trans_key,'trans_val'=>$trans_val,'language_from_theme'=>$language_frm_theme,'select_lang'=>$enable_lang_name,'total_words_encode'=>$total_words_encode));
        }
    public function actionmakeTranslation(){
        $enable_lang        = $this->language_code;
        $enable_lang_name   = Language::model()->findByAttributes(array('code'=>$enable_lang));
        $enable_lang_name   = $enable_lang_name['name'];
        $studio_id          = Yii::app()->user->studio_id;
        $studio_details     = $this->studio;
        $theme              = $studio_details->theme;
        $name               = $studio_details->name;
        $main_language_path = ROOT_DIR.'languages/studio/'.$enable_lang.'.php';
        $path               = ROOT_DIR."languages/".$theme;
        $file_path          = $path."/".$enable_lang.".php";
        if(isset($_POST)){
            if(!@is_dir($path)) {
                chmod(ROOT_DIR."languages",0777);
                @mkdir($path,0777,true);           
                chmod($path,0777);
    }
            $static_message_key   = $_POST['static_message_key'];
            $static_message_value = $_POST['static_message_value'];
            $js_message_key       = $_POST['js_message_key'];
            $js_message_value     = $_POST['js_message_value'];
            $server_message_key   = $_POST['server_message_key'];
            $server_message_value = $_POST['server_message_value'];
            
            $static_msg       = array_combine($static_message_key, $static_message_value);
            $static_msg       = is_array($static_msg)?$static_msg:array();
            $js_msg           = array_combine($js_message_key, $js_message_value);
            $js_msg           = is_array($js_msg)?$js_msg:array();
            $server_msg       = array_combine($server_message_key, $server_message_value);
            $server_msg       = is_array($server_msg)?$server_msg:array();
            
            $static_message   = '$static_messages=' . var_export($static_msg, true) . ';';
            $js_message       = '$js_messages=' . var_export($js_msg, true) . ';';
            $server_message   = '$server_messages=' . var_export($server_msg, true) . ';';
            
            $file_content     = '<?php '.$static_message.$js_message.$server_message.'$results = array();$results["static_messages"] = $static_messages;$results["js_messages"] = $js_messages;$results["server_messages"] = $server_messages; return $results;';
            
            if(file_exists($file_path)){
                $temp_file = fopen($path."/temp.php", "w+");
                chmod($path."/temp.php",0777);
                copy($file_path, $path."/temp.php" );
            }
            chmod($path, 0777);
            if (file_exists($file_path)) {
                chmod($file_path, 0777);
            }
            $file = fopen($file_path, "w+");
            if (fwrite($file, $file_content) === FALSE) {
                echo "Cannot write to the file";
            }else{
                echo "Succesfully write to the file";
                fclose($file);
                chmod($file_path, 0777);
            }
            $enable_lang_first_studio = Language::model()->getFirstLanguageUser($enable_lang);
            if($enable_lang !="en" && $enable_lang_first_studio == $studio_id){
                $file = fopen($main_language_path, "w+");
                chmod(ROOT_DIR."languages",0777);
                chmod(ROOT_DIR."languages/studio",0777);
                chmod($main_language_path,0777);
                file_put_contents($main_language_path, $file_content);
            }
            $log_path           = $path."/log.php";
            $current_file       = $enable_lang.".php";
            $logfile    = $this->logupdate($log_path,$path,$current_file,$name);
        }
    }
    public function logupdate($log_path,$path,$filename,$name,$action=false){
        $add_to_logfile  = array();
        $file_path = $path."/".$filename;
        if($action){
            $status = "deleted";
        }
        $temp_path = $path."/temp.php";
        $temp_file_content = include($temp_path);
        if(empty($temp_file_content)){
            $status  = "added";
        }else{
            $temp    = md5_file($temp_path);
            $current = md5_file($file_path);
            if($temp == $current){
                return true;
            }else{
                $temp_content = array();
                $temp_file = fopen($temp_path, "w+");
                chmod($temp_path,0777);
                file_put_contents($temp_path, '<?php return ' . var_export($temp_content, true) . ';');
                $status = "updated";
            }
        }
        $date = date('Y-m-d h:i:s');
        $email = Yii::app()->user->email;
        $add_to_logfile = $name."(".$email.") ".$status." ".$filename." on ". $date." GMT \n";
        $file = fopen($log_path, "a");
        chmod($log_path,0777);
        fwrite($file, $add_to_logfile);
        fclose($file);
    }
    public function actionDeletelangugefile(){
        $language           = $_POST['language_code'];
        $studio_id          = Yii::app()->user->studio_id;
        $studio_details     = Studio::model()->findbyPk($studio_id);
        $theme              = $studio_details['theme'];
        $name               = $studio_details['name'];
        $path               = ROOT_DIR."languages/".$theme."/";
        $file_path          = $path."/".$language.".php";
        $log_path           = $path."/log.php";
        $current_file       = $language.".php";
        $logfile            = $this->logupdate($log_path,$path,$current_file,$name,"Delete");
        if(file_exists($file_path)){
            if(unlink($file_path)){
                echo "This language file deleted from your theme";
            }
        }else{
            echo "File not exist";
        }
    }

    public function mergeArray($filepath = "") {
        $lng = include($filepath);
        $trans_word = array();
        if(!empty(@$lng['static_messages'])){
            $static_messages    = is_array($lng['static_messages'])? $lng['static_messages'] : array();
            $js_messages        = is_array($lng['js_messages'])? $lng['js_messages'] : array();
            $server_messages    = is_array($lng['server_messages'])? $lng['server_messages'] : array();
        }else{
            $static_messages    = is_array($lng)? $lng : array();
            $js_messages        = array();
            $server_messages    = array();
        }
        $trans_word = array_merge($static_messages,$js_messages,$server_messages);
        return $trans_word;
    }
    
    public function UpdateMainLanguageFile() {
        $studio_id = $this->studio->id;
        $static_msg = TranslateKeyword::model()->getMessages('0');
        $js_msg = TranslateKeyword::model()->getMessages('1');
        $server_msg = TranslateKeyword::model()->getMessages('2');

        $static_message   = '$static_messages=' . var_export($static_msg, true) . ';';
        $js_message       = '$js_messages=' . var_export($js_msg, true) . ';';
        $server_message   = '$server_messages=' . var_export($server_msg, true) . ';';

        $file_content     = '<?php '.$static_message.$js_message.$server_message.'$results = array();$results["static_messages"] = $static_messages;$results["js_messages"] = $js_messages;$results["server_messages"] = $server_messages; return $results;';
        $main_language_path = ROOT_DIR.'languages/studio/en.php';
        $file = fopen($main_language_path, "w+");
        chmod(ROOT_DIR."languages",0777);
        chmod(ROOT_DIR."languages/studio",0777);
        chmod($main_language_path,0777);
        file_put_contents($main_language_path, $file_content);
    }
    public function actioncheckUniqueKey(){
        $studio_id = $this->studio->id;
        if(isset($_REQUEST['key'])){
            $key = $_REQUEST['key'];
            $keyword = TranslateKeyword::model()->find(array('condition'=>"trans_key='".$key."' AND (studio_id=".$studio_id." OR studio_id=0)"));
            if(!empty($keyword)){
                echo "not unique";exit;
            }else{
                echo "unique";exit;
            }
        }else{
            $url=$this->createUrl('admin/dashboard');
            $this->redirect($url);
        }
    }
    public function actionnewKeyword(){
        if($_REQUEST){
            $trans_key  = $_REQUEST['translate_key'];
            $trans_word = $_REQUEST['translate_word'];
            $studio_id  = $this->studio->id;
            $trans =  new TranslateKeyword;
            $trans->trans_key   = $trans_key;
            $trans->trans_value = $trans_word;
            $trans->trans_type  = '0';
            $trans->studio_id   = $studio_id;
            $trans->save();
            $translate_keyword = $this->insertNewKey($trans_key,$trans_word);
        }else{
            $url=$this->createUrl('admin/dashboard');
            $this->redirect($url);
        }
    }
    public function insertNewKey($key,$message){
        $theme = $this->studio->theme;
        $theme_path = ROOT_DIR.'languages/'.$theme;
        $path       = $theme_path.'/en.php';
        chmod(ROOT_DIR."languages",0777);
        chmod($path,0777);
        if(file_exists($path)){
            $translated_data = include($path);
            $static_msg  = $translated_data['static_messages'];
            $js_msg      = $translated_data['js_messages'];
            $server_msg  = $translated_data['server_messages'];
        }else{
           if(!@is_dir($theme_path)) {
                chmod(ROOT_DIR."languages",0777);
                @mkdir($theme_path,0777,true);           
                chmod($theme_path,0777);
            }
            $studio_id        = $this->studio->id;
            $static_msg       = TranslateKeyword::model()->getMessages('0',$studio_id);
            $js_msg           = TranslateKeyword::model()->getMessages('1',$studio_id);
            $server_msg       = TranslateKeyword::model()->getMessages('2',$studio_id);
        }
        $static_msg[$key] = $message;
        $static_message   = '$static_messages=' . var_export($static_msg, true) . ';';
        $js_message       = '$js_messages=' . var_export($js_msg, true) . ';';
        $server_message   = '$server_messages=' . var_export($server_msg, true) . ';';

        $file_content     = '<?php '.$static_message.$js_message.$server_message.'$results = array();$results["static_messages"] = $static_messages;$results["js_messages"] = $js_messages;$results["server_messages"] = $server_messages; return $results;';
        chmod(ROOT_DIR."languages",0777);
        chmod($theme_path,0777);
        $file = fopen($path, "w+");
        chmod($path,0777);
        if(fwrite($file, $file_content) === FALSE){
            echo "Cannot write to the file";
        }else{
            echo "success";
            fclose($file);
            chmod($path,0777);
        }
    }
    public function actionupdateLanguageName(){
        if(isset($_POST['language_id'])){
            $language_id     = $_POST['language_id'];
            $studio_id       = $this->studio->id;
            $translated_name = $_POST['translated_name'];
            $studio_language = StudioLanguage::model()->findByAttributes(array('studio_id'=>$studio_id,'language_id'=>$language_id));
            if(!empty($studio_language)){
                $studio_language->translated_name = $translated_name;
                $studio_language->save();
            }else{
                $studioLanguage = new StudioLanguage;
                $studioLanguage->studio_id = $studio_id;
                $studioLanguage->language_id = 20;
                $studioLanguage->status = 1;
                $studioLanguage->translated_name = $translated_name;
                $studioLanguage->created_by = $user_id;
                $studioLanguage->updated_by = $user_id;
                $studioLanguage->frontend_show_status = '1';
                $studioLanguage->created_date = date('Y-m-d H:i:s');
                $studioLanguage->updated_date = date('Y-m-d H:i:s');
                $studioLanguage->save();
            }
        }else{
            $url=$this->createUrl('admin/dashboard');
            $this->redirect($url);
        }
    }
    public function actionGetLanguageList() {
        $studio = $this->studio;
        $theme = $studio->theme;
        $studio_id = $studio->id;
        $language_id = $this->language_id;
        $trans_word = array();
        $trans_key = TranslateKeyword::model()->findAll(array('condition' => 'studio_id=:studio_id OR studio_id=0 ORDER BY trans_value ASC', 'params' => array(':studio_id' => $studio_id)));
        if (!empty($trans_key)) {
            foreach ($trans_key as $trans_words) {
                $trans_word[$trans_words->trans_key] = $trans_words->trans_value;
            }
        }
        $language_list = Yii::app()->custom->getactiveLanguageList($studio_id, 'original');
        $list = array();
        $count = 0;
        foreach ($language_list as $code => $name) {
            $enable_lang = $code;
            $enable_lang_name = $name;
            $main_language_path = ROOT_DIR . 'languages/studio/' . $enable_lang . '.php';
            $path = ROOT_DIR . "languages/" . $theme;
            $file_path = $path . "/" . $enable_lang . ".php";
            $english_path = $path . "/en.php";
            $sheetName[$count] = $enable_lang_name;
            $headArr[$count] = array('Language Key', 'Original(English)', 'Translate To '.$enable_lang_name);
            if (file_exists($file_path)) {
                $trans_val = $this->mergeArray($file_path);
            } elseif (file_exists($main_language_path)) {
                $trans_val = $this->mergeArray($main_language_path);
            } else {
                $trans_val = $trans_word;
            }
            if (!empty($trans_val)) {
                $i = 0;
                foreach ($trans_word as $key => $val) {
                    $list[$count][$i][0] = $key;
                    $list[$count][$i][2] = $val;
                    $list[$count][$i][1] = $trans_val[$key];
                    $i++;
                }
            }
            $count++;
        }
        $type = 'xls';
        $filename = 'message_translation_' . date('Ymd_His');
        $headArr = array_values($headArr);
        $sheetName = array_values($sheetName);
        $data = array_values($list);
        $sheet = count($sheetName);
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, $type);
        exit;
    }

    public function csvImportTranslation($source) {
        $studio = $this->studio;
        $theme = $studio->theme;
        $studio_id = $studio->id;
        $xls_path = $source;
        $path = ROOT_DIR . "languages/" . $theme;
        $language_list = Yii::app()->custom->getactiveLanguageList($studio_id, 'original');
        $lists = Yii::app()->general->csvImportWithMultiSheet($xls_path);
        $languages = array();
        if(!empty($lists)){
        foreach($lists as $lang_name=>$lang_msg){
            $enable_lang = Language::model()->getLanguageCode($lang_name);
            if(!$enable_lang){
                return "worksheet_error";
            }
            if(!array_key_exists($enable_lang, $language_list)){
                return "notexist";
            }
        }
        }else{
           return "not_valid"; 
        }
        $count = 0;
        foreach ($lists as $langname => $langmsg) {
            $enable_lang = Language::model()->getLanguageCode($langname);
            if ($enable_lang) {
                $file_path = $path . "/" . $enable_lang . ".php";
                foreach ($langmsg as $key => $val) {
                    if($val[2]){
                        $trans_key = $val[0];
                        $trans_type = TranslateKeyword::model()->getLanguageType($trans_key, $studio_id);
                        if ($trans_type == 0) {
                            $static_message_key[$key] = $val[0];
                            $static_message_value[$key] = $val[2];
                        } elseif ($trans_type == 1) {
                            $js_message_key[$key] = $val[0];
                            $js_message_value[$key] = $val[2];
                        } elseif ($trans_type == 2) {
                            $server_message_key[$key] = $val[0];
                            $server_message_value[$key] = $val[2];
                        }else{
                            return "extra_keys";
                        }
                    }
                }
                $static_msg       = array_combine($static_message_key, $static_message_value);
                $static_msg       = is_array($static_msg)? $static_msg : array();
                $js_msg           = array_combine($js_message_key, $js_message_value);
                $js_msg           = is_array($js_msg)? $js_msg : array();
                $server_msg       = array_combine($server_message_key, $server_message_value);
                $server_msg       = is_array($server_msg)? $server_msg : array();
                
                $writetofile = $this->WriteToLangFile($static_msg,$js_msg,$server_msg,$path,$file_path,$studio_id);
                $static_message_key = $static_message_value = $js_message_key = $js_message_value = $server_message_key = $server_message_value = array();
                if(!$writetofile){
                    return false;
                }
            }else{
                return false;
            }
            $count++;
        }
        return true;
    }
    function WriteToLangFile($static_msg,$js_msg,$server_msg,$path,$file_path,$studio_id){
        if(!@is_dir($path)) {
            chmod(ROOT_DIR."languages",0777);
            @mkdir($path,0777,true);           
            chmod($path,0777);
        }
        $static_message = '$static_messages=' . var_export($static_msg, true) . ';';
        $js_message = '$js_messages=' . var_export($js_msg, true) . ';';
        $server_message = '$server_messages=' . var_export($server_msg, true) . ';';

        $file_content = '<?php ' . $static_message . $js_message . $server_message . '$results = array();$results["static_messages"] = $static_messages;$results["js_messages"] = $js_messages;$results["server_messages"] = $server_messages; return $results;';
        if (file_exists($file_path)) {
            $temp_file = fopen($path . "/temp.php", "w+");
            chmod($path . "/temp.php", 0777);
            copy($file_path, $path . "/temp.php");
        }
        chmod($path, 0777);
        if (file_exists($file_path)) {
            chmod($file_path, 0777);
        }
        $file = fopen($file_path, "w+");
        if (fwrite($file, $file_content) === FALSE) {
            return false;
        } else {
            fclose($file);
            chmod($file_path, 0777);
            return true;
        }
    }
    public function actionUploadLangfile() {
        $this->layout = false;
        $action = 'error';
        $max_template_size = 20971520; //20MB=20971520B
        $restricted_template_files = array('mp4', 'mp3', 'sql', 'php', 'xml', 'doc', 'docx', 'pdf','zip','jpg','png','gif','bmp');
        $message = '<span class="error">Error in uploading</span>';
        if (count($_FILES) && $_FILES['lang_file']['size'] > 0 && !($_FILES['lang_file']['error'])) {
            if ($_FILES['lang_file']['size'] > $max_template_size) {
                $action = 'error';
                $message = '<span class="error">File size must be less than 20MB.</span>';
            } else {
                $source  = $_FILES['lang_file']['tmp_name'];  
                $import  = $this->csvImportTranslation($source);
                if($import == 1){
                    $action  = "success";
                    $message = "File imported successfully";
                }elseif($import == "worksheet_error"){
                    $action = "error";
                    $message = "Oops! Worksheet name is incorrect.";
                }elseif($import == "notexist"){
                    $action = "error";
                    $message = "Oops! Please make sure all languages in the worksheet are enabled.";
                }elseif($import == "extra_keys"){
                    $action = "error";
                    $message = "Oops! Please remove extra keys from sheet.";
                }elseif($import == "not_valid"){
                    $action = "error";
                    $message = "Oops! Invalid Data";
                }else{
                    $action = "error";
                    $message = "Oops! Error while importing translation message.";
                }
            }
        }
        $ret = array('action' => $action, 'message' => $message);
        echo json_encode($ret);
    }
}

