<?php
class AppsController extends Controller {
    public function init() {
        parent::init();
    }
    public $layout = 'admin';
    public $headerinfo = '';
    protected function beforeAction($action) {
        parent::beforeAction($action);
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        } else {
            $this->checkPermission();
        }
        Yii::app()->theme = 'admin';
        $this->pageTitle = Yii::app()->name . ' | Manage Home';
        return true;
    }
    public function actionTemplate(){ 
        $redirect_url = $this->createUrl('/admin/dashboard');
        if(isset($_GET['app']) && $_GET['app'] !=""){
            $app_type = @$_GET['app'];
            $app = str_replace("-"," ", $app_type);
            $app = ucwords($app);
            $this->breadcrumbs = array('Mobile & Tv Apps',$app,'Manage Template');
            $this->headerinfo = "Manage Template";
            $this->pageTitle = Yii::app()->name . ' | ' . 'Manage Template';
            $std = $this->studio;
            $studio_id = $std->id;
            $templates = AppsTemplate::model()->findAllByAttributes(array('status' => '1', 'app_type'=> $app_type));
            if(!empty($templates)){
                $this->render('template', array('templates' => $templates, 'studio' => $std));
            }else{
                Yii::app()->user->setFlash('error', "Oops! Sorry trying to access a wrong url");
                $this->redirect($redirect_url);
            }
        }else{
            Yii::app()->user->setFlash('error', "Oops! Sorry trying to access a wrong url");
            $this->redirect($redirect_url);
        }
    }
    public function actionHomepage(){
        $this->breadcrumbs = array('Mobile & Tv Apps', 'Home Page');
        $this->pageTitle   = Yii::app()->name . ' | ' . 'Homepage';
        $this->headerinfo  = "Homepage";
        $studio            = $this->studio;
        $studio_id         = $studio->id;
        $language_id       = $this->language_id;
        $content_enable    = Yii::app()->general->content_count($studio_id);
        $all_images        = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $banner_details    = AppBannerSection::model()->getBannerDimension($studio_id);
        $banners           = AppBanners::model()->getAppBanners($studio_id);
        $banner_url        = Yii::app()->common->getPosterCloudFrontPath($studio_id);
        $banner_text       = AppBannerText::model()->getBannerText($studio_id, $language_id, 'banner_text')->banner_text;
        $base_cloud_url    = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
        $featured          = AppFeaturedSections::model()->getFeaturedSections($studio_id, $language_id); 
        $has_physical      = Yii::app()->general->getStoreLink();
        $home_layout       = StudioConfig::model()->getConfig($studio_id, 'home_layout');
        $this->render('homepage', array('studio' => $studio, 'all_images' => $all_images, 'banner_details' => $banner_details, 'banners' => $banners, 'banner_url' => $banner_url,'banner_text' => $banner_text, 'base_cloud_url' => $base_cloud_url, 'featured' => $featured, 'has_physical' => $has_physical, 'home_layout' => $home_layout, 'content_enable'=>$content_enable));
    }
    /**
     * @method public removeBanner($banner_id) It is used to remove a banner image.
     * @return bool 
     * @author Biswajit Parida<biswajit@muvi.com>
    **/
    function actionRemoveBanner() {
        if (isset($_REQUEST['banner_id']) && $_REQUEST['banner_id']) {
            $studio_id = Yii::app()->user->studio_id;
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
            $unsignedFolderPath = $folderPath['unsignedFolderPath'];
            $s3dir = $unsignedFolderPath . "public/system/studio_banner/" . $studio_id;
            $banner = AppBanners::model()->findByPk($_REQUEST['banner_id']);
            if ($banner && ($banner->studio_id == $studio_id)){
                if ($banner->image_name) {
                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => $s3dir . "/original/" . $banner->image_name
                    ));

                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => $s3dir . "/studio_thumb/" . $banner->image_name
                    ));

                    $res = $banner->delete();
                    if ($res) {
                        Yii::app()->user->setFlash('success', 'Banner removed successfully');
                    }
                } else {
                    Yii::app()->user->setFlash('error', 'Oops! error in banner deletion');
                }
            } else {
                Yii::app()->user->setFlash('error', 'Oops! You do not have access to this');
            }
        } else {
            Yii::app()->user->setFlash('error', 'You do not have access to this');
        }
        $url = $this->createUrl('/apps/homepage');
        $this->redirect($url);
        exit;
    }
    /**
     * @method  actionSaveBannerText It is used to save banner text and banner url.
     * @return bool 
     * @author Biswajit Parida<biswajit@muvi.com>
    **/
    public function actionSaveBannerText(){
        if(isset($_POST['bnr_txt'])){
            $studio_id   = Yii::app()->common->getStudioId();
            $language_id = $this->language_id; 
            $bnr_txt     = @$_POST['bnr_txt'];
            $savetext    = AppBannerText::model()->AddBannerText($studio_id, $language_id, $bnr_txt);
            if((isset($_REQUEST['banner_id']) && $_REQUEST['banner_id'] !="") && (isset($_REQUEST['url_text']) && $_REQUEST['url_text'] !="")){
                $banner_id = @$_REQUEST['banner_id'];
                $url_text  = @$_REQUEST['url_text'];
                for($i = 0; $i< count($banner_id); $i++){
                    $app_banner = AppBanners::model()->findByPk($banner_id[$i]);
                    $app_banner->banner_url = $url_text[$i];
                    $app_banner->save();
                }  
            }  
            Yii::app()->user->setFlash('success', 'Banner text updated.');
            $url = $this->createUrl('/apps/homepage');
        }else{
            Yii::app()->user->setFlash('error', 'Oops! You do not have access to this');
            $url = $this->createUrl('/admin/dashboard');
        }
        $this->redirect($url);
        exit;
    }
    /**
     * @method  AddHomepageSection It is used to create new featured section.
     * @return bool 
     * @author Biswajit Parida<biswajit@muvi.com>
    **/
    public function actionAddHomepageSection(){
        $studio_id   = Yii::app()->common->getStudioId();
        $arr['succ'] = 0;
        $arr['msg']  = 'Error while saving the section.';
        if (isset($_POST['section_name']) && $_POST['section_name'] != '') {
            $section_name = $_POST['section_name'];
            $content_type = @$_POST['content_type'];
            $section  = new AppFeaturedSections;
            $section->studio_id    = $studio_id;
            $section->content_type = $content_type;
            $section->title        = htmlspecialchars($section_name);
            $section->is_active    = 1; 
            $section->id_seq       = $section->getMaxOrd($studio_id) + 1;
            $section->save();
            $arr['succ'] = 1;
            $arr['msg'] = 'Homepage section is added successfully.';
            Yii::app()->user->setFlash('success', $arr['msg']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/apps/homepage');
            exit();
        }
        echo json_encode($arr);
        exit;   
    }
    public function actionHomePageSection(){
        if(isset($_POST['home_layout'])){
            $studio_id   = Yii::app()->common->getStudioId();
            $home_layout = $_POST['home_layout'];
            $config      = StudioConfig::model()->findByAttributes(array('config_key'=>'home_layout', 'studio_id' => $studio_id));
            if(!empty($config)){
                $config->config_value = $home_layout;
            }else{
                $config  = new StudioConfig;
                $config->studio_id    = $studio_id;
                $config->config_key   = 'home_layout';
                $config->config_value = $home_layout;
            }
            $config->save();
            $msg = "Home page Section Updated";
            echo $msg;
            exit;
        }else{
            Yii::app()->user->setFlash('error', 'Oops! You do not have access to this');
            $url = $this->createUrl('/admin/dashboard');
        }
    }
    public function actionMenu(){
        $this->breadcrumbs = array('Mobile & Tv Apps','Menu');
        $this->headerinfo  = "Menu";
        $this->pageTitle   = Yii::app()->name . ' | ' . 'Menu';
        $studio_id         = $this->studio->id;
        $language_id       = $this->language_id;
        $topmenu           = AppMenu::model()->find('studio_id=:studio_id AND position=:position', array(':studio_id' => $studio_id, ':position' => 'top'));
        if(empty($topmenu)) {
            $menu = new AppMenu;
            $menu->studio_id = $studio_id;
            $menu->position  = 'top';
            $menu->status    = '1';
            $menu->save();
            $menu_id = $menu->id;
        }else{
            $menu_id = $topmenu->id;
        }
        $topmenuitems      = AppMenuItems::model()->getAllMenus($studio_id, $menu_id, $language_id);
        $contentCategories = ContentCategories::model()->findAll('studio_id=:studio_id AND parent_id=0 ', array(':studio_id' => $studio_id));
        $this->render('menu', array('contentCategories' => $contentCategories, 'menu_id' => $menu_id, 'topmenuitems' => $topmenuitems));
    }
    public function actionMenuItem(){
        $appmenu     = 1;
        $studio_id   = $this->studio->id;
        $language_id = $this->language_id;
        $message     = '';
        $action      = 'success';
        if(isset($_REQUEST['option']) && $_REQUEST['option'] != ''){
            $option    = $_REQUEST['option'];
            $item_type = isset($_REQUEST['item_type'])? $_REQUEST['item_type'] : '';
            if ($option == 'add_menu_item' && $item_type != '') {
                $menu_id      = @$_REQUEST['menu_id'];
                $menu_item_id = isset($_REQUEST['menu_item_id']) ? $_REQUEST['menu_item_id'] : 0;
                if ($menu_id == 0 || $menu_id == "") {
                    $menu = new AppMenu;
                    $menu->studio_id = $studio_id;
                    $menu->position  = 'top';
                    $menu->status    = '1';
                    $menu->save();
                    $menu_id = $menu->id;
                }
                if ($item_type == 0) {
                    $cats = $_REQUEST['category-values'];
                    foreach ($cats as $value) {
                        $contentCategory = ContentCategories::model()->find('studio_id=:studio_id AND binary_value=:binary_value', array(':studio_id' => $studio_id, ':binary_value' => $value));
                        $menu_title      = $contentCategory->category_name;
                        $menu_permalink  = $contentCategory->permalink;
                        $menuitem = new AppMenuItems;
                        $menuitem->studio_id = $studio_id;
                        $menuitem->menu_id   = $menu_id;
                        $menuitem->link_type = $item_type;
                        $menuitem->parent_id = '0';
                        $menuitem->status    = '1';
                        $menuitem->value     = $value;
                        $menuitem->title     = $menu_title;
                        $menuitem->permalink = $menu_permalink;
                        $menuitem->id_seq    = Yii::app()->general->findMaxmenuorder($appmenu);
                        $menuitem->save();
                        $menu_item_id = $menuitem->id;
                    }
                }
            }elseif($option == 'delete_item'){
                $item_id   = $_REQUEST['item_id'];
                $menu_item = new AppMenuItems;
                $menu_item->deleteAll('id = :id OR language_parent_id = :id OR parent_id = :id', array(
                    ':id' => $item_id,
                ));
                $action = 'success';
                $message = 'Deleted the menu item successfully.';
                Yii::app()->user->setFlash('success', $message);
            }elseif($option == 'save_item'){
                $menu_id = $_REQUEST['menu_id'];
                $item_id = $_REQUEST['menu_item_id'];
                $menu_item = new AppMenuItems;
                if($language_id == 20){
                    $menuitem = $menu_item->findByPk($item_id);
                }else{
                    $menuitem = $menu_item->findByAttributes(array('language_parent_id'=>$item_id, 'language_id'=>$language_id));
                }
                if(!empty($menuitem)){
                    $menuitem->title = $_REQUEST['menu_title'];
                }else{
                   $menuitem = AppMenuItems::model()->findByPk($item_id); 
                   $menu_item->permalink          = $menuitem->permalink;
                   $menu_item->link_type          = $item_type;
                   $menu_item->title              = $_REQUEST['menu_title'];
                   $menu_item->parent_id          = $menuitem->parent_id;
                   $menu_item->language_id        = $language_id;
                   $menu_item->language_parent_id = $item_id;
                   $menu_item->studio_id          = $studio_id;
                   $menu_item->menu_id            = $menu_id;
                   $menu_item->status             = '1';
                   $menu_item->value              = $menuitem->value;
                   $menu_item->id_seq             = $menuitem->id_seq;
                   $menu_item->save();
                }
                $menuitem->save();
                $action = 'success';
                $message = 'Updated the menu item successfully.';
                Yii::app()->user->setFlash('success', $message);
            }
        }
        $ret = array('action' => $action, 'message' => $message);
        echo json_encode($ret);
    }
    public function actionSortmenu() {
        $a = $_REQUEST['jsonString'];
        $jsonstring = stripslashes(str_replace('\"', '"', $a));
        $orders = json_decode($jsonstring);
        $pos = 0;
        foreach ($orders[0] as $order) {
            $lang_items = new AppMenuItems;
            $attr= array('id_seq'=>$pos,'parent_id'=>0);
            $condition = "id=:id OR language_parent_id =:id";
            $params = array(':id'=>$order->id);
            $lang_items = $lang_items->updateAll($attr,$condition,$params);
            $pos++;
            $childrens = $order->children;
            $childrens = $childrens[0];
            if (count($childrens) > 0) {
                $id_child_seq = 0;
                foreach ($childrens as $child) {
                    $lang_child_items = new AppMenuItems;
                    $attr= array('id_seq'=>$id_child_seq,'parent_id'=>$order->id);
                    $condition = "id=:id OR language_parent_id =:id";
                    $params = array(':id'=>$child->id);
                    $lang_child_items = $lang_child_items->updateAll($attr,$condition,$params);
                    $id_child_seq++;
                }
            }
        }
    }
    public function actionSyncWithWeb(){
        $studio_id    = $this->studio->id;
        $web_sections = FeaturedSection::model()->findAll("parent_id = 0 AND sync_with_app = 0 AND studio_id = ".$studio_id);
        if(!empty($web_sections)){
            $appsection  = new AppFeaturedSections;
            foreach($web_sections as $sections){
                $appsection->id                = null;
                $appsection->studio_id         = $studio_id;
                $appsection->language_id       = $sections->language_id;
                $appsection->parent_id         = 0;
                $appsection->title             = $sections->title;
                $appsection->id_seq            = $appsection->getMaxOrd($studio_id) + 1;
                $appsection->created_date      = new CDbExpression("NOW()");
                $appsection->last_updated_date = new CDbExpression("NOW()");
                $appsection->content_type      = $sections->content_type;
                $appsection->web_featured_id   = $sections->id;
                $appsection->isNewRecord       = true;
                $appsection->save();
                $ftsection[]               =  $sections->id;
                $newsection[$sections->id] =  $appsection->id;
            }
            $nsections  = implode(',', $ftsection);
            $fsection   = new FeaturedSection;
            $attr       = array('sync_with_app'=>1);
            $fcondition = "id IN (".$nsections.") OR parent_id IN (".$nsections.")";
            $fsection   = $fsection->updateAll($attr, $fcondition);
            $flangsections = FeaturedSection::model()->findAll("parent_id IN (".$nsections.")");
            if(!empty($flangsections)){
                $appsection  = new AppFeaturedSections;
                foreach($flangsections as $sections){
                    $appsection->id                = null;
                    $appsection->studio_id         = $studio_id;
                    $appsection->language_id       = $sections->language_id;
                    $appsection->parent_id         = $newsection[$sections->parent_id];
                    $appsection->title             = $sections->title;
                    $appsection->id_seq            = AppFeaturedSections::model()->findByPk($newsection[$sections->parent_id], array('select' => 'id_seq'))->id_seq;
                    $appsection->created_date      = new CDbExpression("NOW()");
                    $appsection->last_updated_date = new CDbExpression("NOW()");
                    $appsection->content_type      = $sections->content_type;
                    $appsection->isNewRecord       = true;
                    $appsection->save();
                }
            }
            $fcontents  = FeaturedContent::model()->findAll("section_id IN (".$nsections.")");
            $appcontent = new AppFeaturedContent;
            foreach($fcontents as $content){
                $appcontent->id          = null;
                $appcontent->studio_id   = $studio_id;
                $appcontent->movie_id    = $content->movie_id;
                $appcontent->section_id  = $newsection[$content->section_id];
                $appcontent->is_episode  = $content->is_episode;
                $appcontent->id_seq      = $content->id_seq;
                $appcontent->isNewRecord = true;
                $appcontent->save();
            }
        }
        Yii::app()->user->setFlash('success', "Featured Section successfully sync with web");
        $this->redirect(Yii::app()->getBaseUrl(true) . '/apps/homepage');
        exit();
    }
}