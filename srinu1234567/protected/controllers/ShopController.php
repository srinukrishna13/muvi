<?php

class ShopController extends Controller {

    /**
     * Declares class-based actions.
     */
    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        /*
        if (!(Yii::app()->user->id)) {
            $this->redirect(array('/user/login'));
        }
        */
        $theme = Studio::model()->find(array('select'=>'parent_theme','condition' => 'id=:studio_id','params' => array(':studio_id' =>$this->studio->id)));
        $error_msg = isset($_REQUEST['ERRORMSG']) && trim($_REQUEST['ERRORMSG']) ? $_REQUEST['ERRORMSG'] : 'Internal Error';
        if(!Yii::app()->general->getStoreLink()){
            Yii::app()->user->setFlash('error', $error_msg);
            $this->redirect($redirect_url);
        }
        if(!in_array($theme->parent_theme, array('classic','physical','byod','classic-byod','traditional-byod','modern-byod'))){
            $this->redirect($redirect_url);
        }
        return true;
    }
    public $layout = 'main';
    
    public function init() {
		$studio_id = Yii::app()->common->getStudiosId();
		$content = Yii::app()->general->content_count($studio_id);
		if ((isset($content) && ($content['content_count'] & 4))) {
			if (isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] == true) {
				$this->layout = false;
			}
		}
		require_once $_SERVER["DOCUMENT_ROOT"] .'/protected/components/pagination.php';
        parent::init();
    }

    public function actionindex() {        
        
        $studio_id = $this->studio->id;        
        //Pagination codes
		$pageUrl = Yii::app()->getBaseUrl(TRUE);
        $items_per_page = 24;//$this->template->items_per_page;
        $page_size = $limit = $items_per_page;
        $offset = 0;
        if (isset($_REQUEST['p']) && (is_numeric($_REQUEST['p']))) {
            $_REQUEST['p'] = floor($_REQUEST['p']);
            $offset = ($_REQUEST['p'] - 1) * $limit;
            $page_number = $_REQUEST['p'];
        } else {
            $page_number = 1;
        }
        $ContentType = "";
        
        if (isset($_REQUEST['cat_id']) && (is_numeric($_REQUEST['cat_id']))) {
            
            $_REQUEST['cat_id'] = floor($_REQUEST['cat_id']);
            $cateCagory = ContentCategories::model()->find('studio_id=:studio_id AND binary_value=:binary_value', array(':studio_id'=>$studio_id, ':binary_value'=>$_REQUEST['cat_id']));
            if($cateCagory){
                $pageUrl .= '/'.$cateCagory->permalink;
            } 
            $req['cat_id'] = $cateCagory->id;//$_REQUEST['cat_id'];
			$pageUrl .= '/?';
			$_REQUEST['format'] = preg_replace('/[^A-Za-z0-9\-]/', '', @$_REQUEST['format']);
            if(isset($_REQUEST['format']) && !empty($_REQUEST['format'])){
                $req['format'] = $_REQUEST['format'];
				$pageUrl .= 'format='.$req['format'].'&';
            }
			$_REQUEST['genre'] = preg_replace('/[^A-Za-z0-9\-]/', '',@$_REQUEST['genre']);
            if(isset($_REQUEST['genre']) && !empty($_REQUEST['genre'])){
                $req['genre'] = $_REQUEST['genre'];
			$pageUrl .= 'genre='.$_REQUEST['genre'].'&';
            }
            if(isset($_REQUEST['orderby']) && !empty($_REQUEST['orderby'])&& is_numeric($_REQUEST['orderby'])){
                $req['orderby'] = $_REQUEST['orderby'];
				$pageUrl .= 'orderby='.$_REQUEST['orderby'];
            }
            
			$pageUrl = trim($pageUrl,'/?');
            $newpage_url = $pageUrl;
            $permalink = $cateCagory->permalink;
            $ContentType = $cateCagory->category_name;
            $menu_item = MenuItem::model()->find('studio_id=:studio_id AND language_parent_id = 0 AND permalink=:permalink', array(':studio_id' => $studio_id, ':permalink' => $permalink));
            if(!empty($menu_item)){
                $menu_item_id = $menu_item->id;
                $meta = Yii::app()->common->listpagemeta(@$menu_item_id);
            }else{
                $meta = Yii::app()->common->listProductmeta();
        }
            
            if ($meta['title'] != '') {
                    $title = $meta['title'];
            } else {
                    $temp = Yii::app()->request->url;
                    $q_temp = explode('?', $temp);
                    if (count($q_temp)) {
                            $temp = $q_temp[0];
                    }
                    $temp = explode('/', $temp);
                    $permalink = $temp[count($temp) - 1];
                    $type = explode('-', $permalink);
                    $default_title = implode(' ', $type);
                    $default_title = ucwords($default_title);

                    $Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $default_title);
                    $title = $Words . ' | ' . $this->studio->name;
            }

            if ($meta['description'] != '') {
                    $description = $meta['description'];
            } else {
                    $description = '';
            }
            if ($meta['keywords'] != '') {
                    $keywords = $meta['keywords'];
            } else {
                    $keywords = '';
            }
            eval("\$title = \"$title\";");
            eval("\$description = \"$description\";");
            eval("\$keywords = \"$keywords\";");
            $this->pageTitle = $title;
            $this->pageDescription = $description;
            $this->pageKeywords = $keywords;
            }
            else
            {
              if(isset($_REQUEST['orderby']) && !empty($_REQUEST['orderby'])&& is_numeric($_REQUEST['orderby'])){
                $req['orderby'] = $_REQUEST['orderby'];
				$pageUrl .= '/?orderby='.$_REQUEST['orderby'];
            }  
            }
            
        $standaloneproduct = PGProduct::getProductList(0, $studio_id, '', $limit, $offset, @$req);
        $item_count = $standaloneproduct['count'];
        unset($standaloneproduct['count']);
        $default_currency_id = $this->studio->default_currency_id;
        foreach ($standaloneproduct AS $key => $orderval) {
	    $pgProductList[$key] = $orderval->attributes;
			$pgProductList[$key]['poster'] = PGProduct::getpgImage($orderval->id,'standard');
            $tempprice = Yii::app()->common->getPGPrices($orderval->id, $default_currency_id);
            if(!empty($tempprice)){
                $pgProductList[$key]['sale_price'] = $tempprice['price'];
                $pgProductList[$key]['currency_id'] = $tempprice['currency_id'];
            }
        }
        $page_url = isset($newpage_url)?$newpage_url:Yii::app()->getBaseUrl(true).'/shop';
        $pg = new bootPagination();
        $pg->pagenumber = $page_number;
        $pg->pagesize = $page_size;
        $pg->totalrecords = $item_count;
        $pg->showfirst = true;
        $pg->showlast = true;
        $pg->paginationcss = "pagination-normal";
        $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
        $pg->defaultUrl = $page_url;
        if (strpos($page_url, '?') > -1)
            $pg->paginationUrl = $page_url . "&p=[p]";
        else {
            $pg->paginationUrl = $page_url."?p=[p]";
        }
        $pagination = $pg->process();
        $this->render('itemlist', array('product' => $pgProductList,'pagination' => @$pagination,'orderby'=>$_REQUEST['orderby'],'pageUrl'=>$newpage_url));     
    }

    public function actionCart() {
        if (!empty($_SESSION["cart_item"])) {
            if (Yii::app()->user->id) {
                $studio_id = $this->studio->id;
                $user_id = Yii::app()->user->id;
                $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);                
                if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                    $gateways = $gateway['gateways'];
                    $can_save_card = (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) ? $gateways[0]['paymentgt']['can_save_card'] : 0;
                    $card_sql = "SELECT tot.* FROM (SELECT id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 AND card_last_fourdigit!='' ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
                    $cards = Yii::app()->db->createCommand($card_sql)->queryAll();
                }
                $saveaddress = PGSavedAddress::model()->findAll( "studio_id = :studio_id AND user_id = :user_id",array('studio_id' => $this->studio->id,'user_id' => Yii::app()->user->id) );
                foreach ($saveaddress as $key => $value) {
                    $countryname = Countries::model()->findByAttributes(array('code' => $value['country']));
                    $saveaddress[$key]['country'] = isset($countryname->country)?$countryname->country:$value['country'];
                }
            }
            $countries = Countries::model()->findAll();
            $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id'=>$this->studio->id,'status'=>1,'is_primary'=>1));
            $payment_type = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
        }
        if(trim($gateway_info->short_code)=='stripe' &&  intval($this->IS_PCI_COMPLIANCE[$gateway_info->short_code])){
            $this->cookie_msg = Yii::app()->custom->getPopupBlockMessage();  
        }
        $theme = $this->site_parent_theme;
        $template = Yii::app()->common->getTemplateDetails($theme);
        $settings = Yii::app()->general->getPgSettings();
        $data['is_coupon_exists'] = self::isCouponExists();
        $pid = end($_SESSION["cart_item"])['id'];
        $price = Yii::app()->common->getPGPrices($pid, $this->studio->default_currency_id);
        if(!empty($price)){
            $data['currency'] = Currency::model()->findByPk($price['currency_id']);
        }else{
            $data['currency'] = Currency::model()->findByPk($this->studio->default_currency_id);
        }        
        $gateway_code = $gateway_info->short_code;
        $methods = ShippingMethod::model()->findAll("studio_id=:studio_id AND is_enabled=:is_enabled", array(':studio_id' => $studio_id,':is_enabled' => 1));
        /*
         * ajit@muvi.com
         * 6262: Muvi Kart: Shipping Rule for 'Free Shipping' & other updates
         * change default shipping price
         * default shipping price is stored in this table pg_default_shipping_cost
         * previously it was saved in pg_settings table        
        */
        $default_shipping_cost = PGDefaultShippingCost::model()->find(array('select'=>'default_shipping_cost','condition' => 'studio_id=:studio_id and currency_id=:currency_id','params' => array(':studio_id' =>$studio_id,':currency_id'=>$data['currency']['id'])));
        //get Min Order amount for Free Shipping
        $minimum_shipping_cost = PGMinimumOrderFreeShipping::model()->find(array('select'=>'minimum_order_free_shipping','condition' => 'studio_id=:studio_id and currency_id=:currency_id','params' => array(':studio_id' =>$studio_id,':currency_id'=>$data['currency']['id'])));
        $payment_form = Yii::app()->general->getMuviKartPaymentForm($gateway_code,$methods); 
        Yii::app()->theme = $this->studio->theme;  

        $yearrange['start'] = date('Y');
        $yearrange['end']   = date('Y')+20;
        $config_location = new StudioConfig();
        $cfg_location = $config_location->getconfigvalue('change_location');
        $shiplocation  = (isset($cfg_location['config_value']) && $cfg_location['config_value'])?1:0;
        $addresspage = $this->renderPartial('addresscart',array('saveaddress'=>$saveaddress,'countries'=>$countries,'shiplocation'=>$shiplocation),true);
        $this->render('cart',array('countries'=>@$countries,'card'=>$card,'addresspage'=>$addresspage,'payment_type'=>$payment_type,'yearrange'=>$yearrange,'can_save_card' => @$can_save_card,'cards' => @$cards,'settings' =>@$settings,'data'=>$data,'gateway_code'=>$gateway_code,'methods'=>$methods,'single_method'=>$methods[0]['method_unique_name'],'default_shipping_cost'=>$default_shipping_cost,'minimum_shipping_cost'=>$minimum_shipping_cost,'payment_form'=>$payment_form));
    }
    
    public function actionAddtocart() {
        if (!empty($_POST["quantity"]) && !is_nan($_POST["quantity"])) {
            $productByCode = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('pg_product ')
                    ->where('id =:id AND studio_id=:studio_id', array(':id' => $_POST['id'], ':studio_id' => Yii::app()->common->getStudiosId()))
                    ->queryAll();
            //$sql = "SELECT * FROM pg_product WHERE id='" . $_POST["id"] . "'";
            //$productByCode = Yii::app()->db->createCommand($sql)->queryAll();
            //check for pre order item. get its price accordingly
            if ($productByCode) {
                $default_currency_id = $this->studio->default_currency_id;                          
                $tempprice = Yii::app()->common->getPGPrices($productByCode[0]["id"], $default_currency_id);
                if(!empty($tempprice)){
                    $productByCode[0]['sale_price'] = $tempprice['price'];
                    $productByCode[0]['currency_id'] = $tempprice['currency_id'];
                }
                
                if ($_POST["pricetype"] != 'freeoffer') {
                    $price = ($productByCode[0]["is_preorder"] == 0) ? $productByCode[0]["sale_price"] : self::getPreOrderprice($productByCode[0]["id"]);
                    if ($productByCode[0]["is_free_offer"] == 1) {
                        $price = "0.00";
                        $_SESSION["freeoffer"] = 'Y';
                    }
                } else {
                    $price = '0.00';
                    $_SESSION["freeoffer"] = 'Y';
                }
                $itemArray = array('pg_' . $productByCode[0]["id"] =>
                    array('name' => $productByCode[0]["name"],
                        'id' => $productByCode[0]["id"],
                        'quantity' => $_POST["quantity"],
                        'price' => $price,
                        'currency_id' => $productByCode[0]["currency_id"],
                        'sku' => $productByCode[0]["sku"],
                        'uniqid' => $productByCode[0]["uniqid"],
                        'permalink' => $productByCode[0]["permalink"],
                        'custom_fields' => $productByCode[0]["custom_fields"],
                        'is_free_offer' => $productByCode[0]["is_free_offer"],
                        'size' => $productByCode[0]["size"]
                ));

                if (!empty($_SESSION["cart_item"])) {
                    /*check unique currency id*/
                    $tempcurrency = array();
                    foreach ($_SESSION["cart_item"] as $k => $v) {
                        array_push($tempcurrency, $v['currency_id']);
                    }
                    array_push($tempcurrency, $productByCode[0]["currency_id"]);
                    if(count(array_unique($tempcurrency)) > 1){
                        echo 'currencyerror';
                        exit;
                    }
                    /*End*/
                    if (in_array('pg_' . $productByCode[0]["id"], array_keys($_SESSION["cart_item"]))) {
                        foreach ($_SESSION["cart_item"] as $k => $v) {
                            if ('pg_' . $productByCode[0]["id"] == $k)
                                $_SESSION["cart_item"][$k]["quantity"] = $_SESSION["cart_item"][$k]["quantity"] + $_POST["quantity"];
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }
                $_SESSION['totalqnt'] = 0;
                $item_total = 0;
                foreach ($_SESSION["cart_item"] as $k => $v) {
                    $_SESSION['totalqnt'] = $_SESSION['totalqnt'] + $_SESSION["cart_item"][$k]["quantity"];
                    $item_total += ($v["price"] * $v["quantity"]);
                    $currency_id = $v['currency_id'];
                }
                $_SESSION['item_total'] = Yii::app()->common->formatPrice($item_total, $currency_id);
                $theme = $this->site_parent_theme;
                $template = Yii::app()->common->getTemplateDetails($theme);
                $this->renderPartial('cartpopup');
            } else {
                if (!Yii::app()->request->isAjaxRequest) {
                    Yii::app()->user->setFlash('error', "Oops! Sorry trying to access a wrong url");
                    $this->redirect(Yii::app()->getBaseUrl(true));
                    exit;
                } else {
                    echo '';
                    exit;
                }
            }
        } else {
            if (!Yii::app()->request->isAjaxRequest) {
                Yii::app()->user->setFlash('error', "Oops! Sorry trying to access a wrong url");
                $this->redirect(Yii::app()->getBaseUrl(true));
                exit;
            } else {
                echo '';
                exit;
            }
        }
    }

    public function actionUpdateCart() {
        if (!empty($_SESSION["cart_item"])) {
            foreach ($_SESSION["cart_item"] as $k => $v) {
                if ('pg_'.$_POST["id"] == $k){
                    if($_POST["quantity"]<=0){
                        if(count($_SESSION["cart_item"])>1){
                            unset($_SESSION["cart_item"][$k]);
                        }else{
                            $this->emptyCart();
                        }
                    }else{
                        $_SESSION["cart_item"][$k]["quantity"] = $_POST["quantity"];
                    }
                }
            }
            $_SESSION['totalqnt']=0;
            $item_total=0;
            foreach ($_SESSION["cart_item"] as $k => $v) {
                $_SESSION['totalqnt'] = $_SESSION['totalqnt']+$_SESSION["cart_item"][$k]["quantity"];
                $item_total += ($v["price"] * $v["quantity"]);
                $currency_id  = $v['currency_id'];
            }
            $_SESSION['item_total'] = Yii::app()->common->formatPrice($item_total, $currency_id);
            if(isset($_POST["cartpopup"])){
                $this->renderPartial('cartpopup');
            }
        }
    }
    public function emptyCart(){
        unset($_SESSION["cart_item"]);
        $_SESSION['totalqnt']=0;
        $_SESSION['item_total'] = 0;
        $_SESSION['address_details']=0;
        $_SESSION['payment_details']=0;
        $_SESSION["freeoffer"]='';
        $_SESSION['couponCode']='';
        unset($_SESSION['ship']);
    }

    public function actionRemoveCart() {        
        if (!empty($_SESSION["cart_item"])) {
            foreach ($_SESSION["cart_item"] as $k => $v) {
                if ('pg_'.$_POST["id"] == $k){
                    unset($_SESSION["cart_item"][$k]);
                    //if it is free offer remove the sessio
                    if($v['is_free_offer'])
                    {
                       $_SESSION["freeoffer"]=''; 
                    }
                }
                if((count($_SESSION["cart_item"])==1) && $v['is_free_offer']){
                    unset($_SESSION["cart_item"][$k]);
                    $_SESSION["freeoffer"]='';
                }
                if (empty($_SESSION["cart_item"])){
                    unset($_SESSION["cart_item"]);
                    $_SESSION['address_details']=0;
                    $_SESSION['couponCode']='';
                }
            }
            $_SESSION['totalqnt']=0;
            $item_total=0;
            foreach ($_SESSION["cart_item"] as $k => $v) {
                $_SESSION['totalqnt'] = $_SESSION['totalqnt']+$_SESSION["cart_item"][$k]["quantity"];
                $item_total += ($v["price"] * $v["quantity"]);
                $currency_id  = $v['currency_id'];
            }
            $_SESSION['item_total'] = Yii::app()->common->formatPrice($item_total, $currency_id);
        }else{
            $_SESSION['totalqnt']=0;
        }
        if(isset($_POST["cartpopup"])){
            $this->renderPartial('cartpopup');
        }
    }
    public function actionSaveAddrsession(){
        $_SESSION['ship'] = $_REQUEST['ship'];
        $_SESSION['address_details'] = 1;
        /*Save to Saved address*/
        if (Yii::app()->user->id) {
            //$_SESSION['ship']['first_name'] = Yii::app()->user->display_name;
            $_SESSION['ship']['email'] = Yii::app()->user->email;
        }
        if((Yii::app()->request->isAjaxRequest) && ($_REQUEST['ship']['saveaddr'] || ($_REQUEST['flag']!='insert'))){
            $ip = CHttpRequest::getUserHostAddress();
            $pgsaveaddr = new PGSavedAddress();
            $_SESSION['ship']['id'] = $pgsaveaddr->saveAddress($this->studio->id,Yii::app()->user->id,$_SESSION['ship'],$ip,$_REQUEST['flag']);
            $saveaddress = PGSavedAddress::model()->findAll( "studio_id = :studio_id AND user_id = :user_id",array('studio_id' => $this->studio->id,'user_id' => Yii::app()->user->id) );
            foreach ($saveaddress as $key => $value) {
                $countryname = Countries::model()->findByAttributes(array('code' => $value['country']));
                $saveaddress[$key]['country'] = isset($countryname->country)?$countryname->country:$value['country'];
            }
            $countries = Countries::model()->findAll();
            $config_location = new StudioConfig();
            $cfg_location = $config_location->getconfigvalue('change_location');
            $shiplocation  = (isset($cfg_location['config_value']) && $cfg_location['config_value'])?1:0;
            $this->renderPartial('addresscart', array('saveaddress' => $saveaddress,'countries'=>$countries,'selected_id'=>$_SESSION['ship']['id'],'shiplocation'=>$shiplocation));
        }else{
            echo 1;
        }
        exit;
    }
    public function actionDeleteAddress(){
        $pgsaveaddr = new PGSavedAddress();
        $flag = $pgsaveaddr->deleteAddress($_POST['data'],$this->studio->id,Yii::app()->user->id);
        $saveaddress = PGSavedAddress::model()->findAll( "studio_id = :studio_id AND user_id = :user_id",array('studio_id' => $this->studio->id,'user_id' => Yii::app()->user->id) );
        foreach ($saveaddress as $key => $value) {
            $countryname = Countries::model()->findByAttributes(array('code' => $value['country']));
            $saveaddress[$key]['country'] = isset($countryname->country)?$countryname->country:$value['country'];
        }
        $countries = Countries::model()->findAll();
        $config_location = new StudioConfig();
        $cfg_location = $config_location->getconfigvalue('change_location');
        $shiplocation  = (isset($cfg_location['config_value']) && $cfg_location['config_value'])?1:0;
        $this->renderPartial('addresscart', array('saveaddress' => $saveaddress,'countries'=>$countries,'shiplocation'=>$shiplocation));
        exit;
    }
    public function actionEditAddress(){
        $pgsaveaddr = new PGSavedAddress();
        $req = PGSavedAddress::model()->findByPk($_POST['data']);
        $countryname1 = Countries::model()->findByAttributes(array('code' => $req['country']));
        $req['country'] = isset($countryname1->country)?$countryname1->country:$req['country'];
        $saveaddress = PGSavedAddress::model()->findAll( "studio_id = :studio_id AND user_id = :user_id",array('studio_id' => $this->studio->id,'user_id' => Yii::app()->user->id) );
        foreach ($saveaddress as $key => $value) {
            $countryname = Countries::model()->findByAttributes(array('code' => $value['country']));
            $saveaddress[$key]['country'] = isset($countryname->country)?$countryname->country:$value['country'];
        }
        $countries = Countries::model()->findAll();
        $config_location = new StudioConfig();
        $cfg_location = $config_location->getconfigvalue('change_location');
        $shiplocation  = (isset($cfg_location['config_value']) && $cfg_location['config_value'])?1:0;
        $this->renderPartial('addresscart', array('saveaddress' => $saveaddress,'countries'=>$countries,'req'=>$req,'shiplocation'=>$shiplocation));
        exit;
    }
    public function actionDeliverAddress(){
        $req = PGSavedAddress::model()->findByPk($_POST['data']);
        $pgsettings = Yii::app()->general->getPgSettings();
        $restrict_country = json_decode($pgsettings['restrict_country']);
        $config_location = new StudioConfig();
        $cfg_location = $config_location->getconfigvalue('change_location');
        $shiplocation  = (isset($cfg_location['config_value']) && $cfg_location['config_value'])?1:0;
        if($shiplocation && in_array($_SESSION['country'],$restrict_country)){
            if($req['country'] != $_SESSION['country']){
                echo 2;
            }else{
                $_SESSION['ship'] = $req;
                $_SESSION['address_details'] = 1;
                echo 1;
                exit;
            }
        }else{
            if(!empty($restrict_country) && !in_array($req['country'],$restrict_country)){
                echo 2;
            }else{
                $_SESSION['ship'] = $req;
                $_SESSION['address_details'] = 1;
                echo 1;
                exit;
            }
        }
    }

    public function actionSaveOrder(){
        if (Yii::app()->user->id) {
            $ip = CHttpRequest::getUserHostAddress();
            parse_str($_REQUEST['data'],$pay);
            $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id'=>$this->studio->id,'status'=>1,'is_primary'=>1));
            if($this->PAYMENT_GATEWAY[$gateway_info->short_code]!=''){//check Payment Gatway Exists           
                $datap = $pay['pay'];
                /*coupon integration*/   
                if((isset($_REQUEST['coupon']) && $_REQUEST['coupon'] != '')){
                    $coupon = $_REQUEST['coupon'];
                    $getCoup = Yii::app()->common->getCouponDiscount($coupon, $datap['amount'],$this->studio->id,Yii::app()->user->id,$datap['currency_id']);
                    $datap['amount'] = $amount = $getCoup["amount"];
                    $couponCode = $getCoup["couponCode"];
                    $datap['coupon_code'] = $couponCode;
                    $datap['discount_type'] = 0;//$getCoup["discount_type"];
                    $datap['discount'] = $getCoup["coupon_amount"];
                }
                $datap['amount'] = (float)$datap['amount']+(float)$datap['shipping_cost'];
                $datap['email'] = Yii::app()->user->email;
                $datap['cvv'] = $datap['security'];
                $datap['gateway_code'] = $gateway_info->short_code;                
                if($datap['amount']>0){
                    $payment_gateway_controller = 'Api'.$this->PAYMENT_GATEWAY[$gateway_info->short_code].'Controller';
                    Yii::import('application.controllers.wrapper.'.$payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();                
                    if($pay['card_options']==''){//code for charging the new card
                        if($this->PAYMENT_GATEWAY[$gateway_info->short_code]=='paypalpro'){
                            $datap['ppv'] = 1;
                        }
                        $data = $payment_gateway::processCard($datap);//Authenticate Card                    
                        $res = json_decode($data, true);
                        if (isset($res['isSuccess']) && intval($res['isSuccess'])) {
                            $currency = Currency::model()->findByPk($datap['currency_id']);
                            $user['currency_id'] = $currency->id;
                            $user['currency_code'] = $currency->code;
                            $user['currency_symbol'] = $currency->symbol;
                            $user['amount'] = $datap['amount'];
                            $user['token'] = @$res['card']['token'];
                            $user['profile_id'] = @$res['card']['profile_id'];
                            $user['card_holder_name'] = $datap['card_name'];
                            $user['card_type'] = @$res['card']['card_type'];
                            $user['exp_month'] = $datap['exp_month'];
                            $user['exp_year'] = $datap['exp_year'];
                            $user['card_number'] = $datap['card_number'];
                            $user['cvv'] = $datap['cvv'];
                        }else{
                            echo $data;
                            exit;
                        }
                    }else{//code for charging the saved card
                        $cardsdetails = SdkCardInfos::model()->findByPk(array('id' => $pay['card_options']));
                        $currency = Currency::model()->findByPk($datap['currency_id']);
                        $user['currency_id'] = $currency->id;
                        $user['currency_code'] = $currency->code;
                        $user['currency_symbol'] = $currency->symbol;
                        $user['amount'] = $datap['amount'];
                        $user['token'] = $cardsdetails->token;
                        $user['profile_id'] = $cardsdetails->profile_id;
                        $user['card_holder_name'] = $cardsdetails->card_holder_name;
                        $user['card_type'] = $cardsdetails->card_type;
                        $user['exp_month'] = $cardsdetails->exp_month;
                        $user['exp_year'] = $cardsdetails->exp_year;
                    }
                    $user['studio_name'] = $this->studio->name;
                    $user['gateway_code'] = $gateway_info->short_code;
                    $trans_data = $payment_gateway::processTransactions($user);//charge the card
                    if (intval($trans_data['is_success'])) {
                        if($pay['savecard'] == 1){
                            $sciModel = New SdkCardInfos;
                            $card = $datap;
                            $card['card_last_fourdigit'] = $res['card']['card_last_fourdigit'];
                            $card['token'] = $res['card']['token'];
                            $card['card_type'] = $res['card']['card_type'];
                            $card['auth_num'] = $res['card']['auth_num'];
                            $card['profile_id'] = $res['card']['profile_id'];
                            $card['reference_no'] = $res['card']['reference_no'];
                            $card['response_text'] = $res['card']['response_text'];
                            $card['status'] = $res['card']['status'];
                            $sciModel->insertCardInfos($this->studio->id,Yii::app()->user->id,$card,$ip);
                        }
                    //Save a transaction detail
                    $transaction = new Transaction;
                    $transaction_id = $transaction->insertTrasactions($this->studio->id,Yii::app()->user->id,$datap['currency_id'],$trans_data,4,$ip,$this->PAYMENT_GATEWAY[$gateway_info->short_code]);
                    $datap['transactions_id'] = $transaction_id;
                    /*Save to order table*/        
                    $datap['hear_source'] = $pay['hear_source'];
                    $pgorder = new PGOrder();
                    $orderid = $pgorder->insertOrder($this->studio->id,Yii::app()->user->id,$datap,$_SESSION["cart_item"],$_SESSION['ship'],$ip);
                    /*Save to shipping address*/
                    $pgshippingaddr = new PGShippingAddress();
                    $pgshippingaddr->insertAddress($this->studio->id,Yii::app()->user->id,$orderid,$_SESSION['ship'],$ip);
                    /*Save to Order Details*/
                    $pgorderdetails = new PGOrderDetails();
                    $pgorderdetails->insertOrderDetails($orderid,$_SESSION["cart_item"]);
                    if($pay['card_options']!=''){
                        $data = array('isSuccess'=>1);
                        $data = json_encode($data);
                    }
                    //send email
                    $req['orderid'] = $orderid;
                    $req['emailtype'] = 'orderconfirm';
                    $req['studio_id'] = $this->studio->id;
                    $req['studio_name'] = $this->studio->name;
                    $req['currency_id'] = $datap['currency_id'];
                    // Send email to user
                    Yii::app()->email->getMuviKartEmails($req,'mk_after_order_placed');
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio->id);                
                    if ($isEmailToStudio) {
                        Yii::app()->email->pgEmailTriggers($req);
                    }
                    
                    $this->emptyCart();// Now empty the cart
                    //create order in cds
                    $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio->id));
                    if($webservice)
                    {
                        if($webservice->inventory_type == 'CDS')
                        {
                            $pgorder->CreateCDSOrder($this->studio->id,$orderid);
                            //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
                        }
                    }
                    } else {
                        $data = json_encode($trans_data);
                    }
                }else{                    
                    $data = array('isSuccess'=>1);
                    $data = json_encode($data);
                    $physical = self::physicalDataInsert($datap,$pay,$trans_data,$ip,$this->PAYMENT_GATEWAY[$gateway_info->short_code]);
                }
            }else{
                $data = array('isSuccess'=>0,'Message'=>$this->Language['checkout_not_allowed'] . " please contact <a href=\"mailto:admin@muvi.com\"><u>admin</u></a> !");
                $data = json_encode($data);                
            }
            /* End Payment */            
        }else{
            $data = array('isSuccess'=>0,'Message'=>$this->Language['login_to_save_order']);
            $data = json_encode($data);
        }
        echo $data;
        exit;
    }

    public function actionSuccess(){
        $this->render('success');   
    }
    public function actionProductDetails() {
        //$product = Yii::app()->db->createCommand("SELECT * FROM pg_product WHERE studio_id=".$this->studio->id." AND uniqid='".$product_uniq_id."'")->queryROW();
        if (@$_REQUEST['id']) {
            $studio_id = Yii::app()->common->getStudiosId();
            $product_uniq_id = $_REQUEST['id'];
            $product = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('pg_product ')
                    ->where('uniqid =:id AND studio_id=:studio_id AND ( publish_date<= NOW() OR publish_date IS NULL ) ', array(':id' => $product_uniq_id, ':studio_id' => $studio_id))
                    ->queryROW();

            if ($product) {
                if (Yii::app()->common->isGeoBlockPGContent($product["id"])) {//Auther manas@muvi.com
                    $ContentName = $pname = $product["name"];
                    $meta = Yii::app()->common->productpagemeta("store-page", $product["id"]);
                    if ($meta['title'] != '') {
                    $title = $meta['title'];
                    } else {
                        $temp = Yii::app()->request->url;
                        $temp = explode('/', $temp);
                        $permalink = $temp[count($temp) - 1];
                        $type = explode('-', $permalink);
                        $default_title = implode(' ', $type);
                        $default_title = ucwords($default_title);

                        $Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $default_title);
                        // echo $Words;exit;
                        $title = $Words . ' | ' . $studio->name;
                    }

                    if ($meta['description'] != '') {
                        $description = $meta['description'];
                    } else {
                        $description = '';
                    }
                    if ($meta['keywords'] != '') {
                        $keywords = $meta['keywords'];
                    } else {
                        $keywords = '';
                    }
                    eval("\$title = \"$title\";");
                    eval("\$description = \"$description\";");
                    eval("\$keywords = \"$keywords\";");
                    $this->pageTitle = $title;
                    $this->pageDescription = $description;
                    $this->pageKeywords = $keywords;
                    $default_currency_id = $this->studio->default_currency_id;
                    $tempprice = Yii::app()->common->getPGPrices($product["id"], $default_currency_id);
                    if(!empty($tempprice)){
                        $product['sale_price'] = $tempprice['price'];
                        $product['currency_id'] = $tempprice['currency_id'];
                    }
                    if (@$product['is_preorder'] == 1) {
                        $preordercate = Yii::app()->db->createCommand("SELECT preorder_id FROM pg_preorder_items WHERE item_id=" . $product['id'])->queryROW();
                        $prices = Yii::app()->db->createCommand("SELECT * FROM pg_preorder_price WHERE preorder_id=" . $preordercate['preorder_id'] . " AND currency_id=" . $default_currency_id)->queryROW();
                    }
                    /*$getVideo = PGVideo::model()->findByAttributes(array('studio_id' => $studio_id, 'product_id' => $product["id"]));
                    if(!empty($getVideo) && $getVideo['thirdparty_url'] !=''){// For youtube only
                        preg_match('/src="([^"]+)"/', $getVideo['thirdparty_url'], $match);
                        $url = $match[1];
                        $trailerurls = empty($url)?$getVideo['thirdparty_url']:$url;
                        $vidmatch = preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $trailerurls, $matches);
                        $vid = $matches[1];
                        $trailerurl = 'https://www.youtube.com/embed/'.$vid.'?rel=0';
                    } */
					/* product detail custom value */
					$fcm = new PGCustomMetadata;
					$fcms = $fcm->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id desc'));
					if (count($fcms) > 0) {
						foreach ($fcms as $fcm) {
							$fld = CustomMetadataField::model()->findByPk($fcm->custom_field_id);
							if ($fld->f_id != '' && strlen($fld->f_id) > 0) {
								$arr[$fcm->field_name] = $fld->f_id;
								$arr1[] = array(
									'f_id' => trim($fld->f_id),
									'field_name' => trim($fcm->field_name),
									'field_display_name' => trim($fld->f_display_name),
								);
							}
						}
					}
					if (isset($arr1) && count($arr1) > 0) {
						foreach ($arr1 as $ar) {
							$ar_k = trim($ar['f_id']);
							$ar_k1 = trim($ar['field_name']);
							$k = (int) str_replace('custom', '', $ar_k1);
							if ($k > 5) {
								if (@$product['custom6']) {
									$x = json_decode($product['custom6'], true);
									foreach ($x as $key => $value) {
										foreach ($value as $key1 => $value1) {
											$custom_vals[$ar_k] = array(
												'field_display_name' => trim($ar['field_display_name']),
												'field_value' => trim($value1)
											);
										}
									}
								}
							} else {
								$custom_vals[$ar_k] = array(
									'field_display_name' => trim($ar['field_display_name']),
									'field_value' => trim(is_array(json_decode($product[$ar_k1])) ? implode(', ', json_decode($product[$ar_k1])) : $product[$ar_k1])
								);
							}
						}
						$product['custom'] = $custom_vals;
					}					
                                        $trailerurl = '';
                    $getPGTrailer = PGMovieTrailer::model()->findByAttributes(array('movie_id' => $product["id"], 'is_converted'=>1));
                    if(!empty($getPGTrailer)){
                        if($getPGTrailer['third_party_url'] =='' && $getPGTrailer['trailer_file_name'] != '3rd party url.mp4'){
                            $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                            $trailerurl = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/physical/'.$getPGTrailer['id'].'/'.$getPGTrailer['trailer_file_name'];
                        } else if($getPGTrailer['third_party_url'] !=''){
                            preg_match('/src="([^"]+)"/', $getPGTrailer['third_party_url'], $match);
                            $url = $match[1];
                            $trailerurls = empty($url)?$getPGTrailer['third_party_url']:$url;
                            $vidmatch = preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $trailerurls, $matches);
                            $vid = $matches[1];
                            $trailerurl = 'https://www.youtube.com/embed/'.$vid.'?rel=0';
                            //$trailerurl = $getPGTrailer['third_party_url'];
                        }
                    }
                    $defaultResolution = 144;
                    $this->render('product_details', array('product' => $product, 'prices' => $prices, 'currency_id' => $default_currency_id,'trailerurl' =>@$trailerurl, 'defaultResolution'=>$defaultResolution));
                }else{
                    $content = '<div style="height:250px;text-align:center;"><h3>This product is not available for your country</h3></div>';
                    $this->render('//layouts/geoblock_error',array('content'=>$content));
                    exit;
                }
            } else {
                Yii::app()->user->setFlash('error', "Oops! Sorry no product matching your request");
                $this->redirect(Yii::app()->getBaseUrl(true));
                exit;
            }
        } else {
            Yii::app()->user->setFlash('error', "Oops! Sorry you are trying to access an invalid link");
            $this->redirect(Yii::app()->getBaseUrl(true));
            exit;
        }
    }

    function getPreOrderprice($product_id) {              
        if (intval($product_id)) {            
            $studio_id = Yii::app()->common->getStudiosId();            
            //get the default currency
            $default_currency_id = $this->studio->default_currency_id;
            //get the pre-order category id            
            $preordercate = Yii::app()->db->createCommand("SELECT preorder_id FROM pg_preorder_items WHERE item_id=".$product_id)->queryROW();
            $prices = Yii::app()->db->createCommand("SELECT * FROM pg_preorder_price WHERE preorder_id=".$preordercate['preorder_id']." AND currency_id=".$default_currency_id)->queryROW();  
            return $prices['price_for_unsubscribe'];
        }
    }
    /*
     * Author ajit@muvi.com
     * 2/9/16
     * Logic------
     * Check the current status in physical goods application
     * if elligible for cancellation i.e status in_array 1,2,9
     * check wheather inventory like CDS is present or not
     * if yes-> check current status in cds
     * if status not Toolate then send request to cds
     * update the responce in physical goods application
     * if inventory not present update status in physical goods only
     */
    public function actionCancelItem(){
        $order_item_id = $_POST['order_item_id'];
        $order_number = $_POST['ordernumber'];
        //get the order id from pg
        $orderid = PGOrder::model()->find(array('select'=>'id','condition' => 'order_number=:order_number','params' => array(':order_number' =>$order_number)));
        $studio_id = Yii::app()->common->getStudiosId(); 
        $pgorderdetailsval = PGOrderDetails::model()->findByPk($order_item_id);
        $req['orderid'] = $orderid['id'];
        $req['emailtype'] = 'ordercancel';
        //item id
        $req['item_id'] = $order_item_id;
        $req['studio_id'] = $studio_id;
        $req['studio_name'] = $this->studio->name;
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
        if($webservice)
        {
            if($webservice->inventory_type == 'CDS')
            {                
                //order number,pg_order_details->id
                $value = PGOrderDetails::CancelCDSOrder($studio_id,$order_number,$order_item_id);
                if($value == 'OK')
                {
                    $pgorderdetailsval->item_status=6;
                    $pgorderdetailsval->cds_cancel_status = $value;
                    $pgorderdetailsval->cds_update_date = gmdate('Y-m-d H:i:s');
                    $pgorderdetailsval->save();
                    //send email

                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio->id);                
                    if ($isEmailToStudio) {
                    Yii::app()->email->pgEmailTriggers($req);
                    }
                    Yii::app()->email->getMuviKartEmails($req,'mk_after_order_cancelled');
                    
                    $data = array('isSuccess'=>1,'message'=>'Your cancel request processed successfully.');
                }
                else if($value == 'TOOLATE'){
                   $data = array('isSuccess'=>0,'message'=>'Its too late to. Item already shiipped.'); 
                }
                else{
                   $data = array('isSuccess'=>0,'message'=>'We are unable to process your request.'); 
                }                
            }
        }
        else
        {
            //no inventory system
            //update status in physical goods
            
            $pgorderdetailsval->item_status=6;
            $pgorderdetailsval->cds_update_date=gmdate('Y-m-d H:i:s');
            $pgorderdetailsval->save();
            //send email
            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio->id);                
            if ($isEmailToStudio) {
            Yii::app()->email->pgEmailTriggers($req);
            }
            Yii::app()->email->getMuviKartEmails($req,'mk_after_order_cancelled');
            $data = array('isSuccess'=>1,'message'=>'Your cancel request processed successfully.');
        }       
        $data = json_encode($data);
        echo $data;
        exit;
    }
    /*
     * Check Coupon exists or not derived from UserController
     * @author <manas@muvi.com>
     */
    function isCouponExists() {
        $studio_id = $this->studio->id;
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        $isCouponExists = 0;
        if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 32)) {
            $default_currency_id = $this->studio->default_currency_id;
            //$sql = "SELECT c.id FROM coupon c, coupon_currency cc WHERE c.studio_id = {$studio_id} AND c.status=1 AND c.id=cc.coupon_id AND cc.currency_id={$default_currency_id}";
            $sql = "SELECT c.id FROM coupon c WHERE c.studio_id = {$studio_id} AND c.status=1";
            $coupon = Yii::app()->db->createCommand($sql)->queryAll();
            
            if (isset($coupon) && !empty($coupon)) {
                $isCouponExists = 1;
            }
        }
        return $isCouponExists;
    }
    function actionfreeOffer(){
        $this->layout = false;
        $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id'=>$this->studio->id,'status'=>1,'is_primary'=>1));
$gateway_code = $gateway_info->short_code;
        $freeproducts = PGProduct::model()->findAll(array('select'=>'id,name,sale_price,permalink','condition' => 'studio_id = :studio_id AND is_deleted=0 AND is_free_offer=1','params' => array(':studio_id' =>$this->studio->id)));
        $this->render('freeoffer', array( 'freeproducts' => $freeproducts,'gateway_code'=>$gateway_code));
    }        
    function actionavilFreeOffer(){
        if($_POST['checkoff']==1){
            if($_SESSION["freeoffer"]!='Y'){
                $data = array('isSuccess'=>1);
            }else{
                $data = array('isSuccess'=>0);
}
        }else{
            $data = array('isSuccess'=>0);
        }
        $data = json_encode($data);
        echo $data;
        exit;
    } 
    public function actionCreatePayPalToken() {
        $data = $_REQUEST;
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->user->studio_id;
        if(isset($data) && !empty($data)) {
            /*coupon integration*/   
            if((isset($_REQUEST['coupon']) && $_REQUEST['coupon'] != '')){
                $coupon = $_REQUEST['coupon'];
                $getCoup = Yii::app()->common->getCouponDiscount($coupon, $data['amount'],$this->studio->id,Yii::app()->user->id,$data['currency_id']);
                $data['amount'] = $amount = $getCoup["amount"];
                $data['discount'] = $getCoup["coupon_amount"];
                $couponCode = $getCoup["couponCode"];
                $data['coupon_code'] = $couponCode;
                $data['discount_type'] = 0;//$getCoup["discount_type"];
            }
            $data['amount'] = (float)$data['amount']+(float)$data['shipping_cost'];
            $data['email'] = Yii::app()->user->email;
            $currency = Currency::model()->findByPk($data['currency_id']);
            $data['currency_code'] = $currency->code;
            $data['paymentType'] = "Sale";
            $data['paymentDesc'] = 'SPHE-' . $data['amount'];
            $data['returnURL'] = Yii::app()->getBaseUrl(true)."/shop/PayPalSuccess";
            $data['cancelURL'] = Yii::app()->getBaseUrl(true)."/shop/PayPalCancel";
            $data['currencyCodeType'] = $currency->code;
            $data['shipping'] = 0;
            Yii::app()->session['cart_data'] = $data;
            $payment_gateway_controller = 'ApipaypalproController';
            Yii::import('application.controllers.wrapper.'.$payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $trans_data = $payment_gateway::processTransactionsPCI($data);
            if($trans_data['TOKEN']){
                echo json_encode($trans_data);
            }
        }
    }
    
    public function actionPayPalCancel() {
        Yii::app()->user->setFlash('error', $_REQUEST['ERRORMSG']);
        $this->redirect(Yii::app()->getBaseUrl(true)."/shop/cart");
        exit;
    }
    
    public function actionPayPalSuccess() {
        if (Yii::app()->user->id) {
            $res = $_REQUEST;
            $ip = CHttpRequest::getUserHostAddress();
            $data = $pay = Yii::app()->session['cart_data'];
            $data['email'] = Yii::app()->user->email;
            $trans_data['bill_amount'] = $data['amount'];
            $paypal_transaction_id = isset($res['tx']) && trim($res['tx'])?$res['tx']:$res['txn_id'];
            $payment_gateway_controller = 'ApipaypalproController';
            Yii::import('application.controllers.wrapper.'.$payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $resArray = $payment_gateway::getPCICardTransactionDetails($paypal_transaction_id);
            
            $trans_data['paid_amount'] = $trans_data['dollar_amount'] = $trans_data['amount'] = $resArray["AMT"];
            $trans_data['transaction_status'] = $resArray["PAYMENTSTATUS"];
            $trans_data['transaction_status_reason'] = isset($resArray["PAYMENTSTATUS"]) && trim(strtolower($resArray["PAYMENTSTATUS"]))=='pending' ?$resArray["PENDINGREASON"] :'';
            $trans_data['invoice_id'] = $resArray['TRANSACTIONID'];
            $trans_data['order_number'] = $resArray['PAYERID'];
            $trans_data['response_text'] = json_encode($resArray);
            $data['order_reference_number'] = $data['cart_unique_id'];
            
            $transaction = new Transaction;
            $transaction_id = $transaction->insertTrasactions($this->studio->id,Yii::app()->user->id,$data['currency_id'],$trans_data,4,$ip,'paypalpro');
            $data['transactions_id'] = $transaction_id;
            /*Save to order table*/        
            $data['hear_source'] = $pay['hear_source'];
            $pgorder = new PGOrder();
            $orderid = $pgorder->insertOrder($this->studio->id,Yii::app()->user->id,$data,$_SESSION["cart_item"],$_SESSION['ship'],$ip);
            /*Save to shipping address*/
            $pgshippingaddr = new PGShippingAddress();
            $pgshippingaddr->insertAddress($this->studio->id,Yii::app()->user->id,$orderid,$_SESSION['ship'],$ip);

            /*Save to Order Details*/
            $pgorderdetails = new PGOrderDetails();
            $pgorderdetails->insertOrderDetails($orderid,$_SESSION["cart_item"]);
            //send email
            $req['orderid'] = $orderid;
            $req['emailtype'] = 'orderconfirm';
            $req['studio_id'] = $this->studio->id;
            $req['studio_name'] = $this->studio->name;
            $req['currency_id'] = $data['currency_id'];
            // Send email to user
            Yii::app()->email->getMuviKartEmails($req,'mk_after_order_placed');
            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio->id);                
            if ($isEmailToStudio) {
            Yii::app()->email->pgEmailTriggers($req);
            }
            $this->emptyCart();// Now empty the cart
            
            if(isset($res) && !empty($res) && isset($resArray) && !empty($resArray)) {
                //create order in cds
                $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio->id));

                if($webservice)
                {
                    if($webservice->inventory_type == 'CDS')
                    {
                        $pgorder->CreateCDSOrder($this->studio->id,$orderid);
                        //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
                    }
                }
            }else{
                $pgorder = PGOrder::model()->findByPk($orderid);
                $pgorder->cds_order_status = '-1';
                $pgorder->save();
            }
            /* End Payment */ 
        }else{
            $data = array('isSuccess'=>0,'Message'=>"Please login to save order!");
            $data = json_encode($data);
            
            //Save a transaction detail
            $transaction = new Transaction;
            $transaction_id = $transaction->insertTrasactions($this->studio->id,Yii::app()->user->id,$data['currency_id'],$trans_data,4,$ip,'paypalpro');
            $data['transactions_id'] = $transaction_id;
        }
        $this->redirect(Yii::app()->getBaseUrl(true)."/shop/success");
        exit;
    }
    public function actionCheckProduct(){
		$priceflag = 0;
		foreach ($_SESSION['cart_item'] as $key => $val) {
            $pgmulticurency = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('pg_multi_currency')
                    ->where('product_id =:id and studio_id=:studio_id and currency_id=:currency_id', array(':id' => $val['id'],':studio_id'=>$this->studio->id,':currency_id'=>$val['currency_id']))
                    ->queryRow();         
            if ($pgmulticurency) {
            
                 if ($pgmulticurency['price'] != $val['price'] && $pgmulticurency['currency_id']==$val['currency_id'] ) {
                    $priceflag = 1;
                
                }
            } else {
                $productpriceByCode = Yii::app()->db->createCommand()
                        ->select('sale_price')
                        ->from('pg_product')
                        ->where('id =:id', array(':id' => $val['id']))
                        ->queryRow();
                if ($productpriceByCode['sale_price'] != $val['price']) {
                    $priceflag = 1;
                }
            }
        }
        if ($priceflag) {
            echo 'error';
        } else {
			$_SESSION['check_smethod'] = $_POST['method'];
			$arrayexist = array();
			foreach ($_SESSION['cart_item'] as $key => $value) {
				$prexist = PGProduct::model()->exists('id=:id AND is_deleted=0',array(':id' => $value['id']));            
				if(!$prexist){
					$arrayexist[] = $value['name'];
				}else{
					if (!Yii::app()->common->isGeoBlockPGContent($value['id'])) {
						$arrayexist[] = $value['name'];
					}
				}
			}
			if(!empty($arrayexist)){
				echo implode(',', $arrayexist);
			}else{
				echo 0;
			}
		}
	}
    public function actionGetCurrency() {
        $res = Yii::app()->general->checkFinalPGprice($this->studio->id,$_REQUEST['coupon'],$_SESSION['check_smethod']);
        $currency_id = $res['currency_id'];
        $currency = Currency::model()->findByPk($currency_id);
        $user = SdkUser::model()->findByPk(Yii::app()->user->id);
        //$data['amount'] = $_REQUEST['amount'];
        
        /*coupon integration 
        if((isset($_REQUEST['coupon']) && $_REQUEST['coupon'] != '')){
            $coupon = $_REQUEST['coupon'];
            $getCoup = Yii::app()->common->getCouponDiscount($coupon, $_REQUEST['amount'],$this->studio->id,Yii::app()->user->id,$_REQUEST['currency_id']);
            $data['amount'] = $amount = $getCoup["amount"];
            $data['discount'] = $getCoup["coupon_amount"];
            $couponCode = $getCoup["couponCode"];
            $data['coupon_code'] = $couponCode;
            $data['discount_type'] = 0;//$getCoup["discount_type"];
        }*/ 
        $name = explode(' ',$user->display_name);
        //$data['amount'] = (float)$data['amount']+(float)$_REQUEST['shipping_cost'];
        //$data['currency_id'] = $currency->id;
        //$data['shipping_cost'] = (float)$_REQUEST['shipping_cost'];
        
        $data['amount'] = $res['total_amount'];
        $data['currency_id'] = $res['currency_id'];
        $data['shipping_cost'] = (float)$res['shipping_cost'];
        $data['discount'] = $res['discount'];
        $data['coupon_code'] = $_REQUEST['coupon'];
        $data['discount_type'] = 0;//$getCoup["discount_type"];        
        
        $data['email'] = Yii::app()->user->email;
        $data['first_name'] = isset($name[0]) && trim($name[0])?$name[0]:'';
        $data['last_name'] = isset($name[1]) && trim($name[1])?$name[1]:'';
        $data['currency_code'] = $currency->code;
        
        $data['shipping'] = 0;
        $data['return'] = Yii::app()->getBaseUrl(true)."/shop/payPalSuccess";
        $data['cancel_return'] = Yii::app()->getBaseUrl(true)."/shop/cancel";
        
        $timeparts = explode(" ",microtime());
        $currenttime = bcadd(($timeparts[0]*1000),bcmul($timeparts[1],1000));
        $reference_number = explode('.', $currenttime);
        $data['cart_unique_id'] = $reference_number[0];
        
        $pg_cart_log = new PGCartLog();
        $pg_cart_log->cart_unique_id = $data['cart_unique_id'];
        $pg_cart_log->studio_id = $this->studio->id;
        $pg_cart_log->user_id = Yii::app()->user->id;
        $pg_cart_log->cart_item = json_encode($_SESSION["cart_item"]);
        $pg_cart_log->cart_data = json_encode($data);
        $pg_cart_log->ship = json_encode($_SESSION["ship"]);
        $pg_cart_log->save();
        Yii::app()->session['cart_data'] = $data;
        echo json_encode($data);
    }
    
    public function actionPaypalIPN() {
        $logfile = dirname(__FILE__).'/paypal.txt';
        $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
        $msg .= "\n--------------------------Result---------------------------------------\n";
        $data = serialize($_REQUEST);
        $msg.= $data."\n";
        //Write into the log file
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
    }
    
    public function actioncancel() {
        $msg = isset($_REQUEST['ERRORMSG']) && trim($_REQUEST['ERRORMSG'])?$_REQUEST['ERRORMSG']:$this->ServerMessage['error_transc_process'];
        Yii::app()->user->setFlash('error', $msg);
        $this->redirect(Yii::app()->getBaseUrl(true)."/shop/cart");
        exit;
    }
    
    public function actionProcessPCITransaction() {
        if (Yii::app()->user->id) {
            $ip = CHttpRequest::getUserHostAddress();
            parse_str($_REQUEST['data'],$pay);
            $pay_data['token'] = $token = $_REQUEST['token'];
            if(isset($token) && trim($token)){
                $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id'=>$this->studio->id,'status'=>1,'is_primary'=>1));
                $datap = $pay['pay'];
                $currency = Currency::model()->findByPk($datap['currency_id']);
                $pay_data['currency_id'] = $currency->id;
                $pay_data['currency_code'] = $currency->code;
                $pay_data['currency_symbol'] = $currency->symbol;
                $pay_data['desc'] = 'For ' . $this->studio->name;
                $pay_data['metadata'] = $this->studio->name;
                /*coupon integration*/   
                if((isset($_REQUEST['coupon']) && $_REQUEST['coupon'] != '')){
                    $coupon = $_REQUEST['coupon'];
                    $getCoup = Yii::app()->common->getCouponDiscount($coupon, $datap['amount'],$this->studio->id,Yii::app()->user->id,$datap['currency_id']);
                    $datap['amount'] = $amount = $getCoup["amount"];
                    $couponCode = $getCoup["couponCode"];
                    $datap['coupon_code'] = $couponCode;
                    $datap['discount_type'] = 0;//$getCoup["discount_type"];
                    $datap['discount'] = $getCoup["coupon_amount"];
                }
                $pay_data['amount'] = $datap['amount'] = (float)$datap['amount']+(float)$datap['shipping_cost'];
                $pay_data['email'] = $datap['email'] = Yii::app()->user->email;
                $pay_data['gateway_code'] = $datap['gateway_code'] = $gateway_info->short_code;
                $payment_gateway_controller = 'Api'.$this->PAYMENT_GATEWAY[$gateway_info->short_code].'Controller';
                Yii::import('application.controllers.wrapper.'.$payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $trans_data = $payment_gateway::processPCITransactions($pay_data);
                
                
                if (intval($trans_data['is_success'])) {
                    $trans_data['transaction_status_reason'] = isset($trans_data["transaction_status_reason"]) && trim(strtolower($trans_data["transaction_status_reason"]))=='pending' ?$trans_data["transaction_status_reason"] :'';
                    $datap['order_reference_number'] = $trans_data['invoice_id'];
                    $transaction = new Transaction;
                    $transaction_id = $transaction->insertTrasactions($this->studio->id,Yii::app()->user->id,$datap['currency_id'],$trans_data,4,$ip,$this->PAYMENT_GATEWAY[$gateway_info->short_code]);
                    $datap['transactions_id'] = $transaction_id;
                    /*Save to order table*/        
                    $datap['hear_source'] = $pay['hear_source'];
                    $pgorder = new PGOrder();
                    $orderid = $pgorder->insertOrder($this->studio->id,Yii::app()->user->id,$datap,$_SESSION["cart_item"],$_SESSION['ship'],$ip);
                    /*Save to shipping address*/
                    $pgshippingaddr = new PGShippingAddress();
                    $pgshippingaddr->insertAddress($this->studio->id,Yii::app()->user->id,$orderid,$_SESSION['ship'],$ip);
                    /*Save to Order Details*/
                    $pgorderdetails = new PGOrderDetails();
                    $pgorderdetails->insertOrderDetails($orderid,$_SESSION["cart_item"]);
                    if($pay['card_options']!=''){
                        $data = array('is_success'=>1);
                        $data = json_encode($data);
                    }else{
                        $data = json_encode($trans_data);
                    }
                    //send email
                    $req['orderid'] = $orderid;
                    $req['emailtype'] = 'orderconfirm';
                    $req['studio_id'] = $this->studio->id;
                    $req['studio_name'] = $this->studio->name;
                    $req['currency_id'] = $datap['currency_id'];
                    // Send email to user
                    Yii::app()->email->getMuviKartEmails($req,'mk_after_order_placed');
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio->id);                
                    if ($isEmailToStudio) {
                        Yii::app()->email->pgEmailTriggers($req);
                    }
                    
                    $this->emptyCart();// Now empty the cart
                    //create order in cds
                    $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio->id));
                    if($webservice)
                    {
                        if($webservice->inventory_type == 'CDS')
                        {
                            $pgorder->CreateCDSOrder($this->studio->id,$orderid);
                            //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
                        }
                    }
                } else {
                    $data = json_encode($trans_data);
                } 
                
            }else{
                $data = array('is_success'=>0,'Message'=>"Internal error");
                $data = json_encode($data);
            }
            
        }else{
            $data = array('is_success'=>0,'Message'=>"Please login to save order!");
            $data = json_encode($data);
        }
        echo $data;
        exit;
    }
    public function actioncalculateShippingCost(){
        $pgorder = new PGOrder();
        $rev_shipping_cost = $pgorder->calculateShippingCost($this->studio->id,$_SESSION['ship'],$_SESSION["cart_item"],$_POST['method'],$_REQUEST['currency_id']);      
        //get Min Order amount for Free Shipping
        $minimum_shipping_cost = PGMinimumOrderFreeShipping::model()->find(array('select'=>'minimum_order_free_shipping','condition' => 'studio_id=:studio_id and currency_id=:currency_id','params' => array(':studio_id' =>$this->studio->id,':currency_id'=>$_REQUEST['currency_id'])));
        //echo $minimum_shipping_cost->minimum_order_free_shipping;exit;
        if($rev_shipping_cost['shipping_cost']){
            $code = Currency::model()->find('id=:id', array(':id' => $rev_shipping_cost['currency']));            
            $arr['error'] = 0;
            $arr['shipcost'] = $rev_shipping_cost['shipping_cost'];
            $arr['ship_currency'] = $rev_shipping_cost['currency'];
            $arr['ship_symbol'] = $code->symbol;
            $arr['minimum_order_free_shipping'] = $minimum_shipping_cost->minimum_order_free_shipping;
        }else{
            //default shipping cost
            //6262: Muvi Kart: Shipping Rule for 'Free Shipping' & other updates
            $currency_id = is_numeric($_REQUEST['currency_id'])?$_REQUEST['currency_id']:'';
            $default_shipping_cost = PGDefaultShippingCost::model()->find(array('select'=>'default_shipping_cost','condition' => 'studio_id=:studio_id and currency_id=:currency_id','params' => array(':studio_id' =>$this->studio->id,':currency_id'=>$currency_id)));
            
            $code = Currency::model()->findByPk($currency_id);
            $arr['error'] = 0;
            $arr['shipcost'] = empty($default_shipping_cost)?'0.00':$default_shipping_cost->default_shipping_cost;
            $arr['ship_currency'] = $currency_id;
            $arr['ship_symbol'] = $code->symbol;
            $arr['minimum_order_free_shipping'] = $minimum_shipping_cost->minimum_order_free_shipping;
        }
        echo json_encode($arr);
        exit;
    }       
    /*@author = manas@muvi.com
     * Change session country
     */
    public function actionCountryChange() {
        if(isset($_POST['countrycode']) && !empty($_POST['countrycode'])){
            if(preg_match('/^[a-zA-Z]{2,3}$/', $_POST['countrycode'])){
                $this->emptyCart();
                $_SESSION['country'] = $_POST['countrycode'];
            }            
        }
    }
    
    public function actionUpdateTransactionDetails() {
        $trans_data = array();
        $txn_id = $_REQUEST['paypal_transaction_id'];
        $transaction_id = $_REQUEST['transaction_id'];
        $orderid = $_REQUEST['orderid'];
        
        $studio_id = $this->studio->id;
        $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
        if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
            $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
        }
        
        $payment_gateway_controller = 'ApipaypalproController';
        Yii::import('application.controllers.wrapper.'.$payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $resArray = $payment_gateway::getPCICardTransactionDetails($txn_id);
        if(isset($resArray['ACK']) && strtoupper($resArray['ACK']) == 'SUCCESS') {
            $trans_data['amount'] = $resArray["AMT"];
            $trans_data['transaction_status'] = $resArray["PAYMENTSTATUS"];
            $trans_data['transaction_status_reason'] = isset($resArray["PAYMENTSTATUS"]) && trim(strtolower($resArray["PAYMENTSTATUS"]))=='pending' ?$resArray["PENDINGREASON"] :'';
            $trans_data['invoice_id'] = $resArray['TRANSACTIONID'];
            $trans_data['order_number'] = $resArray['PAYERID'];
            $trans_data['response_text'] = json_encode($resArray);

           //update transaction table
            $transaction = Transaction::model()->findByPk($transaction_id);
            $transaction->amount = $trans_data['amount'];
            $transaction->dollar_amount = $trans_data['amount'];
            $transaction->transaction_status = $trans_data['transaction_status'];
            $transaction->transaction_status_reason = $trans_data['transaction_status_reason'];
            $transaction->invoice_id = $trans_data['invoice_id'];
            $transaction->order_number = $trans_data['order_number'];
            $transaction->response_text = $trans_data['response_text'];
            $transaction->save();

            //update pgorder table for cds
            $pgorder = PGOrder::model()->findByPk($orderid);
            $pgorder->cds_order_status = '';
            $pgorder->save();
            $resPayPal['is_success'] = 1;
            $resPayPal['status'] = 'OK';
        }else{
            $resPayPal['is_success'] = 0;
        }
        echo json_encode($resPayPal);exit;
    }
    public function actionacceptPayment() {
        $tid = $_REQUEST['id']; 
        $transaction = Transaction::model()->findByPk($tid);
        $transaction->transaction_status = 'Success';
        $transaction->save();
        Yii::app()->user->setFlash('success', 'Payment status updated successfully');
        $url = $this->createUrl('store/order');
        $this->redirect($url);        
    }
    public function physicalDataInsert($datap,$pay,$trans_data,$ip,$short_code){
        //Save a transaction detail
        $transaction = new Transaction;
        $transaction_id = $transaction->insertTrasactions($this->studio->id,Yii::app()->user->id,$datap['currency_id'],$trans_data,4,$ip,$short_code);
        $datap['transactions_id'] = $transaction_id;
        /*Save to order table*/        
        $datap['hear_source'] = $pay['hear_source'];
        $pgorder = new PGOrder();
        $orderid = $pgorder->insertOrder($this->studio->id,Yii::app()->user->id,$datap,$_SESSION["cart_item"],$_SESSION['ship'],$ip);
        /*Save to shipping address*/
        $pgshippingaddr = new PGShippingAddress();
        $pgshippingaddr->insertAddress($this->studio->id,Yii::app()->user->id,$orderid,$_SESSION['ship'],$ip);
        /*Save to Order Details*/
        $pgorderdetails = new PGOrderDetails();
        $pgorderdetails->insertOrderDetails($orderid,$_SESSION["cart_item"]);
        if($pay['card_options']!=''){
            $data = array('isSuccess'=>1);
            $data = json_encode($data);
        }
        //send email
        $req['orderid'] = $orderid;
        $req['emailtype'] = 'orderconfirm';
        $req['studio_id'] = $this->studio->id;
        $req['studio_name'] = $this->studio->name;
        $req['currency_id'] = $datap['currency_id'];
        // Send email to user
        Yii::app()->email->getMuviKartEmails($req,'mk_after_order_placed');
        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio->id);                
        if ($isEmailToStudio) {
            Yii::app()->email->pgEmailTriggers($req);
        }

        $this->emptyCart();// Now empty the cart
        //create order in cds
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio->id));
        if($webservice)
        {
            if($webservice->inventory_type == 'CDS')
            {
                $pgorder->CreateCDSOrder($this->studio->id,$orderid);
                //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
            }
        }
        return 1;
    }
    
    public function actionPopupblocked() {
        $this->layout = false;
        Yii::app()->theme = 'bootstrap';
        $this->render('//user/pop_up_block_message');
    }
}
