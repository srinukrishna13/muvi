<?php
class DashboardController extends Controller {

    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);

        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            $this->redirect(array('/index.php/'));
        }

        return true;
    }

    /*     * * Function to show UniqueVisitor graph ***** */

    public function actionUniqueVisitor() {
        $time = strtotime('-1 month'); /* UniqueVisitor of Last month */
        $start_date = date('Y-m-01', $time);
        $end_date = date('Y-m-t', $time);
        $studio_id = $this->studio->id;
        $sql = "SELECT visit_date FROM studio_visitors_log WHERE studio_id = " . $studio_id . " AND (DATE_FORMAT(visit_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') GROUP BY visit_date,ip,browser,os";
        $visitor = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($visitor as $row) {
            $date = date('d', strtotime($row['visit_date']));
            $x[] = $date;
            if (in_array($date, $x)) {
                $reg[$date] +=1;
            } else {
                $reg[$date] = 1;
            }
        }
        $loop = date('t', $time);
        for ($i = 1; $i <= $loop; $i++) {
            $dt = $i < 10 ? '0' . $i : $i;
            $arr[] = isset($reg[$dt]) ? $reg[$dt] : 0;
            $xdata[] = $dt;
        }
        $usergraph[] = array('showInLegend' => false,'type' => 'area', 'name' => ucfirst('No of Views'), 'data' => $arr);
        $graphData = json_encode($usergraph);
        $sql = "SELECT count(*) as cnt FROM studio_visitors_log WHERE studio_id = " . $studio_id . " AND (DATE_FORMAT(visit_date,'%Y-%m-%d') = '" . date('Y-m-d') . "') GROUP BY ip,browser,os";
        $visitortoday = Yii::app()->db->createCommand($sql)->queryAll();        
        $visitortoday = count($visitortoday);//$visitortoday['cnt'] ? $visitortoday['cnt'] : 0;
        $this->renderPartial('uniquevisitor', array('xdata' => json_encode($xdata), 'graphData' => $graphData, 'tot' => $visitortoday));
    }

    /*     * * Function to show total video view graph ***** */

    public function actionVideoViews() {
        $time = strtotime('-1 month'); /* video view of Last month */
        $start_date = date('Y-m-01', $time);
        $end_date = date('Y-m-t', $time);
        $studio_id = $this->studio->id;
        $sql = "SELECT created_date,video_id FROM video_logs WHERE studio_id = " . $studio_id . " AND (DATE_FORMAT(created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') AND video_id!=0";
        $viewvideo = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($viewvideo as $row) {
            $date = date('d', strtotime($row['created_date']));
            $x[] = $date;
            if (in_array($date, $x)) {
                $reg[$date] +=1;
            } else {
                $reg[$date] = 1;
            }
        }
        $loop = date('t', $time);
        for ($i = 1; $i <= $loop; $i++) {
            $dt = $i < 10 ? '0' . $i : $i;
            $arr[] = isset($reg[$dt]) ? $reg[$dt] : 0;
            $xdata[] = $dt;
        }
        $usergraph[] = array('showInLegend' => false,'type' => 'area', 'name' => ucfirst('No of Views'), 'data' => $arr);
        $graphData = json_encode($usergraph);
        $sql = "SELECT count(*) as cnt FROM video_logs WHERE studio_id = " . $studio_id . " AND (DATE_FORMAT(created_date,'%Y-%m-%d') = '" . date('Y-m-d') . "') AND video_id!=0";
        $viewvideotoday = Yii::app()->db->createCommand($sql)->queryRow();
        $viewvideotoday = $viewvideotoday['cnt'];
        $this->renderPartial('videoperday', array('xdata' => json_encode($xdata), 'graphData' => $graphData, 'tot' => $viewvideotoday));
    }

    /*     * * Function to show total video Watching duration graph ***** */

    public function actionVideoDuration() {
        $time = strtotime('-1 month'); /* Revenue of Last month */
        $start_date = date('Y-m-01', $time);
        $end_date = date('Y-m-t', $time);
        $studio_id = $this->studio->id;
        $sql = "SELECT played_length AS watched_hour,created_date FROM video_logs WHERE studio_id = " . $studio_id . " AND (DATE_FORMAT(created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "')";
        $watchedHour = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($watchedHour as $row) {
            $date = date('d', strtotime($row['created_date']));
            $x[] = $date;
            if (in_array($date, $x)) {
                $cnt[$date] +=1;
                $reg[$date] +=$row['watched_hour'];
            } else {
                $cnt[$date] = 1;
                $reg[$date] = $row['watched_hour'];
            }
        }
        $loop = date('t', $time);
        for ($i = 1; $i <= $loop; $i++) {
            $dt = $i < 10 ? '0' . $i : $i;
            $arr[] = isset($reg[$dt]) ? round(($reg[$dt] / $cnt[$dt])/3600,2) : 0;
            $xdata[] = $dt;
        }
        $usergraph[] = array('showInLegend' => false,'type' => 'area', 'name' => ucfirst('Avg Watched Hours'), 'data' => $arr);
        $graphData = json_encode($usergraph);
        $sql = "SELECT SUM(played_length) AS watched_hour,count(*) as cnt FROM video_logs WHERE studio_id = " . $studio_id . " AND (DATE_FORMAT(created_date,'%Y-%m-%d') = '" . date('Y-m-d') . "')";
        $watchedHour = Yii::app()->db->createCommand($sql)->queryRow();
        $avgwatchedHour = round(($watchedHour['watched_hour'] / $watchedHour['cnt'])/3600,4)." Hours";
        $this->renderPartial('videowatchhour', array('xdata' => json_encode($xdata), 'graphData' => $graphData, 'tot' => $avgwatchedHour));
    }

    /*     * * Function to show total revenue graph ***** */

    public function actionTotalRevenue() {
        $time = strtotime('-1 month'); /* Revenue of Last month */
        $sDate = date('Y-m-01', $time);
        $eDate = date('Y-m-t', $time);
        $dbcon = Yii::app()->db;
        $monthsql = "SELECT SUM(transactions.dollar_amount) as tot,transaction_date,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m-%d') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN user_subscriptions ON transactions.subscription_id = user_subscriptions.id WHERE user_subscriptions.status = 1 AND (DATE_FORMAT(transaction_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '" . $eDate . "') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' GROUP BY DATE_FORMAT(user_subscriptions.created_date, '%Y-%m-%d'),plan_id, movie_id ";
        $monthdata = $dbcon->createCommand($monthsql)->queryAll();
        $ppvmonthsql = "SELECT SUM(transactions.dollar_amount) as tot,transaction_date,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN ppv_subscriptions ON transactions.ppv_subscription_id = ppv_subscriptions.id WHERE (DATE_FORMAT(transaction_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '" . $eDate . "') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 GROUP BY DATE_FORMAT(transaction_date, '%Y-%m-%d'),plan_id, movie_id ";
        $ppvmonthdata = $dbcon->createCommand($ppvmonthsql)->queryAll();
        foreach ($ppvmonthdata as $row) {
            $date = date('d', strtotime($row['transaction_date']));
            $ppvx[] = $date;
            if (in_array($date, $ppvx)) {
                $ppvreg[$date] +=$row['tot'];
            } else {
                $ppvreg[$date] = $row['tot'];
            }
        }
        foreach ($monthdata as $row) {
            $date = date('d', strtotime($row['transaction_date']));
            $x[] = $date;
            if (in_array($date, $x)) {
                $reg[$date] +=$row['tot'];
            } else {
                $reg[$date] = $row['tot'];
            }
        }
        /*         * * total revenue so far *** */
        $sql = "SELECT SUM(transactions.dollar_amount) as tot,transaction_date,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN user_subscriptions ON transactions.subscription_id = user_subscriptions.id WHERE user_subscriptions.status = 1 AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' GROUP BY DATE_FORMAT(user_subscriptions.created_date, '%Y-%m'),plan_id, movie_id ";
        $data = $dbcon->createCommand($sql)->queryAll();
        $ppvsql = "SELECT SUM(transactions.dollar_amount) as tot,transaction_date,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN ppv_subscriptions ON transactions.ppv_subscription_id = ppv_subscriptions.id WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id ";
        $ppvdata = $dbcon->createCommand($ppvsql)->queryAll();
        $tot = 0;
        $ppvtot = 0;
        if ($data) {
            $tot = array_sum(array_column($data, 'tot'));
        }
        if ($ppvdata) {
            $ppvtot = array_sum(array_column($ppvdata, 'tot'));
        }
        $total = $tot + $ppvtot;
        /*         * * total revenue so far End *** */
        $loop = date('t', $time);
        for ($i = 1; $i <= $loop; $i++) {
            $dt = $i < 10 ? '0' . $i : $i;
            $tempreg = isset($reg[$dt]) ? $reg[$dt] : 0;
            $tempppvreg = isset($ppvreg[$dt]) ? $ppvreg[$dt] : 0;
            $arr[] = $tempreg + $tempppvreg;
            $xdata[] = $dt;
        }
        $usergraph[] = array('showInLegend' => false, 'name' => ucfirst('Total'), 'data' => $arr);
        $graphData = json_encode($usergraph);
        $this->renderPartial('revenue', array('xdata' => json_encode($xdata), 'graphData' => $graphData, 'tot' => $total));
    }

    /*     * ** Function to show total registerd member graph *** */

    function actionTotalRegmember() {
        $studio_id = $this->studio->id;
        $time = strtotime('-1 month'); /* Registant of Last month */
        $sDate = date('Y-m-01', $time);
        $eDate = date('Y-m-t', $time);
        //$r_sql = "SELECT SQL_CALC_FOUND_ROWS u.id,u.email,u.source,u.signup_location,u.created_date,u.display_name,t.subscription_id FROM sdk_users u LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id LEFT JOIN transactions t ON u.id = t.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ORDER BY u.id DESC";
        $r_sql = "SELECT SQL_CALC_FOUND_ROWS u.id,u.email,u.source,u.signup_location,u.created_date,u.display_name FROM sdk_users u  WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND u.is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ORDER BY u.id DESC";
        $res['registration'] = Yii::app()->db->createCommand($r_sql)->queryAll();
        /* All registerd member */
        //$allsql = "SELECT SQL_CALC_FOUND_ROWS u.id,u.email,u.source,u.signup_location,u.created_date,u.display_name,t.subscription_id FROM sdk_users u LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id LEFT JOIN transactions t ON u.id = t.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' ORDER BY u.id DESC";
        $allsql = "SELECT COUNT(*) as tot FROM sdk_users WHERE studio_id='" . Yii::app()->user->studio_id . "' AND is_developer != '1' AND is_studio_admin != '1' ORDER BY id DESC";
        $data['registration'] = Yii::app()->db->createCommand($allsql)->queryAll();
        foreach ($res['registration'] as $row) {
            $date = date('d', strtotime($row['created_date']));
            $x[] = $date;
            if (in_array($date, $x)) {
                $reg[$date] +=1;
            } else {
                $reg[$date] = 1;
            }
        }
        $tot = $data['registration'][0]['tot'];//count($data['registration']);
        $loop = date('t', $time);
        for ($i = 1; $i <= $loop; $i++) {
            $dt = $i < 10 ? '0' . $i : $i;
            $arr['registration'][] = isset($reg[$dt]) ? $reg[$dt] : 0;
            $xdata[] = $dt;
        }
        $usergraph[] = array('showInLegend' => false, 'name' => ucfirst('registrations'), 'data' => $arr['registration']);
        $graphData = json_encode($usergraph);
        $this->renderPartial('register', array('xdata' => json_encode($xdata), 'graphData' => $graphData, 'tot' => $tot));
    }

    function actionGetServerloc() {
        $studio_id = $this->studio->id;
        $sql = "SELECT * FROM s3buckets WHERE id = (SELECT s3bucket_id FROM studios WHERE id=" . $studio_id . " LIMIT 1)";
        $s3bucket = Yii::app()->db->createCommand($sql)->queryRow();
        if (!empty($s3bucket)) {
            $latitude = $s3bucket['latitude'];
            $longitude = $s3bucket['longitude'];
            $geolocation = $latitude . ',' . $longitude;
            $request = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $geolocation . '&sensor=false';
            $file_contents = @file_get_contents($request);
            $json_decode = json_decode($file_contents);
            $ar = $json_decode->results;
            $loc = $ar[count($ar) - 2]->formatted_address;
        } else {
            $loc = 'Illinois, USA'; //'Verginia,USA';
            $latitude = '41.256703';
            $longitude = '-89.180918';
        }
        $this->renderPartial('server_location', array('address' => $loc, 'latitude' => $latitude, 'longitude' => $longitude));
    }

    function actionStorageUsed() {
        $studio_id = $this->studio->id;
        $storageused =  round(Yii::app()->aws->getStorageUsageOfStudio($studio_id), 2);
        Yii::import('application.controllers.ReportController');
        $storage = ReportController::formatKBytes($storageused*1024*1024*1024,2);
        if($storageused>1024){
            $storageused = ($storageused/1024);
        }
        $percent = round((($storageused/1024) * 100),2);        
        $this->renderPartial('storage',array('storage'=>$storage,'percent'=>$percent));
    }

    function actionGetserverNstorage(){
        $studio_id = $this->studio->id;
        //storage
        $storageused =  round(Yii::app()->aws->getStorageUsageOfStudio($studio_id), 2);
        Yii::import('application.controllers.ReportController');
        $storage = ReportController::formatKBytes($storageused*1024*1024*1024,2);
        //storage location
        $sql = "SELECT * FROM s3buckets WHERE id = (SELECT s3bucket_id FROM studios WHERE id=" . $studio_id . " LIMIT 1)";
        $s3bucket = Yii::app()->db->createCommand($sql)->queryRow();
        if (!empty($s3bucket)) {
            $latitude = $s3bucket['latitude'];
            $longitude = $s3bucket['longitude'];
            $geolocation = $latitude . ',' . $longitude;
            $request = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $geolocation . '&sensor=false';
            $file_contents = @file_get_contents($request);
            $json_decode = json_decode($file_contents);
            $ar = $json_decode->results;
            $loc = $ar[count($ar) - 2]->formatted_address;
        } else {
            $loc = 'Illinois, USA'; //'Verginia,USA';
            $latitude = '41.256703';
            $longitude = '-89.180918';
        }
        //server location and type
        $sqlserverdetails = "SELECT * FROM studio_server_details WHERE studio_id = " . $studio_id . " LIMIT 1";
        $serverdetails = Yii::app()->db->createCommand($sqlserverdetails)->queryRow();
        if (!empty($serverdetails)) {
            $server['latitude'] = $serverdetails['latitude'];
            $server['longitude'] = $serverdetails['longitude'];
            $server['loc'] = $serverdetails['address'];
            $server['type'] = $serverdetails['server_type'];
        } else {
            $refer_id = 7;//here 7 is for sony
            $carr = UserRelation::model()->findByAttributes(array('studio_id' => $studio_id, 'refer_id' => $refer_id));// for reseller
            if(!empty($carr)){
                $sqlserverdetails1 = "SELECT * FROM studio_server_details WHERE refer_id = ".$refer_id." LIMIT 1";
                $serverdetails1= Yii::app()->db->createCommand($sqlserverdetails1)->queryRow();
                if (!empty($serverdetails1)) {
                    $server['latitude'] = $serverdetails1['latitude'];
                    $server['longitude'] = $serverdetails1['longitude'];
                    $server['loc'] = $serverdetails1['address'];
                    $server['type'] = $serverdetails1['server_type'];
                }else{
                    $server['loc'] = 'Virginia, USA';
                    $server['latitude'] = '37.4315734';
                    $server['longitude'] = '-78.6568942';
                    $server['type'] = 0;
                }
            }else{
                $server['loc'] = 'Virginia, USA';
                $server['latitude'] = '37.4315734';
                $server['longitude'] = '-78.6568942';
                $server['type'] = 0;
            }
        }
        $this->renderPartial('server_storage',array('storage'=>$storage,'address' => $loc, 'latitude' => $latitude, 'longitude' => $longitude,'server' => $server));
    }
    function actionBandwidthUsed() {
        $studio_id = $this->studio->id;
        $start_date = Yii::app()->user->lunch_date;
        $end_date = date('Y-m-d');
        $total_bandwidth = Yii::app()->aws->getLocationBandwidthSize($studio_id, $start_date, $end_date, 0, 1);
        Yii::import('application.controllers.ReportController');
        $bandwidthused = ReportController::formatKBytes($total_bandwidth['raw_bandwidth']*1024,2);
        $bandwidth = explode(' ', $bandwidthused);
        $percent = round((($bandwidth['0']/1024) * 100),2);
        $maxbandwith = ($bandwidth['1']=='TB')?100 .' TB':1 .' TB';
        $this->renderPartial('bandwidth',array('bandwidthused'=>$bandwidthused,'percent'=>$percent,'maxbandwith'=>$maxbandwith));
    }

    function actionTrafficGeography() {
        $studio_id = $this->studio->id;
        $sql = "SELECT count(*) as cnt,country FROM studio_visitors_log WHERE studio_id =" . $studio_id." AND (country is not null and country <> '') GROUP BY(country)";
        //print $sql;exit;
        $visitor = Yii::app()->db->createCommand($sql)->queryAll();
        $x = file_get_contents(HOST_URL . Yii::app()->theme->baseUrl . "/views/dashboard/worldmap.json");
        $str = json_decode($x, true);
        foreach ($visitor as $key => $value) {
            $v[$value['country']] = $value['cnt'];
        }
        foreach ($str as $key => $value) {
            if (in_array($value['name'], array_keys($v))) {
                $str[$key]['value'] = $v[$value['name']];
            } else {
                $str[$key]['value'] = 0;
            }
        }
        $this->renderPartial('geotraffic', array('data' => json_encode($str)));
    }

    function actionDivReorder() {
        $defaultsql = "SELECT * FROM dashboard_section order by id ASC";
        $defaultarr1 = Yii::app()->db->createCommand($defaultsql)->queryALL();
        foreach ($defaultarr1 as $k => $v) {
            $defaultarr[$v['id']] = $v['section'];
        }
        if ($_POST['order']) {
            $info = Dashboard::model()->findByAttributes(array('studio_id' => $this->studio->id));
            if (empty($info)) {
                $i = 1;
                foreach ($_POST['order'] as $k => $v) {
                    $dbarr[$i]['studio_id'] = $this->studio->id;
                    $dbarr[$i]['section_id'] = $i;
                    $dbarr[$i]['ordernumber'] = $i;
                    $i++;
                }
                Dashboard::model()->insert_order($dbarr);
            }
            $j = 1;
            foreach ($_POST['order'] as $k => $v) {
                $updatesql = "UPDATE dashboard_ordering SET ordernumber = {$j} WHERE studio_id = {$this->studio->id} AND section_id={$v}";
                $j++;
                Yii::app()->db->createCommand($updatesql)->execute();
            }
        } else {
            //$defaultarr = array(1 => 'hosting', 2 => 'video', 3 => 'website', 4 => 'revenue');
            $sql = "SELECT * FROM dashboard_ordering WHERE studio_id = " . $this->studio->id . " order by ordernumber ASC";
            $arr1 = Yii::app()->db->createCommand($sql)->queryALL();
            if (empty($arr1)) {
                $arr = $defaultarr;
            } else {
                foreach ($arr1 as $k => $v) {
                    $arr[$v['section_id']] = $defaultarr[$v['section_id']];
                }
            }
            //echo "<pre>";print_r($arr);print_r($defaultarr);echo "</pre>";
            $this->renderPartial('divreorder', array('orderarray' => $arr, 'defaultarr' => $defaultarr));
        }
    }
    public function actionChangeCollate() {
        $tables = Yii::app()->db->schema->getTableNames();
        $notread = array(
                    'bandwidth_buffer_log',
                    'video_logs',
                    'visitor_logs',
                    'search_logs',
                    'user_payment_logs',
                    'bandwidth_log',
                    'homepage',
                    'homepage_1',
                    );
        foreach ($tables as $key => $value) {
            if(!in_array($value, $notread)){
                if(strpos($value, 'wp_')!==false){
                }else{
                    //Yii::app()->db->createCommand("ALTER TABLE $value COLLATE utf8_unicode_ci")->execute();
                    //if(!in_array($value, $_SESSION['notread'])){
                        $this->actionChangeCollateTable($value);
                    //}
                }
            }
        }
    }
    function actionChangeCollateTable($table) {
        $column = Yii::app()->db->schema->getTable($table)->columns;
        foreach ($column as $key => $value) {
            if($value->type=='string' && $value->dbType!='datetime' && $value->dbType!='date' && $value->dbType!='timestamp' && $value->dbType!='time' && $value->dbType!='longblob'){
                if(strpos($value->dbType, 'bigint')===false){
                    $query = "ALTER TABLE $table CHANGE {$value->rawName} {$value->rawName} {$value->dbType} CHARACTER SET utf8 COLLATE utf8_unicode_ci;";
                    Yii::app()->db->createCommand($query)->execute();                    
                }
                
            }
        }
        //$_SESSION['notread'][]=$table;
    }

}
