<?php
class LivestreamController extends Controller {
	public $layout='main';
	function init() {
		Yii::app()->theme = 'bootstrap';
		parent::init();
	}
/**
 * @method public Index() Live Streaming Testing.
 * @author GDR<support@muvi.com>
 * @return HTML 
 */
    function actionIndex() {
		$this->pageTitle = "Live Video Streaming App & Software, Live TV Streaming | Live Broadcasting - Muvi";
		$this->pageDescription = "Muvi provides Live Video Streaming App & Software for your Video Streaming needs. Video Streaming Server for all your live broadcast requirements.";
        $this->render('livestream');
    }
/**
 * @method public streamType() Streaming view based on there type
 * @author GDR<support@muvi.com>
 * @return HTML 
 */
    function actionStreamType() {
		$this->layout = FALSE;
		if($_REQUEST['stream_type']=='rtmp'){
			$this->render('rtmp_stream');
		}else{
			$this->render('hls_stream');
		}
    }

	function actionLiveFeedStreaming(){
		Yii::app()->theme = 'bootstrap';
		$this->layout = FALSE;
		if(Yii::app()->common->getStudiosId()==2){
			$this->pageTitle = 'ISKCON Live Streaming - ISKCON Juhu, Mumbai temple Janmashtami Live Streaming';
			$this->pageDescription = 'Watch live streaming of the Janmashtami festival, celebration of the birth of Lord Krishna LIVE from ISKCON’s Juhu, Mumbai temple only at www.iskcontelevisionindia.com';
		}else{
			$this->pageTitle = 'Live Streaming';
			$this->pageDescription = 'Watch live streaming at';
		}
		$studio_id = Yii::app()->common->getStudioId();
		$liveStream = Livestream::model()->find('studio_id=:studio_id',array(':studio_id'=>$studio_id));
		if($liveStream){
			if($liveStream->feed_type==2){
				$this->render('live_rtmpstreaming',array('livestream'=>$liveStream));
			}else{
				$this->render('livefeed_streaming',array('livestream'=>$liveStream));
			}
		}else{
			Yii::app()->user->setFlash('error', 'Oops! Sorry no feeds available for streaming.');
			$this->redirect(Yii::app()->getBaseUrl(TRUE));exit;
		}
	}
/**
 * @method public rtmpFeedStreaming() 
 * @author GDR<support@muvi.com>
 * @return HTML 
 */
	function actionRtmpFeedStreaming(){
		$this->render('rtmpstream');
	}
}

