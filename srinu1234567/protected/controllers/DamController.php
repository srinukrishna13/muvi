<?php

class DamController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
        public $layout='main';
        
        
	public function actionIndex()
	{
            $this->pageTitle = 'DAM (Digital Asset Management) - Cloud based solution for video contentstorage, conversion and distribution by Muvi';
            $this->pageKeywords = 'Muvi.com, Muvi DAM, Digital Asset Management, cloud storage for video, video cloud storage';
            $this->pageDescription = 'Muvi offering DAM (Digital Asset Management) - A cloud based storage solution for video content creators to upload, store, archive, convert and distribute their media content easily across devices and geographies.';            
            $this->render('index');
	}        
}