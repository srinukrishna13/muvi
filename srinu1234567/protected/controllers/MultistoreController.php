<?php

class MultistoreController extends Controller {

    public $defaultAction = 'index';
    public $headerinfo = '';
    public $layout = 'multistore';
    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            $this->redirect(array('/index.php/'));
        }
        return true;
    }

    function actionDashboard() {
        $user_logged_email=$_SESSION['login_attempt_email_id'];
        if(isset($user_logged_email) && $user_logged_email !=''){
            $records = User::model()->findAll("email=:email AND is_active=:is_active AND role_id IN (:role1, :role2 ,:role3)", array(':email' => $user_logged_email,':is_active' => 1,':role1' => 1,':role2' => 2,':role3' => 3));
            $data=array();
            foreach($records as $key=>$record){
                $studio = Studio::model()->find('id=:id', array(':id' => $record['studio_id']));            
                $data[$key]['studio_id']=$record['studio_id'];
                $data[$key]['studio_name']=$studio->name;
                $data[$key]['studio_email']=$user_logged_email;
            }
            $this->render('index',array('data'=>$data)); 
        }else{
            $url = Yii::app()->getBaseUrl(true);
            $this->redirect($url);
        }
    }
    public function actiongetData(){
        if (isset($_REQUEST['studio_id']) && $_REQUEST['studio_id']) {
            //get studio name
            $studio = Studio::model()->find('id=:id', array(':id' => $_REQUEST['studio_id']));
            $res = array('studio_name' => $studio->name);
            echo json_encode($res);exit;
            
        }
    }
}
