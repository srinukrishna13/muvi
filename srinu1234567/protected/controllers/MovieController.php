<?php

class MovieController extends Controller {

    public function init() {
        require_once(dirname(__FILE__) . '/ec/ecfunctions.php');
        parent::init();
    }

    public function actionshow() {
        if($_SERVER['HTTP_X_PJAX'] == true){            
                $this->layout = false;
        }
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        //$path = Yii::app()->getBaseUrl(true).Yii::app()->request->requestUri;
        
        
        $studio_id = Yii::app()->common->getStudiosId();
        $studio = $this->studio;
        $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
        $connection = Yii::app()->db;
        $movie = Yii::app()->db->createCommand()
                ->select('f.id,f.permalink as content_permalink,f.name,f.content_type_id,f.ppv_plan_id,f.content_types_id,m.id as stream_id,m.is_converted,m.full_movie, f.content_category_value, f.content_subcategory_value,f.mapped_id,m.mapped_stream_id')
                ->from('films f ,movie_streams m ')
                ->where('f.id =m.movie_id AND f.parent_id=0 AND m.is_episode=0 AND ((m.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(m.content_publish_date) = 0) OR (m.content_publish_date <=NOW())) AND f.id=:content_id AND f.studio_id=:studio_id', array(':content_id' => $_REQUEST['content_id'], ':studio_id' => $studio_id))
                ->queryAll();
        if (Yii::app()->common->isGeoBlockContent(@$_REQUEST['content_id'], @$movie[0]['stream_id'])) {//Auther manas@muvi.com
            if ($movie) {
                $movie_id = $movie[0]['id'];
				$contentPermalink = $movie[0]['content_permalink'];
                $ContentName = $movie_name = $movie[0]['name'];
                $meta = Yii::app()->common->detailspagemeta($_REQUEST['ctype'], $movie_name, $movie_id);
                $isConverted = 0;
                if (($movie[0]['full_movie'] != '') && ($movie[0]['is_converted'] == 1)) {
                    $isConverted = 1;
                }

                if ($meta['title'] != '') {
                    $title = $meta['title'];
                } else {
                    $temp = Yii::app()->request->url;
                    $temp = explode('/', $temp);
                    $permalink = $temp[count($temp) - 1];
                    $type = explode('-', $permalink);
                    $default_title = implode(' ', $type);
                    $default_title = ucwords($default_title);

                    $Words = preg_replace('/(?<!\ )[A-Z]/', ' $0', $default_title);
                    // echo $Words;exit;
                    $title = $Words . ' | ' . $studio->name;
                }

                if ($meta['description'] != '') {
                    $description = $meta['description'];
                } else {
                    $description = '';
                }
                if ($meta['keywords'] != '') {
                    $keywords = $meta['keywords'];
                } else {
                    $keywords = '';
                }
                eval("\$title = \"$title\";");
                eval("\$description = \"$description\";");
                eval("\$keywords = \"$keywords\";");
                $this->pageTitle = $title;
                $this->pageDescription = $description;
                $this->pageKeywords = $keywords;
                $default_currency_id = $this->studio->default_currency_id;
                
                $can_see_data['movie_id'] = $movie_id;
                $can_see_data['stream_id'] = $movie[0]['stream_id'];
                $can_see_data['studio_id'] = $studio_id;
                $can_see_data['user_id'] = $user_id;
                $can_see_data['default_currency_id'] = $default_currency_id;
                
                $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
                	
                $mov_warning = Yii::app()->common->getPlayMessage($mov_can_see);
                //$movie_payment_types = Yii::app()->common->getMoviePaymentType($movie_id);
                $movie_paid = Yii::app()->common->checkIfPaid($movie_id);

                //print_r($movie_payment_types);

                $comments = Yii::app()->general->comments($movie_id);
                

                $config = new StudioConfig();
                $subcat_config = $config->getconfigvalue('subcategory_enabled'); 
                if(isset($subcat_config) && $subcat_config > 0){
                    if(@$movie[0]['content_category_value']){
                        $sql = "SELECT binary_value, category_name, permalink FROM content_category WHERE studio_id={$studio_id} AND id IN (".$movie[0]['content_category_value'].") ORDER BY id_seq, category_name DESC LIMIT 1";
                        $cats = Yii::app()->db->createCommand($sql)->queryAll(); 
                        foreach($cats as $cat){
                            $categories[] = array(
                                'category_name' => $cat['category_name'],
                                'permalink' => $cat['permalink'],
                                'binary_value' => $cat['binary_value']
                            );
                        }
                    }
                    if(@$movie[0]['content_subcategory_value']){
                        $sql = "SELECT subcat_binary_value, subcat_name, permalink FROM content_subcategory WHERE studio_id={$studio_id} AND id IN (".$movie[0]['content_subcategory_value'].") ORDER BY id desc";
                        $subcats = Yii::app()->db->createCommand($sql)->queryAll(); 
                        foreach($subcats as $subcat){
                            $sub_categories[] = array(
                                'sub_category_name' => $subcat['subcat_name'],
                                'permalink' => $subcat['permalink'],
                                'subcat_binary_value' => $subcat['subcat_binary_value']
                            );
                        }
                    }
                }                
                
                $review_form = Yii::app()->general->reviewform($movie_id);                
                $comment_form = Yii::app()->general->commentform($movie_id);
                $review = Yii::app()->general->review($movie_id);
                $reviews = Yii::app()->general->reviews($movie_id);
                $reviewformonly = Yii::app()->general->reviewformonly($movie_id);
                $myreview = Yii::app()->general->getMyreview($movie_id, $studio_id, $user_id);
                $waterMarkOnPlayer = Yii::app()->general->playerWatermark();
                $extra_content = Yii::app()->custom->getExtracontent($studio_id);
                
                /*Advance Payments by RKS*/
                $item = json_decode($this->getMovieDetails($movie_id, 0), TRUE);  
				$trailer_path = '';
                $multipleVideo = array();
                $defaultResolution = 144;
                $is_subscribed = $payment_type = $adv_payment = 0;
                $price = array();

                $arg['studio_id'] = $this->studio->id;
                $arg['movie_id'] = $movie_id;
                $arg['season_id'] = 0;
                $arg['episode_id'] = 0;
                $arg['content_types_id'] = $item[0]['content_types_id'];
                $isFreeContent = Yii::app()->common->isFreeContent($arg);                
                if (intval($isFreeContent) == 0 && !empty(Yii::app()->common->isPaymentGatwayExists($studio_id))) {
                    if ($isConverted == 1 || $arg['content_types_id'] == 3 || $arg['content_types_id'] == 4) {
                        $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, '', 0);
                        if (isset($ppv->id) && intval($ppv->id)) {
                            $payment_type = $ppv->id;
                        } else {
                            $ppv = Yii::app()->common->getContentPaymentType($item[0]['content_types_id'], $item[0]['ppv_plan_id']);
                            $payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
                        }
                        if (intval($payment_type)) {
                            $price = Yii::app()->common->getPPVPrices($payment_type, $this->studio->default_currency_id);
                        }
                    } else {
                        $ppv = Yii::app()->common->checkAdvancePurchase($movie_id);
                        $adv_payment = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
                        if (intval($adv_payment)) {
                            $price = Yii::app()->common->getPPVPrices($adv_payment, $this->studio->default_currency_id);
                        }
                    }

                    if ($user_id > 0) {
                        $is_subscribed = Yii::app()->common->isSubscribed(Yii::app()->user->id);
                    }
                }    
                $pricing_text = "";
                if(! Yii::app()->common->getPPVBundle($movie_id)){
                    $pricing_text = Yii::app()->custom->showContentPricing($payment_type, $adv_payment, $price, $arg);
                }               
                $user_status['fav_status'] = 1;
                $user_status['login_status'] = 0;
                if($user_id){
                    $stat = UserFavouriteList::model()->getFavouriteContentStatus($movie_id,$studio_id,$user_id);
                    $user_status['fav_status'] = ($stat == 1)?0:1;
                    $user_status['login_status'] = 1;
                }
				$subscribed_price = Yii::app()->common->formatPrice($price['price_for_subscribed'], $price['currency_id']);
				$unsubscribed_price = Yii::app()->common->formatPrice($price['price_for_unsubscribed'], $price['currency_id']);
				

                // Check if Custom filed are there for this Content
                $customData = Yii::app()->Helper->getRelationalField($studio_id,1); 
                $data = Yii::app()->general->getContentData($movie_id,0,$customData);
                if(Yii::app()->general->getStoreLink()){
                    $pgProductList = PGProduct::getProductList(1,$studio_id,$movie_id);
                    unset($pgProductList['count']);
                    $default_currency_id = $this->studio->default_currency_id;
                    foreach ($pgProductList AS $key => $orderval) {
                        $contentproduct[$key] = $orderval->attributes;
                        $tempprice = Yii::app()->common->getPGPrices($orderval->id, $default_currency_id);
                        if(!empty($tempprice)){
                            $contentproduct[$key]['sale_price'] = $tempprice['price'];
                            $contentproduct[$key]['currency_id'] = $tempprice['currency_id'];
                        }
                    }
                }else{
                    $contentproduct = array();
                }
                $item = json_encode($data);

                if(Yii::app()->custom->showRelated($studio_id) == 1){
                    $related = Yii::app()->custom->getContentsFromBinaryValue($studio_id, $data['content_category_value']);                    
                }else{
                    $related = array();
                }                    
                Yii::app()->session['backbtnlink'] = Yii::app()->getBaseUrl(TRUE).'/'.$contentPermalink;
                $this->ogImage = @$data['original_poster'];
                $this->ogUrl = Yii::app()->getBaseUrl(TRUE).'/'.$contentPermalink;
                $this->is_twitter = 1;
                $this->contentDisplayName = $data['display_name'];
                $this->ogTitle = @$data['title'];
                $this->ogDescription = Yii::app()->common->strip_html_tags(@$data['story']);
                $default_series = 1;
                if ($movie[0]['content_types_id'] == 3) {
                    if($movie[0]['mapped_id']){
                        $episodes = movieStreams::model()->findByAttributes(array('movie_id' => $movie[0]['mapped_id'], 'is_episode' => 1, 'is_converted'=> 1));
                        $seasons = Yii::app()->general->getSeasonsToPlay($movie[0]['mapped_id'],$_SESSION[$studio_id]['parent_studio_id']);
                    }else{
                        $episodes = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id, 'is_episode' => 1, 'is_converted'=> 1));
                        $seasons = Yii::app()->general->getSeasonsToPlay($movie_id);
                    }
                    $ses = array();
                    foreach ($seasons as $season) {
                        $ses[] = array('series_number' => $season->series_number);
                    }

                    $default_series = $seasons[0]->series_number;
                    if(isset($_REQUEST['season'])){
                       $default_series = $_REQUEST['season'];  
                    }
                     if(isset($_REQUEST['unique_id'])){
                       $showconfirmModal = $_REQUEST['unique_id'];
                       $seasonID = $_REQUEST['season_id']; 
                       $showconfirmSessionid = $_SESSION['showconfirmuniqueid'];
                    }   

                   $multipart_cntpaging = StudioConfig::model()->getConfig($studio_id, 'multipart_content_paging');
                                            if($multipart_cntpaging && $multipart_cntpaging->config_value){
                                                    $pagination_limit = StudioConfig::model()->getConfig($studio_id, 'pagination_limit');
                                                    $page_size = 15;
                                                    if($pagination_limit && $pagination_limit->config_value){
                                                            $page_size = $pagination_limit->config_value;
                    }
                                                    $pagination_html='<input type="hidden" id="page_size" value="'.$page_size.'" />';

                                        }
                $review_form.=' <input type="hidden" name="show_season_confirm" id="show_season_confirm" value="'.$showconfirmModal.'" /><input type="hidden" name="show_season_confirm_sessionid" id="show_season_confirm_sessionid" value="'.$showconfirmSessionid.'" /><input type="hidden" name="seasonId" id="seasonId" value="'.$seasonID.'" />';
                    $this->render('showparent', array('myreview' => $myreview, 'pricing_text' => $pricing_text, 'default_series' => $default_series, 'reviews' => json_encode($reviews), 'reviewformonly' => $reviewformonly, 'review_summary' => $review, 'review_form' => $review_form, 'item' => $item, 'comments' => $comments, 'allseries' => json_encode($ses), 'isConverted' => $isConverted, 'multipleVideo' => $multipleVideo, 'defaultResolution' => $defaultResolution, 'comments' => $comments, 'mov_can_see' => $mov_can_see, 'v_logo' => $v_logo, 'mov_warning' => $mov_warning, 'movie_payment_types' => $movie_payment_types, 'movie_paid' => $movie_paid,'waterMarkOnPlayer' => $waterMarkOnPlayer, 'contentproduct' => $contentproduct, 'episode'=>$episodes,'pagination'=>@$pagination_html, 'extra_content' => $extra_content,'customData'=>$customData, 'categories' => @$categories, 'sub_categories' => @$sub_categories, 'user_status'=>$user_status, 'related' => json_encode($related)));
                } else {
                    $this->render('show', array('myreview' => $myreview, 'pricing_text' => $pricing_text, 'reviews' => json_encode($reviews), 'reviewformonly' => $reviewformonly, 'review_summary' => $review, 'review_form' => $review_form, 'item' => $item, 'comments' => $comments, 'isConverted' => $isConverted, 'multipleVideo' => $multipleVideo, 'defaultResolution' => $defaultResolution,  'comments' => $comments, 'mov_can_see' => $mov_can_see, 'v_logo' => $v_logo, 'mov_warning' => $mov_warning, 'movie_payment_types' => $movie_payment_types, 'movie_paid' => $movie_paid, 'waterMarkOnPlayer' => $waterMarkOnPlayer, 'contentproduct' => $contentproduct, 'extra_content' => $extra_content,'customData'=>$customData, 'categories' => @$categories, 'sub_categories' => @$sub_categories, 'user_status'=>$user_status, 'related' => json_encode($related),'subscribed_price'=>$subscribed_price,'unsubscribed_price'=>$unsubscribed_price));
                }
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Content doesn\'t exist');
                if ($_SERVER['HTTP_REFERER']) {
                    $this->redirect($_SERVER['HTTP_REFERER']);
                    exit;
                } else {
                    $this->redirect(Yii::app()->getBaseUrl(TRUE) . '/' . $_REQUEST['content_permalink']);
                    exit;
                }
           }
        }else{
            $content = '<div style="height:250px;text-align:center;"><h3>'.$this->Language["content_not_available_in_your_country"].'</h3></div>';
            $this->render('//layouts/geoblock_error',array('content'=>$content));
        }
    }

}
