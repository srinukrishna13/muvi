<?php

class ContactController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public $layout='main';     
    public function actionIndex()
    {
        $this->pageTitle = 'Contact Muvi to create your own Branded Video Streaming Site';
        $this->pageKeywords = 'Muvi, Muvi.com, Muvi Contact, Muvi Contact, Contact Muvi.com';
        $this->pageDescription = 'Contact us and learn how Muvi can help you launch your White label VoD Site similar to a Netflix Clone at Zero Cost!';
        $this->render('index');
    }

    public function actionSend(){
        $response = 'error';
        $msg = 'Sorry, there is some error in sending the email';  
        $contact_email = 'studio@muvi.com';
        
        $ret = Yii::app()->common->sendSampleEmail("ratikanta@muvi.com", "Contact Form Check in Muvi.com", $_REQUEST);
        
         $temporary_email_log = new TemporaryEmailLog();
         $temporary_email_log -> name = $_REQUEST['name'];
         $temporary_email_log -> email = $_REQUEST['email'];
         $temporary_email_log -> form_data = serialize($_REQUEST);
         $temporary_email_log -> page = 'contact_us';
         $temporary_email_log -> save();
         
        if(isset($_REQUEST) && count($_REQUEST) > 0 && ($_REQUEST['ccheck']=='') && ($_REQUEST['submit-btn']=='contact')){
	
            $name = isset($_REQUEST['name'])?trim($_REQUEST['name']):'';
            $email = isset($_REQUEST['email'])?trim($_REQUEST['email']):'';
            $phone = isset($_REQUEST['phone'])?trim($_REQUEST['phone']):'';
            $company = isset($_REQUEST['company'])?trim($_REQUEST['company']):'';
            $message = isset($_REQUEST['message'])?trim($_REQUEST['message']):'';
			if($message == '' || $email == '' || $phone == ''){
				$ret = array('status' => 'error', 'message' => 'Email, Phone, Message  can\'t be left blank!');
				echo json_encode($ret);exit;
			}
            $source = '';
            if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                $source = Yii::app()->request->cookies['REFERRER'];

                //Unset source cookie
                //unset($_COOKIE['REFERRER']);
                //setcookie('REFERRER', '', time() - 60000, '/', DOMAIN_COOKIE, false, false);
            }
            
            $contact_us = new ContactUs();
            $contact_us -> name = $name;
            $contact_us -> email = $email;
            $contact_us -> company = $company;
            $contact_us -> phone = $phone;
            $contact_us -> message = $message;
            $contact_us -> source = $source;
            $contact_us -> created_date = date('Y-m-d H:i:s');
            $contact_us -> ip = $_SERVER["REMOTE_ADDR"];
            $contact_us -> save();
            
            $to = array($contact_email);
            $to_name = 'Muvi';
            $from = 'support@muvi.com';
           // $from='studio@muvi.com';
            $from_name = 'Muvi Support';
            $site_url = Yii::app()->getBaseUrl(true);

            $logo = EMAIL_LOGO;
            $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
            
            $msg = '<p>'.$name.' has fill up the contact request in '.$to_name.'</p>';
            $msg.= '<p>';
            $msg.= '<b>Name :</b> '.$name.'<br />';
            $msg.= '<b>Email :</b> '.$email.'<br />';
            $msg.= '<b>Phone :</b> '.$phone.'<br />';
            $msg.= '<b>Company :</b> '.$company.'<br />';
            $msg.= '<b>Message :</b> '.$message.'<br />';
            $msg.= '</p>';
            
            $params = array(
                'website_name'=> $site_url,
                'logo'=> $logo,
                'msg'=> $msg
            );

            $subject = 'New contact from Muvi';
            $adminGroupEmail = array('sales@muvi.com');
                   
            $mailAddress = array(
                'subject' => $subject,
                'from_email' => $from,
                'from_name' => $from_name,
                'to' => $adminGroupEmail
            );            
            
            $template_name= 'studio_contact_us';
            //$this->mandrilEmail($template_name, $params, $mailAddress);              
         //New code for sending email by Amazon SES    
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/studio_contact_us',array('params'=>$params),true);
        $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$adminGroupEmail,$from,'','','',$name); 
        //New code for sending email by Amazon SES
            $response = 'success';
            $msg = 'Thank you for contacting us. We will respond to you as soon as possible.';
            $nm = explode(" ", $name, 2);
            Hubspot::AddToHubspot($email,$nm[0],$nm[1],$phone);
            
            $user = array('email' => $email, 'firstName' => $nm[0],'lastName'=>$nm[1],'add_list' => 'Muvi All');
            require('MadMimi.class.php');
            $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
            $mimi->AddUser($user);
        }
        $ret = array('status' => $response, 'message' => $msg);
        echo json_encode($ret);            
    }
    public function actionSendFromZopim(){
         $temporary_email_log = new TemporaryEmailLog();
         $temporary_email_log -> name = $_REQUEST['name'];
         $temporary_email_log -> email = $_REQUEST['email'];
         $temporary_email_log -> form_data = serialize($_REQUEST);
         $temporary_email_log -> page = 'zopim';
         $temporary_email_log -> save();
                 
        $email = $_POST['email'];
        $name  = $_POST['name'];
        $nm = explode(" ", $name, 2);
        Hubspot::AddToHubspot($email,$nm[0],$nm[1],'');
        $user = array('email' => $email, 'firstName' => $nm[0],'lastName'=>$nm[1],'add_list' => 'Muvi All');
        require('MadMimi.class.php');
        $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
        $mimi->AddUser($user);
    }
    
    public function actionWebinarsend(){

        $response = 'error';
        $msg = 'Sorry, there is some error in sending the email';  
        $contact_email = 'studio@muvi.com';
        $ret = Yii::app()->common->sendSampleEmail("ratikanta@muvi.com", "Webinar Form Check in Muvi.com", $_REQUEST);
         $temporary_email_log = new TemporaryEmailLog();
         $temporary_email_log -> name = $_REQUEST['name'];
         $temporary_email_log -> email = $_REQUEST['email'];
         $temporary_email_log -> form_data = serialize($_REQUEST);
         $temporary_email_log -> page = 'webinar';
         $temporary_email_log -> save();
         
        if(isset($_REQUEST) && count($_REQUEST) > 0 && ($_REQUEST['ccheck']=='') && ($_REQUEST['submit-btn']=='contact')){
   
            $name = isset($_REQUEST['name'])?trim($_REQUEST['name']):'';
            $email = isset($_REQUEST['email'])?trim($_REQUEST['email']):'';
            $phone = isset($_REQUEST['phone'])?trim($_REQUEST['phone']):'';
            $company = isset($_REQUEST['company'])?trim($_REQUEST['company']):'';
          
		   if($name == '' || $email == '' || $phone == ''){
			$ret = array('status' => 'error', 'message' => 'Email, Phone, Message  can\'t be left blank!');
			echo json_encode($ret);exit;
		   }
            $source = '';
            if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                $source = Yii::app()->request->cookies['REFERRER'];

                //Unset source cookie
                //unset($_COOKIE['REFERRER']);
                //setcookie('REFERRER', '', time() - 60000, '/', DOMAIN_COOKIE, false, false);
            }
             
            $webinar_register = new WebinarRegister();
            $webinar_register -> name = $name;
            $webinar_register -> email = $email;
            $webinar_register -> company = $company;
            $webinar_register -> phone = $phone;
            $webinar_register -> linksource = $source;
            $webinar_register -> created_date = date('Y-m-d H:i:s');
            $webinar_register -> ip = $_SERVER["REMOTE_ADDR"];
            $webinar_register -> save();
	    
	    $nm = explode(" ", $name, 2);
            Hubspot::AddToHubspot($email,$nm[0],$nm[1],$phone);
            
            $user = array('email' => $email, 'firstName' => $nm[0],'lastName'=>$nm[1],'add_list' => 'Muvi All');
            require('MadMimi.class.php');
            $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
            $mimi->AddUser($user);
            
            $to = $contact_email;
            $to_name = 'Muvi';
            $from = $email;
           // $from='studio@muvi.com';
            $from_name = $name;
            $site_url = Yii::app()->getBaseUrl(true);

            $logo = EMAIL_LOGO;
            $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
            $msg = '<p>';
            $msg.= '<b>Name :</b> '.$name.'<br />';
            $msg.= '<b>Email :</b> '.$email.'<br />';
            $msg.= '<b>Phone :</b> '.$phone.'<br />';
            $msg.= '<b>Company :</b> '.$company.'<br />';
            $msg.= '</p>';
            
            $params = array(
                'website_name' => $site_url,
                'logo'=> $logo,
                 'msg' => $msg
            );

            $subject = 'New registration for Webinar';
            $adminGroupEmail = array('sales@muvi.com');
                         
            $mailAddress = array(
                'subject' => $subject,
                'from_email' => $from,
                'from_name' => $from_name,
                'to' => $adminGroupEmail
            );            
	    $cc=array();
	    $bcc=array();           
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/Webinar_register',array('params'=>$params),true);
            $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$adminGroupEmail,$from,$cc,$bcc,'',$name);             

            $response = 'success';
            $msg = 'Thank you for registration. We will email you an invitation to join us live online.';
        }
        $ret = array('status' => $response, 'message' => $msg);
        echo json_encode($ret);            
    }
    public function actionHackathonRegister(){

        $response = 'error';
        $msg = 'Sorry, there is some error in sending the email';  
        $contact_email = 'studio@muvi.com';
        if(isset($_REQUEST) && count($_REQUEST) > 0 && ($_REQUEST['ccheck']=='') && ($_REQUEST['submit-btn']=='hackathon')){
            $email = isset($_REQUEST['email'])?trim($_REQUEST['email']):'';
            $source = '';
            if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                $source = Yii::app()->request->cookies['REFERRER'];
            }
             
            $hackathon_register = new HackathonRegister();
            $hackathon_register->email = $email;
            $hackathon_register->source = $source;
            $hackathon_register->created_date = date('Y-m-d H:i:s');
            $hackathon_register->ip = $_SERVER["REMOTE_ADDR"];
            $hackathon_register->save();
     
            $response = 'success';
            $msg = '<strong>Thank you for sharing your Email ID, we will email you once we announce our next Hackathon.</strong>';
        }
        $ret = array('status' => $response, 'message' => $msg);
        echo json_encode($ret);            
    }
        public function actionSavenewsletter(){
           $email = isset($_REQUEST['email_id'])?trim($_REQUEST['email_id']):''; 
           if($email!='') {
            $user = array('email' => $email,'add_list' => 'MUVI NEWSLETTER SUBSCRIBERS');
            require('MadMimi.class.php');
            $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
            $mimi->AddUser($user);
            echo "Succesfully saved to madmimi";
           }    
        }
    
}