<?php 
class MyLinkPager extends CLinkPager {

    public $customUrl = '';

    protected function createPageUrl($page) {
		
        $url = $this->getPages()->createPageUrl($this->getController(),$page);
		if($this->customUrl){
			if(strstr( $url,'/media/list')){
				$url = str_replace('/media/list', $this->customUrl, $url);
			}
		}
        return $url;
    }
}