<?php
class Movie extends AltActiveRecord {
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'films';
    }
    public function behaviors() {
        return array(
            'commentable' => array(
                'class' => 'ext.comment-module.behaviors.CommentableBehavior',
                // name of the table created in last step
                'mapTable' => 'movies_comments_nm',
                // name of column to related model id in mapTable
                'mapRelatedColumn' => 'movieId'
            ),
       );
    }
}
?>