<?php 
class LivestreamUsers extends CActiveRecord{
	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'livestream_users';
	}
	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
	
	 public function behaviors() {
            return array(
			'sluggable' => array(
				'class'=>'ext.behaviors.SluggableBehavior',
				'columns' => array('name','id'),
				'unique' => true,
				'update' => FALSE,
				'slugColumn' => 'channel_id'
            ),
           );
        }
}