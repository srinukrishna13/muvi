<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SeoInfo
 *
 * @author SKM<support@muvi.com>
 */
class ReportTemplate extends CActiveRecord{
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'report_template';
    }
    
    public function relations(){
        return array();
    }
    public function  getAvailableColumn(){
        $studio_id = Yii::app()->common->getStudiosId();
        $r_sql = "SELECT cl.label_name,cl.label_code,cr.type,cr.id FROM report_template as cr LEFT JOIN custom_lable_column as cl  on cl.id=cr.custom_label_id where cr.studio_id IN(0,$studio_id)";
       (array) $result = Yii::app()->db->createCommand($r_sql)->queryAll();
         return $result;
    }
     public function  getReportDetails($report_templateid){
         $studio_id = Yii::app()->common->getStudiosId();
        $report_sql = "SELECT cl.*,cr.*,cl.id as customlabel_id,cl.type as typel,cr.id as report_templateid FROM report_template as cr LEFT JOIN custom_lable_column as cl  on cl.id=cr.custom_label_id where cr.id=$report_templateid ";
       (array) $result = Yii::app()->db->createCommand($report_sql)->queryRow();
         return $result;
    }
    public function  getColumnDetails(){
        $studio_id = Yii::app()->common->getStudiosId();
        $r_sql = "SELECT cl.* FROM `custom_lable_column` as cl WHERE cl.id NOT IN (select custom_label_id from report_template where studio_id IN (0,$studio_id)) AND cl.type=1";
       (array) $result = Yii::app()->db->createCommand($r_sql)->queryAll();
         return $result;
    }
    public function  getLabelCodeCount($label_code){
       $studio_id = Yii::app()->common->getStudiosId();
        //$r_sql = "SELECT count(cl.label_code) as  countt,cr.* FROM `custom_lable_column` as cl LEFT JOIN report_template as cr on cl.id=cr.custom_label_id WHERE cl.label_code IN (select label_code from custom_lable_column) AND cr.studio_id IN(0,$studio_id) AND cl.label_code='".$label_code."'";
       $r_sql="select count(cl.label_code)as countt,cl.label_code,cr.* from custom_lable_column as cl LEFT JOIN report_template as cr on cl.id=cr.custom_label_id where cl.label_code='".$label_code."' AND cr.studio_id IN (0,$studio_id)";
       (array) $result = Yii::app()->db->createCommand($r_sql)->queryAll();
         return $result;   
     }
     
      public function  getTemplateColumnDetails($template_id){
       $studio_id = Yii::app()->common->getStudiosId();
          $r_sql = "SELECT cl.*,cr.report_template_id,r.id as reporttemplate_id,r.studio_id as studiotype,c.report_title,c.report_type FROM custom_report as c LEFT JOIN `custom_report_column` as cr on c.id=cr.custom_report_id LEFT JOIN report_template as r on r.id=cr.report_template_id LEFT JOIN custom_lable_column as cl on cl.id=r.custom_label_id where cr.custom_report_id=$template_id ORDER BY `cr`.`position` ASC ";
          (array) $result = Yii::app()->db->createCommand($r_sql)->queryAll();
          if(!empty($result))
          {
              return $result;   
          }
          else{
          $r_sql = "SELECT c.report_title,c.report_type FROM custom_report as c where c.id=$template_id";
          (array) $result = Yii::app()->db->createCommand($r_sql)->queryAll(); 
           return $result;   
          }
     }
     
      public function  getTemplateColumnDetailsCode($label_code){
         $studio_id = Yii::app()->common->getStudiosId();
         $r_sql = "SELECT count(cl.id) as  countid FROM custom_report as c LEFT JOIN `custom_report_column` as cr on c.id=cr.custom_report_id LEFT JOIN report_template as r on r.id=cr.report_template_id LEFT JOIN custom_lable_column as cl on cl.id=r.custom_label_id where cl.label_code='$label_code'";
        (array) $result = Yii::app()->db->createCommand($r_sql)->queryAll();
         return $result;   
     }
    
}
