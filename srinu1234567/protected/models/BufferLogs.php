<?php
class BufferLogs extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'bandwidth_log';
    }
    
    public function getBandwidthSize($studio_id = 0, $start_date = '', $end_date = '',$package)
    {
        $data = '';
        $res = 0;
        if (intval($studio_id) && (trim($start_date) != '0000-00-00') && (trim($end_date) != '0000-00-00')) {
            if(isset($package) && $package == 'muvi_studio'){
                $sql = "SELECT SUM(buffer_size) AS bandwidth_size FROM bandwidth_log WHERE studio_id = ".$studio_id." AND (DATE_FORMAT(created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') GROUP BY studio_id";
                $data = Yii::app()->db->createCommand($sql)->queryRow();
                $res = $this->bytesTogb($data['bandwidth_size']*1024,2);
            }elseif($package == 'muvi_studio_on_premise'){
                $sql = "SELECT SUM(played_length) AS watched_hour FROM video_logs WHERE studio_id = ".$studio_id." AND (DATE_FORMAT(created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') GROUP BY studio_id";
                $data = Yii::app()->db->createCommand($sql)->queryRow();
                $res = gmdate("H:i", $data['watched_hour']);
            }
        }
        return $res;
    }
    
    public function getHourBandwidth($studio_id,$movie_id = '',$video_id = 0,$dt,$searchKey,$deviceType)
    {
        $cond = "";
        if(isset($video_id) && $video_id){
            $cond = " AND bbl.video_id = ".$video_id;
        }
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        if($movie_id != ''){
            $qstr = ' AND bbl.movie_id IN ('.$movie_id.')';
        }else{
            $qstr = ' AND bbl.movie_id IN (-1)';
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (((f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%') OR (bbl.country LIKE '%".$searchKey."%')) OR (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%'))";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND bbl.device_type=".$deviceType;
        }
        //$sql = "SELECT SUM(total_played_time) AS played_time,SUM(total_buffered_size) AS bandwidth FROM `bandwidth_buffer_log` WHERE studio_id = ".$studio_id." ".$qstr.$cond." AND DATE_FORMAT(created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' GROUP BY studio_id";
        $sql = "SELECT SUM(total_played_time) AS played_time,SUM(total_buffered_size) AS bandwidth FROM `bandwidth_buffer_log` bbl,films f,movie_streams ms,sdk_users u WHERE bbl.studio_id = ".$studio_id." AND (DATE_FORMAT(bbl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ".$qstr.$cond." AND (bbl.movie_id = f.id AND (f.id<>0 OR f.id !='')) AND (bbl.video_id=ms.id OR (bbl.video_id=0 OR ISNULL(bbl.video_id))) AND bbl.user_id = u.id ".$searchStr.$deviceStr." GROUP BY bbl.studio_id";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }
    
    public function getVideoDetails($studio_id,$movie_id,$video_id = 0,$dt,$searchKey,$deviceType,$offset,$page_size,$is_partner = 0)
    {
        $cond = '';
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        if(trim($movie_id)){
            $cond .= " AND bl.movie_id IN (".$movie_id.")";
        }else{
            $cond .= " AND bl.movie_id IN (-1)";
        }
        
        if($video_id){
            $cond .= " AND bl.video_id=".$video_id;
            $cond_str = " AND ms.id=".$video_id;
        }else{
            $cond_str = '';
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND ((f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%') OR (bl.country LIKE '%".$searchKey."%') OR (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%'))";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND bl.device_type=".$deviceType;
        }
        if($is_partner){
            $sql = "SELECT SQL_CALC_FOUND_ROWS u.display_name,bl.id as bid,bl.created_date,bl.start_time,bl.end_time,bl.played_time,bl.resolution,bl.buffer_size,bl.city,bl.region,bl.country,bl.studio_id,f.name,ms.episode_title,bl.content_type FROM bandwidth_log bl,sdk_users u,films f,movie_streams ms WHERE bl.user_id = u.id AND bl.movie_id = f.id AND bl.studio_id = ".$studio_id.$cond." AND ms.movie_id=f.id ".$cond_str." AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ".$searchStr.$deviceStr." GROUP BY unique_id ORDER BY bl.created_date DESC"; 
        }else{
            $sql = "SELECT SQL_CALC_FOUND_ROWS u.display_name,bl.id as bid,bl.created_date,bl.start_time,bl.end_time,bl.played_time,bl.resolution,bl.buffer_size,bl.city,bl.region,bl.country,bl.studio_id,f.name,ms.episode_title,bl.content_type FROM bandwidth_log bl LEFT JOIN (sdk_users u) ON (bl.user_id = u.id) LEFT JOIN films f ON (bl.movie_id = f.id) LEFT JOIN movie_streams ms ON (f.id = ms.movie_id) LEFT JOIN movie_trailer mt ON (f.id = mt.movie_id) WHERE bl.studio_id = ".$studio_id." AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ".$cond.$searchStr.$deviceStr." GROUP BY unique_id ORDER BY bl.created_date DESC LIMIT ".$offset.",".$page_size;
        }
        $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $data;
    }
    
    public function getSearchedVideoDetails($studio_id,$movie_id,$key)
    {
        $sql = "SELECT u.display_name,bl.created_date,bl.start_time,bl.device_type,bl.end_time,bl.played_time,bl.resolution,bl.city,bl.region,bl.country,bl.buffer_size,f.name FROM bandwidth_log bl,sdk_users u,films f WHERE bl.user_id = u.id AND bl.movie_id = f.id AND bl.studio_id = ".$studio_id." AND bl.movie_id = ".$movie_id." AND u.display_name LIKE '%".$key."%' GROUP BY bl.unique_id ORDER BY bl.created_date DESC";
        $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    
    public function getVideoDetailsReport($dt,$studio_id,$movie_id,$video_id = 0,$searchKey,$deviceType)
    {
        $cond = '';
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $dt = json_decode($dt);
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        if(trim($movie_id)){
            $cond .= " AND bl.movie_id IN (".$movie_id.")";
        }
        if($video_id){
            $cond .= " AND bl.video_id=".$video_id;
            $cond_str = " AND ms.id=".$video_id;
        }else{
            $cond_str = '';
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND ((f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%') OR (bl.country LIKE '%".$searchKey."%') OR (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%'))";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND bl.device_type=".$deviceType;
        }
        $sql = "SELECT u.display_name,bl.created_date,bl.start_time,bl.end_time,bl.played_time,bl.resolution,bl.city,bl.region,bl.country,bl.buffer_size,f.name FROM bandwidth_log bl,sdk_users u,films f,movie_streams ms WHERE bl.user_id = u.id AND bl.movie_id = f.id AND bl.studio_id = ".$studio_id." AND ms.id = bl.video_id {$cond_str} AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ".$cond.$searchStr.$deviceStr." GROUP BY unique_id";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    
    public function bytesTogb($bytes, $precision = 2) { 
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        return round($bytes / $gigabyte, $precision);
    }
    
    public function getContentDetails($studio_id,$movie_id,$video_id)
    {
        $cond = "";
        if($video_id){
            $cond .= " AND ms.id=".$video_id;
        }
        if(trim($movie_id)){
            $cond .= " AND f.id IN (".$movie_id.")";
        }
        $sql = "SELECT f.name,ms.episode_title FROM films f,movie_streams ms WHERE f.studio_id= $studio_id AND f.id = ms.movie_id $cond";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }
    
    public function getTotalBufferDuration($studio_id,$movie_id = 0,$video_id = 0,$dt,$searchKey,$deviceType,$is_partner = 0)
    {
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        $cond = "";
        if (intval($is_partner)) {
            if($movie_id){
                $cond .= " AND bl.movie_id IN (".$movie_id.") ";
            }else{
                $cond .= " AND bl.movie_id IN (-1) ";
            }
        }else{
            if($movie_id){
                $cond .= " AND bl.movie_id=".$movie_id;
            }
        }
        if($video_id){
            $cond .= " AND bl.video_id=".$video_id;
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND ((bl.country LIKE '%".$searchKey."%') OR (f.name LIKE '%".$searchKey."%'))";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND bl.device_type=".$deviceType;
        }
        $sql = "SELECT SUM(played_time) AS buffered_time FROM bandwidth_log bl,films f WHERE bl.studio_id = ".$studio_id.$cond." AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') AND (bl.movie_id = f.id AND (f.id<>0 OR f.id !='')) ".$searchStr.$deviceStr." GROUP BY bl.studio_id";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }
    
    public function getVideoDetailsOfDevice($studio_id,$movie_id,$video_id = 0,$dt,$searchKey,$deviceType,$offset,$page_size)
    {
        $cond = '';
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        if(trim($movie_id)){
            $cond .= " AND bl.movie_id IN (".$movie_id.")";
        }else{
            $cond .= " AND bl.movie_id IN (-1)";
        }
        if($video_id){
            $cond .= " AND video_id=".$video_id;
            $cond_str = " AND ms.id=".$video_id;
        }else{
            $cond_str = '';
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND ((f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%') OR (bl.country LIKE '%".$searchKey."%'))";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND bl.device_type=".$deviceType;
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS bl.device_id,bl.id as bid,bl.created_date,bl.start_time,bl.end_time,bl.played_time,bl.resolution,bl.buffer_size,bl.city,bl.region,bl.country,bl.studio_id,f.name,ms.episode_title FROM bandwidth_log bl,sdk_users u,films f,movie_streams ms WHERE bl.movie_id = f.id AND bl.studio_id = ".$studio_id.$cond." AND ms.movie_id=f.id ".$cond_str." AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ".$searchStr.$deviceStr." GROUP BY unique_id ORDER BY bl.created_date DESC LIMIT ".$offset.",".$page_size;
        $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $data;
    }
    
    public function getTrailerVideoDetails($studio_id,$movie_id,$video_id = 0,$dt,$searchKey,$deviceType,$offset,$page_size,$is_partner = 0)
    {
        $cond = '';
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        if($is_partner){
            if(trim($movie_id)){
                $cond .= " AND bl.movie_id IN (".$movie_id.")";
            }else{
                $cond .= " AND bl.movie_id IN (-1)";
            }
        }
        if($video_id){
            $cond .= " AND bl.video_id=".$video_id;
            $cond_str = " AND ms.id=".$video_id;
        }else{
            $cond_str = '';
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND ((f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%') OR (bl.country LIKE '%".$searchKey."%') OR (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%'))";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND bl.device_type=".$deviceType;
        }
        $sql = "SELECT COUNT(bl.id) as trailer_count,bl.movie_id as movie_id FROM bandwidth_log bl LEFT JOIN (sdk_users u) ON (bl.user_id = u.id) LEFT JOIN films f ON (bl.movie_id = f.id) LEFT JOIN movie_streams ms ON (f.id = ms.movie_id) LEFT JOIN movie_trailer mt ON (f.id = mt.movie_id) WHERE bl.studio_id = ".$studio_id." AND bl.content_type = 2 AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ".$cond.$searchStr.$deviceStr." GROUP BY movie_id ORDER BY bl.created_date DESC LIMIT ".$offset.",".$page_size;
        $data= Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    public function getTrailerVideoDetailsReports($studio_id,$movie_id,$video_id = 0,$dt,$searchKey,$deviceType,$is_partner = 0)
    {
        $cond = '';
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $dt = stripcslashes($dt);
            $dt = json_decode($dt);
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        if($is_partner){
            if(trim($movie_id)){
                $cond .= " AND bl.movie_id IN (".$movie_id.")";
            }else{
                $cond .= " AND bl.movie_id IN (-1)";
            }
        }
        if($video_id){
            $cond .= " AND bl.video_id=".$video_id;
            $cond_str = " AND ms.id=".$video_id;
        }else{
            $cond_str = '';
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND ((f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%') OR (bl.country LIKE '%".$searchKey."%') OR (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%'))";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND bl.device_type=".$deviceType;
        }
        $sql = "SELECT COUNT(bl.id) as trailer_count,bl.movie_id as movie_id FROM bandwidth_log bl LEFT JOIN (sdk_users u) ON (bl.user_id = u.id) LEFT JOIN films f ON (bl.movie_id = f.id) LEFT JOIN movie_streams ms ON (f.id = ms.movie_id) LEFT JOIN movie_trailer mt ON (f.id = mt.movie_id) WHERE bl.studio_id = ".$studio_id." AND bl.content_type = 2 AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ".$cond.$searchStr.$deviceStr." GROUP BY movie_id ORDER BY bl.created_date DESC";
        $data= Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    
}