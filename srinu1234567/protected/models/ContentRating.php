<?php

/* 
 * Used to keep track of rating for the contents
 * table use : movie_ratings
 * Author: RKS
 */

class ContentRating extends CActiveRecord {
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'content_ratings';
    }
    public function relations()
    {
        return array(
            'content' => array(self::BELONGS_TO, 'Film', 'content_id'),
        );
    }     
	
}
?>
