<?php


class VideoManagement extends CActiveRecord
{

    public static function model($className = __CLASS__) 
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'video_management';
    }
    public function get_videodetails_by_id($rec_id)
    {
    $results = $this->findByPk($rec_id)->attributes;
    return $results;
    }
    public function get_videodetails_by_studio_id($studio_id,$content_id=0,$user_id=0,$offset=0,$page_size=16){
       if ($content_id && $user_id) {
            $criteria = new CDbCriteria;
            $criteria->select = 'SQL_CALC_FOUND_ROWS (0),t.*';
            $criteria->join = 'LEFT JOIN movie_streams ms ON t.id=ms.video_management_id';
            $criteria->condition = 't.studio_id=:studio_id AND (ms.movie_id IN(:movie_id) OR t.user_id= :user_id) and t.flag_deleted=0 and t.flag_uploaded = 0';
            $criteria->params = array(":studio_id" => $studio_id, ":movie_id" => $content_id, ":user_id" => Yii::app()->user->id);
            $criteria->limit = $page_size;
            $criteria->offset = $offset;
            $results['data'] = $this->findAll($criteria);
        } elseif ($user_id) {
            $results['data'] = Yii::app()->db->createCommand()
                    ->select('SQL_CALC_FOUND_ROWS (0),v.*')
                    ->from('video_management v')
                    ->where('v.studio_id=:studio_id and v.flag_deleted=:is_deleted and v.flag_uploaded =:is_uploaded and v.user_id=:user_id', array(':studio_id' => $studio_id, ':user_id' => $user_id, ':is_deleted' => 0, ':is_uploaded' => 0))
                    ->limit($page_size)
                    ->offset($offset)
                    ->queryAll();
        } else {
            $results['data'] = Yii::app()->db->createCommand()
                    ->select('SQL_CALC_FOUND_ROWS (0),v.*')
                    ->from('video_management v')
                    ->where('v.studio_id=:studio_id and v.flag_deleted=0 and v.flag_uploaded = 0', array(':studio_id' => $studio_id))
                    ->limit($page_size)
                    ->offset($offset)
                    ->queryAll();
        }
        $results['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        
        return $results;  
    }
    
    public function get_videodetails_by_keyword($keyword,$studio_id)
    {       
         $keyword = trim($keyword);
        if ($keyword != '') {
            $results = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('video_management')
                    ->where('video_name like :match and studio_id=:studio_id and flag_deleted=:is_deleted and flag_uploaded =:is_uploaded', array(':match' => '%' . $keyword . '%', ':studio_id' => $studio_id,':is_deleted' => 0, ':is_uploaded' => 0))                   
                    ->queryAll();
        } else {
            $results = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('video_management')
                    ->where(' studio_id=:studio_id and flag_deleted=:is_deleted and flag_uploaded =:is_uploaded', array(':studio_id' => $studio_id, ':is_deleted' => 0, ':is_uploaded' => 0))
                    ->queryAll();
        }
        
       return $results;  
    }
    public function get_videodetails_by_id_studio($rec_id,$studio_id)
    {
        
         $results = Yii::app()->db->createCommand()
                ->select('*')
                ->from('video_management')
                ->where('id=:id and studio_id=:studio_id', array(':id' => $rec_id, ':studio_id' => $studio_id))
                ->queryAll();
        return $results;
          
    }
    
    public function filter_video_files($studio_id,$cond, $offset = 0, $page_size = 16)
   {
        //SELECT SQL_CALC_FOUND_ROWS (0), vm.*, vs.id AS video_subtitle_id,ms.is_converted as movieConverted,mt.is_converted as trailerConverted FROM video_management vm LEFT JOIN video_subtitle vs ON (vm.id=vs.video_gallery_id) LEFT JOIN movie_streams ms ON (vm.id=ms.video_management_id) LEFT JOIN movie_trailer mt ON (vm.id=mt.video_management_id) WHERE ' . $cond . ' vm.studio_id=' . $studio_id . ' AND vm.flag_deleted=0 AND vm.flag_uploaded=0 GROUP BY vm.id  order by vm.video_name ASC
        $criteria = new CDbCriteria;
        $criteria->select = 'SQL_CALC_FOUND_ROWS (0),t.*,vs.id AS video_subtitle_id, ms.is_converted as movieConverted,mt.is_converted as trailerConverted';
        $criteria->join = 'LEFT JOIN video_subtitle vs ON (t.id=vs.video_gallery_id) LEFT JOIN movie_streams ms ON (t.id=ms.video_management_id) LEFT JOIN movie_trailer mt ON (t.id=mt.video_management_id)';
        $criteria->condition = $cond . ' t.studio_id=:studio_id AND t.flag_deleted=0 and t.flag_uploaded =0';
        $criteria->params = array(":studio_id" => $studio_id);
        $criteria->group = 't.id';
        $criteria->order = 't.video_name ASC';
        $criteria->limit = $page_size;
        $criteria->offset = $offset;
        $allvideo['data'] = $this->findAll($criteria);
        $allvideo['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $allvideo;
   }
   
   public function getPerticularColumnData($id, $studio_id, $columnName){
       $data = Yii::app()->db->createCommand()->select($columnName)->from('video_management')->where('id ='.$id,' and studio_id='.$studio_id)->queryRow();
       return $data;
       
   }
       
}
    ?>