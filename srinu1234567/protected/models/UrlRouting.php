<?php
class UrlRouting extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'url_routing';
    }
	
	function addUrlRouting($data='',$studio_id=''){
		if(!$studio_id){
			$studio_id = Yii::app()->common->getStudioId();
		}
		if(@$data['permalink'] && @$data['mapped_url']){ 
			$this->permalink = $data['permalink'];
			$this->mapped_url = $data['mapped_url'];
			$this->studio_id = $studio_id;
			$this->created_date = new CDbExpression("NOW()");
			$this-> setIsNewRecord(true);
			$this-> setPrimaryKey(NULL);
			$this->save();
			return true;
		}else{
			return false;
		}
	}
}

