<?php
class UserFavouriteList extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'user_favourite_list';
    }
    public function getFavouriteContentStatus($content_id,$studio_id,$user_id=NULL,$content_type='0'){
        $content_user_added_to_fav = 0;
        if($user_id){
            $favlist = $this->findByAttributes(array('content_id'=>$content_id,'content_type'=>$content_type,'studio_id'=>$studio_id,'user_id'=>$user_id,'status'=>'1'));
            if(!empty($favlist)){
                $content_user_added_to_fav = 1;
            }
        }
        return $content_user_added_to_fav;
    }
    public function getUsersFavouriteContentList($user_id, $studio_id = NULL, $page_size=NULL, $offset=NULL, $language_id){
        $list = array();
        $count = 0;
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudioId();
        }
        if ($page_size == NULL) {
            $command = Yii::app()->db->createCommand()
                    ->select('SQL_CALC_FOUND_ROWS (0),id,content_id,content_type')
                    ->from($this->tableName())
                    ->where('user_id = ' . $user_id . ' AND studio_id=' . $studio_id . ' AND status="1"')
                    ->order(array('id desc'));
        } else {
            $command = Yii::app()->db->createCommand()
                    ->select('SQL_CALC_FOUND_ROWS (0),id,content_id,content_type')
                    ->from($this->tableName())
                    ->where('user_id = ' . $user_id . ' AND studio_id=' . $studio_id . ' AND status="1"')
                    ->order(array('id desc'))
                    ->limit($page_size, $offset);
        }
        $data = $command->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        if (!empty($data)) {
            foreach ($data as $fav) {
                $is_episode = $fav['content_type'];
                $customData = array();
                $list[] = Yii::app()->general->getContentData($fav['content_id'],$is_episode,$customData,$language_id,$studio_id,$user_id,'',1 );
            }
        }
        $favlist = array(
            'list' => $list,
            'total' => $count
        );
        return $favlist;
    }
}

