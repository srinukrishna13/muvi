<?php
class Studios extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'studios';
    }
    
    public function getPaypalproStudios($studio_id = 0)
    {
        $start_date = date('Y-m-d');
        if(!$studio_id){
            $sql = "SELECT s.id AS studio_id,us.*,spg.short_code,sci.card_holder_name,sci.card_last_fourdigit FROM sdk_card_infos sci,studios s LEFT JOIN studio_payment_gateways spg ON s.id = spg.studio_id LEFT JOIN user_subscriptions us ON s.id = us.studio_id WHERE spg.short_code = 'paypalpro' AND s.status = 1 AND s.is_deleted = 0 AND us.status = 1 AND sci.user_id = us.user_id AND (DATE_FORMAT(us.start_date,'%Y-%m-%d') > '".$start_date."')";
        }else{
            $sql = "SELECT s.id AS studio_id,us.*,spg.short_code,sci.card_holder_name,sci.card_last_fourdigit FROM sdk_card_infos sci,studios s LEFT JOIN studio_payment_gateways spg ON s.id = spg.studio_id LEFT JOIN user_subscriptions us ON s.id = us.studio_id WHERE spg.short_code = 'paypalpro' AND s.status = 1 AND s.is_deleted = 0 AND us.status = 1 AND sci.user_id = us.user_id AND (DATE_FORMAT(us.start_date,'%Y-%m-%d') > '".$start_date."') AND s.id = ".$studio_id;
        }
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
}
