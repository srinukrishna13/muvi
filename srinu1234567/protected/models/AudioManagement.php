<?php

class AudioManagement extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'audio_gallery';
    }
    /*
     * Modified audio_gallery
     * Query Optimised by SD<suraja@muvi.com>
     * (Avi) 20-01-2011 ::: audio_gallery :: model updation
     */

    public function get_audiodetails_by_id($rec_id) {
        $results = $this->findByPk($rec_id)->attributes;
        return $results;
    }

    public function get_audiodetails_by_studio_id($studio_id, $content_id = 0, $user_id = 0, $offset = 0, $page_size = 16) {
        if ($content_id && $user_id) {
            $criteria = new CDbCriteria;
            $criteria->select = 'SQL_CALC_FOUND_ROWS (0),ag.*';
            $criteria->join = 'LEFT JOIN movie_streams ms ON ag.id=ms.video_management_id';
            $criteria->condition = 'ag.studio_id=:studio AND (ms.movie_id IN(:movie_id) OR ag.user_id= :user_id) and ag.is_deleted=0 and ag.is_uploaded = 0';
            $criteria->params = array(":studio_id" => $studio_id, ":movie_id" => $content_id, ":user_id" => Yii::app()->user->id);
            $criteria->limit = $page_size;
            $criteria->offset = $offset;
            $results['data'] = $this->findAll($criteria);
        } elseif ($user_id) {
            $results['data'] = Yii::app()->db->createCommand()
                    ->select('SQL_CALC_FOUND_ROWS (0),ag.*')
                    ->from('audio_gallery ag')
                    ->where('studio_id=:studio_id and is_deleted=:is_deleted and is_uploaded =:is_uploaded and user_id=:user_id', array(':studio_id' => $studio_id, ':user_id' => $user_id, ':is_deleted' => 0, ':is_uploaded' => 0))
                    ->limit($page_size)
                    ->offset($offset)
                    ->queryAll();
        } else {
            $results['data'] = Yii::app()->db->createCommand()
                    ->select('SQL_CALC_FOUND_ROWS (0), ag.*')
                    ->from('audio_gallery ag')
                    ->where('studio_id=:studio_id and is_deleted=0 and is_uploaded = 0', array(':studio_id' => $studio_id))
                    ->limit($page_size)
                    ->offset($offset)
                    ->queryAll();
        }
        $results['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $results;
    }

    public function get_audiodetails_by_keyword($keyword, $studio_id) {
        $keyword = trim($keyword);
        if ($keyword != '') {
            $results = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('audio_gallery')
                    ->where('audio_name like :match and studio_id=:studio_id and is_deleted=:is_deleted and is_uploaded =:is_uploaded', array(':match' => '%' . $keyword . '%', ':studio_id' => $studio_id,  ':is_deleted' => 0, ':is_uploaded' => 0))
                    ->queryAll();
        } else {
            $results = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('audio_gallery')
                    ->where('studio_id=:studio_id and is_deleted=:is_deleted and is_uploaded =:is_uploaded', array(':studio_id' => $studio_id, ':is_deleted' => 0, ':is_uploaded' => 0))
                    ->queryAll();
        }

        return $results;
    }

    public function get_audiodetails_by_id_studio($rec_id, $studio_id) {
        $results = Yii::app()->db->createCommand()
                ->select('*')
                ->from('audio_gallery')
                ->where('id=:id and studio_id=:studio_id', array(':id' => $rec_id, ':studio_id' => $studio_id))
                ->queryAll();
        return $results;
    }

   public function filter_audio_files($studio_id,$cond, $offset = 0, $page_size = 16)
   {
        $criteria = new CDbCriteria;
        $criteria->select = 'SQL_CALC_FOUND_ROWS (0),t.*, ms.is_converted as movieConverted';
        $criteria->join = 'LEFT JOIN movie_streams ms ON t.id=ms.video_management_id';
        $criteria->condition = $cond . ' t.studio_id=:studio_id AND t.is_deleted=0 and t.is_uploaded =0';
        $criteria->params = array(":studio_id" => $studio_id);
        $criteria->group = 't.id';
        $criteria->order = 't.audio_name ASC';
        $criteria->limit = $page_size;
        $criteria->offset = $offset;
        $allaudio['data'] = $this->findAll($criteria);
        $allaudio['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $allaudio;
   }
}

?>