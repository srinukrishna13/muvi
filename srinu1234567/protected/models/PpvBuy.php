<?php

class PpvBuy extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'ppv_buy';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function save_resseller_customer_studio_id($studio_id){
        $this->studio_id = $studio_id;
        $this->save();
    }

}
