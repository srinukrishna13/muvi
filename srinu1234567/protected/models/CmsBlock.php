<?php
class CmsBlock extends CActiveRecord {
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    public function tableName() {
        return 'cmsblocks';
    }
    public function getWidget($widget_id, $studio_id="", $language_id = 20){
        $widget = array();
        if(!$studio_id)
            $studio_id = Yii::app()->common->getStudiosId();
        if($widget_id){
            $widget = $this->find('studio_id=:studio_id AND id=:widget_id AND parent_id = 0', array(':studio_id' => $studio_id, ':widget_id' =>$widget_id));
            if($language_id !=20){
                $translated = $this->find('studio_id=:studio_id AND parent_id=:widget_id AND language_id = :language_id', array(':studio_id' => $studio_id, ':widget_id' =>$widget_id,':language_id' => $language_id), array('select'=>'parent_id,title,content'));
                if(!empty($translated)){
                    if ($widget->id  == $translated->parent_id) {
                        $widget->title = $translated->title;
                        $widget->content = $translated->content;
                     }
                }
            }
        }
        return $widget;
    }	
}
?>