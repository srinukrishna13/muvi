<?php
class MenuItem extends CActiveRecord {
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'menu_items';
    }
	
    public function getActiveContent($studio_id)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:id AND status=:active AND language_parent_id=0',array(':id' => $studio_id,':active' => '1'))
                ->queryAll();
        return $data;
    }
}
?>