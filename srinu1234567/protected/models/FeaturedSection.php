<?php
class FeaturedSection extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'featured_section';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::HAS_ONE, 'Studio','studio_id'),
            'template'=>array(self::HAS_ONE, 'Template', 'template_id'),
            'contents'=>array(self::HAS_MANY, 'FeaturedContent', 'section_id'),
        );
    }    
    public function getFeaturedSections($studio_id, $language_id = 20, $limit = ""){
        if($limit){
            $limit_content = " LIMIT ".$limit;
        }
        $sections = $this->findAll('studio_id=:studio_id AND parent_id = 0 ORDER BY id_seq '.$limit_content, array(':studio_id' => $studio_id));
        if($language_id !=20){
            $translated = CHtml::listData($this->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select'=>'parent_id,title')),'parent_id','title');
            if(!empty($sections)){
                foreach($sections as $section){
                    if (array_key_exists($section->id, $translated)) {
                       $section->title = $translated[$section->id];
                    }
                    $sectionsdetail[] = $section;
                }
            }
        }else{
            $sectionsdetail = $sections;
        }
        if($sectionsdetail){
            foreach ($sectionsdetail as $key => $sec) {
                $afsection[$key]['studio_id'] = $sec->studio_id;
                $afsection[$key]['language_id'] = $sec->language_id;
                $afsection[$key]['title'] = $sec->title;
                $afsection[$key]['id'] = $afsection[$key]['section_id'] =  $sec->id;
                $afsection[$key]['parent_id'] =  $sec->parent_id;
                $afsection[$key]['content_type'] = $sec->content_type;
                $total_content = AppFeaturedContent::model()->countByAttributes(array(
                    'section_id'=> $sec->id,
                    'studio_id' => $studio_id
                ));
                $afsection[$key]['total'] =  $total_content;
            }
            return $afsection;
        }
    }
}
