<?php

/**
 * This is the model class for table "movie_casts".
 *
 * The followings are the available columns in table 'movie_casts':
 * @property integer $id
 * @property integer $movie_id
 * @property integer $celebrity_id
 * @property string $cast_type
 * @property string $cast_name
 * @property integer $person_priority
 * @property string $ip
 * @property integer $created_by
 * @property string $created_date
 * @property integer $last_updated_by
 * @property string $last_updated_date
 */
class MovieCast extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movie_casts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('movie_id, celebrity_id, person_priority, created_by, last_updated_by', 'numerical', 'integerOnly'=>true),
			array('cast_type, cast_name', 'length', 'max'=>255),
			array('ip', 'length', 'max'=>30),
			array('created_date, last_updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, movie_id, celebrity_id, cast_type, cast_name, person_priority, ip, created_by, created_date, last_updated_by, last_updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'celebrity'=>array(self::BELONGS_TO, 'Celebrity','celebrity_id'),
                    'poster' => array(self::BELONGS_TO, 'Poster','celebrity_id',
                        'condition'=>"object_type='celebrity'")
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'movie_id' => 'Movie',
			'celebrity_id' => 'Celebrity',
			'cast_type' => 'Cast Type',
			'cast_name' => 'Cast Name',
			'person_priority' => 'Person Priority',
			'ip' => 'Ip',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'last_updated_by' => 'Last Updated By',
			'last_updated_date' => 'Last Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('movie_id',$this->movie_id);
		$criteria->compare('celebrity_id',$this->celebrity_id);
		$criteria->compare('cast_type',$this->cast_type,true);
		$criteria->compare('cast_name',$this->cast_name,true);
		$criteria->compare('person_priority',$this->person_priority);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('last_updated_by',$this->last_updated_by);
		$criteria->compare('last_updated_date',$this->last_updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MovieCasts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
