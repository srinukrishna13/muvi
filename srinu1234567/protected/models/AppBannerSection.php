<?php
class AppBannerSection extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'app_banner_sections';
    }
    public function getBannerDimension($studio_id){
        $banner_details = $this->findByAttributes(array('studio_id' => $studio_id), array('select' => 'banner_width, banner_height')); 
        if(empty($banner_details)){
            $banner_details = $this->findByAttributes(array('studio_id' => 0), array('select' => 'banner_width, banner_height')); 
        }
        return $banner_details;
    } 
    public function getAppThumbBannerDimension($studio_id = false){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $default_height = 100;
        $default_width  = 100;
        $newDimension   = self::getBannerDimension($studio_id);
        $width          = $newDimension['banner_width'];
        $height         = $newDimension['banner_height'];
        if($width > $height){
            $final_height = $default_height;
            $final_width  = round(($final_height/$height)*$width);
        }else{
            $final_width  = $default_width;
            $final_height = round(($final_width/$width)*$height);
        }
        $resp = array('width' => $final_width, 'height' => $final_height);
        return $resp;
    }
}
