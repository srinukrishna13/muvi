<?php
class Coupon extends CActiveRecord {
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'coupon';
    }
   public function getcoupon_data($studio_id){
        $db_user = Yii::app()->db;
        $coupon_sql = "SELECT *,(IF (UNIX_TIMESTAMP(used_date) = 0,'',DATE(used_date))) AS cused_date FROM coupon  where status=1 AND studio_id = ".$studio_id . "  ";
         $data = $db_user->createCommand($coupon_sql)->queryAll();
        $item_count = $db_user->createCommand($coupon_sql)->execute();
        //echo $item_count;exit;
        $pages =new CPagination($item_count);
        $pages->setPageSize($page_size);
        $end =($pages->offset+$pages->limit <= $item_count ? $pages->offset+$pages->limit : $item_count);
        $sample =range($pages->offset+1, $end);
        
    
       return $data;
       
       //
        // item count for pagination 
       
     }
     public function getcoupon_history($a){
       $db_user = Yii::app()->db;
       $history_sql = "SELECT * FROM coupon  where status=1 AND coupon_code= '".$a."' LIMIT 0, 1";
     //  echo $history_sql;exit;
     $history_data = $db_user->createCommand($history_sql)->queryAll();
       return $history_data;
     }
       public function delete_coupon($studio_id,$id){
       $db_user = Yii::app()->db;
       if($id!='')
       $delcoupon_sql = "UPDATE  coupon SET status=0 where studio_id=".$studio_id." AND id IN ({$id})";
       else
       $delcoupon_sql = "UPDATE  coupon SET status=0  where studio_id=".$studio_id;
       $command=$db_user->createCommand($delcoupon_sql);
        $command->execute();
       return $command;
     }
     
     public function getCouponSearchData($studio_id,$a){
       $db_user = Yii::app()->db;
       $history_sql = "SELECT * FROM coupon where status=1 AND studio_id='$studio_id' AND coupon_code LIKE '%".$a."%'";
      //echo $history_sql;exit;
       $history_data = $db_user->createCommand($history_sql)->queryAll();
       return $history_data;
     }
     public function getppvCoupondata($dt,$movie_id,$studio_id,$episodid=0){
         $db_user = Yii::app()->db;  
         if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
}
         $sql= "SELECT ppv.amount,ppv.coupon_code,ppv.studio_id,ppv.movie_id,ppv.season_id,ppv.episode_id FROM ppv_subscriptions ppv WHERE ppv.studio_id = '".$studio_id."'
               AND ppv.movie_id = '".$movie_id."' AND DATE_FORMAT(ppv.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'";
         $ppv_data = $db_user->createCommand($sql)->queryAll();
        return $ppv_data;
     }
     public function getppvCouponvalue($couponcode){
         $db_user = Yii::app()->db;
         $sql = "SELECT c.discount_type,c.discount_amount,cc.discount_amount as cash_amt FROM coupon c LEFT JOIN coupon_currency cc ON c.id = cc.coupon_id WHERE coupon_code = '".$couponcode."'";
         $coupon_val = $db_user->createCommand($sql)->queryAll();
         foreach($coupon_val as $key => $val){
             $coupon_value = array('discount_type'=>$val['discount_type'],'discount_amount'=>$val['discount_amount'],'cash_amount'=>$val['cash_amt']);
         }
         return $coupon_value;
     }
     public function getppvCoupondata_multicontent($dt,$movie_id,$studio_id,$episod_id){
       $db_user = Yii::app()->db;  
         if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
         $sql= "SELECT ppv.amount,ppv.coupon_code,ppv.studio_id,ppv.movie_id,ppv.season_id,ppv.episode_id FROM ppv_subscriptions ppv WHERE ppv.studio_id = '".$studio_id."'
               AND ppv.movie_id = '".$movie_id."' AND ppv.episode_id = '".$episod_id."' AND DATE_FORMAT(ppv.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'";
         $ppv_data = $db_user->createCommand($sql)->queryAll();
        return $ppv_data;
   
     }
}
?>