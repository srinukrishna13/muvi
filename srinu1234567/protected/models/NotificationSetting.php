<?php
class NotificationSetting extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'notification_settings';
    }
    
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    
    public function isEmailNotificationSettingsToStudio($type = NULL, $studio_id = 0) {
        if (intval($studio_id) == 0) {
            return TRUE;
        } else {
            $email_types = self::findByAttributes(array('studio_id' => $studio_id, 'type' => $type));
            if (!empty($email_types)) {
                if (intval($email_types->status)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                return TRUE;
            }
        }
    }
    
    public function isEmailNotificationRemoveDevice($studio_id = 0,$type = '') {
        $returnValue=FALSE;
        if($studio_id>0 && $type!=''){
            $email_types = self::findByAttributes(array('studio_id' => $studio_id, 'type' => $type));
            if(!empty($email_types)){
                if (intval($email_types->status)) {
                   $returnValue=TRUE; 
				}
            }
        }
        return $returnValue;
    }
}