<?php
class movieStreams extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
	public $total_series;
	public $series_number;
    public function tableName() {
        return 'movie_streams';
    }
    public function relations()
    {
        return array(
            'film' => array(self::BELONGS_TO, 'Film','movie_id')             
        );
    }    
    public function updateStratTime($movie_stream_id,$upload_start_time)
    {
        $data = Yii::app()->db->createCommand()
                ->update($this->tableName(),array('upload_start_time' => $upload_start_time,'upload_end_time'=>'','upload_cancel_time'=>'','encoding_start_time'=>'','encoding_end_time'=>'','encode_fail_time'=>''),'id=:movie_stream_id',array(':movie_stream_id'=>$movie_stream_id));
        return $data;
    }
    
    public function getEncodingStatus($streamIds) {
        $data = Yii::app()->db->createCommand()->select('count(*) as fileConverted')->from($this->tableName())->where('(is_converted=1 or is_converted=2) and is_download_progress=0 and id in ('.$streamIds.')')->queryAll();
        return $data[0]['fileConverted'];
    }
     public function get_available_episod($data = array()){
       $sql=Yii::app()->db->createCommand()
        ->select('id,episode_number,series_number,movie_id')
        ->from('movie_streams')
        ->where('movie_id=:movie_id and studio_id=:studio_id and series_number=:serise_number and is_episode =:isepisod',array(':movie_id'=>$data['movie_id'],':studio_id'=>$data['studio_id'],':serise_number' =>$data['series_number'],':isepisod'=>1))
        ->queryAll();
       return $sql;
}
    public function get_available_session($data = array()){
        $sql=Yii::app()->db->createCommand()
        ->select('series_number,id,movie_id,studio_id,episode_number ')  
        ->from('movie_streams')        
       ->where('movie_id=:movie_id and studio_id=:studio_id',array(':movie_id'=>$data['movie_id'],':studio_id'=>$data['studio_id']))   
        ->group('series_number')
       ->queryAll(); 
       return $sql;
                
    }
}