<?php

class CustomField extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'custom_field';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
    
    public function findAllFields($studio_id, $type = 1) {
        //Find All FAQs       
        $fields = CustomField::model()->findAll(array(
            'condition' => 'studio_id=:studio_id AND form_type = :form_type',
            'params' => array(':studio_id' => $studio_id, ':form_type' => $type),
            'order' => 't.id_seq ASC'
        ));          
        return($fields);
    }    

    public function findFieldByName($studio_id, $field_name) {
        //Find All FAQs       
        $field = CustomField::model()->find(array(
            'condition' => 'studio_id=:studio_id AND field_name = :field_name',
            'params' => array(':studio_id' => $studio_id, ':field_name' => $field_name),
            'order' => 't.id_seq ASC'
        ));          
        return($field);
    }     
}
