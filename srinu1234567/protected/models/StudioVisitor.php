<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StudioStorage
 *
 * @author Sanjeev<support@muvi.com>
 */
class StudioVisitor extends CActiveRecord{
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'studio_visitors_log';
    }
    
    public function relations(){
        return array();
    }
}