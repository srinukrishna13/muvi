<?php
class OldMovieStream extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'old_movie_streams';
    }
}