<?php

class ResellerBillingDetails extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'reseller_billing_details';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
    public function setBillingDetails($data= array()){
            if(count($data) >= 1){
                foreach($data as $key => $val){
                    $this->$key = $val;
                }
                $this->save();
            }
    }
    public function get_reseller_customer_platform_fee($billing_info_id){
        $sql = Yii::app()->db->createCommand()
                ->select('SUM(amount) as plat_form_fee')
                ->from('reseller_billing_details')
                ->where('billing_info_id=:bill_id',array(':bill_id'=>$billing_info_id))
                ->queryRow();
        return  $sql;
    }


}
