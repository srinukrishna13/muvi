<?php

/**
 * This is the model class for table "studio_country_restriction".
 *
 * The followings are the available columns in table 'studio_country_restriction':
 * @property integer $id
 * @property integer $studio_id
 * @property integer $muvi_id
 * @property string $country_code
 * @property string $ip
 * @property integer $created_by
 * @property string $created_date
 * @property integer $last_updated_by
 * @property string $last_updated_date
 */
class StudioCountryRestriction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'studio_country_restriction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('studio_id, muvi_id, created_by, last_updated_by', 'numerical', 'integerOnly'=>true),
			array('country_code', 'length', 'max'=>2),
			array('ip', 'length', 'max'=>30),
			array('created_date, last_updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, studio_id, muvi_id, country_code, ip, created_by, created_date, last_updated_by, last_updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
                return array(
                    'studio'=>array(self::HAS_MANY, 'Studio','studio_id'),
                );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'studio_id' => 'Studio',
			'muvi_id' => 'Muvi',
			'country_code' => 'Country Code',
			'ip' => 'Ip',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'last_updated_by' => 'Last Updated By',
			'last_updated_date' => 'Last Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('studio_id',$this->studio_id);
		$criteria->compare('muvi_id',$this->muvi_id);
		$criteria->compare('country_code',$this->country_code,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('last_updated_by',$this->last_updated_by);
		$criteria->compare('last_updated_date',$this->last_updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudioCountryRestriction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
