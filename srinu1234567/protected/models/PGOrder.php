<?php

class PGOrder extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'pg_order';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    /* 
     * This function is insert data into order table
     */
    public function insertOrder($studio_id,$buyer_id,$paytype,$cart_item,$shipping_addr,$ip){
        $ordernumber = date('dmy');        
        $item_total = 0;       
        foreach ($cart_item as $item) {
            $item_total += ($item["price"] * $item["quantity"]);
        }
        $this->order_reference_number = $paytype['order_reference_number'];
        $this->studio_id = $studio_id;
        $this->buyer_id = $buyer_id;
        $this->shipping_address = json_encode($shipping_addr);
        $this->total = $item_total;
        $this->tax = '';
        $this->order_status = 'pending';
        $this->payment_status = '';
        $this->payment_type = $paytype['payment_type'];
        $this->shipping_cost = $paytype['shipping_cost'];
        $this->discount = $paytype['discount'];
        $this->discount_type = $paytype['discount_type'];
        $this->grand_total = $paytype['amount'];
        $this->transactions_id = $paytype['transactions_id'];
        $this->coupon_code = @$paytype['coupon_code'];
        $this->hear_source = @$paytype['hear_source'];
        $this->created_date = new CDbExpression("NOW()");
        $this->ip = $ip;
        if($this->save()){
           $last_inserted_id=$this->id;
        }
        $this->order_number = $ordernumber.$last_inserted_id;
        $this->save();
        $couponDetails = Coupon::model()->findByAttributes(array('coupon_code' => @$paytype['coupon_code']));
        if(isset($couponDetails) && !empty($couponDetails)){
            if($couponDetails->coupon_type ==1 && $couponDetails->used_by != 0){
                $qry = "UPDATE coupon SET used_by='".$couponDetails->used_by.",".$buyer_id."',used_date = NOW() WHERE coupon_code='".$paytype['coupon_code']."'";
            }else{
                $qry = "UPDATE coupon SET used_by=".$buyer_id.",used_date = NOW() WHERE coupon_code='".$paytype['coupon_code']."'";
            }
            Yii::app()->db->createCommand($qry)->execute();
        }
        return $last_inserted_id;
    }
    public function getOrderStatus(){
        $status_arr = Yii::app()->db->createCommand("SELECT order_status_id,name FROM pg_order_status ORDER BY order_status_id ASC")->queryAll();
        foreach ($status_arr as $k=>$v){
            $status[$v['order_status_id']] = $v['name'];
        }
        return $status;
    }
    public function getOrderStatusId($status,$belongsto){
        $status_arr = Yii::app()->db->createCommand("SELECT order_status_id FROM pg_order_status WHERE name='".$status."' AND belongs_to='".$belongsto."'")->queryAll();
        return $status_arr[0]['order_status_id'];
    }
    /*
     * This function create an order at CDS
     * author ajit@muvi.in
     * date 22/8/2016
     */
    public function CreateCDSOrder($studio_id,$orderid){
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
        $site_id = $webservice->user_id;
        $password = $webservice->password;
        $pgorderval = PGOrder::model()->findByPk($orderid);
        //get the billing /shipping details
        $pgbillingval = Yii::app()->db->createCommand("SELECT * FROM pg_shipping_address WHERE order_id = ".$orderid)->queryAll();
        
        $pgitemsval = Yii::app()->db->createCommand("SELECT id,product_id,quantity,price FROM pg_order_details WHERE order_id = ".$orderid)->queryAll();
        //get the transaction id from pg_order table
        $transid = PGOrder::model()->find(array('select'=>'transactions_id,coupon_code','condition' => 'studio_id=:studio_id and id=:id','params' => array(':studio_id' =>$studio_id,':id'=>$orderid)));
        //get the transaction details from transaction table
        $transdetails = Transaction::model()->find(array('select'=>'payment_method,invoice_id,order_number,amount','condition' => 'studio_id=:studio_id and id=:id','params' => array(':studio_id' =>$studio_id,':id'=>$transid['transactions_id'])));
        $all_transdetails = "Payment method -".$transdetails['payment_method']." Invoice id-".$transdetails['invoice_id'];
        //source
        $pgsource = Yii::app()->db->createCommand("SELECT sources FROM pg_customer_source WHERE id = ".$pgorderval->hear_source)->queryAll();
        $xml_data_createOrder = "
            <app:createOrder>
             <app:siteId>$site_id</app:siteId>
             <app:password>$password</app:password>
             <app:createOrder>
                &lt;OrderBundle&gt;
                  &lt;Order&gt;
                    &lt;OrderNumber&gt;".$pgorderval->order_number."&lt;/OrderNumber&gt;
                    &lt;CustomerNumber&gt;".$pgorderval->buyer_id."&lt;/CustomerNumber&gt;
                    &lt;SiteID&gt;".$site_id."&lt;/SiteID&gt;
                    &lt;DateCreated&gt;".$pgorderval->created_date."&lt;/DateCreated&gt;
                    &lt;BillingName&gt;".$pgbillingval[0]['first_name']." ".$pgbillingval[0]['last_name']."&lt;/BillingName&gt;
                    &lt;BillingEmail&gt;".$pgbillingval[0]['email']."&lt;/BillingEmail&gt;
                    &lt;BillingAddress1&gt;".$pgbillingval[0]['address']."&lt;/BillingAddress1&gt;
                    &lt;BillingCity&gt;".$pgbillingval[0]['city']."&lt;/BillingCity&gt;
                    &lt;BillingState&gt;&lt;/BillingState&gt;
                    &lt;BillingPostalCode&gt;".$pgbillingval[0]['zip']."&lt;/BillingPostalCode&gt;
                    &lt;BillingCountry&gt;".$pgbillingval[0]['country']."&lt;/BillingCountry&gt;
                    &lt;ShipName&gt;".$pgbillingval[0]['first_name']." ".$pgbillingval[0]['last_name']."&lt;/ShipName&gt;
                    &lt;ShippingEmail&gt;".$pgbillingval[0]['email']."&lt;/ShippingEmail&gt;
                    &lt;ShippingPhone&gt;".$pgbillingval[0]['phone_number']."&lt;/ShippingPhone&gt;
                    &lt;ShipAddress1&gt;".$pgbillingval[0]['address']."&lt;/ShipAddress1&gt;
                    &lt;ShipCity&gt;".$pgbillingval[0]['city']."&lt;/ShipCity&gt;
                    &lt;ShipState&gt;&lt;/ShipState&gt;
                    &lt;ShipPostalCode&gt;".$pgbillingval[0]['zip']."&lt;/ShipPostalCode&gt;
                    &lt;ShipCountry&gt;".$pgbillingval[0]['country']."&lt;/ShipCountry&gt;
                    &lt;OrderLanguage&gt;&lt;/OrderLanguage&gt;
                    &lt;OrderCurrency&gt;&lt;/OrderCurrency&gt;
                    &lt;Remark&gt;&lt;/Remark&gt;
                    &lt;FreeShipping&gt;N&lt;/FreeShipping&gt;
                    &lt;ShippingFlatFeeBT&gt;0&lt;/ShippingFlatFeeBT&gt;
                    &lt;PaymentTerm&gt;".$all_transdetails."&lt;/PaymentTerm&gt;
                    &lt;PaymentReference&gt;".$transdetails['order_number']."&lt;/PaymentReference&gt;
                    &lt;SourceCode&gt;".$pgsource[0]['sources']."&lt;/SourceCode&gt;
                    &lt;PromotionCode&gt;".strtoupper($transid['coupon_code'])."&lt;/PromotionCode&gt;";        
        
        $countitem = 0;
        foreach($pgitemsval AS $items)
        {
            $countitem++;
            //sku no = product id of cds or any common reference for items
            $pgproduct = PGProduct::model()->find('id=:id', array(':id' => $items['product_id']));
            $free = ($pgproduct['is_free_offer']==0)?'R':'F';
            //pass $pgproduct['sku'] in ProductID;
            $xml_data_createOrder .= "            
                    &lt;OrderLine&gt;                      
                      &lt;OrderNumber&gt;".$pgorderval->order_number."&lt;/OrderNumber&gt;
                      &lt;LineID&gt;".$items['id']."&lt;/LineID&gt;
                      &lt;ProductID&gt;".$pgproduct['sku']."&lt;/ProductID&gt;
                      &lt;Quantity&gt;".$items['quantity']."&lt;/Quantity&gt;
                      &lt;UnitPriceBT&gt;".$items['price']."&lt;/UnitPriceBT&gt; 
                      &lt;SalesCode&gt;".$free."&lt;/SalesCode&gt;
                    &lt;/OrderLine&gt;";
        }
        $xml_data_createOrder .= "            
                &lt;TotalLines&gt;".$countitem."&lt;/TotalLines&gt;
              &lt;/Order&gt;
            &lt;/OrderBundle&gt;</app:createOrder>
            </app:createOrder>
        ";        
        $value =  CDS::CreateOrder($xml_data_createOrder);
        $pgorderval->cds_order_status = $value;
        $pgorderval->save();
    }
    /*
     * This function check the available stock of CDS
     * author ajit@muvi.in
     * date 23/8/2016
     * this function will be run by cron job
     */
    public function CheckCDSStock($studio_id){        
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
        $site_id = $webservice->user_id;
        $password = $webservice->password;
        $pgproduct = Yii::app()->db->createCommand("SELECT sku FROM pg_product WHERE studio_id = ".$studio_id." AND is_deleted=0")->queryAll();;
        
        if($pgproduct)
        {
            $xml_data_checkStock = "
                <app:checkStock>
                    <app:siteId>$site_id</app:siteId>
                    <app:password>$password</app:password>
                    <app:checkStock>&lt;SKUList&gt;
                    &lt;account&gt;$site_id&lt;/account&gt;";
            foreach($pgproduct AS $val)
            {
                $xml_data_checkStock .= "
                    &lt;SKU&gt;".$val['sku']."&lt;/SKU&gt;
                ";
            }
            $xml_data_checkStock .= "&lt;/SKUList&gt;</app:checkStock></app:checkStock>";

            $value =  CDS::CheckStock($xml_data_checkStock);
            //insert to pg_cds_stock_status
            if($value)
            {
                $stock = new PGStockStatus;
                foreach($value as $val)
                {
                    //get the product id of this sku item
                    $pgproduct = PGProduct::model()->find('sku=:sku', array(':sku' => $val['skufound']));
                    //delete the value before save
                    PGStockStatus::model()->deleteAll('sku =:sku', array(':sku' => $val['skufound']));

                    $stock->sku = $val['skufound'];                
                    $stock->product_id = $pgproduct->id;
                    $stock->qtyavailable = $val['qtyavailable'];
                    $stock->qtyonhand = $val['qtyonhand'];
                    $stock->statusofitem = $val['statusofitem'];
                    $stock->itemdescription = $val['itemdescription'];
                    $stock->checked_at = gmdate('Y-m-d H:i:s');
                    $stock->isNewRecord = TRUE;
                    $stock->primaryKey = NULL;
                    $stock->save();                
                }
            }
        }
    }
    /*
     * This function check the available stock of CDS
     * author ajit@muvi.in
     * date 9/9/2016
     * this function will be run by cron job
     * can be run in 2 way
     * 1 - ORDERLIST - send order number(s)and get those status
     * 2 - STATUSCHANGED - cds return all the orders whose status is changed
     * Method 2 added for single status. Ajit-2/12/16
     */
    public function CheckItemStatus($studio_id){        
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
        $site_id = $webservice->user_id;
        $password = $webservice->password;
        $pgproduct = Yii::app()->db->createCommand("SELECT order_number FROM pg_order WHERE studio_id = ".$studio_id)->queryAll();
        if($pgproduct)
        {
            $xml_data_getOrderStatus = "
                <app:getOrderStatus>
                    <app:siteId>$site_id</app:siteId>
                    <app:password>$password</app:password>
                    <app:orderStatus>
                        &lt;StatusUpdateRequest&gt;
                        &lt;SiteID&gt;$site_id&lt;/SiteID&gt;
                        &lt;Type&gt;STATUS_CHANGED&lt;/Type&gt;
                        &lt;Orders&gt;";
            foreach($pgproduct AS $val)
            {
                $xml_data_getOrderStatus .= "
                    &lt;Order&gt;
                        &lt;OrderNumber&gt;".$val['order_number']."&lt;/OrderNumber&gt;
                    &lt;/Order&gt;
                ";
            }  
            $xml_data_getOrderStatus .= "&lt;/Orders&gt;
                        &lt;/StatusUpdateRequest&gt;
                    </app:orderStatus>
                </app:getOrderStatus>"; 
            $value=array();
            $value =  CDS::GetItemStatus($xml_data_getOrderStatus); 
            
            if($value)
            {   
                $stocks = new PGOrderDetails;
                $mail='N';
                foreach($value as $ordline)
                {   
                    $mail='N';
                    //method 1 multi dimensional
                    if(count($ordline['OrderLine']) && $ordline['OrderLine']['LineID']!='')
                    {   
                        if($ordline['OrderLine']['LineID'])
                        {  
                            $stock = $stocks->findByPk($ordline['OrderLine']['LineID']);
                            if($stock)
                            {                          
                                //find the numeric status id from database
                                $current_status= self::getOrderStatusId($ordline['OrderLine']['Status'],'CDS');
                                //update the item status if current status is changed
                                if($stock->item_status != $current_status)
                                {                            
                                    $stock->shipping_cost = $ordline['OrderLine']['ShippingCost'];
                                    $stock->tax = $ordline['OrderLine']['ShippingTax'];
                                    $stock->item_status = $current_status;
                                    $stock->cds_invoice_no = json_encode(($value['InvoiceNo']));
                                    $stock->cds_update_date = gmdate('Y-m-d H:i:s');
                                    $stock->cds_shipping_method = $ordline['OrderLine']['ShippingMethod'];
                                    $stock->cds_shipped_quantity = $ordline['OrderLine']['ShippedQTY'];
                                    $stock->cds_tracking = $ordline['OrderLine']['Tracking'];
                                    $stock->cds_consignment_no = $ordline['OrderLine']['Consignment_no'];
                                    $stock->cds_despatch_no = $ordline['OrderLine']['Despatch_no'];
                                    $stock->is_mail_send = 1;
                                    if($stock->save()){
                                        //set values for email                                    
                                        if($ordline['OrderLine']['Status'] == 'Shipped')
                                        {
                                            $status ='ordershipped';
                                            $mail='Y';
                                        }
                                        if($val['Status'] == 'Cancelled')
                                        {
                                            $status ='ordercancel';
                                        }
                                        //get the currency id of this order from transaction table; multicurrency
                                        
                                        $trid = PGOrder::model()->find(array('select'=>'transactions_id','condition' => 'id = :id','params' => array(':id' =>$stock->order_id)));       
                                        $curid = Transaction::model()->find(array('select'=>'currency_id','condition' => 'id = :id','params' => array(':id' =>$trid->transactions_id)));
                                        $req['orderid'] = $stock->order_id;                            
                                        $req['emailtype'] = $status;
                                        $req['studio_id'] = $studio_id;
                                        $req['studio_name'] = $this->studio->name;
                                        $req['currency_id'] = $curid->currency_id;
                                    }
                                }
                            }
                        }
                    }
                    //method2 single dimensanol
                    if($ordline['LineID'] != '' && $ordline['Status']!='')
                    {  
                        $mail='N';
                        $stock = $stocks->findByPk($ordline['LineID']);
                        if($stock)
                        {                          
                            //find the numeric status id from database
                            $current_status= self::getOrderStatusId($ordline['Status'],'CDS');
                            //update the item status if current status is changed
                            if($stock->item_status != $current_status)
                            {                            
                                $stock->shipping_cost = $ordline['ShippingCost'];
                                $stock->tax = $ordline['ShippingTax'];
                                $stock->item_status = $current_status;
                                $stock->cds_invoice_no = json_encode(($value['InvoiceNo']));
                                $stock->cds_update_date = gmdate('Y-m-d H:i:s');
                                $stock->cds_shipping_method = $ordline['ShippingMethod'];
                                $stock->cds_shipped_quantity = $ordline['ShippedQTY'];
                                $stock->cds_tracking = $ordline['Tracking'];
                                $stock->cds_consignment_no = $ordline['Consignment_no'];
                                $stock->cds_despatch_no = $ordline['Despatch_no'];
                                $stock->is_mail_send = 1;
                                if($stock->save()){
                                    //set values for email                                    
                                    if($ordline['Status'] == 'Shipped')
                                    {
                                        $status ='ordershipped';
                                        $mail='Y';
                                    }
                                    if($ordline['Status'] == 'Cancelled')
                                    {
                                        $status ='ordercancel';
                                    }
                                    //get the currency id of this order from transaction table; multicurrency
                                    $trid = PGOrder::model()->find(array('select'=>'transactions_id','condition' => 'id = :id','params' => array(':id' =>$stock->order_id)));       
                                    $curid = Transaction::model()->find(array('select'=>'currency_id','condition' => 'id = :id','params' => array(':id' =>$trid->transactions_id)));
                                    $req['orderid'] = $stock->order_id;                            
                                    $req['emailtype'] = $status;
                                    $req['studio_id'] = $studio_id;
                                    $req['studio_name'] = $this->studio->name;
                                    $req['currency_id'] = $curid->currency_id;
                                }
                            }
                        }
                    }
                    //send mail for this order
                    if($mail=='Y'){
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $studio_id);                
                        if ($isEmailToStudio) {
                            Yii::app()->email->pgEmailTriggers($req);
                        }
                    }
                }
            }
        }
    }   
    public function CheckItemStatusMuvi($studio_id){        
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
        $site_id = $webservice->user_id;
        $password = $webservice->password;
        $pgproduct = Yii::app()->db->createCommand("SELECT order_number FROM pg_order WHERE studio_id = ".$studio_id)->queryAll();;
        if($pgproduct)
        {
            $xml_data_getOrderStatus = "
                <app:getOrderStatus>
                    <app:siteId>$site_id</app:siteId>
                    <app:password>$password</app:password>
                    <app:orderStatus>
                        &lt;StatusUpdateRequest&gt;
                        &lt;SiteID&gt;$site_id&lt;/SiteID&gt;
                        &lt;Type&gt;ORDERLIST&lt;/Type&gt;
                        &lt;Orders&gt;";                 
            foreach($pgproduct AS $val)
            {
                $xml_data_getOrderStatus .= "
                    &lt;Order&gt;
                        &lt;OrderNumber&gt;".$val['order_number']."&lt;/OrderNumber&gt;
                    &lt;/Order&gt;
                ";
            }  
            $xml_data_getOrderStatus .= "&lt;/Orders&gt;
                        &lt;/StatusUpdateRequest&gt;
                    </app:orderStatus>
                </app:getOrderStatus>"; 
            $value=array();
            $value =  CDS::GetItemStatus($xml_data_getOrderStatus); 
            print_r($value);exit;
            if($value)
            {   //print_r($value);
                $stocks = new PGOrderDetails;
                $mail='N';
                foreach($value as $ordline)
                {   
                    $mail='N';
                    foreach($ordline['OrderLine'] as $val)
                    {   print_r($val);
                        if($val['LineID'])
                        {  
                            $stock = $stocks->findByPk($val['LineID']);
                            if($stock)
                            {                          
                                //find the numeric status id from database
                                $current_status= self::getOrderStatusId($val['Status'],'CDS');
                                //update the item status if current status is changed
                                if($stock->item_status != $current_status)
                                {                                    
                                    $stock->shipping_cost = $val['ShippingCost'];
                                    $stock->tax = $val['ShippingTax'];
                                    $stock->item_status = $current_status;
                                    $stock->cds_invoice_no = json_encode(($value['InvoiceNo']));
                                    $stock->cds_update_date = gmdate('Y-m-d H:i:s');
                                    $stock->cds_shipping_method = $val['ShippingMethod'];
                                    $stock->cds_shipped_quantity = $val['ShippedQTY'];
                                    $stock->cds_tracking = $val['Tracking'];
                                    $stock->cds_consignment_no = $val['Consignment_no'];
                                    $stock->cds_despatch_no = $val['Despatch_no'];
                                    $stock->is_mail_send = 1;
                                    /*
                                    if($stock->save()){
                                        //set values for email                                    
                                        if($val['Status'] == 'Shipped')
                                        {
                                            $mail='Y';
                                            $status ='ordershipped';
                                        }
                                        if($val['Status'] == 'Cancelled')
                                        {
                                            $status ='ordercancel';
                                        }
                                        //get the currency id of this order;
                                        $cid = PGProduct::model()->find(array('select'=>'currency_id','condition' => 'id = :id','params' => array(':id' =>$stock->order_id)));
                                        $req['orderid'] = $stock->order_id;                            
                                        $req['emailtype'] = $status;
                                        //$req['item_id'] = $val['LineID'];
                                        $req['studio_id'] = $studio_id;
                                        $req['studio_name'] = $this->studio->name;
                                        $req['currency_id'] = $cid['currency_id'];
                                        
                                    }*/
                                }
                            }
                        }
                    }
                    //send mail for this order
                    if($mail=='Y'){
                        //Yii::app()->email->pgEmailTriggers($req);
                    }
                }
            }
        }
    }
    
    public function calculateShippingCost($studio_id,$ship_address,$ship_item,$ship_method,$currency){
        
        $size =array();
        foreach($ship_item as $shi){
            $size[] = $shi['size'];
        }
        $size = array_unique($size);
        if (in_array("large", $size)){
            $selected_size = "large";
        }
        else if(in_array("medium", $size)){
            $selected_size = "medium";
        }
        else if(in_array("small", $size)){
            $selected_size = "small";
        }
        $scost = PGShippinCost::model()->find(array('select'=>'currency,shipping_cost','condition' => 'studio_id=:studio_id and country=:country and method=:method and size=:size and currency=:currency and is_deleated=0','params' => array(':studio_id' =>$studio_id,':country'=>$ship_address['country'],':method'=>$ship_method,':size'=>$selected_size,':currency'=>$currency)));
        if(!empty($scost)){
            $shippingcost['currency']=$scost->currency;
            $shippingcost['shipping_cost']=$scost->shipping_cost;
        }else{
            $shippingcost=array();
        }
        return $shippingcost;
    }
}

