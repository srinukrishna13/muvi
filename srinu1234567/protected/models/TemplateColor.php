<?php
class TemplateColor extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'template_colors';
    }
    public function relations()
    {
           return array(
               'template'=>array(self::HAS_MANY, 'Template','template_id'),
           );
    }      
}
