<?php

class Currency extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'currency';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ppvpricing'=>array(self::BELONGS_TO, 'PpvPricing','currency_id')
        );
    }

}
