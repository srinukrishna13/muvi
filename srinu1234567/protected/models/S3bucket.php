<?php
class S3bucket extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 's3buckets';
    }
    public function relations(){
		return array(
		);
    }
    
    public function getAllocatedBucket($s3bucket_id)
    {
        $command = Yii::app()->db->createCommand()
		->select('*')
		->from('s3buckets')
                ->where('id = '.$s3bucket_id);
	return $command->queryAll();
    }


    public function getAllBuckets()
    {
        $command = Yii::app()->db->createCommand()
		->select('*')
		->from('s3buckets');
	return $command->queryAll();
    }    
}
