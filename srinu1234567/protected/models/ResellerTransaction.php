<?php

class ResellerTransaction extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'reseller_transactions';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
    public function setTransactionData($data = array()){
        if(count($data) >= 1){
            foreach($data as $key => $val){
                $this->$key = $val;
            }
            $this->save();
            return $this->id;
        }
    }

    public function get_ressslerpaymenthistory($id){
        $sql = Yii::app()->db->createCommand()
          ->select('t.card_info_id, t.portal_user_id, t.billing_info_id,t.billing_amount,t.paid_amount,t.order_num,t.created_date,s.reseller_plan_id,s.plans,s.is_custom,s.start_date,s.end_date,s.payment_status,s.payment_key,t.pay_for_customer')
          ->from('reseller_transactions t')
          ->join('reseller_subscriptions s', 't.subscription_id = s.id')
          ->where('t.portal_user_id=:id', array(':id'=>$id))
          ->order('t.id DESC')
          ->queryAll();
        return $sql;
}

}
