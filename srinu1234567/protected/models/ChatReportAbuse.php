<?php

class ChatReportAbuse extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'chat_report_abuse';
    }

}