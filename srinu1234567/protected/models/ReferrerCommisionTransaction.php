<?php

class ReferrerCommisionTransaction extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'referrer_commision_transaction';
    }

     
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
    public function get_amount_paid($referrer_id){
        $thismonthdate = date('Y-m', strtotime("first day of this month"));
        $sql = Yii::app()->db->createCommand()
             ->select('sum(commision_paid) as total')
             ->from('referrer_commision_transaction')
             ->where("referrer_id=:ref_id and DATE_FORMAT(created_date,'%Y-%m')= :this_month",array(':ref_id'=>$referrer_id,':this_month'=>$thismonthdate))
             ->queryRow();
        return $sql;
    }

}
