<?php
class Template extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'templates';
    }
    public function relations()
    {
           return array(
               'template_colors'=>array(self::HAS_MANY, 'TemplateColor','template_id'),
           );
    } 
    
    public function getTemplate($code)
    {
        $template = $this->find(array('condition' => 'code="'.$code.'"'));
        return $template;
    }    
}
