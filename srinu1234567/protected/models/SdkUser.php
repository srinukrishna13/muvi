<?php
class SdkUser extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'sdk_users';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
           'user_profile' => array(self::HAS_ONE, 'UserProfile','user_id'),
           'user_address' => array(self::HAS_ONE, 'UserAddress','user_id'), 
           'profile_picture'=> array(self::HAS_ONE, 'Poster','object_id',
         'condition' => "object_type='user'"),

        );
    }
    public function encrypt($value){
            $enc = new bCrypt();
            return $enc->hash($value);
    }    
        public function getUserDetails($user_id)
        {
            $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('id=:user_id',array(':user_id' => $user_id))
                    ->queryRow();
            return $data;
        }
	
    
   function saveUserPayment($data, $studio_id = '', $is_paypal = 0){
       if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        //Check studio has plan or not
        $start_date = $end_date = Null;
        $plan_id = $plan_price = $payment_gateway_id = 0;
        if ((isset($data['data']['plan_id']) && intval($data['data']['plan_id']))) {
            $plan_id = $data['data']['plan_id'];
            $currency_id = $data['data']['currency_id'];
            $user_id = $data['data']['user_id'];
            //Get plan detail which user selected at the time of registration
            $planModel = new SubscriptionPlans();
            $plan = $planModel->find('id=:plan_id AND studio_id=:studio_id', array(':plan_id' => $plan_id, ':studio_id' => $studio_id));
            if (isset($plan) && !empty($plan)) {
                $price_list = Yii::app()->common->getUserSubscriptionPrice($plan_id, $currency_id);
                $plan_price = $price_list['price'];

                $trail_period = $plan->trial_period;
                $couponCode = "";
                if($data['data']['coupon_code'] != ''){
                    $couponCode = trim($data['data']['coupon_code']);
                    $trail_period = $plan->trial_period;
                    if($data['data']['free_trail_charged'] != ''){
                        $trail_period = $data['data']['free_trail_charged'];
                    }
                    
                    if(intval($trail_period) > 0){
                        $use_discount = 1;
                    }
                }
                $discount_price = '';
                if($trail_period > 0){
                    if($data['data']['discount_amount'] != ''){
                        $discount_price = $data['data']['discount_amount'];
                    }
                }

                $is_transaction = 0;
                if (isset($trail_period) && intval($trail_period) == 0 && isset($data['data']['transaction_data']) && !empty($data['data']['transaction_data'])) {
                    $is_transaction = 1;
                    $use_discount = 0;
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                    
                } else {
                     if($data['data']['free_trail_charged'] != ''){
                    $trail_recurrence = 'day';
                     }else if($plan->trial_recurrence != ''){
                        $trail_recurrence = $plan->trial_recurrence;
                    }
                    $trial_period = $trail_period . ' ' . strtolower($trail_recurrence) . "s";
                    $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                }
            } else {
                $arr['error'] = 'Selected paln don\'t exists in this studio';
                return $arr;
            }
        }
        
        //Save card information if payment gateway is not manual
        $card_id = 0;
        if (isset($data['PAYMENT_GATEWAY']) && $data['PAYMENT_GATEWAY'] != 'manual') {
            if (isset($data['data']['card_last_fourdigit']) && trim($data['data']['card_last_fourdigit'])) {
                //Update card to inactive mode
                $sql = "UPDATE sdk_card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                $con = Yii::app()->db;
                $ciData = $con->createCommand($sql)->execute();
                
                $sciModel = New SdkCardInfos;
                $sciModel->studio_id = $studio_id;
                $sciModel->user_id = $user_id;
                $sciModel->gateway_id = $data['GATEWAY_ID'];
                $sciModel->card_uniq_id = Yii::app()->common->generateUniqNumber();
                $sciModel->card_holder_name = $data['data']['card_name'];
                $sciModel->exp_month = $data['data']['exp_month'];
                $sciModel->exp_year = $data['data']['exp_year'];
                $sciModel->card_last_fourdigit = $data['data']['card_last_fourdigit'];
                $sciModel->auth_num = $data['data']['auth_num'];
                $sciModel->token = $data['data']['token'];
                $sciModel->profile_id = $data['data']['profile_id'];
                $sciModel->card_type = $data['data']['card_type'];
                $sciModel->reference_no = $data['data']['reference_no'];
                $sciModel->response_text = $data['data']['response_text'];
                $sciModel->status = $data['data']['status'];
                $sciModel->ip = $ip_address;
                $sciModel->created = new CDbExpression("NOW()");

                $sciModel->save();
                $card_id = $sciModel->id;
                $arr['card_id'] = $sciModel->id;
            }
            $payment_gateway_id = $data['PAYMENT_GATEWAY_ID'];
        }
        
        $is_subscribed = 0;
        $is_success = 1;
        if ($is_paypal) {
            $is_success = 0;
        }
        
        if (intval($plan_id) && intval($payment_gateway_id)) {
            //Create user subscription for making transaction
            $usModel = new UserSubscription;
            $usModel->studio_id = $studio_id;
            $usModel->user_id = $user_id;
            $usModel->plan_id = $plan_id;
            $usModel->card_id = $card_id;
            $usModel->studio_payment_gateway_id = $payment_gateway_id;
            $usModel->currency_id = $currency_id;
            $usModel->profile_id = $data['data']['profile_id'];
            $usModel->amount = $plan_price;
            $usModel->discount_amount = @$discount_price;
            $usModel->use_discount = $use_discount;
            $usModel->start_date = $start_date;
            $usModel->end_date = $end_date;
            $usModel->status = 1;
            $usModel->is_success = $is_success;
            $usModel->coupon_code = @$couponCode;
            $usModel->ip = $ip_address;
            $usModel->created_by = $user_id;
            $usModel->created_date = new CDbExpression("NOW()");
            $usModel->save();
            
            $user_subscription_id = $usModel->id;
            $is_subscribed = 1;
            
            $couponDetails = CouponSubscription::model()->findByAttributes(array('coupon_code' => @$couponCode));
            if (isset($couponDetails) && !empty($couponDetails)) {
                $command = Yii::app()->db->createCommand();
                if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                    $qry = $command->update('coupon_subscription', array('used_by'=>$couponDetails->used_by.",".$user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code'=>$couponCode));
                } else {
                    $qry = $command->update('coupon_subscription', array('used_by'=>$user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code'=>$couponCode));
        }
            }
        }
        
        //Save transaction data
        if (intval($is_transaction)) {
            $transaction = new Transaction;
            $transaction->user_id = $user_id;
            $transaction->studio_id = $studio_id;
            $transaction->plan_id = $plan_id;
            $transaction->currency_id = $currency_id;
            $transaction->transaction_date = new CDbExpression("NOW()");
            $transaction->payment_method = $data['PAYMENT_GATEWAY'];
            $transaction->transaction_status = $data['data']['transaction_data']['transaction_status'];
            $transaction->invoice_id = $data['data']['transaction_data']['invoice_id'];
            $transaction->order_number = $data['data']['transaction_data']['order_number'];
            $transaction->dollar_amount = $data['data']['transaction_data']['dollar_amount'];
            $transaction->amount = $data['data']['transaction_data']['amount'];
            $transaction->response_text = $data['data']['transaction_data']['response_text'];
            $transaction->subscription_id = $user_subscription_id;
            $transaction->ip = $ip_address;
            $transaction->created_date = new CDbExpression("NOW()");
            $transaction->save();
        }
        
        $arr['is_subscribed'] = $is_subscribed;
        return $arr;
   }
        
   function saveUserDetails($data, $studio_id = ''){
       if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $name = trim($data['data']['name']);
        $email = trim($data['data']['email']);
        $password = $data['data']['password'];

        //Getting Ip address
        $ip_address = Yii::app()->request->getUserHostAddress();
        $geo_data = new EGeoIP();
        $geo_data->locate($ip_address);

        //Get referrence link from where user come this site.
        $source = $data['source'];

        //Make encrypted password and token
        $pass = '';

        $enc = new bCrypt();
        if ($password) {
            $pass = $enc->hash($password);
        }
        $confirm_token = $enc->hash($email);
        if (strpos($confirm_token, "/") !== FALSE) {
            $confirm_token = str_replace('/', '', $confirm_token);
        }
        
        $user = new SdkUser;
        $user->email = $email;
        $user->studio_id = $studio_id;
        $user->signup_ip = $ip_address;
        $user->display_name = $name;
        $user->nick_name = @$_REQUEST['nick_name']?@$_REQUEST['nick_name']:$name;
        $user->encrypted_password = $pass;
        $user->source = $source;
        $user->status = 1;
        $user->confirmation_token = $confirm_token;
        $user->signup_location = serialize($geo_data);
        $user->created_date = new CDbExpression("NOW()");
        if (@$data['data']['fb_access_token']) {
            $user->fb_access_token = $data['data']['fb_access_token'];
        }
        if (@$data['data']['fb_userid']) {
            $user->fb_userid = $data['data']['fb_userid'];
        }
        if (@$data['data']['gplus_userid']) {
            $user->gplus_userid = $data['data']['gplus_userid'];
        }
        $return = $user->save();
        $user_id = $user->id;
        $arr['user_id'] = $user_id;
        $is_saved = Yii::app()->custom->saveCustomFieldValues($studio_id, $user_id);
        return $arr;
   }      
	
/**
 * @method public saveSdkUser(array $request) Save the SDK user info.
 * @return boolen true/false
 * @author GDR<support@muvi.com>
 */	
    function saveSdkUser($data, $studio_id = '', $is_paypal = 0) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $name = trim($data['data']['name']);
        $email = trim($data['data']['email']);
        $password = $data['data']['password'];
         $is_subscription_bundles=$data['data']['is_subscription_bundles'];
        $mobile_number = @$data['data']['mobile_number'];
        $api_unique_id = @$data['data']['api_unique_id'];
        $subscribe_newsletter = @$data['data']['subscribe_newsletter'];
        //Getting Ip address
        $ip_address = Yii::app()->request->getUserHostAddress();
        $geo_data = new EGeoIP();
        $geo_data->locate($ip_address);

        //Get referrence link from where user come this site.
        $source = $data['source'];

        //Make encrypted password and token
        $pass = '';

        $enc = new bCrypt();
        if ($password) {
            $pass = $enc->hash($password);
        }
        $confirm_token = $enc->hash($email);
        if (strpos($confirm_token, "/") !== FALSE) {
            $confirm_token = str_replace('/', '', $confirm_token);
        }

        //Check studio has plan or not
        $start_date = $end_date = Null;
        $plan_id = $plan_price = $payment_gateway_id = 0;

        //print '<pre>';print_r($data);exit;
        
        if ((isset($data['data']['plan_id']) && intval($data['data']['plan_id']))) {
            $plan_id = $data['data']['plan_id'];
            $currency_id = $data['data']['currency_id'];

            //Get plan detail which user selected at the time of registration
            $planModel = new SubscriptionPlans();
            $plan = $planModel->find('id=:plan_id AND studio_id=:studio_id', array(':plan_id' => $plan_id, ':studio_id' => $studio_id));
            if (isset($plan) && !empty($plan)) {
                $price_list = Yii::app()->common->getUserSubscriptionPrice($plan_id, $currency_id);
                $plan_price = $price_list['price'];

                $trail_period = $plan->trial_period;
                $couponCode = "";
                if($data['data']['coupon_code'] != ''){
                    $couponCode = trim($data['data']['coupon_code']);
                    $trail_period = $plan->trial_period;
                    if($data['data']['free_trail_charged'] != ''){
                        $trail_period = $data['data']['free_trail_charged'];
                    }
                    
                    if(intval($trail_period) > 0){
                        $use_discount = 1;
                    }
                }
                $discount_price = '';
                if($trail_period > 0){
                    if($data['data']['discount_amount'] != ''){
                        $discount_price = $data['data']['discount_amount'];
                    }
                }
                
                $is_transaction = 0;
                if (isset($trail_period) && intval($trail_period) == 0 && isset($data['data']['transaction_data']) && !empty($data['data']['transaction_data'])) {
                    $is_transaction = 1;
                    $use_discount = 0;
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                    
                } else {
                    if($data['data']['free_trail_charged'] > 0){
                        $trail_recurrence = 'day';
                    }else{
                        if($plan->trial_recurrence != ''){
                            $trail_recurrence = $plan->trial_recurrence;
                        }
                    }
                    $trial_period = $trail_period . ' ' . strtolower($trail_recurrence) . "s";
                    $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                }
            } else {
                $arr['error'] = 'Selected paln don\'t exists in this studio';
                return $arr;
            }
        }

        //Save SDK user detail
        $user = new SdkUser;
        $user->email = $email;
        $user->studio_id = $studio_id;
        $user->signup_ip = $ip_address;
        $user->display_name = $name;
        $user->nick_name = @$_REQUEST['nick_name']?@$_REQUEST['nick_name']:$name;
        $user->mobile_number = @$_REQUEST['mobile_number']?@$_REQUEST['mobile_number']:$mobile_number;
        $user->api_unique_id = $api_unique_id?$api_unique_id:0;
		if(@$_REQUEST['livestream'] &&  $_REQUEST['livestream']== 1){
			$user->is_broadcaster = 1;
		}
        $user->encrypted_password = ($data['data']['is_api'] > 0)?$password:$pass;
        $user->source = $source;
        $user->status = 1;
        if($subscribe_newsletter == '1'){
            $user->announcement_subscribe = $subscribe_newsletter;
        }else{
            $user->announcement_subscribe = '0';
        }
        $user->confirmation_token = $confirm_token;
        $user->signup_location = isset($data['data']['signup_location'])?$data['data']['signup_location']:serialize($geo_data);
        $user->created_date = new CDbExpression("NOW()");
        $user->user_language = Yii::app()->controller->language_code;
        if (@$data['data']['fb_access_token']) {
            $user->fb_access_token = $data['data']['fb_access_token'];
        }
        if (@$data['data']['fb_userid']) {
            $user->fb_userid = $data['data']['fb_userid'];
        }
        if (@$data['data']['gplus_userid']) {
            $user->gplus_userid = $data['data']['gplus_userid'];
        }
        $return = $user->save();
        $user_id = $user->id;
        $arr['user_id'] = $user_id;
        $is_saved = Yii::app()->custom->saveCustomFieldValues($studio_id, $user_id);
        
        //Save card information if payment gateway is not manual
        $card_id = 0;
        if (isset($data['PAYMENT_GATEWAY']) && $data['PAYMENT_GATEWAY'] != 'manual') {
            if (isset($data['data']['card_last_fourdigit']) && trim($data['data']['card_last_fourdigit'])) {
                //Update card to inactive mode
                $sql = "UPDATE sdk_card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                $con = Yii::app()->db;
                $ciData = $con->createCommand($sql)->execute();

                $sciModel = New SdkCardInfos;
                $sciModel->studio_id = $studio_id;
                $sciModel->user_id = $user_id;
                $sciModel->gateway_id = $data['GATEWAY_ID'];
                $sciModel->card_uniq_id = Yii::app()->common->generateUniqNumber();
                $sciModel->card_holder_name = $data['data']['card_name'];
                $sciModel->exp_month = $data['data']['exp_month'];
                $sciModel->exp_year = $data['data']['exp_year'];
                $sciModel->card_last_fourdigit = $data['data']['card_last_fourdigit'];
                $sciModel->auth_num = $data['data']['auth_num'];
                $sciModel->token = $data['data']['token'];
                $sciModel->profile_id = $data['data']['profile_id'];
                $sciModel->card_type = $data['data']['card_type'];
                $sciModel->reference_no = $data['data']['reference_no'];
                $sciModel->response_text = $data['data']['response_text'];
                $sciModel->status = $data['data']['status'];
                $sciModel->ip = $ip_address;
                $sciModel->created = new CDbExpression("NOW()");

                $sciModel->save();
                $card_id = $sciModel->id;
                $arr['card_id'] = $sciModel->id;
            }
            $payment_gateway_id = $data['PAYMENT_GATEWAY_ID'];
        }

        $is_subscribed = 0;
        $is_success = 1;
        if ($is_paypal) {
            $is_success = 0;
        }
        
        if (intval($plan_id) && intval($payment_gateway_id)) {
            //Create user subscription for making transaction
            $usModel = new UserSubscription;
            $usModel->studio_id = $studio_id;
            $usModel->user_id = $user_id;
            $usModel->plan_id = $plan_id;
            $usModel->card_id = $card_id;
            $usModel->studio_payment_gateway_id = $payment_gateway_id;
            $usModel->currency_id = $currency_id;
            $usModel->profile_id = $data['data']['profile_id'];
            $usModel->amount = $plan_price;
            $usModel->discount_amount = @$discount_price;
            $usModel->use_discount = $use_discount;
            $usModel->coupon_code = @$couponCode;
            $usModel->start_date = $start_date;
            $usModel->end_date = $end_date;
            $usModel->status = 1;
            $usModel->is_success = $is_success;
            $usModel->ip = $ip_address;
            $usModel->created_by = $user_id;
            $usModel->is_subscription_bundle = $data['data']['is_subscription_bundles'];
            $usModel->created_date = new CDbExpression("NOW()");
            $usModel->save();
            
            $user_subscription_id = $usModel->id;
            $is_subscribed = 1;
            
            $couponDetails = CouponSubscription::model()->findByAttributes(array('coupon_code' => @$couponCode));
            if (isset($couponDetails) && !empty($couponDetails)) {
                $command = Yii::app()->db->createCommand();
                if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                    $qry = $command->update('coupon_subscription', array('used_by'=>$couponDetails->used_by.",".$user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code'=>$couponCode));
                } else {
                    $qry = $command->update('coupon_subscription', array('used_by'=>$user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code'=>$couponCode));
        }
            }
        }
        
        //Save transaction data
        if (intval($is_transaction)) {
            $transaction = new Transaction;
            $transaction->user_id = $user_id;
            $transaction->studio_id = $studio_id;
            $transaction->plan_id = $plan_id;
            $transaction->currency_id = $currency_id;
            $transaction->transaction_date = new CDbExpression("NOW()");
            $transaction->payment_method = $data['PAYMENT_GATEWAY'];
            $transaction->transaction_status = $data['data']['transaction_data']['transaction_status'];
            $transaction->invoice_id = $data['data']['transaction_data']['invoice_id'];
            $transaction->order_number = $data['data']['transaction_data']['order_number'];
            $transaction->dollar_amount = $data['data']['transaction_data']['dollar_amount'];
            $transaction->amount = $data['data']['transaction_data']['amount'];
            $transaction->response_text = $data['data']['transaction_data']['response_text'];
            $transaction->subscription_id = $user_subscription_id;
            if(isset($data['data']['is_subscription_bundles']) && !empty($data['data']['is_subscription_bundles'])){
            $transaction->transaction_type = 7;    
            }
            $transaction->ip = $ip_address;
            $transaction->created_date = new CDbExpression("NOW()");
            $transaction->save();
        }
        
        $arr['is_subscribed'] = $is_subscribed;
        return $arr;
    }

    
     function saveSdkUserSubscriptionBundles($data, $studio_id = '', $default_currency_idd) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        //$name = trim($data['data']['name']);
        //$email = trim($data['data']['email']);
        //$password = $data['data']['password'];

        //Getting Ip address
        $ip_address = Yii::app()->request->getUserHostAddress();
        $geo_data = new EGeoIP();
        $geo_data->locate($ip_address);

        //Get referrence link from where user come this site.
        $source = $data['source'];

        //Make encrypted password and token
        $pass = '';

        $enc = new bCrypt();
        if ($password) {
            $pass = $enc->hash($password);
        }
        $confirm_token = $enc->hash($email);
        if (strpos($confirm_token, "/") !== FALSE) {
            $confirm_token = str_replace('/', '', $confirm_token);
        }

        //Check studio has plan or not
        $start_date = $end_date = Null;
        $plan_id = $plan_price = $payment_gateway_id = 0;

        //print '<pre>';print_r($data);exit;
        
        if ((isset($data['data']['plandetailbundles_id']) && intval($data['data']['plandetailbundles_id']))) {
            $plan_id = $data['data']['plandetailbundles_id'];
            $currency_id = $data['data']['currency_id'];
            //Get plan detail which user selected at the time of registration
            $planModel = new SubscriptionPlans();
            $plan = $planModel->find('id=:plan_id AND studio_id=:studio_id', array(':plan_id' => $plan_id, ':studio_id' => $studio_id));
            if (isset($plan) && !empty($plan)) {
                $price_list = Yii::app()->common->getUserSubscriptionBundlesPrice($plan_id, $default_currency_idd, $country);
                //$price_list = Yii::app()->common->getUserSubscriptionPrice($plan_id, $currency_id);
                $plan_price = $price_list['price'];
         

                $is_transaction = 0;
                if (isset($plan->trial_period) && intval($plan->trial_period) == 0 && isset($data['data']['transaction_data']) && !empty($data['data']['transaction_data'])) {
                    $is_transaction = 1;
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                    
                } else {
                    $trial_period = $plan->trial_period . ' ' . strtolower($plan->trial_recurrence) . "s";
                    $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                }
            } else {
                $arr['error'] = 'Selected paln don\'t exists in this studio';
                return $arr;
            }
        }
       
        //Save card information if payment gateway is not manual
        $card_id = 0;
       $email = trim($data['data']['email']);
       $user  = SdkUser::model()->findBYAttributes(array('studio_id'=>$studio_id,'email'=>$email));
       $user_id = $user->id;
        if (isset($data['PAYMENT_GATEWAY']) && $data['PAYMENT_GATEWAY'] != 'manual') {
            if (isset($data['data']['card_last_fourdigit']) && trim($data['data']['card_last_fourdigit'])) {
                //Update card to inactive mode
                
                $sql = "UPDATE sdk_card_infos SET is_cancelled=1 WHERE user_id=" . $user_id . " AND studio_id=" . $studio_id;
                $con = Yii::app()->db;
                $ciData = $con->createCommand($sql)->execute();

                $sciModel = New SdkCardInfos;
                $sciModel->studio_id = $studio_id;
                $sciModel->user_id = $user_id;
                $sciModel->gateway_id = $data['GATEWAY_ID'];
                $sciModel->card_uniq_id = Yii::app()->common->generateUniqNumber();
                $sciModel->card_holder_name = $data['data']['card_name'];
                $sciModel->exp_month = $data['data']['exp_month'];
                $sciModel->exp_year = $data['data']['exp_year'];
                $sciModel->card_last_fourdigit = $data['data']['card_last_fourdigit'];
                $sciModel->auth_num = $data['data']['auth_num'];
                $sciModel->token = $data['data']['token'];
                $sciModel->profile_id = $data['data']['profile_id'];
                $sciModel->card_type = $data['data']['card_type'];
                $sciModel->reference_no = $data['data']['reference_no'];
                $sciModel->response_text = $data['data']['response_text'];
                $sciModel->status = $data['data']['status'];
                $sciModel->ip = $ip_address;
                $sciModel->created = new CDbExpression("NOW()");

                $sciModel->save();
                $card_id = $sciModel->id;
                $arr['card_id'] = $sciModel->id;
            }
            $payment_gateway_id = $data['PAYMENT_GATEWAY_ID'];
        }

        $is_subscribed = 0;
        $is_success = 1;
        if ($is_paypal) {
            $is_success = 0;
        }
        if (intval($plan_id) && intval($payment_gateway_id)) {
            //Create user subscription for making transaction
            $usModel = new UserSubscription;
            $usModel->studio_id = $studio_id;
            $usModel->user_id = $user_id;
            $usModel->plan_id = $plan_id;
            $usModel->card_id = $card_id;
            $usModel->studio_payment_gateway_id = $payment_gateway_id;
            $usModel->currency_id = $currency_id;
            $usModel->profile_id = $data['data']['profile_id'];
            $usModel->amount = $plan_price;
            $usModel->start_date = $start_date;
            $usModel->end_date = $end_date;
            $usModel->status = 1;
            $usModel->is_success = $is_success;
            $usModel->ip = $ip_address;
            $usModel->created_by = $user_id;
            $usModel->created_date = new CDbExpression("NOW()");
            $usModel->is_subscription_bundle = $data['data']['is_subscription_bundle_checked'];
            $usModel->save();
            
            $user_subscription_id = $usModel->id;
            $is_subscribed = 1;
        }
        
        //Save transaction data
        if (intval($is_transaction)) {
            
            $transaction = new Transaction;
            $transaction->user_id = $user_id;
            $transaction->studio_id = $studio_id;
            $transaction->plan_id = $plan_id;
            $transaction->currency_id = $currency_id;
            $transaction->transaction_date = new CDbExpression("NOW()");
            $transaction->payment_method = $data['PAYMENT_GATEWAY'];
            $transaction->transaction_status = $data['data']['transaction_data']['transaction_status'];
            $transaction->invoice_id = $data['data']['transaction_data']['invoice_id'];
            $transaction->order_number = $data['data']['transaction_data']['order_number'];
            $transaction->dollar_amount = $data['data']['transaction_data']['dollar_amount'];
            $transaction->amount = $data['data']['transaction_data']['amount'];
            $transaction->response_text = $data['data']['transaction_data']['response_text'];
            $transaction->subscriptionbundles_id = $user_subscription_id;
            $transaction->transaction_type = 7;
            $transaction->ip = $ip_address;
            $transaction->created_date = new CDbExpression("NOW()");
            $transaction->save();
            $trasaction_idd=$transaction->id;
        }
        $arr['amount'] = $plan_price;
        $arr['is_subscribed'] = $is_subscribed;
        $arr['user_id']=$user_id;
        $arr['transaction_id']=$trasaction_idd;
        return $arr;
    }
    
    
    public function updateSdkUser($studio_id,$user_data)
        {
            $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(),$user_data,'studio_id=:studio_id AND is_studio_admin =:is_studio_admin',array(':studio_id' => $studio_id,':is_studio_admin'=>'1'));
            return $data;
        }
        
        public function getCountSdkUser($studio_id)
        {
            $created_date = Date('Y-m-d');
            $sql = "SELECT * FROM `sdk_users` u LEFT JOIN transactions t ON t.user_id=u.id  WHERE u.studio_id = ".$studio_id." AND `is_studio_admin` = '0' AND `is_developer` = '0' AND u.status = 1 AND DATE_FORMAT(u.created_date,'%Y-%m-%d') <= '".$created_date."'";
            $data = Yii::app()->db->createCommand($sql)->queryAll();
            return $data;
        }
        
        public function getCountRegisteredSdkUser($studio_id)
        {
            $created_date = Date('Y-m-d');
            $sql = "SELECT * FROM `sdk_users` u RIGHT JOIN transactions t ON t.user_id=u.id  WHERE u.studio_id = ".$studio_id." AND `is_studio_admin` = '0' AND `is_developer` = '0' AND u.status = 1 AND DATE_FORMAT(u.created_date,'%Y-%m-%d') <= '".$created_date."'";
            $data = Yii::app()->db->createCommand($sql)->queryAll();
            return $data;
        }
    public function getUserLanguage($studio_id,$user_id){
        $user  = SdkUser::model()->findBYAttributes(array('studio_id'=>$studio_id,'id'=>$user_id),array('select'=>'user_language'));
        if(!empty($user)){
            $lang_code = $user->user_language;
        }else{
            $lang_code = Yii::app()->controller->default_language_code;
        }
        return  $lang_code;
    }  
     public function save_reseller_customer_data($data = array()){
        if(count($data) > 0){
            foreach($data as $key => $val){
                $this->$key = $val;
            }
            $this->save();
            return $this->id;
        }
    }
    public function updateSubscribe($unique_id){
        if($unique_id){
            $command = Yii::app()->db->createCommand();
            $command->update('sdk_users', array('is_subscribe_inapi'=>1), 'api_unique_id=:api_unique_id', array(':api_unique_id'=>$unique_id));
        }
    }
    public function apiCancelSubscribe($unique_id){
        if($unique_id){
            $command = Yii::app()->db->createCommand();
            $command->update('sdk_users', array('status'=>0, 'is_subscribe_inapi'=>0), 'api_unique_id=:api_unique_id', array(':api_unique_id'=>$unique_id));
        }
    }
    
    public function updateApiUniqueId($mobile_number, $studio_id, $user_id, $uniqueID){
        if($uniqueID && $mobile_number && $user_id && $studio_id){
            $command = Yii::app()->db->createCommand();
            $command->update('sdk_users', array('api_unique_id'=>$uniqueID), 'id=:id AND studio_id=:studio_id AND mobile_number=:mobile_number', array(':id'=>$user_id, ':studio_id'=>$studio_id, ':mobile_number'=>$mobile_number)); 
        }
    }
    public function saveOtp($otp, $studio_id, $username, $flag) {
        $getOtpexpirytime = Yii::app()->general->getDefaultExpiryTime();
        $date = gmdate("Y-m-d H:i:s", strtotime('+' . $getOtpexpirytime . 'hours'));
        $command = Yii::app()->db->createCommand();
        if ($flag === 1) {
            $command->update('sdk_users', array('otp' => $otp, 'opt_expiry_time' => $date), 'studio_id=:studio_id AND mobile_number=:mobile_number', array(':studio_id'=>$studio_id, ':mobile_number'=>$username));
        }else if($flag === 2){
            $command->update('sdk_users', array('otp' => $otp, 'opt_expiry_time' => $date), 'studio_id=:studio_id AND email=:email', array(':studio_id'=>$studio_id, ':email'=>$username));
        }
    }    
}
