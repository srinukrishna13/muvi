<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SeoInfo
 *
 * @author Sanjeev<sanjeev@muvi.com>
 */
class SeoInfo extends CActiveRecord{
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'seo_info';
    }
    
    public function relations(){
        return array();
    }
    
    public function clearOldSeoInfo($studio_id,$type,$pagename = '',$cid = 0, $clid = 0)
    {
        switch ($type){
            case 'page':
                $data = Yii::app()->db->createCommand()
                    ->delete($this->tableName(),'studio_id=:id AND ( page LIKE :pc OR page LIKE :pct OR page LIKE :hp)',array(':id' => $studio_id,':pc' => 'content',':pct' => 'contentlisting',':hp' =>'homepage'));
                break;
            case 'store':
                $data = Yii::app()->db->createCommand()
                    ->delete($this->tableName(),'studio_id=:id AND  page LIKE :sp',array(':id' => $studio_id,':sp'=>'%store%'));
                break;
            case 'static':
                $data = Yii::app()->db->createCommand()
                    ->delete($this->tableName(),'studio_id=:id AND page LIKE :pc',array(':id' => $studio_id,':pc' => $pagename));
                break;
            case 'dynamic-content':
                $data = Yii::app()->db->createCommand()
                    ->delete($this->tableName(),'studio_id=:id AND page=:page AND movie_id=:cid',array(':id' => $studio_id,':page' => $pagename,':cid'=>$cid));
                break;
            case 'dynamic-content-listing':
                $data = Yii::app()->db->createCommand()
                    ->delete($this->tableName(),'studio_id=:id AND page=:page',array(':id' => $studio_id,':page' => $pagename));
                break;
            default :
                break;
        }
        return $data;
    }
    
    public function checkSeoInfoByPage($studio_id,$pagename)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:id AND page LIKE :pagename',array(':id'=>$studio_id,':pagename' => $pagename))
                ->queryRow();
        return $data;
    }
    
    public function checkSeoInfoByMovieId($studio_id,$id)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:id AND movie_id =:id ',array(':id'=>$studio_id,':id' => $id))
                ->queryRow();
        return $data;
    }
    
    public function checkSeoInfoByContentId($studio_id,$id)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:id AND content_types_id =:id ',array(':id'=>$studio_id,':id' => $id))
                ->queryRow();
        return $data;
    }

    public function saveSeoInfo($seoInfo = array())
    {
        $data = Yii::app()->db->createCommand()
                ->insert($this->tableName(),$seoInfo);
        return $data;
    }
    
    public function getAllInfo($studio_id,$type)
    {
        switch ($type){
            case 'content':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page=:page',array(':id' => $studio_id,':page' => $type))
                    ->queryRow();
                break;
            case 'content-page':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page=:page',array(':id' => $studio_id,':page' => $type))
                    ->queryRow();
                break;
            case 'contentlisting':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page=:page',array(':id' => $studio_id,':page' => $type))
                    ->queryRow();
                break;
            case 'content-listing-page':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page=:page',array(':id' => $studio_id,':page' => $type))
                    ->queryRow();
                break;
            case 'store':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page=:page',array(':id' => $studio_id,':page' => $type))
                    ->queryRow();
                break;
            case 'store-page':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page=:page',array(':id' => $studio_id,':page' => $type))
                    ->queryRow();
                break;
            case 'static':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND (page NOT LIKE :page1 AND page NOT LIKE :page2 AND page NOT LIKE :page3 AND page NOT LIKE :page4)',array(':id' => $studio_id,':page1' => 'content',':page2' => 'contentlisting',':page3' => 'content-page',':page4' => 'content-listing-page'))
                    ->queryAll();
                break;
            default :
                $data = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from($this->tableName())
                        ->where('studio_id=:id',array(':id' => $studio_id))
                        ->queryAll();
                break;
        }
        
        return $data;
    }
    
    public function pageMeta($studio_id,$page){
        switch ($page){
            case 'home':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page LIKE :page',array(':id' => $studio_id,':page' => '%homepage%'))
                    ->queryRow();
                break;
            case 'aboutus':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page LIKE :page',array(':id' => $studio_id,':page' => '%aboutus%'))
                    ->queryRow();
                break;
            case 'contentlisting':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page =:page',array(':id' => $studio_id,':page' => 'contentlisting'))
                    ->queryRow();
                break;
            case 'content':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page =:page',array(':id' => $studio_id,':page' => 'content'))
                    ->queryRow();
                break;
            case 'store':
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page =:page',array(':id' => $studio_id,':page' => 'store'))
                    ->queryRow();
                break;
            default :
                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND page LIKE :page',array(':id' => $studio_id,':page' => '%'.$page.'%'))
                    ->queryRow();
                break;
        }
        return $data;
    }
    
    public function detailspagemeta($studio_id,$type, $movie_name,$movie_id = 0){     
        $data = Yii::app()->db->createCommand()
            ->select('*')
            ->from($this->tableName())
            ->where('studio_id=:id AND movie_id =:movie_id',array(':id' => $studio_id,':movie_id' => $movie_id))
            ->queryRow();
        return $data;
    }
    public function productpagemeta($studio_id,$type,$movie_id = 0){     
        $data = Yii::app()->db->createCommand()
            ->select('*')
            ->from($this->tableName())
            ->where('studio_id=:id AND movie_id =:movie_id',array(':id' => $studio_id,':movie_id' => $movie_id))
            ->queryRow();
        return $data;
    }
    public function listpagemeta($studio_id,$type_id){     
        $data = Yii::app()->db->createCommand()
            ->select('*')
            ->from($this->tableName())
            ->where('studio_id=:id AND content_types_id =:types_id',array(':id' => $studio_id,':types_id' => $type_id))
            ->queryRow();
        return $data;
    }
    
}
