<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To keep log of template action for download/upload
 * @author : RKSahoo
 */

class TemplateLog extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'template_log';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

}

