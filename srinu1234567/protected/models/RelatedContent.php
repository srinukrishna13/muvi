<?php
class RelatedContent extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'related_content';
    }
    public function insertRelatedData($studio_id,$data){
        if($data){
            $movieids = explode(',',trim($data['movieids'],','));
            $contenttypes = explode(',',trim($data['contenttypes'],','));            
            foreach ($movieids as $key=>$value) {
                $this->studio_id = $studio_id;
                $this->movie_id = $data['mastercontentid'];
                $this->movie_stream_id = $data['mastercontentstreamid'];
                $this->content_id = $value;
                $this->content_type = $contenttypes[$key];
                $this->type = @$data['type'];
                $this->isNewRecord = true;
                $this->primaryKey = NULL;
                $this->save();
            }
            return 1;
        }else{
            return '';
        }
    }    
}