<?php

class PpvMultiSeasonPricing extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'ppv_multi_season_pricing';
    }
}
