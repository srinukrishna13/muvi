<?php
class Livestream extends CActiveRecord {
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'livestream';
    }
	/**
	 * @method public SaveFeeds(array $data) It will save the feed info of a content/Chanel
	 * @return bool True/False
	 * @author Gaydhar<support@muvi.com>
	 */
	function saveFeeds($data,$studio_id='',$movie_id='',$user_id=''){
		if(!$studio_id){
			$studio_id = Yii::app()->user->studio_id;
		}
		if($data){
			$this->movie_id = $movie_id;
			$this->feed_type = $data['feed_type'];
			$this->feed_url = $data['feed_url'];
			$this->feed_method = $data['method_type'];
			$this->studio_id = $studio_id;
			$this->created_by = $user_id?$user_id:Yii::app()->user->id;
			$this->created_date = gmdate('Y-m-d H:i:s');
			$this->ip = Yii::app()->common->getIP();
            if(@$data['method_type'] == 'push'){
                $this->stream_url = @$data['stream_url'];
                $this->stream_key = @$data['stream_key'];
                $this->is_recording = @$data['is_recording'];
                if(@$data['is_recording'] == 1 && @$data['delete_content'] == 1){
                    $this->delete_no = @$data['delete_no'];
                    $this->delete_span = @$data['delete_span'];
                }
                if(@$data['start_streaming'] !=''){
                    $this->start_streaming = @$data['start_streaming'];
                }
            }
			$this->save();
			return $this->id;
		}else{
			return '';
		}
	}
}
?>