
function stripe () {
    this.processCard = function(isAuthenticateOnly) {
        $('#register_membership').html(JSLANGUAGE.wait);
        $('#register_membership').attr('disabled', 'disabled');

        // Processing popup
        $("#loadingPopup").modal('show');
        if ($("#ppvModalMain").length) {
            $("#ppvModalMain").addClass('fade');
        }
        if (parseInt(isAuthenticateOnly)) {
            stripeAuthResponse();
        } else {
            stripeResponseHandler();
        }
    };

    stripeAuthResponse = function() {

            document.membership_form.action = HTTP_ROOT + "/user/" + action;
            document.membership_form.submit();
    };

    stripeResponseHandler = function() {
        var description = 'Creation of New customer';
        var card_number = $('#card_number').val();
        var card_name = $('#card_name').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();
        if($.trim($('#email_address').val())){
            var email = $('#email_address').val();
        }else{
            var email = $('#email').val();
        }
        var plan_id = 0;
        if ($('#plan_id').length) {
            plan_id = $('#plan_id').val();
        }else if($('#plandetail_id').length){
            plan_id = $('#plandetail_id').val();
        }
        var currency_id = 0;
        if ($('#currency_id').length) {
            currency_id = $('#currency_id').val();
        }
        $("#card_div").append("<input type='hidden' name='data[payment_method]' value='stripe' />");
        var url = HTTP_ROOT + "/user/processCard";
        $.post(url, {'email': email, 'description': description, 'card_number': card_number,'card_name':card_name,'exp_month':exp_month,'exp_year':exp_year,'cvv':cvv ,'plan_id': plan_id, 'currency_id': currency_id}, function (data) {
            if (parseInt(data.isSuccess) === 1) {
                $("#loadingPopup").modal('hide');
                $("#successPopup").modal('show');
                if (data.card) {
                    for (var i in data.card) {
                        $("#card_div").append("<input type='hidden' name='data[" + i + "]' value='" + data.card[i] + "' />");
                    }
                }
                
                if (data.transaction_data) {
                    for (var i in data.transaction_data) {
                        $("#card_div").append("<input type='hidden' name='data[transaction_data][" + i + "]' value='" + data.transaction_data[i] + "' />");
                    }
                }
                setTimeout(function () {
                    document.membership_form.action = HTTP_ROOT + "/user/" + action;
                    document.membership_form.submit();
                    return false;
                }, 5000);
            } else {
                $("#loadingPopup").modal('hide');
                if ($("#ppvModalMain").length) {
                    $("#ppvModalMain").removeClass('fade');
                }
                if ($("#membership_loading").length) {
                    $("#membership_loading").hide();
                }                 
                $('#register_membership').html(btn);
                $('#register_membership').removeAttr('disabled');
                if($("#paypal").length){
                    $("#paypal").removeAttr("disabled");
                }
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                }
            }
        }, 'json');
    };
    
    loadScript = function(url, callback) {
        var script = document.createElement("script")
        script.type = "text/javascript";
        if (script.readyState){  //IE
            script.onreadystatechange = function(){
                if (script.readyState == "loaded" || script.readyState == "complete"){
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function(){
                callback();
            };
        }
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    };
    
    this.pciPayment = function() {
        loadScript("https://checkout.stripe.com/checkout.js", function(){
            $.post(HTTP_ROOT + "/user/paymentUserDeatils", {gateway_code:'stripe'}, function(res){
                var result = $.parseJSON(res);
                var pg_amount = $('#pg_amount').val();
                var coupon = $('#coupon').val();
                var pg_currency_id = $('#pg_currency_id').val();
                var pg_shipping_cost = $('#pg_shipping_cost').val();
                var url = HTTP_ROOT + '/shop/getCurrency';
                $.post(url, {'currency_id': pg_currency_id,'coupon':coupon,'amount':pg_amount,'shipping_cost':pg_shipping_cost}, function (data) {
                    var data = $.parseJSON(data);
                    var amount = Math.round(data.amount*100)/100;
                    
                    setTimeout(function(){
                        $('#pci_click_event').trigger('click');
                    }, 2000);
                    token_triggered = false;
                    var handler = StripeCheckout.configure({
                        key: result.api_password,
                        image: result.studio_logo,
                        locale: 'auto',
                        token: function(token) {
                            token_triggered = true;
                            var pci_url = HTTP_ROOT + '/shop/ProcessPCITransaction';
                            var pay_data = $('#payform').serialize();
                            var coupon_id = $.trim($('#coupon').val())?$('#coupon').val():'';
                            var token_id = token.id;
                            $.post(pci_url, {'data': pay_data,'coupon':coupon_id,'token':token_id}, function (resArray) {
                                $("#loadingPopup").modal('hide');
                                if(resArray.is_success){
                                    $("#successPopup").modal('show');
                                    $('.loader_cart').hide();
                                    setTimeout(function () {
                                        window.location.href = HTTP_ROOT + '/shop/success';
                                        return false;
                                    }, 5000);
                                }else{
                                    window.location.href = HTTP_ROOT + '/shop/cancel';
                                    return false;
                                }
                            },'json');
                        },
                        closed: function() {
                            if (!token_triggered) {
                                window.location.href = HTTP_ROOT + '/shop/cancel';
                                return false;
                            } 
                        }
                    });

                    document.getElementById('pci_click_event').addEventListener('click', function(e) {
                        handler.open({
                            name: result.studio_name,
                            amount: amount*100,
                            currency: data.currency_code
                        });
                        e.preventDefault();
                    });

                    window.addEventListener('popstate', function() {
                        handler.close();
                    });
                    
                });
            });
        });
    };
}
