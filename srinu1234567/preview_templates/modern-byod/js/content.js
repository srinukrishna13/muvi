var url = HTTP_ROOT + "/content/findepisodes";
var loginurl = HTTP_ROOT + "/user/checklogin";
var comment_url = HTTP_ROOT+"/media/comment_add";
var delay = 1000, setTimeoutConst, setTimeoutConst1;
var loader=true;
var pageNumber = 1, scrollAmountTrigger = 400, offset = 0, limit = 2;
if ($("#episodes").length > 0 ){
    limit = $("#limit").val();
}
fetchEpisodeContent(pageNumber, offset);
$(document).ready(function() {
      
    $('#series').change(function() {
         $('#loader_episode').show();
        $('.allplay').addClass('hide');
        $.post(url, {'movie_id': $('#content_id').val(), 'series': $('#series').val(), 'limit' : limit}, function(res) {
            $("#episodes").html(res);
            $('#loader_episode').hide();
            initializeContentData();
            loader = true;
            offset=2;
            pageNumber=1;
            fetchEpisodeContent(pageNumber, offset);
        });
    });

    $('.rating-tooltip-manual').rating({
        extendSymbol: function() {
            var title;
            $(this).tooltip({
                container: 'body',
                placement: 'bottom',
                trigger: 'manual',
                title: function() {
                    return title;
                }
            });
            $(this).on('rating.rateenter', function(e, rate) {
                title = rate;
                $(this).tooltip('show');
            })
                    .on('rating.rateleave', function() {
                        $(this).tooltip('hide');
                    });
        }
    });

    $('#watch_now').click(function() {
        $("#playbtn").trigger("click");
    });
});

$(document).ready(function() {

    $('.thumbnail').hover(
            function() {
                var obj = this;
                setTimeoutConst = setTimeout(function() {
                    $(obj).find('.caption').fadeIn('slow');
                }, 500);
            },
            function() {
                clearTimeout(setTimeoutConst);
                $(this).find('.caption').fadeOut('slow');
            }
    );
    $("#review").validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                mail: true,
            },
            rate: {
                required: true,
                min: 1
            }
        },
        submitHandler: function(form) {
            $('#loader_review').show();
            $.ajax({
                url: HTTP_ROOT + "/content/savereview",
                data: $("#review").serialize(),
                dataType: 'json',
                method: 'post',
                success: function(result) {
                    if (result.status == 'success')
                    {
                        $('#loader_review').hide();  
                        location.reload();
                    }
                    else
                    {
                        $('#loader_review').hide();  
                        $('#review_error').html(result.message);
                        return false;
                    }
                }
            });
        }
    });
    $('.post-comment').click(function (e) { 
        var movie_id = $(this).attr('data-content_id');
        var com_text = $('#commentfield').val();
        $("#commentform").validate({
            rules: {
                "commentfield": {
                    required: true
                }
            },
            submitHandler: function (form) {

                $.ajax({
                    method: "POST",
                    url: loginurl,
                    beforeSend: function () {
                     $('#comment_loader').show();
                    },
                    success: function (res) {
                        if (parseInt(res) === 1) {
                            if (com_text != "") {
                                $.post(comment_url, {com_text: com_text, movie_id: movie_id}, function (res) {
                                    $("#comment_box").val("");
                                    $('#commentfield').val('');
                                    $("#comment_list").html(res);
                                    $('#comment_loader').hide();
                                });
                            }
                        } else {
                            $('#comment_loader').hide();
                            $("#loginModal").modal('show');
                            return false;
                        }
                    }
                });

            }
        });
    });
    
    $('.comment-reply-btn').click(function(e){
        e.preventDefault();        
        var reply_form = $(this).closest('form');
        var reply_loader = reply_form.find('.loader');
        var parent_id = $(this).attr('data-parent_id');
        var movie_id = $(this).attr('data-content_id');         
        $.ajax({
            method: "POST",
            url: loginurl,
            beforeSend: function () {
                reply_loader.show();
            },
            success: function(res){
                if (parseInt(res) === 1) {                    
                    var reply_text = reply_form.find("[name='reply_text']").val();
                    $.post(comment_url, {com_text: reply_text, movie_id: movie_id, parent_id: parent_id}, function (reply_msg) {
                        reply_loader.hide();
                        reply_form.find("[name='reply_text']").val('');
                        $("#comment_list").html(reply_msg);
                    });
                } else {
                    reply_loader.hide();
                    $("#loginModal").modal('show');
                    return false;
                }            
            }   
        });
    });
});    

function initializeContentData() {
    $(".playbtn").click(function () {
        $('#loader').show();
        var movie_id = $(this).attr('data-movie_id');
        var stream_id = $(this).attr('data-stream_id');
        var isppv = $(this).attr('data-isppv');
        var is_ppv_bundle = $(this).attr('data-is_ppv_bundle');
        var isadv = $(this).attr('data-isadv');
    
        if (typeof isadv !== typeof undefined && isadv !== false) {

        } else {
            isadv = 0;
        }
    
        var permalink = $(this).attr("data-content-permalink");
        var contentTypePermalink = $(this).attr("data-content-type-permalink");

        $('#loginModal').on('show.bs.modal', function (e) {
            $('#loader').hide();
            $("#movie_id").val(movie_id);
            $("#stream_id").val(stream_id);
            $("#isppv").val(isppv);
            $("#is_ppv_bundle").val(is_ppv_bundle);
            $("#isadv").val(isadv);
            $('#content-permalink').val(permalink);
            $('#content-type-permalink').val(contentTypePermalink);
        });
    });
}
function fetchEpisodeContent(pageNumber, offset)
{
    pageNumber++;
    if (pageNumber != 2) {
        offset = offset + 2;
    }
    if ($("#episodes").length > 0 ) {
        $('#loader_episode').show();
        $.ajax({
            method: "POST",
            url: url,
            data: {'movie_id': $('#content_id').val(), 'series': $('#series').val(), 'offset': offset, 'limit' : limit}
        }).done(function (msg) {
            $('#loader_episode').hide();
            if(msg){
                $("#episodes").append(msg);
                initializeContentData();
                if(loader){
                    fetchEpisodeContent(pageNumber, offset);
                }
            }else{
                loader = false;
            }
        });
    }

}
function resize_player() {
    if (full_screen == false) {
        full_screen = true;
        var large_screen = setTimeout(function() {
            if (full_screen == true) {
            }
        }, 5000);
    } else {
        //clearTimeout(large_screen);
        full_screen = false;
    }
}
$( "#myTrailer" ).addClass( "section" );
(function(){
     var halfwidth= $(window).width() / 2;
     var halfheight= $(window).height() / 2; 
     $('.section').width(halfwidth);
     $('.section').height(halfheight);
 $("#trailer-btn").fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            closeBtn: false,
            scrolling: false,
            titleShow: false,
            padding: 0,
            content: $('#myTrailer').show(),
    beforeShow: function () {
               $(".fancybox-inner").attr("style","background: black");
               $(".fancybox-inner").append('<div style="display: block;" class="loader"></div>');
               var url= HTTP_ROOT + "/ThirdParty/ThirdPartyList";
               var movie_id = $('#p_movieid').val();
               $.post(url,{movie_id: movie_id,halfheight: halfheight,halfwidth: halfwidth},function(res){
               $(".fancybox-inner").attr("style","width:" + halfwidth + "px !important; height :" + halfheight +"px !important;background : black !important;");
               $('#myTrailer').html(res);
//             $('.fancybox-overlay').click(function(){
//                   e.preventDefault();   
//                  $('.fancybox-overlay-fixed').css('display', 'none');
//                });
               $(".loader").hide();
                }); 
                },'afterClose': function () {
                    $('.fancybox-overlay-fixed').remove();
                    $("#myTrailer").hide();
                } 
       });  
 })();

function show_reply(parent_id) {
    $("#reply_area_" + parent_id).show();
}