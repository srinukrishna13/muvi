<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
    <tbody>
        <tr>
            <td>
                <table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
                    <tbody>
                        <tr style="background-color:#f3f3f3">
                            <td style="text-align:left;padding-top:10px"><span mc:edit="logo"><?php echo $params['logo']; ?></span></td>
                        </tr>
                    </tbody>
                </table>
                <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                    <tbody>
                        <tr>
                            <td>
                                <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                    <p style="display:block;margin:0 0 17px">
                                        Dear <?php echo $params['name']; ?>,<br><br/> 
                                        Thank you for your application.</p>
                                    <p>We will confirm your participation for the Hackathon in couple of days time via return email.</p>
                                    <p>Please note - it is essential you have a confirmation email from us to participate in the event.</p>
                                    <p style="display:block;margin:0">
                                        Thank you,<br>
                                        Muvi Hackathon team <br />
                                        hackathon@muvi.com
                                    </p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>