<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:none;background-repeat:repeat-x" width="100%">
    <tbody>
        <tr>
            <td>
                <table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
                    <tbody>
                        <tr style="background-color:#f3f3f3">
                            <td style="text-align:left;padding-top:10px">
                                <div mc:edit="logo"><?php echo $params['logo']; ?></div>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                    <tbody>
                        <tr>
                            <td>
                                <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                    <p style="display:block;margin:0 0 17px">
                                    Hi All,
                                    </p>
                                    <div mc:edit="mailcontent">Cloudfront URL was not created.Below are the details of the Studio <br>
                                        Studio Id: <strong><span mc:edit="studio_id"><?php echo $params['studio_id']; ?></span></strong><br/>
                                        Studio Name: <strong><span mc:edit="studio_name"><?php echo $params['studio_name']; ?></span></strong><br/>                                        
                                        Domain Name:<strong><span mc:edit="domain_name"><?php echo $params['domain_name']; ?></span></strong><br/>
                                    </div>
                                    <p style="display:block;margin:20px 0 17px">Regards,<br/>Team Muvi</p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>