<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
    <p style="display:block;margin:0 0 17px">
        Hi <span mc:edit="name"><?php echo $params['name']; ?></span>
    </p>              
    <p style="display:block;margin:0 0 17px">
        A new support ticket <span mc:edit="url1"><?php echo $params['url1']; ?></span> is added in reference to your e-mail.
    </p>
    <p style="display:block;margin:0 0 17px">
        Thanks,<br/>
        Muvi Support
    </p>
</div>