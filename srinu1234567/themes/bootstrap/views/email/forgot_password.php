<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Muvi</title>
</head>
<body>
        <table height="400px" width="700px" bgcolor="#f0f0f0" align="center" border="0">
         <tr height="50px">
             <td width="50px"></td>
             <td align="left"><a href="http://www.muvi.com"><img src="<?php echo EMAIL_LOGO; ?>"/></a></td>
            </tr>
         <tr height="300px">
             <td colspan="2" bgcolor="#FFFFFF">
                <table border="0" align="center" width="600px" height="300px">
                     <tr height="250px">
                         <td>
                         	Hi <span mc:edit="user_name"></span>,
                             <br/><br/>
                             Click the login button bellow to set a new password. And start discovering movies again. In case of any problems you can tweet us @muvi or email <a href="mailto:help@muvi.com">help@muvi.com</a>.
                             <br/><br/>
                             Regards,<br/>
                             Muvi<br/>
							 Keep Discovering
                         </td>
                        </tr>
                        <tr height="50px" align="center">
                            <td>
                            	<table width="20%">
                                	<tr>
                                        <td width="132px" bgcolor="#0099FF" align="center"><span style="padding:10px 0; font:normal 20px Arial, Helvetica, sans-serif; width:130px; display:block; color:#FFF; background-image: linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -o-linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -moz-linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -webkit-linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -ms-linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0.1, rgb(0,134,212)), color-stop(1, rgb(1,151,238))); border-radius:4px; border:solid 1px #0086d4; text-decoration:none;"><span mc:edit="login_link"></span></span></td>
                        			</tr>
                                </table>
                            </td>
                    	</tr>
                    </table>
                </td>
            </tr>
            <tr height="50px">
             <td colspan="2"></td>
            </tr>
        </table>
</body>
</html>