<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
						<td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"><?php echo $params['logo']; ?></div>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
							<p style="display:block;margin:0 0 17px">
                                <strong>Dear <span mc:edit="username"><?php echo $params['username']; ?></span></strong>,<br><br/> 
                               
                                    Welcome to <span mc:edit="website_name"><?php echo $params['website_name']; ?></span> <br/>
                                    Thank you for registering at <span mc:edit="website_name"><?php echo $params['website_name']; ?></span>.
                               
								<br/>
                                <strong>Activate Your Free Month Today!</strong><br>
                            </p>
                            <p style="display:block;margin:0 0 17px">
                                    Purchase a subscription today and start watching your favorite TV Shows and Movies from <span mc:edit="website_name"><?php echo $params['website_name']; ?></span> Network, what’s more if you activate this today we are giving you access for the 1st month for <strong>FREE</strong> on us! 
                            </p>
                            <p style="display:block;margin:0 0 17px">
                            You can cancel any time you want. No commitments, hidden fees or hassles.<br/>
                                So hurry up and <span mc:edit="payment_link"><?php echo $params['payment_link']; ?></span> to visit the payment page to activate your premium membership.
                            </p>
                            <p style="display:block;margin:0 0 17px">
                                If you have any questions you can always visit our <span mc:edit="faq_link"><<?php echo $params['faq_link']; ?>/span> page to find out more about <span mc:edit="website_name"></span> or if you need further assistance, please contact us at <span mc:edit="admin_email"><?php echo $params['admin_email']; ?></span><br/>								
							</p>
							<p style="display:block;margin:0">
                                <strong>Sincerely,</strong><br>
								Team <span mc:edit="website_name"><?php echo $params['website_name']; ?></span>
							</p>
						</div>
					</td>
				</tr>
                 <tr>
                    <td>
                        <table style="width:100%">
                            <tbody>
                                <tr>
                                    <td style="width:70%">
                                        <p style="font-size:13px;margin:10px 0px">
                                            <span mc:edit="website_address"><?php echo $params['website_address']; ?></span>
                                        </p>
                                    </td>
                                    <td style="width:25%">
                                        <p style="font-size:13px;margin:10px 0px">
                                        <span mc:edit="fb_link"><?php echo $params['fb_link']; ?></span>&nbsp;<span mc:edit="gplus_link"><?php echo $params['gplus_link']; ?></span>&nbsp;<span mc:edit="twitter_link"><?php echo $params['twitter_link']; ?></span>
                                        </p>    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>