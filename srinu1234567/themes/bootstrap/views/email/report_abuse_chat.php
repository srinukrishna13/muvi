<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
    <tbody>
        <tr>
            <td>
                <table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
                    <tbody>
                        <tr style="background-color:#f3f3f3">
                            <td style="text-align:left;padding-top:10px">
                                <div mc:edit="logo"><?php echo $params['logo']; ?></div>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <?php if($params['sent_to']!='admin'){?>
                <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                    <tbody>
                        <tr>
                            <td>
                                <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                    <p style="display:block;margin:0 0 17px">
                                        Hello <span mc:edit="name"><?php echo $params['name']; ?></span>,
                                    </p>              
                                    <p style="display:block;margin:0 0 17px">
                                        We appreciate reports concerning users who violate <?php echo $params['studio_name'];?>'s Terms of Use.
                                    </p>
                                    <p style="display:block;margin:0 0 17px">
                                        We have reported the incident. We will use the information you provide to conduct an investigation.</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%;font-family:helvetica,Arial;">
                                    <tbody>
                                        <tr>
                                            <td style="color:#555;font-size:14px;line-height:1.8em;text-align:left;width:100%;">
                                                Thanks,
                                                <p style="font-size:14px;margin:2px 0px">
                                                    <?php echo $params['studio_name'];?>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <?php } else {?>
                <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                    <tbody>
                        <tr>
                            <td>
                                <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                    <p style="display:block;margin:0 0 17px">
                                        Hello <span mc:edit="name"><?php echo $params['name']; ?></span>,
                                    </p>              
                                    <p style="display:block;margin:0 0 17px">
                                        <?php echo $params['reported_by_name'];?> has reported for unwanted behavior against <?php echo $params['reported_to_name'];?> who for violating the Terms of Use.
                                    </p> 
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%;font-family:helvetica,Arial;">
                                    <tbody>
                                        <tr>
                                            <td style="color:#555;font-size:14px;line-height:1.8em;text-align:left;width:100%;">
                                                Thanks,
                                                <p style="font-size:14px;margin:2px 0px">
                                                    Team Muvi
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php }?>
            </td>
        </tr>
    </tbody>
</table>