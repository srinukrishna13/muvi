<div class="wrapper">
    <div class="container home-page-studio">
        <!--<h2 class="btm-bdr">Muvi</h2>-->
        <h3>Launch fully featured OTT Video on Demand (VOD) Platform instantly, at Zero investment! <br/>
            Muvi takes care of everything including hosting, templates, player, DRM, payment gateway and maintenance. Take a look at our features below.
    </div>
</div>


<div class="wrapper blubg" id="navblubar" style="z-index:150;display:none;">
    <div class="container home-page-customers barlinks">
        <div class="span9">
            <a href="#features">Features</a>
            <a href="<?php echo Yii::app()->baseUrl; ?>/examples">Examples</a>
            <a href="<?php echo Yii::app()->baseUrl; ?>/tour">Tour</a>
            <a href="<?php echo Yii::app()->baseUrl; ?>/pricing">Pricing</a>
            <a href="<?php echo Yii::app()->baseUrl; ?>/muvi">More</a>
        </div>
        
        <div class="span3 pull-right top_social">
            <ul class="social">
                <li class="pull-right"><a href="https://www.linkedin.com/company/muvi-studio" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/linkedin.png" alt="LinkedIn" title="LinkedIn" /></a></li>                
                <li class="pull-right"><a href="https://www.facebook.com/MuviStudioB2B" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/facebook.png" alt="facebook" alt="Facebook" /></a></li>
                <li class="pull-right"><a href="http://www.twitter.com/muvistudiob2b" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/twitter.png" alt="twitter" alt="Twitter" /></a></li>
                
            </ul>
        </div>          
    </div>
</div> 

<div class="wrapper whtbg" id="features">
    <div class="container pad50">
        <div class="row-fluid">
            <div class="span7">
                <div class="item zerocap">
                    <h2 class="blue">Zero CapEX</h2>
                    <p>No capital expenditure required to purchase expensive hardware for hosting or bandwidth to support video streaming. Muvi takes care of 
                        all this. Get your website and mobile app up and running with investing $0, really!</p>  
                </div>
            </div>
            <div class="span4 pull-right txt-right">
                <div class="item">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/zero-capex.png" alt="zero-capex" title="Zero Capex" />
                </div>
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="second">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/quick-to-lunch.png" class="move_top" alt="quick-to-lunch" title="Quick to Launch" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item ql">
                <h2 class="blue">Quick to Launch</h2>
                <p>Get the website up and running within a few hours. <br />Save millions of dollars by reducing time to market.</p>            
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="third">
    <div class="container pad50">

        <div class="span7">
            <div class="item infscl">
                <h2 class="blue">Infinite Scalability</h2>
                <p>Muvi leverages world&rsquo;s leading cloud infrastructure and content delivery network (CDN), Amazon Web Services, to deliver unlimited storage and infinite scalability. 
                    Support millions of traffic and views with no worries about bandwidth and maintenance.</p>    
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/scalability.png" alt="scalability" title="Infinite Scalability" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="fourth">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/drm.png" alt="drm" title="DRM Across All Devices" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item drm">
                <h2 class="blue">DRM Across All Devices</h2>
                <p>Muvi offers multiscreen content security and encryption across different platforms. It ensures the consistency of your business model across different devices and 
                    applications.</p>       
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="fifth">
    <div class="container pad50">

        <div class="span7">
            <div class="item custlook">
                <h2 class="blue">Customize Look and Feel</h2>
                <p>Customize the entire video streaming website and mobile app to match the look and feel of your brand. It&rsquo;s much more than just changing your logo or 
                    color scheme, and much more!</p>  
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/customize-look.png" alt="customize-look" title="Customize Look and Feel" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="sixth">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/payperview.png" alt="payperview" title="Pay-per-view and Subscription Models" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item ppv">
                <h2 class="blue">Revenue Models</h2>
                <p>Content management system (CMS) allows complete control on price and geographical distribution. Supports different revenue models like AVoD, TVoD (Pay-Per-View), 
                    SVoD and hybrids. Advertising platform allows you to integrate different types of ads in your content.</p>    
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="seventh">
    <div class="container pad50">

        <div class="span7">
            <div class="item webmob">
                <h2 class="blue">TV Everywhere</h2>
                <p>Create brand presence across all platforms - web, mobile, TV, media boxes (Apple TV, Roku, Chromecast, Amazon Fire etc.) and gaming consoles (Playstation, XBOX etc.). 
                    Muvi powers your streaming website, mobile app (iPhone, Android, Windows Phone) and smart TV app.</p>  
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/web-mobile.png" alt="web-mobile" title="Web, Mobile and TV" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="eightth">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/marketing-tool.png" alt="marketing-tool" title="Built-in Marketing Tools" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item bltmrkt">
                <h2 class="blue">Built-in Marketing Tools</h2>
                <p>Muvi offers several built-in marketing tools such as SEO-optimized website, user database for email marketing and social media 
                    integration. Custom viral apps are also available to promote your site and content.</p>   
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="nineth">
    <div class="container pad50">

        <div class="span7">
            <div class="item audrel">
                <h2 class="blue">Audience Relationship Management</h2>
                <p>Own your audience not just for one movie, but for a lifetime (and more if you believe in second life :)). Filter using several criteria including 
                    city/country, age group and taste. Send customized emails, sell additional pay-per-view content.</p>       
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/audiance-relationship.png" alt="Audiance Relationship" title="Audience Relationship Management" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper grybg">
    <div class="container home-page-customers">
        <h2 class="btm-bdr">Our Customers</h2>
        <ul>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/maa-tv.png" alt="Maaflix" title="Maaflix" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/iskon.png" alt="Iskcon" title="Iskcon" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/funkara_logo.png" alt="Funkara" title="Funkara" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/fountain-films.png" alt="Fountain" title="Fountain" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/sepl.png" alt="SEPL" title="SEPL" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/movie-tee-vee.png" alt="Movie Tee Vee" title="Movie Tee Vee" /></li>
            <li><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/pocket-films.png" alt="Pocket Films" title="Pocket Films" /></li>                        
        </ul>
    </div>
</div> 

<div class="wrapper">
    <div class="container">
        <h2 class="btm-bdr">Press</h2>
        <ul class="press-icons cntr">
            <li class="toi"><a title="Times of India" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/press/toi_press.jpg" data-toggle="lightbox" target="_blank"></a></li> 
            <li class="dna"><a title="DNA" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/press/dna_press.jpg" data-toggle="lightbox" target="_blank"></a></li>
            <li class="bs"><a title="Business Standard" data-toggle="lightbox" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/press/bs_press.jpg" target="_blank"></a></li>
            <li class="iw"><a title="InformationWeek" href="http://www.informationweek.in/informationweek/press-releases/299016/muvi-studio-launches-video-demand-platform" target="_blank"></a></li>
            <li class="lri"><a title="Light Reading" href="http://www.lightreading.in/lightreadingindia/news/299017/muvi-studio-launches-video-demand-platform" target="_blank"></a></li>
            <li class="rtn"><a title="Rapid TV News" href="http://www.rapidtvnews.com/2014122336553/muvi-studio-launches-vod-paas-technology.html#axzz3MiHcCr9N" target="_blank"></a></li>
            <li class="ao"><a title="Asia OTT" href="http://www.asiaott.com/2014/12/24-8925.html" target="_blank"></a></li>
            <li class="nt"><a title="Next Asia" href="http://nextvasia.com/vod/muvi-studio-launches-vod-platform/" target="_blank"></a></li>
            <li class="fov"><a title="Fierce Online Video" href="http://www.fierceonlinevideo.com/press-releases/muvi-studio-launched-its-video-demand-vod-platform-partnering-biggest-produ" target="_blank"></a></li>
            <li class="ei"><a title="Entrepreneur India" href="http://www.entrepreneurindia.com/news/Bangalore-based-tech-startup-Muvi-Studio-launches-VoD-platform-5791/" target="_blank"></a></li>
            <li class="bt"><a title="Business Today" href="http://businesstoday.intoday.in/prnewswire/index.jsp?doc=201412220333PR_NEWS_EURO_ND__enIN201412224123_indiapublic&dir=25" target="_blank"></a></li>
            <li class="btrade"><a title="Bollywood Trade" href="http://www.bollywoodtrade.com/press-release/muvi-studio-launched-its-video-on-demand-vod-platform-by-partnering-with-biggest-production-houses-worldwide/b62e9154-afb8-496f-b21b-8f4bb666bae3/" target="_blank"></a></li>
            <li class="tp"><a title="Television Post" href="http://www.televisionpost.com/technology/indian-tech-start-up-muvi-studio-launches-vod-platform/" target="_blank"></a></li>
            <li class="dq"><a title="DQ India" href="http://nextvasia.com/vod/muvi-studio-launches-vod-platform/" target="_blank"></a></li>
            <li class="vintera"><a title="Vintera" href="http://www.vintera.tv/ru/news/?page=3" target="_blank"></a></li>
        </ul>
    </div>
</div> 

<?php 
$host_ip = Yii::app()->params['host_ip'];
      
       
if (in_array(HOST_IP,$host_ip)){
    ?>
<div class="container blogsection">
    <div class="span4">
        <h2 class="btm-bdr">Latest Blogs</h2>
        <?php
        if(HOST_IP!='127.0.0.1'){
            $args = array('cat' => 1, 'posts_per_page' => 1);
            query_posts($args);
            while ( have_posts() ) {
                the_post();
                ?>

                <article>
                    <header>
                        <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="blue"><?php the_title(); ?></a></h3>
                    </header><!-- .entry-header -->

                    <div>
                        <?php the_excerpt() ?>
                    </div><!-- .entry-content -->
                    <a href="<?php echo home_url(); ?>" title="Goto blogs"><?php /*the_title();*/ echo 'More blogs'?> &raquo;</a>
                </article>

            <?php
            } 
            wp_reset_query();
        }
        ?>        
    </div>
    <div class="span4">
        <h2 class="btm-bdr">Industry News</h2>
        <?php
        if(HOST_IP!='127.0.0.1'){
            $args = array('cat' => 2, 'posts_per_page' => 1);
            query_posts($args);            
            while ( have_posts() ) {
                the_post();
                ?>

                <article>
                    <header>
                        <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="blue"><?php the_title(); ?></a></h3>
                    </header><!-- .entry-header -->

                    <div>
                        <?php the_excerpt() ?>
                    </div><!-- .entry-content -->
                    <a href="<?php echo 'http://muvi.com/blogs/category/industry-news/'; ?>" title="Goto news"><?php /*the_title();*/ echo 'More news'?> &raquo;</a>
                </article>

            <?php
            } 
            wp_reset_query();
        }
        ?>        
    </div>    
    <div class="span4 pull-right">            
            <a class="twitter-timeline"  href="https://twitter.com/muvistudiob2b" data-widget-id="538237511214956544">Tweets by @muvistudiob2b</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>          
    </div>
</div>
<?php }?>

<script type="text/javascript">
    var $stickyHeight = $(window).height();
    var $padding = 10;
    var $topOffset = 100;

    function scrollSticky(){
      if($(window).scrollTop() >0) {
        //if($(window).scrollTop() + $padding > $topOffset) {
          $('#top_bar .navbar').attr('style', 'position:fixed;z-index:100;background-color:#42b7da;width:100%;');
        //}else{
        //  $('#top_bar .navbar').css('position','');
        //  $('#top_bar .navbar').css('background-color','');
       // } 
      }else{
          $('#top_bar .navbar').css('position','');
          $('#top_bar .navbar').css('background-color','');
      }
    }
    $(window).scroll(function(){
      scrollSticky();
    });

$(document).ready(function(){
    $('a[href^="#"]').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });
    $(".faq_block").click(function(){
        var obj_id = this.id;
        var sl = obj_id.split('_');
        var sl_no = sl[1];
        //$(this).hide();
        $("#ans_"+sl_no).slideToggle("slow");
        $("#question_"+sl_no+" .plus_icon").toggle();
        $("#question_"+sl_no+" .minus_icon").toggle();
        //$("#question_"+sl_no).toggle("slow");
    });
    $(".que").hover(
        function(){
            $(this).css("text-decoration","underline");
        }, function () {
           $(this).css({"text-decoration":"none"});
         }
    );
    
});
</script>
<!--Download Pop up form-->


<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/ekko-lightbox.min.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ekko-lightbox.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function ($) {
    // delegate calls to data-toggle="lightbox"
    $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
        event.preventDefault();
        return $(this).ekkoLightbox({
            onShown: function() {
                if (window.console) {
                    return console.log('Checking our the events huh?');
                }
            },
            onNavigate: function(direction, itemIndex) {
                if (window.console) {
                    return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                }
            }
        });
    });
});
</script>    

<?php require_once dirname(__FILE__).'/../layouts/contactform.php';?>
