<?php
$v = RELEASE;
$randomVar = rand();
$studio_ad = (isset($studio_ads) && count($studio_ads) > 0) ? $studio_ads[0] : array();
$play_length = 0;
$play_percent = 0;
if(isset($durationPlayed['played_length']) && isset($durationPlayed['played_percent'])) {
    $play_length = $durationPlayed['played_length'];
    $play_percent = $durationPlayed['played_percent'];    
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!DOCTYPE html> 
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title;?> | <?php echo $this->studio->name;?></title>
        <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>
            var user_id="<?php echo Yii::app()->user->id; ?>";
            var STORE_AUTH_TOKEN="<?php echo $authToken; ?>";  
            var HTTP_ROOT = "<?php echo Yii::app()->getbaseUrl(true); ?>";
            var notification = 0;
            <?php if ($this->notification){ ?>
                notification = "<?php echo $this->notification; ?>";
            <?php } ?>
            var favicon = "<?php echo $this->favIcon; ?>";
        </script>
        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/js/themes/notification.js"></script>
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>
        <script>
            var myplay_length = "<?php echo $play_length;?>";
        </script>
        <script type='text/javascript'>
        // video js error message
            var you_aborted_the_video_playback ="<?php echo $this->Language["you_aborted_the_video_playback"]; ?>";
            var a_network_error_caused ="<?php echo $this->Language["a_network_error_caused"]; ?>";
            var the_video_playback_was_aborted ="<?php echo $this->Language["the_video_playback_was_aborted"]; ?>";
            var the_video_could_not_be_loaded ="<?php echo $this->Language["the_video_could_not_be_loaded"]; ?>";
            var the_video_is_encrypted ="<?php echo $this->Language["the_video_is_encrypted"]; ?>";
            var no_compatible_source ="<?php echo $this->Language["no_compatible_source"];?>";
            
            var muviWaterMark = "";
            <?php if(isset($_REQUEST['waterMarkOnPlayer']) && $_REQUEST['waterMarkOnPlayer'] != ''){ ?>
                var muviWaterMark = "<?php echo $_REQUEST['waterMarkOnPlayer'] ?>";
            <?php } else if($waterMarkOnPlayer != ''){ ?>
                var muviWaterMark = "<?php echo $waterMarkOnPlayer; ?>";
            <?php } ?>
        </script>
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl() ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl() ?>/common/bootstrap/css/bootstrap.min.css" />          
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/video.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/js/vjs.youtube.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
        <!--<script src="<?php echo Yii::app()->baseUrl; ?>/js/videojs.hls.min.js"></script>-->
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs.progressTips.js"></script> 
        
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs.progressTips.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/small_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />    
        <!--
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs.ads.css" rel="stylesheet" type="text/css">
        -->
        <link rel="stylesheet" href="//googleads.github.io/videojs-ima/third_party/videojs-ads/videojs.ads.css" />
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs.vast.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/video.js.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs.ads.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/vast-client.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs.vast.js"></script>   

        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/video/quality/video-quality-selector.css?rand=<?php echo $randomVar; ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/video/quality/video-quality-selector.js?rand=<?php echo $randomVar; ?>"></script>    
        <script data-cfasync="false" type="text/javascript">
            videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
        </script>
        <style type="text/css">
            .video-js {height: 50%; padding-top: 48%;}
            .vjs-fullscreen {padding-top: 0px}  
            .RDVideoHelper{display: none;}
            video::-webkit-media-controls {
                display:none !important;
                opacity: 0;
            }
            video::-webkit-media-controls-panel {
                display: none !important;
            }
            *::-webkit-media-controls-panel {
                display: none!important;
                -webkit-appearance: none;
            }
            /* Old shadow dom for play button */
            *::-webkit-media-controls-play-button {
                display: none!important;
                -webkit-appearance: none;
            }
            /* New shadow dom for play button */
            /* This one works */
            *::-webkit-media-controls-start-playback-button {
                display: none!important;
                -webkit-appearance: none;
            }
            body {
                background: #000;
            }
        #backButton {
            position:absolute;
            left: 100px;
            top: 70px;
            display: none;
            /*height:30px;
            background-image: url('http://devstudio.prangya.com/images/back_button.png');
             width: 30px;*/
            z-index: 1; 
        }
        #pause_touch{
            margin: auto;
            left:0;
            right: 0;
            bottom: 0;
            top: 0;
            position: absolute;
            height: 64px;
            width: 64px;
            display: none;
        }
        #play_touch{
            margin: auto;
            left:0;
            right: 0;
            bottom: 0;
            top: 0;
            position: absolute;
            height: 64px;
            width: 64px;
            display: none; 
        }
        </style>
        <?php 
        if(isset($block_ga)){ 
            $block_ga = Yii::app()->session['block_ga'];
        if ($this->studio->google_analytics != '' && $block_ga!=0 ) { ?>
            <script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', '<?php echo html_entity_decode($this->studio->google_analytics); ?>', 'auto');
    ga('send', 'pageview');
            </script>
            <?php
        }}
        ?>        
    </head>
    <body>
        <?php if($play_percent != 0 && $play_length != 0) { ?>
                <div class="modal fade" id="play_confirm" tabindex="-1" role="dialog" >
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"> <?php echo $this->Language['resume_watching']; ?></h4>
                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-danger" id="confirm_yes" type="button" onclick="FromDurationPlayed()"><?php echo $this->Language['yes']; ?></button>
                                <button data-dismiss="modal" class="btn btn-default-with-bg" type="button" onclick="FromBeginning()"><?php echo $this->Language['btn_cancel']; ?></button>
                            </div>  
                        </div>
                    </div>
                </div>
        <?php } ?> 
        <input type="hidden" id="full_video_duration" value="" />
        <input type="hidden" id="full_video_resolution" value="" />
        <input type="hidden" id="u_id" value="0" />
        <input type="hidden" id="buff_log_id" value="0" />
        <input type="hidden" id="backbtnlink" value="<?php echo @Yii::app()->session['backbtnlink'];?>" />
        <div class="wrapper">
            <div class="videocontent" style="overflow:hidden;">
                <video id="video_block" class="video-js moo-css vjs-default-skin" autoplay preload="auto" autobuffer width="auto" height="auto" data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution; ?>" } }}' controls="true" webkit-playsinline>
                    <?php
                    foreach ($multipleVideo as $key => $val) {
                        if(Yii::app()->common->isMobile() == 1){
                            echo '<source src="' . $val . '" data-res="' . $key . '" type="video/mp4" />';
                        } else{
                            echo '<source src="' . $fullmovie_path . '" data-res="' . $key . '" type="video/mp4" />';
                        }
                    }
                    foreach($subtitleFiles as $subtitleFilesKey => $subtitleFilesval){
                        if($subtitleFilesKey == 1){
                            echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" default="true" ></track>';
                        } else{
                            echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" ></track>';
                        }
                    }
                    ?>
                </video>
            </div> 
        </div>
        <div id="episode_block" class="episode_block">
            <a href="play_video_backup.php"></a>
        </div>
        <script>
        var play_status = 0;
        </script>
        
        <?php 
        if (strtolower(@$movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower(@$movieData['content_type']);
        }
        ?>
        <?php
        if (strtolower(@$movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower(@$movieData['content_type']);
        }
        ?>
        <script>
            var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = true;
            
            var setInterSpeed = '';
            var multipleVideoResolution = new Array();
            multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;
            var ended = 0;
            var videoOnPause = 0;
            var adavail = 0;
            var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/video/getNewSignedUrlForPlayer";
            var nAgt = navigator.userAgent;
            var browserName = navigator.appName;
            var verOffset;
            var bitCLick = false;
            var bitvideoChangeRes = false;
            // In Opera 15+, the true version is after "OPR/" 
            if ((verOffset = nAgt.indexOf("OPR/")) != -1) {
                browserName = "Opera";
            }
            // In older Opera, the true version is after "Opera" or after "Version"
            else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
                browserName = "Opera";
            }
            // In MSIE, the true version is after "MSIE" in userAgent
            else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
                browserName = "Microsoft Internet Explorer";
            }
            // In Chrome, the true version is after "Chrome" 
            else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
                browserName = "Chrome";
            }
            // In Safari, the true version is after "Safari" or after "Version" 
            else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
                browserName = "Safari";
            }
            // In Firefox, the true version is after "Firefox" 
            else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
                browserName = "Firefox";
            }
            var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
            var stream_id = "<?php echo isset($stream_id) ? $stream_id : 0 ?>";
			var player = '';
			var percen = 5;
			var play_length = 0;
			var t = 0;
            var bitControlbarclick=false;
            var lastBufferSize =0;
            var cnt =0;
            var checkurlactive =0;
            var track; 
            var setInterSpeed  = {
						ips_start: function () {
							var self = this;
							this.interval = setInterval(function(){ checkInterNetSpeed(multipleVideoResolution,player) },60000);
							console.log("ips-start");
						},
						ips_clear: function () {
							clearInterval(this.interval);
							console.log("ips-clear");
							delete this.interval;
						},
						ips_restart: function () {
							console.log("ips-re-start");
							if (!this.interval) this.ips_start();
						}
             };
            
            $(document).ready(function () {
                var  track,  loadwebvtt = document.getElementById("loadwebvtt"),
                    hideTracks = function() {
                        // Oddly, there's no way to remove a track from a video, so hide them instead
                        for (i = 0; i < player.textTracks.length; i++) {
                            player.textTracks[i].mode = "hidden";
                        }
                    };

                var movie_id = "<?php echo $movie_id ?>";
                var full_movie = "<?php echo $fullmovie_path ?>";
                var can_see = "<?php echo $can_see ?>";
                var wiki_data = "<?php echo isset($wiki_data) ? $wiki_data : '' ?>";
                var url = "<?php echo Yii::app()->baseUrl; ?>/video/add_log";
                var episode_id = "<?php echo isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0 ?>";
                var content_type = "<?php echo isset($content_type) ? $content_type : "" ?>";
                var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";
                var previousTime = 0;
                var currentTime = 0;
                var seekStart = null;
                var forChangeRes = 0;
                var bufferenEnddd = 0;
                var bufferenEndd = 0;
                var previousBufferEnd =0;
                var bufferDurationaas = 0;
                var isiPad = null;
                var isAndroid = null;
                var is_restrict = "<?php echo isset($is_restrict) ? $is_restrict : 0 ?>";
                var is_studio_admin = "<?php echo Yii::app()->session['is_studio_admin'] ?>";
                $('#video_block').bind('contextmenu', function () {
                    return false;
                });

            $('.videocontent').mouseover(function() {
                $('#backButton').show();
            });
            $('.videocontent').mouseout(function() {
                $('#backButton').hide();
            });
                var playerSetup = videojs('video_block',{plugins: {resolutionSelector: {
                            force_types: ['video/mp4'],
                            default_res: "<?php echo $defaultResolution; ?>"
                        }}});
                playerSetup.ready(function () {
                if(playerSetup){
                     setInterSpeed.ips_start();
                 }
                    player = this;
                //Added by prangya 
                this.progressTips(); // Displying duration in the player during playback.
                // Displying Play/pause button with animation during playback.
                 $('#video_block').append('<img src="/images/touchpause.png" id="pause_touch" />');
                 $('#video_block').append('<img src="/images/touchplay.png" id="play_touch" />');
                $('#video_block').append('<img src="/images/pause.png" id="pause"/>'); 
                $('#video_block').append('<img src="/images/play-button.png" id="play" />');
                $('#video_block').append('<img src="/images/back-button.png" width="40" height="40" id="backButton" style="cursor:pointer" data-toggle="tooltip" title="Back to Browse" allowFullScreen = true/>'); 
                    $('#backButton').click(function(){
                        player.dispose();
                        if($('#backbtnlink').val() !='')
                            window.location.href = $('#backbtnlink').val();
                        else
                            parent.history.back();
                        return false;
                    });
                    $(".vjs-fullscreen-control").click(function () {
                        $(this).blur();
                    });
                     
                    <?php if(isset(Yii::app()->user->id) && Yii::app()->user->id != 1038){ ?>
                        setInterval(function() {
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo Yii::app()->baseUrl; ?>/user/canlogin',
                                success: function(res) {
                                    if(res != 1)
                                        window.location.href ="<?php echo $this->siteurl.'/user/logout' ?>" ;
                                }
                            });             
                        }, 60000);
                        setInterval(function(){
                            $.ajax({
                                url: '<?php echo Yii::app()->baseUrl; ?>/site/updateLoginHistory/',
                                type: "POST",
                                data: {login_count:'login_count'},          
                                success: function (data) {
                                    console.log(data);
                                }
                            }); 
                        }, 300000);
                    <?php } ?> 

                    $(document).bind('keydown', function(e) {
                        if (e.keyCode === 32) { 
                                //document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                                if(player.paused()){
                                    player.play();
                                } else{
                                    player.pause();
                                }
                        }

                    });
                if(is_mobile === 0){
                    $('#video_block_html5_api').on('click',function(e){ 
                   // document.getElementById('video_block_html5_api').onclick = function(e) {
                          e.preventDefault();
                        if (player.paused()){
                            $('#pause').hide(); 
                            $('#play').show();
                            
                        } else{
                            $('#play').hide(); 
                            $('#pause').show(); 
                        }
                    });
                }else{
                     player.on("play", function () {
                         videoOnPause = 0;
                         $('#play_touch').hide();
                         $('#pause_touch').hide();
                     });
                     player.on("pause", function () {
                         videoOnPause = 1;
                         $('#play_touch').show();
                         $('#pause_touch').hide();
                     });
                     $('#pause_touch').bind('touchstart',function(e){
                            player.pause();
                     });
                     $('#play_touch').bind('touchstart',function(e){
                            player.play();
                     });
                     $('#video_block_html5_api').bind('touchstart',function(e){ 
                            if(player.play()){
                                $('#pause_touch').show(); 
                                $('#play_touch').hide(); 
                                setTimeout(function(){
                                $('#pause_touch').hide();   
                                 },3000); 
                             }
                    });
                }
                    player.on('error', function () {
                        var errordetails = this.player().error();
                        if (errordetails.code == 2 && seekStart === null){
                            seekStart = 123;
                            createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTime,browserName);
                            $(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
                        }
                       if($('.vjs-modal-dialog-content').html()!==''){
                            $(".vjs-modal-dialog-content").html("<div>"+no_compatible_source+"</div>");
                        } else {
                        if(document.getElementsByTagName("video")[0].error != null){
                            var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                            if (videoErrorCode == 2) {
                                if(seekStart === null){
                                    seekStart = 123;
                                    createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTime,browserName);
                                }
                                $(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
                            } else if (videoErrorCode === 3) {
                                $(".vjs-error-display").html("<div>"+the_video_playback_was_aborted+"</div>");
                            } else if (videoErrorCode === 1) {
                                 $(".vjs-error-display").html("<div>"+you_aborted_the_video_playback+"</div>");
                            } else if (videoErrorCode === 4) {
                                  $(".vjs-error-display").html("<div>"+the_video_could_not_be_loaded+"</div>");
                            } else if (videoErrorCode === 5) {
                                $(".vjs-error-display").html("<div>"+the_video_is_encrypted+"</div>");
                            }
                        }
                      }
                    });
                    
                    $(".vjs-tech").mousemove(function () {
                        if (full_screen === true && show_control === false) {
                            $("#video_block .vjs-control-bar").show();
                            show_control = true;
                            var timeout = setTimeout(function () {
                                if (full_screen === true) {

                                }
                            }, 10000);
                            $(".vjs-control-bar").mousemove(function () {
                                event.stopPropagation();
                            }).mouseout(function () {
                                event.stopPropagation();
                            });
                        } else {
                            clearTimeout(timeout);
                        }
                    });
                    player.on("fullscreenchange", resize_player);
//                    //For Iphone;
                        isiPad = nAgt.match(/iPad/i);
                        if (isiPad) {
                            $('.vjs-fullscreen-control').hide();
                            var iframeiheight = $(window).height();
                            $( ".video-js" ).attr('style','padding-top:'+ iframeiheight +'px');
                            $( window ).on( "resize", function( event ){
                                var iframeiheight = $(window).height(); 
                            setTimeout(function () {
                                         $( ".video-js" ).attr('style','padding-top:'+ iframeiheight +'px');
                                     }, 0.001);
                            });
                            $('.vjs-res-button').attr('style','width : 14em; position: relative');
                            $('#backButton').bind('touchstart', function(){
                                if($('#backbtnlink').val() !='')
                                    window.location.href = $('#backbtnlink').val();
                                else
                                   parent.history.back();
                                return false;   
                            });
                            $(".vjs-control-content ").bind('touchstart', function(event) {
                                if ($('.vjs-control-content:visible').length === 0 ) {
                                    $('.vjs-menu').show();
                                }else{
                                    $('.vjs-menu').hide();
                                } 
                            });
                        }
                        
                        //For Android;
                        isAndroid = nAgt.match(/Android/i);
                        if(isAndroid) {
                           $('.vjs-fullscreen-control').show();
                            var thisIframesHeight = $( window ).height();
                            $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
                            $( window ).on( "orientationchange", function( event ) {
                                if(window.orientation === 0){
                                     thisIframesHeight = $( window ).height();
                                     $( ".video-js" ).attr('style','padding-top:155%; height:100%;');
                                }else{
                                    $( ".video-js" ).attr('style','padding-top:43%; height:50%;');
                                    }
                                });
                            
                            $(".vjs-res-button").bind('touchstart', function(event) {
                                     showSettingDiv();
                                });
                           
                            $('#backButton').bind('touchstart', function(){
                                if($('#backbtnlink').val() !='')
                                    window.location.href = $('#backbtnlink').val();
                                else
                                   parent.history.back();
                                return false;   
                                });
                            $(".vjs-control-content ").bind('touchstart', function(event) {
                                if ($('.vjs-control-content:visible').length === 0 ) {
                                    $('.vjs-menu').show();
                                }else{
                                    $('.vjs-menu').hide();
                                    } 
                            });
                       }                         
                    <?php if($v_logo != ''){ ?>
                        player.watermark({
                            file: "<?php echo $v_logo; ?>",
                            xrepeat: 0,
                            opacity: 0.75
                        });
                        if (is_mobile !== 0) {
                            $("#video_block_html5_api").attr('poster', item_poster);
                            $("#video_block").attr('poster', item_poster)
                            $(".vjs-watermark").attr("style", "bottom:40px;right:1%;width:7%;cursor:pointer;");
                        }else{
                            $(".vjs-watermark").attr("style", "bottom:46px;right:1%;width:7%;cursor:pointer;");
                        }
                        $(".vjs-watermark").click(function () {
                            window.history.back();
                        });
                    <?php } ?>
                    if($("div.vjs-subtitles-button").length)
                    {
                        var divtagg = $(".vjs-subtitles-button .vjs-control-content .vjs-menu .vjs-menu-content li:first-child");
                        var divtaggVal = divtagg.html();
                        if(divtaggVal === 'subtitles off'){
                            divtagg.html("Subtitles Off");
                        }
                    }
                    $("#vid_more_info").hide();
                    $("#episode_block").html("");
                    $("#episode_block").hide();
                    if (is_mobile !== 0) {
                        $(".vjs-big-play-button").show();
                        player.pause();
                    } else {
                        if (full_movie.indexOf('http://youtu') > -1) {
                            player.src({type: "video/youtube", src: "http://youtu.be/32xWXN6Zuio"});
                            player.play();
                        }else{
                            if(browserName === "Safari"){
                                player.pause();
                                var videoToBePlayed = multipleVideoResolution["<?php echo $defaultResolution; ?>"];
                                $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data,browser_name:browserName}, function (res) {
                                    player.src(res).play();
                                });
                            }else{
                                player.src(full_movie).play();
                            }
                        } 
                    }
                
                <?php if($play_percent != 0 && $play_length != 0) { ?>
                 player.pause();
                 play_status = 1;
                 $('#play_confirm').modal('show');
                 $(document).bind('keydown', function(e) {
                if (e.keyCode === 13) {
                     $('#confirm_yes').trigger('click');      
                }
                });
                <?php } ?>
                    $(".vjs-control-bar").show();
                    // BACK BUTTON OF VIDEO PLAYER DOESN'T HIDE IN FULLSCREEN MODE.  
                    setInterval(function() {
                        var user_active = player.userActive();
                        if(user_active === true){
                            $('#backButton').show();
                        } else {
                            $('#backButton').hide();
                        }
                   }, 1000);
                    <?php 
                        if($showSetting == 1){ 
                            $showRes = 'AUTO ( '.$defaultResolution.'p )';
                            if($defaultResolution == 'BEST'){
                                $showRes = 'AUTO ( '.$defaultResolution.' )';
                            }
                    ?>
                            $('.vjs-res-button').html('<span onclick="showSettingDiv();" data-toggle="tooltip" title="Setting" onclick><svg xmlns:xlink="http://www.w3.org/1999/xlink" height="100%" version="1.1" viewBox="0 0 36 36" width="100%"><defs><path d="M27,19.35 L27,16.65 L24.61,16.65 C24.44,15.79 24.10,14.99 23.63,14.28 L25.31,12.60 L23.40,10.69 L21.72,12.37 C21.01,11.90 20.21,11.56 19.35,11.38 L19.35,9 L16.65,9 L16.65,11.38 C15.78,11.56 14.98,11.90 14.27,12.37 L12.59,10.69 L10.68,12.60 L12.36,14.28 C11.89,14.99 11.55,15.79 11.38,16.65 L9,16.65 L9,19.35 L11.38,19.35 C11.56,20.21 11.90,21.01 12.37,21.72 L10.68,23.41 L12.59,25.32 L14.28,23.63 C14.99,24.1 15.79,24.44 16.65,24.61 L16.65,27 L19.35,27 L19.35,24.61 C20.21,24.44 21.00,24.1 21.71,23.63 L23.40,25.32 L25.31,23.41 L23.62,21.72 C24.09,21.01 24.43,20.21 24.61,19.35 L27,19.35 Z M18,22.05 C15.76,22.05 13.95,20.23 13.95,18 C13.95,15.76 15.76,13.95 18,13.95 C20.23,13.95 22.05,15.76 22.05,18 C22.05,20.23 20.23,22.05 18,22.05 L18,22.05 Z" id="ytp-svg-39"></path></defs><use class="ytp-svg-shadow" xlink:href="#ytp-svg-39"></use><use class="ytp-svg-fill" xlink:href="#ytp-svg-39"></use></svg></span>');
                            $(".vjs-control-bar").append('<div onclick="showResDiv();" class="customized-res" id="settingDiv"><span style="text-align:left;">Quality<span> <span style="text-align:right; float:right;"><span id="currentPlayerRes"><?php echo $showRes; ?></span>&nbsp;&nbsp;<b>&gt;</b></span></div>');
                            $(".vjs-control-bar").append('<div id="video-res-sec" class="customized-res"></div>');
                            videoChangeRes('AUTO',player);
                            if (is_mobile !== 0) {    
                                $("#settingDiv").attr("style", "bottom:40px; width:140px;");
                                $("#video-res-sec").attr("style", "bottom:40px; width:60px;");
                            }else{
                                $("#settingDiv").attr("style", "bottom:46px; width:140px;");
                                $("#video-res-sec").attr("style", "bottom:46px; width:60px;");
                            }
                            if (isiPad) {
                                $("#settingDiv").bind('touchstart', function() {
                                    showResDiv();
                                });
                                $(".vjs-res-button").bind('touchstart', function(event) {
                                    showSettingDiv();
                                    $("#video-res-sec").bind('touchstart', function(event) {
                                        showResDiv(); 
                                    });
                                });
                            }
                    <?php } ?>
                    player.on("play", function () {
	                    document.getElementById("video_block").style.cursor = 'default';
                        //$('.vjs-loading-spinner').hide();
                        if($("div.vjs-poster").length)
                        {
                            $(".vjs-poster").attr('style','display: none;');
                        }
                        if (is_mobile !== 0) {
                            $(".vjs-big-play-button").hide();
                        }
                        videoOnPause = 0;
                        $("#episode_block").hide();
                    });

                    player.on("pause", function () {
                        videoOnPause = 1;
                        $("#episode_block").hide();
                    });

                    player.on('timeupdate', function () {
                        previousTime = currentTime;
                        currentTime = player.currentTime();
                        previousBufferEnd = bufferDurationaas;
                        var r = player.buffered();
                        var buffLen = r.length;
                        buffLen = buffLen - 1;
                        bufferDurationaas = r.end(buffLen);
						 t = currentTime + 1;
		                if(currentTime  < t)
		                {
		                   document.getElementById("video_block").style.cursor = 'default';
		                }
                        if(checkurlactive != 0 && currentTime>checkurlactive){
                                checkurlactive =0;
                        }
                    });

                    player.on('changeRes', function () {
                        forChangeRes = 123;
                        seekStart = previousTime;
                        var currTim = previousTime;
                        
                        updateNewBuffered(player,previousBufferEnd,currTim);
                        createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim,browserName);
                        forChangeRes = 0;
                    });
                    player.on("progress",function(){
                        if (is_mobile === 0) {
                            if(videoOnPause === 0 && browserName=== "Safari" ){
                                //checkBuffer(player);
                            }
                        }
                    });
                    //Implementing Ad Codes
                    <?php
                    if (@$mvstream->enable_ad == 1) {
                        $adn = AdNetworks::model()->findByPk($studio_ad->ad_network_id);
                        if ($studio_ad->ad_network_id == 1) {
                            ?>
                            if (is_mobile === 1)
                                ad_url = "http://search.spotxchange.com/vast/2.00/<?php echo $studio_ad->channel_id ?>?VPI=MP4&content_page_url=" + encodeURIComponent(window.location.href) + "&cb=" + Math.random();
                            else
                                ad_url = "http://search.spotxchange.com/vast/2.00/<?php echo $studio_ad->channel_id ?>?VPAID=0&content_page_url=" + encodeURIComponent(window.location.href) + "&cb=" + Math.random() + "&player_width=" + $('#video_block').innerWidth() + "&player_height=" + $('#video_block').innerHeight();
                            player.ads();
                            adavail = 1;
                            <?php
                        }
                        if (@$studio_ad->ad_network_id == 2) {
                            ?>
                            ad_url = "http://shadow01.yumenetworks.com/dynamic_preroll_playlist.vast2xml?domain=<?php echo $studio_ad->channel_id ?>";
                            player.ads();
                            adavail = 1;
                            <?php
                        }
                        if (@$mvstream->rolltype == 1) {
                            ?>
                            var adCalled = 0;
                            player.one("loadstart", function () {
                                if (adCalled === 0)
                                {
                                    player.vast({
                                        skip: 5,
                                        url: ad_url
                                    });
                                    adCalled = 1;
                                }
                            });

                            player.one('adend', function (e) {
                                adavail = 2;
                                e.stopPropagation();
                                createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data,0,browserName);
                            });
                            player.one('adserror', function(e){
                                adavail = 2;
                            });

                            player.one('adtimeout', function(e){
                                adavail = 2;
                            });  
                            player.one('error', function(e){
                                adavail = 2;
                            }); 
                            player.one('adscanceled', function(e){
                                adavail = 2;
                            });
                            <?php
                        }
                        if (@$mvstream->rolltype == 2) {

                            $roll_after = 0;
                            $roll_parts = explode(':', $mvstream->roll_after);
                            if ($roll_parts[0] > 0)
                                $roll_after += 3600 * $roll_parts[0];
                            if ($roll_parts[1] > 0)
                                $roll_after += 60 * $roll_parts[1];
                            if ($roll_parts[2] > 0)
                                $roll_after += $roll_parts[2];
                            ?>
                            var state = {};
                            var midrollPoint = <?php echo $roll_after ?>;
                            var ad_played = 0;

                            player.on('timeupdate', function (event) {
                                var currentTime = player.currentTime(), opportunity;
                                currentTime = currentTime.toFixed(0);

                                if (ad_played === 0 && currentTime === midrollPoint)
                                {
                                    ad_played = 1;
                                    player.vast({
                                        skip: 5,
                                        url: ad_url
                                    });
                                    player.ads.endLinearAdMode();
                                    previousTime = player.currentTime();
                                }

                            });

                            player.one('adend', function (e) {
                                adavail = 2;
                                e.stopPropagation();
                                createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data,0,browserName);
                            });
                            player.one('adserror', function(e){
                                adavail = 2;
                            });

                            player.one('adtimeout', function(e){
                                adavail = 2;
                            });  
                            player.one('error', function(e){
                                adavail = 2;
                            }); 
                            player.one('adscanceled', function(e){
                                adavail = 2;
                            });
                            <?php
                        }
                    }
                    ?>  
                    var x =1;
                    $("video").on("seeking", function () {
                        setInterSpeed.ips_clear(); //clear
                        setInterSpeed.ips_restart();//re-init
                        console.log("------------seeking 1st time "+ x)
                        x+=1;
                        document.getElementById("video_block").style.cursor = 'none';
                        var currTimmm = previousTime;
                        var currTim = player.currentTime();
                        console.log(previousBufferEnd + "-" + currTimmm);
                        console.log( bufferDurationaas + "-" + currTim);
                        if (forChangeRes === 0) {
                            if (previousBufferEnd < currTim) {
                                console.log(browserName);
                                if (seekStart === null) {
                                    console.log("seek star");
                                    seekStart = previousTime;
                                    createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, player.currentTime(),browserName);
                                }
                            }
                        }
                        //focefully-show-subtitles
                        if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                        {
                            $('.vjs-subtitles').removeAttr("style");
                            $('.vjs-text-track').removeAttr("style");
                            $('.vjs-tt-cue').removeAttr("style");
                            $('.vjs-text-track').attr("style","display:block");
                            }
                    });

                    $("video").on('seeked', function () {
                            seekStart = null;

                    });
                    //Implementng Video Logs
                    if (is_studio_admin !== "true") {
                        <?php
                        if(Yii::app()->aws->isIpAllowed() && isset(Yii::app()->user->add_video_log) && (Yii::app()->user->add_video_log == 1)){?>
                            var started = 0;
                            var ended = 0;
                            var logged = 0;
                            var log_id = 0;
                        setInterval(function(){
                            if (adavail === 0 || adavail === 2) {
                                var curlength = player.currentTime();
                                var buff_log_id = document.getElementById('buff_log_id').value;
                                updateBuffered(player, curlength, buff_log_id);
                            }
                        }, 60000);
                            
                            player.on('loadedmetadata', function() {
                                var duration = player.duration();
                                $('#full_video_duration').val(duration);
                                if (typeof player.getCurrentRes === "function") {
                                    var video_resolution = player.getCurrentRes();
                                } else {
                                    var video_resolution = 144;
                                }
                                $('#full_video_resolution').val(video_resolution);
                                buffered_loaded();
                                //focefully-show-subtitles
                                if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                                {
                                    $('.vjs-subtitles').removeAttr("style");
                                    $('.vjs-text-track').removeAttr("style");
                                    $('.vjs-tt-cue').removeAttr("style");
                                    $('.vjs-text-track').attr("style","display:block");
                                    }
                             });
                        <?php } ?>
                    }
                });
            });
          
        // Create Sign Url Forpage
        function createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTimess, browserName) {
            document.getElementById("video_block").style.cursor = 'none';
            console.log('play video in Muvi');
            $(".vjs-subtitles ").hide();
            var currentTimesForVideo = currentTimess;
            var play_time = player.currentTime();
            $(".vjs-error-display").addClass("hide");
           
           // $(".vjs-control-bar").hide();
            if (typeof player.getCurrentRes === "function") {
                var currentVideoResolution = player.getCurrentRes();
            } else {
                var currentVideoResolution = 144;
            }
            if(checkurlactive!=0){
                return false;
            }
            if (is_mobile === 0) {
                var r = player.buffered();
                var buffLen = r.length;
                if (buffLen > 0) {
                    buffLen = buffLen - 1;
                } else {
                    buffLen = 0;
                }
                var bufferenEnddd = r.end(buffLen);
                var bufferStart = r.start(buffLen);
                if (typeof player.getCurrentRes === "function") {
                    var currentVideoResolution = player.getCurrentRes();
                } else {
                    var currentVideoResolution = 144;
                }
                lastBufferSize = 0;
                var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                $('#full_video_resolution').val(currentVideoResolution);
                
                var vid_duration = parseInt(Math.round(player.duration()%60));
                var curr_duration =  parseInt(Math.round(player.currentTime()%60));
                console.log("browserName ---- "+ browserName);
            //Ajax Call Start
            $.ajax({
                url: createSignedUrl,
                type: 'post',
                data: {
                        video_url: videoToBePlayed+'?t='+currentTimess, 
                        wiki_data: wiki_data, 
                        browser_name: browserName},
                success: function(res) {
                if(res)
                {
                    player.src(res).play();
                    checkurlactive = currentTimess+10;
                    $('#custVidRes li').each(function() {
                        if ($(this).attr('selected') == 'selected') {
                            var newRes = currentVideoResolution + 'p';
                            if (currentVideoResolution == 'BEST') {
                                var newRes = currentVideoResolution;
                            }
                            if ($(this).attr('class') == 'AUTO') {
                                $("#currentPlayerRes").html('AUTO ( ' + newRes + ' )');
                            } else {
                                $("#currentPlayerRes").html(newRes);
                            }
                        }
                    });
                    
                    $(".vjs-error-display").removeClass("hide");
                    player.on("loadedmetadata", function() {
                        player.currentTime(currentTimess);
                        //focefully-show-subtitles
                        if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                        {
                            $('.vjs-subtitles').removeAttr("style");
                            $('.vjs-text-track').removeAttr("style");
                            $('.vjs-tt-cue').removeAttr("style");
                            $('.vjs-text-track').attr("style","display:block");
                            }
                    });
                }
            },
            error: function(jqXhr, textStatus, errorThrown){ console.log("Error :: " +  errorThrown ); }
            });
        //Ajax Call End  
	} else {
                $('#custVidRes li').each(function() {
                    if ($(this).attr('selected') == 'selected') {
                        var newRes = currentVideoResolution + 'p';
                        if (currentVideoResolution == 'BEST') {
                            var newRes = currentVideoResolution;
                        }
                        if ($(this).attr('class') == 'AUTO') {
                            $("#currentPlayerRes").html('AUTO ( ' + newRes + ' )');
                        } else {
                            $("#currentPlayerRes").html(newRes);
                        }
                    }
                });
            }
        }
        function resize_player() {
            if (full_screen === false) {
                full_screen = true;
                var large_screen = setTimeout(function () {
                    if (full_screen === true) {
                    }
                }, 5000);
            } else {
                //clearTimeout(large_screen);
                full_screen = false;
            }
        }
            // If Safari browser
            function checkBuffer(player){
                    var currentTime = player.currentTime();
                    var playerDuration = player.duration();
                    var r = player.buffered();
                    var buffLen = r.length;
                    buffLen = buffLen - 1;
                    var bufferenEnddd = r.end(buffLen);
                    var bufferenEndd = bufferenEnddd - 1;
                    if(bufferenEndd < currentTime){
                        if(playerDuration !== bufferenEnddd){
                            player.pause();
                        }else{
                            player.play();
                         }
                    }else{
                        if(bufferenEnddd !== currentTime){
                            if(player.paused()){
                                player.play();
                            }
                        }
                    }
            }
        </script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/screentime.js"></script>
        <script>
            <?php
                $action = Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
                $action = explode('/', $action);
                $page = $action[count($action)-1];
                if(strpos($action[count($action)-1],'?') !== FALSE){
                    $page = $action[count($action)-2];
                }
                $full_domain = 'www.'.$studio->domain;
                 if($page == $studio->domain || $page == $full_domain){
                    $page = 'home';
                }
            ?>
            $.screentime({
              fields: [
                    { selector: 'html',
                      name: 'HTML'
                    }
              ],
              reportInterval: <?php echo REPORT_INTERVAL;?>,
              googleAnalytics: false,
              callback: function(data, log) {
                    <?php
                        $action = Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
                        $action = explode('/', $action);
                        $page = $action[count($action)-1];
                        if(strpos($action[count($action)-1],'?') !== FALSE){
                            $page = $action[count($action)-2];
                        }
                        $full_domain = 'www.'.$studio->domain;
                         if($page == $studio->domain || $page == $full_domain){
                            $page = 'home';
                        }
                    ?>
                    var timeInMs = Date.now();
                    var page = btoa(timeInMs+"<?php echo $page;?>"+timeInMs);
                    var timeDiff = <?php echo REPORT_INTERVAL;?>/60;
                    var urlForReport = "<?php echo Yii::app()->baseUrl; ?>/site/logVisitor?pid="+page;
                    $.post(urlForReport, {timeDiff:timeDiff}, function (res) {});
              }
            });
            
                        
            var interval;
              function buffered_loaded() {
                //var v = document.getElementById('video_block');
                var v = videojs('video_block');
                if (!v.buffered) {
                    updateBuffered(v,'',-1);
                } else {
                    updateBuffered(-1,'',-1);
                  //interval = setInterval(updateBuffered, 60000);
                }
              }
              
              var buff_log_id = 0;
              var u_id = 0;
              var resolution = 144;
              
              function updateNewBuffered(player,previousBufferEnd,currTim){
                var buff_log_id = document.getElementById('buff_log_id').value;
                updateBuffered(player,currTim,buff_log_id);
                var bufferenEnddd = previousBufferEnd;
                var bufferenStart = currTim;
                var resolution = player.getCurrentRes();
                
                $.post('<?php echo Yii::app()->baseUrl; ?>/report/addNewBufferLog', {movie_id: "<?php echo $movie_id ?>",video_id:"<?php echo isset($stream_id) ? $stream_id : 0 ?>",start_time: bufferenStart, end_time: bufferenEnddd, resolution:resolution }, function (res) {
                    var obj = JSON.parse(res);
                    buff_log_id = obj.id;
                    u_id = obj.u_id;
                    document.getElementById('u_id').value = u_id;
                    if(buff_log_id){
                        document.getElementById('buff_log_id').value = buff_log_id;
                    }
                });
              }
              
              function updateBuffered(player,currTim,log_id) {
                var duration = parseInt(document.getElementById('full_video_duration').value);
                if((typeof player == 'undefined') || player == -1){
                    var v = videojs('video_block');
                    var r = v.buffered();
                }else{
                    var r = player.buffered();
                }
                if (r) {
                    var buffLen = r.length;
                    if(buffLen > 0){
                        buffLen = buffLen - 1;
                    }else{
                        buffLen = 0;
                    }
                    var bufferenEnddd = r.end(buffLen);  
                    if(buffLen < 0){
                        var bufferenStart = 0;
                    }else{
                        var bufferenStart = r.start(buffLen);
                    }
                    var resolution = document.getElementById('full_video_resolution').value;
                    
                    var buff_log_id = document.getElementById('buff_log_id').value;
                    var u_id = document.getElementById('u_id').value;
                    //if(parseFloat(duration) <= parseFloat(bufferenEnddd)){
                        if(parseFloat(log_id) || parseFloat(buff_log_id)){
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo Yii::app()->baseUrl; ?>/report/addBufferLog',
                                data: {movie_id: "<?php echo $movie_id ?>",video_id:"<?php echo isset($stream_id) ? $stream_id : 0 ?>",start_time: bufferenStart, end_time: bufferenEnddd, buff_log_id : buff_log_id,resolution:resolution,u_id:u_id },
                                success: function(res){
                                    var obj = JSON.parse(res);
                                    buff_log_id = obj.id;
                                    u_id = obj.u_id;
                                    document.getElementById('u_id').value = u_id;
                                    if(buff_log_id){
                                        document.getElementById('buff_log_id').value = buff_log_id;
                                    }
                                }
                            });
                        }
                    //}
                    
                    
                    /*$.post('<?php echo Yii::app()->baseUrl; ?>/report/addBufferLog', {movie_id: "<?php echo $movie_id ?>",video_id:"<?php echo isset($stream_id) ? $stream_id : 0 ?>",start_time: bufferenStart, end_time: bufferenEnddd, buff_log_id : buff_log_id,resolution:resolution,u_id:u_id }, function (res) {
                        var obj = JSON.parse(res);
                        buff_log_id = obj.id;
                        u_id = obj.u_id;

                    });*/
                    
                }
              }
            function checkInterNetSpeed(multipleVideoResolution,player){
                return false;
                //console.log("-------------------------------------@1")
                if(ended === 0 && videoOnPause === 0 && (adavail === 0 || adavail === 2) ){
                    var r = player.buffered();
                    var buffLen = r.length;
                    if(buffLen > 0){
                        buffLen = buffLen - 1;
                    }else{
                        buffLen = 0;
                    }
                    var bufferenEnddd = r.end(buffLen);
					var bufferStart = r.start(0);
					var extraBufferSize = 0;
					if(cnt==0)
						lastBufferSize = r.start(0);
					else
						lastBufferSize = lastBufferSize
					var videoDuration = 60;
					var currentVideoResolution = 240;
					var videoResolutionSizePerRes = player.getCurrentRes();
                    var playedTime = (bufferenEnddd - lastBufferSize);
					lastBufferSize = playedTime;
                    if(lastBufferSize === 0){
						playedTime = (bufferenEnddd - bufferStart);
                    }
                    if(playedTime !== 0){
                        var userIntSpeed = ((videoResolutionSizePerRes/videoDuration)*playedTime) + extraBufferSize;
                        var speedMbps = ((userIntSpeed/1024) / (30)).toFixed(2);
                        lastBufferSize = bufferenEnddd;
                        extraBufferSize = 0;
                    if(bitvideoChangeRes){ 
                        //If forcefully video quality changed then video will play the same...
                        player.changeRes(videoResolutionSizePerRes);
                    }else{
                            //video quality will fix as per internet speed detection...
                    if(parseInt(Math.round(player.duration())) == parseInt(Math.round(lastBufferSize))){
                        player.changeRes(videoResolutionSizePerRes);
                    }else{
                            if (speedMbps !== null && userIntSpeed !== null) {
                            var changeUrlAccInternetSpeed = "<?php echo Yii::app(true)->baseUrl; ?>/user/getNewSignedUrlForInternetSpeedEmb";
							console.log(speedMbps + '---' + videoResolutionSizePerRes + '---' + playedTime + '---' + userIntSpeed + '---' + videoDuration);
                            $.ajax({
                                    type:'POST',
                                    url:changeUrlAccInternetSpeed,
                                    data:{multiple_video_url: multipleVideoResolution, user_internet_speed: speedMbps},
                                    success: function(res) {
                                        try {
                                            console.log('-ips-start');
                                            if (typeof player.getCurrentRes === "function") {
                                                //console.log("-------------------------------------@8")
                                                if (videoResolutionSizePerRes !== res) {
													if(ended === 0 && videoOnPause === 0 && (adavail === 0 || adavail === 2) ){
														player.changeRes(res);
														var videoRes = res;
														if(videoRes.toLowerCase().match('best'))
															$('#settingDiv').html('Quality<span> <span style="text-align:right; float:right;"><span id="currentPlayerRes"> AUTO ( '+videoRes+' )</span>&nbsp;&nbsp;</b></span>');
														else
															$('#settingDiv').html('Quality<span> <span style="text-align:right; float:right;"><span id="currentPlayerRes"> AUTO ( '+videoRes+'p )</span>&nbsp;&nbsp;</b></span>');
													}
												 }
                                            }
                                        }
                                        catch (err) {
                                        console.log("err.message ::: " + err.message);
                                        }
                                    }
								});
                                cnt++;
                                console.log('-ips-end');
						}
					}
				}
              }
            }
            }
            function showSettingDiv(){
                if($('#settingDiv:visible').length == 0){
                    if($('#video-res-sec:visible').length == 0){
                        $("#settingDiv").show();
                    }else{
                        $("#video-res-sec").hide();
                    }
                } else{
                    $("#settingDiv").hide();
                }
            }
            function showResDiv(){
                $("#settingDiv").hide();
                if($('#video-res-sec:visible').length == 0){
                    $("#video-res-sec").show();
                } else{
                    $("#video-res-sec").hide();
                }
            }
            function videoChangeRes(videoRes){
                var showMultiReset = '';
                <?php
                    $setResMenu12  = '';
                    foreach ($multipleVideo as $key => $val) {
                        $addRes = '';
                        if($key != 'BEST'){
                            $addRes ='p';
                        }
                        ?>
                            showMultiReset += "<li class=\"<?php echo $key; ?>\" onclick=\"videoChangeRes('<?php echo $key; ?>');\" ontouchend=\"videoChangeRes('<?php echo $key; ?>');\"><span class=\"textlftAlg\"><?php echo $key.$addRes; ?></span></li>";
                        <?php
                        $setResMenu12 .= "<li class=\"".$key."\" onclick=\"videoChangeRes('".$key."');\" ontouchend=\"videoChangeRes('".$key."');\"><span class=\"textlftAlg\">".$key.$addRes."</span></li>";
                    }
                ?>
                showMultiReset += "<li class=\"AUTO\" onclick=\"videoChangeRes('AUTO');\" ontouchend=\"videoChangeRes('AUTO');\"><span class=\"textlftAlg\">AUTO</span></li>";
                $("#video-res-sec").html('<ul id="custVidRes">' + showMultiReset + '</ul>');
                $("." + videoRes).append('<span class="textRgtAlg"><b>&#10004;</b></span>');
                $("." + videoRes).attr('selected','selected');
                if(videoRes === "AUTO"){
                    /*setInterSpeed = setInterval(function(){
                     checkInterNetSpeed(multipleVideoResolution,videoPlayer)
                    },120000);
                    return false;*/
                    setInterSpeed.ips_clear(); //clear
                    setInterSpeed.ips_restart();//re-init
                    bitvideoChangeRes = false;
                    return false;
                } else{
                    clearInterval(setInterSpeed);
                    player.changeRes(videoRes);
                    bitvideoChangeRes = true; 
                }
                $("#settingDiv").hide();
                $("#video-res-sec").hide();
            }
        
        function FromDurationPlayed() {  
            percen = <?php echo $play_percent;?> ;
            play_length = <?php echo $play_length;?> ; 
            if(myplay_length!="")
                player.currentTime(myplay_length);
            player.play();
            play_status = 0;
            $('.modal').hide();
            player.on('loadedmetadata',function(){
                player.currentTime("<?php echo $play_length;?>");
                if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                {
                    $('.vjs-subtitles').removeAttr("style");
                    $('.vjs-text-track').removeAttr("style");
                    $('.vjs-tt-cue').removeAttr("style");
                    $('.vjs-text-track').attr("style","display:block");
                }
            });
        }
        function FromBeginning() {
            percen = 5 ;
            play_length = 0 ;
            player.play();
            play_status = 0;
            $('.modal').hide();     
        }
        </script>
        <style>
            .ytp-svg-shadow {
                stroke: #000;
                stroke-opacity: .15;
                stroke-width: 2px;
                fill: none;
            }
            .ytp-svg-fill {
                fill: #ccc;
            }
            .customized-res{
                display:none; 
                z-index: 2000;
                right:10%;
                padding:0px 5px 0px 5px;
                cursor:pointer; 
                position:absolute;
                visibility: visible;
                opacity: 0.1%;
                line-height: 23px;
                -webkit-transition: visibility .1s,opacity .1s;
                -moz-transition: visibility .1s,opacity .1s;
                -o-transition: visibility .1s,opacity .1s;
                transition: visibility .1s,opacity .1s;
                background-color: #07141e;
                background-color: rgba(7,20,30,.7);
                border-radius: 3px;
            }
            #video-res-sec ul{
                list-style: none;
                margin: 0px;
                padding: 0px;
            }
            #video-res-sec ul li{
                list-style: none;
                margin: 0px;
                padding: 0px;
            }
            .textlftAlg{
                text-align:left;
            }
            .textRgtAlg{
                text-align:right; float:right;
            }
        </style>      
        <?php $this->renderPartial('//layouts/restrict_streaming_device', array('stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction)); ?>
        <?php $this->renderPartial('//layouts/video_view_log', array('stream_id' => $stream_id, 'movie_id' => $movie_id)); ?>
    </body>
</html>
