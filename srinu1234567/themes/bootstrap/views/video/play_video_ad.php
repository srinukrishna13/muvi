<?php
$v = 7;
$randomVar = rand();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title; ?> | <?php echo $this->studio->name; ?></title>
        <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo $this->siteurl; ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>
        <script type='text/javascript'>
<?php if ($this->studio->id == 1087) { ?>
                var muviWaterMark = "<b><?php echo Yii::app()->request->userHostAddress; ?></b>";
<?php } else {
    ?>var muviWaterMark = "";<?php }
?>
        </script>
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo $this->siteurl ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->siteurl ?>/common/bootstrap/css/bootstrap.min.css" />          

        <script data-cfasync="false" type="text/javascript" src="<?php echo $this->siteurl; ?>/vast/js/video.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo $this->siteurl; ?>/js/vjs.youtube.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo $this->siteurl; ?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
        <!--<script src="<?php echo Yii::app()->baseUrl; ?>/js/videojs.hls.min.js"></script>-->
        <link href="<?php echo $this->siteurl; ?>/common/css/small_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->siteurl; ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

        <link href="<?php echo $this->siteurl; ?>/vast/css/videojs.ads.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->siteurl; ?>/vast/css/videojs.vast.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->siteurl; ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->siteurl; ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

        <script src="<?php echo $this->siteurl; ?>/vast/js/videojs.ads.js"></script>
        <script src="<?php echo $this->siteurl; ?>/vast/js/vast-client.js"></script>
        <script src="<?php echo $this->siteurl; ?>/vast/js/videojs.vast.js"></script>   


        <link href="<?php echo $this->siteurl; ?>/video/quality/video-quality-selector.css?rand=<?php echo $randomVar; ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo $this->siteurl; ?>/video/quality/video-quality-selector.js?rand=<?php echo $randomVar; ?>"></script>        


        <?php if ($ad_enabled > 0) { ?>
            <!-- Video Ad -->
            <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/es5-shim.js"></script>
            <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/ie8fix.js"></script>
            <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/swfobject.js"></script>
            <?php if ($ad_type == 'vpaid') { ?>
                <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/videojs-vast-vpaid.js"></script>
                <script type="text/javascript" src="<?php echo $this->siteurl; ?>/muviplayer/scripts/adplayer.js"></script>
            <?php } ?>                  


            <link rel="stylesheet" type="text/css" href="<?php echo $this->siteurl; ?>/muviplayer/styles/videojs.vast.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->siteurl; ?>/muviplayer/styles/videojs.vpaid.css"/>   

            <?php if ($ad_network_id == 3) { ?>
                <script type="text/javascript" src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
                <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.ima.js"></script> 
            <?php } ?>
            <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/contrib/videojs.ads.css" rel="stylesheet" type="text/css">        
        <?php } ?>      

        <script data-cfasync="false" type="text/javascript">
            videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
        </script>
        <?php
        if (isset($block_ga)) {
            $block_ga = Yii::app()->session['block_ga'];
            if ($this->studio->google_analytics != '' && $block_ga != 0) {
                ?>
                <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', '<?php echo html_entity_decode($this->studio->google_analytics); ?>', 'auto');
                    ga('send', 'pageview');
                </script>
                <?php
            }
        }
        ?>        
    </head>
    <body>
        <input type="hidden" id="full_video_duration" value="" />
        <input type="hidden" id="full_video_resolution" value="" />
        <input type="hidden" id="u_id" value="0" />
        <input type="hidden" id="buff_log_id" value="0" />
        <div class="wrapper">
            <div class="videocontent" style="overflow:hidden;">
                <video id="video_block" crossdomain class="video-js moo-css vjs-default-skin" controls autoplay preload="auto" autobuffer poster="" data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution; ?>" } }}'>
                    <?php
                    foreach ($multipleVideo as $key => $val) {
                        echo '<source src="' . $fullmovie_path . '" data-res="' . $key . '" type="video/mp4" />';
                    }
                    foreach($subtitleFiles as $subtitleFilesKey => $subtitleFilesval){
                        if($subtitleFilesKey == 1){
                            echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" default="true" ></track>';
                        } else{
                            echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" ></track>';
                        }
                    }
                    ?>
                </video>
            </div> 
        </div>
        <div id="quotes"></div>
        <div id="episode_block" class="episode_block">
            <a href="play_video_backup.php"></a>
        </div>

        <?php
        if (strtolower(@$movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower(@$movieData['content_type']);
        }
        ?>
        <?php
        if (strtolower(@$movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower(@$movieData['content_type']);
        }
        ?>
        <script type="text/javascript">
            var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = true;

            var setInterSpeed = '';
            var multipleVideoResolution = new Array();
            multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;
            var ended = 0;
            var videoOnPause = 0;
            var adavail = 0;
            var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/video/getNewSignedUrlForPlayer";
            var nAgt = navigator.userAgent;
            var browserName = navigator.appName;
            var verOffset;
            // In Opera 15+, the true version is after "OPR/" 
            if ((verOffset = nAgt.indexOf("OPR/")) != -1) {
                browserName = "Opera";
            }
            // In older Opera, the true version is after "Opera" or after "Version"
            else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
                browserName = "Opera";
            }
            // In MSIE, the true version is after "MSIE" in userAgent
            else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
                browserName = "Microsoft Internet Explorer";
            }
            // In Chrome, the true version is after "Chrome" 
            else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
                browserName = "Chrome";
            }
            // In Safari, the true version is after "Safari" or after "Version" 
            else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
                browserName = "Safari";
            }
            // In Firefox, the true version is after "Firefox" 
            else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
                browserName = "Firefox";
            }
            var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
            $(document).ready(function () {
                var movie_id = "<?php echo $movie_id ?>";
                var full_movie = "<?php echo $fullmovie_path ?>";
                var can_see = "<?php echo $can_see ?>";
                var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";
                var wiki_data = "<?php echo isset($wiki_data) ? $wiki_data : '' ?>";
                var url = "<?php echo Yii::app()->baseUrl; ?>/video/add_log";
                var episode_id = "<?php echo isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0 ?>";
                var content_type = "<?php echo isset($content_type) ? $content_type : "" ?>";

                var previousTime = 0;
                var currentTime = 0;
                var seekStart = null;
                var forChangeRes = 0;
                var bufferenEnddd = 0;
                var bufferenEndd = 0;
                var previousBufferEnd = 0;
                var bufferDurationaas = 0;
                var firstVideoPlay = 0;
                var stream_id = "<?php echo isset($stream_id) ? $stream_id : 0 ?>";

                var is_restrict = "<?php echo isset($is_restrict) ? $is_restrict : 0 ?>";
                var is_studio_admin = "<?php echo Yii::app()->session['is_studio_admin'] ?>";

                var player;
                var playerHeight = window.innerHeight - 5;
                var playerWidth = window.innerWidth - 5;
                var ad_src;
                var ad_src1;

                $('#video_block').width(playerWidth).height(playerHeight);

<?php
if ($ad_enabled == 1) {
    if ($ad_network_id == 1 || $ad_network_id == 2) {
        ?>
        <?php if ($ad_network_id == 1) { ?>
                            var ad_src = 'https://search.spotxchange.com/vast/2.00/<?php echo $channel_id; ?>?VPAID=1&content_page_url=' + encodeURIComponent(window.location.href) + '&cb=' + Math.random() + '&player_width=' + playerWidth + '&player_height=' + playerHeight;
                            var ad_src1 = 'https://search.spotxchange.com/vast/2.00/<?php echo $channel_id; ?>?VPAID=0&content_page_url=' + encodeURIComponent(window.location.href) + '&cb=' + Math.random() + '&player_width=' + playerWidth + '&player_height=' + playerHeight;
        <?php } else if ($ad_network_id == 2) { ?>
            <?php if ($ad_type == 'vpaid') { ?>
                                ad_src = "http://shadow01.yumenetworks.com/yvp/21/xml/<?php echo $channel_id; ?>/vpaid_html5.xml";
            <?php } else { ?>
                                ad_src = ad_src1 = "http://shadow01.yumenetworks.com/dynamic_preroll_playlist.vast2xml?domain=<?php echo $channel_id; ?>";
            <?php } ?>
        <?php } ?>
        <?php
    }
    if (($ad_network_id == 1 || $ad_network_id == 2) && count($ad_timing) < 1) {
        ?>
                        var playerPlugins = {
                            "plugins": {
                                "ads-setup": {
                                    "adCancelTimeout": 200000, // Wait for ten seconds before canceling the ad.
                                    "adsEnabled": true,
                                    adTagUrl: ad_src,
                                    responseTimeout: 500000
                                },
                                "resolutionSelector": {
                                    force_types: ['video/mp4'],
                                    default_res: "<?php echo $defaultResolution; ?>"
                                }
                            },
                            responsive: true,
                            width: playerWidth,
                            height: playerHeight
                        };
    <?php } else { ?>
                        var playerPlugins = {
                            "plugins": {
                                "resolutionSelector": {
                                    force_types: ['video/mp4'],
                                    default_res: "<?php echo $defaultResolution; ?>"
                                }
                            },
                            responsive: true,
                            width: playerWidth,
                            height: playerHeight
                        };
        <?php
    }
} else {
    ?>
                    var playerPlugins = {
                        "plugins": {
                            "resolutionSelector": {
                                force_types: ['video/mp4'],
                                default_res: "<?php echo $defaultResolution; ?>"
                            }
                        },
                        responsive: true,
                        width: playerWidth,
                        height: playerHeight
                    };
<?php }
?>
                videojs('#video_block', playerPlugins, function () {
                    if (typeof player == 'undefined')
                        player = this;
<?php
if ($ad_enabled == 1) {
    if ($ad_network_id == 3) { ?>
                        var options = {
                            id: 'video_block',
                            adTagUrl: "https://googleads.g.doubleclick.net/pagead/ads?client=<?php echo $channel_id; ?>&w=" + playerWidth + "&h=" + playerHeight + "&description_url=" + encodeURIComponent(window.location.href) + "&max_ad_duration=60"
                        };
                        player.ima(options);
                        <?php if ($rolltype == 1 || $rolltype == 3) {?>
                        player.ima.requestAds();
                        <?php } ?>
    <?php
    }   
    else if ($rolltype == 1 || $rolltype == 3) {
        ?>
                            player.ads();
                            player.one("loadstart", function () {
                                player.vast({
                                    skip: 5,
                                    url: ad_src1
                                });
                            });
                            console.log('Preroll');
        <?php
        $found_zero = array_search(0, $ad_timing);
        if($found_zero > -1)
            unset($ad_timing[$found_zero]);
    }?>
    console.log("<?php echo $rolltype;?>");
    <?php if (count($ad_timing) > 0) {
        ?>
                            var tim = new Array();
                            var playedads = new Array();
                            var playindx = 0;
        <?php if ($ad_network_id == 3) { ?>
                                var adavail = 0;
        <?php } else {
            ?>
                                var adavail = 0;
        <?php }
        $j = 0;
        if($rolltype == 2){
        ?> 
                adavail = 1;
                <?php
        }
        foreach ($ad_timing as $adt) {
            ?>
                                tim[<?php echo $j; ?>] = parseInt(<?php echo $adt; ?>);
            <?php
            $j++;
        }        
        ?>
                            player.on('timeupdate', function (event) {

                                var currentTime = player.currentTime(), opportunity;
                                currentTime = parseInt(currentTime);
        <?php if ($ad_network_id == 3) { ?>
                                    if (tim.indexOf(currentTime) != -1 && playedads.indexOf(currentTime) == -1 && adavail == 1 && $('#video_block').hasClass('vjs-ad-playing') === false)
                                    {
                                        console.log("Midroll Ad Playing " + currentTime);
                                        player.ima.requestAds();
                                        adavail = 0;
                                    }
            <?php
        } else {
            ?>
                        console.log(player.ads.state);
                                    if (tim.indexOf(currentTime) != -1 && playedads.indexOf(currentTime) == -1 && adavail == 1 && $('#video_block').hasClass('vjs-ad-playing') === false)
                                    {
                                        <?php if($rolltype == 2) {?>
                                                console.log('Played Index '+playindx);
                                                if(playindx == 0){
                                        player.ads();
                                        player.vast({
                                            skip: 5,
                                            url: ad_src1
                                        });
                                        console.log("Started Only Midroll");
                                                }else{
                                                    console.log("has "+player.ads.state);

                                            //player.trigger('adsready');
                                            //player.ads.startLinearAdMode();
                                            player.vast.preroll();
                                                }
                                        <?php }else{?>
                                            player.vast.preroll();
                                        <?php }?>
                                        console.log('is ad ' + $('#video_block').hasClass('vjs-ad-playing'));
                                        playedads[playindx] = parseInt(currentTime);
                                        playindx++;
                                        console.log("Midroll Ad Playing " + currentTime);

                                        
                                        adavail = 0;
                                    }
        <?php } ?>
                            });
        <?php
    }?>
                            player.on('adend', function (e) {
                                console.log("Ad Ended");
                                adavail = 1;
                                e.stopPropagation();
                                createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, 0, browserName);
                            });            
            <?php
}
?>
                    player.on('error', function () {
                        if (document.getElementsByTagName("video")[0].error != null) {
                            var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                            if (videoErrorCode === 2) {
                                if (seekStart === null) {
                                    seekStart = 123;
                                    createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTime, browserName);
                                }
                            }
                        }
                    });

                    if (is_mobile !== 0) {
                        $("#video_block_html5_api").attr('poster', item_poster);
                        $("#video_block").attr('poster', item_poster);
                    }

                    $(".vjs-tech").mousemove(function () {
                        if (full_screen === true && show_control === false) {
                            $("#video_block .vjs-control-bar").show();
                            show_control = true;
                            var timeout = setTimeout(function () {
                                if (full_screen === true) {

                                }
                            }, 10000);
                            $(".vjs-control-bar").mousemove(function () {
                                event.stopPropagation();
                            }).mouseout(function () {
                                event.stopPropagation();
                            });
                        } else {
                            clearTimeout(timeout);
                        }
                    });

                    player.on("fullscreenchange", resize_player);
                    player.watermark({
                        file: "<?php echo $v_logo; ?>",
                        xrepeat: 0,
                        opacity: 0.75
                    });
                    if (is_mobile !== 0) {
                        $(".vjs-watermark").attr("style", "bottom:40px;right:1%;width:7%;cursor:pointer;");
                    } else {
                        $(".vjs-watermark").attr("style", "bottom:46px;right:1%;width:7%;cursor:pointer;");
                    }
                    $(".vjs-watermark").click(function () {
                        window.history.back();
                    });

                    $("#vid_more_info").hide();
                    $("#episode_block").html("");
                    $("#episode_block").hide();
                    if (is_mobile !== 0) {
                        player.pause();
                        var videoToBePlayed = multipleVideoResolution["<?php echo $defaultResolution; ?>"];
                        $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data, browser_name: browserName}, function (res) {
                            player.src(res).play();
                        });
                    } else {
                        if (full_movie.indexOf('http://youtu') > -1) {
                            player.src({type: "video/youtube", src: "http://youtu.be/32xWXN6Zuio"});
                            player.play();
                        } else {
                            if (browserName === "Safari") {
                                player.pause();
                                var videoToBePlayed = multipleVideoResolution["<?php echo $defaultResolution; ?>"];
                                $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data, browser_name: browserName}, function (res) {
                                    player.src(res).play();
                                });
                            } else {
                                player.src(full_movie).play();
                            }
                        }
                    }

                    player.on("play", function () {
                        videoOnPause = 0;
                        $("#episode_block").hide();
        if (firstVideoPlay === 0) {
            firstVideoPlay = 1;
            setInterval(function () {
                        checkInterNetSpeed(multipleVideoResolution, player)
                    }, 120000);
        }
                            if (firstVideoPlay === 0) {
                                firstVideoPlay = 1;
    <?php
    $showRes = 'AUTO ( ' . $defaultResolution . 'p )';
    if ($defaultResolution == 'BEST') {
        $showRes = 'AUTO ( ' . $defaultResolution . ' )';
    }
    ?>
                                $('.vjs-res-button').html('<span onclick="showSettingDiv();" data-toggle="tooltip" title="Setting" onclick><svg xmlns:xlink="http://www.w3.org/1999/xlink" height="100%" version="1.1" viewBox="0 0 36 36" width="100%"><defs><path d="M27,19.35 L27,16.65 L24.61,16.65 C24.44,15.79 24.10,14.99 23.63,14.28 L25.31,12.60 L23.40,10.69 L21.72,12.37 C21.01,11.90 20.21,11.56 19.35,11.38 L19.35,9 L16.65,9 L16.65,11.38 C15.78,11.56 14.98,11.90 14.27,12.37 L12.59,10.69 L10.68,12.60 L12.36,14.28 C11.89,14.99 11.55,15.79 11.38,16.65 L9,16.65 L9,19.35 L11.38,19.35 C11.56,20.21 11.90,21.01 12.37,21.72 L10.68,23.41 L12.59,25.32 L14.28,23.63 C14.99,24.1 15.79,24.44 16.65,24.61 L16.65,27 L19.35,27 L19.35,24.61 C20.21,24.44 21.00,24.1 21.71,23.63 L23.40,25.32 L25.31,23.41 L23.62,21.72 C24.09,21.01 24.43,20.21 24.61,19.35 L27,19.35 Z M18,22.05 C15.76,22.05 13.95,20.23 13.95,18 C13.95,15.76 15.76,13.95 18,13.95 C20.23,13.95 22.05,15.76 22.05,18 C22.05,20.23 20.23,22.05 18,22.05 L18,22.05 Z" id="ytp-svg-39"></path></defs><use class="ytp-svg-shadow" xlink:href="#ytp-svg-39"></use><use class="ytp-svg-fill" xlink:href="#ytp-svg-39"></use></svg></span>');
                                $(".vjs-control-bar").append('<div onclick="showResDiv();" class="customized-res" id="settingDiv"><span style="text-align:left;">Quality<span> <span style="text-align:right; float:right;"><span id="currentPlayerRes"><?php echo $showRes; ?></span>&nbsp;&nbsp;<b>&gt;</b></span></div>');
                                $(".vjs-control-bar").append('<div id="video-res-sec" class="customized-res"></div>');
                                videoChangeRes('AUTO', player);
                                if (is_mobile !== 0) {
                                    $("#settingDiv").attr("style", "bottom:40px; width:140px;");
                                    $("#video-res-sec").attr("style", "bottom:40px; width:60px;");
                                } else {
                                    $("#settingDiv").attr("style", "bottom:46px; width:140px;");
                                    $("#video-res-sec").attr("style", "bottom:46px; width:60px;");
                                }
                            }
                    });

                    player.on("pause", function () {
                        videoOnPause = 1;
                        $("#episode_block").hide();
                    });

                    player.on('timeupdate', function () {
                        previousTime = currentTime;
                        currentTime = player.currentTime();
                        previousBufferEnd = bufferDurationaas;
                        var r = player.buffered();
                        var buffLen = r.length;
                        buffLen = buffLen - 1;
                        bufferDurationaas = r.end(buffLen);
                    });

                    player.on('changeRes', function () {
                        forChangeRes = 123;
                        seekStart = previousTime;
                        var currTim = previousTime;

                        updateNewBuffered(player, previousBufferEnd, currTim);
                        createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim, browserName);
                        forChangeRes = 0;
                    });

                    player.on("progress", function () {
                        if (videoOnPause === 0 && browserName === "Safari") {
                            checkBuffer(player);
                        }

                    });

                    $("video").on("seeking", function () {
                        console.log("Seeking"); 
                        console.log($('#video_block').attr('class'));
                        if($('#video_block').hasClass('vjs-ad-playing') === false){                            
                            var currTimmm = previousTime;
                            var currTim = player.currentTime();
                            console.log(previousBufferEnd + "-" + currTimmm);
                            console.log(bufferDurationaas + "-" + currTim);
                            if (forChangeRes === 0) {
                                if (previousBufferEnd < currTim) {
                                    console.log(browserName);
                                    if (seekStart === null) {
                                        console.log("seek start");
                                        seekStart = previousTime;
                                        createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim, browserName);
                                    }
                                }
                            }
                        }
                    });

                    $("video").on('seeked', function () {
                        seekStart = null;

                    });
                    //Implementng Video Logs
                    if (is_studio_admin !== "true") {
<?php if (isset(Yii::app()->user->add_video_log) && (Yii::app()->user->add_video_log == 1)) { ?>
                            var started = 0;
                            var ended = 0;
                            var logged = 0;
                            var log_id = 0;
                            var percen = 5;
                            player.on("play", function () {
                                console.log("Ad avail" + adavail);
                                console.log("started " + started);
                                if (started === 0 && (adavail === 0 || adavail === 2)) {
                                    $.post(url, {movie_id: movie_id, episode_id: stream_id, status: "start", log_id: log_id, video_length: player.duration()}, function (res) {
                                        if (res > 0)
                                        {
                                            log_id = res;
                                        }
                                        else {
                                            history.go(-1);
                                        }
                                    });
                                    started = 1;
                                    ended = 0;
                                    logged = 0
                                }
                                console.log("Video play Started");
                            });

                            player.on("ended", function () {
                                if (ended === 0 && (adavail === 0 || adavail === 2)) {
                                    $.post(url, {movie_id: movie_id, episode_id: stream_id, status: "complete", log_id: log_id, played_length: player.currentTime()}, function (res) {
                                        log_id = 0;
                                    });
                                    ended = 1;
                                    started = 0;
                                }
                            });

                            player.on('timeupdate', function () {
                                if (started === 1 && (adavail === 0 || adavail === 2)) {
                                    var curlength = Math.trunc(player.currentTime());
                                    var fulllength = player.duration();
                                    fulllength = Math.trunc((fulllength * percen) / 100);
                                    if (curlength === fulllength && fulllength > 1)
                                    {
                                        percen = parseInt(percen) + 5;
                                        //logged = 0;
                                        var buff_log_id = document.getElementById('buff_log_id').value;
                                        updateBuffered(player, curlength, buff_log_id);
                                        $.post(url, {movie_id: movie_id, episode_id: stream_id, status: "halfplay", log_id: log_id, played_length: player.currentTime()}, function (res) {
                                            log_id = res;
                                        });
                                    }
                                }
                            });

                            player.on('loadedmetadata', function () {
                                var duration = player.duration();
                                $('#full_video_duration').val(duration);
                                if (typeof player.getCurrentRes === "function") {
                                    var video_resolution = player.getCurrentRes();
                                } else {
                                    var video_resolution = 144;
                                }
                                $('#full_video_resolution').val(video_resolution);
                                buffered_loaded(player);
                            });
<?php } ?>
                    }
                });
            });
            function createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTimess, browserName,currentVideoResolution) {
                var currentTimesForVideo = currentTimess;
                $(".vjs-error-display").addClass("hide");
                $('.vjs-loading-spinner').attr('style','animation :spin 1.5s infinite linear');
                $('.vjs-loading-spinner').show();                
                if(typeof currentVideoResolution == 'undefined'){
                    if (typeof player.getCurrentRes === "function") {
                        var currentVideoResolution = player.getCurrentRes();
                    } else {
                        var currentVideoResolution = 144;
                    }
                }
                var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                $('#full_video_resolution').val(currentVideoResolution);
                $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data, browser_name: browserName}, function (res) {
                    if (is_mobile === 0) {
                        player.currentTime(currentTimesForVideo);
                    }
                    player.src(res).play();
                    $('#custVidRes li').each(function () {
                        if ($(this).attr('selected') == 'selected') {
                            var newRes = currentVideoResolution + 'p';
                            if (currentVideoResolution == 'BEST') {
                                var newRes = currentVideoResolution;
                            }
                            if ($(this).attr('class') == 'AUTO') {
                                $("#currentPlayerRes").html('AUTO ( ' + newRes + ' )');
                            } else {
                                $("#currentPlayerRes").html(newRes);
                            }
                        }
                    });
                    $(".vjs-error-display").removeClass("hide");
                    player.on("loadeddata", function () {
                        console.log(currentTimesForVideo);
                        player.currentTime(currentTimesForVideo);
                        $('.vjs-loading-spinner').removeAttr('style', 'animation :spin 1.5s infinite linear'); 
                    });
                });
            }
            function resize_player() {
                if (full_screen === false) {
                    full_screen = true;
                    var large_screen = setTimeout(function () {
                        if (full_screen === true) {
                        }
                    }, 5000);
                } else {
                    //clearTimeout(large_screen);
                    full_screen = false;
                }
            }
            // If Safari browser
            function checkBuffer(player) {
                var currentTime = player.currentTime();
                var playerDuration = player.duration();
                var r = player.buffered();
                var buffLen = r.length;
                buffLen = buffLen - 1;
                var bufferenEnddd = r.end(buffLen);
                var bufferenEndd = bufferenEnddd - 1;
                if (bufferenEndd < currentTime) {
                    if (playerDuration !== bufferenEnddd) {
                        player.pause();
                    } else {
                        player.play();
                    }
                } else {
                    if (bufferenEnddd !== currentTime) {
                        if (player.paused()) {
                            player.play();
                        }
                    }
                }
            }
        </script>
        <script src="<?php echo $this->siteurl; ?>/common/js/screentime.js"></script>
        <script type="text/javascript">
<?php
$action = Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
$action = explode('/', $action);
$page = $action[count($action) - 1];
if (strpos($action[count($action) - 1], '?') !== FALSE) {
    $page = $action[count($action) - 2];
}
$full_domain = 'www.' . $studio->domain;
if ($page == $studio->domain || $page == $full_domain) {
    $page = 'home';
}
?>
            $(document).ready(function () {
                var timeInMs = Date.now();
                var page = btoa(timeInMs + "<?php echo $page; ?>" + timeInMs);
                var url = "<?php echo Yii::app()->baseUrl; ?>/site/initiateLog?pid=" + page;
                $.post(url, {}, function (res) {
                });
            });

            var interval;
            function buffered_loaded(v) {
                if (!v.buffered) {
                    updateBuffered(v, '', -1);
                } else {
                    updateBuffered(-1, '', -1);
                    //interval = setInterval(updateBuffered, 60000);
                }
            }

            var buff_log_id = 0;
            var u_id = 0;
            var resolution = 144;

            function updateNewBuffered(player, previousBufferEnd, currTim) {
                var buff_log_id = document.getElementById('buff_log_id').value;
                updateBuffered(player, currTim, buff_log_id);
                var bufferenEnddd = previousBufferEnd;
                var bufferenStart = currTim;
                var resolution = player.getCurrentRes();

                $.post('<?php echo Yii::app()->baseUrl; ?>/report/addNewBufferLog', {movie_id: "<?php echo $movie_id ?>", video_id: "<?php echo isset($stream_id) ? $stream_id : 0 ?>", start_time: bufferenStart, end_time: bufferenEnddd, resolution: resolution}, function (res) {
                    var obj = JSON.parse(res);
                    buff_log_id = obj.id;
                    u_id = obj.u_id;
                    document.getElementById('u_id').value = u_id;
                    if (buff_log_id) {
                        document.getElementById('buff_log_id').value = buff_log_id;
                    }
                });
            }
            function updateBuffered(player, currTim, log_id) {
                var duration = parseInt(document.getElementById('full_video_duration').value);
                if ((typeof player == 'undefined') || player == -1) {
                    var v = videojs('video_block');
                    var r = v.buffered();
                } else {
                    var r = player.buffered();
                }
                if (r) {
                    var buffLen = r.length;
                    if (buffLen > 0) {
                        buffLen = buffLen - 1;
                    } else {
                        buffLen = 0;
                    }
                    var bufferenEnddd = r.end(buffLen);
                    if (buffLen < 0) {
                        var bufferenStart = 0;
                    } else {
                        var bufferenStart = r.start(buffLen);
                    }
                    var resolution = document.getElementById('full_video_resolution').value;

                    var buff_log_id = document.getElementById('buff_log_id').value;
                    var u_id = document.getElementById('u_id').value;
                    if (parseFloat(log_id) || parseFloat(buff_log_id)) {
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo Yii::app()->baseUrl; ?>/report/addBufferLog',
                            data: {movie_id: "<?php echo $movie_id ?>", video_id: "<?php echo isset($stream_id) ? $stream_id : 0 ?>", start_time: bufferenStart, end_time: bufferenEnddd, buff_log_id: buff_log_id, resolution: resolution, u_id: u_id},
                            async: false,
                            success: function (res) {
                                var obj = JSON.parse(res);
                                buff_log_id = obj.id;
                                u_id = obj.u_id;
                                document.getElementById('u_id').value = u_id;
                                if (buff_log_id) {
                                    document.getElementById('buff_log_id').value = buff_log_id;
                                }
                            }
                        });
                    }
                }
            }
            function checkInterNetSpeed(multipleVideoResolution, player) {
                if (ended === 0 && videoOnPause === 0 && (adavail === 0 || adavail === 2)) {
                    var defaultResolution = player.getCurrentRes();
                    var imageAddr = [], downloadSize = [], desc = []
                    imageAddr[0] = "<?php echo $internetSpeedImage; ?>";
                    downloadSize[0] = 1036053; //bytes
                    desc[0] = "Singapore S3 Bucket";
                    var startTime, endTime;
                    var changeUrlAccInternetSpeed = "<?php echo Yii::app()->baseUrl; ?>/user/getNewSignedUrlForInternetSpeedEmb";
                    for (var i = 0; i < desc.length; i++) {
                        console.log('start');
                        var download = new Image();
                        download.i = i;
                        download.onload = function () {
                            console.log('end');
                            endTime = (new Date()).getTime();
                            var duration = (endTime - startTime) / 1000;
                            var bitsLoaded = downloadSize[this.i] * 8;
                            var speedBps = (bitsLoaded / duration).toFixed(2);
                            var speedKbps = (speedBps / 1024).toFixed(2);
                            var speedMbps = (speedKbps / 1024).toFixed(2);
                            $.post(changeUrlAccInternetSpeed, {multiple_video_url: multipleVideoResolution, user_internet_speed: speedMbps}, function (res) {
                                if (defaultResolution !== res) {
                                    if (ended === 0 && videoOnPause === 0 && (adavail === 0 || adavail === 2)) {
                                        console.log('new');
                                        //var currTim = player.currentTime();
                                        //createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, '', currTim, browserName,res);
                                        player.changeRes(res);
                                    }
                                }
                                console.log(res);
                            });
                        }
                        download.onerror = function (err, msg) {
                            oProgress.innerHTML = "Invalid image, or error downloading";
                        }
                        startTime = (new Date()).getTime();
                        download.src = imageAddr[i] + "?nnn=" + startTime;
                    }
                }
            }
            function showSettingDiv() {
                if ($('#settingDiv:visible').length == 0) {
                    if ($('#video-res-sec:visible').length == 0) {
                        $("#settingDiv").show();
                    } else {
                        $("#video-res-sec").hide();
                    }
                } else {
                    $("#settingDiv").hide();
                }
            }
            function showResDiv() {
                $("#settingDiv").hide();
                if ($('#video-res-sec:visible').length == 0) {
                    $("#video-res-sec").show();
                } else {
                    $("#video-res-sec").hide();
                }
            }

            function videoChangeRes(videoRes, videoPlayer) {
                var showMultiReset = '';
<?php
$setResMenu12 = '';
foreach ($multipleVideo as $key => $val) {
    $addRes = '';
    if ($key != 'BEST') {
        $addRes = 'p';
    }
    ?>
                    showMultiReset += "<li class=\"<?php echo $key; ?>\" onclick=\"videoChangeRes('<?php echo $key; ?>', videoPlayer);\"><span class=\"textlftAlg\"><?php echo $key . $addRes; ?></span></li>";
    <?php
    $setResMenu12 .= "<li class=\"" . $key . "\" onclick=\"videoChangeRes('" . $key . "', videoPlayer);\"><span class=\"textlftAlg\">" . $key . $addRes . "</span></li>";
}
?>
                showMultiReset += "<li class=\"AUTO\" onclick=\"videoChangeRes('AUTO', videoPlayer);\"><span class=\"textlftAlg\">AUTO</span></li>";
                $("#video-res-sec").html('<ul id="custVidRes">' + showMultiReset + '</ul>');
                $("." + videoRes).append('<span class="textRgtAlg"><b>&#10004;</b></span>');
                $("." + videoRes).attr('selected', 'selected');
                if (videoRes === "AUTO") {
                    setInterSpeed = setInterval(function () {
                        checkInterNetSpeed(multipleVideoResolution, videoPlayer)
                    }, 120000);
                    return false;
                } else {
                    clearInterval(setInterSpeed);
                    videoPlayer.changeRes(videoRes);
                }
                $("#settingDiv").hide();
                $("#video-res-sec").hide();
            }
        </script>          
        <style>
            .ytp-svg-shadow {
                stroke: #000;
                stroke-opacity: .15;
                stroke-width: 2px;
                fill: none;
            }
            .ytp-svg-fill {
                fill: #ccc;
            }
            .customized-res{
                display:none; 
                z-index: 2000;
                right:10%;
                padding:0px 5px 0px 5px;
                cursor:pointer; 
                position:absolute;
                visibility: visible;
                opacity: 0.1%;
                line-height: 23px;
                -webkit-transition: visibility .1s,opacity .1s;
                -moz-transition: visibility .1s,opacity .1s;
                -o-transition: visibility .1s,opacity .1s;
                transition: visibility .1s,opacity .1s;
                background-color: #07141e;
                background-color: rgba(7,20,30,.7);
                border-radius: 3px;
            }
            #video-res-sec ul{
                list-style: none;
                margin: 0px;
                padding: 0px;
            }
            #video-res-sec ul li{
                list-style: none;
                margin: 0px;
                padding: 0px;
            }
            .textlftAlg{
                text-align:left;
            }
            .textRgtAlg{
                text-align:right; float:right;
            }
            .vjs-hidden{display: none !important;}
        </style>            
    </body>
</html>
