<?php
    /*
     * ### Third Party URL Link / Embed links video view page
     * ### @avi 7-11-2016 <aravind@muvi.com>
     * ### @Youtube / vume 
     */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title; ?> | <?php echo $this->studio->name; ?></title>
        <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>
            var user_id="<?php echo Yii::app()->user->id; ?>";
            var STORE_AUTH_TOKEN="<?php echo $authToken; ?>";  
            var HTTP_ROOT = "<?php echo Yii::app()->getbaseUrl(true); ?>";
            var notification = 0;
            <?php if ($this->notification){ ?>
                notification = "<?php echo $this->notification; ?>";
            <?php } ?>
            var favicon = "<?php echo $this->favIcon; ?>";
        </script>
         <script src="<?php echo Yii::app()->getbaseUrl(); ?>/js/themes/notification.js"></script>
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>
        <style type="text/css">
            iframe {
                width: 100%   !important;
                height:  100% !important;
                position: absolute;
                top: 0;
                left: 0;
            }
            .classthirdpartydiv{
                width: 100%    !important;
                height:  100% !important;
                position: absolute;
                top: 0;
                left: 0;
                border:0px solid red;
            }
            #backButton {
                position:absolute;
                left: 40px !important;
                top: 40px !important;
                display: none;
                z-index: 1; 
            }
            @media screen and (max-width: 480px) {
                #backButton{
                position:absolute;
                left: 30px !important;
                top: 30px !important;
                width: 30px !important;
                height: 30px !important;
                display: none;
                z-index: 1; 
                }
            }
        </style>
    </head>
</head>
    <body>
        <input type="hidden" id="backbtnlink" value="<?php echo @Yii::app()->session['backbtnlink'];?>" />
        <div class="classthirdpartydiv">
            <?php echo $thirdparty_url; ?>
        </div>
        <script>        
        $(document).ready(function () {           
           $('.classthirdpartydiv').append('<img src="/images/back-button.png" width="40" height="40" id="backButton" style="cursor:pointer" data-toggle="tooltip" title="Back to Browse" allowFullScreen = true/>'); 
           $('.classthirdpartydiv').mouseover(function() {
                $('#backButton').show();
            });
            $('.classthirdpartydiv').mouseout(function() {
                $('#backButton').hide();
            });
            $('#backButton').click(function(){
                if($('#backbtnlink').val() !='')
                   window.location.href = $('#backbtnlink').val();
                else
                  parent.history.back();
                  return false;
            });
            $('#backButton').bind('touchstart', function(){
                if($('#backbtnlink').val() !='')
                    window.location.href = $('#backbtnlink').val();
                else
                   parent.history.back();
                return false;   
            });
        });
        </script>
        <?php $this->renderPartial('//layouts/restrict_streaming_device', array('stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction)); ?>
    </body>
</html>
















