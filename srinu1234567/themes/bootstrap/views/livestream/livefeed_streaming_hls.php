<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />        
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>  
        <script>
            var user_id="<?php echo Yii::app()->user->id; ?>";
            var STORE_AUTH_TOKEN="<?php echo $authToken; ?>";  
            var HTTP_ROOT = "<?php echo Yii::app()->getbaseUrl(true); ?>";
            var notification = 0;
            <?php if ($this->notification){ ?>
                notification = "<?php echo $this->notification; ?>";
            <?php } ?>
            var favicon = "<?php echo $this->favIcon; ?>";
        </script>
    
         <script src="<?php echo Yii::app()->getbaseUrl(); ?>/js/themes/notification.js"></script>
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>		
        <script type='text/javascript'>
        // video js error message
            var you_aborted_the_media_playback ="<?php echo $this->Language["you_aborted_the_media_playback"]; ?>";
            var a_network_error_caused_the ="<?php echo $this->Language["a_network_error_caused_the"]; ?>";
            var the_media_playback_was_aborted ="<?php echo $this->Language["the_media_playback_was_aborted"]; ?>";
            var the_media_could_not_be_loaded ="<?php echo $this->Language["the_media_could_not_be_loaded"]; ?>";
            var the_media_is_encrypted ="<?php echo $this->Language["the_media_is_encrypted"]; ?>";
            var no_compatible_source_was_found ="<?php echo $this->Language["no_compatible_source_was_found"];?>";

        </script>
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />        

        <link rel="stylesheet" href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video-js.min.css">
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video.min.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/hls.js/dist/hls.min.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/src/videojs5.hlsjs.js"></script>
        <style type="text/css">
            /* .video-js {height: 50%; padding-top: 48%; width: 100%;}*/
            .vjs-fullscreen {padding-top: 0px}  
            .RDVideoHelper{display: none;}       
            video::-webkit-media-controls{display:none!important}
            video::-webkit-media-controls-panel{display:none!important} 
            body {
                background-color: #1E1E1E;;
                color: #fff;
                font: 12px Roboto,Arial,sans-serif;
                height: 100%;
                margin: 0;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 100%;
            }
            html {
                overflow: hidden;
            }
            .video-js .vjs-control {
                float:left;
            }
            .video-js .vjs-control.vjs-fullscreen-control{
                float:right;
            }    
            /*                        .vjs-big-play-button{display: block !important;visibility: hidden;};*/
        </style>
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/js/backbutton.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/css/backbutton.css" />
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js?v=<?php echo RELEASE ?>"></script>
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo RELEASE ?>" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <input type="hidden" id="backbtnlink" value="<?php echo @ Yii::app()->session['backbtnlink']; ?>" />
        <?php
        $posterURL = 'https://video-js.zencoder.com/oceans-clip.png';
        if (isset($livestream->poster_url) && $livestream->poster_url) {
            $posterURL = $livestream->poster_url;
        }
        ?>
        <div id="videoDivContent">
            <div class="videocontent">
                <div class="playercontent" id="stream_player_1" style="position: relative;">
                    <video data-title="Live stream" id="stream_player" class="video-js vjs-default-skin vjs-big-play-centered vjs-16-9" controls preload="auto">
                        <source src="<?php echo @$livestream->feed_url; ?>" type="application/x-mpegURL"  data-title="Live stream"/>
                    </video>
                        <!--<div class="player-text-div" style="color: #fff;position: absolute;"><?php //echo @$livestream->player_text; ?></div>-->
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
            var is_mobile = "";
            var player = "";
            var videoOnPause = 0;
            var videoSource = '';
            $(function () {
                var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
                var playerSetup = videojs('stream_player');
                player = playerSetup;
                playerSetup.ready(function () {
//                    
//                    if (is_mobile !== 0){ 
//                        player.play();
//                    }
                    player = this;
                    if (is_mobile !== 0) {
                        $(".vjs-big-play-button").show();
                        player.pause();
                    } else {
                        $(".vjs-big-play-button").hide();
                    }
                    player.play();
                    $(".vjs-control-bar").show();
                    $('#stream_player').bind('contextmenu', function () {
                        return false;
                    });
                    $(".vjs-fullscreen-control").click(function () {
                        $(this).blur();
                    });
                    player.on("loadstart", function () {
                        if (is_mobile !== 0) {
                            $(".vjs-big-play-button").on("touchstart", function () {
                                player.play();
                                $(".vjs-big-play-button").hide();
                                $('.vjs-loading-spinner').show();
                            });
                        }
                    });
                    player.on("play", function () {
                        if (is_mobile !== 0) {
                            $(".vjs-control-bar").show();
                            $(".vjs-big-play-button").hide();
                            $('.vjs-loading-spinner').hide();
                        }
                        videoOnPause = 0;
                    });
                    player.on("pause", function () {
                        videoOnPause = 1;
                    });

                    if ($(document).height() != null) {
                        var thisIframesHeight = $(window).height();//$(document).height();                           
                        $(".video-js").attr('style', 'padding-top:' + thisIframesHeight + 'px');
                    } else {
                        $(".video-js").attr('style', 'padding-top:47%; height:50%;');
                    }
                    $(window).resize(function () {
                        var thisIframesHeight = $(window).height();
                        $(".video-js").attr('style', 'padding-top:' + thisIframesHeight + 'px;width:100%;');
                    });
                    player.on('error', function () {
                        var errordetails = this.player().error()
                        if(errordetails.code !='' && videoSource == ''){
                            $(".vjs-modal-dialog-content").html("<div>Live stream has been disconnected</div>");
                        }else if($('.vjs-modal-dialog-content').html()!==''){
                          $(".vjs-modal-dialog-content").html("<div>"+no_compatible_source_was_found+"</div>");
                        } else {
                        if(document.getElementsByTagName("video")[0].error != null){
                            var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                            if (videoErrorCode === 2) {
                                $(".vjs-error-display").html("<div>"+a_network_error_caused_the+"</div>");
                            } else if (videoErrorCode === 3) {
                                $(".vjs-error-display").html("<div>"+the_media_playback_was_aborted+"</div>");
                            } else if (videoErrorCode === 1) {
                                 $(".vjs-error-display").html("<div>"+you_aborted_the_media_playback+"</div>");
                            } else if (videoErrorCode === 4) {
                                  $(".vjs-error-display").html("<div>"+the_media_could_not_be_loaded+"</div>");
                            } else if (videoErrorCode === 5) {
                                $(".vjs-error-display").html("<div>"+the_media_is_encrypted+"</div>");
                            }
                        }
                        }
                });
                <?php if($livestream->feed_type == 2) { ?>
                    checkLiveStreamOffline();
                    setInterval(function(){
                        checkLiveStreamOffline();
                    }, 10000);
                <?php } ?>
            });
        });
            
        function checkLiveStreamOffline(){
            <?php if(@$feedWithIp){ ?>
                var checkStreamurl = '<?php echo @$livestream->feed_url; ?>';
                $.ajax({
                    type: 'HEAD',
                    url: '<?php echo @$feedWithIp; ?>',
                    success: function(){
                        if ($('#checkLiveStream').length){
                            $('#checkLiveStream').remove();
                            if(videoOnPause){
                                player.src(checkStreamurl);
                                player.pause();
                            } else{
                                player.src(checkStreamurl).play();
                            }
                        }
                        videoSource = checkStreamurl;
                    },
                    error: function() {
                        if (!$('#checkLiveStream').length){
                            $("#stream_player").append("<span id='checkLiveStream'><img src='/images/streamoffline.png' width='20' height='20' title='stream offline'/>&nbsp;&nbsp;Live stream offline</span>");
                                player.src("");
                        }
                        videoSource = "";
                    }
                });
            <?php } ?>
        }
    </script>
    <?php $this->renderPartial('//layouts/restrict_streaming_device', array('stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction)); ?>
    <?php $this->renderPartial('//layouts/video_view_log', array('stream_id' => $stream_id, 'movie_id' => $movie_id, 'is_live' => 1)); ?>

</html>