<link href="https://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/live/video.js"></script>
<script>
	
	
	var isMobile = false; //initiate as false
	// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
   
	var techOrderArr = ["html5", "flash"]; // Default for everyone
	if(($.browser.chrome || $.browser.mozilla) && !isMobile ) {
		 techOrderArr = ["flash","html5"];
	} 
	videojs.options.flash.swf = "<?php echo Yii::app()->theme->baseUrl; ?>/js/live/video-js.swf";
	
</script>
<!--[if IE]><script>
    techOrderArr = ["flash","html5"]; // IE specific
<![endif]-->
<style type="text/css">
	.ls-form-heading{
		font-size: 22px;
		color: #000;
		line-height: 30px;
		margin: 10px 0px;
	}
	.ls-first-radio{
		padding: 0px;
	}
	.ls-form{
		line-height: 40px;
	}
	.ls-btn-blue{background: #42b7da none repeat scroll 0 0;
		border-radius: 4px;
		color: #fff;
		font-size: 20px;
		font-weight: 400;
		padding: 10px 20px;
		text-align: center;
		text-shadow: none;
		margin-bottom: 8px;
	}
	.ls-radio{
		margin:-3px 0px 0px !important;
	}
	.playercontent{
		height: 500px;
		border: 5px solid #ccc;
		margin-bottom: 10px;
		width: 100%;
	}
.playercontent video{margin: 5px;}
.info-text{padding-bottom: 20px;line-height: 30px;font-size: 17px;}
.feed-lable{font-size: 20px;width: 10%;}
.feed-input{width:70%;text-transform: none !important;}
.feed-btn{width:10%;margin: 0 !important;}
button.btn, input.btn[type="submit"]{padding-left: 0px;padding-right:0px;}
@media (max-width: 768px) {
	.feed-lable{font-size: 14px;}
}
@media (max-width: 767px) {
	.feed-lable{font-size: 14px;width:auto;}
	.feed-input{width:auto;}
	.feed-btn{width:auto;}
}
.flash-enable{
	color: #fff;
    font-weight: 500;
    margin: auto;
    position: absolute;
    text-align: center;
    top: 32%;
    vertical-align: middle;
    width: 100%;
    z-index: 999999;
}

</style>
<div class="container" style="min-height: 500px;">
	<div class="span12">
		<div class="btm-bdr ls-head">End-to-end Solution for Live Streaming</div>
	</div>
	<div class="span12">
		<div class="info-text">
			Muvi supports live streaming for HLS and RTMP. You can live stream on multiple devices-mobile, web and TV. Muvi takes care of everything including white-labeled website and mobile app, hosting, billing, analytics and support. See a live demo of live streaming below.
		</div>
		<div>
			<form  role="form" class="ls-form form-inline" action="javascript:void(0);" method="post" name="livestreamForm" id="livestreamForm" onSubmit="liveStream();">
				<div class="form-group col-sm-12">
					<label for="hls_url" class="feed-lable">HLS Feed:</label>
					<input type="text" class="form-control feed-input" id="hls_url"  placeholder="Please enter a valid HLS Feed...." value="" required="true" pattern="https?://.+" >
					<button type="submit" class="btn btn-default ls-btn-blue feed-btn" >Test It!</button>
				</div>
			</form>
		</div>	
		<div class="playercontent" id="stream_player" style="position: relative;">
			<?php $this->renderPartial('hls_stream');?>
		</div>
	</div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
	console.log(techOrderArr);
	 _V_("example_video_1",{
        techOrder: techOrderArr
    });
//	$(window).load(function(){
//		var hasFlash = false;
//		try {
//			hasFlash = Boolean(new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
//		} catch(exception) {
//			hasFlash = ('undefined' != typeof navigator.mimeTypes['application/x-shockwave-flash']);
//		}
//		if(!hasFlash){
//			//$('#flash-msg-div').show();
//			//alert('please enable flash plugin for best view');
//		}
//	});
	
	function getLivestream(stream_type){
		var url = "<?php echo Yii::app()->getBaseUrl(TRUE).'/livestream/streamType';?>";
		$.post(url,{'stream_type':stream_type},function(res){
			$('#stream_player').html(res);
		});
	}
</script>
