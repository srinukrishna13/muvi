<link href="https://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/live/video.js"></script>
<script>
	videojs.options.flash.swf = "<?php echo Yii::app()->theme->baseUrl; ?>/js/live/video-js.swf";
</script>
<style type="text/css">
	.ls-form-heading{
		font-size: 22px;
		color: #000;
		line-height: 30px;
		margin: 10px 0px;
	}
	.ls-first-radio{
		padding: 0px;
	}
	.ls-form{
		line-height: 40px;
	}
	.ls-btn-blue{background: #42b7da none repeat scroll 0 0;
		border-radius: 4px;
		color: #fff;
		font-size: 20px;
		font-weight: 400;
		padding: 10px 20px;
		text-align: center;
		text-shadow: none;
		margin-bottom: 8px;
	}
	.ls-radio{
		margin:-3px 0px 0px !important;
	}
	.playercontent{
		height: 500px;
		border: 5px solid #ccc;
		margin-bottom: 10px;
		width: 91%;
	}
	.playercontent video{margin: 5px;}
</style>
<div class="container" style="min-height: 500px;">
	<div class="span12">
		<div class="btm-bdr ls-head">Live Stream from Feed or Camera</div>
	</div>
	<div class="span12">
		<form  role="form" class="ls-form" action="javascript:void(0);" method="post" name="livestreamForm" id="livestreamForm">
			<div class="col-sm-10">
				<div class="ls-form-heading">Select your feed type</div>
			</div>
			<div class="form-group col-sm-10" >
				<label class="radio-inline ls-first-radio"><input type="radio" class="ls-radio" name="stream_type" id="stream_type1" value="hls" checked="checked" >&nbsp;&nbsp;HLS&nbsp;&nbsp;</label>
				<label class="radio-inline"><input type="radio" class="ls-radio" name="stream_type" id="stream_type2" value="rtmp" onchange="getLivestream('rtmp');">&nbsp;&nbsp;RTMP&nbsp;&nbsp;</label>
				<label class="radio-inline"><input type="radio" class="ls-radio" name="stream_type" id="stream_type3" value="webcame" readonly="readonly" disabled="true" >&nbsp;&nbsp;WEBCAM&nbsp;&nbsp;</label><br/>
			</div>
			<div class="form-group col-sm-10">
				<input type="text" class="form-control" id="hls_url"  style="width: 80%;text-transform: none;" placeholder="http://wms.shared.streamshow.it/telesanremo/telesanremo/playlist.m3u8" required pattern="https?://.+" >
				<button type="button" class="btn btn-default ls-btn-blue" onclick="liveStream();">Test It!</button>
			</div>
		</form>
		<div class="playercontent col-sm-10" id="stream_player" >
			<?php $this->renderPartial('hls_stream');?>
		</div>
	</div>
	<div class="clear"></div>
</div>

<script type="text/javascript">
	function getLivestream(stream_type){
		var url = "<?php echo Yii::app()->getBaseUrl(TRUE).'/livestream/streamType';?>";
		$.post(url,{'stream_type':stream_type},function(res){
			$('#stream_player').html(res);
		});
	}
</script>
