<?php $v = RELEASE;?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video-js.css" rel="stylesheet" type="text/css">
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/video.js"></script>       
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/videojs-contrib-hls.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
    <style type="text/css">
    /*.video-js {height: 50%; padding-top: 48%; width: 100%;}*/
    .vjs-fullscreen {padding-top: 0px}  
    .RDVideoHelper{display: none;}
    video::-webkit-media-controls{display:none!important}
    video::-webkit-media-controls-panel{display:none!important}
    body {
        background-color: #1E1E1E;;
        color: #fff;
        font: 12px Roboto,Arial,sans-serif;
        height: 100%;
        margin: 0;
        overflow: hidden;
        padding: 0;
        position: absolute;
        width: 100%;
    }
    html {
        overflow: hidden;
    }        
    .movie-name{position: absolute;top:0;left: 0;right: 0;}
    .movie-name a{color: #fff;}
    .vjs-fluid{
        width: 100%;
        height: 0;
        padding-top: 56.25%;
    }
    .temp-bg{
        position: absolute;
        z-index: 90;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #000;
    }
    .vjs-big-play-button{display: none !important;visibility: hidden;};
    #video_block .vjs-icon-fullscreen-enter:before, 
    #video_block .video-js .vjs-fullscreen-control:before {
    content: "\f108"; }

    #video_block._fullscreen .vjs-icon-fullscreen-exit:before, 
    #video_block._fullscreen .vjs-fullscreen-control:before {
    content: "\f109"; }    
    </style>   
</head>
<body>
    <center id="videoDivContent">
        <div class="demo-video video-js-responsive-container vjs-hd"  style="position: relative;">
            <div class="videocontent" style="overflow:hidden;">                
                <video id="video_block" class="video-js vjs-default-skin vjs-big-play-centered vjs-16-9" autobuffer webkit-playsinline preload="auto" autoplay ="true" controls="true" fluid = 'true'>
                <source src="<?php echo @$livestream->feed_url;?>"  type="application/x-mpegURL" data-title="Live stream">
                </video>                
            </div>            
        </div>
    </center>
<script>           
    var  full_screen = false, show_control = true;    
    var is_mobile =  "";
    var player = "";            
    $(function(){       
        var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
        var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";        
        var playerSetup = videojs('video_block');  
         playerSetup.ready(function () {
             player=this;   
            $(".vjs-control-bar").show();
                $('#video_block').bind('contextmenu', function () {
                    return false;
                });         
                $(".vjs-fullscreen-control").click(function () {                    
                    $(this).blur();
                });
                player.on("play", function () {
                     //$('.play-btn').remove();
                     if (is_mobile !== 0) {
                         $(".vjs-control-bar").show();
                     }
                 });           
                if($(document).height() != null){                            
                       var thisIframesHeight = $(window).height();//$(document).height();                           
                       $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
                   } else{
                       $( ".video-js" ).attr('style','padding-top:47%; height:50%;');
                 }                                   
            });                                                                                
    }); 
</script> 
</body>
</html>