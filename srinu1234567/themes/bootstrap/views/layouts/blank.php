<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/images/icon.png" type="image/png" rel="icon" />

        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300italic,300,400italic,500italic,500,700,700italic,900,900italic' rel='stylesheet' type='text/css' />

        <?php Yii::app()->bootstrap->register(); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />    

    </head>
    <?php $class = Yii::app()->controller->getId(); ?>
    <body class="<?php echo $class; ?>">

        <?php echo $content; ?>

    </body>
</html>
