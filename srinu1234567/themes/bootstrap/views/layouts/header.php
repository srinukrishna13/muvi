    <?php
    $is_home = 0;
    $class = '';
    if ((Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId()) == 'site/index'  )
    {
        $is_home = 1;
        $class = Yii::app()->controller->getId();
    }
    else
    {
        $class = Yii::app()->controller->getId();
    }    
    
    ?>

<div id="top_bar" class="<?php echo $class?>">
    
    <div class="navbar">
        <div class="container">
            <div class="navbar-inner">
                <div class="container">
                    <a data-target="#yii_bootstrap_collapse_0" data-toggle="collapse" class="btn btn-navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" title="Muvi" href="<?php echo Yii::app()->getbaseUrl(true); ?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="Muvi" title="Muvi" /></a>
                    <div id="yii_bootstrap_collapse_0" class="nav-collapse">
                        <ul class="nav" id="yw19" style="padding-top:5px;">
                            <!--<li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/sdk" class="<?php echo ($class == 'sdk')?'current':''?>">SDK<br /><span>Branded Website/App</span></a></li>
                            <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/muvi" class="<?php echo ($class == 'muvi')?'current':''?>">MUVI.COM<br /><span>Individual Content</span></a></li>
                            <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/dam" class="<?php echo ($class == 'dam')?'current':''?>">DAM<br /><span>Asset Management</span></a></li>    -->
                                <li><a href="<?php echo HOST_URL; ?>#features">Features</a></li>
                                <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/examples">Examples</a></li>
                                <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/tour">Tour</a></li>
                                <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/pricing">Pricing</a></li>
                                <li class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">More <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/howitworks">How it works</a></li>
                                        <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/technology">Technology</a></li>
                                        <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/ads">Advertising</a></li>
										<li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/live-streaming">Live Streaming</a></li>
                                        <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/api">API</a></li>
                                        <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/faqs">FAQs</a></li>
                                        <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/muvi">Marketplace</a></li>
                                        <li class="separator"><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/about">About Us</a></li>
                                    </ul>
                                </li>                        
                        </ul>

                        <ul id="yw20" class="pull-left" style="margin-left:80px;"><li class="nav-divider"></li></ul>

                        <ul id="yw21" class="pull-right <?php echo (Yii::app()->user->id > 0)?'pull':''?>">
                            <?php if(Yii::app()->user->isGuest) {?>
                            <li><a data-target="#LoginModal" data-toggle="modal" href="javascript:void(0)" tabindex="-1">Login</a></li>
							<li><a class="btn btn-primary btn-white"  href="<?php echo Yii::app()->getbaseUrl(true); ?>/signup?purchasetype=freetrial" tabindex="-1" type="freetrial" >Free Trial</a></li>
							<li><a class="btn btn-primary btn-white"  href="<?php echo Yii::app()->getbaseUrl(true); ?>/signup?purchasetype=buynow" tabindex="-1" type="buynow">Buy Now</a></li>
                            <?php }else{?>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#"> Account <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/admin/managecontent">Back to Admin</a></li>
                                    <li><a href="<?php echo Yii::app()->getbaseUrl(true); ?>/user/logout" tabindex="-1">Logout</a></li>
                                </ul>
                            </li>                             
                            <li><a class="btn btn-primary btn-white" href="<?php echo Yii::app()->getbaseUrl(true); ?>/contact" tabindex="-1">Contact Us</a></li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
    $show_vid = 0;
        if($class != 'contact' && $class != 'page' && $class != 'blogs' && $class != 'muvi' && $class != 'pricing'  && $class != 'login' && $class != 'ads' && $class !='livestream' && $class !='apidocs'){?>
    <div class="container">
        <div class="hero-unit">
            <?php 
            if($class == 'sdk'){?>
            <h1>Launch multi-screen VOD platform, instantly!</h1>
            <h2>End-to-end solution includes web and mobile app, video server and hosting. Fully managed.</h2>           
            <?php }
            else if($class == 'dam'){?>
            <h1>Store. Convert. Distribute</h1>
            <h2>Cloud-based solution with ZERO CapEx</h2>            
            <?php } 
            else if($class == 'affiliate'){?>
            <h1>Partner with a growing marketplace</h1>
            <h2>Acquire subscribers from new markets</h2>            
            <?php }             
            else{?>
            <h1>Launch multi-screen VOD platform, instantly!</h1>
            <h2>End-to-end solution includes web and mobile app, video server and hosting. Fully managed.</h2>   
            <?php } 
            if(Yii::app()->user->isGuest) {
                $show_vid = 1;
                $config = Yii::app()->common->getConfigValue(array('trial_period'), 'value');
           ?>
            <div class="row-fluid martop20">
                <div class="span3"></div>
                <div class="span3">
                    <a class="btn btn-white btn-free-trial btn-blue-trial" href="<?php echo Yii::app()->getBaseUrl(true);?>/signup">Start Free Trial <i class="fa fa-rocket"></i></a>
                    <h5><em>No credit card required</em></h5>
                </div>    
                <div class="span3 marleft0">
                    <a class="btn btn-white btn-free-trial btn-how-works" href="#" role="button" id="how-work-btn">How It Works <i class="fa fa-play-circle"></i></a>
                </div>
                <div class="span3"></div>
            </div> 
            <?php }?>
        </div>
    </div>
        <?php }?>
</div>

<?php
if($show_vid == 1)
{
    $v_logo = Yii::app()->getBaseUrl(true).'/images/logo-light-blue.png';
    $video_path = 'http://d1yjifjuhwl7lc.cloudfront.net/public/muviStudioVideo.mp4';
?>
<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video.js"></script>
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css" rel="stylesheet" type="text/css" />
<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js"></script>
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo RELEASE ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/fancybox.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/css/fancybox.css" type="text/css" media="screen" />
<style type="text/css">
.video-js {height: 50%; padding-top: 36%;}
.videocontent{display: none;}
video::-webkit-media-controls {
    display:none !important;
}
</style>
<div id="howItWorks"> 
     <div class="wrapper">
         <div class="videocontent">
            <video id="video_block" class="video-js vjs-default-skin" controls width="100%">
                <source src="" type="video/mp4" />
            </video>
        </div> 
    </div>
</div>
<script type="text/javascript"> 
$("#how-work-btn").fancybox({
    'closeBtn': true,
    'scrolling': false,
    'titleShow': false,
    'padding': 0,
    'content': $('#howItWorks').show(),
    'href' : $(this).attr('href'),
    closeClick: false,
    helpers: { overlay: { closeClick: false } }, // prevents closing when clicking OUTSIDE fancybox
    afterShow: function() {
        var is_mobile = "<?php echo Yii::app()->common->isMobile(); ?>";
        if(is_mobile != 1)
        {
            $(".fancybox-close").hide();
            $(".fancybox-wrap").hover(function() {
                setTimeout(function() {
                    $(".fancybox-close").fadeIn();
                }, 200);    
            }, function() {
                setTimeout(function() {
                    $(".fancybox-close").fadeOut();
                }, 200);                       
            });       
        }
    },
    'beforeShow': function () {
        var main_video = videojs('video_block');
        main_video.src('<?php echo $video_path;?>').play();
        main_video.pause();
        main_video.play();
        var $video = $('<a />', {
            href: '#',
            id: 'trailer',
            style: 'display:block;height:auto;width:740px;'
        });
        $('.videocontent').show();
        var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = true;
        $(".vjs-error-display").attr("style","display:none"); 
        $('#howItWorks').append($video);        
        main_video.watermark({
            file: "<?php echo $v_logo; ?>",
            xrepeat: 0,
            opacity: 0.75
        });
        $(".vjs-watermark").attr("style", "bottom:46px;right:1%;width:20%;");
        $(".vjs-tech").mousemove(function () {
            if (full_screen == true && show_control == false) {
                $("#video_block .vjs-control-bar").show();
                show_control = true;
                var timeout = setTimeout(function () {
                    if (full_screen == true) {

                    }
                }, 5000);
            } else {
                clearTimeout(timeout);
            }
        });
    },
    'afterClose': function () {
        var main_video = videojs('video_block');
        $("#howItWorks").hide();
        main_video.pause();
    },   
});    
</script>    
<?php
    if(Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId() ==  'site/introvideo')
    {
    ?>
    <script type="text/javascript"> 
        $(document).ready(function(){
            $("#how-work-btn").trigger('click');
        });
    </script>
    <?php
    }
}
?>