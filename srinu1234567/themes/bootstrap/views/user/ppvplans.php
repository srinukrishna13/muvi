<!-- PPV Plan Modal -->
<?php
if(isset($gateways[0]) && !empty($gateways[0])){
    $can_save_card = (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) ? $gateways[0]['paymentgt']['can_save_card'] : 0;
    
    if (isset($plan) && !empty($plan)) {
        $amount = '';
        $symbol = (isset($currency->symbol) && trim($currency->symbol)) ? $currency->symbol: $currency->code.' ';

        if (isset($data['content_types_id']) && intval($data['content_types_id']) != 3) {
            if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                $amount =  $price['price_for_subscribed']; 
            } else {
                $amount =  $price['price_for_unsubscribed']; 
            }
        }
                
?>
    <input type="hidden" value="<?php echo (isset($data['is_show']) && intval($data['is_show'])) ? 1 : 0; ?>" id = "is_show" />
    <input type="hidden" value="<?php echo (isset($data['is_season']) && intval($data['is_season'])) ? 1 : 0; ?>" id = "is_season" />
    <input type="hidden" value="<?php echo (isset($data['is_episode']) && intval($data['is_episode'])) ? 1 : 0; ?>" id = "is_episode" />
    <input type="hidden" value="<?php echo (isset($plan->content_types_id) && intval($plan->content_types_id)) ? $plan->content_types_id : 0; ?>" id = "content_types_id" />
<input type="hidden" value="1" id = "is_ppv" />
    <div id="price_detail" style="position: relative;">
        <div class="col-md-12">
            <span class="error" id="plan_error"></span>
            <?php if (isset($data['msg']) && trim($data['msg'])) { ?>
            <span class="error"><?php echo $data['msg'];?></span>
            <?php } else { ?>

             <form id="ppv_plans_form" name="ppv_plans_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false">

                 <!-- For multipart content when bundle is not enabled-->
                 <?php if (isset($data['content_types_id']) && intval($data['content_types_id']) == 3) { ?>
                     <?php if (isset($data['min_season']) && intval($data['min_season'])) { ?>

                     <?php 
                         $is_show_price_not_zero = $is_default_season_price_not_zero = $is_season_price_not_zero = $is_episode_price_not_zero = 1;
                         $ppv_season_status = $season_price = $default_season_price = 0;
                         
                         if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                             if (abs($price['show_subscribed']) < 0.00001) {
                                 $is_show_price_not_zero = 0;
                             }
                         } else {
                             if (abs($price['show_unsubscribed']) < 0.00001) {
                                 $is_show_price_not_zero = 0;
                             }
                         }

                         if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                            $default_season_price = $season_price = $price['season_subscribed'];
                             if (abs($price['season_subscribed']) < 0.00001) {
                                 $is_season_price_not_zero = 0;
                             }
                            
                             if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
                                $ppv_season_status = 1;
                                if (isset($price['default_season_price'])) {
                                    $season_price = @$price['default_season_price'];
                                    if (isset($price['default_season_price']) && abs($price['default_season_price']) < 0.00001) {
                                        $is_default_season_price_not_zero = 0;
                                    }
                                }
                             }
                         } else {
                            $default_season_price = $season_price = $price['season_unsubscribed'];
                             if (abs($price['season_unsubscribed']) < 0.00001) {
                                 $is_season_price_not_zero = 0;
                             }

                            if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
                                $ppv_season_status = 1;
                                if (isset($price['default_season_price'])) {
                                    $season_price = @$price['default_season_price'];
                                    if (isset($price['default_season_price']) && abs($price['default_season_price']) < 0.00001) {
                                        $is_default_season_price_not_zero = 0;
                         }
                                }
                            }
                         }

                         if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                             if (abs($price['episode_subscribed']) < 0.00001) {
                                 $is_episode_price_not_zero = 0;
                             }
                         } else {
                             if (abs($price['episode_unsubscribed']) < 0.00001) {
                                 $is_episode_price_not_zero = 0;
                             }
                         }

                         $charged_amt = 0;
                         $is_show_checked = $is_season_checked = $is_episode_checked = 0;

                         if (isset($data['is_show']) && intval($data['is_show']) && intval($is_show_price_not_zero)) {
                             $is_show_checked =1;
                             $charged_amt = (isset($data['member_subscribed']) && trim($data['member_subscribed'])) ? $price['show_subscribed']: $price['show_unsubscribed']; 
                         } else if (isset($data['is_season']) && intval($data['is_season']) && (intval($is_season_price_not_zero) || intval($is_default_season_price_not_zero))) {
                             $is_season_checked = 1;
                             $charged_amt = $season_price;
                         } else if (isset($data['is_episode']) && intval($data['is_episode']) && intval($is_episode_price_not_zero)) {
                             $is_episode_checked = 1;
                             $charged_amt = (isset($data['member_subscribed']) && trim($data['member_subscribed'])) ? $price['episode_subscribed']: $price['episode_unsubscribed']; 
                         }
                     ?>

                     <?php if (isset($data['is_show']) && intval($data['is_show']) && intval($is_show_price_not_zero)) { ?>
                     <div class="form-group">
                        <div class="col-md-10">
                           <label style="font-weight: normal">
                               <input type="radio" name="data[plan]" <?php if (intval($is_show_checked)) { ?>checked="checked" <?php } ?> value="show" id="showtext" data-price="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['show_subscribed']; } else { echo $price['show_unsubscribed']; }?>" onclick="setPriceForMulitPart(this);" />
                           <?php echo $this->Language['entire_show'].":"; ?>
                           </label>
                        </div>
                        <div class="col-md-2">
                           <?php echo $symbol; ?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['show_subscribed']; } else { echo $price['show_unsubscribed']; }?>
                        </div>
                     </div>
                     <?php } ?>

                     <?php if (isset($data['is_season']) && intval($data['is_season']) && intval($data['min_season']) && (intval($is_season_price_not_zero) || intval($is_default_season_price_not_zero))) { ?>
                     <div class="form-group">
                        <div class="col-md-3">
                           <label style="font-weight: normal">
                           <input type="radio" name="data[plan]" <?php if (intval($is_season_checked)) { ?>checked="checked" <?php } ?> value="season" id="seasontext" data-price="<?php echo number_format((float) $season_price, 2, '.', '');?>" onclick="setPriceForMulitPart(this);"/>
                           <?php echo $this->Language['season'].":"; ?>
                           </label>
                        </div>
                        <div class="col-md-7">
                           <select class="form-control" name="data[season]" id="seasonval" data-default_season_price="<?php echo $default_season_price;?>" data-ppv_plan_id="<?php echo $price['ppv_plan_id'];?>"  data-ppv_pricing_id="<?php echo $price['id'];?>" data-currency_id="<?php echo $price['currency_id'];?>" data-isSubscribed="<?php echo $data['member_subscribed'];?>" data-is_season_multi_price="<?php echo $ppv_season_status;?>" onchange="setSeason(this);">
                              <?php for ($i = $data['min_season']; $i <= $data['max_season']; $i++) { ?>
                              <option value="<?php echo $i;?>" <?php if((int)@$season == $i){ echo "selected";} ?>><?php echo $this->Language['season']; ?> <?php echo $i;?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-md-2">
                            <div class="season-loader-ppv" style="text-allign:center;display: none;">
                                <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
                                <span class="sr-only"><?php echo $this->Language['loding']; ?></span>
                        </div>
                            <div id="season-price-dv">
                            <?php echo $symbol; ?><span id="season-price-spn"><?php echo number_format((float) $season_price, 2, '.', '');?></span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <?php } ?>

                 
                     <?php 
                     if (isset($data['is_episode']) && intval($data['is_episode']) && intval($data['min_season']) && intval($is_episode_price_not_zero)) { ?>
                     <div class="form-group">
                        <div class="col-md-3">
                           <label style="font-weight: normal">
                           <input type="radio" name="data[plan]" <?php if (intval($is_episode_checked)) { ?>checked="checked" <?php } ?> value="episode" id="episodetext" data-price="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['episode_subscribed']; } else { echo $price['episode_unsubscribed']; }?>" onclick="setPriceForMulitPart(this);"/>
                           <?php echo $this->Language['episode'].":"; ?>
                           </label>
                        </div>
                        <div class="col-md-3">
                           <select class="form-control" name="data[season]" id="seasonval1" onchange="showEpisode(this);">
                              <?php for ($i = $data['min_season']; $i <= $data['max_season']; $i++) { ?>
                              <option value="<?php echo $i;?>" <?php if((int)@$data['series_number'] == $i){ echo "selected";} ?>><?php echo $this->Language['season']; ?> <?php echo $i;?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-md-4 ">
                           <select class="form-control" name="data[episode]" id="episodeval" onchange="setEpisode(this);">
                              <?php foreach ($data['episodes'] as $key => $value) { ?>
                              <option data-episode_id="<?php echo $value['id'];?>" value="<?php echo $value['embed_id'];?>" <?php if(@$data['stream_id'] == $value['embed_id']){ echo "selected";} ?>><?php echo $value['episode_title'];?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-md-2">
                           <?php echo $symbol; ?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['episode_subscribed']; } else { echo $price['episode_unsubscribed']; }?>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <?php } ?>
                     <div class="clearfix"></div>
                     <div class="form-group">
                        <div class="col-md-12">
                           <label id="data[plan]-error" class="error" for="data[plan]" style="display: none;"><?php echo $this->Language['choose_plan']; ?></label>
                        </div>
                     </div>

                     <div class="form-group">
                         <div class="col-md-12">
                             <div style="font-weight: bold">
                                 <?php echo $this->Language['price'].":"; ?> 
                                     <span id="charged_amt" data-amount="<?php echo $charged_amt;?>" data-currency="<?php echo $symbol;?>"><?php echo$symbol;?><?php echo $charged_amt;?></span>
                                     <span id="discount_charged_amt" style="display: none;">
                                     <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol.$charged_amt;?></span>
                                     <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                                     </span>
                             </div>
                         </div>
                     </div>

                     <?php } else { ?>
                         <span class="error"><?php echo $this->Language['no_video']; ?></span>
                     <?php } ?>
                     <?php } else  { ?>
                     <div class="form-group">
                         <?php if (isset($data['content_types_id']) && intval($data['content_types_id']) != 4) { ?>
                        <?php if (isset($validity->validity_period) && intval($validity->validity_period)) { ?>
                        <div class="col-md-12">
                            <?php echo $this->Language['available_for']; ?> <?php echo $validity->validity_period.' '.strtolower($validity->validity_recurrence)."s"?> <?php echo $this->Language['to_watch']; ?>
                        </div>
                        <?php } ?>
                        <?php } ?>

                         <div class="col-md-12">
                             <div style="font-weight: bold">
                                 <?php echo $this->Language['price'].":"; ?> 
                                     <span id="charged_amt" data-amount="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['price_for_subscribed']; } else { echo $price['price_for_unsubscribed']; }?>" data-currency="<?php echo $symbol;?>"><?php echo$symbol;?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['price_for_subscribed']; } else { echo $price['price_for_unsubscribed']; }?></span>
                                     <span id="discount_charged_amt" style="display: none;">
                                     <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol.$amount;?></span>
                                     <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                                     </span>
                             </div>
                         </div>
                     </div>
                 <?php } ?>
                 <div class="form-group ">
                   <?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
                     <div class="col-sm-6 pull-left">
                       <div class="input-group input-group-sm">
                          <input type="text" class="form-control" name="data[coupon_code]" id="coupon" placeholder="<?php echo $this->Language['coupon_code_optional']; ?>">
                          <input type="hidden" name="data[coupon_use]" value="0" id="coupon_use" />
                          <span class="input-group-btn">
                          <button type="button" class="btn btn-info btn-flat" id="coupon_btn" onclick="validateCoupon();"><?php echo $this->Language['btn_apply']; ?></button>
                          </span>
                       </div>
                       <div id="invalid_coupon_error" style="color:red;font-size:11px;display: none"></div>
                       <div id="valid_coupon_suc" class="has-success" style="display: none;">
                          <label for="inputSuccess" class="control-label" style="font-weight: normal;color: #4da30c;"><?php echo $this->Language['discount_on_coupon']; ?> <span id="coupon_in_amt"></span></label>
                       </div>
                     </div>
                     <div class="col-md-6">
                         <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return validateCategoryForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                      </div>
                 <?php } else { ?>
                     <div class="col-md-12">
                         <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return validateCategoryForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                      </div>
                 <?php } ?>
                  <div class="clear"></div>
               </div>
            </form>
            <?php } ?>
         </div>
   </div>

<?php }
    include('ppv_card_detail.php');
} ?>
    
<script type="text/javascript">
    var action = 'ppvpayment';
    var btn = '<?php echo $this->Language['btn_paynow']; ?>';
    var is_coupon_exists = 0;
    <?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
            is_coupon_exists = 1;
    <?php }?>
    
    $(document).ready(function () {
        getCardDetail();
        isCouponExist();
        
        $('input').attr('autocomplete', 'off');
        $('form').attr('autocomplete', 'off');
    });
    
    $(document).click(function() {
        $(".popover").hide();
    });

    $(".popover").click(function(event) {
        event.stopPropagation();
    });
    
    function setEpisode(obj) {
        var episode = $(obj).val();
         $("#btn_proceed_payment").attr( "onClick", "javascript: return validateCategoryForm();" );
        if ($.trim(episode)) {
            //$("#episodetext").prop('checked', true);
        } else {
            //$("#episodetext").prop('checked', false);
        }
        resetCouponPrice();
        if($('label.error').length) {
            $('label.error').hide();
        }
    }
    
    function setSeason(obj) {
        var season = $(obj).val();
        var is_season_multi_price = $(obj).attr("data-is_season_multi_price");
        if (parseInt(season) && parseInt(is_season_multi_price)) {
            var ppv_plan_id = $(obj).attr("data-ppv_plan_id");
            var ppv_pricing_id = $(obj).attr("data-ppv_pricing_id");
            var isSubscribed = $(obj).attr("data-isSubscribed");
            var currency_id = $(obj).attr("data-currency_id");
            var default_season_price = $(obj).attr("data-default_season_price");
        
            var url = "<?php echo Yii::app()->baseUrl; ?>/user/getPPVSeasonPrice";
            
            $("#season-price-dv").hide();
            $(".season-loader-ppv").show();
            
            $("#btn_proceed_payment").prop("disabled", true);
            $("#coupon_btn").prop("disabled", true);
            
            $.post(url, {'ppv_plan_id': ppv_plan_id, 'ppv_pricing_id': ppv_pricing_id, 'isSubscribed': isSubscribed, 'currency_id': currency_id, 'season': season}, function (res) {
                $(".season-loader-ppv").hide();
                
                var price = res;
                if (parseInt(res) === -1) {
                    price = default_season_price;
                }
                
                $("#season-price-spn").text(price);
                $("#seasontext").attr("data-price", price);
               $("#btn_proceed_payment").attr( "onClick", "javascript: return validateCategoryForm();" );
                $("#season-price-dv").show();
                $("#btn_proceed_payment").text("<?php echo $this->Language['btn_proceed_payment']; ?>");
                $("#btn_proceed_payment").removeAttr("disabled");
                $("#coupon_btn").removeAttr("disabled");
                
                if ($("#seasontext").is(':checked')) {
                    var cur = $("#charged_amt").attr('data-currency');
                    $("#charged_amt").attr("data-amount", price);
                    $("#charged_amt").text(cur+price);
                    $("#dis_charged_amt").text(cur+price);
                    $("#coupon_use").val(0);
                    $("#coupon").val('');
                    
                    if (parseFloat(price) === 0.00) {
                        $("#btn_proceed_payment").text("Play");
                        $("#coupon_btn").prop("disabled", true);
                    }
                }
            });
        }else{
                 $("#btn_proceed_payment").attr( "onClick", "javascript: return validateCategoryForm();" );
        }
         resetCouponPrice();
        if (parseInt(season)) {
            //$("#seasontext").prop('checked', true);
        } else {
            //$("#seasontext").prop('checked', false);
        }
    }
    
    function setPriceForMulitPart(obj) {
        var price = $(obj).attr("data-price");
        var cur = $("#charged_amt").attr('data-currency');
        $("#charged_amt").text(cur+price);
        $("#charged_amt").attr('data-amount', price);
        $("#dis_charged_amt").text(cur+price);
        $("#btn_proceed_payment").text("<?php echo $this->Language['btn_proceed_payment']; ?>");
         $("#btn_proceed_payment").attr( "onClick", "javascript: return validateCategoryForm();" );
        if (parseFloat(price) === 0.00) {
            $("#btn_proceed_payment").text("Play");
            $("#coupon_btn").prop("disabled", true);
        }
        
        if (parseInt(is_coupon_exists)) {
            resetCouponPrice();
        }
    }
    
    function showEpisode(obj) {
        var movie_id = $("#ppvmovie_id").val();
        var season = $(obj).val();
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/getEpisodes";
         resetCouponPrice();
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        
        if($('label.error').length) {
            $('label.error').hide();
        }
        
        $.post(url, {'movie_id': movie_id, 'season': season}, function (res) {
            $("#btn_proceed_payment").removeAttr("disabled");
            var str = '';

            if (res.length) {
                for (var i in res) {
                    str = str + '<option data-episode_id="'+res[i].id+'" value="'+res[i].embed_id+'">'+res[i].episode_title+'</option>';
                }
            }
            $("#episodeval").html(str);
              $('#loader-ppv').show();
             $("#btn_proceed_payment").attr( "onClick", "javascript: return validateCategoryForm();" );
            if ($.trim($("#episodeval"))) {
                 $('#loader-ppv').hide();
                //$("#episodetext").prop('checked', true);
            } else {
                  $('#loader-ppv').hide();
                $("#episodetext").prop('checked', false);
            }
        }, 'json');
    }
    
    jQuery.validator.addMethod("isseason", function (value, element) {
        if ($("#seasontext").is(':checked')) {
            if ($.trim($("#seasonval").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, JSLANGUAGE.select_season);
    
    jQuery.validator.addMethod("isepisode", function (value, element) {
        if ($("#episodetext").is(':checked')) {
            if ($.trim($("#episodeval").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, JSLANGUAGE.select_episode);
    
</script>

<script type="text/javascript" src="<?php echo $this->siteurl;?>/common/js/action.js"></script>
<!-- As the processing the card in different payment gateway is different. So processCard javascript method is in following included file.-->

<?php 
$payment_gateway_str = '';
if(isset($this->PAYMENT_GATEWAY) && count($this->PAYMENT_GATEWAY) > 1){
    $payment_gateway_str = implode(',', $this->PAYMENT_GATEWAY);
    foreach ($this->PAYMENT_GATEWAY as $gateway_code){
        
    ?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code].'.js';?>"></script>
    <?php }}else{
if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
    $payment_gateway_str = $this->PAYMENT_GATEWAY[$gateway_code];?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code].'.js';?>"></script>
<?php } }?>

<input type="hidden" id="payment_gateway_str" value="<?php echo $payment_gateway_str;?>">