<div class="container">
    <div class="container">
        <div class="span12">
            <h2 class="btm-bdr">Frequently Asked Questions</h2>
            <div class="row" style="margin-left: 5px;">
                <div id="question_1" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">How is it different from YouTube and Vimeo?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_1" class="ans_block">
                    YouTube and Vimeo offer only a player that you can embed in your VOD platform. You still have to build the rest of the VOD platform which includes website, payment gateway, analytics and much more. Please refer <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/howisitdiff">www.muvi.com/howisitdiff</a>. 
                    With Muvi, you get “www.myname.com” whereas with YouTube or Vimeo, you get “www.vimeo.com/myname”. 

                </div>
                <div class="clear" style="height: 15px;"></div>
                <div id="question_2" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">Does it support all pricing models, SVOD, AVOD and TVOD?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_2" class="ans_block">
                    Yes. Muvi supports all pricing models for your VOD platform including SVOD (subscription), AVOD (advertising) and TVOD (pay-per-view), and a mix and match of all these models. You can configure all these from the Admin Panel.
                </div>
                <div class="clear" style="height: 15px;"></div>
                <div id="question_3" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">Are my content secure? Does it have DRM?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_3" class="ans_block">
                    Yes. Muvi uses world-class DRM, very similar to the one used by Netflix. Your content are encrypted and completely secure. Only the users who have paid for or authorized to view the content can view it.
                </div>
                <div class="clear" style="height: 15px;"></div>
                <div id="question_4" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">Can I customize the look and feel of the website?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_4" class="ans_block">
                    Yes, you can completely customize the look and feel of your VOD platform. Customization goes much beyond than just replacing logo and color scheme. You can completely change the layout so that your VOD platform looks very unique like no other.
                    Select one of the available templates or add your custom design or ask our design team to produce a custom design for you. All options are available.
                </div>
                <div class="clear" style="height: 15px;"></div>
                <div id="question_5" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">How much does Muvi cost?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_5" class="ans_block">
                    First 14 days are free. Then, pay a low monthly fee of $399 + a fee based on bandwidth consumed. See pricing to learn more. $399 per month includes everything including hosting, storage, DRM, CDN, player and all website features. 
                    The bandwidth based <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/pricing">pricing</a> allows you to pay as you grow. More users you have>more revenue you make>more bandwidth you consume >and only accordingly, you pay more.
                </div>
                <div class="clear" style="height: 15px;"></div>
                <div id="question_6" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">Who collects the money from subscribers?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_6" class="ans_block">
                    You. <br/>
                    It’s your platform and subscribers. Muvi uses your payment gateway, all of the money collected from your users minus the nominal payment gateway fee goes to your bank account. 
                </div>
                <div class="clear" style="height: 15px;"></div>
                <div id="question_7" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">I am interested in launching a mobile app. Does Muvi support this?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_7" class="ans_block">
                    Support for mobile app is currently in Beta. It will be publicly available in Q2 2015. 
                    It will use the same CMS (content management system). All videos, metadata and settings you currently add will apply as it is. You won’t have to re-upload or update anything. 

                </div>
                <div class="clear" style="height: 15px;"></div>
                <div id="question_8" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">Can I try this out?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_8" class="ans_block">
                    Yes, you can signup for our <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/signup">14-Days FREE TRIAL</a>. 
                    You can also see the user interface (VOD website) in <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/examples">Examples</a>, 
                    and screenshots of the Admin Panel in <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/tour">Tours</a> section.                     
                </div>
                <div class="clear" style="height: 15px;"></div>
                <div id="question_9" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">I am not interested in launching my own VOD platform. What other choices do I have to monetize my content?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_9" class="ans_block">
                    You can upload your content on <a href="http://www.muvi.com" target="_blank">Muvi.com</a>. It works very similar to YouTube, i.e., users consume the content on Muvi.com and you earn a revenue for every view. Learn more at <a href="http://www.muvi.com/muvi">http://www.muvi.com/muvi</a>.
                </div>
                <div class="clear" style="height: 15px;"></div>



                <div id="question_10" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">I love your platform, but I don&rsquo;t have any content. Do you also provide the content?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_10" class="ans_block">
                    No, currently we do not provide any video content; it&rsquo;s your responsibility to add your own content.
                </div>
                <div class="clear" style="height: 15px;"></div>           

                <div id="question_11" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">Can I upload content for which I currently do not hold the rights?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_11" class="ans_block">
                    No, you must and can only upload content for which you currently hold the rights for and must stream the same to only those geographies for which these 
                    rights are valid. You can control the geographic settings from the Admin panel. 
                </div>
                <div class="clear" style="height: 15px;"></div>              

                <div id="question_12" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">What kind of CDN and hosting is used? Who is the technology partner?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_12" class="ans_block">
                    Muvi has partnered with Amazon to bring their world class cloud hosting, 
                    CDN and storage for your VoD Platform so that you are assured of 100% peace of mind and uptime guarantee.
                </div>
                <div class="clear" style="height: 15px;"></div>            

                <div id="question_13" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">What if my business scales exponentially, will Muvi be able to handle millions of users and requests per sec?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_13" class="ans_block">
                    Muvi is hosted on Amazon Cloud, which scales along with your business requirement, so you don&rsquo;t need to worry about a sudden surge in 
                    traffic as along with the surge in traffic the hardware automatically scales!
                </div>
                <div class="clear" style="height: 15px;"></div> 

                <div id="question_14" class="faq_block">
                    <div class="pull-left plus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/plus.png" /></div>
                    <div class="pull-left minus_icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/minus.png" /></div>
                    <div class="pull-left que">Does Muvi support Live Streaming?</div>
                </div>
                <div class="clear"></div>
                <div id="ans_14" class="ans_block">
                    Yes, Muvi supports Live Streaming, however since this is a complex request we currently handle this offline. 
                    <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/contact">Contact us</a> with your Live Streaming requirement and we will help you out.
                </div>
                <div class="clear" style="height: 15px;"></div>             


            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".faq_block").click(function () {
            var obj_id = this.id;
            var sl = obj_id.split('_');
            var sl_no = sl[1];
            //$(this).hide();
            $("#ans_" + sl_no).slideToggle("slow");
            $("#question_" + sl_no + " .plus_icon").toggle();
            $("#question_" + sl_no + " .minus_icon").toggle();
            //$("#question_"+sl_no).toggle("slow");
        });
        $(".que").hover(
                function () {
                    $(this).css("text-decoration", "underline");
                }, function () {
            $(this).css({"text-decoration": "none"});
        }
        );
    });

</script>