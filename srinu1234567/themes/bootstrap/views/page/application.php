<style>
    .form-control{
        margin: 10px;
    }
    select{
        height: 40px;
        width: 230px;
    }
    .form-group{
        margin: 10px;
    }
    label.error{
        display: inline;
    }
</style>
<script type="text/javascript">
    $.validator.addMethod("mail", function (value, element) {
        return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
    }, "Please enter a correct email address");
    jQuery.validator.addMethod("phone", function (value, element) {
        return this.optional(element) || /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(value);
    }, "Please enter correct phone number");

    $(document).ready(function () {
        //form validation rules
        $("#partnerapply").validate({
            rules: {
                "application_type[]": {
                    required: true
                },
                name: {
                    required: true
                },
                company_name: {
                    required: true
                },
                industry: {
                    required: true
                },
                email: {
                    required: true,
                    mail: true
                },
                phone: {
                    required: true,
                    phone: true
                },
                numberofcustomer: {
                    required: true
                },
                reason: {
                    required: true
                }
            },
            messages: {
                "application_type[]": {
                    required: "Please select atleast one"
                },
                name: {
                    required: "Please provide your name"
                },
                company_name: {
                    required: "Please enter company name"
                },
                industry: {
                    required: ""
                },
                email: {
                    required: 'Please enter your email',
                    mail: 'Please enter a valid email address'
                },
                phone: {
                    required: 'Please enter your phone',
                    phone: 'Please enter a valid phone number'
                },
                numberofcustomer: {
                    required: 'Please choose no of custmers'
                },
                reason: {
                    required: 'Please enter reason'
                }
            },
            submitHandler: function (form) {
                $('#nextbtn').html('wait!...');
                $('#nextbtn').attr('disabled', 'disabled');
                $.post('insertapplication', $("form#partnerapply").serialize(), function (res) {
                    if (res.succ) {
                        $('#content').html(res.msg);
                    } else {
                        $('#email-error').html(res.msg).show();
                        $('#nextbtn').html('Submit Application');
                        $('#nextbtn').removeAttr('disabled');
                    }
                }, 'json');
            }
        });
    });
</script>
<div class="container" id="content" style="min-height: 500px;">
    <h3>Apply for Reseller/Referral Partnership</h3>
    <div>
        <form class="form-horizontal" method="post" name="partnerapply" id="partnerapply" action="javascript:void(0);">
            <div class="form-group">
                <label for="application" class="col-sm-3 control-label">Type of Application</label>
                <div class="col-sm-9" style="height: 40px;">
                    <input type="checkbox" name="application_type[]" value="1"> Referral &nbsp;&nbsp;&nbsp;
                    <input type="checkbox" name="application_type[]" value="2"> Reseller
                    <label id="application_type[]-error" class="error" for="application_type[]" style="display: inline;"></label>
                </div>                
            </div>
            <div class="clear"></div>
            <div class="form-group">
                <label for="companyname" class="col-sm-3 control-label">Company Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="industry" class="col-sm-3 control-label">Industry</label>
                <div class="col-sm-9">
                    <select class="form-control" name="industry">
                        <option value="">Select Your Industry</option>
                        <option value="Broadcaster">Broadcaster</option>
                        <option value="IT Services">IT Services</option>
                        <option value="Individual Consultant">Individual Consultant</option>
                        <option value="Others">Others</option>
                    </select>
                    &nbsp;&nbsp;&nbsp; <i>Broadcaster, IT Services, Individual Consultant, Others</i>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Primary Contact</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" placeholder="FirstName LastName" value="">
                </div>
            </div>            
            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Phone Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Starting with country code">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="email" name="email" value="">
                    <label id="email-error" class="error" for="email" style="display: none;"></label>
                </div>
            </div>
            <div class="form-group">
                <label for="numberofcustomer" class="col-sm-3 control-label">No of Customers</label>
                <div class="col-sm-9">
                    <select class="form-control" name="numberofcustomer">
                        <option value=""></option>
                        <option value="1-5">1-5</option>
                        <option value="6-10">6-10</option>
                        <option value="More Than 10">More Than 10</option>
                    </select>
                    &nbsp;&nbsp;&nbsp; <i>Potential number of customers you can bring in</i>
                </div>
            </div>
            <div class="form-group">
                <label for="reason" class="col-sm-3 control-label">Reason for Application</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="reason"></textarea>
                </div>
            </div>
            <div>
                <button type="submit" class="btn btn-blue" id="nextbtn">Submit Application</button>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>