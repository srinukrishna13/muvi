<div class="container">
    <div class="container contact_us">
        <div class="span12">
            <h2 class="btm-bdr">Tour of Admin Panel</h2>
            <div class="row">
                <div style="position:relative;">
                <div id="pic_slider" class="pics pull-left" style="position: relative; overflow: hidden;">
                  <?php  
                  $demo_form = '<form class="form-inline" name="demo_form" id="demo_form" method="post"><input type="text" class="form-control" id="demo_name" name="demo_name" placeholder="Name" /><input type="text" class="form-control" id="demo_company" name="demo_company" placeholder="Company" style="margin-left:20px;" /><input type="email" class="form-control" id="demo_email" name="demo_email" placeholder="Email Address"  style="margin-left:20px;"/><button type="submit" class="btn btn-blue" id="demo_submit"  style="margin-left:20px;">Request Demo</button><div id="demo_error" class="error"></div></form>';
                  $tour_desc = array(
                      array("img" => "Choose-a-template.jpg", "heading"=>"Choose a template","desc" => "Chose one of the available templates, add your own or ask our design team to custom design a template."),
                      array("img" => "Easily-upload-videos.jpg", "heading"=>"Easily upload videos","desc" => "Easily upload videos using your web browser. Depending on your Internet speed, a full length movie takes less than 30 minutes. "),
                      array("img" => "Manage-meta-data.jpg", "heading"=>"Manage metadata","desc" => "Manage metadata of your content including trailer, actors, poster, genre and more. All metadata becomes visible on the content page making it more interesting for users and are also used for search."),
                      array("img" => "Set-pricing-model.jpg", "heading"=>"Set pricing model (SVOD, TVOD)","desc" => "Set different pricing models for your content including pay-per-view and subscription. A ton of options are available including offers, coupons and discounted pricing for subscribers."),
                      array("img" => "Analytics.jpg", "heading"=>"Analytics","desc" => "View easy to understand and in-depth analytics of subscribers, content, what users are searching for and more."),
                      array("img" => "Reports.jpg", "heading"=>"Reports","desc" => "View more than a dozen reports available including revenue and website traffic."),
                      array("img" => "Email-marketing.jpg", "heading"=>"Subscriber Communication","desc" => "Leverage built-in email marketing tools to send custom emails to subscribers. Great for retaining and building your subscriber base.")
                      //array("img" => "live_demo.jpg", "heading"=>"Live Demo", "desc" => $demo_form) 
                );
                $cnt = count($tour_desc);
                for($i = 0; $i < $cnt ; $i++){ ?>
                
                    <img id ="img_tour_<?php echo $i; ?>" src="<?php echo Yii::app()->theme->baseUrl."/images/tour/".$tour_desc[$i]['img']; ?>" alt="<?php echo $tour_desc[$i]['heading']?>" title="<?php echo $tour_desc[$i]['heading']?>" width="100%" height="100%" style=" top: 0px; left: 0px; display: block; z-index: 4; opacity: 1; width: 100%; height: 100%; ">
                
<?php  } ?>
                </div>
                <div id="tour_next" class="tour_next"> </div>
                <div id="tour_prev" class="tour_prev"> </div>
            </div>
    <?php for($i = 0; $i < $cnt ; $i++){ ?>
        
                <div id="desc_tour_<?php echo $i; ?>" class="tour_detail">
                    <div class="tour_heading"><?php echo $tour_desc[$i]['heading']?></div>
                    <div class="clear" style="height:10px;"></div>
                    <div class="tour_desc"><?php echo $tour_desc[$i]['desc']?></div>
                </div>
    <?php } ?>
                     
                <div id="output" class="output_tour">
                    
                </div>
            </div>
        </div>
        <!--
        <div class="clear" style="height:10px;"></div>
        <div class="span12">
            <h2 class="btm-bdr">Demo</h2>
            <div class="row">
                <p>
                    Visit examples to see demo of a sample front-end (VOD platform). Fill up the form below to request the demo of the back-end CMS and analytics.
                </p>
                <div class="clear" style="height:10px;"></div>
                <div style="padding-left: 10%;">
                    <?php echo $demo_form ?>
                </div>
            </div>
        </div>-->
    </div>
</div>
<script type="text/javascript">
            $(document).ready(function(){  
                $('#pic_slider').cycle({ 
                    fx:     'fade', 
                    timeout: 0, 
                    next:   '#tour_next', 
                    prev:   '#tour_prev', 
                    //before:  onBefore, 
                    after:   onAfter 
                 });
            });
            $("#demo_submit").live("click", function(){
                $("#demo_form").validate({
                    wrapper: "li",
                    rules: {    
                        demo_name: {
                            required: true,
                            minlength: 5
                        },                
                        demo_email: {
                            required: true,
                            mail: true
                        },
                        demo_company: {
                            required: true,
                            minlength: 5
                        },                        
                    },                      
                    messages: {             
                        demo_name: {
                            required: "Please enter your name, &nbsp;&nbsp;&nbsp;",
                            minlength: "Name must be atleast of 5 characters long, &nbsp;&nbsp;&nbsp;",
                        },                
                        demo_email: {
                            required: "Please enter email address, &nbsp;&nbsp;&nbsp;",
                            mail: "Please enter a valid email address,&nbsp;&nbsp;&nbsp;",
                        },
                        demo_company: {
                            required: "Please enter your company name",
                            minlength: "Company name must be atleast of 5 characters long",
                        },                          
                    },
                    errorPlacement: function(error, element) {
                        error.appendTo('#demo_error');
                    },        
                    submitHandler: function(form) {
						$.ajax({
                            url: "<?php echo Yii::app()->getbaseUrl(true); ?>/site/demorequest",
                            data: $('#demo_form').serialize(),
                            type:'POST',
                            dataType: "json",
							 beforeSend:function(){
								ga('send', 'event', { eventCategory: 'demo', eventAction: 'submit', eventLabel: 'demoform'});
							},
                            success: function (data) {                                
                                if(data.stat == 'success'){
                                    bootbox.alert(data.message, function(){
                                       $("#demo_form")[0].reset();
                                       // window.location = '<?php echo Yii::app()->getbaseUrl(true); ?>/sdk';
                                    });                                    
                                }else{
                                    $('#demo_error').html('data.message');
                                    return false;
                                }
                            }
                        });
                    }                    
                });                
            });
            
            function onAfter() { 
                var id_spl = this.id.split("_");
                var id_no = id_spl[2];
                var desc_content = $("#desc_tour_"+id_no).html();
                $('#output').html(desc_content);
                //$('#demo_error').html("");
                //$('#output').find('form').attr("id", "demo_form");
               
            }
        </script>