<?php
if (isset(Yii::app()->user->created_at)) {
    $bildate = date('dS', (strtotime(Yii::app()->user->created_at) + 15 * 24 * 60 * 60));
} else {
    $bildate = date('dS', (time() + 15 * 24 * 60 * 60));
}
$months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
$config = Yii::app()->common->getConfigValue(array('trial_period','monthly_charge'), 'value');
?>
<p>
    <span class="payment-span">$<?php echo $config[1]['value']?> per month + Bandwidth fee, if you exceed 1TB in a month.</span><br />
    <a href="<?php echo Yii::app()->baseUrl; ?>/pricing" target="_blank" class="link">Learn more about pricing</a>
	<div style="height:0px;"></div>
</p>
<form class="form-horizontal" method="post" name="paymentMethod" id="paymentMethod" enctype="multipart/form-data" onsubmit="return validateForm();" action="javascript:void(0);">
    <div id="card-info-error" class="error" style="display: none;margin: 0 0 15px 150px;"></div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Name on Card</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="card_name" name="card_name" placeholder="Enter Name" required="true">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Card Number</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Enter Card Number" required="true">
        </div>
    </div>
    <div class="form-group exp-dt-dropdown">
        <label for="inputEmail3" class="col-sm-3 control-label">Expiry Date</label>
        <div class="col-sm-9">
            <select name="exp_month" id="exp_month" class="form-control" required="true">
                <option value="">Month</option>	
                <?php for ($i = 1; $i <= 12; $i++) { ?>
                    <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
<?php } ?>
            </select>
            <select name="exp_year" id="exp_year" class="form-control" required="true" onchange="getMonthList();">
                <option value="">Year</option>
                <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
<?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Security Code</label>
        <div class="col-sm-9">
            <input type="password" id="" name="" style="display: none;" />
            <input type="password" class="form-control" id="security_code" name="security_code" placeholder="Enter security code" required="true">
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword3" class="col-sm-3 control-label">Billing Address</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="address1" name="address1" placeholder="Address 1" required="true">
            <input type="text" class="form-control" id="address2" name="address2" placeholder="Address 2" >
            <input type="text" class="form-control" id="city" name="city" placeholder="City" required="true"><br/>
            <input type="text" class="form-control" id="state" name="state" placeholder="State" required="true" style="width: 23%">
            <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode" required="true" style="width: 24%">
        </div>
    </div>
    <div class="form-group">
            <div class="col-sm-9">
                    <input type="checkbox" class="form-control" id="terms_of_use" name="terms_of_use" required="true" checked="checked">&nbsp; I have read and agree with the <a href="<?php echo $this->createUrl('signup/signupTerms');?>" target="_blank">terms of use</a> of Muvi 
                    <br/>
                    <label id="terms_of_use-error" class="error" for="terms_of_use" style="display: none;"></label><br/>
            </div>
    </div>	
    
    <div class="clear2"></div>
    <div class="row">
        <!--<div class="span3 center">
            <button id="market_submit" class="btn btn-gray btn-large" id="prvbtn" type="button"> Back </button>
        </div>-->

        <div class="center">
            <button type="submit" class="btn btn-blue" id="nextbtn">Next</button>
        </div>
    </div>   
    <input type="hidden" value="0" id="is_submit" name="is_submit"/>
</form>
<div style="display: none;" id="mnth_opt">
    <option value="">Expiry Month</option>
    <option value="1">JAN</option>
    <option value="2">FEB</option>
    <option value="3">MAR</option>
    <option value="4">APR</option>
    <option value="5">MAY</option>
    <option value="6">JUN</option>
    <option value="7">JUL</option>
    <option value="8">AUG</option>
    <option value="9">SEP</option>
    <option value="10">OCT</option>
    <option value="11">NOV</option>
    <option value="12">DEC</option>
</div>

<div id="successPopup" class="modal fade" style="width: 660px;">
    <div class="modal-dialog" style="width: 660px;">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title success-popup-payment">Thank you, Your card authenticated successfully.<br/>Please <a href="<?php echo Yii::app()->getBaseUrl(true).'/signup/success'?>">Click here</a> to go to the next step Or Wait we are redirecting you...</div>
            </div>
        </div>
    </div>
</div>

<div id="loadingPopup" class="modal fade in">
        <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header" style="border: none;">
                                <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                                <div><img src="<?php echo Yii::app()->baseUrl;?>/images/payment_loading.gif" style="padding:5px;"/></div>
                        </div>
                </div>
        </div>
</div>	

<script type="text/javascript">	
	//$("#loadingPopup").modal('show');
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    
    $(document).ready(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
    });
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;
        
        if (curyr === selyr) {
            startindex = curmonth;
        }
        
        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" '+selected+'>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }
    
    function validateForm() {
        $('#card-info-error').hide();
        
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: "required",
                address1: "required",
                city: "required",
                state: "required",
                zipcode: "required",
				'terms_of_use':'required',
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                address1: "Please enter your address",
                city: "Please enter your city",
                state: "Please enter your State",
                zipcode: "Please enter your Zipcode",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            }
        });
        var x = validate.form();
        if (x) {
            $('#nextbtn').html('wait!...');
            $('#nextbtn').attr('disabled','disabled');

            // Processing popup
            $("#loadingPopup").modal('show');
            var url ="<?php echo Yii::app()->baseUrl; ?>/signup/createConsumer";
            var card_name = $('#card_name').val();
            var card_number = $('#card_number').val();
            var exp_month = $('#exp_month').val();
            var exp_year = $('#exp_year').val();
            var cvv = $('#security_code').val();
            var address1 = $('#address1').val();
            var address2 = $('#address2').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var zip = $('#zipcode').val();
			
            $.post(url,{'card_name':card_name, 'card_number':card_number, 'exp_month':exp_month, 'exp_year':exp_year, 'cvv':cvv, 'address1':address1, 'address2':address2, 'city':city, 'state':state, 'zip':zip},function(data){
               if (parseInt(data.isSuccess) === 1) {
                    /*$('#nextbtn').html('Next');
                    $('#nextbtn').removeAttr('disabled');
                    $("#paymentMethod")[0].reset();*/
					$("#loadingPopup").modal('hide');
                    $("#successPopup").modal('show');
                    setTimeout(function() {
                        $('#is_submit').val(1);
                        //window.history.back(0) = '<?php echo Yii::app()->baseUrl; ?>/signup/signupTerms';
                        document.paymentMethod.action = '<?php echo Yii::app()->baseUrl; ?>/signup/success';
                        document.paymentMethod.submit();return false;
                    }, 5000);
               } else {
                    $("#loadingPopup").modal('hide');
                    $('#nextbtn').html('Next');
                    $('#nextbtn').removeAttr('disabled');
                    $('#is_submit').val('');
                    if ($.trim(data.Message)) {
                        $('#card-info-error').show().html(data.Message);
                    } else {
                        $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                    }
               }
            }, 'json');
        }
    }
</script>