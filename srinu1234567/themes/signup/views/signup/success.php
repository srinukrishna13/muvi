<div class="span1"></div>
<div class="span10 singup-succ">    
    <div class="divider-blue2"></div>

    <div class="span6">
        <h4 class="blue" style="text-align: left;">Next Steps</h4>
        <div class="free-trial-list">
            <ul>
                <li>Add a few content, try out the flow to see how it works</li>
                <li>Select a template, add your logo</li>
                <li>Send us your payment gateway information, it's required to accept credit card payment on your site</li>
                <li>Go live! Change DNS setting of your name to IP address to go live on your own domain name</li>

            </ul>
        </div>
    </div>

    <div class="span3">
        <div class="comment-box">
            <p>A Muvi representative will contact you shortly. Please ask if you have any questions, we will respond ASAP!</p>
            <div class="traingle-icon"></div>
        </div>
    </div>
    <div class="clear2"></div>
    <div class="divider-blue2"></div>    

    <div class="footer-succ-signup center">
        <p>
            <a class="btn btn-blue" href="<?php echo $this->createUrl('admin/dashboard'); ?>">Proceed to Admin Panel</a>
        </p>
    </div>

</div>	

<style>
    .clear2{
        height: 0 !important;
    }
    .pad50{
        padding-top: 30px !important;
        padding-bottom: 30px !important;
    }
    .divider-blue2{
        margin: 21px 0px !important;
    }
    .footer-succ-signup{
        padding-top: 0 !important;
    }
</style>