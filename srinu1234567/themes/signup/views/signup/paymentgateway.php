<div class="span12">
	<p>You need a payment gateway to accept credit card payments on you VOD website.</p>
	<p>If you already have a payment gateway, Please email the information to studio@muvi.com. Otherwise, a Muvi representative will contact you soon to help you setup a payment gateway account.</p>
</div>
<div style="clear: both"></div>
<div class="next-prev-btn">
	<a href="<?php echo Yii::app()->baseUrl;?>/signup/typeofContent"><button type="button" class="btn btn-grey" id="prvbtn">Back</button></a> &nbsp;&nbsp;
	<a href="<?php echo Yii::app()->baseUrl;?>/signup/signupTerms"><button type="submit" class="btn btn-blue" id="nextbtn">Next</button></a>
</div>