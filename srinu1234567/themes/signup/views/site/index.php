<?php $base_url = Yii::app()->request->getHostInfo();?>

<div class="wrapper">
    <div class="container home-page-studio">
        <h2 class="btm-bdr">Muvi</h2>
        <h3>Enables content owners <span class="blue">maximize revenue</span> from their content by offering several tools <br />
            that help find the right audience and increase the reach of their content.</h3>
    </div>
</div>


<div class="wrapper grybg">
    <div class="container home-page-customers home-page-studio error">
        <p class="center home_img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/oops.png" /></p>
        <h3>We are sorry!</h3>
        <h3>This account is not active at this time. Please <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/contact" class="link">contact us</a> to get this account.</h3>
    </div>        
</div> 

<div class="container" id="page">
    <h2 class="btm-bdr">Our Solutions</h2>
    <div class="sols">
        <div class="span3">
            <p class="center home_img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/streaming-app.png" /></p><br>
            <h3><a href="<?php echo Yii::app()->getBaseUrl(true); ?>/sdk">Streaming Website/App</a></h3>
            <p>Muvi SDK offers a streaming website and mobile app on your name (www.yourname.com). Get it up and running in minutes, at ZERO cost. 
                Muvi  takes care of everything including hosting, streaming, DRM, payment gateway and maintenance.</p>
        </div>
        <div class="span3">
            <p class="center home_img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/digital-asset.png" /></p><br>
            <h3><a href="<?php echo Yii::app()->getBaseUrl(true); ?>/dam">Digital Asset Management</a></h3>
            <p>Store all your digital assets in the cloud. Muvi DAM (Digital Asset Management) provides content creators and aggregates a secure, 
                cost-effective cloud-based solution for storage, conversion and distribution of their media content.</p>
        </div>
        <div class="span3">
            <p class="center home_img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/monetize.png" /></p><br>
            <h3><a href="<?php echo Yii::app()->getBaseUrl(true); ?>/muvi">Monetize your Content</a></h3>
            <p><a href="http://www.muvi.com" class="blue" target="_blank">Muvi.com</a> is a social content discovery platform for movies and TV 
                shows. Content owners upload their content to directly sell to the audience who pays to watch it. Unlike YouTube and other 
                streaming platforms, Muvi.com helps you find the exact audience whose taste matches with your content.</p>
        </div>
        <div class="span3">
            <p class="center home_img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/affiliate.png" /></p><br>
            <h3><a href="<?php echo Yii::app()->getBaseUrl(true); ?>/affiliate">Affiliate Program</a></h3>
            <p><a href="http://www.muvi.com" class="blue" target="_blank">Muvi.com</a> is a growing marketplace for streaming companies. Muvi Affiliate helps you setup your 
                presence in this marketplace which in turns allows you acquire new subscribers. Imagine opening a storefront in &ldquo;eBay for media content&rdquo;.</p>
        </div>                

    </div>
</div>   

