<?php if(WP_USE_THEMES==1) { ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 frt_footer_box">
            <div class="col-md-5 col-sm-5 text-center frt_footer_box1">
                    <h3>Who is Using Muvi?</h3><br/>
<?php
                $args = array(
                    'posts_per_page' => -1,
                    'post_type' => 'banners',
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key' => 'banner_categories',
                            'value' => 'who-is-using-it',
                            'compare' => '=',
                        )
                    )
                );
                $the_query = new WP_Query($args);
                ?> 
                <?php if ($the_query->have_posts()): ?>
                    <div class="owl-carousel owl-theme who_is_useing_it">
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <?php $img_info = get_field('image'); ?>
                            <?php $linkinfo = get_field('link'); ?>
                        <div class="item">
                                <?php if ($linkinfo): ?>
                                    <a href="<?php echo $linkinfo; ?>" target="_blank">
                                    <?php endif; ?>
                                        <img alt="<?php the_title(); ?>" title="<?php the_title(); ?>" src="<?php echo $img_info; ?>" >
                                    <?php if ($linkinfo): ?>
                                    </a>
                                <?php endif; ?>
                          </div>
                        <?php endwhile; ?>
                             
                   </div>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
 </div>
            <div class="col-md-1 col-sm-1 frt_footer_box2"></div>
            <div class="col-md-1 col-sm-1"></div>
            <div class="col-md-5 col-sm-5 frt_footer_box3">

                        <?php
                $args = array(
                    'posts_per_page' => -1,
                    'post_type' => 'testimonial'
                );
                $the_query = new WP_Query($args);
                ?> 
                <?php if ($the_query->have_posts()): ?>
                    <div class="owl-carousel owl-theme frt_testimonial">
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <?php $img_info = get_field('image'); ?>
                            <?php $linkinfo = get_field('link'); ?>
                            <div class="item">
                                <div class="col-md-12 text-center">
                                    <img alt="client" src="<?php echo the_post_thumbnail_url('true'); ?>" class="frt_testimonial_img">
                                        <span class="frt_testimonial_title"><?php the_title(); ?></span>
                                </div>
                              <div class="col-md-12 frt_testimonial_txt"><?php the_content(); ?></div>
                            </div>
                        <?php endwhile; ?>
                             
                   </div>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>

                </div>
        </div>
    </div>
 </div>
 <footer id="footer" class="footer-4">
    <div class="container">
        <div class="row">
            <div class="footer-details">
                <?php
                if (is_active_sidebar('secondary-footer-widget')) {
                    dynamic_sidebar('secondary-footer-widget');
                }
                ?>
            </div>
        </div>
    </div>
</footer>
<?php } ?>
