var interval;
var buff_log_id = 0;
var u_id = 0;
var resolution = 144;
function buffered_loaded() {
    var v = videojs('video_block');
    if (!v.buffered) {
        updateBuffered(v,'',-1);
    } else {
        updateBuffered(-1,'',-1);
    }
}

function updateNewBuffered(player,previousBufferEnd,currTim){
    var buff_log_id = document.getElementById('buff_log_id').value;
    updateBuffered(player,currTim,buff_log_id);
    var bufferenEnddd = previousBufferEnd;
    var bufferenStart = currTim;
    var resolution = player.getCurrentRes();
    
    var buff_log_id = document.getElementById('buff_log_id').value;
    var u_id = document.getElementById('u_id').value;
    var device_id = document.getElementById('device_id').value;
    var device_type = document.getElementById('device_type').value;
    var user_id = document.getElementById('user_id').value;
    var embed_id = $('#embed_id').val();
    
    $.post('/test/EmbedNewBufferLog', {'embed_id': embed_id,'resolution':resolution,'start_time': bufferenStart, 'end_time': bufferenEnddd,'device_id':device_id,'device_type':device_type,'user_id':user_id}, function (res) {
        var obj = JSON.parse(res);
        buff_log_id = obj.id;
        u_id = obj.u_id;
        document.getElementById('u_id').value = u_id;
        if(buff_log_id){
            document.getElementById('buff_log_id').value = buff_log_id;
        }
    });
}

function updateBuffered(player,currTim,log_id) {
    var duration = parseInt(document.getElementById('full_video_duration').value);
    if((typeof player == 'undefined') || player == -1){
        var v = videojs('video_block');
        var r = v.buffered();
    }else{
        var r = player.buffered();
    }
    if (r) {
        var buffLen = r.length;
        if(buffLen > 0){
            buffLen = buffLen - 1;
        }else{
            buffLen = 0;
        }
        var bufferenEnddd = r.end(buffLen);  
        if(buffLen < 0){
            var bufferenStart = 0;
        }else{
            var bufferenStart = r.start(buffLen);
        }
        var resolution = document.getElementById('full_video_resolution').value;
        
        var buff_log_id = document.getElementById('buff_log_id').value;
        var u_id = document.getElementById('u_id').value;
        var device_id = document.getElementById('device_id').value;
        var device_type = document.getElementById('device_type').value;
        var user_id = document.getElementById('user_id').value;
        //if(parseFloat(duration) <= parseFloat(bufferenEnddd)){
        if(parseFloat(log_id) || parseFloat(buff_log_id)){
            var embed_id = $('#embed_id').val();
            $.ajax({
                type: 'POST',
                url: '/test/embedBufferLog',
                data: {'embed_id': embed_id,'start_time': bufferenStart, 'end_time': bufferenEnddd,'device_id':device_id,'device_type':device_type,'user_id':user_id,buff_log_id : buff_log_id,resolution:resolution,u_id:u_id},
                success: function(res){
                    var obj = JSON.parse(res);
                    buff_log_id = obj.id;
                    u_id = obj.u_id;
                    document.getElementById('u_id').value = u_id;
                    if(buff_log_id){
                        document.getElementById('buff_log_id').value = buff_log_id;
                    }
                }
            });
        }
        //}
    }
}