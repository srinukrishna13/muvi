<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row m-t-40 m-b-40">
    <div class="col-md-8 col-sm-12 m-b-20">
        <div class="Block">
            <form role="form" class="form-inline" action="<?php echo $this->createUrl('/userfeature/Savefeature')?>">
                <div class="form-group">
                    <div class="col-md-12 p-l-0">
                        <div class="checkbox">
                    <label><input type="checkbox" name="rating_status" value="1" <?php echo ($studio->rating_activated == 1)?'checked="checked"':''?> /> <i class="input-helper"></i>Enable Rating</label>
                        </div>
                    </div>
                        </div>
        <button type="submit" class="btn btn-primary waves-effect btn-sm">Save</button>
    </form>   
        </div>
    </div>
   
            <?php if($studio->rating_activated == 1){?>
    <div class="col-md-12">
    <div class="row">
         <div class="col-xs-12">
        <div class="Block">
               
                <div class="Block-Header">
                            <div class="icon-OuterArea--rectangular">
                                <em class="icon-star icon left-icon "></em>
                            </div>
                            <h4>All ratings</h4>
                        
                       
                </div>
             <hr>
        <?php
        if (count($ratings) > 0) {
            $c = 0;
            ?>
            <table class="table" id="example">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Content</th>
                        <th>Name</th>
                        <th data-hide="phone">Email</th>
                        <th data-hide="phone">Rating</th>
                        <th data-hide="phone">Review</th>
                        <th data-hide="phone">Review Time</th>
                        <th data-hide="phone">IP Address</th>
                        <th data-hide="phone">Action</th>
                    </tr>  
                </thead>
                <tbody>
                    <?php
                    if(!empty($ratings)){
                    foreach ($ratings as $rating) {
                        $c++;
                        $content = Film::model()->findByPk($rating->content_id);
                        $content_name = $content->name;    
                        
                        $usr = new SdkUser;
                        $user = $usr->findByPk($rating->user_id);                        
                        ?>
                        <tr>
                            <td><?php echo $c; ?></td>
                            <td><?php echo $content_name; ?></td>
                            <td><?php echo $user->display_name; ?></td>
                            <td><?php echo $user->email; ?></td>
                            <td><?php echo $rating->rating; ?></td>
                            <td><?php echo $rating->review; ?></td>
                            <td><?php echo Yii::app()->common->phpDate($rating->created_date); ?></td>
                            <td><?php echo $rating->user_ip; ?></td>
                            <td>                                
                                <h5><a href="#" onclick="openDeletepopup('<?php echo $rating->id?>'); return false;"><em class="icon-trash"></em>&nbsp;&nbsp;Remove</a></h5> 
                                <?php if($rating->status == 1){?>
                                <h5><a href="#" onclick="ChangeStatuspopup('<?php echo $rating->id?>'); return false;"><em class="icon-ban red"></em>&nbsp;&nbsp;Disable</a> </h5>
                                <?php }else{
                                ?>
                                <h5><a href="#" onclick="ChangeStatuspopup('<?php echo $rating->id?>'); return false;"><em class="icon-check"></em>&nbsp;&nbsp;Enable</a></h5>
                                <?php
                                }?>                                     
                            </td>
                        </tr>

                        <?php
                    }}else{ ?>
                        <tr><td colspan="9"> No data found</td></tr>
                  <?php  }
                    ?>
                </tbody>
            </table>                        
            <?php
        }else{
        ?>
        <p class="error">No Review found.</p>
        <?php
        }
        ?>
    </div>  
    </div>
    </div>
    </div>

<!--Delete Page-->
<div id="openDeletepopup" class="modal fade form-horizontal" role="dialog" aria-hidden="true" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog" style="">
        <form action="<?php echo $this->createUrl('userfeature/deleterating');?>" method="post" id="delrev" enctype="multipart/form-data">
            <input type="hidden" id="deleterating" name="deleterating" value="" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="page_title">Delete Review?</h4>
                </div>
                <div class="modal-body" id="popup_page_content">              
                    Do you really want to delete this review?
                </div>              
                    
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Yes</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>

            </div>
	</form>	
    </div>
</div>

<!--Delete Page-->
<div id="ChangeStatuspopup" class="modal fade form-horizontal" role="dialog" aria-hidden="true" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog" style="">
        <form action="<?php echo $this->createUrl('userfeature/reviewstatus');?>" id="revstatus" method="post" enctype="multipart/form-data">
            <input type="hidden" id="reviewstatus" name="reviewstatus" value="" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="page_title">Change Review Status?</h4>
                </div>
                <div class="modal-body" id="popup_page_content">              
                    Do you really want to change status of this review?
                </div>              
                    
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Yes</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>

            </div>
	</form>	
    </div>
</div>
<?php if (count($ratings) > 0) { ?>
 <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/jsnew/datatable.js"></script>
 <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/jsnew/datatable_custom_paging.js"></script>
<script>

$(document).ready(function() {
$('#example').DataTable({
        "bInfo" : false,
        "bLengthChange": false,
         "iDisplayLength": 10,
         <?php if(count($ratings)< 10){ ?>
         "paging":false,  
         <?php } ?>
        createdRow: function ( row ) {
            $('td', row).attr('tabindex', 0);
        }
    });
});
</script>
<?php } ?>
<script type="text/javascript">
function openDeletepopup(rating){
    $('#deleterating').val(rating);
    //$("#openDeletepopup").modal('show');
    swal({
            title: "Delete Review?",
            text: "Do you really want to delete this review? ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            $('#delrev').submit();
        });
}    
</script>  
<script type="text/javascript">
function ChangeStatuspopup(rating){
    $('#reviewstatus').val(rating);
    //$("#ChangeStatuspopup").modal('show');
    swal({
            title: "Change Status?",
            text: "Do you really want to change status of this review?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            $('#revstatus').submit();
        });
}    
</script>    
<?php }?>

        </div>
    
     

