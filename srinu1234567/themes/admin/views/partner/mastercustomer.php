<?php
$months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
$base_price = $packages['package'][0]['base_price'];
$plan = 'Month';
?>
<input type="hidden" id="plan" value="<?php echo $plan; ?>">
<input type="hidden" id="base_price" value="<?php echo $base_price; ?>">
<script type="text/javascript">
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    var pricingall = '2';
    var packagesall = '1';
    var planall = 'Month';
    var custom_codes = '';
    function hideCards() {
        $('#packagediv').show();
        //showPrice();
        $("#new_card_info").hide();
        $("#terms-div").hide();
        $('#carddetaildiv').hide();
    }
    function showCards() {
        $('#packagediv').hide();
        //showPrice();
        $("#new_card_info").show();
        $("#terms-div").show();
        $('#carddetaildiv').show();
    }
    function validateNewUser() {
        var validate = $("#new_user_form").validate({
            rules: {
                'customer_name': {
                    required: true
                },
                'email': {
                    required: true,
                    mail: true
                },
                'subdomain': {
                    required: true
                },
            },
            messages: {
                'customer_name': {
                    required: "Please enter Name"
                },
                'email': {
                    required: "Please enter email address",
                    //$('#errors').html(msg).show();
                    mail: "Please enter a valid email address"
                },
                'subdomain': {
                    required: "Please enter subdomin"
                },
            }
        });
        var x = validate.form();

        if (x) {
            checkEmailExists(1);
        } else {
            return false;
        }
    }
    function checkEmailExists(arg) {
        $("#nextbutn").html('Wait...');
        $("#nextbutn").attr('disabled', 'disabled');
        var url = "<?php echo Yii::app()->baseUrl; ?>/signup/checkDomainEmail";
        var email = $.trim($('#email').val());
        var wb = $('#subdomain').val();
        var newsubdomain;
        wb = wb.replace('http://', '');
        var wb_parts = wb.split(/[\s.]+/);
        if (wb_parts.length == 1){
            //$('#subdomain').val(wb_parts + '.com');
            newsubdomain = wb_parts + '.com';
        }else{
            newsubdomain = wb;
        }
        $.post(url, {'email': email, 'subdomain': $('#subdomain').val()}, function (res) {
            $("#nextbutn").html('Add Customer');
            $("#nextbutn").removeAttr("disabled");
            if (res.succ) {
                showCards();                
            } else {
                if (res.email_error || res.purchase_error) {
                    $('#email-error').show();
                    $('#email-error').html('This email address has already been used.');
                }
                if (res.domain_error) {
                    $('#subdomain-error').show();
                    $('#subdomain-error').html('Oops! Sorry this domain name has already taken by someone else.');
                }
                if (res.invalid_url) {
                    $('#subdomain-error').show();
                    $('#subdomain-error').html('Please enter a valid domain name.');
                }
            }
        }, 'json');
    }
    function showPrice() {
        var base_price = parseFloat($('#base_price').val());
        var plan = $('#plan').val();
        var price_total = base_price;
        price_total = price_total.toFixed(2);
        $(".charge_now").html(price_total);
        $(".new_charge").html(price_total);
    }    
    function validateForm() {
        $('#card-info-error').hide();
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                payment_info: "required",
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: "required",
                address1: "required",
                city: "required",
                state: "required",
                zipcode: "required",
                terms_of_use: "required",
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                payment_info: "Please select payment information",
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                address1: "Please enter your address",
                city: "Please enter your city",
                state: "Please enter your State",
                zipcode: "Please enter your Zipcode",
                terms_of_use: "Please select terms of use",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            }
        });

        var x = validate.form();

        if (x) {
            $("#packagetext").val(packagesall);
            $("#pricingtext").val(pricingall);
            $("#plantext").val(planall);
            $("#custom_codes").val(custom_codes);
            $('.close').hide();
            $('#nextbtn').html('wait!...');
            $('#nextbtn').attr('disabled', 'disabled');
            $("#loadingPopup").modal('show');
            /*Implement payment*/
            var partnerurl = "<?php echo Yii::app()->baseUrl; ?>/partner/InsertCustomer";
            $("#loadingPopup").modal('show');
            $(".flash-msg").hide();
            var eml = '<?php echo Yii::app()->user->email;?>';
            var chargesmall = 0;
            if(eml=='maxim.manas@gmail.com'){
                chargesmall = 1;
            }
            $.post(partnerurl, {'data': $('#new_user_form').serialize()}, function (dt) {
                if (dt.isSuccess == 'success') {
                    var url = "<?php echo Yii::app()->baseUrl; ?>/payment/authenticateCard";
                    var card_name = $('#card_name').val();
                    var card_number = $('#card_number').val();
                    var exp_month = $('#exp_month').val();
                    var exp_year = $('#exp_year').val();
                    var cvv = $('#security_code').val();
                    var address1 = $('#address1').val();
                    var address2 = $('#address2').val();
                    var city = $('#city').val();
                    var state = $('#state').val();
                    var zip = $('#zipcode').val();
                    var pricing = pricingall;
                    var packages = packagesall;
                    var plan = planall;
                    var cloud_hosting = $.trim($("#cloud_hosting").val());
                    var custom_code = $.trim($("#custom_codes").val());
                    
                    $.post(url, {'chargesmall': chargesmall,'userid': dt.userid, 'studioid': dt.studioid, 'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'address1': address1, 'address2': address2, 'city': city, 'state': state, 'zip': zip, 'packages': packages, 'pricing': pricing, 'plan': plan, 'cloud_hosting': cloud_hosting, 'custom_code': custom_code}, function (data) {
                        $("#loadingPopup").modal('hide');
                        $("#backbtn").hide();
                        if (parseInt(data.isSuccess) === 1) {
                            $("#successPopup").modal('show');
                            setTimeout(function () {
                                //$('#is_payment').val(1);
                                window.location = "<?php echo Yii::app()->baseUrl; ?>/partner/" + dt.relation_type;
                                return false;
                            }, 5000);
                        } else {
                            if($('#is_customer').val()!=1){
                                var meg = '<div class="alert alert-' + dt.isSuccess + ' alert-dismissable flash-msg"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>' + dt.Message + '</div>';
                                $('.pace').before(meg);
                            }
                            $('.close').show();
                            $('#nextbtn').html('Authorize Payment');
                            $('#nextbtn').removeAttr('disabled');
                            $('#is_customer').val(1);
                            $('#newuserid').val(dt.userid);
                            $('#newstudioid').val(dt.studioid);
                            if ($.trim(data.Message)) {
                                $('#card-info-error').show().html(data.Message);
                            } else {
                                $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                            }
                        }
                    }, 'json');
                } else {
                    $("#loadingPopup").modal('hide');
                    var meg = '<div class="alert alert-' + dt.isSuccess + ' alert-dismissable flash-msg"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>' + dt.Message + '</div>';
                    $('.pace').before(meg);
                }
            }, 'json');
        }
    }
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;

        if (curyr === selyr) {
            startindex = curmonth;
        }

        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }
    $(document).ready(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
        showPrice();
    });
</script>
<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #42B970">Thank you, Your subscription has been activated successfully.<br/>Please wait we are redirecting you...</h4>
            </div>
        </div>
    </div>
</div>
<form action="javascript:void(0);" method="post" name="new_user_form" id="new_user_form">
    <input type="hidden" name="is_customer" id="is_customer" value="0">
    <input type="hidden" name="newuserid" id="newuserid" value="">
    <input type="hidden" name="newstudioid" id="newstudioid" value="">
    <h3>New Customer</h3><br /><br />
    <div class="row form-group" id="packagediv">
        <div class="col-lg-12">            
            <div class="form-group" >
                <label class="col-lg-2 toper" for="Customer Name" > Customer Name</label>
                <div class="col-lg-5">
                    <input type="text" class="form-control" name="customer_name" autocomplete="false" required="true"/>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <label class="col-lg-2 toper" for="Email">Email Address</label>
                <div class="col-lg-5">
                    <input type="text" class="form-control  col-lg-5" name="email" id="email" autocomplete="false" required="true"/>
                    <label id="email-error" class="error" for="email"></label>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <label class="col-lg-2 toper" for="subdomain">Website Subdomain</label>
                <div class="col-lg-5">
                    <input type="text" class="form-control  col-lg-5" name="subdomain" id="subdomain" autocomplete="false" required="true"/>
                    <label id="subdomain-error" class="error" for="subdomain"></label>
                </div>
                <?php echo $studio['domain']; ?>
            </div>
            <div class="clearfix"></div>
            <input type="hidden" name="relation_type" value="master" />
            <input type="hidden" name="plan" id="plantext" value="Month" />
            <input type="hidden" name="packages" id="packagetext" value="1" />
            <input type="hidden" name="pricing" id="pricingtext" value="2" />
            <input type="hidden" name="cloud_hosting" id="cloud_hosting" value="" />
            <input type="hidden" name="custom_code" id="custom_codes" value="" />
        </div>
        <div class="col-lg-12">
            <div class="pull-left">
                <h3>Projected Pricing</h3>
            </div>
            <div class="clearfix"></div>
            <div class="row form-group">
                <div class="col-lg-11">
                    <div>
                        Charging to your card now: <label class="control-label-font">
                            $<span class="charge_now"><?php echo $charging_now; ?></span>
                        </label>
                    </div>

                    <div class="monthly_payment">
                        <div>
                            Monthly payment: 
                            <label class="control-label-font">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per month <span class="bandwidth">+ <a href="javascript:void(0);">bandwidth</a></span>
                        </div>
                        <div>
                            Next Billing date: <label class="control-label-font"><?php echo Date('F d, Y', strtotime("+1 Months")); ?></label>
                        </div>
                    </div>

                    <div class="yearly_payment" style="display: none;">
                        <div>
                            Yearly payment: 
                            <label class="control-label-font">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per year <span class="bandwidth">+ <a href="javascript:void(0);">bandwidth</a></span>
                        </div>
                        <div>
                            Next Billing date: <label class="control-label-font"><?php echo Date('F d, Y', strtotime("+1 Years")); ?></label>
                        </div>
                    </div>
                </div>              
            </div>

            <div>
                See <a href="https://muvi.com/pricing" target="_blank">pricing</a> for more detail. Contact your Muvi representative if you have any questions.
            </div>
            <br /><br />
            <button type="button" class="btn btn-primary" onclick="return validateNewUser();" id="nextbutn">Add Customer</button>                

        </div>        
    </div>
</form>
<form method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);">
    <div id="carddetaildiv" style="display: none;">
        <div class="col-lg-12">
            Charging to your card now: <label class="control-label-font">
                $<span class="charge_now"><?php echo $charging_now; ?></span>
            </label>
        </div>
        <div id="new_card_info" style="display: none;">
            <div id="card-info-error" class="error" style="display: none;margin: 0 0 15px 150px;"></div>
            <div class="col-lg-6">

                <div class="row">
                    <div class="col-lg-12">
                        <label class="control-label col-lg-1">Name on Card</label>                    
                        <div class="col-lg-11">
                            <input type="text" class="form-control" id="card_name" name="card_name" placeholder="Enter Name" />
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label class="control-label col-lg-1">Card Number</label>                    
                        <div class="col-lg-11">
                            <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Enter Card Number" />
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label class="control-label col-lg-1">Expiry Date</label>                    
                        <div class="col-lg-11">
                            <div style="float: left;width: 49%;margin-right: 2%;">
                                <select name="exp_month" id="exp_month" class="form-control">
                                    <option value="">Expiry Month</option>	
                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="float: left;width: 49%;">
                                <select name="exp_year" id="exp_year" class="form-control" onchange="getMonthList();">
                                    <option value="">Expiry Year</option>
                                    <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label class="control-label col-lg-1">Security Code</label>                    
                        <div class="col-lg-11">
                            <input type="password" id="" name="" style="display: none;" />
                            <input type="password" class="form-control" id="security_code" name="security_code" placeholder="Enter security code" />
                        </div>
                    </div>                     
                </div>
            </div>  
            <div class="col-lg-6">
                <div class="row form-group">
                    <div class="col-lg-12">
                        <label class="control-label col-lg-1">Billing Address</label>                    
                        <div class="col-lg-11">
                            <input type="text" class="form-control" id="address1" name="address1" placeholder="Address 1" ><br/>
                            <input type="text" class="form-control" id="address2" name="address2" placeholder="Address 2" ><br/>
                            <input type="text" class="form-control" id="city" name="city" placeholder="City"><br/>
                            <div style="float: left;width: 49%;margin-right: 2%;">
                                <input type="text" class="form-control" id="state" name="state" placeholder="State" />
                            </div>
                            <div style="float: left;width: 49%;">
                                <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode" />
                            </div>
                            <div style="clear: both"></div>
                        </div>
                    </div>                     
                </div>
            </div>
        </div>
        <div id="terms-div" style="display: none;">
            <div class="row form-group">
                <div class="col-lg-9">
                    <div class="col-lg-9">
                        <label style="font-weight: normal;">
                            <div class="pull-left" style="margin-right: 5px;margin-top: -2px;">
                                <input type="checkbox" name="terms_of_use" id="terms_of_use">
                            </div>
                            I agree with the <a href="https://muvi.com/signup/signupTerms" target="_blank">Muvi Reseller Terms</a>
                        </label>
                        <div><label id="terms_of_use-error" class="error" for="terms_of_use" style="display: none;"></label></div>
                    </div>
                </div>                     
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-9">
                <label class="col-lg-1">
                    <button type="button" class="btn btn-blue btn-left" id="backbtn" onclick="hideCards();">Back</button>
                </label>
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-blue btn-left" id="nextbtn">Authorize Payment & Add Customer</button>
                </div>
            </div>
        </div>
    </div>
</form>