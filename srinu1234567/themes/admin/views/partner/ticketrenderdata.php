<input type="hidden" class="data-count" value="<?php echo $total_records ?>" /> 
<input type="hidden" id="data-count1" value="<?php echo $count_searched ?>" /> 

    <table id="example1" class="table" >
                    <thead>
                        <tr>
                            <th>Ticket#</th>
                            <th>Store Name</th>
                            <th style="width: 250px;">Title</th>
                            <th id='priority'  class="sortcolumn">
                                Priority
                            </th>
                            <th  id='last_updated_date' onclick="sortby('last_updated_date', 'desc');" class="sortcolumn <?php if ($_GET['sortBy'] == 'last_updated_date_desc' || $_GET['sortBy'] == 'last_updated_date_asc') { ?> sortactive<?php } ?>">

                                <?php
                                if ($sort_by == 'last_updated_date_desc') {
                                  $sort = "fa fa-sort-desc";
                              } elseif ($sort_by == 'last_updated_date_asc') {
                                    $sort = "fa fa-sort-asc";
                                } else {
                                  $sort = "fa fa-sort-desc";
                               }
                               
                                ?>
                                <i id="last_updated_date_i" class="<?php echo $sort ?>"></i> 
                                Last Update
                            </th> 
                            <th  id='eta'  class="sortcolumn center <?php if ($_GET['sortBy'] == 'eta_desc' || $_GET['sortBy'] == 'eta_asc') { ?> sortactive<?php } ?>">
                             ETA
                            </th>
                            
                            <th >Status</th>
                           
                        </tr>	
                    </thead>
                    <tbody>
<?php

foreach ($list as $key => $item) {
    ?>
                            <tr> 
                                <td class="center"><a href="<?php echo Yii::app()->getBaseUrl(true) . "/partner/viewTicket{$uri}/id/{$item['id']}"; ?>"><?php echo $item['id']; ?></a></td>
                                <td>
<?php 
 if($item['portal_user_id']==0) {
$master = new TicketMaster();
$master = $master->findByPk($item['ticket_master_id']);
echo $master->studio_name; 
}else{  
    echo "All Stores";
}
?>          </td>
                                <td style="word-break: inherit;"><a href="<?php echo Yii::app()->getBaseUrl(true) . "/partner/viewTicket{$uri}/id/{$item['id']}"; ?>"><?php
                                    if ($item['title'] != '') {
                                        print strlen($item['title']) > 50 ? substr(stripslashes(nl2br(htmlentities($item['title']))), 0, 50) . "..." : stripslashes(nl2br(htmlentities($item['title']))) ;
                                    } else {
                                        print strlen($item['description']) > 50 ? substr(stripslashes(nl2br(htmlentities($item['title']))), 0, 50) . "..." : stripslashes(nl2br(htmlentities($item['description']))) ;
                                    }
                                    ?>
                                    <p id="sort_grey"> <?php
                                    $note = $this->ticketNotes($item['id'], 'LIMIT 1');
                                    
                                   $pos = strpos($note[0]['note'], 'quoted-printable');
                                    if($pos){
                                    $notes_explode=explode('quoted-printable',$note[0]['note']);
                                    
                                    $final_note=trim($notes_explode[1]);
                                    
                                    }
                                    else{
                                        
                                        $pos1=strpos($note[0]['note'], 'UTF-8');
                                    if($pos1)
                                        {
                                         $notes_explode1=explode('UTF-8',$note[0]['note']);
                                         $final_note=trim($notes_explode1[1]);  
                                        }else
                                        {
                                  $final_note=trim($note[0]['note']);
                                        } 
                                         } 
                       
                                    print !empty($note) ? strlen($final_note) > 60 ? substr(str_replace("\xC2\xA0", " ",wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($final_note)))))), 0, 60) . "..." : substr(str_replace("\xC2\xA0", " ",wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($final_note)))))), 0, 60) : '';
                                    ?></p>
                                    </a></td>
                                <td class='td_priority'><?php echo $item['priority']; ?></td>
                                <td class = 'td_last_updated_date '><?php echo date('M d, h:ia', strtotime($item['last_updated_date'])); ?></td>
                                <td class="center td_eta"><?php print $item['eta'] == '0000-00-00' ? 'N/A' : date('M d, Y', strtotime($item['eta'])); ?></td>
                                <td class="center"><?php echo $item['status']; ?></td>
                                 
                            </tr>  
<?php } ?>
                    </tbody>
                </table>
 
 
    
   