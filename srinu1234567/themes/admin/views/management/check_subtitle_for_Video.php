<div>
    <div class="form-group col-sm-12">
        <b>List of Subtitles already added</b>
    </div>
    <div class="form-group col-sm-12">
        <table class="table" id="video_list_tbl">
            <thead>
                <tr>
                    <th>Language</th>
                    <th>File Name</th>
                    <th class="width">Action</th>
            </tr>
            </thead>
            <tbody>
                <?php
                    if (!empty($getVideoSubtitle)) {
                        foreach ($getVideoSubtitle as $key => $val) {
                            echo "<tr>";
                            echo "<td>".$val['name']."</td>";
                            echo "<td>".$val['filename']."</td>";
                            echo "<td><h5><a href='".$this->createAbsoluteUrl('management/deleteSubtitle',array('subtitleId' => $val['subId']))."' data-msg='Are you sure want to delete the subtitle?' class='confirm'><em class='icon-trash'></em>&nbsp;&nbsp; Delete</a></h5></td>";
                            echo "</tr>";
                        }
                    } else { ?>	
                        <tr>
                            <td colspan="3">No Data Found</td>
                        </tr>
                <?php } ?> 
            </tbody>
        </table>
    </div>
</div>
<input type="hidden" name="subVideoId" value="<?php echo $video_id; ?>" />
<div id='subLangUpload'>
    <div class="form-group col-sm-12">
        <b>Add Subtitle</b>
    </div>
    <div class="form-group">
        <div class="col-sm-6">
            <div class="fg-line">
                <div class="select">
                    <select class="form-control input-sm subtiLang" name="data[subtitle_language][]">
                        <?php foreach ($getSubLang as $getSubLangkey => $getSubLangvalue) { ?>
                            <option value="<?php echo $getSubLangvalue['id']; ?>"><?php echo $getSubLangvalue['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <button type="button" class="btn btn-default-with-bg btn-file btn-sm" onclick="browsesubtitle(this)" >Browse</button>
            <input class="form-control input-sm cost multi-file subtitle_file" type="file" onchange="showfilename(this)" required="required"  name="data[subtitle_file][]" style="display: none;">
            <span>&nbsp;&nbsp;No file chosen</span>
        </div>
    </div>
</div>

<p class="col-sm-12">
    <a href="javascript:void(0);" onclick="addMore('<?php echo $couponType;?>_<?php echo $discountType;?>');" class="text-gray">
        <em class="icon-plus"></em>&nbsp; Add more subtitle
    </a>
</p>
<script>
    function addMore() {
        var isTrue = 0;
        $("#subLangUpload").find(".multi-file").each(function() {
            if ($.trim($(this).val()) !== '') {
                isTrue = 1;
            } else {
                isTrue = 0;
                $(this).focus();return false;
            }
        });
        if (parseInt(isTrue)) {
            $("#subLangUpload").append('<div class="form-group"><div class="col-sm-6"><div class="fg-line"><div class="select"><select class="form-control input-sm subtiLang" name="data[subtitle_language][]"><?php foreach ($getSubLang as $getSubLangkey => $getSubLangvalue) { ?><option value="<?php echo $getSubLangvalue['id']; ?>"><?php echo $getSubLangvalue['name']; ?></option><?php } ?></select></div></div></div><div class="col-sm-6"><button type="button" class="btn btn-default-with-bg btn-file btn-sm" onclick="browsesubtitle(this)">Browse</button><input id="subtitle_file" class="form-control input-sm cost multi-file" onchange="showfilename(this)" type="file" required="required"  name="data[subtitle_file][]" style="display: none;"><span>&nbsp;&nbsp;No file chosen</span></div></div>');
        } 
    }
    function browsesubtitle(browse){
        $(browse).next('input[type=file]').trigger('click');
    }
    function showfilename(browse){
        var filename = $(browse).val();
        filename = "&nbsp;&nbsp;"+filename;
        $(browse).next().html(filename);
    }
    $(function () {
        $("a.confirm").bind('click', function (e) {
            e.preventDefault();
            var location = $(this).attr('href');
            var msg = $(this).attr('data-msg');
            confirmDelete(location, msg);
        });
    });
</script>