<input type="hidden" id="data-count" value="<?php echo $total_video ?>" /> 
<input type="hidden" id="data-count1" value="<?php echo $count_searched ?>" /> 
<input type="hidden" id="page_size" value="<?php echo $page_size?>" /> 
<?php
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getVideoGalleryCloudFrontPath($studio_id);
?>
<table class="table" id="list_tbl">
<thead>
            <tr>
                <th style="width:100px;">
                <div class="checkbox m-b-15">
                    <label>
                        <input type="checkbox" class="sub_chkall" id="check_all" onchange="check_allbox()" >
                        <i class="input-helper"></i>

                    </label>
                </div>
                </th>
                <th>File Name</th>
                <th>Video</th>
                <th data-hide="phone" class="width">Properties</th>
                <th data-hide="phone" class="width">Action</th>
        </tr>
        </thead>
        
        <?php
        // echo "<pre>";
        // print_r($image_details);
        if (!empty($allvideo)) {
            foreach ($allvideo as $key => $val) {
                //  $images_list=explode("public",$val['list_image_name']);
                // $images_cropped=explode("public",$val['image_name']);



                if ($val['thumb_image_name'] == '') {
                    $img_path ="https://d1yjifjuhwl7lc.cloudfront.net/public/no-image-h-thumb.png";
                    $video_remote_url= "https://d1yjifjuhwl7lc.cloudfront.net/public/no-image-h-thumb.png";
                    
                } else {
                    //$img_path = $val['list_image_name'];
                    $img_path = $base_cloud_url . "videogallery-image/" . $val['thumb_image_name'];
                    $video_remote_url = $base_cloud_url . $val['video_name'];
                }
                ?>
        
                <tbody>
                    <tr>
                        <td>
                           <div class="checkbox m-b-15">
                            <label>
                            <input type="checkbox" class="sub_chk" data-id="<?php echo $val['id']; ?>" name="delete_all[]" >
                            <i class="input-helper"></i>
                            
                            </label>
                            </div> 
                        </td>
                        <td>
                            <?php 
                                echo $val['video_name']; 
                                if($val['thumbnail_status']==1){
                                      echo '<br/><small style="color : green;">Generating thumbnails</small>';
                                }
                            ?>
                        </td>

                        <td>
                            <?php if ($val['flag_uploaded'] == 0) { ?>
                                <div class="Box-Container m-b-10"> 
                                   <?php if ($val['thumbnail_images'] !== '') { ?> 
                                        <div class="thumbnail imageSlide "  id="imageSlide" data-slide="true" data-id="<?php echo $val['id'];?>">
                                           <img src="<?php echo $img_path; ?>"  alt="<?php echo "All Video"; ?>" >

                                       </div>
                                    <?php } else { ?>  
                                        <img src="<?php echo $img_path; ?>"  alt="<?php echo "All Video"; ?>" style="widht: 280px; height:160px;" >
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </td>    

                        <td>

                          <?php 

                              $video_prop=json_decode($val['video_properties']); 
                               foreach($video_prop as $k=>$v)

                               {

                                   if($k=='duration'){

                                      $k=$k."(hh:mm:ss.ms)";

                                   }

                                   echo ucfirst(str_replace('_',' ',$k)).":".$v;

                                   echo "<br/>";
                                }

                               if(($val['flag_uploaded'] !=1 ) && ($val['flag_uploaded'] !=2 )){ 

                                   echo "Encoded : "; 

                                    if(($val['movieConverted'] == 1) || ($val['trailerConverted'] == 1)){ 

                                       echo "Yes"; 

                                    } else{ 

                                        echo "No"; 

                                    } 

                                }

                            ?>

                        </td>   



                        <td> 

                            <?php if ($val['flag_uploaded'] == 1) { ?>

                        <p><a title="Video download in progress. It will be available within an hour" data-toggle="tooltip" href="javascript:void(0);"><em class="icon-cloud-download"></em>&nbsp; Download in progress</a></p>

       <?php } else if ($val['flag_uploaded'] == 2) {
            ?>

                       <p data-toggle="tooltip" title="Downloading failed"><em class="fa fa-info-circle" style="color:red;"></em></p>

                   <?php
                    } else {
                        ?>
                        <p>

                            <a href="<?php echo $this->createAbsoluteUrl('management/deletevideo', array('rec_id' => $val['id'], 'No_of_Times_Used' => $val['No_of_Times_Used'])); ?>" data-toggle="tooltip" title="Delete Video" data-msg ="Are you sure to delete the Video ?" class="confirm" id="confirm"><em class="icon-trash" ></em>&nbsp; Delete Video </a>

                       

                        </p>  
                        <p><a href="javascript:void(0);" onclick="openUploadpopup('<?php echo $val['id']; ?>','<?php echo $val['video_name']; ?>');" data-toggle="tooltip" title="<?php if((isset($val['video_subtitle_id'])) && ($val['video_subtitle_id'] != '')){ echo 'Change Subtitle'; } else{ echo 'Add Subtitle'; }?>" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; <?php if((isset($val['video_subtitle_id'])) && ($val['video_subtitle_id'] != '')){ echo 'Change Subtitle'; } else{ echo 'Add Subtitle'; }?></a></p>



       <?php } ?>
                </td>
                </tr>	
                </tbody>
       
                <?php }
            } else { ?>					
            <tbody>
                <tr>
                    <td colspan="4">No Video Found</td>
                </tr>
            </tbody>
<?php } ?> 
 </table>

<script>
    $(document).ready(function () {
    $("#check_all").click(function () {
        $(".sub_chk").prop('checked', $(this).prop('checked'));
    });
});
$(function () {
        $("a.confirm").bind('click', function (e) {
            e.preventDefault();
            var location = $(this).attr('href');
            var msg = $(this).attr('data-msg');
            confirmDelete(location, msg);
        });
    });
    var change="";
$(function() {
        $(".imageSlide").mouseenter(function() {
            console.log('enter');
            var obj= this;
            var data_slide= $(obj).attr('data-slide');
            var id = $(obj).attr('data-id');
            if(data_slide){
            $.post('/management/ImageThumbnail',{id: id},function(res){
            if(res){
                $.each(res, function(key, value){
                    $(obj).append("<img src='"+value+"' class='hide' />");
                });
                clearInterval(change);
                change = setInterval(function () {
                    var img = $(obj).find("img:not(.hide)");
                    img.addClass("hide");
                    var nextImg = "";
                    if( img[0] === $(obj).find("img").last()[0] ){
                        nextImg = $(obj).find("img").first();
                    } else{
                        nextImg = img.next("img");
                    }
                    nextImg.removeClass("hide").addClass("show");
                }, 2000);
                    };
                   $(obj).attr('data-slide',false); 
            },'json');
            }
        });
        $('.imageSlide').mouseleave(function() {
            console.log("out");
            clearInterval(change);
            $(this).find("img:not(:first)").remove();
            $(this).find('img').first().removeClass('hide');
        });
    });
</script>
<style>
    .thumbnail {
        width : 280px;
        height: 160px;
    }
    .thumbnail  > img {
        height: 100%;
    }
    .hide {
        display: none !important;
    }

</style>
