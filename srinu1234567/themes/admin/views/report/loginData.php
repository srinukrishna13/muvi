<input type="hidden" class="data-count" value="<?php echo $logins['count']?>" />
<thead>
    <th>#</th>
    <th>User</th>
    <th data-hide="phone">Login At</th>
    <th data-hide="phone">Logout At</th>
    <th data-hide="phone">IP</th>
</thead>
<tbody class="list">
    <?php 
    if($logins['count']){
        if(isset($page) && $page){
            $cnt = ($page- 1) * $page_size + 1;
        }else{
            $cnt = 1;
        }
    foreach($logins['data'] as $login){?>
        <tr>
            <td><?php echo $cnt++;?></td>
            <td class="user"><a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $login['user_id'];?>');"><?php echo $login['email']?$login['email']:$login['display_name'];?></a></td>
            <td><?php echo $login['login_at'];?></td>
            <td><?php echo $login['logout_at'];?></td>
            <td><?php echo $login['ip'];?></td>
        </tr>

    <?php }}else{?>
        <tr>
            <td colspan="8">No Record found!!!</td>
        </tr>	
    <?php	}?>
</tbody>