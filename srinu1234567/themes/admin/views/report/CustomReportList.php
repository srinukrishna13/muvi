<div class="row m-b-10">
    <form class="form-horizontal" role="form" name="frmcustom_report" method="post" id="frmcustom_report">
      <div class="col-xs-12 m-b-20">
          <div class="col-md-2" style="padding-left: 0px;width: 8.667%;">
        <button type="button" class="btn btn-primary m-b-40 m-t-20" onclick="addNewCustomReport(this);" data-id="">Add Report</button>
    </div>
             <div class="col-xs-4 m-t-10" >
        
       <?php  if(!empty($customReport_data)){ ?>
            <button onclick="delete_selected_image()" id="delete_image" type="button" class="btn btn-danger m-t-10">
                Delete Selected
            </button>
<?php } ?>
    </div>
         <div class="Block m-t-40 ">
            <table class="table">
             <thead>
          <tr>
              <th><div class="checkbox m-b-15">
                  <?php  if(!empty($customReport_data)){ ?>
                  <label>
                        <input type="checkbox" class="sub_chkall" id="check_all"  >
                        <i class="input-helper"></i>

                  </label> <?php } ?></div> </th>    
          <th>Report Name</th>
          <th>Report Template</th>
          <!--<th>Assigned To</th>-->
          <th>Action</th>
          </tr>
             </thead>
             <tbody>
                  <?php
            $i=1;
            if(!empty($customReport_data)){
            foreach($customReport_data as $customReport){  ?>
              <tr>
                  <td><div class="checkbox m-b-15">
                            <label>
                            <input type="checkbox" class="sub_chk" data-id="<?php echo $customReport['id']; ?>" name="delete_all[]" >
                            <i class="input-helper"></i>
                            
                            </label>
                            </div> </td>   
              <td><label for="report_title"><a href="<?php echo Yii::app()->baseUrl; ?>/report/ShowCustomreport/Report/<?php echo $customReport['id']?>"><?php echo $customReport['report_title']; ?></a></label> 
</td>
              <td><?php if($customReport['report_type']==1){ echo 'User';} else { echo 'Content';} ?></td>
              <?php /*<td><?php
              if(!empty($customReport['partner_id'])){
              $arr=explode(',',$customReport['partner_id']);
              for($i=0;$i<count($arr);$i++){
                  $partners_name= CustomReport::model()->getPartnerName($arr[$i]);
                  if($i<=2){
                  echo $partners_name['first_name'];
                  echo "<br>";
                  }
                  else{
                  $more=1;
                  }
              }
             
              }
              else{
                echo 'N/A';  
              }
              ?>
                  <?php
              if(!empty($customReport['partner_id'])){?>
              <button type = " button" class = " btn btn-default" data-toggle = "tooltip" 
                      data-placement = "right" title = " <?php  for($i=3;$i<count($arr);$i++){
                      $partners_name= CustomReport::model()->getPartnerName($arr[$i]);
                       echo $partners_name['first_name'].',';
                      }
                  ?>" style="padding:8px 0px;">
   <?php  if($more==1 && ((count($arr)-3)>0)){echo '<a  href="javascript:void(0)">'.(count($arr)-3) . ' MORE</a>';} ?>
</button>
                  <script>
   $(function () { $("[data-toggle = 'tooltip']").tooltip(); });
</script>
              <?php } ?>
              
              </td><?php */ ?>
              <td><h5><a href="<?php echo Yii::app()->baseUrl; ?>/report/CustomReportEdit/Report/<?php echo $customReport['id']?>"><em class="icon-pencil"></em>&nbsp;&nbsp; Edit</a></h5><h5><a id="delete_<?php echo $customReport['id']; ?>" onclick="delete_image(<?php echo $customReport['id'] ?>)" style="cursor:pointer;"><em class="icon-trash"></em>&nbsp;&nbsp;Delete</a></h5>
              <?php  if(!empty($customReport['partner_id'])){ ?><h5><a data-id="<?php echo $customReport['report_title'];?>" data-reportid="<?php echo $customReport['id'];?>" onclick="addEditCategory(this);" href="javascript:void(0)" class=""><em class="fa fa-plus-circle"></em>&nbsp;&nbsp; Manage Assignment</a> </h5><?php } ?>
              </td>
              </tr> 
            <?php $i++; }} else{ ?>
               <tr> 
                   <td colspan="3"> No Record Found..</td>
             
               </tr> 
            <?php } ?>
             </tbody>
            </table>
            
         </div>
      </div>
   </form>
</div>
<div class="modal fade" id="ppvModale" role="dialog" data-backdrop="static" data-keyboard="false" >
</div>
<!-- modal start here -->
<div class="modal fade" id="ppvModal" role="dialog" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal  ppv_modal" name="ppv_modal" id="ppv_modal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_ppv" name="id_ppv" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="ppvbtn" class="btn btn-default">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end here -->

<div class="modal fade" id="ppvpopup" role="dialog" data-backdrop="static" data-keyboard="false" ></div>
<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/autosize.min.js"></script>


<!--scripts-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-sortable.js"></script>
<script>
function delete_image(rec_id)
{
    $("#delete_"+rec_id).after("<img src='"+HTTP_ROOT+"/images/loader.gif' height='20px' class='ajax-loader'/>");
   swal({
                                title: "Delete Custom Report?",
                                text:"Are you sure to delete the Custom Report ?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true,
                                html:true
                            }, function(isConfirm) { 
                                
                               // console.log("inside");
                               if(isConfirm){
                               delete_singleimage(rec_id);
                           }
                           else
                           {
                            $(".ajax-loader").hide();   
                           }
                               
                                }); 
}

function delete_singleimage(rec_id)
{
                         $.ajax({  
                                url:'<?php echo Yii::app()->getBaseUrl(true); ?>/report/DeleteCustomRepoprt',  
                                method:'POST',  
                                data:{rec_id:rec_id},  
                               success:function(res)  
                                 {                                 
                                  if(res){
                                  
                            swal({
  title: "Custom report Deleted Successfully!",
  timer: 2000,
  showConfirmButton: false
});    
                                    window.location.reload();
                                  }else
                                  {
                                   swal("Image already in use");   
                                  }
                                }
                            });   
}

$(document).ready(function(){
$("#check_all").click(function () {
   
        $(".sub_chk").prop('checked', $(this).prop('checked'));
    });
});
    
    
   function addNewCustomReport()
   {
      window.location.href ="<?php echo Yii::app()->baseUrl; ?>/report/AddNewCustomreport";  
   }
    function addEditCategory(obj) {
        var report_title = $(obj).attr('data-id');
        var type = $(obj).attr('data-reportid');
       $('.loader').show();
        $.post("<?php echo Yii::app()->baseUrl; ?>/report/PartnerAssign",{'report_title' : report_title,'type' : type},function(res){
            //$("#ppvModal").modal('hide');
            $('.loader').hide();
            $("#ppvpopup").html(res).modal('show');
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });

            $('.cost').on("contextmenu",function(event) {
                return false;
            });
            
             var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/report/AvailablePartners", filter: function (e) {
                return e;
            }}});
            content.clearPrefetchCache(),
            content.initialize(),
            $("#content").tagsinput({
                itemValue: function(item) {
                    return item.content_id;
                },
                itemText: function(item) {
                    return item.name;
                },
                typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});
            
            var contnts = $.trim(sel_contents) ? jQuery.parseJSON(sel_contents) : '';
            if (contnts.length) {
                for (var i in contnts) {
                    if (parseInt(contnts[i].content_id)) {
                        $("#content").tagsinput('add', { "content_id": contnts[i].content_id , "name": contnts[i].name});
                    }
                }
            }
            
            $(".bootstrap-tagsinput ").removeClass('col-md-8');
            
            if ($('.auto-size')[0]) {
                autosize($('.auto-size'));
            }
            
            
            
        });
    }
  function delete_selected_image()
{
    
    var allVals = [];  
        $(".sub_chk:checked").each(function() {  
            allVals.push($(this).attr('data-id'));
        });  
        //alert(allVals.length); return false;  
        if(allVals.length <=0)  
        {  
            swal({
  title: "Please Select Custom Report to Delete!",
  timer: 2000,
  showConfirmButton: false
});     
            return false;  
        }  
        else {            
            //show alert as per the videos mapped status
          //check_videomapped(allVals); 
         delete_selected_images(allVals);
       }
 }
 
 function delete_selected_images(allVals)
 {
     //alert(allVals);
                            swal({
                                title: "Delete Custom Report?",
                                text:"Are you sure to delete "+allVals.length+" custom Report?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true,
                                html:true
                            }, function() { 
                                
                               // console.log("inside");
                               delete_images(allVals);
                                });
                                
 }
 function delete_images(allVals)
 {
     $('#delete_btn').after("<img src='"+HTTP_ROOT+"/images/loader.gif' height='20px' class='ajax-loader'/>");
     $("#delete_image").attr("disabled","disabled");
     $('.ajax-loader').show();
    //     var a = $('#page-selection ul.bootpag li.prev').data('lp');
    //     alert(a);
    //     return;
     var curent_active_page =  $('#page-selection ul.bootpag li.active').data('lp');
     var prev_page =  $('#page-selection ul.bootpag li.prev').data('lp');
     var searchText = $.trim($(".search").val());    
     var imagecount_activepage=$("#image_list_tbl > tbody > tr").length;
//     alert(imagecount_activepage);
//     return;
    console.log("Total_count"+imagecount_activepage);
    console.log("active_page"+curent_active_page);
    console.log("prev_page"+prev_page);
    
        if(allVals.length<imagecount_activepage)
       {
        var page=curent_active_page;          
       }
       else
       {
        var page=prev_page;           
       }
       console.log("page"+page);    
          $.ajax({  
                                url:'<?php echo Yii::app()->getBaseUrl(true); ?>/report/DeleteSelectedImages',  
                                method:'POST',  
                                data:{id:allVals},  
                               success:function()  
                                 {  
                                    $('.ajax-loader').remove();
                                     $("#delete_image").attr("disabled",false);
//                                  window.location.reload();
                                    //getimageDetails(searchText,page);
                                    swal({
  title: "Custom report Deleted Successfully!",
  timer: 2000,
  showConfirmButton: false
});    
                                    window.location.reload();
                                }
                            });    
 }
 
</script>