<style>
  .comiseo-daterangepicker-triggerbutton {
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #d3d3d3;
    border-top:0px !important;
    border-left:0px !important; 
    border-right:0px !important;
    border-radius: 0px !important;
    color: #333;
    font-weight: normal;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #d3d3d3;
    color: #333;
    font-weight: normal;
}    
</style>
<?php
    $total = $results['count']/$page_size;
    $maxVisible = $total/2;
    if($maxVisible < 5){
        $maxVisible = $total;
    }
    
    $loginTotal = $logins['count']/$page_size;
    $loginMaxVisible = $loginLotal/2;
    if($loginMaxVisible < 5){
        $loginMaxVisible = $loginTotal;
    }
?>

<div class="row m-b-40">
    <div class="col-xs-12">
        <button class="btn btn-primary primary report-dd m-t-10" value="csv">Download CSV</button>
    </div>
</div>
<input type="hidden" id="page_size" value="<?php echo $page_size?>" />
<div class="row">
    
        
        <div class="col-md-4">
            <div class="row">
            <div class="col-md-12">
              <div class="input-group">
                <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                <div class="fg-line">
                    <div class="select">
                    <input class="form-control" type="text" id="search_date" name="searchForm[revenue_date]" value='<?php echo @$dateRange;?>' />
                    </div>
                </div>
              </div>
            </div>
       
    <div class="col-md-12">
        <table class="table table-hover" id="totallogin" style="display:none;">
                    <tbody>
                        <tr>
                            <td>Logins</td>
                            <td id="login-count"><?php echo @$logins['count'];?></td>
                        </tr>
                    </tbody>
                </table>
         <table class="table table-hover" id="totalview">
                    <tbody>
                        <tr>
                            <td>Total views so far</td>
                            <td id="login-count"><?php echo $totalsviewcount;?></td>
                        </tr>
                         <tr>
                            <td>Total views last month</td>
                            <td id="login-count"><?php echo $totalsviewcountlasthour;?></td>
                        </tr>
                         <tr>
                            <td>Total hour watched so far</td>
                            <td id="login-count"><?php echo $this->timeFormat($totalshourscount);?></td>
                        </tr>
                         <tr>
                            <td>Total hour watched last month</td>
                            <td id="login-count"><?php echo $this->timeFormat($totalsdurationcounthour);?></td>
                        </tr>
                    </tbody>
                </table>
        
        
    </div>
            </div>
             </div>
    <div class="col-md-8 login-details">
                <div id="loginchart" style="display:none;"></div>
                <div id="viewchart"></div>
            </div>
</div>
<div class="row">
    <div class="col-md-12 m-t-40">
         <div class="tabpanel">
        <ul class="nav nav-tabs">
            <li class="active" onclick="displayview()"><a href="#tab1default" data-toggle="tab">Views</a></li>
            <li ><a href="#tab2default" data-toggle="tab">Searches</a></li>
            <li onclick="displaylogin()"><a href="#tab3default" data-toggle="tab">Logins</a></li>
            
        </ul>
        
        <div class="panel-body p-l-0 p-r-0">
                <div class="tab-content">
                    
                     <div class="tab-pane fade in active" id="tab1default">
                        <div class="row">
                            <div class="col-md-4 pull-left">
                                <div class="form-group input-group">
                                    <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                    <div class="fg-line">
                                        <input  id="search-view" class="form-control input-sm search" placeholder="Search User" />
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-12 table-responsive table-responsive-tabletAbove">
                            <table class="table tablesorter" id="view-content">

                            </table>
                            </div>
                            <div class="page-selection-div">
                                <div id="page-selection-view" class="pull-right"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="tab2default">
                        <div class="row">
                            <div class="col-md-4 pull-left">
                                <div class="form-group input-group">
                                    <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                    <div class="fg-line">
                                       <input  id="search-search" class="form-control input-sm search" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <table class="table tablesorter" id="content">
                            </table>
                            <div class="page-selection-div">
                                <div id="page-selection" class="pull-right"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab3default">
                        <div class="row">
                            <div class="col-md-4 pull-left">
                                <div class="form-group input-group">
                                    <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                    <div class="fg-line">
                                        <input  id="search-login" class="form-control input-sm search" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <table class="table tablesorter" id="login-content">

                            </table>
                            <div class="page-selection-div">
                                <div id="page-selection-login" class="pull-right"></div>
                            </div>
                        </div>
                    </div>
                     
                </div>
                </div>
           <div class="loader text-center" style="display:none">
        <div class="preloader pls-blue">
            <svg viewBox="25 25 50 50" class="pl-circular">
                <circle r="20" cy="50" cx="50" class="plc-path"/>
            </svg>
        </div>
    </div>
    </div> 
    </div>
</div>
<div class="h-40"></div>

<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="user-profile">
        </div>
    </div>
</div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/list.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<!--link rel="stylesheet" href="<?php echo ASSETS_URL;?>css/jquery.bootpag.min.css"-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/reports.js"></script>
<script>
    function displaylogin()
    {
    $("#loginchart").show();
    $("#totallogin").show();
    $("#viewchart").hide();
    $("#totalview").hide();
    }
     function displayview()
    {
    $("#loginchart").hide();
    $("#totallogin").hide();
    $("#viewchart").show();
    $("#totalview").show();
    }
    $(function() {
        loadDateRangePicker('search_date','<?php echo $lunchDate;?>');
        $('.page').each(function(){
            $(this).children('a').removeAttr('href');
        });
    });
    $('#search_date').change(function(){
        var date_range = $('#search_date').val();
        var searchTextSearch = $.trim($("#search-search").val());
        var searchTextLogin = $.trim($("#search-login").val());
        var searchTextView = $.trim($("#search-view").val());
        getUserActionDetails(date_range,searchTextSearch,searchTextLogin,searchTextView);
    });
    
    $(document.body).on('keydown','#search-search',function (event){
        var searchTextSearch = $.trim($("#search-search").val());
        var searchTextLogin = $.trim($("#search-login").val());
        var searchTextView = $.trim($("#search-view").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextSearch.length > 2 || searchTextSearch.length <= 0)){
            var date_range = $('#search_date').val();
            getUserActionDetails(date_range,searchTextSearch,searchTextLogin,searchTextView);
        }
    });
    $(document.body).on('keydown','#search-login',function (event){
        var searchTextSearch = $.trim($("#search-search").val());
        var searchTextLogin = $.trim($("#search-login").val());
        var searchTextView = $.trim($("#search-view").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextLogin.length > 2 || searchTextLogin.length <= 0)){
            var date_range = $('#search_date').val();
            getUserActionDetails(date_range,searchTextSearch,searchTextLogin,searchTextView);
        }
    });
    
    $(document.body).on('keydown','#search-view',function (event){
        var searchTextSearch = $.trim($("#search-search").val());
        var searchTextLogin = $.trim($("#search-login").val());
         var searchTextView = $.trim($("#search-view").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextView.length > 2 || searchTextView.length <= 0)){
            var date_range = $('#search_date').val();
            getUserActionDetails(date_range,searchTextSearch,searchTextLogin,searchTextView);
        }
    });
    
    
    $('.report-dd').click(function(){
        var date_range = $('#search_date').val();
        var searchTextSearch = $.trim($("#search-search").val());
        var searchTextLogin = $.trim($("#search-login").val());
        var searchTextView = $.trim($("#search-view").val());
        var type = $(this).val();
        if(type == 'csv'){
            window.location = '<?php echo Yii::app()->baseUrl."/report/getUsagesReport?dt=";?>'+date_range+'&type='+type+'&search_value_search='+searchTextSearch+'&search_value_login='+searchTextLogin+'&search_value_view='+searchTextView;
        }
    });
</script>
<script>
    $(function () {
        $('#loginchart').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: <?php echo $xdata?>
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'User Logins'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                enabled:false
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: <?php echo $graphData;?>,
            credits: {
                enabled: false
            },
            exporting: { 
                enabled: false
            }
        });
    });
</script>
<script>
    $(function () {
        $('#viewchart').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: <?php echo $xdataview?>
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Views'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                enabled:false
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: <?php echo $graphDataview;?>,
            credits: {
                enabled: false
            },
            exporting: { 
                enabled: false
            }
        });
    });
</script>