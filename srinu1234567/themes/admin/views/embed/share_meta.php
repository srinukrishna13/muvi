    <?php $og_url=Yii::app()->getbaseUrl(true).$_SERVER["REQUEST_URI"];?>
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="<?php echo @$og_url;?>" />
    <meta property="og:site_name" content="<?php echo Yii::app()->getbaseUrl(true);?>"/>
    <meta property="og:title" content="<?php echo @$page_title;?> | <?php echo @$studio_name;?>" />
    <meta property="og:description" content="<?php echo CHtml::encode(@$page_desc);?>" />
    <meta property="og:image" content="<?php echo @$item_poster;?>"/>
    <meta property="og:image:secure_url" content="<?php echo @$item_poster;?>"/>
    <?php if($og_video){        
        /*if(HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95'){
            $flash_player_url="https://www.muvi.com/mediaplayer.swf";
        }else{
            $flash_player_url=Yii::app()->getbaseUrl(true).'/mediaplayer.swf';
        }*/
        ?>
        <!--<meta property="og:video" content="<?php echo $flash_player_url;?>?file=<?php echo @$share_fullmovie_path;?>&autostart=true"/>
        <meta property="og:video:secure_url" content="<?php echo $flash_player_url;?>?file=<?php echo @$share_fullmovie_path;?>&autostart=true" />      
        <meta property="og:video:type" content="application/x-shockwave-flash" /> -->  
    <?php } ?>

