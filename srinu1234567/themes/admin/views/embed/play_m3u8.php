<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title> Muvi.com</title>
        
        <?php if(!isset($_REQUEST['isApp'])){ 
             $this->renderPartial('share_meta',array('page_title'=>$page_title,'studio_name'=>$this->studio->name,'page_desc'=>$page_desc,'item_poster'=>$item_poster,'og_video' =>$og_video));             
        } ?> 
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>  
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
        }</script>
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />        

        <link rel="stylesheet" href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video-js.min.css">
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video.min.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/hls.js/dist/hls.min.js"></script>        
        <script type='text/javascript'>
        // video js error message
            var you_aborted_the_media_playback ="<?php echo $this->Language["you_aborted_the_media_playback"]; ?>";
            var a_network_error_caused_the ="<?php echo $this->Language["a_network_error_caused_the"]; ?>";
            var the_media_playback_was_aborted ="<?php echo $this->Language["the_media_playback_was_aborted"]; ?>";
            var the_media_could_not_be_loaded ="<?php echo $this->Language["the_media_could_not_be_loaded"]; ?>";
            var the_media_is_encrypted ="<?php echo $this->Language["the_media_is_encrypted"]; ?>";
            var no_compatible_source_was_found ="<?php echo $this->Language["no_compatible_source_was_found"];?>";

        </script>
      <style type="text/css">
          .vjs-fullscreen{padding-top:0}
          .RDVideoHelper{display:none}
            video::-webkit-media-controls{
                display:none!important
            }
            video::-webkit-media-controls-panel{
                display:none!important
            }
          .nojs { display: block;}
            body {
                background-color: #1E1E1E;;
                color: #fff;
                font: 12px Roboto,Arial,sans-serif;
                height: 100%;
                margin: 0;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 100%;
            }	
            #play{
                display: none;
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                -webkit-animation: fadeinout .5s linear forwards;
                animation: fadeinout .5s linear forwards;
            }


            @-webkit-keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform: 5%; }
                50% { transform: scale(1.4); }
            }

            @keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform:  5%; }
                50% { transform: scale(1.4); }
            }
            #pause{
                display: none;
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                -webkit-animation: fadeinout .5s linear forwards;
                animation: fadeinout .5s linear forwards;
            }
            @-webkit-keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform: 5%; }
                50% { transform: scale(1.4); }
            }

            @keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform:  5%; }
                50% { transform: scale(1.4); }
            }
             #pause_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none;
            }
            #play_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none; 
            }
            .play-btn{
                background: url("<?php echo Yii::app()->theme->baseUrl;?>/img/black_play_icon.png") no-repeat scroll 0 0;
                bottom: 0;
                height: 60px;
                left: 0;
                margin: auto;
                position: absolute;
                right: 0;
                top: 0;
                width: 60px;
                cursor: pointer;
            }
            .vjs-big-play-button{display: none !important;visibility: hidden;};
      </style>
    </head>
   <body>
   <div class="videocontent nojs" style="overflow:hidden;">
            <video id="video_player" class="video-js vjs-default-skin vjs-16-9" webkit-playsinline="true"  fluid = 'true' controls="false"
                  <?php
                   if (!isset($_REQUEST['isApp']) || (isset($_REQUEST['isApp']) && $_REQUEST['isApp'] == 2)) {
                       echo "poster=none";
                   } if (isset($_REQUEST['isApp'])) {
                       ?> autoplay ="true" preload="auto" <?php } else { ?> preload="auto" <?php } ?> 
    >
    <source src="<?php echo $thirdparty_url; ?>" type="application/x-mpegURL">
    </video>
    </div> 
        <center>
            <div id='play-btn' class="play-btn" <?php if(isset($_REQUEST['isApp'])){ ?> style='display:none;' <?php } ?> ></div>    
        </center>
    <script>
    var player = "";
    main()
    function main()
    {
                var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
                var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";
                var setup = {debug: true, }
                var player = videojs('video_player', setup).ready(function () {
                    $('#video_player').append('<img src="/images/touchpause.png" id="pause_touch" />');
                    $('#video_player').append('<img src="/images/touchplay.png" id="play_touch" />');
                    $('#video_player').append('<img src="/images/pause.png" id="pause"/>');
                    $('#video_player').append('<img src="/images/play-button.png" id="play" />');
                    //player.play();
                   <?php if(!isset($_REQUEST['isApp'])){ ?>
                   // autoplay=false;                   
                    $('#video_player,.play-btn').hover(function(){
                        $('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl;?>/img/red_play_icon.jpg")');
                    },function(){
                        $('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl;?>/img/black_play_icon.png")');
                    });
                   <?php } ?>
                   <?php if(!isset($_REQUEST['isApp'])){?>
                        $('#play-btn').on('click',function(){
							player.play();
                            $('#play-btn').remove(); 
                        });                        
            <?php } ?>
                   
            <?php if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 2)) { ?>
            var thisIframesHeight = $( window ).height()-20;
            <?php if(@$_REQUEST['device_type'] == 3){ ?>
            var thisIframesHeight = $( window ).height();
            <?php } ?>
            $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px; width:100%;');
            $(".vjs-control-bar").show();
            $('.vjs-control-bar').css('z-index','2147483647');
            $(".vjs-captions-button").hide();
            $(window).on('resize',function( event ) {
                var thisIframesHeight = $(window).height()-20;
                <?php if(@$_REQUEST['device_type'] == 3){ ?>
                var thisIframesHeight = $( window ).height();
                <?php } ?>
                $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px; width:100%;');
            }).trigger('resize');
            <?php } else { ?>
                        var thisIframesHeight = $(window).height();
                        $(".video-js").attr('style', 'padding-top:' + thisIframesHeight + 'px; width:100%;');
                        //$( ".video-js" ).attr('style','padding-top:48%; height:50%; width:100%;');
            <?php } ?> 
            //Poster Fix ...@avi
            $('.vjs-poster').css({
            'background-image': 'url('+"<?php echo $item_poster ;?>"+')',
            'display': 'block'
            });
            player.on('timeupdate',function(){
                if(player.currentTime()>0)
                {
                    $(".vjs-poster").removeAttr('style');
                }
            });
            <?php
            if(@$_REQUEST['device_type'] == 3){ ?>
                player.on("loadeddata", function () {
                player.play();
                });
            <?php } ?>
                    
                <?php if (!isset($_REQUEST['isApp'])) { ?>
                            if (is_mobile === 0) {
                                $('#video_player_html5_api').on('click', function (e) {
                                    if (player.paused()) {
                                        $('#pause').hide();
                                        $('#play').show();
                                    } else {
                                        $('#play').hide();
                                        $('#pause').show();
                                    }
                                });
                            } else {
                                $('#pause_touch').bind('touchstart', function (e) {
                                player.pause();
                                $('#play_touch').show();
                                $('#pause_touch').hide();
                            });
                            $('#play_touch').bind('touchstart', function (e) {
                                player.play();
                                setTimeout(function () {
                                    $('#play_touch').hide();
                                }, 500);
                            });
                            $('#video_player_html5_api').bind('touchstart', function (e) {
                                if (player.play()) {
                                    $('#pause_touch').show();
                                    $('#play_touch').hide();
                                    setTimeout(function () {
                                        $('#pause_touch').hide();
                                    }, 3000);
                                }
                            });
                        }
            <?php } ?>
                player.on('error', function () {
                    if($('.vjs-modal-dialog-content').html()!==''){
                          $(".vjs-modal-dialog-content").html("<div>"+no_compatible_source_was_found+"</div>");
                        } else {
                        if(document.getElementsByTagName("video")[0].error != null){
                            var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                            if (videoErrorCode === 2) {
                                    $(".vjs-error-display").html("<div>"+a_network_error_caused_the+"</div>");
                            } else if (videoErrorCode === 3) {
                                $(".vjs-error-display").html("<div>"+the_media_playback_was_aborted+"</div>");
                            } else if (videoErrorCode === 1) {
                                 $(".vjs-error-display").html("<div>"+you_aborted_the_media_playback+"</div>");
                            } else if (videoErrorCode === 4) {
                                  $(".vjs-error-display").html("<div>"+the_media_could_not_be_loaded+"</div>");
                            } else if (videoErrorCode === 5) {
                                $(".vjs-error-display").html("<div>"+the_media_is_encrypted+"</div>");
                            }
                        }
                        }
                });
                });
            }
    </script>
   </body>
</html>
<!--//https://s3.amazonaws.com/_bc_dml/example-content/bipbop-advanced/bipbop_16x9_variant.m3u8-->
<!-- <video id="video_player" class="video-js vjs-default-skin" controls preload="auto" width=960 height=400 data-setup='{ "autoplay": true, "techOrder": ["flash"] }'> -->