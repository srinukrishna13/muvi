<?php
$lat = $latitude;
$lng = $longitude;

?>
    <div class="indicator text-center m-t-20 m-b-40" id="servermap">
        <div id="map" style="height:155px;width: 100%;"></div>
    </div>
    <div class="indicator-Desc">
        <h5 class="text-capitalise f-500">Server</h5>
<!--        <div class="info-block">
            <p>
                <span class="grey p-l-20">
                    <em class="fa fa-square icon left-icon blue"></em>
                    Server Location :<?php echo $address; ?>
                </span>
            </p>
        </div>-->
        <div class="info-block">

            <p>
                <span class="grey p-l-20">
                    <em class="fa fa-square icon left-icon blue"></em>
                    Storage :<span id="storageaddr"><?php echo $address; ?></span>
                </span>
            </p>
        </div>
    </div>



<style>
    .toggle-editor, #address_switch {
        display: none;
    }
    .gm-style-cc { display:none; }
    img[src="https://maps.gstatic.com/mapfiles/api-3/images/google4.png"]{display:none}
</style>
<script async defer src="https://maps.google.com/maps/api/js?sensor=false&async=2&callback=GoogleMapsLoaded"></script>
<script type="text/javascript">
    function GoogleMapsLoaded() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 3,
            center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker1 = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
            icon:'<?php echo Yii::app()->getBaseUrl(true) ?>/images/servericon.png'
        });
    }
</script>
