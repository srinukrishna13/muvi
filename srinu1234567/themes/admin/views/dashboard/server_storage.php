<style>
    .toggle-editor, #address_switch {
        display: none;
    }
    .gm-style-cc { display:none; }
    img[src="https://maps.gstatic.com/mapfiles/api-3/images/google4.png"]{display:none}
</style>
<script async defer type="text/javascript" src="https://maps.google.com/maps/api/js?async=2&callback=GoogleMapsLoaded"></script>
<div class="col-md-6 col-lg-6 m-b-40">
    <div class="indicator text-center m-t-20 m-b-40" id="servermap">
        <div id="map" style="height:200px;width: 315px;"></div>
    </div>
    <div class="indicator-Desc">
        <h5 class="text-capitalise f-500">Servers</h5>
        <div class="info-block">
            <ul style="padding-left: 20px;">
                <li>Location: <?php echo $server['loc']; ?></li>
                <li>Server: 
                    <?php if($server['type']==1){?>
                        Dedicated.
                    <?php }else{?>
                        Shared. <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/ticket/addTicket">Upgrade to Dedicated <br>Server for performance and reliability</a>
                    <?php }?>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-6 col-lg-6 m-b-40">
    <div class="indicator text-center m-t-20 m-b-40">
        <div id="map1" style="height:200px;width: 315px;"></div>
    </div>
    <div class="indicator-Desc">
        <h5 class="text-capitalise f-500">Secure, Cloud Storage</h5>
        <div class="info-block">
            <ul style="padding-left: 20px;">
                <li>Storage Location: <?php echo $address; ?>
                <li>Storage Used: <?php echo $storage; ?>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
function GoogleMapsLoaded(){
    var latlng = new google.maps.LatLng(<?php echo $server['latitude'];?>,<?php echo $server['longitude'];?>);
    var latlng2 = new google.maps.LatLng(<?php echo $latitude;?>,<?php echo $longitude;?>);
    var myOptions =
    {
        zoom: 4,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI:true
    };

    var myOptions2 =
    {
        zoom: 4,
        center: latlng2,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI:true
    };

    var map = new google.maps.Map(document.getElementById("map"), myOptions);
    
    var map2 = new google.maps.Map(document.getElementById("map1"), myOptions2);

    var myMarker = new google.maps.Marker(
    {
        position: latlng,
        map: map,
        icon:'<?php echo Yii::app()->getBaseUrl(true) ?>/images/servericon.png'
   });

    var myMarker2 = new google.maps.Marker(
    {
        position: latlng2,
        map: map2,
        icon:'<?php echo Yii::app()->getBaseUrl(true) ?>/images/servericon.png'
    });
}
</script>