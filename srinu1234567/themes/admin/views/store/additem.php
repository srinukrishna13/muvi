<style>
    .loading_div{display: none;}      
    .jcrop-keymgr{display:none !important;}    
    .addmore-content{display:none;}
    .fixedWidth--Preview img {max-width: none !important;}

</style>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/common/js/placeholder/holder.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<link href="<?php echo ASSETS_URL; ?>css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
<div id="success_msg"></div>
<input type="hidden" name="is_multi_content" id="is_multi_content" value="<?= @$is_multi_content; ?>" />
<?php
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
?>

<form role="form" method="post" enctype="multipart/form-data" id="itemform" name="itemform" class="form-horizontal">
<input type="hidden" value="<?php echo @$fieldval[0]['id']; ?>" name="pg[id]"  />
    <div class="row m-t-40">
        <div class="col-md-4 col-sm-12 pull-right--desktopOnly">
            <div class="Block">
                <div class="Block-Header">
                    <?php                    
                    $cropDimesion = Yii::app()->common->getPgDimension();
                    ?>
                    <input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth2" id="reqwidth2" />
                    <input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight2" id="reqheight2" />   
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-cloud-upload icon left-icon "></em>
                    </div>
                    <h4>Upload Poster</h4>
                </div>
                <hr>

                <div class="border-dotted m-b-40">
                    <div class="text-center">
                        <input type="button" class="btn btn-default-with-bg btn-file" data-toggle="modal" data-target=".bs-example-modal-lg" value="Browse">

                        <h5 class="grey m-t-10 m-b-20">Upload image size of <span class="reqimgsize" id="reqimgsize"><?php echo $cropDimesion['width'] . 'X' . $cropDimesion['height']; ?></span></h5>
                    </div>
                    <div class="text-center">
                        <div class="m-b-10 displayInline fixedWidth--Preview">
                            <?php
                            $no_image_array = '/img/No-Image-Vertical.png';
                            if (!in_array($posterImg, $no_image_array)) {
                                ?>
                                <div class="poster-cls  avatar-view jcrop-thumb">
                                    <div id="avatar_preview_div">    
                                        <?php if(@$fieldval[0]['id']!=''){
                                            $posterImg = $pgposter;
                                        }else{
                                            $posterImg = POSTER_URL . '/no-image-a.png';
                                        }?>
                                        <?php if (strpos($posterImg, 'no-image') > -1) { ?>
                                            <img data-src="holder.js/<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" alt="<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" />
                                        <?php } else { ?>
                                            <img title="poster-img" rel="tooltip" id="preview_content_img" src="<?php echo $posterImg; ?>" style="width: <?php echo $cropDimesion['width']; ?>px;">
                                        <?php } ?>
                                    </div>
                                </div>
                                <?= $this->renderPartial('//layouts/image_gallery', array('cropDimesion' => $cropDimesion, 'all_images' => $all_images, 'celeb' => $celeb, 'base_cloud_url' => $base_cloud_url)); ?>

                            <?php } else { ?>
                                <div class="poster-cls  avatar-view jcrop-thumb">
                                    <div id="avatar_preview_div">
                                        <img title="poster-img" rel="tooltip" id="preview_content_img" src="<?php echo $posterImg; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <canvas id="previewcanvas" style="overflow:hidden;display: none;margin-left: 10px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12">
            <div class="Block">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-info icon left-icon "></em>
                    </div>
                    <h4>Basic Information</h4>
                </div>
                <hr>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <div class="loading_div">
                            <div class="preloader pls-blue preloader-sm">
                                <svg viewBox="25 25 50 50" class="pl-circular">
                                <circle r="20" cy="50" cx="50" class="plc-path"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if($productize){?>
                <div class="form-group">
                    <label for="itemtype" class="col-md-4 control-label">Personalization: </label>
                    <div class="col-md-8">
                        <div class="control-label row">
                            <div class="col-sm-12">
                                <label class="radio radio-inline m-r-20">
                                    <input type="radio" value="0" name="pg[productize_flag]" <?php echo (@$fieldval[0]['id'] != '')?'':'checked=checked';?> <?php echo (@$fieldval[0]['productize_flag'] == 0)?'checked=checked':'';?>>
                                    <i class="input-helper"></i>Disabled
                                </label>
                                <label class="radio radio-inline m-r-20">
                                    <input type="radio" value="1" name="pg[productize_flag]"<?php echo (@$fieldval[0]['productize_flag'] == 1)?'checked=checked':'';?>>
                                    <i class="input-helper"></i>Enabled
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
                <div id="formContents">
                    <?php 
                        if(@$customData){
                            $defaultFields = array('name','description','sale_price','sku','product_type');
                            $relationalFields = Yii::app()->Helper->getRelationalPGField(Yii::app()->user->studio_id); 
                            $formData = $customData['formData'];
                            unset($customData['formData']);
                            foreach ($customData AS $ckey=>$cval){
                                if(!in_array($cval['f_name'],$defaultFields)){
                                    if($relationalFields && array_key_exists($cval['f_name'],$relationalFields)){
                                        $cval['f_name'] = $relationalFields[$cval['f_name']]['field_name'];
                                    }
                                }
                                $active_fields[] = $cval['f_name'];
                                ?>
                            <div class="form-group">
                                <label for="<?= $cval['f_display_name'];?>" class="col-md-4 control-label"><?= $cval['f_display_name'];?><?php if($cval['f_is_required']){?><span class="red"><b>*</b></span><?php }?>:</label>
                                <?php if(!$cval['f_type']){?>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <?php if($cval['f_name']=='sale_price'){?>
                                            <div class="moreCurrencyDiv">
                                                <?php
                                                if(count($currency_code) > 1){
                                                    $pgcurrenyname = "pg[multi_currency_id][]";
                                                    $pgsalepricename = "pg[multi_sale_price][]";
                                                }else{
                                                    $pgcurrenyname = "pg[currency_id]";
                                                    $pgsalepricename = "pg[sale_price]";                                                    
                                                }
                                                if((count($currency_code) > 1) && !empty($pgmulti)){
                                                    $counter = 1;
                                                    foreach ($pgmulti as $mkey => $multivalue) {?>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                                                <?php 
                                                                    foreach ($currency_code as $key => $value) {
                                                                            if(@$mkey==$value["id"]){
                                                                                echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                                            }else{
                                                                                echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                                            }                                                                                    
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="fg-line">
                                                        <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$multivalue; ?>" class="form-control input-sm cost" step="0.01">
                                                        <span class="countdown1"></span>
                                                    </div>
                                                </div>                                                            
                                                <div class="col-md-4">                                                    
                                                    <div class="fg-line">
                                                        <h5>
                                                            <?php if($counter==1){?>
                                                            <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink" style="display: <?php if(count($pgmulti) < count($currency_code)){echo 'block';}else{echo 'none';}?>;">
                                                                &nbsp; Add more currency
                                                            </a>
                                                            <?php }else{?>
                                                            <a onclick="removecurrency('<?php echo count($currency_code);?>',this);" href="javascript:void(0);">
                                                                &nbsp; Remove currency
                                                            </a>
                                                            <?php }?>
                                                        </h5>
                                                    </div>                                                                                                       
                                                </div>                                                            
                                            </div>
                                            <?php   $counter++;}
                                                }else{
                                            ?>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                                                <?php 
                                                                    foreach ($currency_code as $key => $value) {
                                                                        if(@$fieldval[0]['currency_id']==$key){
                                                                            echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                                        }else{
                                                                            echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                                        }
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="fg-line">
                                                        <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$fieldval[0]['sale_price']; ?>" class="form-control input-sm cost" step="0.01">
                                                        <span class="countdown1"></span>
                                                    </div>
                                                </div>
                                                <?php if(count($currency_code) > 1){?>
                                                <div class="col-md-4">                                                    
                                                    <div class="fg-line">
                                                        <h5>
                                                            <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink">
                                                                &nbsp; Add more currency
                                                            </a>
                                                        </h5>
                                            </div>
                                                </div>
                                                <?php }?>
                                            </div>
                                            <?php }?>
                                        </div>
                                        <?php }else{?>
                                                <input type='text' placeholder="<?= $cval['f_value'];?>" id="<?= $cval['f_id'];?>" name="pg[<?= $cval['f_name'];?>]" value="<?php echo @$fieldval[0][$cval['f_name']]; ?>" class="form-control input-sm checkInput" <?php if($cval['f_is_required']){?> required  <?php }?> >
                                        <?php }?>
                                        <?php if($cval['f_id']=='skuno'){?>
                                                <span class="red" id="email-error" style="display: none;"></span>
                                        <?php }?>        
                                        </div>
                                    </div>	
                                <?php }elseif($cval['f_type']==1){?>
                                        <div class="col-md-8">
                                            <div class="fg-line">
                                                <textarea class="form-control input-sm checkInput" rows="5" placeholder="<?= $cval['f_value'];?>" name="pg[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" ><?php echo @$fieldval[0][$cval['f_name']]; ?></textarea>
                                            </div>
                                        </div>
                                <?php }elseif($cval['f_type']==2){?>
                                        <div class="col-md-8">
                                            <div class="fg-line">
                                                <div class="select">
                                                <?php if($cval['f_name']=='size'){?>
                                                <select name="pg[<?= $cval['f_name'];?>]" placeholder="" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                                                    <?php
                                                        echo "<option value=''>-Select-</option>";
                                                        $opData = $shipping_size;
                                                        foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
                                                            if (@$fieldval[0]['id'] && @$fieldval[0][$cval['f_name']]==$opkey) {
                                                                $selectedDd = 'selected ="selected"';
                                                            }
                                                            echo "<option value='".$opkey."' ".$selectedDd." >" . $opvalue . "</option>";
                                                        }
                                                    ?>
                                                </select>
                                                <?php }else{?>
                                                <select name="pg[<?= $cval['f_name'];?>]" placeholder="" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                                                    <?php
                                                        echo "<option value=''>-Select-</option>";
                                                        $opData = json_decode($cval['f_value'],true);
                                                        foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
                                                            if (@$fieldval[0]['id'] && @$fieldval[0][$cval['f_name']]==$opvalue) {
                                                                $selectedDd = 'selected ="selected"';
                                                            }
                                                            echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
                                                        }
                                                    ?>
                                                </select>
                                                <?php }?>
                                            </div>
                                        </div>
                                        </div>
                                <?php }elseif($cval['f_type']==3){
                                        if($cval['f_id']=='genre' || $cval['f_id']=='language' || $cval['f_id']=='censor_rating'){?>
                                        <select  data-role="tagsinput"  name="pg[<?= $cval['f_name'];?>][]" placeholder="Use Enter or Comma to add new" id="<?= $cval['f_id'];?>" multiple >
                                            <?php
                                            if (@$fieldval[0]['id'] && @$fieldval[0][$cval['f_name']]) {
                                                $pregenre = json_decode($fieldval[0][$cval['f_name']]);
                                                foreach ($pregenre AS $k => $v) {
                                                    echo "<option value='" . $v . "' selected='selected'>" . $v . "</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                        <?php }else{ ?>
                                            <div class="col-md-8">
                                                <select name="pg[<?= $cval['f_name'];?>][]" placeholder="" id="<?= $cval['f_id'];?>" multiple <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                                                <?php
                                                    $opData = json_decode($cval['f_value'],true);
                                                    foreach($opData AS $opkey=>$opvalue){
                                                        $selectedDd = '';
                                                        if (@$fieldval[0]['id'] && in_array($opvalue, json_decode($fieldval[0][$cval['f_name']],true))) {
                                                            $selectedDd = 'selected ="selected"';
                                                    }
                                                        echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                <?php	}?>
                                <?php }elseif($cval['f_type']==4){
                                if($cval['f_name']=='product_type'){?>
                                    <div class="col-md-8">
                                        <div class="control-label row">
                                            <div class="col-sm-12">
                                                <label class="radio radio-inline m-r-20">
                                                    <input type="radio" value="0" name="pg[<?= $cval['f_name'];?>]" <?php echo (@$fieldval[0]['id'] != '')?'':'checked=checked';?> <?php echo (@$fieldval[0]['product_type'] == 0)?'checked=checked':'';?>>
                                                    <i class="input-helper"></i>Standalone
                                                </label>
                                                <label class="radio radio-inline m-r-20">
                                                    <input type="radio" value="1" name="pg[<?= $cval['f_name'];?>]"<?php echo (@$fieldval[0]['product_type'] == 1)?'checked=checked':'';?>>
                                                    <i class="input-helper"></i>Link to content
                                                </label>
                                            </div>
                                            <div id="showcntent" <?php echo (@$fieldval[0]['id'] != '' && @$fieldval[0]['product_type'] != 0)?'':'style="display: none;"';?> class="fg-line col-sm-12">
                                                <select id="movie_id" name="pg[contentid][]" class="form-control tokenize-sample" multiple="multiple">
                                                    <?php 
                                                    $movie_ids = '';
                                                    if(isset($movies) && !empty($movies)) {
                                                        foreach ($movies as $key => $value) {
                                                    ?>
                                                            <option value="<?php echo $value['movie_id'];?>" <?php echo in_array($value['movie_id'],$pgcontent)?'selected=selected':'';?> ><?php echo $value['movie_name'];?></option>
                                                        <?php 
                                                            if(in_array($value['movie_id'],$pgcontent)){
                                                                $movie_ids = ltrim($value['movie_id'], ',');
                                                            }
                                                        } ?>
                                                    <?php }
                                                    ?>
                                                </select>                                    
                                            </div>
                                            <span id="error_movie_id" class="red col-sm-12"></span>
                                            <input type="hidden" class="form-control" name="pg[contentidtxt]" id="movie_ids" value="<?php echo $movie_ids;?>">
                                            <span class="countdown1"></span>
                                        </div>
                                    </div>
                                <?php } }?>
                        </div>
                        <?php }
                            //show default size field if not present
                            if(!in_array('size',$active_fields)){
                        ?>
                            <div class="form-group">
                            <label for="size" class="col-md-4 control-label">Size:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="size" name="pg[size]" class="form-control input-sm">
                                            <?php
                                            foreach($shipping_size AS $key=>$val){
                                            ?>
                                                <option value="<?php echo $key;?>" <?php echo (@$fieldval[0]['size'] == $key) ? 'selected=selected' : ''; ?> ><?php echo $val;?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php    
                            }
                        }else{?>
                    <div class="form-group">
                        <label for="movieName" class="col-md-4 control-label">Item Name<span class="red"><b>*</b></span>:</label>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' placeholder="Enter item name here." id="mname" name="pg[name]" value="<?php echo htmlspecialchars(@$fieldval[0]['name']); ?>" class="form-control input-sm checkInput" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Description: </label>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <textarea class="form-control input-sm checkInput" rows="5" placeholder="Enter item description here" name="pg[description]" id="mdescription"><?php echo @$fieldval[0]['description']; ?></textarea>
                                <span class="countdown1"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="itemtype" class="col-md-4 control-label">Item Type: </label>
                        <div class="col-md-8">
                            <div class="control-label row">
                                <div class="col-sm-12">
                                    <label class="radio radio-inline m-r-20">
                                        <input type="radio" value="0" name="pg[product_type]" <?php echo (@$fieldval[0]['id'] != '')?'':'checked=checked';?> <?php echo (@$fieldval[0]['product_type'] == 0)?'checked=checked':'';?>>
                                        <i class="input-helper"></i>Standalone
                                    </label>
                                    <label class="radio radio-inline m-r-20">
                                        <input type="radio" value="1" name="pg[product_type]"<?php echo (@$fieldval[0]['product_type'] == 1)?'checked=checked':'';?>>
                                        <i class="input-helper"></i>Link to content
                                    </label>
                                </div>
                                <div id="showcntent" <?php echo (@$fieldval[0]['id'] != '' && @$fieldval[0]['product_type'] != 0)?'':'style="display: none;"';?> class="fg-line col-sm-12">
                                    <select id="movie_id" name="pg[contentid][]" class="form-control tokenize-sample" multiple="multiple">
                                        <?php 
                                        $movie_ids = '';
                                        if(isset($movies) && !empty($movies)) {
                                            foreach ($movies as $key => $value) {
                                        ?>
                                                <option value="<?php echo $value['movie_id'];?>" <?php echo in_array($value['movie_id'],$pgcontent)?'selected=selected':'';?> ><?php echo $value['movie_name'];?></option>
                                            <?php 
                                                if(in_array($value['movie_id'],$pgcontent)){
                                                    $movie_ids = ltrim($value['movie_id'], ',');
                                                }
                                            } ?>
                                        <?php }
                                        ?>
                                    </select>                                    
                                </div>
                                <span id="error_movie_id" class="red col-sm-12"></span>
                                <input type="hidden" class="form-control" name="pg[contentidtxt]" id="movie_ids" value="<?php echo $movie_ids;?>">
                                <span class="countdown1"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-md-4 control-label">Price<span class="red"><b>*</b></span>: </label>
                        <div class="col-md-8">
                            <div class="moreCurrencyDiv">
                            <?php
                            if(count($currency_code) > 1){
                                $pgcurrenyname = "pg[multi_currency_id][]";
                                $pgsalepricename = "pg[multi_sale_price][]";
                            }else{
                                $pgcurrenyname = "pg[currency_id]";
                                $pgsalepricename = "pg[sale_price]";                                                    
                            }
                            if((count($currency_code) > 1) && !empty($pgmulti)){
                                $counter = 1;
                                foreach ($pgmulti as $mkey => $multivalue) {?>
                                    <div class="row">
                                        <div class="col-md-3">
                            <div class="fg-line">
                                <div class="select">
                                                    <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                        <?php 
                                            foreach ($currency_code as $key => $value) {
                                                                if(@$mkey==$value["id"]){
                                                                    echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                }else{
                                                                    echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="fg-line">
                                <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$multivalue; ?>" class="form-control input-sm cost" step="0.01">
                                <span class="countdown1"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="fg-line">
                                <h5>
                                    <?php if($counter==1){?>
                                    <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink" style="display: <?php if(count($pgmulti) < count($currency_code)){echo 'block';}else{echo 'none';}?>;">
                                        &nbsp; Add more currency
                                    </a>
                                    <?php }else{?>
                                    <a onclick="removecurrency('<?php echo count($currency_code);?>',this);" href="javascript:void(0);">
                                        &nbsp; Remove currency
                                    </a>
                                    <?php }?>
                                </h5>
                            </div>                                                                                                       
                        </div>                                                            
                        </div>
                        <?php   $counter++;}
                            }else{
                        ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fg-line">
                                    <div class="select">
                                        <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                            <?php 
                                                foreach ($currency_code as $key => $value) {
                                                    if(@$fieldval[0]['currency_id']==$key){
                                                        echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                    }else{
                                                        echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="fg-line">
                                    <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$fieldval[0]['sale_price']; ?>" class="form-control input-sm cost" step="0.01">
                                <span class="countdown1"></span>
                            </div>
                        </div>
                            <?php if(count($currency_code) > 1){?>
                            <div class="col-md-4">                                                    
                                <div class="fg-line">
                                    <h5>
                                        <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink">
                                            &nbsp; Add more currency
                                        </a>
                                    </h5>
                    </div>
                            </div>
                            <?php }?>
                        </div>
                        <?php }?>
                        </div>
                        </div>
                    </div>
                    <div id="sku">
                        <div class="form-group">
                            <label for="sku" class="col-md-4 control-label">SKU Number<span class="red"><b>*</b></span>:</label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type='text' placeholder="Enter SKU number." id="skuno" name="pg[sku]" value="<?php echo @$fieldval[0]['sku']; ?>" class="form-control input-sm checkInput" >
                                    <span class="red" id="email-error" style="display: none;"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="size">
                        <div class="form-group">
                            <label for="size" class="col-md-4 control-label">Size:</label>
                            <div class="col-md-8">
                                <div class="select">
                                    <select id="size" name="pg[size]" class="form-control input-sm">
                                    <?php foreach($shipping_size AS $key=>$val){?>
                                        <option value="<?php echo $key;?>" <?php echo (@$fieldval[0]['size'] == $key) ? 'selected=selected' : ''; ?> ><?php echo $val;?></option>
                                    <?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="releaseDate" class="col-md-4 control-label">Release/Recorded Date:</label>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="release_date" name="pg[release_date]" value="<?php
                                if (isset($fieldval[0]['release_date']) && $fieldval[0]['release_date']!='0000-00-00') {
                                        echo date('m/d/Y', strtotime(@$fieldval[0]['release_date']));
                                }
                                ?>" class="form-control input-sm checkInput" >
                            </div>
                        </div>
                    </div>
                    <div id="product_status">
                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Status<span class="red"><b>*</b></span>:</label>
                            <div class="col-md-8">
                                <div class="select">
                                    <select id="product_status" name="pg[status]" class="form-control input-sm">
                                       <option value="1" <?php echo (@$fieldval[0]['status'] == 1)?'selected=selected':''; ?> >Active</option>
                                       <option value="2" <?php echo (@$fieldval[0]['status'] == 2)?'selected=selected':''; ?>  >Inactive</option>
                                       <option value="3" <?php echo (@$fieldval[0]['status'] == 3)?'selected=selected':''; ?>  >Out of Stock</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="form-group">
                        <label for="Discount" class="col-md-4 control-label">Publish on<span><b></b></span>: </label>

                        <div class="col-md-4">
                            <div class="fg-line">
                                <input class="form-control input-sm publish_date_cls" placeholder="Publish Date" type="text" data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'" placeholder="mm/dd/yyy"  id="publish_date" name="pg[publish_date]" value="<?php echo (@$fieldval[0]['publish_date'] != '0000-00-00' && @$fieldval[0]['publish_date'] != '')?date('m/d/Y',strtotime($fieldval[0]['publish_date'])):''; ?>">
                               
                                <span class="countdown1"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="fg-line">
                                <!--<input class="form-control input-sm cpublish_time_cls" placeholder="Publish Time" data-mask="" data-inputmask="'alias': 'hh:mm'"  placeholder="hh:mm" id="publish_time" name="publish_time" value="">
                            </div>                            
                        </div>
                    </div>-->
                    <?php if(@$enable_category == 1){?>
                    <div class="form-group">
                        <label for="category" class="col-md-4 control-label">Content Category<span class="red"><b>*</b></span>: </label>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <select  name="pg[content_category_value][]" id="content_category_value" required="true" class="form-control input-sm checkInput" <?php echo $disable; ?>>
                                    <option value="">-Select-</option>
                                    <?php
                                    foreach ($contentCategories AS $k => $v) {
                                        if (@$fieldval && (@$fieldval[0]['content_category_value'] & $k)) {
                                            $selected = "selected='selected'";
                                        } else {
                                            $selected = '';
                                        }
                                        echo '<option value="' . $k . '" ' . $selected . ' >' . $v . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                    <?php }?>
<div class="form-group">
    <div class="col-md-offset-4 col-md-8">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="pg[content_publish_date]" id="content_publish_date" <?php if (@$fieldval[0]['publish_date']) { ?>checked="checked"<?php } ?>/>
                <i class="input-helper"></i> Make it Live at Specific time
            </label>
        </div>
        <div class="row m-t-10 <?php if (@$fieldval[0]['content_publish_date']) {} else { ?>content_publish_date_div<?php } ?>" id="content_publish_date_div" <?php if (@$fieldval[0]['publish_date']) { ?>style=""<?php }else{ ?> style="display:none;"<?php } ?>>
            <div class="col-sm-5">
                <div class="input-group">
                    <div class="fg-line">
                        <input class="form-control input-sm publish_date_cls" placeholder="Publish Date" type="text" data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'" placeholder="mm/dd/yyy"  id="publish_date" name="pg[publish_date]" value="<?php echo (@$fieldval[0]['publish_date'] != '0000-00-00' && @$fieldval[0]['publish_date'] != '')?date('m/d/Y',strtotime($fieldval[0]['publish_date'])):''; ?>">
                    </div>
                    <span class="input-group-addon"><i class="icon-calendar"></i></span>

                </div>



            </div>
            <div class="col-sm-5">
                <div class="input-group">
                    <div class="fg-line">
                        <input class="form-control input-sm cpublish_time_cls" placeholder="Publish Time" data-mask="" data-inputmask="'alias': 'hh:mm'"  placeholder="hh:mm" id="publish_time" name="pg[publish_time]" value="<?php echo (@$fieldval[0]['publish_date'] != '0000-00-00' && @$fieldval[0]['publish_date'] != '')?date('H:i',strtotime($fieldval[0]['publish_date'])):''; ?>" type="text">
                    </div>
                    <span class="input-group-addon"><em class="icon-clock"></em></span>

                </div>



            </div>
            <div class="col-sm-2">
                <label for="content_publish_date">UTC</label>
            </div>
        </div>
    </div>
</div>
<!--<div class="form-group">
    <div class="col-md-offset-4 col-md-8">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="pg[feature]" id="feature" <?php if (@$fieldval[0]['feature']) { ?>checked="checked"<?php } ?>/>
                <i class="input-helper"></i> Make it featured product
            </label>
        </div>
    </div>
</div>
                     <div class="form-group">
                        <label for="Discount" class="col-md-4 control-label">Discount<span><b></b></span>: </label>
                        <div class="col-md-2">
                            <div class="fg-line">
                                <input type="number" class="checkInput form-control input-sm" name="pg[discount]" value="<?php echo @$fieldval[0]['discount']; ?>" placeholder="Enter Discount...">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="fg-line">
                                <input id="datepicker_for_no" class="form-control input-sm" type="text"  name="pg[discount_till_date]" value="<?php echo (@$fieldval[0]['discount_till_date'] != '0000-00-00' && @$fieldval[0]['discount_till_date'] != '')?date('m/d/Y',strtotime($fieldval[0]['discount_till_date'])):''; ?>" placeholder="validate until"  onkeypress="return onlyAlphabets(event, this);" autocomplete="off" readonly>
                               
                                <span class="countdown1"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="discountype" class="col-md-4 control-label">Discount type: </label>
                        <div class="col-md-8">
                            <div class="fg-line control-label row">
                                <div class="col-sm-12">
                                    <label class="radio radio-inline m-r-20">
                                        <input type="radio" value="0" name="pg[discount_type]" <?php echo (@$fieldval[0]['discount_type'] == 0)?'checked=checked':'';?>>
                                        <i class="input-helper"></i>Flat
                                    </label>
                                    <label class="radio radio-inline m-r-20">
                                        <input type="radio" value="1" name="pg[discount_type]" <?php echo (@$fieldval[0]['discount_type'] == 1)?'checked=checked':'';?>>
                                        <i class="input-helper"></i>Percent
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>-->


               <div class="form-group m-t-30">
                    <div class="col-md-offset-4 col-md-8">

                        <button type="submit" class="btn btn-primary btn-sm" id="save-btn">
                            <?php echo (@$fieldval[0]['id'] != '')?'Update':'Save';?>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
	
    
    
    <input type="hidden" id="x1" name="jcrop[x1]" />
    <input type="hidden" id="y1" name="jcrop[y1]" />
    <input type="hidden" id="x2" name="jcrop[x2]" />
    <input type="hidden" id="y2" name="jcrop[y2]" />
    <input type="hidden" id="w" name="jcrop[w]">
    <input type="hidden" id="h" name="jcrop[h]">    
</form>	
<div style="display: none;" id="defaultcurrencydiv">
    <?php echo $this->renderPartial('addmorecurrency', array('currency_code'=>$currency_code,'pgcurrenyname'=>$pgcurrenyname,'pgsalepricename'=>$pgsalepricename), true);?>
</div> 
<input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth" id="reqwidth" />
<input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight" id="reqheight" />   
<div style="clear: both;"></div>
<!-- Change Poster Popup-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery-ui-timepicker-addon.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.tokenize.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.tokenize.js"></script>
<script type="text/javascript">
    Array.prototype.getUnique = function () {
        var u = {}, a = [];
        for (var i = 0, l = this.length; i < l; ++i) {
            if (u.hasOwnProperty(this[i])) {
                continue;
            }
            a.push(this[i]);
            u[this[i]] = 1;
        }
        return a;
    }
    $(function () {
        var url = "<?php echo Yii::app()->baseUrl; ?>/admin/movie_autocomplete";
        $('#movie_id').tokenize({
            datas: url,
            placeholder: 'Search contents',
            nbDropdownElements: 10,
            onAddToken:function(value, text, e){
                if (parseInt(value)) {
                    var video_ids = $("#movie_ids").val();
                    video_ids = video_ids.split(",");
                    video_ids.push(value);
                    video_ids.toString();
                    $("#movie_ids").val(video_ids);
                }
            },
            onRemoveToken:function(value, e){
                var video_ids = $("#movie_ids").val();
                video_ids = video_ids.split(",");
                
                var index = video_ids.indexOf(value);
                if(index !== -1) {
                    video_ids.splice(index,1);
                }
                video_ids.toString();
                $("#movie_ids").val(video_ids);
            }
        });
    });
    function click_browse(file_name) {
        $("#" + file_name).click();
    }

    $(document).ready(function(){
        $('.cost').on("keypress",function(event) {
            return decimalsonly(event);
        });
        $('.cost').keyup(function() {
            if($(this).val()!=''){$(this).parent().parent().find('.multierrorprice').remove();}
        });
	$("[data-mask]").inputmask(), $("#release_date").datepicker({changeMonth: !0, changeYear: !0})
        $('input[name="pg[product_type]"]').click(function(){
            if ( $(this).is(':checked') ) {
                if ($(this).val() == '1' ) {
                    $('#showcntent').show();
                } else {
                     $('#showcntent').hide();
                     $('#error_movie_id').html('');
                }
            }
        });
   //---------------------------------
   $("#itemform").validate({
    rules: {
        "pg[name]": "required",        
        "pg[sale_price]": "required",
        "pg[sku]": "required",

    },
    messages: {
        "pg[name]": "Please enter item name",
        "pg[sale_price]": "Please enter amount",
        "pg[sku]": "Please enter SKU number",

    },
    errorPlacement: function(error, element) {
        error.addClass('red');
        error.insertAfter(element.parent());
    },
    submitHandler: function(form)
    {
        if($("input[name='pg[product_type]']:checked"). val()==1){
            if($('#movie_ids').val()==''){
                $('#error_movie_id').html('Enter content name');
                return false;
            }else{
                $('#error_movie_id').html('');
            }                
        }else{
            $('#error_movie_id').html('');
        }        
        var isprice = 1;
        $(".moreCurrencyDiv").find(".cost").each(function () {
            $(this).parent().find('label').remove();
            if($(this).val()==''){
                $(this).parent().after("<label class='error red multierrorprice'>This field is required.</label>");
                isprice = 0;
            }
        });
        if(isprice==0){
            return false;
        }
        var currency = new Array();
        $(".moreCurrencyDiv").find(".select").each(function () {
            currency.push($(this).find('select').val());
        });
        var x = currency.getUnique();
        if (x.length === currency.length) {
            //do nothing
        } else {
            swal("Currency should be unique");
            return false;
        }
        var url = "<?php echo Yii::app()->getBaseUrl(true) ?>/store/checkSkuNumber";         
        $('#save-btn').attr('disabled','disabled');
        $.post(url, {'skuno': $('#skuno').val(),'check':'<?php echo (@$fieldval[0]['id'] != '')?'FALSE':'TRUE';?>','id':'<?php echo @$fieldval[0]['id']?>'}, function (res)
        {
            if (res.succ)
            {
                $('#is_submit').val(1);
                document.itemform.action = '<?php echo Yii::app()->baseUrl; ?>/store/<?php echo (@$fieldval[0]['id'] != '')?'EditSaveItem':'SaveItems';?>';
                document.itemform.submit();
            } else {
                $('#save-btn').removeAttr('disabled');
                $('#email-error').show();
                $('#email-error').html('This sku number has been used.');
            }
        }, 'json');
    } 
    }); 
 })
    function posterpreview(obj) {
        $("#previewcanvas").show();
        var canvaswidth = $("#reqwidth").val();
        var canvasheight = $("#reqheight").val();
        if ($('#g_image_file_name').val() == '') {
            var x1 = $('#x1').val();
            var y1 = $('#y1').val();
            var width = $('#w').val();
            var height = $('#h').val();
        } else {
            var x1 = $('#x13').val();
            var y1 = $('#y13').val();
            var width = $('#w3').val();
            var height = $('#h3').val();
        }
        var canvas = $("#previewcanvas")[0];
        var context = canvas.getContext('2d');
        var img = new Image();
        img.onload = function () {
            canvas.height = canvasheight;
            canvas.width = canvaswidth;
            context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
            //$('#imgCropped').val(canvas.toDataURL());
        };
        $("#avatar_preview_div").hide();
        if ($('#g_image_file_name').val() == '') {
            img.src = $('#preview').attr("src");
        } else {
            img.src = $('#glry_preview').attr("src");
        }
        $('#myLargeModalLabel').modal('hide');
        $(obj).html("Next");
    }
    function hide_file() {
        $('#preview').css("display", "none");
    }
    function hide_gallery() {
        $('#glry_preview').css("display", "none");
    }
    function seepreview(obj) {

        if ($("#x13").val() != "") {
            $(obj).html("Please Wait");
            $('#myLargeModalLabel').modal({backdrop: 'static', keyboard: false});
            posterpreview(obj);
        } else {
            if ($("#celeb_preview").hasClass("hide")) {
                $('#myLargeModalLabel').modal('hide');
                $(obj).html("Next");
            } else {
                $(obj).html("Please Wait");
                $('#myLargeModalLabel').modal({backdrop: 'static', keyboard: false});
                if ($("#x13").val() != "") {
                    posterpreview(obj);
                } else if ($('#x1').val() != "") {
                    posterpreview(obj);
                } else {
                    $('#myLargeModalLabel').modal('hide');
                    $(obj).html("Next");
                }
            }
        }
    }
    function fileSelectHandler() {
        $('#celeb_preview').css("display", "block");
        $("#g_image_file_name").val('');
        $("#g_original_image").val('');
        $('#glry_preview').css("display", "none");
        clearInfo();
        $(".jcrop-keymgr").css("display", "none");
        $("#editceleb_preview").hide();
        $("#celeb_preview").removeClass("hide");
        var reqwidth = parseInt($('#reqwidth').val());
        var reqheight = parseInt($('#reqheight').val());
        var aspectRatio = reqwidth / reqheight;
        var oFile = $('#celeb_pic')[0].files[0];
        var ext = oFile.name.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";
            $('#topbanner_submit_btn').attr('disabled', 'disabled');
            swal('Please select a valid image file (jpg and png are allowed)');
            return;
        }
        if (oFile.name.match(/['|"|-|,]/)) {
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";
            $('#topbanner_submit_btn').attr('disabled', 'disabled');
            swal('File names with symbols such as \' , - are not supported');
            return;
        }
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";

            swal('Please select a valid image file (jpg and png are allowed)');
            return;
        }
        var img = new Image();
        img.src = window.URL.createObjectURL(oFile);
        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);

            if (width < reqwidth || height < reqheight) {
                $("#celeb_preview").addClass("hide");
                document.getElementById("celeb_pic").value = "";
                swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' x ' + reqheight);

                return;
            }
            var oImage = document.getElementById('preview');
            var oReader = new FileReader();
            oReader.onload = function (e) {
                $('.error').hide();
                oImage.src = e.target.result;
                oImage.onload = function () {
                    if (typeof jcrop_api != 'undefined') {
                        jcrop_api.destroy();
                        jcrop_api = null;
                        $('#preview').width(oImage.naturalWidth);
                        $('#preview').height(oImage.naturalHeight);
                        $('#glry_preview').width("450");
                        $('#glry_preview').height("250");
                    }
                    $('#preview').Jcrop({
                        minSize: [reqwidth, reqheight],
                        aspectRatio: aspectRatio,
                        boxWidth: 400,
                        boxHeight: 150,
                        bgFade: true,
                        bgOpacity: .3,
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo
                    }, function () {
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];
                        jcrop_api = this;
                        jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                    });
                };
            };
            oReader.readAsDataURL(oFile);
        }
    }
    function toggle_preview(id, img_src, name_of_image) {

        $('#gallery_preview').css("display", "block");
        clearInfo();
        $('#gallery_preview').removeClass("hide");
        document.getElementById("celeb_pic").value = "";
        var reqwidth = parseInt($('#reqwidth').val());
        var reqheight = parseInt($('#reqheight').val());
        var aspectRatio = reqwidth / reqheight;
        showLoader();
        var image_file_name = name_of_image;
        var image_src = img_src;
        $("#g_image_file_name").val(image_file_name);
        $("#g_original_image").val(image_src);
        var res = image_file_name.split(".");
        var image_type = res[1];
        var img = new Image();
        img.src = img_src;
        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            if (width < reqwidth || height < reqheight) {
                showLoader(1);
                $("#gallery_preview").addClass("hide");
                $("#g_image_file_name").val("");
                $("#g_original_image").val("");
                swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' X ' + reqheight);
                return;
            }
            var oImage = document.getElementById('glry_preview');
            showLoader(1)
            oImage.src = img_src;
            oImage.onload = function () {
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#glry_preview').width(oImage.naturalWidth);
                    $('#glry_preview').height(oImage.naturalHeight);
                    $('#preview').width("800");
                    $('#preview').height("300");
                }
                $('#glry_preview').Jcrop({
                    minSize: [reqwidth, reqheight],
                    aspectRatio: aspectRatio,
                    boxWidth: 400,
                    boxHeight: 150,
                    bgFade: true,
                    bgOpacity: .3,
                    onChange: updateInfoallImage,
                    onSelect: updateInfoallImage,
                    onRelease: clearInfoallImage
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
            };
        };
    }
    var gmtDate = '<?= gmdate('d'); ?>';
    var gmtMonth = '<?= gmdate('m'); ?>';
    var gmtYear = '<?php echo gmdate('Y'); ?>';
    $("#publish_date").datepicker({changeMonth: !0, changeYear: !0, minDate: new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)})
   
    
    function showLoader(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loaderDiv').show();
            $('#myLargeModalLabel button,input[type="text"]').attr('disabled', 'disabled');
            $('#profile button').attr('disabled', 'disabled');

        } else {
            $('.loaderDiv').hide();
            $('#myLargeModalLabel button,input[type="text"]').removeAttr('disabled');
            $('#profile button').removeAttr('disabled');
        }
    }
    function addmorecurrency(cnt){
        cnt = parseInt(cnt);
        var childdiv = $('.moreCurrencyDiv').children('div').length;
        //alert(cnt +' '+ childdiv);
        if(childdiv < cnt){
            var morecurreny = $('#defaultcurrencydiv').html();
            $('.moreCurrencyDiv').append(morecurreny);
            var childdiv1 = childdiv+1;
            if(childdiv1==cnt){$('#addmorelink').hide();}
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });
            $('.cost').keyup(function() {
                if($(this).val()!=''){$(this).parent().parent().find('.multierrorprice').remove();}
            });
        }else{
            $('#addmorelink').hide();
        }
    }
    function removecurrency(cnt,obj){
        cnt = parseInt(cnt);
        var childdiv = $('.moreCurrencyDiv').children('div').length;
        //alert(cnt +' '+ childdiv);
        if(childdiv <= cnt){
            $(obj).parent().parent().parent().parent().remove();
            $('#addmorelink').show();
        }else{
            $('#addmorelink').show();
        }
    }
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57)){                
                return true;
            }else
                return false;
        }
    }
 </script>
 
<script type="text/javascript">
    // Minified Javascript 	
     $("#content_publish_date").click(function () {
        0 == this.checked ? $("#content_publish_date_div").hide() : $("#content_publish_date_div").show()
    }), function () {
        $("[data-mask]").inputmask(),$("#publish_date").datepicker({changeMonth: !0, changeYear: !0, minDate: $("#publish_date").val() ? new Date($("#publish_date").val()) : new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)}), $("#publish_time").timepicker({showInputs: !1}), $("#castname").autocomplete({source: function (e, t) {

            }, select: function (e, t) {
                e.preventDefault(), $("#castname").val(t.item.label), $("#cast_id").val(t.item.value)
            }, focus: function (e, t) {
                $("#cast_name").val(t.item.label), $("#cast_id").val(t.item.value), e.preventDefault()
            }}), $("#dprogress_bar").draggable({appendTo: "body", start: function (e, t) {
                isDraggingMedia = !0
            }, stop: function (e, t) {
                isDraggingMedia = !1
            }})
    }();    
</script>
