<div class="row m-b-40">
    <div class="col-xs-12">
        <a href="<?php echo $this->createUrl('store/AddItem'); ?>"><button class="btn btn-primary btn-default m-t-10">Add Item</button></a>
    </div> 
</div>

<div class="filter-div">

    <div class="row">
        <div class="col-md-9 m-b-10">
            <form action="<?php echo $this->createUrl('store/GoodsLibrary'); ?>" name="searchForm" id="search_content" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group  input-group">
                            <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                            <div class="fg-line">
                                <input type="text" class="filter_input form-control fg-input" name="searchForm[search_text]" value="<?php echo @$searchText; ?>"  id="search_content" onkeypress="handle(event);" placeholder="What are you searching for?"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
<table class="table mob-compress" id="movie_list_tbl">
    <thead>
        <tr>
            <th>Item Name</th>
            <th>Preview</th>
            <th>Properties</th>
            <th data-hide="phone" class="width">Action</th>
        </tr>
    </thead>    
    <tbody>
        <?php
        $cnt = 1;
        if ($data) {
            foreach ($data AS $key => $details) {
                $plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink'] : str_replace(' ', "-", strtolower($details['name']));
        ?>
                <tr>
                    <td><a href="<?php echo 'http://' . Yii::app()->user->siteUrl . '/' .  @$plink; ?>" target='_blank' >
        <?php
        echo (strlen(@$details['name']) > 30) ? strip_tags(substr(@$details['name'], 0, 30)). '...' : $details['name'];
        ?>
                    </a></td>
                    <td>
                        <div class="Box-Container  m-b-10">
                            <div class="thumbnail">                                
                                <img src="<?php echo $details['pgposter']; ?>" alt="<?php echo $details['name']; ?>" style="max-width:100px;">
                            </div>
                        </div>
                    </td>
                    <td>                            
                        <?php echo $details['contentname']; ?><br />                        
                        Price - <?php echo Yii::app()->common->formatPrice($details['sale_price'], $details['currency_id']);?><br />
                        Total Sales - <?php echo $details['totquantity']?$details['totquantity']:0; ?><br />
                        Revenue - <?php echo Yii::app()->common->formatPrice($details['totsubtotal'], $details['currency_id']); ?><br />
                        SKU Number - <?php echo $details['sku']; ?><br />
                        Status -
                        <?php 
                            if($details['status'] == 1)echo "Active";
                            else if($details['status'] == 2)echo "Inactive";
                            else if($details['status'] == 3)echo "<span class='red'>Out of Stock</span>";                        
                        ?>
                    </td>
                    <td>
                        <span id="<?php echo $details['id']; ?>_title" style="display:none"><?php echo $details['name']; ?></span>
                        <span id="<?php echo $details['id']; ?>_link" style="display:none"><?php echo $details['thirdparty_url']; ?></span>
                        <?php if(@$view_trailer){if($details['thirdparty_url']!=''){?>
                        <h5><a href="javascript:void(0);" onclick="openUploadpopup('<?php echo $details['id'];?>',1);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Change Trailer</a></h5>        
                        <?php }else{?>
                        <h5><a href="javascript:void(0);" onclick="openUploadpopup('<?php echo $details['id'];?>',0);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload Trailer</a></h5>        
                        <?php } }?>
                        <h5><a href="<?php echo $this->createUrl('store/EditItem/', array('uid' => $details['uniqid'])); ?>" ><em class="icon-pencil"></em>&nbsp;&nbsp; Edit Item</a></h5>
                        <h5><a href="javascript:void(0)" class="confirm" data-msg ="Are you sure?" onclick="confirmDelete('<?php echo $this->createUrl('store/deleteItem/', array('id' => $details['id'])); ?>', '<?php echo ""; ?>');"><em class="icon-trash"></em>&nbsp;&nbsp; Delete Item</a></h5>
                        <?php if(@$geoexist){?>
                        <h5><a href="javascript:void(0);" onclick="openGeoBlock(this);" data-pg_product_id="<?php echo $details['id'];?>"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp;&nbsp; Geo-block</a></h5>
                        <?php }?>
                    </td>
                </tr>	
        <?php
        $cnt++;
    }
} else {
    ?>
            <tr>
                <td colspan="5">
                    <p class="text-red">Oops! You don't have any item in your Studio.</p>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<div class="pull-right">
<?php
if ($data) {
    $opts = array('class' => 'pagination m-t-0');
    $this->widget('CLinkPager', array(
        'currentPage' => $pages->getCurrentPage(),
        'itemCount' => $item_count,
        'pageSize' => $page_size,
        'maxButtonCount' => 6,
        "htmlOptions" => $opts,
        'selectedPageCssClass' => 'active',
        'nextPageLabel' => '&raquo;',
        'prevPageLabel' => '&laquo;',
        'lastPageLabel' => '',
        'firstPageLabel' => '',
        'header' => '',
    ));
}
?>
</div>

<div class="h-40"></div>
<?php 
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getVideoGalleryCloudFrontPath($studio_id);
?>
<!-- Upload Popup  end -->
<div class="modal fade is-Large-Modal bs-example-modal-lg" id="addvideo_popup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Video - <span class="" id="pop_movie_name"></span></h4>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <!--<li role="presentation">
                            <a id="profile-tab"  href="#profile" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Library</a>
                        </li>-->
                        <li role="presentation"  class="active">
                            <a id="home-tab" href="#home"  aria-controls="Upload-Video" role="tab" data-toggle="tab">Embed from Youtube</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content"> 
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <!--------------Upload Video-------------->
                            <div class="row is-Scrollable" style="overflow-x:hidden;">
                                <div class="col-xs-12 m-t-40 m-b-20">
                                    <div class="form-horizontal">	
                                        <div class="form-group">
                                            <div class="col-md-2">
                                                <label for="uploadVideo" class="control-label">Upload Method &nbsp;</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="hidden" value="?" name="utf8">
                                                <input type="hidden" name="product_id" id="product_id" value=""/>
                                                <div class="fg-line">
                                                    <div class="select">
                                                        <select name="upload_type" class="filter_dropdown form-control" id="filetype">
                                                            <option value="browes">From Computer</option>
                                                            <option value="server">Server to Server Transfer</option>
                                                            <option value="dropbox">From Dropbox</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix m-b-20"></div>
                                            <div class="savefile" id="browes_div">
                                                <div class="col-md-2">&nbsp;</div>
                                                <div class="col-md-4">
                                                    <input type="button" value="Browse File" onclick="click_browse();" class="btn btn-default-with-bg btn-sm" >
                                                    <input type="file" name="file" style="display:none;" id="videofile" required onchange="checkfileSize();" >
                                                </div>
                                            </div>
                                            <div class="clear-fix"></div>
                                            <div class="savefile" id="server_div" style="display: none;">
                                                <div class="col-md-2">&nbsp;</div>
                                                <div class="col-md-4">
                                                    <div class="fg-line">
                                                        <input type="url" pattern="" value="" class="form-control input-sm" name="server_url" id="server_url" placeholder="Path to the file on your server" required>
                                                    </div>
                                                    <span class="error red help-block" id="server_url_error"></span>
                                                    <div style="width: 48%;float: left;">
                                                        <input type="text" name="username"  id="ftpusername" class="form-control input-sm" placeholder='Username if any'>
                                                        <label id="ftpusername-error" class="error" for="ftpusername" style="display: inline-block;"></label>
                                                    </div>
                                                    <div style="width: 48%;float: right;">
                                                        <input type="password" name="password" id="ftppassword" class="form-control input-sm" placeholder='Password if any'>
                                                        <label id="ftppassword-error" class="error" for="ftppassword"></label>
                                                    </div>
                                                    <button class="btn btn-primary btn-sm" type="button" onclick="validateURL('');">Submit</button>
                                                </div>
                                            </div>
                                            <div class="savefile" id="dropbox_div" style="display: none;">
                                                <div class="col-md-2">&nbsp;</div>
                                                <div class="col-md-4">
                                                    <div class="fg-line">
                                                        <input type="text" name="dropbox" id="dropbox" class="form-control" placeholder="Path to the file on dropbox">
                                                    </div>
                                                    <span class="error red help-block" id="dropbox_url_error"></span>
                                                    <button class="btn btn-primary btn-sm" type="button" onclick="validateURL('dropbox_url_error');">Submit</button>
                                                </div>
                                            </div>
                                            <div class="clear-fix"></div>                                            
                                        </div>
                                        <div class="form-group m-t-20">
                                            <div class="col-md-12">
                                                <div class="red">Muvi recommends S3 Sync and FTP for faster upload</div>
                                                <ul style="padding-left: 12px;">
                                                    <li>
                                                        <span style="font-weight: bold;">S3 Sync : </span>
                                                        Install a tool we provide in your server. Videos in the server will be automatically synced with Muvi's Video Library
                                                    </li>
                                                    <li>
                                                        <span style="font-weight: bold;">FTP : </span>
                                                        Use a traditional FTP client to upload to Muvi's Video Library
                                                    </li>
                                                </ul>
                                                See <a target="_blank" href="https://www.muvi.com/help/uploading-videos#Muvi_Recommended">help article</a> for more details. Please add a <a href="<?php echo Yii::app()->getbaseUrl(true) . "/ticket/ticketList"; ?>">support ticket</a> to use one of the above tools.
                                            </div>
                                        </div>
                                    </div>		
                                </div>
                                <!----------------------------->
                                <div class="row m-b-20 m-t-20">
                                    <div class="col-sm-2">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                                            <div class="fg-line">
                                                <input type="text" id="search_video1" class="form-control fg-input input-sm" placeholder="Search" onkeyup="searchvideo('video');">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group input-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control" name="video_duration" id="video_duration" onchange="view_filtered_list();">
                                                        <option value="0">Video Duration</option>
                                                        <option  value="1">Less than 5 minutes</option>
                                                        <option  value="2">Less than 30 minutes</option>
                                                        <option  value="3">Less than 120 minutes</option>
                                                        <option  value="4">More than 120 minutes</option>                          
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="form-group input-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control" name="file_size" id="file_size" onchange="view_filtered_list();">
                                                        <option value="0">File Size</option>
                                                        <option value="1">Less than 1GB</option>
                                                        <option value="2">Less than 10GB</option>
                                                        <option value="3">More than 10GB</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="form-group input-group">
                                            <div class="fg-line">
                                                <div class="select" >
                                                    <select class="form-control" name="is_encoded" id="is_encoded" onchange="view_filtered_list();">
                                                        <option value="0"> Mapped to Content</option>
                                                        <option value="1">Yes</option>
                                                        <option value="2">No</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-sm-2">
                                        <div class="form-group input-group">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control" name="uploaded_in" id="uploaded_in" onchange="view_filtered_list();">
                                                        <option value="0">Uploaded In</option>
                                                        <option value="1">This Week</option>
                                                        <option value="2">This Month</option>
                                                        <option value="3">This Year</option>
                                                        <option value="4">Before This Year</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-sm-1">
                                        <div class="form-group input-group">
                                            <div class="fg-line">
                                                <a href="javascript:void(0);" class="btn btn-default" onclick="reset_filter()"> Reset Filter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                        <div class="preloader pls-blue text-center " >
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                            <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="row  Gallery-Row">
                                    <div class="col-md-12 p-t-40 p-b-40" id="video_content_div">
                                        <ul class="list-inline text-left">
                                            <?php
                                            foreach ($all_videos as $key => $val) {
                                                if ($val['video_name'] == '') {
                                                    $img_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/no-image-h.png';
                                                } else {

                                                    $orig_video_path = $base_cloud_url . $val['video_name'];
                                                    $video_thumb_path = $base_cloud_url . "videogallery-image/" . $val['thumb_image_name'];
                                                }
                                                ?> 
                                                <li>
                                                    <div class="Preview-Block video_thumb">
                                                        <div class="thumbnail m-b-0">
                                                            <div class="relative m-b-10">
                                                                <input type="hidden" name="original_video<?php echo $val['id']; ?>" id=original_video<?php echo $val['id']; ?>" value="<?php echo $orig_video_path; ?>">
                                                                <input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['video_name']; ?>"    />
                                                                <img class="img" src="<?php echo $video_thumb_path; ?>"  alt="<?php echo "All_Image"; ?>" >
                                                                <div class="caption overlay overlay-white">
                                                                    <div class="overlay-Text">
                                                                        <div>
                                                                            <a href="javascript:void(0);" id="thumb_<?php echo $val['id']; ?>" onclick="addvideoFromVideoGallery('<?php echo $orig_video_path; ?>', '<?php echo $val['id']; ?>');" title="Select Video">
                                                                                <span class="btn btn-primary btn-sm">
                                                                                    <em class="icon-check"></em>&nbsp; Select Video
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <p style="font-size:12px !important;"  title="<?php echo $val['video_name']; ?>"><?php echo strlen($val['video_name']) > 40 ? substr($val['video_name'], 0, 40) . ".." : $val['video_name']; ?></p>
                                                        </div>


                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!------Embed from 3rd Party------->
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="row is-Scrollable m-b-20 m-t-20">
                                <div class="col-sm-12 m-t-40 m-b-20">
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <label for="uploadVideo" class="control-label col-md-3">Enter Youtube url &nbsp;</label>
                                            <div class="col-md-4">
                                                <input type="hidden" name="product_id" id="product_id" value=""/>                                                
                                                <input type="hidden" name="movie_name" id="movie_name" value=""/>
                                                <div class="fg-line">
                                                    <!--
                                                    <input type="text" id="embed_url" class="form-control  input-sm" placeholder="Link from YouTube, Vimeo or another OVP" onkeyup="embedThirdPartyPlatform();">
                                                    -->
                                                    <input type="text" id="embed_url" class="form-control  input-sm" name="embedurl" required="" placeholder="Link from YouTube"  onkeyup="embedThirdPartyPlatform();">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-10">
                                                <!--
                                                <input type="button" value="Save" onclick="embedFromThirdPartyPlatform();" class="btn btn-primary btn-sm">
                                                -->
                                                <input type="button" value="Save" id="save-btn" onclick="embedFromThirdPartyPlatform();" class="btn btn-primary btn-sm" >
                                            </div>
                                        </div>
                                        <span class="error red help-block" id="embed_url_error"></span>
                                        <div id="embedcode"></div>
                                    </div> 
                                </div>
                            </div> 
                        </div>
                        <!-------End of Embed from 3rd party------>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="ppvPopup" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="Maxcontent"></div>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/footable.js"></script>	
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>	
<script type="text/javascript">
    function confirmDelete(location, msg) {
        swal({
            title: "Delete Item?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            //swal("Deleted!", "Your content has been deleted.", "success");
            window.location.replace(location);
        });

    }

    function click_browse() {
        $("#videofile").click();
    }
    function handle(e) {
        if (e.keyCode === 13) {
            $('#search_content').submit();
        }
        return false;
    }

    $(document).ready(function () {
        $("[rel='tooltip']").tooltip();

        $('.thumbnail').hover(
                function () {
                    $(this).find('.caption').slideDown(250); //.fadeIn(250)
                },
                function () {
                    $(this).find('.caption').slideUp(250); //.fadeOut(205)
                }
        );
        $('#filter_dropdown').change(function () {
            $('#search_content').submit();
        });
        $("#filetype").change(function () {
            $('.error').html('')
            $('.savefile').hide();
            $('#' + $(this).val() + '_div').show();
        });
    });
    function openUploadpopup(product_id,flag) {
        var name = $("#" +product_id + "_title").html();
        $('input[type=file]').val('');
        $('#movie_name').val(name);
        $('#product_id').val(product_id);
        $('#pop_movie_name').html(name);        
        if(flag){
            $('#embed_url').val($("#" +product_id + "_link").html());
        }else{
            $('#embed_url').val('');
        }
        $("#addvideo_popup").modal('show');
        $('#save-btn').removeAttr('disabled');
        $('.error').html('');
    }
    function embedThirdPartyPlatform(){
        var embedurl = $("#embed_url").val();
        if( embedurl.length == 0){
            $('#save-btn').attr('disabled', 'disabled');  
        //return false;
        }else{
            $('#save-btn').removeAttr('disabled');
            return true;
        }
    }
    function embedFromThirdPartyPlatform() {
        var str =$("#embed_url").val();
        var videoUrl1 = "youtube.com";
        var videoUrl2 = "youtu.be";
        //var videoUrl2 = "vimeo.com"; 
        //var videoUrl3 = 'm3u8';
        if(str.length ==0){
            //swal("Oops! Platform should not be blank");
            $('#save-btn').attr('disabled', 'disabled');
            return false;
        }
        else if(str.length !==''){
            if ((str.indexOf(videoUrl1) === -1) && (str.indexOf(videoUrl2) === -1)){
                swal("Oops! Please enter youtube url");
                return false;
            }else{
                //swal("url is valid");
                showLoader();
                var embed_text =$("#embed_url").val();
                var action_url = '<?php echo $this->createUrl('store/embedFromThirdPartyPlatform'); ?>'
                $('#embed_url_error').html('');
                var product_id = $('#product_id').val();
                //alert ('embed_text'+ embed_text);
                $.post(action_url, {'thirdparty_url': embed_text,'product_id': product_id}, function (res) {
                    //if(res){
                     //$('#embed_url_error').html();
                    showLoader(1);
                    window.location.href = window.location.href;
                    // } 
                });
            }
        }else{
            return true;
        }
        // return false;  
    }
    function addvideoFromVideoGallery(videoUrl, galleryId) {
        showLoader();
        var action_url = '<?php echo $this->createUrl('store/addVideoFromVideoGallery'); ?>'
        $('#server_url_error').html('');
        var movie_id = $('#movie_id').val();
        var movie_stream_id = $('#movie_stream_id').val();
        $.post(action_url, {'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id}, function (res) {
            showLoader(1);
            if (res.error) {
                $('#server_url_error').html('There seems to be something wrong with the video. Please contact your Muvi representative. ');
            } else {
                if(res.is_video === 'uploaded') {
                    window.location.href = window.location.href;
                } else if (res.is_video) {
                    $("#addvideo_popup").modal('hide');
                    $('#server_url').val('');
                    $('#server_url_error').val('');
                    var text = '<h5><a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour"><em class="fa fa-refresh fa-spin"></em>&nbsp;&nbsp; Encoding in progress</a></h5>';
                    $('#' + movie_stream_id).html(text);
                } else {
                    $('#server_url_error').html('There seems to be something wrong with the video');
                }
            }
        }, 'json');
        console.log('valid url');
    }
    function showLoader(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loaderDiv').show();
            $('#addvideo_popup button,input[type="text"]').attr('disabled', 'disabled');
            $('#profile button').attr('disabled', 'disabled');

        } else {
            $('.loaderDiv').hide();
            $('#addvideo_popup button,input[type="text"]').removeAttr('disabled');
            $('#profile button').removeAttr('disabled');
        }
    }
    function openGeoBlock(obj){
        //var dv = '<div class="modal-dialog modal-lg"><div class="modal-content" style="height: 180px;overflow: hidden;    padding-top: 20px;text-align: center;"><h3>Loading...</h3></div></div>';
        var pg_product_id = $(obj).attr('data-pg_product_id');
        var url = '<?php echo Yii::app()->baseUrl; ?>/store/OpenGeoCategory';
        //$("#ppvPopup").html(dv).modal('show');
        $.post(url,{'pg_product_id':pg_product_id}, function (res) {
            $("#ppvPopup").html(res).modal('show');
        });
    }
</script>
