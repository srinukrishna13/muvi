<link href="<?php echo ASSETS_URL; ?>css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .bootstrap-tagsinput{width: 100%}
</style>

<?php

    $selcontents = '';
    if (@$content) {
        $selcontents = json_encode($content); 
    }
?>
<script>
    sel_contents = '<?php echo $selcontents;?>';
</script>
    

<div class="modal-dialog modal-lg">
    <form action="javascript:void(0);" method="post" name="adv_form" id="adv_form" data-toggle="validator">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Pre-Ordering Items</h4>
            </div>
            
            <input type="hidden" name="data[id]" value="<?php echo $data['id'];?>" />
            <input type="hidden" name="data[allstatus]" value=""  id="status_id" />
            <div class="modal-body" >
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row m-b-20">
                            <div class="col-sm-3">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm single-title" placeholder="Title" name="data[title]" value="<?php echo $data['title']; ?>" autocomplete="off" />
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-12">
                        <div id="single_part_box">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        Non-subscribers   
                                                    </div>
                                                     <div class="col-sm-6">
                                                        Subscribers
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="fees" class="col-sm-2 control-label">
                                        <?php if ($cnt_single == 0) { ?>Fees:
                                        <?php } else { ?>&nbsp;
                                        <?php } ?>
                                    </label>
                                    
                                    <div class="col-sm-10">
                                        <div id="single_parent_box">
                                        <?php if (isset($pricing) && !empty($pricing)) { 
                                            
                                            $cnt_single = 0;
                                            foreach ($pricing as $key => $value) {
                                               
                                                $price_for_unsubscribed = $value['price_for_unsubscribe'];
                                                $price_for_subscribed = $value['price_for_subscribe'];

                                                $symbol = $value['symbol'];
                                                $pricing_id = $value['ppv_pricing_id'];
                                                $currency_id = $value['currency_id'];
                                                $code= $value['code'];                                                
                                            ?>
                                            <div class="row m-b-20">
                                                <!-- Checkbox-->
                                                <div class="col-sm-3">
                                                    <div class="row">
                                                        <div class="col-sm-3 m-t-10">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="singlechk" name="data[status][]" value="<?php echo $currency_id;?>" checked="checked" <?php if ($this->studio->default_currency_id == $currency_id) {?>disabled="true" checked="checked"<?php }?> /><i class="input-helper"></i>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 p-l-0 p-r-0">
                                                            <input type="hidden" name="data[pricing_id][]" value="<?php echo $pricing_id;?>" />
                                                            <div class="fg-line">
                                                                <div class="select">
                                                                    <select class="form-control input-sm currency" name="data[currency_id][]" <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                                                        <?php foreach ($currency as $key1 => $value1) {print_r($value1) ?>
                                                                        <option value="<?php echo $currency_id;?>" <?php if ($currency_id == $value1['id']) {?>selected="selected"<?php }?>><?php echo $value1['code'];?>(<?php echo $value1['symbol'];?>)</option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Subscriber and Non Subscriber-->
                                                <div class="col-sm-9">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_unsubscribed][]" value="<?php echo $price_for_unsubscribed; ?>" autocomplete="off" />
                                                            </div>
                                                            <small class="help-block"><label id="data[price_for_unsubscribed][]-error" class="error red" for="data[price_for_unsubscribed][]" style="display: none;"></label></small>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_subscribed][]" value="<?php echo $price_for_subscribed; ?>" autocomplete="off" />
                                                            </div>
                                                            <small class="help-block"><label id="data[price_for_subscribed][]-error" class="error red" for="data[price_for_subscribed][]" style="display: none;"></label></small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <?php 
                                            $cnt_single++;
                                            }
                                        } else { ?>
                                            <div class="row m-b-20">
                                                <!-- Checkbox-->
                                                <div class="col-sm-3">
                                                    <div class="row">
                                                        <div class="col-sm-3 m-t-10">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="singlechk" name="data[status][]" checked="checked" disabled="disabled" value="<?php echo $studio->default_currency_id; ?>" /><i class="input-helper"></i>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 p-l-0 p-r-0">
                                                            <input type="hidden" name="data[pricing_id][]" value="" />
                                                            <div class="fg-line">
                                                                <div class="select">
                                                                    <select class="form-control input-sm currency" name="data[currency_id][]" disabled="disabled">
                                                                        <?php foreach ($currency as $key => $value) { ?>
                                                                            <option value="<?php echo $value['id']; ?>" <?php if ($studio->default_currency_id == $value['id']) { ?>selected="selected"<?php } ?>><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Subscriber and Non Subscriber-->
                                                <div class="col-sm-9">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_unsubscribed][]" value="" autocomplete="off" />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_subscribed][]" value="" autocomplete="off" />
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        </div>
                                        
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <small class="help-block"><label id="data[price_for_unsubscribed][]-error" class="error red" for="data[price_for_unsubscribed][]" style="display: none;"></label></small>
                                                </div>
                                                 <div class="col-sm-6">
                                                    <small class="help-block"><label id="data[price_for_subscribed][]-error" class="error red" for="data[price_for_subscribed][]" style="display: none;"></label></small>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!-- Add Specific price category-->
                                        <div class="row">
                                            <div class="col-md-12 text-left">
                                                <a href="javascript:void(0);" onclick="addMore('single', 0);" class="text-gray">
                                                    <em class="icon-plus blue"></em> Add more price for specific country
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="content">Content: </label>
                                    <div class="col-sm-10">

                                        <div class="fg-line">
                                            <select class="input-sm" data-role="tagsinput" name="data[content][]" placeholder="Type to add new content" id="content" multiple>
                                            </select>
                                        </div>
                                        <small class="help-block">
                                            <label id="data[content][]-error" class="error red" for="data[content][]" style="display: none"></label>
                                        </small>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="valid_until">Valid Until: </label>
                                    <div class="col-sm-10">
                                        <div class="fg-line">
                                            <input class="form-control input-sm" type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'" id="expiry_date" name="data[expiry_date]" value="<?php echo isset($data['expiry_date']) ? date('m/d/Y', strtotime($data['expiry_date'])) : ''; ?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="description">Description: </label>
                                    <div class="col-sm-10">
                                        <div class="fg-line">
                                            <textarea class="form-control input-sm auto-size" placeholder="Pre-purchase this content at a discounted rate. We will notify when it becomes available." name="data[description]" value="" autocomplete="off"><?php echo $data['description'];?></textarea>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" <?php if ($type ==3) { ?> onclick="return validateMultiAdvForm();" <?php } else { ?> onclick="return validateSingleAdvForm();" <?php } ?> id="bill-btn"><?php if (isset($pricing) && !empty($pricing)) { ?> Update<?php } else {?>Add<?php } ?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>	
</div>

<div id="single-data" style="display: none;">
    <div class="row m-b-20">
        <!-- Checkbox-->
        <div class="col-sm-3">
            <div class="row">
                <div class="col-sm-3 m-t-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="singlechk" name="data[status][]"  value="<?php echo $currency[0]->id; ?>" /><i class="input-helper"></i>
                        </label>
                    </div>
                </div>
                <div class="col-sm-8 p-l-0 p-r-0">
                    <input type="hidden" name="data[pricing_id][]" value="" />
                    <div class="fg-line">
                        <div class="select">
                            <select class="form-control input-sm currency" name="data[currency_id][]">
                                <?php foreach ($currency as $key => $value) { ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Subscriber and Non Subscriber-->
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-6">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_unsubscribed][]" value="" autocomplete="off" />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_subscribed][]" value="" autocomplete="off" />
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-12 m-t-20 text-right">
            <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
                <em class="icon-trash blue" ></em>&nbsp; Remove
            </a>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript">
    var gmtDate = '<?= gmdate('d'); ?>';
    var gmtMonth = '<?= gmdate('m'); ?>';
    var gmtYear = '<?php echo gmdate('Y'); ?>';
    
    //$("#expiry_date").datepicker({changeMonth: !0, changeYear: !0, minDate: $("#expiry_date").val() ? new Date($("#expiry_date").val()) : new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)})
    $("#expiry_date").datepicker({changeMonth: !0, changeYear: !0, minDate: new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)})
                    
    Array.prototype.getUnique = function(){
       var u = {}, a = [];
       for(var i = 0, l = this.length; i < l; ++i){
          if(u.hasOwnProperty(this[i])) {
             continue;
          }
          a.push(this[i]);
          u[this[i]] = 1;
       }
       return a;
    }
    
    function addMore(type, flag) {
        var isTrue = 0;
        $("#"+type+"_part_box").find("."+type+"-cost").each(function() {
            if ($.trim($(this).val()) !== '') {
                isTrue = 1;
            } else {
                isTrue = 0;
                $(this).focus();return false;
            }
        });
        
        if (parseInt(flag)) {
            isTrue = 1;
        }
        
        if (parseInt(isTrue)) {
            var str ='';
            if (type === 'single') {
                str = $("#single-data").html();
            } else if (type === 'multi') {
                str = $("#multi-data").html();
            }
            
            $("#"+type+"_parent_box").append(str);
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });
            $('.cost').on("contextmenu",function(event) {
                return false;
            });
        }
    }
    
    function removeBox(obj) {
        $(obj).parent().parent().remove();
    }
       
    function validateSingleAdvForm() {    
        var validate = $("#adv_form").validate({
            rules: {
                'data[title]': {
                    required: true
                },
                'data[frequency]': {
                    required: true
                },
                'data[expiry_date]': {
                    required: true
                },
                'data[price_for_unsubscribed][]': {
                    required: true,
                    allrequired: true,
                    price: true,
                    uniquecurrency: true
                },
                'data[price_for_subscribed][]': {
                    required: true,
                    allrequired: true,
                    price: true,
                    uniquecurrency: true
                }
            },
            messages: {
                'data[title]': {
                    required: 'Please enter the title'
                },
                'data[expiry_date]': {
                    required: 'Please add valid date'
                },
                'data[price_for_unsubscribed][]': {
                    required: 'Please enter fee',
                    allrequired: 'Please enter fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                },
                'data[price_for_subscribed][]': {
                    required: 'Please enter fee',
                    allrequired: 'Please enter fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                }
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
        var x = validate.form();
        
        if (x) {
            if (!$.trim($("#content").val())) {
                swal("Oops! The given content doesn't exists in your studio");
                return false;
            } else {
                var currency = new Array();

                $("#single_part_box").find(".singlechk").each(function(){
                    if ($(this).is(':checked')) {
                        currency.push($(this).parent().parent().parent().next('div').find('select').val());
                    }
                });

                $("#status_id").val(currency.getUnique().toString());

                $(".singlechk").each(function(){
                    $(this).prop("disabled", false);
                });

                $(".currency").each(function(){
                    $(this).prop("disabled", false);
                });

                var url = "<?php echo Yii::app()->baseUrl; ?>/store/addEditPreorder";
                document.adv_form.action = url;
                document.adv_form.submit();
            }
        }
    }

jQuery.validator.addMethod("allrequired", function (value, element) {
    var isTrue = 1;
    $("#single_parent_box").find(".singlechk").each(function(){
        if ($(this).is(':checked')) {
            var cost = $(this).parent().parent().parent().parent().parent().next('div').find('.single-cost');
            cost.each(function(){
                var price = $(this).val();
                
                if (price === '') {
                    isTrue = 0;
                    return false;
                }
            });
        }
    });
    
    if (isTrue) {
        return true;
    }
}, "Please enter fee");

jQuery.validator.addMethod("price", function (value, element) {
    var isTrue = 1;
    $("#single_parent_box").find(".singlechk").each(function(){
        if ($(this).is(':checked')) {
            var cost = $(this).parent().parent().parent().parent().parent().next('div').find('.single-cost');
            cost.each(function(){
                var price = $(this).val();
                
                price = $.trim(price);
                price = Math.floor(price * 100) / 100;
                price = Math.round(price);
                if (price <= 0) {
                    isTrue = 0;
                    return false;
                }
            });
        }
    });
    
    if (isTrue) {
        return true;
    }
}, "Minimum price should be 50 cent");

jQuery.validator.addMethod("uniquecurrency", function (value, element) {
    var currency = new Array();
    
    $("#single_parent_box").find(".singlechk").each(function(){
        if ($(this).is(':checked')) {
            currency.push($(this).parent().parent().parent().next('div').find('select').val());
        }
    });
    
    var cur = currency.getUnique();
    if (cur.length === currency.length) {
        return true;
    } else {
        return false;
    }
}, "Currency should be unique");
</script>