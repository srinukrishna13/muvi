<?php
$sortby = isset($_GET['sortBy']) ? $_GET['sortBy'] : '';
?>
<?php $url = $this->createUrl('store/order'); ?>
<div class="row m-b-40"></div>
<div class="filter-div">

    <div class="row">
        <div class="col-md-9 m-b-10">
            <form id="searchform" action="<?php echo $url; ?>" name="searchForm" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group  input-group">
                            <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                            <div class="fg-line">
                                <input type="text" class="filter_input form-control fg-input" id="search" placeholder="Enter order number,item name or customer name" value="<?php if (isset($_GET['search']) && trim($_GET['search'])) echo urldecode(trim($_GET['search'])); ?>" name="search">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table" id="example">
            <thead>
                <?php if ($items_count >= 3) { ?>
                    <tr>
                        <td colspan="8" class="text-right">
                            <?php
                            $opts = array('class' => 'pagination m-t-0 m-b-40');
                            $this->widget('CLinkPager', array(
                                'currentPage' => $pages->getCurrentPage(),
                                'itemCount' => $items_count,
                                'pageSize' => $page_size,
                                "htmlOptions" => $opts,
                                'maxButtonCount' => 6,
                                'nextPageLabel' => '&raquo;',
                                'prevPageLabel' => '&laquo;',
                                'selectedPageCssClass' => 'active',
                                'lastPageLabel' => '',
                                'firstPageLabel' => '',
                                'header' => '',
                            ));
                            ?>

                        </td>
                    </tr>
                <?php }
                ?>
                <tr>
                    <th>Order Number</th>
                    <th>Date</th>
                    <th>Customer Name</th>
                    <th>Order Status</th>
                    <th>Item Name</th>
                    <th>Total Amount</th>

                </tr>	
            </thead>

            <tbody>
                <?php
                $first = $cnt = 1;
                if ($data) {
                    $first = $cnt = (($pages->getCurrentPage()) * 3) + 1;



                    foreach ($data AS $key => $details) {
                        $cnt++;
                        ?>

                        <tr>
                            <td>
                                <a href="javascript:void(0);">#<?php echo $details['order_number']; ?></a>

                            </td>

                            <td><?php echo $details['date'] ? date('jS M ,Y', strtotime($details['date'])) : 'NA'; ?></td>   

                            <td><?php echo $details['customer_name']; ?></td>
                            <td><?php echo $details['order_status']; ?> </td>
                            <td> <?php
                                foreach ($details['details'] as $key => $value) {
                                    echo 'NAME-' . $value['name'] . '<br/>';
                                    echo 'PRICE- $' . $value['price'] . '<br/>';
                                    echo 'QUANTITY-' . $value['quantity'].'<br/>'.'<br/>';
                                }
                                ?>  
                            </td>

                            <td> <?php echo Yii::app()->common->formatPrice($details['total_amount'], 153); ?>    </td>     

                        </tr>

                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="5">No Record found!!!</td>
                    </tr>	

                <?php } ?>
            </tbody>
        </table>
    </div>
</div>


    <script type="text/javascript">
        function sortby(sortby, order) {
            var BASE_URL = "<?php echo Yii::app()->getBaseUrl(true); ?>";
            var sort_uri = "<?php print isset($_GET['sortBy']) ? $_GET['sortBy'] : ''  ?>";
            if (sortby + '_' + order == sort_uri)
                order = desc;
            var pathname = window.location.pathname;
            var search = document.getElementById("search").value;

            window.location.href = url;
        }
    </script>