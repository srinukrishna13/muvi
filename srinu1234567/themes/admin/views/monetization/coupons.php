<style type="text/css">
    .ui-datepicker{
        z-index: 99999 !important;
    }
</style>
<?php
$studio = $this->studio;
?>

<div class="row m-b-20 m-t-40">
    <div class="col-sm-4">
        <div class="form-group  input-group">
            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
            <div class="fg-line">
                <input class="filter_input form-control fg-input" id="search_content" placeholder="What are you searching for?" type="text">
            </div>
        </div>
    </div> 
    <div class="col-sm-4">
        <button class=" topper btn btn-primary"  class="btn btn-primary margin-bottom" data-toggle="modal" data-target="#myModal" > New Coupon </button>
        <button class=" topper btn btn-primary" onclick="couponcsv_download();">Download CSV </button>
        <button class=" topper btn btn-default-with-bg"  class="btn btn-primary margin-bottom" onclick="couponmsg_check();">Delete</button>
       
    </div> 
    <div class="col-sm-4 ">
         
        <?php
           if($items_count>=20){ 
               $opts = array('class' => 'pagination m-t-0 pull-right');
                                    $this->widget('CLinkPager', array(
                                        'currentPage'=>$pages->getCurrentPage(),
                                        'itemCount'=>$items_count,
                                        'pageSize'=>$page_size,
                                        "htmlOptions" => $opts,
                                        'maxButtonCount'=>6,
                                        'nextPageLabel' => '&raquo;',
                                        'prevPageLabel'=>'&laquo;',
                                        'selectedPageCssClass' => 'active',
                                        'lastPageLabel'=>'',
                                        'firstPageLabel'=>'',
                                        'header'=>'',
                                    ));
           }
        ?>
    </div>
    
</div>
<?php
if(isset($menu['menu']) && ($menu['menu'] & 1) || ($menu['menu'] & 256)){
?>
<div class="bs-example bs-example-tabs" data-example-id="togglable-tabs" role="tabpanel">
    <ul role="tablist" class="nav nav-tabs" id="myTab" style="width: 100%;">

        <li role="presentation" <?php if((!isset($_GET['type'])) || ((isset($_GET['type']) && trim($_GET['type'])==1))) { ?>class="active" <?php } ?>>
            <a aria-controls="paid" data-toggle="tab" id="paid-tab" role="tab" href="#paid" onclick="showListDiv(1)" aria-expanded="true">One Time (PPV)</a>
        </li>
        <li role="presentation" <?php if(isset($_GET['type']) && trim($_GET['type'])==2) { ?>class="active" <?php } ?>>
            <a aria-controls="lead" data-toggle="tab" id="lead-tab" role="tab" href="#lead" onclick="showListDiv(2);" aria-expanded="false">Recurring (Subscription)</a>
        </li>

    </ul>
</div>
<?php }?>
<div class="row">
    <div class="col-md-12">
        <form method="post" action="javascript:void(0);" id="mngCouponForm" name="mngCouponForm" >
            <table class="table table-hover" id="coupon_table">
                <thead>
                
                    <tr>
                        <th class="max-width-40"><div class="checkbox m-b-0"><label><input class="chkall" id="selectall" type="checkbox" name="checkall" onclick="checkUncheckAll();" /><i class="input-helper"></i> </label></div></th>
                        <th>Coupon</th>
                        <th>Coupon Type</th>

<!--                <th data-hide="phone">Used by a single user</th>-->
                <th data-hide="phone">Valid?</th> 
                <th data-hide="phone">Used?</th>
                <th data-hide="phone">User</th>
                <th data-hide="phone">Used Date</th>
                </tr>
                </thead>
                <tbody>

                    <?php
              
                    $first=$cnt =1;
                    if($data){
                            $first = $cnt=(($pages->getCurrentPage())*20)+1;
                    foreach ($data as $key => $value) {
                         $cnt++;
               
                        $ccode = $value['coupon_code'];//exit;
                            ?>
                    <tr>
                        <?php //echo $value['id'];?>
                            <input type="hidden" name='studio_id' value='<?php echo $value['studio_id']; ?>' />
                            <td>
                                <div class="checkbox m-b-0">
                                    <label>
                                        <input class="chkind" type="checkbox" name="data[]" value="<?php echo $value['id'];?>" onclick="checkUncheckAll(1);" />
                                        <i class="input-helper"></i> 
                                    </label>
                                </div>
                            </td>
                                                                          
                            <td style="color:#33CCCC;"> <a href="#" data-toggle="modal" data-target="#myModal1" onclick="Coupon_history('<?php echo $ccode; ?>')" name="coupon_history"><?php echo $value['coupon_code']; ?></a></td>
                            <?php
                        echo "<td>";
                        if ($value['coupon_type'] == 1) {
                            echo "Multi-use";
                        } else {
                            echo "Once-use";
                        }
                        echo "</td>";
//                        echo "<td>";
//                        if ($value['user_can_use'] == 1) {
//                            echo "Multiple times";
//                        } else {
//                            echo "Once";
//                        }
//                        echo "</td>";
                        ?>
                        <?php
                        $now = strtotime(date("Y-m-d")); // or your date as well
                        $cpn_date = strtotime($value['valid_until']);
                        $datediff = $cpn_date - $now;
                        $diff;
                        if (($value['used_by'] == 0 && $datediff >= 0) || ($value['coupon_type'] == 1 && $datediff >= 0)) {
                            ?>
                            <td><?php echo 'Yes'; ?></td>
                            <?php
                        } else {
                            ?>
                                <td><?php echo 'No';?></td>
                            <?php 
                            } 
                            if($value['used_by']!=0){
                                ?>
                                <td><?php echo 'Yes' ;?></td>
                            <?php }else{?>
                                    <td><?php echo '-';?></td>
                            <?php } ?>  

                        <?php if ($value['used_by'] == '0') { ?>
                            <td><?php echo '-'; ?></td>
                            <?php
                        } else if ($value['used_by'] != '0' && $value['coupon_type'] == 1) {
                            echo "<td><a href='#' data-toggle='modal' data-target='#showUserList' onclick='showUserEmail(" . $value['id'] . ",".$_GET['type'].");'>Show User List</span></td>";
                        } else {
                            ?>
                            <td><?php echo Yii::app()->common->getuseremail($value['used_by']); ?></td>
                            <?php } ?>
                            <?php if($value['used_by']==0){?>
                            <td><?php echo '-';?></td>
                            <?php }else{?>
                                    <td><?php echo date('M jS, Y',strtotime($value['used_date']));?></td>
                            <?php } 
                            ?>


                    </tr>
      <!-- /.box-header -->

                    <?php }
                } else {
                    ?>
                    <tr>
                            <td colspan="5">No Record found!!!</td>
                    </tr>	
            <?php	}?>
            </tbody>
        </table>
    </form>
    </div>
</div>

<!--here goes the code for modal-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close m-t-20" data-dismiss="modal">&times;</button>
                <h4 class="modal-title p-t-20" class="coupon_title" >New Coupon</h4> 
                 <div class="form-horizontal p-t-20">
                        <div class="row">
                            <div class="col-md-8">
                                
                        </div>
                    </div>
                    <div class="errorTxt red"></div>
                    <?php
                    if(isset($menu['menu']) && ($menu['menu'] & 1) || ($menu['menu'] & 256)){
                    ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 padd-t-0" for="Type"> Payment Type:</label>
                        <div class="radio m-b-20 col-md-9">
                            <div class="col-md-4 row">
                            <label>
                                <input id="pay_onetime" type="radio" checked="checked" name="payment_type" value="0" onclick="showPaymentTypeInput('onetime')">
                              <i class="input-helper"></i>
                              One Time (PPV)
                            </label>
                            </div>
                            
                            <div class="col-md-8 row">
                            <label>
                                <input id="pay_recurring" type="radio" name="payment_type" value="1" onclick="showPaymentTypeInput('recurring')">
                              <i class="input-helper"></i>
                              Recurring (Subscription)
                            </label>
                            </div>
                          </div>
                    </div>
                    <?php }?>
                </div>
                
                 <div class="form-horizontal">
                        
                    <div class="errorTxt red"></div>
                    <div class="form-group">
                        <label class="control-label col-md-3 padd-t-0" for="Type"> Coupon Type:</label>
                        <div class="radio m-b-20 col-md-9">
                            <div class="col-md-4 row">
                            <label>
                              <input id="amount_cash" type="radio" checked="checked" name="show_div" value="0" onclick="showCouponDiv('single_coupon');">
                              <i class="input-helper"></i>
                              Once-use
                            </label>
                                <div id="coupon_message" class="grey"></div>
                            </div>
                            
                            <div class="col-md-8 row">
                            <label>
                              <input id="amount_prcnt" type="radio" name="show_div" value="1" onclick="showCouponDiv('multiple_coupon');">
                              <i class="input-helper"></i>
                              Multi-use
                            </label>
                            
                          </div>
                          </div>
                        
<!--                        <div class="col-sm-7 m-t-10">
                            <lable class="radio radio-inline m-r-20">  <input id="amount_cash" type="radio" checked="checked" name="show_div" value="0" onclick="showCouponDiv('single_coupon');"><i class="input-helper"></i> Once-use</lable>
                            <lable class="radio radio-inline m-r-20">  <input id="amount_prcnt" type="radio" name="show_div" value="1" onclick="showCouponDiv('multiple_coupon');"><i class="input-helper"></i> Multi-use</lable><br/>
                            <div id="coupon_message" class="grey"></div>
                        </div>-->
                    </div>
                </div>
             
            <form class="form-horizontal" action="javascript:void(0)" name="single_coupon" id="single_coupon" method="post">
                    <input type="hidden" name="payment_type_onceuse" id="payment_type_onceuse" value="onetime">
                    <input type="hidden"  name="coupon_type" value="0"/>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="Coupons"> # of Coupons:</label>
                        <div class="col-md-9">
                            <div class="fg-line">
                                <input name="no_of_coupon" autocomplete="off" id="no_of_coupon" onKeyPress="checkValueOfNumb(this)" onKeyUp="checkValueOfNumb(this)" class="form-control input-sm" type="text" required="required" placeholder="Value between 1-500">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label padd-t-0" for="Discount"> Discount:</label>
                        
                        <div class="radio m-b-20 col-md-9">
                            <div class="col-md-2 row">
                            <label>
                              <input id="amount_cash" type="radio" class="amount_cash_once" checked="checked"  name="amount_cash" value="0" onclick="changeDiscountCurr('0', 'coupon_disc_amt_acr_type', 'once');">
                              <i class="input-helper"></i>
                              Cash
                            </label>
                            </div>
                            <div class="col-md-8 row">
                            <label>
                              <input id="amount_prcnt" type="radio" class="amount_cash_once"   name="amount_cash" value="1" onclick="showValOnly();">
                              <i class="input-helper"></i>
                              %
                            </label>
                            </div>
                            
                            <label id="recurring_payment"  style="display: none;">(Applies to First Billing Cycle)
                            </label>
                            <div id="coupon_message" class="grey"></div>
                          </div>
                        
<!--                        <div class="col-sm-7 m-t-10">
                            <lable class="radio radio-inline m-r-20"><input id="amount_cash" type="radio" class="amount_cash_once" checked="checked"  name="amount_cash" value="0" onclick="changeDiscountCurr('0', 'coupon_disc_amt_acr_type', 'once');"><i class="input-helper"></i> Cash</lable>
                                <lable class="radio radio-inline m-r-20"><input id="amount_prcnt" type="radio" class="amount_cash_once"   name="amount_cash" value="1" onclick="changeDiscountCurr('1','coupon_disc_amt_acr_type','once');"><i class="input-helper"></i> %</lable><br/>
                            <lable class="radio radio-inline m-r-20"><input id="amount_prcnt" type="radio" class="amount_cash_once"   name="amount_cash" value="1" onclick="showValOnly();"><i class="input-helper"></i> %</lable><br/>

                        </div>-->
                        <div id="coupon_disc_amt_acr_type" class="col-sm-12"></div>
                        <div class="col-sm-offset-3 col-sm-3" id="valonly" style="display: none;">
                            <div class="fg-line">
                                <input id="amount_value" autocomplete="off" class="form-control input-sm cost multi-cost" type="text" required="required"  name="amount_val" placeholder="Value" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="free_trail_div_onceuse" style="display: none;">
                        <label class="col-md-3 control-label" for="Valid"> Free Trial:</label>
                        <div class="col-md-9">
                            <div class='row'>
                                <div class='col-md-6'>
                                    <div class="input-group">
                                        <div class="fg-line">
                                            <input id="free_trail_onceuse" onKeyPress="checkValueOfFreeTrail(this)" onKeyUp="checkValueOfFreeTrail(this)" class="form-control input-sm" type="text" autocomplete="off" name="free_trail_onceuse">
                                        </div>
                                        <span class="input-group-addon border0">Days</span>
                                        <span id="err_dt" style="color:#f55753"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="discount_cycle_div_onceuse" style="display: none;">
                        <label class="col-md-4 control-label" for="Valid"> Discount Multiple Cycle:</label>
                        <div class="col-md-7">
                            <div class='row'>
                                <div class='col-md-6'>
                                    <div class="input-group">
                                        <div class="fg-line">
                                            <input id="discount_cycle_onceuse" class="form-control input-sm" type="text" required="required"  autocomplete="off" name="discount_cycle_onceuse">
                                        </div>
                                        <span class="input-group-addon">Cycles</span>
                                        <span id="err_dt" style="color:#f55753"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="Valid"> Valid Until:</label>

                        <div class="col-md-9">
                            <div class='row'>
                                <div class='col-md-6'>
                                    <div class="input-group">
                                        <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                                        <div class="fg-line">
                                            <input placeholder="From" id="datepicker" class="form-control input-sm" type="text" onchange="return checkForDate();" onkeypress="return onlyAlphabets(event, this);" required="required"  autocomplete="off" name="valid_from">
                                        </div>


                                    </div>
                                </div>
                                <div class='col-md-6'>
                                    <div class="input-group">
                                        <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                                        <div class="fg-line">
                                            <input placeholder="To" id="datepicker2" class="form-control input-md" type="text" onchange="return checkForDate()" onkeypress="return onlyAlphabets(event, this);" required="required"  autocomplete="off" name="valid_until">
                                        </div>
                                        <span id="err_dt" style="color:#f55753"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    
                <div class="modal-footer">    
                    <button id="sub-btn" class="btn btn-primary btn-sm" name="sub-btn"  onclick="return validate_coupondata();">Submit</button>
                    <button class="btn btn-default btn-sm" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </form>
            </div>

             
            
            <form class="form-horizontal" action="javascript:void(0)" name="multiple_coupon" id="multiple_coupon" method="post">

                <div class="modal-body">
                    <input type="hidden" name="payment_type_multiuse" id="payment_type_multiuse" value="">
                    <input type="hidden"  name="coupon_type" value="1"/>
                    <!--                        <div class="form-group">
                                                <label class="col-sm-4 control-label" for="Discount"> Used by a single user:</label>
                                                <div class="col-sm-7 m-t-10">
                                                    <lable class="radio radio-inline m-r-20"><input id="amount_cash" type="radio" name="user_can_use" checked="checked" value="0"><i class="input-helper"></i>  Once</lable>
                                                    <lable class="radio radio-inline m-r-20"><input id="amount_prcnt" type="radio" name="user_can_use" value="1"><i class="input-helper"></i>  Multiple times</lable>
                                                </div>
                                            </div>-->

                    <div class="form-group">
                        <label class="control-label col-md-3" for="Coupons"> Restrict usage per user:</label>
                        <div class="col-md-9">
                            <div class="fg-line">
                                <input name="restrict_user" autocomplete="off" id="restrict_user" onkeyup="checkValueOfRestrict(this)"  class="form-control input-sm" type="text" required="required" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label col-md-3" for="Coupons"> Coupon Code:</label>

                        <div class="col-md-9">
                            <div class="fg-line">
                                <input name="coupon_code" autocomplete="off" id="coupon_code" class="form-control input-sm" type="text" required="required" placeholder="Coupon Code"  
                                value="<?php

                                    $character_set_array = array();
                                    $character_set_array[] = array('count' => 5, 'characters' => 'CDEFGHIJKLMNOPQRSTUVWXYZ');
                                    $character_set_array[] = array('count' => 3, 'characters' => '0123456789');
                                    $temp_array = array();
                                    foreach ($character_set_array as $character_set) {
                                        for ($i = 0; $i < $character_set['count']; $i++) {
                                            $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
                                        }
                                    }
                                    shuffle($temp_array);
                                    $a =  implode('', $temp_array);
                                        //echo '<pre>';
                                        //print_r($a);
                                     //echo $ccode;
                                    if($a!=$ccode)
                                    {
                                        echo "$a";
                                    }else{
                                        $character_set_array = array();
                                    $character_set_array[] = array('count' => 5, 'characters' => 'CDEFGHIJKLMNOPQRSTUVWXYZ');
                                    $character_set_array[] = array('count' => 3, 'characters' => '0123456789');
                                    $temp_array = array();
                                    foreach ($character_set_array as $character_set) {
                                        for ($i = 0; $i < $character_set['count']; $i++) {
                                            $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
                                        }
                                    }
                                    shuffle($temp_array);
                                    $b =  implode('', $temp_array);
                                    echo $b;
                                    }

                                ?>">
                            </div>
                            </div>
                        </div>
                       

                    <div class="form-group">
                        <label class="col-md-3 padd-t-0 control-label" for="Discount"> Discount:</label>
                        <div class="radio m-b-20 col-md-9">
                             <div class="col-md-2 row">
                            <label>
                              <input id="amount_cash" type="radio" class="amount_cash_multi" checked="checked"  name="amount_cash" onclick="changeDiscountCurr('0', 'coupon_disc_amt_acr_type_for_multi', 'multi');" value="0">
                              <i class="input-helper"></i>
                              Cash
                            </label>
                             </div>
                            
                             <div class="col-md-2 row">
                            <label>
                              <input id="amount_prcnt" type="radio" class="amount_cash_multi"   name="amount_cash" value="1" onclick="showValOnlyMulti();">
                              <i class="input-helper"></i>
                              %
                            </label>
                            <div id="coupon_message" class="grey"></div>
                          </div>
                          </div>
                        
                        
<!--                        <div class="col-sm-7 m-t-10">
                            <lable class="radio radio-inline m-r-20"><input id="amount_cash" type="radio" class="amount_cash_multi" checked="checked"  name="amount_cash" onclick="changeDiscountCurr('0', 'coupon_disc_amt_acr_type_for_multi', 'multi');" value="0"><i class="input-helper"></i> Cash</lable>
                                <lable class="radio radio-inline m-r-20"><input id="amount_prcnt" type="radio" class="amount_cash_multi"   name="amount_cash" value="1" onclick="changeDiscountCurr('1','coupon_disc_amt_acr_type_for_multi','multi');"><i class="input-helper"></i> %</lable>
                            <lable class="radio radio-inline m-r-20"><input id="amount_prcnt" type="radio" class="amount_cash_multi"   name="amount_cash" value="1" onclick="showValOnlyMulti();"><i class="input-helper"></i> %</lable>
                        </div>-->

                        <div id="coupon_disc_amt_acr_type_for_multi" class="col-sm-12"></div>
                        <div class="col-md-offset-3 col-sm-2" id="valonlymulti" style="display: none;">
                            <div class="fg-line">
                                <input id="amount_value2" autocomplete="off" class="form-control input-sm cost multi-cost" type="text" required="required"  name="amount_val" placeholder="Value" >
                            </div>
                        </div>

                    </div>
                    <div class="form-group" id="free_trail_div_multiuse" style="display: none;">
                        <label class="col-md-3 control-label" for="Valid"> Free Trail:</label>
                        <div class="col-md-9">
                            <div class='row'>
                                <div class='col-md-6'>
                                    <div class="input-group">
                                        <div class="fg-line">
                                            <input id="free_trail_multiuse" onKeyPress="checkValueOfFreeTrail(this)" onKeyUp="checkValueOfFreeTrail(this)" class="form-control input-sm" type="text" autocomplete="off" name="free_trail_multiuse">
                                        </div>
                                        <span class="input-group-addon border0">Days</span>
                                        <span id="err_dt" style="color:#f55753"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="discount_cycle_div_multiuse" style="display: none;">
                        <label class="col-md-3 control-label" for="Valid"> Discount Multiple Cycle:</label>
                        <div class="col-md-9">
                            <div class='row'>
                                <div class='col-md-6'>
                                    <div class="input-group">
                                        <div class="fg-line">
                                            <input id="discount_cycle_multiuse" class="form-control input-sm" type="text" required="required"  autocomplete="off" name="discount_cycle_multiuse">
                                        </div>
                                        <span class="input-group-addon">Cycles</span>
                                        <span id="err_dt" style="color:#f55753"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="Discount"> Valid Until:</label>
                            
                            
                            <div class="col-md-9">
                                <div class='row'>
                                    <div class='col-md-6'>
                                        <div class="input-group">
                                <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                                <div class="fg-line">
                                    <input id="datepicker_for_no" class="form-control input-sm" type="text" onchange="return checkForDateMulti();" onkeypress="return onlyAlphabets(event,this);" required="required"  autocomplete="off" placeholder="From" name="valid_from">
                                </div>
                                
                               
                            </div>
                                    </div>
                                    <div class='col-md-6'>
                                         <div class="input-group">
                                 <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                                <div class="fg-line">
                                    <input id="datepicker_for_no2" class="form-control input-sm" type="text" onchange="return checkForDateMulti();" onkeypress="return onlyAlphabets(event,this);" required="required"  autocomplete="off" placeholder="To" name="valid_until">
                                </div>
                                <span id="err_dt2" style="color:#f55753"></span>
                                </div>
                                    </div>
                                </div>
                               
                            </div>
                            
                            
                            
                        </div>
                    </div>
                        <div class="modal-footer">  
                            <button id="sub-btn" class="btn btn-primary"  onclick="return validate_multicoupondata();">Submit</button>
                            <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                        </div>
             </form>

        </div>
    </div>
</div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Coupon History</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal"  name="hisory" id="hisory" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="coupon_code"> <div id="coupon_code_show" style="font-size:18px;"></div></label>  
                    <div class="col-sm-8"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Created_Date"> Coupon Type:</label>  
                    <div class="col-sm-8">
                        <div id="coupon_type" class="history"></div>
                    </div>
                </div>
<!--                <div class="form-group">
                    <label class="control-label col-sm-4" for="Created_Date"> Used by a single user:</label>  
                    <div class="col-sm-8">
                        <div id="coupon_use" class="history"></div>
                    </div>
                </div>-->
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Created_Date"> Created Date:</label>  
                    <div class="col-sm-8">
                         <div id="created_date" class="history"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Type"> Type :</label>     
                    <div class="col-sm-8">
                        <div id="discount_type" class="history"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Value"> Value :</label>   
                    <div class="col-sm-8">
                      <div id="discount_amount" class="history"></div>
                    </div>
                </div>
                <div class="form-group" id="res_use_div" style="display:none;">
                    <label class="col-sm-4 control-label" for="Value"> Restrict Usage :</label>   
                    <div class="col-sm-8">
                      <div id="res_use" class="history"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Valid_Until"> Valid Until :</label>   
                    <div class="col-sm-8">
                        <div id="valid_until" class="history"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Valid"> Valid?</label>     
                    <div class="col-sm-8">
                        <div id="is_valid" class="history"></div> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="used"> Used?</label>     
                    <div class="col-sm-8">
                        <div id="is_valid_" class="history"></div>
                     </div>
                </div>
                <div class="form-group">
                   <label class="col-sm-4 control-label" for="used1"> Used By?</label>     
                    <div class="col-sm-8">
                        <div id="used_by" class="history"></div>
                    </div>
                </div>
                <div class="form-group" >
                    <label class="col-sm-4 control-label" for="used2"> Used On Date :</label>       
                    <div class="col-sm-8">
                        <div id="used_date" class="history"></div>
                     </div>
                </div>
                <div class="form-group" >
                    <label class="col-sm-4 control-label" for="used2"> Extend free trail :</label>       
                    <div class="col-sm-8">
                        <div id="eft" class="history"></div>
                     </div>
                </div>
                <div class="form-group" >
                    <label class="col-sm-4 control-label" for="used2"> Discount multiple cycle :</label>       
                    <div class="col-sm-8">
                        <div id="dmc" class="history"></div>
                     </div>
                </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
       
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="deleteModal" role="dialog" style="overflow-y:hidden !important;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Delete Coupon?</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <span id="coupon-msg">Are you sure  you want to<b> delete coupons</b> </span> 
                    </div>
                </div>
                <div class="modal-footer">

                    <a href="javascript:void(0);" onclick="deleteAllCoupon();" id="sub-btn"  class="btn btn-default delete_coupon coupon_yes">Yes</a>


                    <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="showUserList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >List of user who used the coupon</h4>
            </div>
            <div class="modal-body">
                <div id="listofUser"></div>
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

              </div>
        </div>
    </div>
</div>


     
<div id="coupon_currency_dup_field" style="display: none" >
   
    <div class="form-group">
        <div class="col-md-offset-3 col-md-3">
            <div class="fg-line">
                <div class="select">
                    <select class="form-control input-sm currency" name="data[currency_id][]">
                        <?php foreach ($currency as $key => $value) { ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option><?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="fg-line">
                <input id="amount_value" autocomplete="off" class="form-control input-sm cost multi-cost" type="text" required="required"  name="data[amount_value][]" <?php if($discountType == 1){?> onblur="handleChange(this)" <?php } ?> placeholder="Value">
            </div>
        </div>
        <div class="col-md-2 row m-t-15 text-right">
            <a href="javascript:void(0);" onclick="removeBox(this);" class="text-black">
                <em class="icon-trash"></em>&nbsp; Delete
            </a>
        </div>
        <div class='clearfix'></div>
    </div>
</div> 


<script>
var search = "";
<?php if((!isset($_GET['type'])) || ((isset($_GET['type']) && trim($_GET['type'])==1))) {?>
    search ="type/1";     
<?php }else if(isset($_GET['type']) && trim($_GET['type'])==2) {?>
    search ="type/2";
<?php }?>
$(function() {
    if (localStorage.getItem('details_list')) {
        $("#details_list option").eq(localStorage.getItem('details_list')).prop('selected', true);
    }

    $("#details_list").on('change', function() {
        localStorage.setItem('details_list', $('option:selected', this).index());
    });
});    

function showListDiv(type){
    var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/coupons/";
    //if (parseInt(type)) {
        url+= "type/"+type;
    //}
    window.location.href = url;
}

function getCouponDetails(searchText){
    $('.loader').show();
    $.post('/monetization/showCouponSearch/'+search,{search_text:searchText},function(res){
        $('#coupon_table').html(res);
        $('.loader').hide();

    });
}
$(document.body).on('keydown','#search_content',function (event){
        var searchTextSub = $.trim($("#search_content").val());
        //alert(searchTextSub.length);
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextSub.length >= 2 || searchTextSub.length <= 2)){
            //var date_range = $('#revenue_date').val();
            getCouponDetails(searchTextSub);
        }
        
    });
</script>
<script>
    $(function () {
        $("#datepicker").datepicker({minDate: 0});
        $("#datepicker2").datepicker({minDate: 0});
        $("#datepicker_for_no").datepicker({minDate: 0});
        $("#datepicker_for_no2").datepicker({minDate: 0});
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/ajaxForCouponCurrency", {'discount_type': 0, 'coupon_type': 'once'}, function (res) {
            $("#coupon_disc_amt_acr_type").html(res);
            $("#coupon_disc_amt_acr_type_for_multi").html(res);
        });
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/ajaxForCouponCurrency", {'discount_type': 0, 'coupon_type': 'multi'}, function (res) {
            $("#coupon_disc_amt_acr_type_for_multi").html(res);
        });
    });
    $(document).ready(function () {
        showCouponDiv("single_coupon");
    });
    function couponcsv_download() {
        //alert('hi');
        window.location.href = HTTP_ROOT + "/monetization/couponcsv_download/"+search;
        return false;
    }


    function Coupon_history(coupon_code) {
         var cpn_code = coupon_code;
   // console.log(coupon_code);
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/Coupon_history/"+search;
        $.ajax({
            type: "POST",
            dataType: "json",
            url:  url,
            data: { cpn_code: coupon_code },
            success: function(data){
             
                console.log(data);
                  
                $("#modal1").modal('show');
                $('#coupon_code_show').html(data.coupon_code);
                $('#created_date').html(data.created_date);
                $('#coupon_type').html(data.coupon_type);
                $('#coupon_use').html(data.coupon_use);
            $('#discount_type').html(data.discount_type);
                
                if(data.discount_type=='0'){
                   $('#discount_type').html('Cash');
               }
               else{
                   $('#discount_type').html('%');
               }
                
                
                   $('#eft').html(data.extend_free_trail);
                
                
                
                   $('#dmc').html(data.discount_multiple_cycle);
                
               if(data.res_use!=0){
                   $('#res_use_div').show();
                   $('#res_use').html(data.res_use);
                }else{
                    $('#res_use_div').hide();
                }
               $('#discount_amount').html(data.discount_amount);
                $('#valid_until').html(data.valid_until);
               //  $('#is_valid').html(data.is_valid);
                 $('#is_valid').html(data.is_valid);
                 //$('#is_valid_').html(data.is_valid);
                if(data.cupon_used=='0'){
                   $('#is_valid_').html('No');
               }
               else{
                   $('#is_valid_').html('Yes');
               }
                $('#used_by').html(data.used_by);
               //$('#used_by').html(data.used_by);
                if(data.cupon_used=='0'){
                   $('#used_date').html('-');
               }
               else{
                   $('#used_date').html(data.used_date);
               }
                //$('#used_date').html(data.used_date);
            }
        });               
    }

  
     function checkUncheckAll(arg) {
    if (parseInt(arg)) {
        $(".chkind").prop('click', function () {
            if ($(this).is(':checked')) {
                var len = $(".chkind").length;
                var ind_len = 0;
                $(".chkind").each(function () {
                    if ($(this).is(':checked')) {
                        ind_len++;
                    }
                });

                if (parseInt(len) === parseInt(ind_len)) {
                    $(".chkall").prop("checked", true);
                }
            } else {
                $(".chkall").prop("checked", false);
            }
        });
    } else {
        $(".chkall").prop('click', function () {
            if ($(this).is(':checked')) {
                $(".chkind").prop("checked", true);
            } else {
                $(".chkind").prop("checked", false);
            }
        });
    }
}
    function deleteAllCoupon() {
        if ($(".chkind:checked").length) {
            if ($('.delete_coupon').html()) {
                var action = '<?php echo Yii::app()->baseUrl; ?>/monetization/deleteAllCoupon/'+search;
                $('#mngCouponForm').attr("action", action);
                $('#mngCouponForm').submit();
            }
        } else if ($(".chkind:checked").length == '0') {
            return false;
        }
    }
    
    $('#no_of_coupon').on("keypress",function(event) {
        return numbersonly(event);
    });
    
    $('#restrict_user').on("keypress",function(event) {
        return numbersonly(event);
    });
    
    $('#free_trail_onceuse').on("keypress",function(event) {
        return numbersonly(event);
    });
    
    $('#free_trail_multiuse').on("keypress",function(event) {
        return numbersonly(event);
    });
    
    $('#no_of_coupon').on("contextmenu",function(event) {
        return false;
    });
    
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if (unicode >= 48 && unicode <= 57)
                return true;
            else
                return false;
        }
    }
    
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57))
                return true;
            else
                return false;
        }
    }
    
    function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}
    function validate_coupondata() {
        var currency = new Array();
        var ifSubmit = 0;
        var discTyp = $('.amount_cash_once:checked').val();
        if ($("#coupon_currency_amt_once_" + discTyp).length) {
            $("#coupon_currency_amt_once_" + discTyp).find(".currency").each(function() {
                var amount_for_curr = $.trim($(this).val());
                if(jQuery.inArray(amount_for_curr, currency) != -1){
                    alert("Currency Name Repeated. Please use different currencies");
                    ifSubmit = 1;
                    return false;
                } else{
                    currency.push(amount_for_curr);
                }
            });
        }
        if(ifSubmit === 0){
            $("#single_coupon").validate({
                rules: {
                    "no_of_coupon": {
                        required: true
                    },
                    "amount_value": {
                        required: true
                    },
                    "datepicker": {
                        required: true
                    },
                    "datepicker2": {
                        required: true
                    }
                },
                messages: {
                    "no_of_coupon": {
                        required: "No of Coupon to be generated!"
                    },
                    "amount_value": {
                        required: 'Please provide your Discount!'
                    },
                    "datepicker": {
                        required: 'Please provide Valid From Date!'
                    },
                    "datepicker2": {
                        required: 'Please provide Valid To Date!'
                    }
                },
                errorPlacement: function(error, element) {
                    error.addClass('red');
                    error.insertAfter(element.parent());
                },
                submitHandler: function (form) {
                    $(".currency").each(function(){
                        $(this).prop("disabled", false);
                    });
                    document.single_coupon.action = '<?php echo $this->createUrl('monetization/addCoupons');?>';
                    document.single_coupon.submit();
                    return false;     
                }
            });
        }
    }
    function validate_multicoupondata(){
        var currency = new Array();
        var ifSubmit = 0;
        var discTyp = $('.amount_cash_multi:checked').val();
        if ($("#coupon_currency_amt_multi_" + discTyp).length) {
            $("#coupon_currency_amt_multi_" + discTyp).find(".currency").each(function() {
                var amount_for_curr = $.trim($(this).val());
                if(jQuery.inArray(amount_for_curr, currency) != -1){
                    alert("Currency Name Repeated. Please use different currencies");
                    ifSubmit = 1;
                    return false;
                } else{
                    currency.push(amount_for_curr);
                }
            });
        }
        if(ifSubmit === 0){
            $("#multiple_coupon").validate({
            rules: {
                    "coupon_code": {
                        required: true
                    },
                    "amount_value2": {
                        required: true
                    },
                    "datepicker_for_no": {
                        required: true
                    },
                    "datepicker_for_no2": {
                        required: true
                    }
                },
                messages: {
                    "coupon_code": {
                        required: "Please provide Coupon Code!"
                    },
                    "amount_value2": {
                        required: 'Please provide your Discount!'
                    },
                    "datepicker_for_no": {
                        required: 'Please provide Valid From Date!'
                    },
                    "datepicker_for_no2": {
                        required: 'Please provide Valid To Date!'
                    }
                },
                 errorPlacement: function(error, element) {
                    error.addClass('red');
                    error.insertAfter(element.parent());
                },
                submitHandler: function (form) {
                    $(".currency").each(function(){
                        $(this).prop("disabled", false);
                    });
                    document.multiple_coupon.action = '<?php echo $this->createUrl('monetization/addCoupons');?>';
                    document.multiple_coupon.submit();
                    return false;     
                }
            });
        }
    }
    function couponmsg_check() {
        if ($(".chkind:checked").length) {
            if ($(".chkind:checked").length == '0') {
                var msg = "Please check atleast one coupon";
                swal("Delete Coupon?", msg);
            }
            else if ($(".chkind:checked").length > '0') {
               swal({
                    title: "Delete Coupon?",
                    text: "Are you sure want to delete coupons?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true
                  },
                  function(){
                    deleteAllCoupon();
                  });
            }
        }else{
            var msg = "Please check atleast one coupon";
            swal("Delete Coupon?", msg);
        }
        
    }
    function showCouponDiv(divId){
        if(divId == 'single_coupon'){
             $("#coupon_message").html("Can be used only once");
            $("#single_coupon").show();
            $("#multiple_coupon").hide();
        }
        else if(divId == 'multiple_coupon'){
             $("#coupon_message").html("Can be used multiple times by multiple people");
            $("#single_coupon").hide();
            $("#multiple_coupon").show();
        }
    }
    function showPaymentTypeInput(payType){
        if(payType == 'recurring'){
            $("#payment_type_onceuse").val("recurring");
            $("#payment_type_multiuse").val("recurring");
            $("#free_trail_div_onceuse").show();
            $("#recurring_payment").show();
            //$("#discount_cycle_div_onceuse").show();
            $("#free_trail_div_multiuse").show();
            //$("#discount_cycle_div_multiuse").show();
        }else{
            $("#payment_type_onceuse").val("onetime");
            $("#payment_type_multiuse").val("onetime");
            $("#free_trail_div_onceuse").hide();
            $("#recurring_payment").hide();
            $("#discount_cycle_div_onceuse").hide();
            $("#free_trail_div_multiuse").hide();
            $("#discount_cycle_div_multiuse").hide();
        }
    }
    
    function showUserEmail(id,type){
        $("#listofUser").html("");
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/showUserlist";
        $.post(url,{id:id,type:type},function(res){
            $("#listofUser").html(res);
        });
    }
    function changeDiscountCurr(discType, divId, couponType) {
        $("#valonly").css('display', 'none');
        $("#valonlymulti").css('display', 'none');
        $("#coupon_disc_amt_acr_type").css('display', 'block');
        $("#coupon_disc_amt_acr_type_for_multi").css('display', 'block');

        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/ajaxForCouponCurrency", {'discount_type': discType, 'coupon_type': couponType}, function (res) {
            $("#" + divId).html(res);
        });
    }
    function checkValueOfNumb(input) {
        if (input.value < 0)
            input.value = 0;
        if (input.value > 500)
            input.value = 500;
    }
    
    function checkValueOfRestrict(input) {
        if (input.value < 0)
            input.value = 0;
        if (input.value >= 65535)
            input.value = 65535;
    }
    
    function checkValueOfFreeTrail(input) {
        if (input.value < 0)
            input.value = 0;
        if (input.value >= 100)
            input.value = 100;
    }
    
    function checkForDate(){
        var dt1 = $("#datepicker").val();
        var dt2 = $("#datepicker2").val();
        
        if(new Date(dt2) < new Date(dt1))
        {
            $("#datepicker2").val("");
            $("#err_dt").html("End Date should be greater then or equals to From Date!");
            return false;
        }else{
            $("#err_dt").html("");
            return true;
        }
    }
    function checkForDateMulti(){
        var dt1 = $("#datepicker_for_no").val();
        var dt2 = $("#datepicker_for_no2").val();
        
        if(new Date(dt2) < new Date(dt1))
        {
            $("#datepicker_for_no2").val("");
            $("#err_dt2").html("End Date should be greater then or equals to From Date!");
            return false;
        }else{
            $("#err_dt2").html("");
            return true;
        }
    }

    function showValOnly() {
        $("#valonly").css('display', 'block');
        $("#coupon_disc_amt_acr_type").css('display', 'none');
    }

    function showValOnlyMulti() {
        $("#valonlymulti").css('display', 'block');
        $("#coupon_disc_amt_acr_type_for_multi").css('display', 'none');
    }
    
    $("input[type='radio']:checked").each(function() {
        var idVal = $(this).attr("id");
    });
    
    function handleChange(input) {
        if (parseFloat(input.value) < 1) input.value = 1;
        if (input.value > 100) input.value = 100;
    }
    
    $(function() {
        $('#amount_value, #amount_value2').on('input', function() {
          this.value = this.value
            .replace(/[^\d.]/g, '')             // numbers and decimals only
            .replace(/(^[\d]{3})[\d]/g, '$1')   // not more than 3 digits at the beginning
            .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
            .replace(/(\.[\d]{2})./g, '$1');    // not more than 2 digits after decimal
            
            if(this.value.substring(0,1) == "0")
            {
               this.value = this.value.replace(/^0+/g, '');       	     
            } 
            if($(this).val() < 1 || $(this).val() > 100){
            $(this).val('');  
         }
    });
        
        $('#no_of_coupon, #free_trail_onceuse, #free_trail_multiuse, #restrict_user').on('input', function() {
          this.value = this.value
            .replace(/[^\d]/g, '')             // numbers and decimals only
            .replace(/(^[\d]{3})[\d]/g, '$1');   // not more than 3 digits at the beginning
    
            if(this.value.substring(0,1) == "0")
            {
               this.value = this.value.replace(/^0+/g, '');       	     
         }
            if($(this).val() < 1 || $(this).val() > 500){
            $(this).val('');  
         }
    });
    
      });
  </script>
  <style>
      .history{
         padding-top:7px;
      }
      </style>
 
