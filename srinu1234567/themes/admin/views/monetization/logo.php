<?php
$logo_path = Yii::app()->common->getInvoiceLogoPath(Yii::app()->user->studio_id);
?>
<div class="row">
    <div class="col-sm-8">
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <label class="control-label col-sm-2">Logo</label>
                    <div class="col-sm-10">
                        <button type="button" class="btn btn-default-with-bg" data-width="<?php echo $dimension['invoicelogo']['logo_width']; ?>" data-height="<?php echo $dimension['invoicelogo']['logo_height']; ?>" data-name="invoicelogo" onclick="openInvoiceImageModal(this)">Browse</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="fixedWidth--Preview m-t-20 m-b-20">
                            <img src="<?php echo $logo_path ?>" alt="<?php echo $studio->name ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>