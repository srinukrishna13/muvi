<?php 
$api_username = $api_password = $api_mode = '';
if (isset($payment_gateway_data) && !empty($payment_gateway_data) && $payment_gateway_data->short_code == 'paygate') {
    $api_username = $payment_gateway_data->api_username;
    $api_password = $payment_gateway_data->api_password;
    $api_signature = $payment_gateway_data->api_signature;
    $non_3d_secure = $payment_gateway_data->non_3d_secure;
    $api_mode = $payment_gateway_data->api_mode;
} ?>
<div class="form-group">
    <label class="control-label col-md-4"> 3D Secure ID:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <input type="text" class="form-control input-sm" name="data[api_username]" value="<?php echo $api_username; ?>" required>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">3D Secure Password:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <input type="text" class="form-control input-sm" name="data[api_password]" value="<?php echo $api_password; ?>" required>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">Non 3D Secure ID:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <input type="text" class="form-control input-sm" name="data[api_signature]" value="<?php echo $api_signature; ?>" required>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">Non 3D Secure Password:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <input type="text" class="form-control input-sm" name="data[non_3d_secure]" value="<?php echo $non_3d_secure; ?>" required>
        </div>
    </div>
</div>

<?php if (!stristr($_SERVER['HTTP_HOST'], "muvi.com")) { ?>
<div class="form-group">
    <label class="control-label col-md-4">Mode:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm" require name="data[api_mode]" id="mode">
                    <option value="sandbox" <?php if ($api_mode == 'sandbox'){ ?>selected="selected"<?php } ?>>Sandbox</option>
                    <option value="live" <?php if ($api_mode == 'live'){ ?>selected="selected"<?php } ?>>Live</option>
                </select>
            </div>
        </div>
    </div>
</div>
<?php } ?>