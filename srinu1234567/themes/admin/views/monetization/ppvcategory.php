<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Set PPV - <span class="" id="ppv_movie_name"></span> </h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <?php if (isset($ppv_categories) && !empty($ppv_categories)) { ?>
                    <form action="javascript:void(0);" class="form-horizontal" method="post" name="ppv_category_form" id="ppv_category_form" onsubmit="PPVCategoryForm();">
                        <input type="hidden" name="data[movie_id]" value="<?php echo $res['movie_id']; ?>" />

                        <div class="form-group">
                            
                                <label class="control-label col-sm-3">Category:</label>                    
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control input-sm" name="data[ppv_plan_id]" onchange="showPricing(this);">
                                                <option>Select Category</option>
                                                <?php foreach ($ppv_categories as $key => $value) { ?>
                                                    <option value="<?php echo $value->id;?>" <?php if(isset($film->ppv_plan_id) && $film->ppv_plan_id == $value->id) { ?> selected="selected"<?php } ?>><?php echo $value->title;?></option>
                                                 <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                        
                        </div>
                        
                        <?php if (isset($res['content_types_id']) && $res['content_types_id'] == 3) { ?>
                            <div class="row pricedetail" <?php if(isset($film->ppv_plan_id) && intval($film->ppv_plan_id)) { ?>style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
                                <div class="col-sm-12">
                                    <div class="row">
                                    <label class="col-sm-3">Pricing Detail:</label>
                                    </div>
                                </div>
                            </div>
                            
                            <?php foreach ($ppv_categories as $key => $value) { ?>
                            <div class="row allprice catgryprice_<?php echo $value->id;?>" <?php if(isset($film->ppv_plan_id) && $film->ppv_plan_id == $value->id) { ?> style="display: block;"<?php } else { ?> style="display: none;"<?php } ?>>
                                <?php 
                                if (isset($pricing[$value->id]) && !empty($pricing[$value->id])) {
                                    foreach ($pricing[$value->id] as $key => $value1) {
                                        $show_unsubscribed = $value1['show_unsubscribed'];
                                        $season_unsubscribed = $value1['season_unsubscribed'];
                                        $episode_unsubscribed = $value1['episode_unsubscribed'];

                                        $show_subscribed = $value1['show_subscribed'];
                                        $season_subscribed = $value1['season_subscribed'];
                                        $episode_subscribed = $value1['episode_subscribed'];

                                        $symbol = $value1['symbol'];
                                        $code= $value1['code'];
                                    ?>
                                    <div class="col-sm-12 m-t-10">
                                        <div class="row">
                                        <div class="col-sm-2">
                                            <?php echo $code;?>(<?php echo $symbol;?>)
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="row m-b-10">
                                            <div class="col-sm-3">
                                                Non-subscribers
                                            </div>

                                            <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                                            <div class="col-sm-3">
                                                Show &nbsp;&nbsp;
                                            
                                                <?php echo $show_unsubscribed;?>
                                            </div>
                                            <?php } ?>

                                            <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
                                            <div class="col-sm-3">
                                                Season &nbsp;&nbsp;
                                            
                                                <?php echo $season_unsubscribed;?>
                                            </div>
                                            <?php } ?>

                                            <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>
                                            <div class="col-sm-3">
                                                Episode &nbsp;&nbsp;
                                            
                                                <?php echo $episode_unsubscribed;?>
                                            </div>
                                            <?php } ?>
                                            </div>
                                            <div class="row ">
                                            
                                            <div class="col-sm-3">
                                                Subscribers
                                            </div>

                                            <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                                            <div class="col-sm-3">
                                                Show &nbsp;&nbsp;
                                           
                                                <?php echo $show_subscribed;?>
                                            </div>
                                            <?php } ?>

                                            <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
                                            <div class="col-sm-3">
                                                Season &nbsp;&nbsp;
                                           
                                                <?php echo $season_subscribed;?>
                                            </div>
                                            <?php } ?>

                                            <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>
                                            <div class="col-sm-3">
                                                Episode &nbsp;&nbsp;
                                            
                                                <?php echo $episode_subscribed;?>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                
                            </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="row pricedetail m-b-10 m-t-20" <?php if(isset($film->ppv_plan_id) && intval($film->ppv_plan_id)) { ?>style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
                                <div class="col-sm-12">
                                    <div class="row">
                                    <label class="col-sm-3">Pricing Detail:</label>
                                    </div>
                                </div>
                            </div>
                            <?php foreach ($ppv_categories as $key => $value) { ?>
                                <div class="row allprice catgryprice_<?php echo $value->id;?>" <?php if(isset($film->ppv_plan_id) && $film->ppv_plan_id == $value->id) { ?> style="display: block;"<?php } else { ?> style="display: none;"<?php } ?>>
                                    <?php 
                                    if (isset($pricing[$value->id]) && !empty($pricing[$value->id])) {
                                        foreach ($pricing[$value->id] as $key => $value1) {
                                            $price_for_unsubscribed = $value1['price_for_unsubscribed'];
                                            $price_for_subscribed = $value1['price_for_subscribed'];

                                            $symbol = $value1['symbol'];
                                            $code= $value1['code'];
                                        ?>
                                        <div class="col-sm-12 m-b-10">
                                            <div class="row">
                                            <div class="col-sm-3">
                                                <?php echo $code;?>(<?php echo $symbol;?>)
                                            </div>
                                            
                                            
                                            <div class="col-sm-9">
                                                <div class="row m-b-10">
                                                <div class="col-sm-12">
                                                    Non-subscribers &nbsp;&nbsp;
                                               
                                                    <?php echo $price_for_unsubscribed;?>
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-sm-12">
                                                    Subscribers &nbsp;&nbsp;
                                                
                                                    <?php echo $price_for_subscribed;?>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="divider"></div>
                                           </div>  
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                
                                </div>
                            <?php } ?>
                        <?php } ?>
                        
                        <div class="row form-group" id="sbmt-div">
                            <div class="col-sm-12 text-right  m-t-30">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                <?php } else { ?>
                    <div class="row form-group">
                        <div class="col-sm-12 red">
                            No PPV Category found for this content type.
                        </div>              
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>									
</div>

<script type="text/javascript">
    function showPricing(obj) {
        var id = $('option:selected', obj).val();
        
        if (parseInt(id)) {
            $(".pricedetail").show();
            $(".allprice").hide();
            $(".catgryprice_"+id).show();
        } else {
            $(".pricedetail").hide();
            $(".allprice").hide();
        }
    }
    
    function PPVCategoryForm() {
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/setPPVCategory";
        document.ppv_category_form.action = url;
        document.ppv_category_form.submit();
    }
</script>