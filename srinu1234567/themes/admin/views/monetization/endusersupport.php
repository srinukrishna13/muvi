<div class="alert alert-success alert-dismissable flash-msg m-t-20" style="display:none">
    <i class="icon-check"></i> &nbsp;
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <span id="success-msg"></span>
</div>
<div class="row m-t-10">
    <div class="col-md-12">
        <a href="javascript:void(0)" class="btn btn-primary" onclick="newuser();">Add Free User</a>
    </div>
</div>
<div class="row m-t-40">
    <div class="col-md-4">
        <div class="form-group input-group">
            <span class="input-group-addon p-l-0"><em class="icon-user"></em></span>
            <div class="fg-line">
                <div class="select">
                    <select class="form-control" id="users-box">
                        <option value="all">All</option>
                        <option value="registrations">Registrations</option>
                        <option value="subscriptions">Subscriptions</option>
                        <option value="ppvusers">PPV Subscriptions</option>
                        <option value="cancelled">Cancellations</option>
                        <option value="deleted">Deletions</option>
                        <option value="device_switch">Device Deleted</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group input-group">
            <span class="input-group-addon"><em class="icon-magnifier"></em></span>
            <div class="fg-line">
                <input class="search form-control" placeholder="Search by name or email" />
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <a href="javascript:void(0)" class="btn btn-default" onclick="location.reload()">Reset Filter</a>
    </div>
</div>
    
<div class="row m-t-20">
    <div class="col-md-12">
        <div id="endusersupport_list"></div>
    </div>
    <div class="col-md-12" id="pag">
        <div id="page-selection">
            <div id="page-selection-enduser" class="pull-right"></div>
        </div>
    </div>
    <input type="hidden" id="page_size" value="<?php echo $page_size?>" />
    <div class="loader text-center centerLoader" style="display:none">
        <div class="preloader pls-blue">
            <svg viewBox="25 25 50 50" class="pl-circular">
                <circle r="20" cy="50" cx="50" class="plc-path"/>
            </svg>
        </div>
    </div>
</div>
    
<!-- add free user modal starts-->
<div id="newuserPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" >
        <form action="javascript:void(0);" method="post" name="new_user_form" id="new_user_form" data-toggle="validator">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add free User</h4>
                        
                </div>
                <div class="modal-body">
                    <h5 class="f-300">*Free user will have unlimited access to all subscription and PPV content</h5>
                    <div class="form-group">
                        <div id="errors" class="help-block col-sm-12 error red" style="padding-bottom:15px;"></div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group" >
                            <label class="col-sm-3 toper  control-label" for="First Name" > Name:</label>
                            <div class="col-sm-9">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" name="data[Admin][first_name]" autocomplete="off" required="true"/>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-sm-3 toper  control-label" for="Email">Email Address:</label>
                            <div class="col-sm-9">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" name="data[Admin][email]" id="email" autocomplete="off" required="true"/>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-sm-3 toper  control-label" for="Role">Password:</label>
                            <div class="col-sm-9">
                                <div class="fg-line">
                                    <input type="password" class="form-control input-sm" name="data[Admin][password]" autocomplete="off" required="true"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="return validateNewUser('endusersupport');" id="newmembership">Submit</button>
                </div>
            </div>
        </form>	
    </div>
</div>
<!-- add free user modal ends-->


<!-- update email modal starts-->
<div id="emailPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" >
        <form action="javascript:void(0);" method="post" name="update_email_address" id="update_email_address" data-toggle="validator">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Update Email Address of User</h4>
                        
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group" >
                            <label class="col-sm-12 toper control-label red" id="update_email_errors" style="display: none;" for="First Name" ></label>
                        </div>
                        
                        <div class="form-group" >
                            <label class="col-sm-3 toper  control-label" for="First Name" > Old Email Address:</label>
                            <div class="col-sm-9">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" name="data[Admin][old_email]" id="old_email" autocomplete="off" required="true"/>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-sm-3 toper  control-label" for="Email">New Email Address:</label>
                            <div class="col-sm-9">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" name="data[Admin][new_email]" id="new_email" autocomplete="off" required="true"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="return updateEmailAddress();" id="newemail">Submit</button>
                </div>
            </div>
        </form>	
    </div>
</div>
<!-- update email modal ends-->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="user-profile">
        </div>
    </div>
</div>    
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/reports.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<script>
    $(function() {
        var userType = $("#users-box").val();
        var searchText = $.trim($(".search").val());
        getUserList(userType,searchText);
    });
    
    $('#users-box').change(function(){
        var userType = $("#users-box").val();
        var searchText = $.trim($(".search").val());
        getUserList(userType,searchText);
    });
    
    $(document.body).on('keydown','.search',function (event){
        var searchText = $.trim($(".search").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchText.length > 2 || searchText.length <= 0)){
            var userType = $("#users-box").val();
            var searchText = $.trim($(".search").val());
            getUserList(userType,searchText);
        }
    });
</script>