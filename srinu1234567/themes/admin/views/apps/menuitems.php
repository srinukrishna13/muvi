<?php        
if ($language_id == $menu_language && $menu_parent_language == 0) {
    $readonly = '';
    $disable = '';
} else {
    $readonly = 'readonly';
    $disable = 'disabled';
}
if ($item_type != 2) {
    $readonly = 'readonly';
}
?>
<li id="list_<?php echo $menu_item_id; ?>" data-id=" <?php echo $menu_item_id; ?>" data-name="<?php echo $menu_title; ?>"><div class="panel panel-default" id="sort_<?php echo $menu_item_id; ?>">
    <div class="panel-heading" role="tab" id="heading<?php echo $menu_item_id; ?>">
        <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-accordion" href="#collapse<?php echo $menu_item_id; ?>" aria-expanded="false" aria-controls="collapse<?php echo $menu_item_id; ?>"><?php echo $menu_title; ?></a>
            <a class="collapsed pull-right" role="button" data-toggle="collapse" data-parent="#res-accordion" href="#collapse<?php echo $menu_item_id; ?>" aria-expanded="false" aria-controls="collapse<?php echo $menu_item_id; ?>">Edit</a>
        </h4>
    </div>
    <div id="collapse<?php echo $menu_item_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $menu_item_id; ?>">
        <div class="panel-body">
            <form name="frmMN" class="frmMN" method="post">
                <input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>" />
                <input type="hidden" name="menu_item_id" value="<?php echo $menu_item_id; ?>" />
                <input type="hidden" name="option" value="save_item" />
                <input type="hidden" name="item_type" value="<?php echo $item_type; ?>" />
                <input type="hidden" name="language_id" value="<?php echo $menu_language; ?>" />
                <div class="form-group">
                    <label class="control-label">Title</label>
                    <div class="fg-line"><input type="text" name="menu_title" value="<?php echo $menu_title; ?>" class="form-control input-sm" /></div>
                </div>
                <div class="form-group">
                    <label class="control-label">Permalink</label>
                    <div class="fg-line"><input type="text" name="menu_permalink"  value="<?php echo $menu_permalink; ?>" class="form-control input-sm menu_permalink" <?php echo $readonly; ?>/></div>
                </div><div class="form-group">
                    <input type="submit" class="btn btn-primary btn-sm menu-item-save" data-id="<?php echo $menu_item_id; ?>" value="Save" />&nbsp;
                    <input type="button" class="btn btn-danger btn-sm menu-item-remove" data-id=" <?php echo $menu_item_id; ?> "  value="Remove" <?php echo $disable; ?> /></div>
            </form>
        </div>
    </div>
    </div>
        
