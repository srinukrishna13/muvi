<?php
$studio = $this->studio;
$site_url = Yii::app()->getBaseUrl(true);
if($studio->fb_link != '')
    $fb_link = '<a href="'.$studio->fb_link.'" ><img src="'.$site_url.'/images/fb.png" alt="Find Us on Facebook"></a>';
else
    $fb_link = '';
if($studio->tw_link != '')
    $twitter_link = '<a href="'.$studio->tw_link.'" ><img src="'.$site_url.'/images/twitter.png" alt="Find Us on Twitter"></a>';
else
    $twitter_link = '';                 
if($studio->gp_link != '')
    $gplus_link = '<a href="'.$studio->gp_link.'" ><img src="'.$site_url.'/images/google_plus.png" alt="Find Us on Google Plus"></a>';
else
    $gplus_link = ''; 

$logo_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $studio->theme . '/' . $studio->logo_file;
if (file_exists($logo_path))
    $logo_path = Yii::app()->getbaseUrl(true) . '/images/logos/' . $studio->theme . '/' . $studio->logo_file;
else
    $logo_path = Yii::app()->getbaseUrl(true) . '/themes/' . $studio->theme . '/images/'.$studio->default_color.'/logo.png';

$logo = '<a href="'.$site_url.'"><img src="'.$logo_path.'" alt="" /></a>';
?>
<link href="<?php echo Yii::app()->baseUrl;?>/css/admin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div  style="padding:20px;">
    <div><?php echo $logo; ?></div>
    <div class='clear' style="height:20px;"></div><br />
    <div>
        <div style="font-size: 16px;">Hi <?php echo $display_name; ?>,</div>
        <div class='clear' style="height:10px;"></div>
        <div style="font-size: 16px;"><?php echo $mail_content; ?></div>
        <div class='clear' style="height:10px;"></div>
        <?php
            foreach($item_detail as $key => $val){
                echo "<div style='height:150px;'>";
                $this->renderPartial('movie_block', array('data'=>$val,'user_data' => $user_data));
                echo "</div><div class='clear'></div>";
            }
        ?>
    </div>
    <div style="clear:both;height:5px;"></div>
    <div style="padding-top:20px;font-size: 16px;">
        Best Regards<br /><?php echo $studio->name ;?>
        <div style="clear:both;height:10px;"></div>
        <table>
            <tr>
                <td style="float:left;font-size:18px;padding-top:5px;">Follow Us</td>
                <?php if($fb_link != ''){?><td style="float:left;padding-left:30px;"><?php echo $fb_link?></td><?php }?>
                <?php if($twitter_link != ''){?><td style="float:left;padding-left:10px;"><?php echo $twitter_link?></td><?php }?>
                <?php if($gplus_link != ''){?><td style="float:left;padding-left:10px;"><?php echo $gplus_link?></td><?php }?>
                <td style="float:left;padding-left:40px;"><img src="http://dfquahprf1i3f.cloudfront.net/public/images/line.jpg" style="width:1.5px;height:35px;"></td>
                <td style="float:left;padding-left:40px;padding-top:5px;"><a href="http://<?php echo $studio->domain;?>/contactus" style="font-size: 18px;text-decoration: none;color:#000;">Contact Us</a></td>
            </tr>
        </table>
    </div>
</div>

