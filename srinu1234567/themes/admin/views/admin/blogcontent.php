<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<div class="row m-t-40 m-b-40">
    <div class="col-md-12">
       
        <form role="form" id="frmPST" class="form-horizontal" name="frmPST" enctype="multipart/form-data" method="post" action="<?php echo Yii::app()->getBaseUrl(true)?>/admin/addpost">
            <input type="hidden" name="post_id" id="post_id" value="<?php echo (isset($post->id))?$post->id: ''?>" />
            <?php if (@IS_LANGUAGE == 1) { ?>
                <input type="hidden" name="parent_id" value="<?php if (isset($post->parent_id)) {echo @$post->parent_id;} else { echo @$parent_id;}?>" id="parent_id" />
            <?php } ?>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Blog Title:</label>
                <div class="col-sm-10">
                    <div class="fg-line">
                        <input type="text" placeholder="Enter Blog Title" class="form-control input-sm" name="post_title" id="post_title" value="<?php echo (isset($post->post_title))?  Yii::app()->common->htmlchars_encode_to_html($post->post_title):'';?>" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Author:</label>
                <div class="col-sm-10">
                    <div class="fg-line">
                        <input type="text" placeholder="Enter Author Name" class="form-control input-sm" name="author" id="author" value="<?php echo ($post->author !="")?$post->author :$this->studio->name;?>" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Permalink:</label>
                <div class="col-sm-10">
                    <div class="fg-line">
                        <input type="text" readonly="readonly" placeholder="Permalink" class="form-control input-sm" name="permalink" id="permalink" value="<?php echo (isset($post->permalink))?$post->permalink:'';?>" />
                    </div>
                    <div id="perma_error" class="error"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Short Description:</label>
                <div class="col-sm-10">
                    <div class="fg-line">
                        <textarea placeholder="Max 300 Words...." class="form-control input-sm" rows="3" name="post_short_content" id="post_short_content"><?php echo (isset($post->post_short_content))?Yii::app()->common->htmlchars_encode_to_html($post->post_short_content):'';?></textarea>
                    </div>
                </div>
            </div>   
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div id="avatar_preview_div">
                            <?php if(@$post->featured_image !=""){
                                $studio_id = Yii::app()->user->studio_id;
                                $featimgUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                                $full_url   = $featimgUrl. "/system/featured_image/" . $studio_id . "/original/" .$post->featured_image;
                            ?>
                                <img src="<?php echo @$full_url; ?>" alt="<?php echo @$post->post_title;?>" title="<?php echo @$post->post_title;?>">
                            <?php } ?>                                   
                    </div>
                    <canvas id="previewcanvas" style="overflow:hidden;display: none;"></canvas>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Featured Image:</label>
                <div class="col-sm-10">
                    <input type="button" class="btn btn-default-with-bg btn-sm" value="Browse" name="featured_image" id="featured_image" data-width="<?php echo $blog_dimension[0]; ?>" data-height="<?php echo $blog_dimension[1]; ?>" onclick="openImageModal(this);" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Blog Content:</label>
                <div class="col-sm-10">
                    <div class="fg-line">
                        <textarea placeholder="Enter Blog Content" id="post_content" name="post_content" rows="10" class="form-control input-sm"><?php echo (isset($post->post_content))?Yii::app()->common->htmlchars_encode_to_html($post->post_content):'';?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Action:</label>
                <div class="col-sm-10">
                    <label class="radio radio-inline m-r-20">
                        <input type="radio" <?php echo (isset($post->has_published) && $post->has_published == 1)?'':'checked="checked"';?> value="save" id="save" name="status">
                        <i class="input-helper"></i> Save
                    </label>
                    <label class="radio radio-inline m-r-20">
                        <input type="radio" value="save_publish" id="save_publish" name="status" <?php echo (isset($post->has_published) && $post->has_published == 1)?'checked="checked"':'';?>>
                        <i class="input-helper"></i> Save and Publish
                    </label>
                </div>    
            </div>
            <div class="form-group m-t-30">
                <div class="col-sm-offset-2 col-md-10">
                   <button class="btn btn-primary" type="submit">Submit</button> 
                   <button class="btn btn-default" type="button" onclick="javascript: history.back();">Back</button> 
                </div>
            </div>
            <div class="modal is-Large-Modal fade" id="featured_img" tabindex="-1" role="dialog" aria-labelledby="featured_img">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="section_id1" id="section_id1" value="" />
                            <div role="tabpanel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active" onclick="hide_file()">
                                        <a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
                                    </li>
                                    <li role="presentation" onclick="hide_gallery()"> 
                                        <a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="upload_by_browse">
                                        <div class="row is-Scrollable">
                                            <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                                <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('browse_feat_img')">
                                                <input id="browse_feat_img" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
                                                <p class="help-block"></p>
                                            </div>
                                            <input type="hidden" class="x1" id="x1" name="fileimage[x1]" />
                                            <input type="hidden" class="y1" id="y1" name="fileimage[y1]" />
                                            <input type="hidden" class="x2" id="x2" name="fileimage[x2]" />
                                            <input type="hidden" class="y2" id="y2" name="fileimage[y2]" />
                                            <input type="hidden" class="w" id="w" name="fileimage[w]"/>
                                            <input type="hidden" class="h" id="h" name="fileimage[h]"/>
                                            <div class="col-xs-12">
                                                <div class="Preview-Block row">
                                                    <div class="col-md-12 text-center" id="upload_preview">
                                                        <img id="preview" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="upload_from_gallery">
                                        <input type="hidden" name="g_image_file_name" id="g_image_file_name" />
                                        <input type="hidden" name="g_original_image" id="g_original_image" />
                                        <input type="hidden" class="x1" id="x13" name="jcrop_allimage[x13]" />
                                        <input type="hidden" class="y1" id="y13" name="jcrop_allimage[y13]" />
                                        <input type="hidden" class="x2" id="x23" name="jcrop_allimage[x23]" />
                                        <input type="hidden" class="y2" id="y23" name="jcrop_allimage[y23]" />
                                        <input type="hidden" class="w" id="w3" name="jcrop_allimage[w3]" />
                                        <input type="hidden" class="h" id="h3" name="jcrop_allimage[h3]" />
                                        <div class="row  Gallery-Row">
                                            <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">
                
            </div>
                                            <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                                <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                                    <div class="preloader pls-blue  ">
                                                        <svg class="pl-circular" viewBox="25 25 50 50">
                                                        <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                        </svg>
                                                    </div>
                                                </div>
                                                <div class="Preview-Block row">
                                                    <div class="col-md-12 text-center" id="gallery_preview">
                                                        <img id="glry_preview" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick="seepreview(this);">Next</button>
                        </div>
                    </div>
                </div>
            </div>    
            <input type="hidden" id="img_width" name="img_width" value="">
            <input type="hidden" id="img_height" name="img_height" value="">   
        </form>                
                      
        
    </div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true)?>/themes/admin/js/tinymce/tinymce.min.js"></script>

<!-- Add New CMS Page -->
<script type="text/javascript">
$(document).ready(function(){
    tinymce.init({
    selector: "#post_content",
        formats: {
        bold: {inline: 'b'},  
    },
    valid_elements : '*[*]',
    force_br_newlines : true,
    force_p_newlines : false,
    forced_root_block : false,
    menubar: false,
    height: 300,
    plugins: [
    'advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker',
    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
    'save table contextmenu directionality emoticons template paste textcolor'
    ],
    setup: function (editor) {
    editor.addButton('newmedia', {
     text: 'Add Image',
     title: 'Add image',
     icon: 'image',
    onclick: function() {
        $("#MediaModal").modal("show");
        $('#glry_preview').removeAttr('src width height');
        $('#imgtolib').removeAttr('src width height');
        $('#choose_img_name').val("");
        $("#InsertPhoto").text("Insert Image");
        $("#InsertPhoto").removeAttr("disabled");
        $("#cancelPhoto").removeAttr("disabled");
        $('.overlay').removeAttr('style');
        $("#browsefiledetails").text("No file selected");
        $("#tinygallery").load("<?php echo Yii::app()->getBaseUrl(true)?>/template/tinyGallery");
    } });
     },
    content_css: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
    toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link newmedia | media code",
    });
});
 </script>
 
<script type="text/javascript">
$(document).ready(function(){
    
    $("#frmPST").validate({ 
        rules: {    
            post_title: {
                required: true,
                minlength: 2
            }, 
            permalink: {
                required: true,
                minlength: 2
            },             
            post_short_content: {
                required: true,
                minlength: 4,
                maxlength:300
            }, 
            post_content: {
                required: true,
                minlength: 4
            },              
        }, errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        }
        
        /*submitHandler: function(form) {
            $("#frmPST").submit();
        }   */             
    });
    
    $('#post_title').change(function(){
        var post_title = $(this).val();
        var post_id = $('#post_id').val();
        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true) ?>/site/createpostpermalink",
            data: {'post_title': post_title, 'post_id' : post_id},
            dataType: 'json',
            success: function (result) {
                console.log(result.newurl);
                $('#permalink').val(result.newurl);
                //window.location = result.newurl;
            }
        });          
    });
    $('#permalink').change(function(){
        var permalink = $(this).val();
        var post_id = $('#post_id').val();
        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true) ?>/site/checkpostpermalink",
            data: {'permalink': permalink, 'post_id' : post_id},
            dataType: 'json',
            success: function (result) {
                console.log(result.exists);
                
                if(result.exists == 1)
                {
                    $('#perma_error').html('Permalink already exists');
                    return false;
                }
                else
                {
                    $('#perma_error').html('').hide();
                    return false;                    
                }
                    
                //window.location = result.newurl;
            }
        });          
    });    
});
function openImageModal(obj) {
    var width = $(obj).attr('data-width');
    var height = $(obj).attr('data-height');
    $(".help-block").html("Upload a transparent image of size " + width + " x " + height+'px');
    $("#img_width").val(width);
    $("#img_height").val(height);
    $("#all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
    $("#featured_img").modal('show');
}
function click_browse(modal_file) {
    $("#" + modal_file).click();
}
function fileSelectHandler() {
    document.getElementById("g_original_image").value = "";
    document.getElementById("g_image_file_name").value = "";
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    $(".jcrop-keymgr").css("display", "none");
    $("#celeb_preview").removeClass("hide");
    $('#uplad_buton').removeAttr('disabled');
    var oFile = $('#browse_feat_img')[0].files[0];
    var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
    if (!rFilter.test(oFile.type)) {
        swal('Please select a valid image file (jpg and png are allowed)');
        $("#celeb_preview").addClass("hide");
        document.getElementById("browse_feat_img").value = "";
        $('#uplad_buton').attr('disabled', 'disabled');
        return;
    }
    var aspectratio = img_width / img_height;
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            swal('You have selected small file, please select one bigger image file');
            $("#celeb_preview").addClass("hide");
            document.getElementById("browse_feat_img").value = "";
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('preview');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('.error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                    $('#glry_preview').width("450");
                    $('#glry_preview').height("250");
                }
                $('#preview').css("display", "block");
                $('#celeb_preview').css("display", "block");
                $('#preview').Jcrop({
                    minSize: [img_width, img_height], // min crop size
                    maxSize: [img_width, img_height], // min crop size
                    allowResize: false, // min crop size
                    aspectRatio: aspectratio, // keep aspect ratio 1:1
                    boxWidth: 450,
                    boxHeight: 250,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, img_width, img_height]);
                });
            };
        };
        oReader.readAsDataURL(oFile);
    };
}
function showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('button').attr('disabled', 'disabled');
    } else {
        $('.loaderDiv').hide();
        $('button').removeAttr('disabled');
    }
}
function toggle_preview(id, img_src, name_of_image)
{
    $('#glry_preview').css("display", "block");
    document.getElementById("browse_feat_img").value = "";
    showLoader();
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    var aspectratio = img_width / img_height;
    var image_file_name = name_of_image;
    var image_src = img_src;
    clearInfo();
    $("#g_image_file_name").val(image_file_name);
    $("#g_original_image").val(image_src);
    var res = image_file_name.split(".");
    var image_type = res[1];
    var img = new Image();
    img.src = img_src;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            showLoader(1);
            swal('You have selected small file, please select one bigger image file more than ' + img_width + ' X ' + img_height);
            $("#celeb_preview").addClass("hide");
            $("glry_preview").addClass("hide");
            $("#g_image_file_name").val("");
            $("#g_original_image").val("");
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('glry_preview');
        showLoader(1)
        oImage.src = img_src;
        oImage.onload = function () {
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#glry_preview').width(oImage.naturalWidth);
                $('#glry_preview').height(oImage.naturalHeight);
                $('#preview').width("450");
                $('#preview').height("250");
            }
            $("#glry_preview").css("display", "block");
            $('#gallery_preview').css("display", "block");
            $('#glry_preview').Jcrop({
                minSize: [img_width, img_height], // min crop size
                maxSize: [img_width, img_height], // min crop size
                allowResize: false,
                aspectRatio: aspectratio, // keep aspect ratio 1:1
                boxWidth: 450,
                boxHeight: 250,
                bgFade: true, // use fade effect
                bgOpacity: .3, // fade opacity
                onChange: updateInfoallImage,
                onSelect: updateInfoallImage,
                onRelease: clearInfoallImage
            }, function () {
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                jcrop_api = this;
                jcrop_api.setSelect([10, 10, img_width, img_height]);
            });
        };
    };
}
function hide_file()
{
    $('#glry_preview').css("display", "none");
    $('#celeb_preview').css("display", "none");
    $('#preview').css("display", "none");
    document.getElementById('browse_feat_img').value = null;
}
function hide_gallery()
{
    $('#preview').css("display", "none");
    $("#glry_preview").css("display", "block");
    $('#gallery_preview').css("display", "none");
    $("#g_image_file_name").val("");
    $("#g_original_image").val("");
}
function seepreview(obj) {
    if ($('#g_image_file_name').val() == '')
        curSel = 'upload_by_browse';
    else
        curSel = 'upload_from_gallery';
    if ($('#'+curSel).find(".x13").val() != "") {
        $(obj).html("Please Wait");
        $('#featured_img').modal({backdrop: 'static', keyboard: false});
        posterpreview(obj, curSel);
    } else {
        if ($("#celeb_preview").hasClass("hide")) {
            $('#featured_img').modal('hide');
            $(obj).html("Next");
        } else {
            $(obj).html("Please Wait");
            $('#featured_img').modal({backdrop: 'static', keyboard: false});
            if ($('#'+curSel).find(".x13").val() != "") {
                posterpreview(obj, curSel);
            } else if ($('#'+curSel).find('.x1').val() != "") {
                posterpreview(obj, curSel);
            } else {
                $('#featured_img').modal('hide');
                $(obj).html("Next");
            }
        }
    }
}
function posterpreview(obj, curSel) {
    $("#previewcanvas").show();
    var canvaswidth = $("#img_width").val();
    var canvasheight = $("#img_height").val();
    var x1 = $('#'+curSel).find('.x1').eq(0).val();
    var y1 = $('#'+curSel).find('.y1').eq(0).val();
    var width  = $('#'+curSel).find('.w').val();
    var height = $('#'+curSel).find('.h').val();
    var canvas = $("#previewcanvas")[0];
    var context = canvas.getContext('2d');
    var img = new Image();
    img.onload = function() {
        canvas.height = canvasheight;
        canvas.width = canvaswidth;
        context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
        //$('#imgCropped').val(canvas.toDataURL());
    };
    $("#avatar_preview_div").hide();
    if ($('#g_image_file_name').val() == '') {
        img.src = $('#preview').attr("src");
    } else {
        img.src = $('#glry_preview').attr("src");
    }
    $('#featured_img').modal('hide');
    $(obj).html("Next");
 }
</script>  
