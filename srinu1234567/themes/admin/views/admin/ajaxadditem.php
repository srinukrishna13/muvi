<div class="modal-dialog">
    <form action="javascript:void(0);" method="post" class="form-horizontal" name="additem_form" id="additem_form">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 style="font-size:22px;" class="modal-title">
                    Add Items
                </h4>
            </div>
            <div class="modal-body">                
                    <div class="form-group ">
                        
                            <label for="movieName" class="col-md-3 control-label">Item Name<span class="red"><b>*</b></span>:</label>
                            <div class="col-sm-9">
                            <select id="movie_ids" name="product_id[]" class="form-control tokenize-sample" multiple="multiple" display="none">
                                <?php 
                                $movie_ids = '';
                                if(isset($movies) && !empty($movies)) {
                                    foreach ($movies as $key => $value) {
                                ?>
                                        <option value="<?php echo $value['id'];?>" selected="selected" ><?php echo $value['name'];?></option>
                                    <?php } ?>
                                <?php }
                                $movie_ids = ltrim($value['movie_id'], ',')?>
                            </select>   
                            <input type="hidden" class="form-control" name="movie_id" id="hidden_movie_ids" value="<?php echo $movie_ids;?>">
                            </div>
                    </div>
                <div class="modal-footer">
                    <input type="hidden" name="content_id" value="<?php echo $_REQUEST['content_id']; ?>">                
                    <button type="submit" class="btn btn-primary" id="bill-btn" onclick="savecontent()">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>	
</div>

<script type="text/javascript">
    $(document).ready(function () {
       
        var url = "<?php echo Yii::app()->baseUrl; ?>/admin/product_autocomplete";
        $('#movie_ids').tokenize({
            datas: url,
            placeholder: 'Search items',
            nbDropdownElements: 10,
            onAddToken:function(value, text, e){
                if (parseInt(value)) {
                    var video_ids = $("#hidden_movie_ids").val();
                    video_ids = video_ids.split(",");
                    video_ids.push(value);
                    video_ids.toString();
                    $("#hidden_movie_ids").val(video_ids);
                }
            },
            onRemoveToken:function(value, e){
                var video_ids = $("#hidden_movie_ids").val();
                video_ids = video_ids.split(",");
                
                var index = video_ids.indexOf(value);
                if(index !== -1) {
                    video_ids.splice(index,1);
                }
                video_ids.toString();
                $("#hidden_movie_ids").val(video_ids);
            }
        });
    });
  function savecontent() {
    $.ajax({
        url: "<?php echo Yii::app()->getBaseUrl(true) ?>/admin/addItems",
        data: $('#additem_form').serialize(),
        type: 'POST',        
        success: function (data) {
            if (data) {                
                $('html').animate({scrollTop: 0}, 2000);
                $("#items_popup").modal('hide');
                $('.alert').remove();
                var meg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i> &nbsp;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Updated successfully.</div>';
                $('.pace').before(meg).show("slow");
                location.reload();
            }
        }
    });
  }  
</script>
