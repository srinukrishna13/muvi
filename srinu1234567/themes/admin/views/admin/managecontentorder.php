<?php
/* 
 Page to show the managed Content ordering in a specific category
 * author<suraja@muvi.com>
 */
// print_r($sub_category);
//  exit;
?>

<style>
    .poster_listng{
     width:13.6%;   
     min-height:auto !important;
     margin-bottom:10px;
    }
    #sortable li{
        background: transparent;
        border: 1px solid #f6eeee;
        height: 136px;
    }
    #sortable li .relative{
        height:132px;
    }
    #sortable li .relative img{
        display: inline-block;
        position: relative;
        top: 30%;
        -webkit-transform: translateY(-30%);
        -ms-transform: translateY(-30%);
        transform: translateY(-30%);
    }
    #sortable li:hover {
       cursor: move;
    }

    #sortable li.ui-sortable-helper {
        cursor: move;
    }
    </style>
 <div class="row m-t-40">
     
     <!--- drop down for the content  -->
     <div class="form-horizontal">
         <div class="col-sm-4 text-left">
             <div class="form-group">

                 <label class="col-sm-3 control-label">Category:</label>

                 <div class="col-sm-9">
                     <div class="fg-line">  
                         <div class="select">
                         <select class="form-control input-sm" id="category" onchange="show_subcatgory()">
                             <?php foreach ($category as $key => $val) { ?>
                                 <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                  <?php } ?>
                             </select>
                             
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="col-sm-4 text-left">
            <div class="">
                
                 <div id="subcat_div" class="form-group col-sm-9 text-left" style="display:none;">
                     <div class="fg-line">
                         <div class="select" >
                             <select id="subcategory" name="subcategory" class="form-control input-sm">
                                                               
                             </select>
                             <input type="hidden" id="sub_category_id" name="sub_category_id" />
                         </div>
                     </div>
                 </div>
                 
             </div>
      </div>
     </div> 
    <div id="render_content_data">
        <?php $this->renderPartial('rendercontentorder',array( 'data' => $data, 'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount, 'episodeViewcount' => @$episodeViewcount,'cos'=>$cos));?>
                                                </div>
           </div>
<div class="row m-t-20" >
          <div class="col-sm-12 text-left">
                <button id="saveordwer" class="btn btn-primary" onclick="save_order()"  >Save Ordering</button> 

            </div>
     </div>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12/themes/resources/demos/style.css">
  <style>
 
  #sortable li { margin: 3px 3px 3px 0; padding: 1px; float: left; width: 100px;  font-size: 4em; text-align: center; }
  </style>  
<!--  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
  <script>
  function save_order(){
     var dataList= $(".poster_listng").map(function() {
    return $(this).data("streamid");
    }).get();
    var category=$("#category :selected").val();
    var method = $('input[name="method"]:checked').val();
    var orderby_flag = $('select[name="orderby_flag"]').val();
    //var sub_category=$("#subcategory :selected").val();
   // console.log(dataList.join('|'));
    $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true); ?>/admin/saveDroppedPosition",
            type: 'POST',
            data: {'stream_id':dataList,'category':category,'method':method,'orderby_flag':orderby_flag},
            async: true,
            success: function () {
            var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>&nbsp;Content ordered successfully.</div>'
            $('.pace').prepend(sucmsg);
                setTimeout(function () {
                    $('.alert-dismissable').slideUp('slow');
                    return false;
                }, 2000);
             }
    });
    }
    
    function show_subcatgory()
    {
    var sub_category=$("#category :selected").val();        
     $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true); ?>/admin/getSubcategory",
            type: 'POST',
            data: {'category':sub_category},
            async: true,
            beforeSend:function(){
                $('#loaderDiv').show();
            },
            success: function (res) {
            
            if(res.trim()!='empty'){    
            $('#subcategory').find('option').remove();   
            $('#subcategory').append(res);
            $("#subcat_div").show();           
            }
            else
            {
            $("#subcat_div").css("display","none"); 
            $('#subcategory').find('option').remove();  
                               
            }
             showlisting(sub_category); 
            }
    });   
               
    }
    function showlisting(sub_category)
    {   
       
      $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true); ?>/admin/getAllList",
            type: 'POST',
            data: {'category_id':sub_category},
            async: false,
            success: function (res) {
                $('#loaderDiv').hide();
                $("#render_content_data" ).empty();
                res = res ? res : "No data found";   
                $("#render_content_data" ).append(res);
            }
    });  
    }
   
  </script>