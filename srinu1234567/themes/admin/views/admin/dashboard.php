<div class="row">
    <div class="col-md-12">
        <div style="position: relative;">
            <div class="title">Good <span id="timenow"></span> <?php echo Yii::app()->user->first_name?>!</div>
            <div class="title2">Welcome to Muvi</div>
            <?php if($this->studio->is_default == 0 && $this->studio->status == 1 && $this->studio->is_subscribed == 0 && $this->studio->is_deleted == 0) { ?>
                <div class="title3">
                    Your free trial expires on <?php echo Yii::app()->common->normalDate($this->studio->start_date);?>. <a href="javascript:void(0);" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/purchase');" style="color: #FFFFFF;text-decoration: underline">Purchase Subscription</a> to continue with the service.
                </div>
            <?php }?>
        </div>
    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/dashboard-bnr.jpg">
    </div>
</div>


<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <?php 
        if($this->studio->is_default == 0 && $this->studio->status == 1 && $this->studio->is_subscribed == 0 && $this->studio->is_deleted == 0) {
        ?>
        <div class="top-blue-box">
            <h3>Your free trial expires on <?php echo Yii::app()->common->normalDate($this->studio->start_date);?></h3>
            <p class="text17">Email <a href="mailto:studio@muvi.com"><strong><u>studio@muvi.com</u></strong></a> for any questions or issue, someone will respond right away!</p>
        </div>
        <?php }?>
        <div class="clearfix"></div>
        <div class="white-box">
            <div class="col-md-9">
                <div class="white-box-title">Upload a Video</div>
                <p>Add a content and see your preview website</p><br>
                <?php  if(Yii::app()->common->hasPermission('content', 'view') ){  ?>
                <a class="btn btn-primary" href="<?php echo Yii::app()->getBaseUrl(true)?>/admin/managecontent">Add Content</a>
                <?php } ?>
            </div>

            <div class="col-md-3"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/upload-video.png" class=" pull-right"> </div>
        </div>

        <div class="clearfix"></div>
        <div class="white-box">
            <div class="col-md-9">
                <div class="white-box-title">Select a Template </div>
                <p>Select a template, add your logo and banner</p><br>
                <?php  if(Yii::app()->common->hasPermission('website', 'view') ){  ?>
                <a class="btn btn-primary" href="<?php echo Yii::app()->getBaseUrl(true)?>/template">Select Template</a>
                <?php }?>
            </div>

            <div class="col-md-3"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/template.png" class=" pull-right"> </div>
        </div>

        <div class="clearfix"></div>
        <div class="white-box">
            <div class="col-md-9">
                <div class="white-box-title">Dig Deeper </div>
                <p>Browse around to discover other setting and configuration, live chat or email <a href="mailto:studio@muvi.com">studio@muvi.com</a> if you need help.
                    Our customer support representative available 24/7.</p>
            </div>

            <div class="col-md-3"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/dipper.png" class=" pull-right"> </div>
        </div>

        <div class="clearfix"></div>
        <div class="white-box">
            <div class="col-md-9">
                <div class="white-box-title">Setup Payment gateway</div>
                <p>Setup Payment gateway and provide us the information. We will integrate with your website so that you can accept credit card payments.</p>
            </div>

            <div class="col-md-3"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/payment.png" class=" pull-right"> </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>


<script type="text/javascript">
    $(document).ready(function () {

        var visitortime = new Date();
        var curtime = visitortime.getHours() + '.' + visitortime.getMinutes();

        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true) ?>/admin/getdaytime",
            type: 'POST',
            data: {'timenow': curtime},
            dataType: 'json',
            success: function (data) {
                console.log(data.msg);
                $('#timenow').html(data.msg);
            }
        });
    });
</script>
<?php if ($this->subscription_popup==0) {?>
<div  class="loaderDiv">
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.gif" />
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style type="text/css">
    .loaderDiv{position: absolute;left: 50%;top:25%;display: none;}
    .box.box-primary{border:none !important;}
    button.close{
        border-radius: 15px 15px 15px 15px;
        -moz-border-radius: 15px 15px 15px 15px;
        -webkit-border-radius: 15px 15px 15px 15px;
        width: 25px;
        height: 25px;
        margin: 5px;
        border: 2px solid #000;
    }
</style>
<script>
    function openinmodal(url) {
        $('.loaderDiv').show();   
        $.get(url, {"modalflag": 1}, function (data) {
            $('.loaderDiv').hide();
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
</script>
<?php }?>