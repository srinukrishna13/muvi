<div class="form-box" id="login-box">
	<div id="login_form">
		<div class="header">Sign In</div>
	<!--    <form action="" method="post">-->
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>
			<div class="body bg-gray">
				<div class="form-group">
					<!--<input type="text" name="userid" class="form-control" placeholder="User ID"/>-->
					<?php //echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
			<?php echo $form->error($model,'email'); ?>
				</div>
				<div class="form-group">
					<!--<input type="password" name="password" class="form-control" placeholder="Password"/>-->
					<?php //echo $form->labelEx($model,'password'); ?>
					<?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Password')); ?>
					<?php echo $form->error($model,'password'); ?>
				</div>          
				<div class="form-group">
					<?php echo $form->checkBox($model,'rememberMe'); ?>
					<?php echo $form->label($model,'rememberMe'); ?>
					<?php echo $form->error($model,'rememberMe'); ?>
	<!--                <input type="checkbox" name="remember_me"/> Remember me-->
				</div>
        </div>
        <div class="footer">                                                               
			<?php echo CHtml::submitButton('Sign me in',array('class'=>'btn bg-olive btn-block')); ?>
<?php $this->endWidget(); ?>
            <!--<p><a href="javascript:void(0);" onclick="forgotpassword();">I forgot my password</a></p>-->
            <!--<a href="register.html" class="text-center">Register a new membership</a>-->
        </div>
	</div>	
<!-- Forgot Password Starts -->
	<div id="forgotPass" style="display: none;">
		<div class="header">Forgot Password</div>
		<!--<form action="<?php echo $this->createUrl('admin/forgotPassword');?>" method="post" >-->
			<div class="body bg-gray">
				<div class="form-group">
					<?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'Enter Your email..','id'=>'fpemail')); ?>
					<?php echo $form->error($model,'email'); ?>
				</div>       
			</div>
			<div class="footer">                                                               
				<?php echo CHtml::Button('Submit',array('class'=>'btn bg-olive btn-block','onclick'=>"sendpasswordLink()")); ?>   
				<p><a href="javascript:void(0);" onclick="forgotpassword();">Sign in</a></p>
			</div>
		<!--</form>-->
	</div>
</div>

<script type="text/javascript">
	function forgotpassword(){
		if($('#forgotPass').is(":visible")){
			$('#forgotPass').hide('slow');
			$('#login_form').show('slow');
		}else{
			$('#forgotPass').show('slow');
			$('#login_form').hide('slow');
		}
	}
	function sendpasswordLink(){
		var email = $('#fpemail').val();
		var url = '<?php echo $this->createUrl('admin/forgotPassword');?>';
		$.post(url,{'email':email},function(res){
			console.log(res.err);
			console.log(res.msg);
		},'json');
	}
</script>       