<style>
    #celeb_preview1 {
       max-width: 100%;
    }
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/cropper.min.css" type="text/css" >
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/crop-avatar.css" type="text/css" >
<!-- Cropping modal -->
<?php
$content_banner = Yii::app()->general->getContentBannerSize(Yii::app()->user->studio_id);
$banner_width = $content_banner['banner_width'];
$banner_height = $content_banner['banner_height'];
?>
 <div class="modal fade is-Large-Modal" id="topbanner"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			<form class="avatar-form" method="post" id='topbannerForm' action="javascript:void(0);" onsubmit="return uploadTopbanner();" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="avatar-modal-label">Top Banner</h4></div>
                <div class="modal-body">

                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active" onclick="hide_file1()">
                                <a id="home-tab1" href="#home1"  aria-controls="Upload-Video" role="tab" data-toggle="tab">Upload Banner</a>
                            </li>
                            <li role="presentation" onclick="hide_gallery1()">
                                <a id="profile-tab1"  href="#profile1" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home1">
                               

                                <div class="row is-Scrollable">
                                    <div class="col-xs-12 m-t-40 m-b-20 text-center">
									<input type="hidden" value="?" name="utf8">
                                        <input type="button" value="Upload File" class="btn btn-default-with-bg btn-file btn-sm" onclick="click_browse('celeb_pic1');">
                                         <input id="celeb_pic1" name="Filedata1"  style="display:none;"  type="file" onchange="topfileSelectHandler()" />  
                                        	
                                            <p class="help-block"><?php echo  "Upload a transparent image of size ".$banner_width."x".$banner_height ; ?></p>
                                                                                        <input type="hidden" id="top_x1" name="jcrop[x1]" />
											<input type="hidden" id="top_y1" name="jcrop[y1]" />
											<input type="hidden" id="top_x2" name="jcrop[x2]" />
											<input type="hidden" id="top_y2" name="jcrop[y2]" />
											<input type="hidden" id="top_w" name="jcrop[w]">
											<input type="hidden" id="top_h" name="jcrop[h]">
											<input type="hidden" id="content_id" name="content_id" value=""/>
											<div class="col-xs-12">

                                        <div class="Preview-Block">
                                            <div class="thumbnail m-b-0">
                                                <div class="m-b-10 fixedWidth--Preview" id="celeb_preview1">

                                                    <!--Place your Image Preview Here-->
                                                    <img id="preview1">

                                                </div>
                                            </div>



                                        </div>
										<div class="Preview-Block">
										<?php if ($celeb['poster']['poster_file_name']) { ?>
											<div class="thumbnail m-b-0">
                                                <div class="m-b-10" id="editceleb_preview1">
                                                    <?php
                                                    $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                                                    ?>
                                                    <img src="<?php echo $postUrl . '/system/posters/' . $celeb['poster']['id'] . "/medium/" . $celeb['poster']['poster_file_name'] ?>" />
                                                </div>
												</div>
                                            <?php } ?>
										</div>
                                    </div>
										
										
                                    </div>
									
                           
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile1">
							 <input type="hidden" name="g_image_file_name1" id="g_image_file_name1" />
                                            <input type="hidden" name="g_original_image1" id="g_original_image1" />

                                            <input type="hidden" id="x15" name="jcrop_topbanner[x15]" />
                                            <input type="hidden" id="y15" name="jcrop_topbanner[y15]" />
                                            <input type="hidden" id="x25" name="jcrop_topbanner[x25]" />
                                            <input type="hidden" id="y25" name="jcrop_topbanner[y25]" />
                                            <input type="hidden" id="w5" name="jcrop_topbanner[w5]" />
                                            <input type="hidden" id="h5" name="jcrop_topbanner[h5]" />

                                <div class="row  Gallery-Row">
                                    <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="video_content_div">
                                        <ul class="list-inline text-left">
										<?php
										foreach ($all_images as $key => $val) {
                                                        if ($val['image_name'] == '') {
                                                            $img_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/no-image-h.png';
                                                        } else {
                                                            //$img_path = $val['list_image_name'];
                                                            $img_path = $base_cloud_url . $val['s3_thumb_name'];
                                                            $orig_img_path = $base_cloud_url . $val['s3_original_name'];
                                                        }
                                                        ?> 
										
                                            <li>
                                                <div class="Preview-Block">
                                            <div class="thumbnail m-b-0">
                                                <div class="relative m-b-10">
												 <input type="hidden" name="original_image<?php echo $val['id']; ?>" id=original_image<?php echo $val['id']; ?>" value="<?php echo $orig_img_path; ?>">
                                                            <input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['image_name']; ?>"    />
                                                            <img class="img" src="<?php echo $img_path; ?>"  alt="<?php echo "All_Image"; ?>"  >
												
                                              
                                                    <div class="caption overlay overlay-white">
                                                        <div class="overlay-Text">
                                                            <div>
                                                                <a href="javascript:void(0);" id="thumb_<?php echo $val['id']; ?>" onclick="toggle_preview1(<?php echo $val['id']; ?>, '<?php echo $orig_img_path; ?>', '<?php echo $val['image_name']; ?>')">
                                                                    <span class="btn btn-primary icon-with-fixed-width">
																	  <em class="icon-check"></em>
																	</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										

                                        </div>
                                            </li>
                                        <?php } ?>
                                            
                                        </ul>
                                    </div>
                                    <div class="col-md-6  is-Scrollable p-t-40 p-b-40" >
                                         <div class="text-center m-b-20  loaderDiv" style="display: none;">
                                             <div class="preloader pls-blue text-center " >
                                                    <svg class="pl-circular" viewBox="25 25 50 50">
                                                        <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                    </svg>
                                                </div>
                                         </div>
					<div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="gallery_preview1">
                                                <img id="glry_preview1" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary avatar-save" id='topbanner_submit_btn' type="submit">Save</button>
                    <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
		</div>
				</form>
            </div>
        </div>
    </div>





<!-- /.modal -->
<script type="text/javascript">
    $('#topbanner').on('hidden.bs.modal', function () {
        $(this).find('form')[0].reset();
        $("#topbanner_preview_div").html('<img id="top_content_img" />');
        $('#topbannerInput').val('');
    });
    var top_jcrop_api;
    var top_bounds, top_boundx, top_top_boundy;
    function topfileSelectHandler() {
        document.getElementById("g_original_image1").value = ""; 
        document.getElementById("g_image_file_name1").value = ""; 
        var reqwidth = <?php echo $banner_width;?>;
        var reqheight = <?php echo $banner_height;?>;
        var taspectRatio = reqwidth / reqheight;
        clearTopbannerInfo();
        $('#topbanner_submit_btn').removeAttr('disabled');
        $(".jcrop-keymgr").css("display", "none");
        $("#editceleb_preview1").hide();
        $("#celeb_preview1").removeClass("hide");
        // get selected file
        var oFile = $('#celeb_pic1')[0].files[0];
        var ext = oFile.name.split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#celeb_preview1").addClass("hide");
        document.getElementById("celeb_pic1").value = "";
        swal('Please select a valid image file (jpg and png are allowed)');
        return;
    }
    if (oFile.name.match(/['|"|-|,]/)) {
        $("#celeb_preview1").addClass("hide");
        document.getElementById("celeb_pic1").value = "";
        swal('File names with symbols such as \' , - are not supported');
        return;
    }
     var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
             $("#celeb_preview1").addClass("hide");
             document.getElementById("celeb_pic1").value = "";
             swal('Please select a valid image file (jpg and png are allowed)');
            return;
        }
        var img = new Image();

        img.src = window.URL.createObjectURL(oFile);

        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            
            window.URL.revokeObjectURL(img.src);

           
            if (width < reqwidth || height < reqheight) {
                swal('You have selected small file, please select one bigger image file more than '+reqwidth+' X '+ reqheight);
                $('#topbanner_submit_btn').attr('disabled', 'disabled');
                $("#celeb_preview1").addClass("hide");
                $("#celeb_preview").addClass("hide");
                 document.getElementById("celeb_pic1").value = "";
                return;
            }

            // preview element
            var oImage = document.getElementById('preview1');

            // prepare HTML5 FileReader
            var oReader = new FileReader();
            oReader.onload = function (e) {
                $('.error').hide();
                // e.target.result contains the DataURL which we can use as a source of the image
                //alert(e.target.result);
                oImage.src = e.target.result;
                oImage.onload = function () { // onload event handler
                    // destroy Jcrop if it is existed
                    if (typeof jcrop_api != 'undefined') {
                        jcrop_api.destroy();
                        jcrop_api = null;
                        $('#preview1').width(oImage.naturalWidth);
                        $('#preview1').height(oImage.naturalHeight);
                        $('#glry_preview1').width("450");
                        $('#glry_preview1').height("250");

                    }

                    //setTimeout(function(){
                    // initialize Jcrop
                    $('#preview1').Jcrop({
                        minSize: [reqwidth,reqheight], // min crop size
                         aspectRatio : taspectRatio, // keep aspect ratio 1:1
                            boxWidth:400,
                            boxHeight: 150,
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        onChange: updateTopbannerInfo,
                        onSelect: updateTopbannerInfo,
                        onRelease: clearTopbannerInfo
                    }, function () {

                        // use the Jcrop API to get the real image size
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];

                        // Store the Jcrop API in the jcrop_api variable
                        // Store the Jcrop API in the jcrop_api variable
                        jcrop_api = this;
                        //jcrop_api.animateTo([10, 10, 290, 410]);
                        jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                    });
                    //},100);

                };
            };

            // read selected file as DataURL
            oReader.readAsDataURL(oFile);
                        
                        
                       
                    }
    }

    function updateTopbannerInfo(e) {
        $('#top_x1').val(e.x);
        $('#top_y1').val(e.y);
        $('#top_x2').val(e.x2);
        $('#top_y2').val(e.y2);
        $('#top_w').val(e.w);
        $('#top_h').val(e.h);
    }
    ;

// clear info by cropping (onRelease event handler)
    function clearTopbannerInfo() {
        $('#top_x1').val('');
        $('#top_y1').val('');
        $('#top_x2').val('');
        $('#top_y2').val('');
        $('#top_w').val('');
        $('#top_h').val('');
        $('#topbanner_submit_btn').removeAttr('disabled');
    }
    ;
    
    function updateTopbanneGalInfo(e) {
        $('#x15').val(e.x);
        $('#y15').val(e.y);
        $('#x25').val(e.x2);
        $('#y25').val(e.y2);
        $('#w5').val(e.w);
        $('#h5').val(e.h);
    }
    ;

// clear info by cropping (onRelease event handler)
    function clearTopbanneGalInfo() {
        $('#x15').val('');
        $('#y15').val('');
        $('#x25').val('');
        $('#y25').val('');
        $('#w5').val('');
        $('#h5').val('');
        $('#topbanner_submit_btn').removeAttr('disabled');
    }
    ;
    function uploadTopbanner() {
        var formobj = document.getElementById('topbannerForm');
        $('#topbanner_submit_btn').text('Wait!...');
        $('#topbanner_submit_btn').attr('disabled', 'disabled');

        $.ajax({
            url: "<?php echo Yii::app()->baseUrl; ?>/admin/cropTopbanner", // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(formobj), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            dataType: 'json',
            success: function (data)  // A function to be called if request succeeds
            {
                if(data.succ==1)
                {
                clearTopbannerInfo();
                $('#topbanner_submit_btn').removeAttr('disabled');
                $("#topbanner_preview_div").html('<img id="top_content_img" />');
                $("#topbanner-preview-img").html('<img src="' + data.topbannerthumb + '"/>');
                //$('.addmore-content').show();
                $('#topbannerInput').val('');
                $('#topbanner_submit_btn').text('Save');
                $('#topbanner').modal('hide');
                $('#add_change-top-banner-text').html(" Change Banner ");
                $('#remove-top-banner-text').show();
                $('#success_msg').html('<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i>&nbsp;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Top Banner upload successfully.</div>');
            }else if(data.succ==2)
            {
                swal('Please check the file format you have Uploaded');
                $('#topbanner_submit_btn').removeAttr('disabled');
                $('#topbanner_submit_btn').text('Save');

            }
            else
            {
            swal('Error in File upload, Try Again'); 
             $('#topbanner_submit_btn').removeAttr('disabled');
             $('#topbanner_submit_btn').text('Save');
            }
            }
        });
    }


function toggle_preview1(id, img_src, name_of_image)
        {
           
        document.getElementById("celeb_pic1").value = "";
        $('#topbanner_submit_btn').removeAttr('disabled');
        var reqwidth = <?php echo $banner_width;?>;
        var reqheight = <?php echo $banner_height;?>;
        clearTopbanneGalInfo();
        var taspectRatio = reqwidth / reqheight;  
        showLoader(); 
        
       // $("#glry_preview").css("display","none");
        var image_file_name = name_of_image;
        //$("#thumb_"+id).css("border","3px solid #efefef !important");
        var image_src = img_src;
        

        $("#g_image_file_name1").val(image_file_name);
        $("#g_original_image1").val(image_src);
        var res = image_file_name.split(".");
        var image_type = res[1];
        //alert(image_type);
        var img = new Image();
        img.src = img_src;

        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            if (width < reqwidth || height < reqheight) {
                showLoader(1); 
                /*$('.error').html('You have selected small file, please select one bigger image file more than 400 X 400').show();*/
                swal('You have selected small file, please select one bigger image file more than '+reqwidth+' X '+reqheight);
                $('#topbanner_submit_btn').attr('disabled','disabled');
                $("#glry_preview1").addClass("hide");
                
                $("#g_image_file_name1").val("");
                $("#g_original_image1").val("");
                
                return;
            }

            // preview element
            var oImage = document.getElementById('glry_preview1');
            showLoader(1)
            oImage.src = img_src;
           // $("#glry_preview").css("display","block");
            oImage.onload = function () {
               
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#glry_preview1').width(oImage.naturalWidth);
                    $('#glry_preview1').height(oImage.naturalHeight);
                   $('#preview1').width("500");
                   $('#preview1').height("300");
                }

                //setTimeout(function(){
                // initialize Jcrop
                $('#glry_preview1').Jcrop({
                    minSize: [reqwidth,reqheight], // min crop size
                    aspectRatio :taspectRatio, // keep aspect ratio 1:1
                    boxWidth: 400,
                    boxHeight: 150,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateTopbanneGalInfo,
                    onSelect: updateTopbanneGalInfo,
                    onRelease: clearTopbanneGalInfo
                }, function () {

                    // use the Jcrop API to get the real image size
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
            //};

            // read selected file as DataURL
            //oReader.readAsDataURL(oFile);                
        };
        }
        
       
function hide_file1() {

$('#preview1').css("display", "none");
}
function hide_gallery1() {

$('#glry_preview1').css("display", "none");
}
</script>