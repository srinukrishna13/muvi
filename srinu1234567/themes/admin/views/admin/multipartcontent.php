
<?php //echo "<pre>";print_r($movieDetails[0]);exit;?>
<div class="box-body table-responsive no-padding">
	<!--<button class="btn bg-olive btn-flat margin btn_add_movie" onclick="addMovie(<?php echo Yii::app()->user->id;?>);"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Movie</button>-->
	<a href="<?php echo $this->createUrl('admin/addEpisode',array('movie_id'=>$movieDetails[0]['movie_id'],'uniq_id'=>$movieDetails[0]['uniq_id']));?>"><button class="btn bg-olive btn-flat margin btn_add_movie" ><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Episode</button></a>
    <div class="pull-right">
		<?php 	
		if($data){
			$this->widget('CLinkPager', array(
				'currentPage'=>$pages->getCurrentPage(),
				'itemCount'=>$item_count,
				'pageSize'=>$page_size,
				'maxButtonCount'=>6,
				'nextPageLabel'=>'Next &gt;',
				'header'=>'',
			));
		}
		?>
	</div>	
	
	<table class="table table-hover">
		<tbody>
			<tr>
				<td colspan="7" style="background: #f5f5f5;">
					<div class="col-lg-12">
						<div class="col-lg-2">
							<img src="<?php echo $this->getPoster(@$movieDetails[0]['movie_id'],'films','standard');?>" style="width: 150px;height: 200px;"/> 
						</div>
						<div class="col-lg-10">
							<div class="col-lg-6"><strong>Name: </strong> <em><?php echo @$movieDetails[0]['name'];?></em></div>
							<div class="col-lg-6"><strong>Date:</strong> <em><?php echo @$movieDetails[0]['release_date']?date('m/d/Y',strtotime(@$movieDetails[0]['release_date'])):'Not Available';?></em></div>
							<div class="clear" style="clear: both;line-height: 20px;"></div>
							<div class="col-lg-12"><strong>Description:</strong> <em><?php echo @$movieDetails[0]['story']?@$movieDetails[0]['story']:'Not Available';?></em></div>
							<div class="clear" style="clear: both;line-height: 20px;"></div>
							<div class="col-lg-12 casts"><strong>Casts:</strong> 
								<?php $casts = $this->getCasts($movieDetails[0]['movie_id']);
								if($casts){
									unset($casts['actor']);
									unset($casts['director']);
									foreach($casts AS $key=>$val){
										echo "<a href='#' target='_blank'>".@$val['celeb_name']."</a>";
										if($key==4 || ((count($casts)-1)==$key)){
											break;
										}else{
											echo ", ";
										}
									}
								}else{
									echo "<em>Not Available</em>";
								}?>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</td>
			</tr>
			<tr>
				<th>S/L#</th>
				<th>Poster</th>
				<th>Episode#</th>
				<th>Title</th>
				<th>Date</th>
				<th>Views</th>
				<!--<th>Video</th>-->
				<th>Action</th>
			</tr>
			<?php
			$cnt=(($pages->getCurrentPage())*10)+1;
			if($data){
				$series_number='';
				foreach($data AS $key=>$details){
					$posterImg = $this->getPoster($details['id'],'moviestream','episode');
					$img_path = $posterImg;
					$episodeTitle = $details['episode_title']?$details['episode_title']:'Episode#-'.$details['episode_number'];
					 $movie_id = $details['movie_id'];
					 $episode_id = $details['id'];
					 $freeviewcount = isset($viewcount[$episode_id]['f'])?$viewcount[$episode_id]['f']:0;
					 $paidviewcount = isset($viewcount[$episode_id]['p'])?$viewcount[$episode_id]['p']:0;
					 //$plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink']:str_replace(' ', "-",strtolower($details['episode_title']));

				if($series_number=='' || ($series_number != $details['series_number'])){?>
				<tr><td colspan="7" style="background: #EFF3F7;">Season# <em><?php echo $details['series_number'];?></em></tr>		
				<?php }
					$series_number = $details['series_number'];
				?>
				<tr>
					<td><?php echo $cnt++;?> </td>
					<td><a href="javascript:void(0);" class="" target="_blank" rel="tooltip" data-title="Tooltip"><img src="<?php echo $img_path;?>" alt="Episode Poster"></a></td>
					<td><strong><?php echo $details['episode_number'];?></strong></td>
					<td><strong><a href="<?php echo $this->createUrl('admin/editEpisode',array('movie_id'=>$movie_id,'movie_stream_id'=>$details['id']));?>"><?php if($details['episode_title']){echo $details['episode_title'];}else{echo "Episode#-".$details['episode_number'];};?></a></strong></td>
					<td><?php if($details['episode_date']){echo date('jS M Y',strtotime($details['episode_date']));}else{echo date('jS M Y',strtotime($details['created_date']));};?></a></td>
					<td>
						<?php echo "<b>$paidviewcount</b>";?>
					</td>
					<td style="font-size: 22px;">
						<span id="<?php echo $details['id'];?>">
						<?php if(!$details['full_movie']){?>
							<a href="javascript:void(0);" onclick="openUploadpopup(<?php echo $movie_id;?>,'<?php echo addslashes($episodeTitle);?>',<?php echo $details['id'];?>);"><span class="glyphicon glyphicon-upload" title="Upload Video"></span></a> &nbsp; 
						<?php }else{?>
							<a href="<?php echo $this->createUrl('video/play_video',array('movie'=>$movie_id,'episode'=>$details['id'],'preview'=>true));?>" target="_blank"><span class="glyphicon glyphicon-play-circle"></span></a>&nbsp; 
						<?php }?>
						</span>
						<a href="<?php echo $this->createUrl('admin/editEpisode',array('movie_id'=>$movie_id,'movie_stream_id'=>$details['id']));?>" ><span class="glyphicon glyphicon-pencil" title="Edit"></span></a> &nbsp; 
						<a href="<?php echo $this->createUrl('admin/removeEpisode',array('movie_id'=>$movie_id,'movie_stream_id'=>$details['id']));?>" onclick="return confirm('Are you sure you want to remove the Episode from Series?')" class="del-cls"><span class="glyphicon glyphicon-remove" title="Remove"></span></a>
					</td>
				</tr>
			<?php	}
			}else{?>
				<tr>
					<td colspan="7"><p class="text-red">Oops! You don't have episode under this content.</p></td>
				</tr>
			<?php }  ?>
        </tbody>
    </table>
</div>
<!-- Upload Popup  End -->
<div style="position:fixed;border:1px solid grey;background: #FFF;left: 60%;top:70%;border-radius: 5px;display: none;" id="dprogress_bar">
  <div style="background-color:#DBEAF9;height:40px;width:440px;padding:10px;border-radius: 5px;" id="status_header">
	<div style="float:left;font-weight:bold;">File Upload Status</div>
	<div onclick="manage_progressbar();" style="float:right;">X</div>
  </div>
  <div style="padding:10px;" id="all_progress_bar"></div>
</div>

<?php include 'uploadVideo_popup.php';?>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/js/custom_upload.js"></script>		
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/js/s3_multi.js"></script>		

<script type="text/javascript">
	function click_browse(){ 
		$("#videofile").click();
	}

	function openUploadpopup(movie_id,name,movie_stream_id){
		$('input[type=file]').val('');
		$('#movie_name').val(name);
		$('#movie_stream_id').val(movie_stream_id);
		$('#movie_id').val(movie_id);
		$('#contentType').val('tv');
		$('#pop_movie_name').html(name);
		$("#addvideo_popup").modal('show');
	}

	var url = '<?php echo Yii::app()->baseUrl;?>';
	$(function() {
		var mov_name = "";var episode='';
		$('#dprogress_bar').draggable({
			appendTo: 'body',
			start: function(event, ui) {
				isDraggingMedia = true;
			},
			stop: function(event, ui) {
				isDraggingMedia = false;
			}
		});
	});
	
// Remove Video
	  function deleteVideo(movie_id){
		  if(confirm('Are you sure you want to remove this video from the movie? \n This video will parmanetly deleted from the movie.')){
			  $.post(HTTP_ROOT+"/admin/removeVideo",{'is_ajax':1,'movie_id':movie_id},function(res){
				  if(res.error==1){
					  alert('Error in removing movie');
				  }else {
					  $('#fmovie_name').html("<small><em>Not Available</em></small>&nbsp <a href='javascript:void(0);' onclick='openUploadpopup();'>Upload Video</a>");
					  alert('Your movie video removed successfully');
				  }
			  },'json');
		  }
	  }

	 function manage_progressbar(){
		$("#all_progress_bar").toggle('slow');
	  }
	
</script>