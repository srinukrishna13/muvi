<!-- Modal Starts Here -->
<div id="addvideo_popup" class="modal fade" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Upload Video - <span class="" id="pop_movie_name"></span> </h4>
			</div>
			<div class="modal-body">
                                <div class="form-group" style="border-top:1px solid #f4f4f4">
					<label for="uploadVideo">Upload Video</label>
					<div style="margin:0;padding:0;">
						<input type="hidden" value="?" name="utf8">
					</div>
					<input type="hidden" name="movie_stream_id" id="movie_stream_id" value=""/>
					<input type="hidden" name="movie_id" id="movie_id" value=""/>
					<input type="hidden" name="movie_name" id="movie_name" value=""/>
					<input type="hidden" name="contentType" id="contentType" value=""/>
					<input type="hidden" name="content_types_id" id="content_types_id" value=""/>
                                        <input type="button" value="Upload File" onclick="click_browse();" class="btn btn-primary btn-large">
					<input type="file" name="file" style="display:none;" id="videofile" required onchange="checkfileSize();" accept="video/*">
				</div>
				
			</div>
		</div>									
	</div>
</div>
<!-- Modal End  -->