<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.form.js"></script>
<div class="col-lg-12">
	<ul id="breadcrumbs_imp">
		<li >Add Movie</li>
		<li class="activ">Upload Video</li>
	</ul>
</div>
<div class="col-md-12">
<div id="addvideo_popup" class="box box-primary" >
<!--	<div class="box-header">
		<h3 class="box-title">Upload Video </h3>
	</div>	-->
	<form action="<?php echo $this->createUrl('admin/uploadeMovie');?>" method="post" enctype="multipart/form-data" id="addvideo_form" data-toggle="validator">
		<div class="box-body">
			<div class="form-group">
<!--				<label for="movie_name">Movie Name: <?php echo @$name;?></label>-->
				<h4 class="text-aqua"><span class="glyphicon glyphicon-info-sign"></span> Note:</h4>
				<ul>
					<li>Make sure that you are uploading correct video for the movie</li>
					<li>Video Upload will take some time so be patience till it completed</li>
					<li>During Upload movie don't click on any link of the page or Don't refresh the page</li>
					<li>If you will do so then you have start uploading the movie again from a fresh copy</li>
				</ul>
			</div>
			<div class="form-group" style="border-top:1px solid #f4f4f4">
				<label for="uploadVideo">Upload Video</label>
				<input type="file" id="video_id" name="video_name" required onchange="checkextension();">
				<p class="help-block">Uploaded video will considered as Movie video.</p>
			</div>
		</div>
		<input type="hidden" value="<?php echo @$movie_id;?>" name="movie_id" id="movie_id"  />
		<div class="box-footer addmovie-footer">
			<button type="submit" class="btn btn-primary">Upload</button>
			<a href="<?php echo $this->createUrl('admin/managecontent');?>"><button type="button" class="btn btn-default">Skip <i class="fa fa-angle-double-right"></i></button></a>
		</div>
	</form>	
</div>
</div>
<div style="bottom:10px;position:fixed;right:2%;border:1px solid grey;display: none;background: #FFF;" id="progressbar-popup">
	<div style="background-color:#DBEAF9;height:40px;width:400px;padding:10px;" id="status_header">
	  <div style="float:left;font-weight:bold;">File Upload Status</div>
	  <div onclick="manage_progressbar();" style="float:right;">X</div>
	</div>
	<div style="padding:10px;" id="all_progress_bar">
		<div class="upload" id="upload_xd5jukx2zmk">
			<h5><?php echo @$name;?></h5>
			<h6>File Upload is in progress....</h6>
			<img src="<?php echo Yii::app()->baseurl;?>/images/ajax-loader.gif"/>
			<h6 id="rem_time">&nbsp;</h6>
			<div class="progress xs progress-striped active" style="display: block;">
			  <div style="width: 0%" class="progress-bar progress-bar-success" id="pstatus"></div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12" id="upload_succ" style="display: none;">
	<!-- Success box -->
	<div class="box box-solid box-success">
		<div class="box-header">
			<h3 class="box-title"><span class="glyphicon glyphicon-cloud-upload"></span> Upload Movie Completed Successfully</h3>
<!--			<div class="box-tools pull-right">
				<button data-widget="collapse" class="btn btn-success btn-sm"><i class="fa fa-minus"></i></button>
				<button data-widget="remove" class="btn btn-success btn-sm"><i class="fa fa-times"></i></button>
			</div>-->
		</div>
		<div class="box-body">
			<p>
				Your Movie Upload Completed Successfully.<br/>
				You can visit movie details page to edit all other information of the movie.<br>
				<a href="<?php echo $this->createUrl('admin/movieDetails/query/'.urlencode($name));?>">Click here</a> to move to Details page.
			</p>
		</div>
	</div>
</div>
<script type="text/javascript">
	var timeoutint ='';
	//For 1mb it takes 12sec
	var avgtimefor1mb = 9;
	var totalTimeneeded = 0;
	//Interval duration taken 15sec
	var intervalDuration =1;
	var perintval = 0;
	var completedPer = 0 ;
	var filesize=0; 
	(function() {
		  var bar = $('#pstatus');
		  var movie_name = $('#video_id').val();
		  $('#addvideo_form').ajaxForm({
			  beforeSend: function() {
				$('#progressbar-popup').show();
				// status.empty();
				 totalTimeneeded=0;
				 perintval=0;
				 completedPer=0;
				 filesize=0;
				 var percentVal = '0%';
				 GetFileSize('video_id');
				 bar.width(percentVal)
				 //percent.html(percentVal);
				 movie_name = $('#video_id').val();
			  },
			  uploadProgress: function(event, position, total, percentComplete) {
				//check_fileupload_size($('#video_id').val());
				//$('#addvideo_popup').modal('hide');
				if(percentComplete>90){
					percentComplete = 90;
				}
			  },
			  success: function() {
				  //$('#addvideo_popup').modal('hide');
				  var percentVal = '100%';
				  bar.width(percentVal);
				  $('#fmovie_name').html(movie_name+'&nbsp;<a href="javascript:void(0);" onclick="openUploadpopup()">Change Video</a>');
				  clearTimeout(timeoutint);
			  },
			  complete: function(xhr) {
				   clearTimeout(timeoutint);
				   var percentVal = '100%';
				   bar.width(percentVal)
				   $('#progressbar-popup').hide();
				   //$('#addvideo_popup').modal('hide');
				   if(xhr.responseText=='size_error'){
						$("#video_id").val('');
						alert('Sorry, the file you are trying to upload is too big \n Please upload a file size less then 5GB');
				   }else if(xhr.responseText=='extension_error'){
						$("#video_id").val('');
						alert('Sorry, the file you are trying to upload is too big \n Please upload a file size less then 5GB');
				   }else if(xhr.responseText=='error'){
						$("#video_id").val('');
						alert('Sorry, We are having some problem in uploading the file. \n Please try again after sometime');
				   }else{
					   $("#video_id").val('');
					   //$('#fmovie_name').html(movie_name+'&nbsp;<a href="javascript:void(0);" onclick="openUploadpopup()">Change Video</a>');
					   $('#addvideo_popup').hide();
					   $('#upload_succ').show();
				   }
				   console.log(xhr.responseText);
				   //status.html(xhr.responseText);
			  }
		  });
	  })();
	  var settout ='';
	 
	 
	  // remove Video
	  function deleteVideo(movie_id){
		  if(confirm('Are you sure you want to remove this video from the movie? \n This video will parmanetly deleted from the movie.')){
			  $.post(HTTP_ROOT+"/admin/removeVideo",{'is_ajax':1,'movie_id':movie_id},function(res){
				  if(res.error==1){
					  alert('Error in removing movie');
				  }else {
					  $('#fmovie_name').html("<small><em>Not Available</em></small>&nbsp <a href='javascript:void(0);' onclick='openUploadpopup();'>Upload Video</a>");
					  alert('Your movie video removed successfully');
				  }
			  },'json');
		  }
	  }
	  
	  function checkextension(){
		// get the file name, possibly with path (depends on browser)
		var filename = $("#video_id").val();
		// Use a regular expression to trim everything before final dot
		var extension = filename.replace(/^.*\./, '');
		// Iff there is no dot anywhere in filename, we would have extension == filename,
		// so we account for this possibility now
		if (extension == filename) {
			extension = '';
		} else {
			// if there is an extension, we convert to lower case
			// (N.B. this conversion will not effect the value of the extension
			// on the file upload.)
			extension = extension.toLowerCase();
		}
		switch (extension) {
			case 'mp4':
				// Checking size of the file
				//for IE
				if ($.browser.msie) {
					//before making an object of ActiveXObject,
					//please make sure ActiveX is enabled in your IE browser
					var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var filePath = $("#" + fileid)[0].value;
					var objFile = objFSO.getFile(filePath);
					var fileSize = objFile.size; //size in kb
					fileSize = (fileSize / 1048576).toFixed(2); //size in mb
					if(parseFloat(fileSize)>5120){
						$("#video_id").val('');
						alert('Sorry, the file you are trying to upload is too big \n Please upload a file size less then 5GB');
					}
				}
				//for FF, Safari, Opeara and Others
				else {
					fileSize = $("#video_id")[0].files[0].size //size in kb
					fileSize = (fileSize / 1048576).toFixed(2); //size in mb
					if(parseFloat(fileSize)>5120){
						$("#video_id").val('');
						alert('Sorry, the file you are trying to upload is too big \n Please upload a file size less then 5GB');
					}
				}
				break;
			default:
				// Cancel the form submission
				alert('Sorry! This video format is not supported. \n Only MP4 format video are allowed to upload');
				$("#video_id").val('');
				break;
		}

	  }
	  function GetFileSize(fileid) {
		try {
		//for IE
		if ($.browser.msie) {
			//before making an object of ActiveXObject,
			//please make sure ActiveX is enabled in your IE browser
			var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var filePath = $("#" + fileid)[0].value;
			var objFile = objFSO.getFile(filePath);
			var fileSize = objFile.size; //size in kb
			fileSize = (fileSize / 1048576).toFixed(2); //size in mb
		}
		//for FF, Safari, Opeara and Others
		else {
			fileSize = $("#" + fileid)[0].files[0].size //size in kb
			fileSize = (fileSize / 1048576).toFixed(2); //size in mb
		}
		console.log("Uploaded File Size is" + fileSize + "MB");
		uploadprogressbar(fileSize);
		}
		catch (e) {
		alert("Error is :" + e);
		}
	}

	function uploadprogressbar(filesize){
		if(!totalTimeneeded){
			totalTimeneeded = (parseFloat(avgtimefor1mb)*parseFloat(filesize)).toFixed(2);
			console.log(totalTimeneeded);
			perintval = ((100/parseFloat(totalTimeneeded))*parseInt(intervalDuration)).toFixed(2);
			console.log(perintval);
		}else{
			completedPer = (parseFloat(completedPer) + parseFloat(perintval));
		}
		if(parseFloat(completedPer)>98){
			completedPer = 98;
		}
		var percentVal = completedPer+'%';
		console.log(percentVal);
		$('#pstatus').width(percentVal);
		timeoutint = setTimeout("uploadprogressbar("+filesize+")",'1000');
	}
</script>