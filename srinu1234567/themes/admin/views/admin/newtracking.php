<div class="row m-t-20">
    <div class="col-md-12">
     <form class="form-horizontal" role="form" name="trackingcode" action="<?php echo $this->createUrl('admin/edittracking');?>" method="post" data-toggle='validator'>
         <input type="hidden" value="<?php echo $data['id']; ?>" name="track_id" class="track_id">
        <div class="form-group">
            <div class="col-md-3 control-label">
                <label>Name</label>
            </div>
            <div class="col-md-9 control-label">
                <input type="text" id="track_name" name="track_name" placeholder="Track Name" value="<?php echo $data['name'];?>" class="form-control input-sm" required />
            </div>

        </div>
        <div class="form-group">
            <label for="movie_name" class="col-md-3 control-label">Location:</label>
            <div class="col-md-9">
                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm checkInput location" id="location" name="location">
                            <option value ="" selected="selected">----Choose location----</option>
                            <option value="Below" <?php if($data['location'] =='Below'){echo "selected='selected'";} ?> >Below</option>
                            <option value="Above" <?php if($data['location'] =='Above'){echo "selected='selected'";} ?> >Above</option> 
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="movie_name" class="col-md-3 control-label">Tag:</label>
            <div class="col-md-9">
                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm checkInput tag" id="location" name="tag">

                            <?php if ($action =='edit') { if ($data['location'] =='Below') {?>
                            <option value="open head" <?php if($data['tag'] == 'open head'){echo "selected='selected'";} ?>><?php echo htmlspecialchars('<head>');  ?></option>
                            <option value="open body" <?php if($data['tag'] == 'open body'){echo "selected='selected'";} ?>><?php echo htmlspecialchars('<body>');  ?></option>
                               <?php } else if($data['location'] =='Above') {?>                            
                            <option value="close head" <?php if ($data['tag'] == 'close head'){echo "selected='selected'";} ?> ><?php echo htmlspecialchars('</head>');  ?></option>
                           <option value="close body" <?php if($data['tag'] == 'close body'){echo "selected='selected'";} ?>><?php echo htmlspecialchars('</body>');  ?></option>

                                <?php } } else {?>
                                 <option value ="" selected="selected">----Choose tag----</option>

                                <?php }?>
                            

                            
                                

                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-3">
                <label>Code</label>
            </div>   
            <div class="col-md-9">
                <div class="fg-line">
                    <textarea class="form-control input-sm" id="tracking_code" name="code" placeholder="Enter Your Code"> <?php echo html_entity_decode($data['code']);?> </textarea>
                </div>
            </div> 
        </div>
        <div class="form-group">
        <div class="box-footer col-md-10 col-md-offset-3">
				<button class="btn btn-primary" value="update" type="submit">Update</button>
			</div>
        </div>
    </form>
    </div>
</div>
<script type="text/javascript">
 
$('select.location').on('change', function() {
var position =this.value;
  if (position == 'Above'){
      $('select.tag').html("<option value='close head'>"+ '<?php echo htmlspecialchars('</head>');  ?>' +"</option><option value='close body'>"+ '<?php echo htmlspecialchars('</body>');  ?>' +"</option>");
  }
  else
  {
     $('select.tag').html("<option value='open head'>"+ '<?php echo htmlspecialchars('<head>');  ?>' +"</option><option value='open body'>"+ '<?php echo htmlspecialchars('<body>');  ?>' +"</option>");
  }
});

</script>       
