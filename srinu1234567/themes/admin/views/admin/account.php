<div class="row m-t-40 m-b-40">
    <div class="col-xs-12">
        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-info icon left-icon "></em>
                </div>
                <h4>Contact Info</h4>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-8">
                    <form class="form-horizontal" method="post" role="form" id="account" name="account" action="<?php echo Yii::app()->baseUrl; ?>/admin/saveaccount">
                        <div class="loading" id="subsc-loading"></div>  
                        <div class="form-group">
                            <label class="col-md-4 control-label">Studio Name:</label>  
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" id="studio_name" name="studio_name" placeholder="Studio Name" value="<?php echo $this->studio->name; ?>" class="form-control input-sm" required />
                                </div>
                            </div>              
                        </div>       
                        <div class="form-group">
                            <label class="col-md-4 control-label">Full Name:</label>  
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" id="first_name" name="first_name" placeholder="Full Name" value="<?php echo $userdata['first_name'] ?>" class="form-control input-sm" required />
                                </div>
                            </div>              
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Address:</label>  
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <textarea class="form-control input-sm" id="address" name="address" placeholder="Address"><?php echo $userdata['address_1'] ?></textarea>
                                </div>
                            </div>              
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Phone:</label>  
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" id="phone" name="phone" placeholder="Phone" value="<?php echo $userdata['phone_no'] ?>" class="form-control input-sm" required />
                                </div>
                            </div>              
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email Address:</label>  
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" id="email" name="email" class="form-control" value="<?php echo $userdata['email'] ?>" required />
                                </div>
                            </div>              
                        </div>         

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" class="btn btn-primary btn-sm m-t-30">Update</button>
                            </div>
                        </div>                

                    </form>        
                </div> 
            </div>
        </div>

        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-info icon left-icon "></em>
                </div>
                <h4>Security</h4>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-8">

                    <form class="form-horizontal" method="post" role="form" id="security" name="security" action="<?php echo Yii::app()->baseUrl; ?>/admin/savepassword">
                        <div class="loading" id="security-loading"></div>  
                        <div class="form-group">
                            <label class="col-md-4 control-label">Old Password:</label>  
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="password" id="password" name="password" class="form-control input-sm" autocomplete="false" placeholder="Please enter your old password" />
                                </div>
                            </div>              
                        </div>     
                        <div class="form-group">
                            <label class="col-md-4 control-label">New Password:</label>  
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="password" id="new_password" name="new_password" class="form-control input-sm" autocomplete="false" placeholder="Enter new password" />
                                </div>
                            </div>              
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Confirm Password:</label>  
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="password" id="conf_password" name="conf_password" class="form-control input-sm" autocomplete="false" placeholder="Re-enter new password" />
                                </div>
                            </div>              
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" class="btn btn-primary btn-sm m-t-30">Update</button>
                            </div>
                        </div>            

                    </form>    
                </div>       
            </div>  
        </div>

        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-info icon left-icon "></em>
                </div>
                <h4>Cancel Subscription</h4>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-8">


                    <?php
                    $config = Yii::app()->common->getConfigValue(array('onhold_period'), 'value');
                    if (isset($studio->is_subscribed) && isset($studio->status) && $studio->is_subscribed == 0 && $studio->status == 4) {
                        ?>
                        <div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    Your account is now cancelled. Thank you for your trust and business so far!
                                </div>                     
                            </div>            

                            <div class="form-group">
                                <div class="col-md-12">
                                    Your data will be kept for <?php echo $config[0]['value']; ?> days and then permanently deleted. If you change your mind within <?php echo $config[0]['value']; ?> days (we hope you do :))), please come back anytime to <a onclick="openinmodal('<?php echo Yii::app()->baseUrl; ?>/payment/reactivate');" href="javascript:void(0);"><strong>re-activate</strong></a> your account.
                                </div>                     
                            </div>            

                        </div>

<?php } else { ?>
                        <form class="form-horizontal" method="post" role="form" id="cancel_subscription" name="cancel_subscription" action="javascript:void(0);" onsubmit="return validateCancelForm();">
                            <div id="cancel-subscription">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        All of your data including videos and meta data will be deleted <?php echo $config[0]['value']; ?> days after cancellation. Please make sure to export all your data before canceling subscription.
                                    </div>                     
                                </div>            
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <button type="button" class="btn btn-primary btn-sm m-t-30" onclick="cancelSubscription();">Cancel Subscription</button>
                                    </div>
                                </div>                

                            </div> 

                            <div id="confirm-subscription" style="display: none;">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        Please select the reason below that best describes your reason for cancellation.
                                    </div>                     
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="cancel_reason" value="Monthly platform fee is too high." />
                                            <i class="input-helper"></i>Monthly platform fee is too high.
                                        </label>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="cancel_reason" value="Bandwidth fee is too high." />
                                            <i class="input-helper"></i> Bandwidth fee is too high.
                                        </label>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="cancel_reason" value="Platform is very difficult to use." />
                                            <i class="input-helper"></i> Platform is very difficult to use.
                                        </label>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="cancel_reason" value="Not happy with customer support." />
                                            <i class="input-helper"></i> Not happy with customer support.
                                        </label>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="cancel_reason" value="Missing some features that I am looking for." />
                                            <i class="input-helper"></i> Missing some features that I am looking for.
                                        </label>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="cancel_reason" value="Don't have a need at this time, may come back to it later." />
                                            <i class="input-helper"></i>Don't have a need at this time, may come back to it later.
                                        </label>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="radio radio-inline m-r-20">
                                            <input type="radio" name="cancel_reason" value="I misunderstood this product, thought it to be something else." />
                                            <i class="input-helper"></i>I misunderstood this product, thought it to be something else.
                                        </label>

                                        <div><label id="cancel_reason-error" class="error red" for="cancel_reason" style="display: none;"></label></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="fg-line">
                                            <textarea placeholder="Please provide additional notes that will help us better the product" cols="90" rows="5" name="custom_notes" class="form-control input-sm" maxlength="200"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-primary btn-sm m-t-30">Confirm Cancellation</button>
                                    </div>
                                </div>
                            </div>
                        </form>
<?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod("mail", function (value, element) {
    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
}, "Please enter a correct email address");


$(document).ready(function () {
    $('#subsc-loading').hide();

    $("#account").validate({
        rules: {
            studio_name: {
                required: true,
                minlength: 4,
            },
            first_name: {
                required: true,
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                mail: true
            },
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        }        
    });

    $("#security").validate({
        rules: {
            password: {
                required: true,
            },
            new_password: {
                required: true,
                minlength: 6,
            },
            conf_password: {
                equalTo: '#new_password',
            },
        },
        messages: {
            password: {
                required: 'Please enter your current password',
            },
            new_password: {
                required: 'Please enter new password',
            },
            conf_password: {
                equalTo: 'Please enter the same password again',
            },
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        }        
    });
});

function cancelSubscription() {
    $("#cancel-subscription").hide();
    $("#confirm-subscription").show();
}

function validateCancelForm() {
    var validate = $("#cancel_subscription").validate({
        rules: {
            cancel_reason: {
                required: true
            }
        },
        messages: {
            cancel_reason: {
                required: 'Please choose the reason.'
            }
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        }        
    });

    var isTrue = validate.form();

    if (isTrue) {
        document.cancel_subscription.action = '<?php echo Yii::app()->baseUrl; ?>/payment/cancelSubscription';
        document.cancel_subscription.submit();
        return false;
    }
}

function resend_link()
        {
          $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true) ?>/admin/resendEmailVerificationLink",
            type: 'POST',
            success: function (res) {
              if(res==1)
              {
                  alert("Email verification link sent");
              }
            }
        });    
            
        }
function openinmodal(url) {
    //$('.loaderDiv').show();   
    $.get(url, {"modalflag": 1}, function (data) {
        //$('.loaderDiv').hide();
        $('#mymodaldiv').html(data);
        $("#mymodal").modal('show');
    });
}
</script>
<!-- Modal Starts Here -->
<div  class="loaderDiv">
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.gif" />
</div>
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style>
    .loaderDiv{position: absolute;left: 45%;top:30%;display: none;}
</style>