<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
	<?php if(Yii::app()->controller->action->id!='home'){?>
	<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<?php }else{?>
		<link href="<?php echo Yii::app()->baseUrl;?>/css/landingpage.css" rel="stylesheet" type="text/css" />
	<?php }?>
     <link href="<?php echo Yii::app()->theme->baseUrl;?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
     <link href="<?php echo Yii::app()->theme->baseUrl;?>/css/AdminLTE.css" rel="stylesheet" type="text/css" />
     
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
     <?php if(YII_DEBUG){
         Yii::app()->clientScript->registerCoreScript('jquery'); 
    }else{?>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <?php } ?>

    <!-- jQuery UI 1.10.3 -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery-ui-1.10.3.min.js"></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap.min.js"></script>
</head>
<body   class="wysihtml5-supported  pace-done">
    <!--<div class="container" id="page">-->
	<?php //if(Yii::app()->controller->action->id=='home'){?>
	<?php include 'header_outer.php';?>
	<?php //}?>
    <div class="wrapper row-offcanvas row-offcanvas-left" style="min-height: 589px;" id="page">
	        <?php echo $content; ?>
        <div class="clear"></div>
    </div>
</body>
</html>
