<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>    
        <link href="<?php echo Yii::app()->baseUrl; ?>/css/admin.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css?v=<?php echo RELEASE ?>" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <!-- Theme style -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/adminlte/css/AdminLTE.css?v=<?php echo RELEASE ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/adminlte/css/skins/_all-skins.css?v=<?php echo RELEASE ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/autocomplete.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css?v=<?php echo RELEASE ?>" />
        <style type="text/css">
            .flash-msg{
                position: absolute !important;
                margin-left: 25% !important;
                margin-top: 2% !important;
                z-index: 999999;
            }
            .content { overflow:auto; padding-bottom:100px; /* this needs to be bigger than footer height*/}
            .colheight{min-height: 295px;}
            .sidebar-menu .lasttop{top:-53px;}
        </style>
        <script type="text/javascript">
            var HTTP_ROOT = '<?php echo Yii::app()->baseUrl; ?>';
        </script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript">
            if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }
        </script>

        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap-typeahead.js"></script>        
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $.validator.addMethod("mail", function (value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
                }, "Please enter a correct email address");
                jQuery.validator.addMethod("phone", function (value, element) {
                    return this.optional(element) || /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(value);
                }, "Please enter correct phone number");
            });
        </script>    
    </head>
    <?php
    $studio = $this->studio;
    $home_page = Yii::app()->getbaseUrl(true);
    ?>
    <body class="skin-blue wysihtml5-supported  pace-done sidebar-mini" > 
        <?php if (!isset(Yii::app()->user->id)) { ?>
            <?php echo $content; ?>
        <?php } ?>
        
        <!------- ends --->
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/validator.min.js"></script>

        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/adminltefiles/dashboard.js?v=<?php echo RELEASE; ?>"></script>        
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/adminltefiles/app.js?v=<?php echo RELEASE; ?>"></script>     
<div  class="loaderDiv">
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.gif" />
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style type="text/css">
    .loaderDiv{position: absolute;left: 50%;top:25%;display: none;}
    .box.box-primary{border:none !important;}
    button.close{
        border-radius: 15px 15px 15px 15px;
        -moz-border-radius: 15px 15px 15px 15px;
        -webkit-border-radius: 15px 15px 15px 15px;
        width: 25px;
        height: 25px;
        margin: 5px;
        border: 2px solid #000;
    }
</style>
    </body>
</html>
