<?php
$languages = $this->enable_laguages;
$enable_lang = $this->language_code;
if ($_COOKIE['Language']) {
    $enable_lang = $_COOKIE['Language'];
}
$poster_sizes = $this->poster_sizes;
$horizontal = $poster_sizes['horizontal'];
$vertical = $poster_sizes['vertical'];
$hor_width = $horizontal['width'];
$hor_height = $horizontal['height'];
$ver_width = $vertical['width'];
$ver_height = $vertical['height'];

$lang_code = 'en';
if (isset(Yii::app()->controller->language_code) && trim(Yii::app()->controller->language_code)) {
    $lang_code = Yii::app()->controller->language_code;
}
$studio = $this->studio;
$theme = Yii::app()->common->getTemplateDetails($studio->parent_theme);
$user_logged_email = $_SESSION['login_attempt_email_id'];
$checkmultistore = Yii::app()->common->checkMultiStore($user_logged_email);
$checkparentstore = Yii::app()->common->checkparentStore($this->studio->id); //multi studio
$checkcontenttype = Yii::app()->general->content_count($this->studio->id);
if (@$_SESSION['parentchield_studio'])
    $checkchieldstore = Yii::app()->common->checkChieldStore($_SESSION['parentchield_studio']);
if (@Yii::app()->controller->id == 'admin' && in_array(@Yii::app()->controller->action->id, array('newContents', 'managecontent', 'editMovie'))) {
    $is_child_store = Yii::app()->common->childStorecheck($this->studio->id);
} else {
    $is_child_store = 0;
}
$app_type = (isset($_GET['app']) && $_GET['app'] != "") ? $_GET['app'] : false;
?>
<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang_code; ?>" lang="<?php echo $lang_code; ?>">
    <head>
        <meta charset="UTF-8">
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="icon" type="image/png" href="<?php echo Yii::app()->getBaseUrl(true) . '/img/favicon.png'; ?>">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>    
        <!--Custom CSS-->
        <style>

            #example_filter{display:none;}
            table.display tbody tr:nth-child(even) td{
                background-color: white !important;
            }

            table.display tbody tr:nth-child(odd) td {
                background-color: white !important;
            }
            table.display tbody tr:nth-child(odd):hover td {
                background: #fafbfc !important;
            }
            table.display tbody tr:nth-child(even):hover td{
                background: #fafbfc !important;
            }
            #srch_help{
                display: none;
            }
            .search_help{
                background: #fff none repeat scroll 0 0;
                margin-top: 20px;
                height: auto;

            }
            .default_cont{
                height: 500px;
                overflow: auto; 
            }
            .back_btn{
                color: #21C2F8;
                font-weight: bold;
                cursor: pointer;
            }
            .help-toggle{
                background: #fff none repeat scroll 0 0;
                border: 1px solid #ccc;
                color: #505050;
                float: right;
                font-size: 1.5em;
                padding: 3px 13px;
                border-radius: 18px;
            }

            .ui-widget-content{
                z-index: 999999 !important;
            }


        </style>
        <link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/common/cssnew/datatable.css">        

        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssstyle/muvi.css?v=<?php echo RELEASE; ?>">
        <!--Vendor / CSS from 3rd Party-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/jquery-ui.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.2.3/css/simple-line-icons.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/jquery.mCustomScrollbar.css?v=<?php echo RELEASE; ?>">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/owl.transitions.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/easypiechart.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/footable.core.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/sweetalert.css?v=45">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9 ]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <link rel="stylesheet" href="css/ie-8.css">
        <![endif]-->

        <script type="text/javascript">
            var HTTP_ROOT = '<?php echo Yii::app()->getBaseUrl(true); ?>';
            var POSTER_URL = '<?= POSTER_URL; ?>';
            var HOR_POSTER_WIDTH = "<?php echo $hor_width; ?>";
            var HOR_POSTER_HEIGHT = "<?php echo $hor_height; ?>";
            var VER_POSTER_WIDTH = "<?php echo $ver_width; ?>";
            var VER_POSTER_HEIGHT = "<?php echo $ver_height; ?>";
        </script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/jquery.min.js"></script>
        <script type="text/javascript">
            /*  if (!window.jQuery) {
             document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
             } */
            //Add blue animated border and remove with condition when focus and blur

            $(document).ready(function () {


                if ($('.fg-line')[0]) {
                    $('body').on('focus', '.fg-line .form-control', function () {
                        $(this).closest('.fg-line').addClass('fg-toggled');
                    });

                    $('body').on('blur', '.form-control', function () {
                        var p = $(this).closest('.form-group, .input-group'),
                                i = p.find('.form-control').val();

                        if (p.hasClass('fg-float')) {
                            if (i.length === 0) {
                                $(this).closest('.fg-line').removeClass('fg-toggled');
                            }
                        } else {
                            $(this).closest('.fg-line').removeClass('fg-toggled');
                        }
                    });
                }
                $('body').on('click', '.action', function (e) {
                    e.preventDefault();
                    $('.left-pos-and-initial-Show').toggleClass('sidebar-toggled');
                });

                // Select the image in gallery
                $('.Gallery-Row').on("click", ".Preview-Block", function () {
                    $('.overlay').removeAttr('style');
                    $(this).children().find('.overlay').css('opacity', '1');
                });

            });

            function resend_link()
            {
                $.ajax({
                    url: "<?php echo Yii::app()->getBaseUrl(true) ?>/admin/resendEmailVerificationLink",
                    type: 'POST',
                    success: function (res) {
                        if (res == 1)
                        {
                            swal("Please check your email to complete the email verification");
                            $("#body_alert").removeClass("hasAlert");
                        }
                    }
                });

            }
        </script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/footable.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/custom.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/sweetalert.js"></script>
        <!-- Vendor / JS from 3rd Party-->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/jquery.mCustomScrollbar.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/owl.carousel.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/jquery.easypiechart.min.js"></script>

        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>        
        <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/common/js/jquery.validate.min.js"></script>
        <script type="text/javascript">
            var CHECKSTUDIOLOGIN;

        </script>
        <?php if (isset(Yii::app()->user->id) && (Yii::app()->user->id) > 0) { ?>
            <script type="text/javascript">
                CHECKSTUDIOLOGIN = 1;

            </script>
        <?php } else { ?>
            <script type="text/javascript">
                CHECKSTUDIOLOGIN = 0;

            </script>        
        <?php } ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $.validator.addMethod("mail", function (value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
                }, "Please enter a correct email address");
                jQuery.validator.addMethod("phone", function (value, element) {
                    return this.optional(element) || /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(value);
                }, "Please enter correct phone number");

                $("#aq").autocomplete({
                    minLength: 0,
                    source: function (request, response) {
                        $.ajax({
                            url: "https://www.muvi.com/wpstudio/wp-content/themes/muvi/news-search-data.php",
                            crossOrigin: true,
                            dataType: "json",
                            data: {
                                q: encodeURI(request.term)
                            },
                            success: function (data) {
                                response(data);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $("#aq").val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        document.getElementById('srch_help').style.display = "block";
                        document.getElementById('help_title').innerHTML = ui.item.label;
                        document.getElementById('help_content').innerHTML = ui.item.content;
                        //location.href = 'https://studio.muvi.in/help/'+ui.item.name;
                        $("#aq").val('');
                        $(".default_cont").hide();
                        // window.open('http://devstudio.tapan.com/help/'+ui.item.name,'_blank');


                        return false;
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>")
                            .append("<a><strong>" + item.label + "</strong></a>")
                            // .append("<a>" + item.label + "</a>")
                            .appendTo(ul);
                };


                $(".help-toggle").click(function () {
                    $(".helpbox").animate({width: 'toggle'}, "slow");
                    $(".help-toggle").toggle();
                });
                $(".back_btn").click(function () {
                    document.getElementById('srch_help').style.display = "none";
                    document.getElementById('dflt_cont').style.display = "block";
                });

            });
            (function ($) {
                $(window).load(function () {
                    $(".left-pos-and-initial-Show").mCustomScrollbar({
                        mouseWheelPixels: 100,
                        scrollInertia: 500
                    });

                });
            })(jQuery);

            function changeLang(lang_code) {
                var cname = "Language";
                var cvalue = lang_code;
                $.ajax({
                    url: HTTP_ROOT + '/site/setLanguageCookie/',
                    type: "POST",
                    data: {lang_code: lang_code},
                    async: true,
                    success: function (data) {
                        location.reload();
                    }
                });
            }

            function setCookie(cname, cvalue, exdays) {
                var domain = window.location.hostname;
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + "; " + expires + ";domain=." + domain + ";path=/";
            }
            
            var userinternetSpeed = "<?php echo isset($_SESSION['internetSpeed']) ? $_SESSION['internetSpeed'] : 1 ?>";
            <?php
                if(!$_SESSION['internetSpeed'] && $_SESSION['internetSpeed']==""){ 
                    $studio_id = Yii::app()->user->studio_id;
                    $bucketInfoDetails = Yii::app()->common->getBucketInfo('', $studio_id);
                    $internetSpeedImage = CDN_HTTP . $bucketInfoDetails['bucket_name'] . '.' . $bucketInfoDetails['s3url'] . '/check-download-speed.jpg';
            ?>
                    var imageAddr = [], downloadSize = [], desc = [];
                    imageAddr[0] = "<?php echo  $internetSpeedImage ?>";
                    downloadSize[0] = 1036053;
                    desc[0] = "Singapore S3 Bucket";
                    var startTime, endTime;
                    var speedMbps;
                    function showResults(index) {  
                       // alert("hii");
                        console.log("Hello"+index);
                        var duration = (endTime - startTime) / 1000;
                        var bitsLoaded = downloadSize[index] * 8;
                        var speedBps = (bitsLoaded / duration).toFixed(2);
                        var speedKbps = (speedBps / 1000).toFixed(2);
                        speedMbps = (speedKbps / 1000).toFixed(2);
                        userinternetSpeed = speedMbps;
                        $.post("<?php Yii::app()->baseUrl ?>/user/setUserInterNetSpeed", {speedMbps: speedMbps}, function (res) {}); 
                    }
                    window.onload = function() {

                        for (var i = 0; i < desc.length; i++) {
                            var download = new Image();
                            download.i = i;
                            download.onload = function() {
                                endTime = (new Date()).getTime();
                                showResults(this.i);
                            }
                            download.onerror = function(err, msg) {
                                console.log("Invalid image, or error downloading");
                            }
                            startTime = (new Date()).getTime();
                            download.src = imageAddr[i] + "?nnn=" + startTime;
                            console.log(download.src);
                        }
                    }
            <?php } ?>
        </script>

    </head>
    <?php
    $studio = Studios::model()->findByPk(Yii::app()->user->studio_id);
    $config = new StudioConfig();
    $class = "";
    $showsubscrip = 0;
    if ($this->studio->is_default == 0 && $this->studio->status == 1 && $this->studio->is_subscribed == 0 && $this->studio->is_deleted == 0) {
        $class = "has__message--block";
        $showsubscrip = 1;
    }
    ?>
    <body id="body_alert" class="<?php echo $class; ?>"> 
        <?php
        if ($showsubscrip) {
            ?>
            <div class="navbar-fixed-top m-b-0 message__block " role="alert">
                <div class="row">
                    <div class="col-sm-8 text-left">
                        <h5 class="m-t-0 m-b-0">Your free trial expires on <span class="f-700"><?php echo Yii::app()->common->normalDate($this->studio->start_date); ?></span>. Purchase a subscription to activate the account and unlock all awesome features. </h5>
                    </div>
                    <div class="col-sm-4 text-right">
                        <button type="button" class="btn btn-success" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/purchase');">
                            <i class="fa fa-shopping-cart"></i> 
                            &nbsp;Purchase Subscription
                        </button>
                    </div>
                </div>
            </div>
        <?php } ?>
        <nav class="navbar navbar-default navbar-fixed-top">
            <ul class="nav navbar-nav">
                <!--Hamburger Menu : Displayed in Tablet and Mobile Only-->
                <li>
                    <button class="action action--open" aria-label="Close Menu">
                        <em class="icon-menu c-black"></em>
                    </button>
                </li>
                <!--Logo : Displayed in Large and Medium Sized Desktop Only-->
                <li class="hidden-xs hidden-sm">
                    <a class="navbar-brand" href="<?php echo $this->createUrl('admin/dashboard'); ?>">
                        <?php
                        /* if(isset($this->master_logo)){?>
                          <img src="<?php echo $this->master_logo; ?>" alt="<?php echo $this->master_name; ?>" style="height: 29px;">
                          <?php }else{ */
                        ?>
                        <img src="<?php echo Yii::app()->getBaseUrl(true) . '/img/muvi-studio-logo.png'; ?>" alt="Muvi Logo">
                        <?php //}?>
                    </a>
                </li>
                <!--Menu -->
                <li class="pull-right relative">
                    <?php if (isset($this->master_logo)) { ?>
                        <!--Reseller Logo Div-->
                        <div class="absolute partner-brand hidden-xs">
                            <p class="font-12 grey">Managed By</p>
                            <img src="<?php echo $this->master_logo; ?>" alt="Partner Logo">
                        </div>
                    <?php } ?>
                    <ul class="list-inline MenuRight">
                        <?php
                        $is_preview = 0;

                        if ($this->dummy_data_completed == 1) {
                            if ($studio->is_default == 0 && $studio->status == 1 && $studio->is_subscribed == 0 && $studio->is_deleted == 0) {//Lead 
                                $is_preview = 1;
                            } else if ($studio->is_default == 0 && $studio->status == 1 && $studio->is_subscribed == 1 && $studio->is_deleted == 0) {//Customer
                                $is_preview = 1;
                            } else if ($studio->is_default == 1 && $studio->status == 1) {//Demo
                                $is_preview = 1;
                            }
                        }
                        ?>
                        <?php
                        if (count($languages) > 1) {
                            ?>
                            <li class="p-b-0">
                                <div class="fg-line">
                                    <div class="select" style="margin-top: -1px;">
                                        <select class="form-control input-sm" onchange="changeLang(this.value)" style="padding-right:30px">
                                            <?php
                                            foreach ($languages as $key => $value) {
                                                if(trim($value['translated_name']) !="") {
                                                 $value['name'] = $value['translated_name'];
                                                 }
                                                if ($value['status'] != 0) {
                                                    ?>
                                                    <option value="<?php echo $value['code']; ?>" <?php
                                                    if ($enable_lang == $value['code']) {
                                                        echo "SELECTED";
                                                    }
                                                    ?>><?php echo $value['name']; ?></option>
                                                <?php } elseif ($value['code'] == "en") { ?>
                                                    <option value="<?php echo $value['code']; ?>" <?php
                                                            if ($enable_lang == $value['code']) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?php echo $value['name']; ?></option>
                                <?php }
                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </li>
                        <?php }
                        if (intval($is_preview)) {
                            ?>
                            <li class="border">
                                <a href="javascript:void(0);" onclick="window.open('http://<?php echo $studio->domain; ?>?preview=2', '_blank');">Preview Website</a>
                            </li>
<?php } else if ($this->dummy_data_completed == 0) {
    ?>
                            <li class="border">
                                <a href="javascript:void(0);">Preparing your website <span class="fa fa-refresh fa-spin-custom" aria-hidden="true"></span></a>
                            </li>
                            <li class="border" style="display:none;">
                                <a href="javascript:void(0);"  onclick="window.open('http://<?php echo $studio->domain; ?>?preview=2', '_blank');">Preview Website</a>
                            </li>

<?php } ?>	

                        <li class="dropdown">
                            <!--Logged in User Name-->
                            <a data-toggle="dropdown" href="" aria-expanded="false">
                                <span class="hidden-xs text-capitalize">
                                    <?php
                                    if (Yii::app()->user->first_name) {
                                        echo ucfirst(Yii::app()->user->first_name) . " " . ucfirst(Yii::app()->user->getState('last_name'));
                                    } else {
                                        echo strstr(Yii::app()->user->email, '@', true);
                                    }
                                    ?> &nbsp;&nbsp;
                                </span>
                                <em class="icon-options-vertical font-12"></em>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li class="LoggedIn-User">
                                    <b class="text-capitalize">
                                        <?php
                                        if (Yii::app()->user->first_name) {
                                            echo ucfirst(Yii::app()->user->first_name) . " " . ucfirst(Yii::app()->user->getState('last_name'));
                                        } else {
                                            echo strstr(Yii::app()->user->email, '@', true);
                                        }
                                        ?>
                                    </b>
                                <?php if (isset(Yii::app()->user->created_at)) { ?>
                                        <span class="light-grey">Member since <?php echo date('M Y', strtotime(Yii::app()->user->created_at)); ?></span>
<?php } ?>
                                </li>
<?php
if ($checkmultistore) {
    foreach ($checkmultistore AS $avlmulticheck) {
        ?> 
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:void(0);" data-studio_id="<?php echo $avlmulticheck['studio_id']; ?>" data-studio_name="<?php echo $avlmulticheck['studio_name']; ?>" data-studio_email="<?php echo $avlmulticheck['studio_email']; ?>" onclick="multiStudioLogin(this);">
                                                <em class="icon-link"></em> &nbsp;&nbsp;<?php echo $avlmulticheck['studio_name']; ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?> 
<?php
if (@$checkchieldstore && is_array($checkchieldstore)) {
    foreach ($checkchieldstore AS $avlmulticheck) {
        ?> 
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:void(0);" data-studio_id="<?php echo @$avlmulticheck['studio_id']; ?>" data-studio_name="<?php echo @$avlmulticheck['studio_name']; ?>" data-studio_email="<?php echo @$avlmulticheck['studio_email']; ?>" onclick="multiStudioLogin(this);">
                                                <em class="icon-link"></em> &nbsp;&nbsp;<?php echo @$avlmulticheck['studio_name']; ?>
                                            </a>
                                        </li>
        <?php
    }
}
?>

                                <li class="divider"></li>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('user/logout'); ?>">
                                        <em class="icon-power"></em> &nbsp;&nbsp;Log Out
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div class="container-fluid">
            <?php
            $studio_id = Yii::app()->user->studio_id;
            $app = Yii::app()->general->apps_count($studio_id);
            @$activeAction = @Yii::app()->controller->action->id;
            if (@$app_type) {
                $activeAction = @$app_type;
            }
            $active_item = "";
            $icon_rotate = "";
            $style_menu = "";
            ?>
            <!--Sidebar Navigation-->
            <div class="sidebar left  left-pos-and-initial-Show">
                <ul class="menu-wrap">
                    <?php if (Yii::app()->common->hasPermission('home', 'view')) { ?>
                        <li class="<?php if (in_array($activeAction, array('dashboard'))) { ?>active-item<?php } ?>">
                            <a href="<?php echo $this->createUrl('admin/dashboard'); ?>">
                                <em class="icon-home left-icon"></em> Home
                            </a>
                        </li>
                    <?php
                    }
                    if (Yii::app()->common->hasPermission('content', 'view')) {
                        if (in_array($activeAction, array('managecontent', 'celeblist', 'manageCategory', 'manageSubCategory', 'contenttype', 'mrss', 'manageChannel', 'manageimage', 'managevideo', 'manageplaylist'))) {
                            $active_item = "active-item";
                        }
                        if (Yii::app()->controller->id == "mrss") {
                            $active_item = "active-item";
                        }
                        ?>
                        <li class="<?php echo $active_item; ?>">
                            <a href="javascript:void(0);">
                                <em class="icon-film left-icon"></em> Manage Content
                                <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                            </a>

                            <!--Submenus-->
                            <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                <li <?php if ($activeAction == 'managecontent') { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('admin/managecontent'); ?>">
                                        Content Library <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                    </a>

                                    <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                        <li <?php if (($activeAction == 'managecontent') && (Yii::app()->controller->id == 'admin')) { ?>class="active-sub-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('admin/managecontentorder'); ?>">Content Ordering</a>
                                        </li>
                                    </ul>





                                </li>
    <?php if (empty($checkcontenttype) || (isset($checkcontenttype) && ($checkcontenttype['content_count'] & 1))) { ?>
                                    <li <?php if ($activeAction == 'managevideo') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('management/managevideo'); ?>">
                                            Video Library
                                        </a>
                                    </li>
    <?php }if ((isset($checkcontenttype) && ($checkcontenttype['content_count'] & 4))) { ?>
                                    <li <?php if ($activeAction == 'manageaudio') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('management/manageaudio'); ?>">
                                            Audio Library
                                        </a>
                                    </li>
                                <?php } ?>                                
                                <li <?php if ($activeAction == 'manageimage') { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('management/manageimage'); ?>">
                                        Image Library
                                    </a>
                                </li>
								<?php $IsDownloadable = Yii::app()->general->IsDownloadable($studio_id);
									if($IsDownloadable == 1){?>
								<li <?php if ($activeAction == 'managefile') { ?>class="active-sub-item"<?php } ?>>
									<a href="<?php echo $this->createUrl('management/managefile'); ?>">
										File Library
									</a>
								</li>	
								<?php }?>
                                <li <?php if ($activeAction == 'celeblist') { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('adminceleb/celeblist'); ?>">
                                        Manage Cast/Crew
                                    </a>
                                </li>
                                <?php if ((isset($checkcontenttype) && ($checkcontenttype['content_count'] & 4))) { ?>
                                    <li <?php if ($activeAction == 'ManagePlaylist') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('management/ManagePlaylist'); ?>">
                                            Manage Playlist
                                        </a>
                                    </li>
    <?php } ?>
    <?php /* if (Yii::app()->common->isLivestream()) { ?>
      <li <?php if ($activeAction == 'manageChannel') { ?>class="active-sub-item"<?php } ?>>
      <a href="<?php echo $this->createUrl('lstream/manageChannel'); ?>">
      Live Streaming
      </a>
      </li>
      <?php } */ ?>
                                <li <?php if (($activeAction == 'manageCategory') || ($activeAction == 'manageSubCategory')) { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('category/manageCategory'); ?>">
                                        Manage Metadata
                                    </a>
                                </li>                                
                                <!--<li <?php if (Yii::app()->controller->id == "mrss") { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('mrss/index'); ?>">
                                        Manage MRSS Feed
                                    </a>
                                </li>-->
                                <li <?php if ($activeAction == 'ContentSetting') { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('management/ContentSetting'); ?>">
                                        Settings
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) { ?>
    <?php
    if (Yii::app()->common->hasPermission('website', 'view')) {
        $active_item = "";
        if ((Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'drafttemplate' ) && (in_array($activeAction, array('home', 'celeblist', 'banner', 'bannertext', 'homepage', 'footerlinks', 'cmspage', 'editpage', 'logo', 'domainname', 'videobanner', 'settings', 'menu', 'cmsblocks', 'cmsblock', 'editor')))) {
            $active_item = "active-item";
        }
        if (empty($app) || (isset($app) && ($app['apps_menu'] & 1))) {
            ?>
                                <li class="<?php echo $active_item; ?>">
                                    <a href="javascript:void(0);">
                                        <em class="icon-globe left-icon"></em> Website
                                        <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                    </a>

                                    <!--Submenus-->
                                    <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
            <?php
            $active_item = "";
            if ((Yii::app()->controller->id == 'template') && ($activeAction == 'home' || $activeAction == 'logo' || $activeAction == 'banner' || $activeAction == 'featuredsections' || $activeAction == 'videobanner' || $activeAction == 'settings')) {
                $active_item = "active-sub-item";
            }
            ?>
                                        <li class="<?php echo $active_item; ?>">
            <?php if (Yii::app()->user->studio_id != 52 && Yii::app()->user->studio_id != 465) { ?>
                                                <a href="javascript:void(0);">Templates
                                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                                </a>
                                                <?php } else { ?>
                                                <a href="javascript:void(0);">Templates
                                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                                </a>
                                                <?php } ?>	
                                            <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                <li <?php if (($activeAction == 'homepage') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('template/homepage'); ?>">Home Page</a>
                                                </li>
            <?php if (Yii::app()->user->studio_id != 52 && Yii::app()->user->studio_id != 465) { ?>
                                                    <li <?php if (($activeAction == 'home') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                        <a href="<?php echo $this->createUrl('/template/'); ?>">Manage Template</a>
                                                    </li>
            <?php } ?>	

                          <!--<li <?php if ($activeAction == 'settings') { ?>class="active-sub-sub-item"<?php } ?>>
                              <a href="<?php echo $this->createUrl('template/settings'); ?>">Advanced Settings</a>
                          </li>  -->                                          
                                            </ul>
                                        </li>
                                        <li <?php if ($activeAction == 'menu') { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/template/menu'); ?>">Menu</a>
                                        </li> 
                                        <li <?php if ($activeAction == 'footerlinks' || $activeAction == 'editpage') { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/template/footerlinks'); ?>">Static Pages</a>
                                        </li>
                                        <li <?php if ($activeAction == 'cmspage') { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/template/cmspage'); ?>">Terms of use</a>
                                        </li>
                                        <li <?php if ($activeAction == 'domainname') { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/template/domainname'); ?>">Domain Name</a>
                                        </li>
                                        <li <?php if ($activeAction == 'cmsblocks' || $activeAction == 'cmsblock') { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/template/cmsblocks'); ?>">Widgets</a>
                                        </li>    
                                    </ul>
                                </li>
                                <?php
                            }
                            $active_item = "";
                            if (((Yii::app()->controller->id == 'template' && in_array($activeAction, array('androidApp', 'iOSApp', 'rokuApp', 'androidTvApp', 'fireTvApp', 'appleTvApp'))) || (Yii::app()->controller->id == 'apps' && in_array($activeAction, array('android', 'ios', 'roku', 'android-tv', 'fire-tv', 'apple-tv', 'homepage', 'menu'))))) {
                                $active_item = "active-item";
                            }
                            if (empty($app)) {
                                ?>
                                <li class="<?php echo $active_item; ?>">
                                    <a href="javascript:void(0);">
                                        <em class="fa fa-television left-icon"></em> Mobile & TV Apps
                                        <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                    </a>
                                    <!--1st Level Submenu-->
                                    <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                        <li <?php if ((in_array($activeAction, array('androidApp', 'android'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="javascript:void(0);">Android App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                            <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                <li <?php if (($activeAction == 'android') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/apps/android/template'); ?>">Template</a>
                                                </li>
                                                <li <?php if (($activeAction == 'androidApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/template/androidApp'); ?>">Publish to App Store</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li <?php if ((in_array($activeAction, array('iOSApp', 'ios'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="javascript:void(0);">iOS App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                            <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                <li <?php if (($activeAction == 'ios') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/apps/ios/template'); ?>">Template</a>
                                                </li>
                                                <li <?php if (($activeAction == 'iOSApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/template/iOSApp'); ?>">Publish to App Store</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li <?php if ((in_array($activeAction, array('rokuApp', 'roku'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="javascript:void(0);">Roku App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                            <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                <!--li <?php if (($activeAction == 'roku') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/apps/roku/template'); ?>">Template</a>
                                                </li-->
                                                <li <?php if (($activeAction == 'rokuApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/template/rokuApp'); ?>">Publish to App Store</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li <?php if ((in_array($activeAction, array('androidTvApp', 'android-tv'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="javascript:void(0);">Android TV App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                            <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                <!--li <?php if (($activeAction == 'android-tv') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/apps/android-tv/template'); ?>">Template</a>
                                                </li-->
                                                <li <?php if (($activeAction == 'androidTvApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/template/androidTvApp'); ?>">Publish to App Store</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li <?php if ((in_array($activeAction, array('fireTvApp', 'fire-tv'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="javascript:void(0);">Fire TV App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                            <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                <!--li <?php if (($activeAction == 'fire-tv') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/apps/fire-tv/template'); ?>">Template</a>
                                                </li-->
                                                <li <?php if (($activeAction == 'fireTvApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/template/fireTvApp'); ?>">Publish to App Store</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li <?php if ((in_array($activeAction, array('appleTvApp', 'apple-tv'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="javascript:void(0);">Apple Tv App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                            <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                <!--li <?php if (($activeAction == 'apple-tv') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/apps/apple-tv/template'); ?>">Template</a>
                                                </li-->
                                                <li <?php if (($activeAction == 'appleTvApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/template/appleTvApp'); ?>">Publish to App Store</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li <?php if (($activeAction == 'homepage') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/apps/homepage'); ?>">Home Page</a>
                                        </li> 
                                        <li <?php if (($activeAction == 'menu') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/apps/menu'); ?>">Menu</a>
                                        </li> 
                                        <li <?php if (($activeAction == 'notificationcenter') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/template/notificationcenter'); ?>">App Notification center</a>
                                        </li>                                     
                                    </ul>
                                </li>
                                <?php
                            } else {
                                if (isset($app) && (($app['apps_menu'] & 2) || ($app['apps_menu']) & 4 || ($app['apps_menu']) & 8 || ($app['apps_menu']) & 16 || ($app['apps_menu']) & 32 || ($app['apps_menu']) & 128)) {
                                    ?>
                                    <li class="<?php echo $active_item; ?>">
                                        <a href="javascript:void(0);">
                                            <em class="fa fa-television left-icon"></em> Mobile & TV Apps
                                            <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                        </a>
                                        <!--1st Level Submenu-->
                                        <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                <?php if (isset($app) && ($app['apps_menu'] & 4)) { ?>
                                                <li <?php if ((in_array($activeAction, array('androidApp', 'android'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                                    <a href="javascript:void(0);">Android App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                                    <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                        <li <?php if (($activeAction == 'android') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/apps/android/template'); ?>">Template</a>
                                                        </li>
                                                        <li <?php if (($activeAction == 'androidApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/template/androidApp'); ?>">Publish to App Store</a>
                                                        </li>
                                                    </ul>
                                                </li>
                <?php } if (isset($app) && ($app['apps_menu'] & 2)) { ?>
                                                <li <?php if ((in_array($activeAction, array('iOSApp', 'ios'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                                    <a href="javascript:void(0);">iOS App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                                    <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                        <li <?php if (($activeAction == 'ios') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/apps/ios/template'); ?>">Template</a>
                                                        </li>
                                                        <li <?php if (($activeAction == 'iOSApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/template/iOSApp'); ?>">Publish to App Store</a>
                                                        </li>
                                                    </ul>
                                                </li>

                <?php } if (isset($app) && ($app['apps_menu'] & 8)) { ?>
                                                <li <?php if ((in_array($activeAction, array('rokuApp', 'roku'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                                    <a href="javascript:void(0);">Roku App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                                    <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                        <!--li <?php if (($activeAction == 'roku') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/apps/roku/template'); ?>">Template</a>
                                                        </li-->
                                                        <li <?php if (($activeAction == 'rokuApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/template/rokuApp'); ?>">Publish to App Store</a>
                                                        </li>
                                                    </ul>
                                                </li>

                <?php } if (isset($app) && ($app['apps_menu'] & 128)) { ?>
                                                <li <?php if ((in_array($activeAction, array('androidTvApp', 'android-tv'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                                    <a href="javascript:void(0);">Android TV App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                                    <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                        <!--li <?php if (($activeAction == 'android-tv') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/apps/android-tv/template'); ?>">Template</a>
                                                        </li-->
                                                        <li <?php if (($activeAction == 'androidTvApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/template/androidTvApp'); ?>">Publish to App Store</a>
                                                        </li>
                                                    </ul>
                                                </li>

                <?php } if (isset($app) && ($app['apps_menu'] & 16)) { ?>
                                                <li <?php if ((in_array($activeAction, array('fireTvApp', 'fire-tv'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                                    <a href="javascript:void(0);">Fire TV App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                                    <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                        <!--li <?php if (($activeAction == 'fire-tv') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/apps/fire-tv/template'); ?>">Template</a>
                                                        </li-->
                                                        <li <?php if (($activeAction == 'fireTvApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/template/fireTvApp'); ?>">Publish to App Store</a>
                                                        </li>
                                                    </ul>
                                                </li>
                <?php } if (isset($app) && ($app['apps_menu'] & 32)) { ?>
                                                <li <?php if ((in_array($activeAction, array('appleTvApp', 'apple-tv'))) && (Yii::app()->controller->id == 'template' || Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                                    <a href="javascript:void(0);">Apple Tv App <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em></a>
                                                    <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                        <!--li <?php if (($activeAction == 'apple-tv') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/apps/apple-tv/template'); ?>">Template</a>
                                                        </li-->
                                                        <li <?php if (($activeAction == 'appleTvApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-sub-item"<?php } ?>>
                                                            <a href="<?php echo $this->createUrl('/template/appleTvApp'); ?>">Publish to App Store</a>
                                                        </li>
                                                    </ul>
                                                </li>
                <?php } ?>
                <?php if (isset($app)) { ?>
                                                <li <?php if (($activeAction == 'homepage') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/apps/homepage'); ?>">Home Page</a>
                                                </li> 
                                                <li <?php if (($activeAction == 'menu') && (Yii::app()->controller->id == 'apps')) { ?>class="active-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/apps/menu'); ?>">Menu</a>
                                                </li> 
                                                <li <?php if (($activeAction == 'notificationcenter') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('/template/notificationcenter'); ?>">App Notification center</a>
                                                </li>
                                    <?php } ?>
                                        </ul>
                                    </li>
                                    <?php
                                }
                            }

                            $active_item = "";
                            if ((Yii::app()->controller->id == 'template') && ($activeAction == 'tvguide')) {
                                $active_item = "active-item";
                            }
                            if ($this->has_tvguide > 0) {
                                ?>
                                <li <?php if ((Yii::app()->controller->id == 'template') && ($activeAction == 'tvguide')) { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('template/tvguide/'); ?>"><em class="left-icon"><img src=<?php echo Yii::app()->baseUrl; ?>"/img/playout.png" alt="" width="18" style="margin-top:-10px;"></em> Playout</a>
                                </li>
        <?php } ?> 

        <?php
        //$active_item = "";
        //if ((Yii::app()->controller->id == 'template') && ($activeAction == 'rokuApp')) {
        //    $active_item = "active-item";
        //}
        //if(empty($app) || (isset($app) && ($app['apps_menu'] & 8)) ){
        ?>
        <!--                            <li class="<?php echo $active_item; ?>">
                                <a href="javascript:void(0);">
                                    <em class="fa fa-television left-icon"></em> TV Apps
                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                </a>
                            <!--1st Level Submenu-->
                            <!--<ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                <li <?php if (($activeAction == 'rokuApp') && (Yii::app()->controller->id == 'template')) { ?>class="active-sub-item"<?php } ?>>
                                    <a href="<?php echo $this->createUrl('/template/rokuApp'); ?>">Roku App</a>
                                </li>
                            </ul>
                        </li>
                        </li>-->

                            <!--Physical goods links-->
                                    <?php
                                    $pgconfig = Yii::app()->general->getStoreLink();
                                    if ($pgconfig) {
                                        ?>
                                <li class="<?php if ((Yii::app()->controller->id == 'store') && in_array($activeAction, array('GoodsLibrary', 'order', 'Settings', 'Preorder'))) {
                                            echo "active-item";
                                        } ?>">
                                    <a href="javascript:void(0);">
                                        <em class="icon-handbag left-icon"></em> Muvi Kart
                                        <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                    </a>
                                    <!--1st Level Submenu-->
                                    <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                <?php /* <li <?php if (($activeAction == 'GoodsLibrary') && (Yii::app()->controller->id == 'store')) { ?>class="active-sub-item"<?php } ?>>
                                  <a href="<?php echo $this->createUrl('/store'); ?>">Store</a>
                                  </li> */ ?>
                                        <li <?php if (($activeAction == 'order') && (Yii::app()->controller->id == 'store')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/store/order'); ?>">Order</a>
                                        </li>
                                        <li <?php if (($activeAction == 'Settings') && (Yii::app()->controller->id == 'store')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/store/Settings'); ?>">Settings</a>
                                        </li>
                                        <li <?php if (($activeAction == 'ShippingCost') && (Yii::app()->controller->id == 'store')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/store/ShippingCost'); ?>">Shipping</a>
                                        </li>
                                    </ul>
                                </li>
        <?php } ?>
                            <!--End Physical goods-->
                            <li class="treeview <?php if (in_array($activeAction, array('managePlayer'))) { ?>active-item<?php } ?>">
                                <a href="<?php echo $this->createUrl('management/managePlayer'); ?>">
                                    <em class="icon-control-play left-icon"></em> Player
                                </a>
                            </li>
                                    <?php
                                    $active_item = "";
                                    if ((Yii::app()->controller->id == 'userfeature') && ($activeAction == 'ratings' || $activeAction == 'newsletter' || $activeAction == 'addtofavourite' || $activeAction == 'myLibrary')) {
                                        $active_item = "active-item";
                                    }
                                    ?>
                            <li class="<?php echo $active_item; ?>">
                                <a href="javascript:void(0);">
                                    <em class="icon-user-follow left-icon"></em> User Features
                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                </a>
                                <!--1st Level Submenu-->
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                    <li <?php if (($studio->parent_theme != 'singleshow-a') && ($activeAction == 'ratings') && (Yii::app()->controller->id == 'userfeature')) { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('/userfeature/ratings'); ?>">Rating</a>
                                    </li>
                                    <?php
                                    $config = $config->getconfigvalue('newsletter');
                                    if ($config['config_value'] == 1) {
                                        ?>
                                        <li <?php if (($activeAction == 'newsletter') && (Yii::app()->controller->id == 'userfeature')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/userfeature/newsletter'); ?>">News Letter</a>
                                        </li>
        <?php } ?>
                                    <li <?php if (($activeAction == 'socialnetworks') && (Yii::app()->controller->id == 'userfeature')) { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('/userfeature/socialnetworks'); ?>">Social network integration</a>
                                    </li>
        <?php /* ?>        
          <li <?php if (($activeAction == 'mylibrary') && (Yii::app()->controller->id == 'mylibrary')) { ?>class="active-sub-item"<?php } ?>>
          <a href="<?php echo $this->createUrl('/userfeature/myLibrary'); ?>">My Library</a>
          </li>
          <li <?php if (($activeAction == 'addtofavourite') && (Yii::app()->controller->id == 'userfeature')) { ?>class="active-sub-item"<?php } ?>>
          <a href="<?php echo $this->createUrl('/userfeature/addtofavourite'); ?>">Add to Favorites</a>
          </li>
         */
        ?>
                                    <li <?php if (($activeAction == 'settings') && (Yii::app()->controller->id == 'settings')) { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('/userfeature/Settings'); ?>">Settings</a>
                                    </li>
                                    <li <?php if (($activeAction == 'userRestrictions') && (Yii::app()->controller->id == 'userfeature')) { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('/userfeature/userRestrictions'); ?>">User Restrictions</a>
                                    </li>
                                    <li <?php if (($activeAction == 'managequeue') && (Yii::app()->controller->id == 'managequeue')) { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('/userfeature/managequeue'); ?>">Manage Queue</a>
                                    </li>
                                    <?php 
                                        $app = Yii::app()->general->apps_count($studio_id); 
                                        if(empty($app) || (isset($app) && ($app['apps_menu'] & 2)) || (isset($app) && ($app['apps_menu'] & 4))){
                                    ?>
                                    <li <?php if (($activeAction == 'mobileapps') && (Yii::app()->controller->id == 'mobileapps')) { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('/userfeature/mobileapps'); ?>">Mobile Apps</a>
                                    </li> 
                                    <?php } ?>
                                </ul>
                            </li>
        <?php
    }
}
?>
<?php if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) { ?>
    <?php
    if (Yii::app()->common->hasPermission('report', 'view')) {
        $active_item = "";
        if (in_array($activeAction, array('revenue', 'subscribers', 'usages', 'usersReport', 'userdata', 'general', 'video', 'contentPartners', 'CustomReport'))) {
            $active_item = "active-item";
        }
        ?>
                            <li class="<?php echo $active_item; ?>">
                                <a href="javascript:void(0);">
                                    <em class="icon-graph left-icon"></em> Analytics
                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                </a>
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                    <li <?php if ($activeAction == 'revenue') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('report/revenue'); ?>">Revenue</a>
                                    </li>
                                    <li <?php if ($activeAction == 'usersReport') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('report/usersReport'); ?>">Users</a>
                                    </li>
                                    <li <?php if ($activeAction == 'usages') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('report/usages'); ?>">User Action</a>
                                    </li>
                                    <li <?php if ($activeAction == 'video') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('report/video'); ?>">Video</a>	
        <!--                                        <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                            <li <?php if ($activeAction == 'video') { ?>class="active-sub-sub-item"<?php } ?>>
                                                <a href="<?php echo $this->createUrl('report/video'); ?>">Views</a>
                                            </li>
                                            <li <?php if ($activeAction == 'analytics') { ?>class="active-sub-sub-item"<?php } ?>>
                                                <a href="<?php echo $this->createUrl('report/analytics'); ?>">Analytics</a>
                                            </li>
                                        </ul>-->

                                    </li>
                                    <li <?php if ($activeAction == 'contentPartners') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('report/contentPartners'); ?>">Content Partners</a>
                                    </li>
                                    <li <?php if ($activeAction == 'CustomReport') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('report/CustomReport'); ?>">Custom Reports</a>
                                    </li>
                                </ul>
                            </li>
    <?php }
    ?>
    <?php
    if (Yii::app()->common->hasPermission('marketing', 'view')) {
        $active_item = "";
        if (in_array($activeAction, array('email_triggers', 'advancedSetting', 'seo'))) {
            $active_item = "active-item";
        }
        ?>
                            <li class="<?php echo $active_item; ?>">
                                <a href="javascript:void(0);">
                                    <em class="icon-hourglass left-icon"></em> Marketing
                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                </a>
                                <!--1st Level Submenu-->
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                    <li <?php if ($activeAction == 'email_triggers') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('admin/email_triggers'); ?>">Email Triggers</a>
                                    </li>
                                    <li <?php if ($activeAction == 'seo') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('admin/seo'); ?>">SEO</a>
                                    </li>
                                    <li <?php if ($activeAction == 'advancedSetting') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('admin/advancedSetting'); ?>">Advanced</a>
                                    </li>
                                </ul>
                            </li>
        <?php
    }
    if (Yii::app()->common->hasPermission('monetization', 'view')) {
        $active_item = "";
        if ((in_array(Yii::app()->controller->id, array('monetization', 'admin', 'store'))) && (in_array($activeAction, array('subscriptions', 'ppv', 'ppvbundles', 'videoad', 'coupons', 'payment', 'restrictions', 'settings', 'currencysetup')))) {
            $active_item = "active-item";
        }
        $studio_id = $this->studio->id;
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        ?> 
                            <li class="<?php echo $active_item; ?>" id="monetization-menu">
                                <input type="hidden" id="monetization-menu-count" value="0" />
                                <a href="javascript:void(0);" data-id="1">
                                    <em class="fa fa-money left-icon"></em> Monetization
                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                </a>
                                <!--1st Level Submenu-->
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
        <?php if (Yii::app()->common->isPaymentGatwayExists($this->studio->id)) { ?>
                                        <li <?php if (in_array($activeAction, array('subscriptions'))) { ?>class="active-sub-item"<?php } ?> <?php echo (isset($data) && (($data['menu'] & 1) || ($data['menu'] & 256))) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                                    <a href="<?php echo $this->createUrl('monetization/subscriptions'); ?>">Subscription</a>

                                                </li>

                                        <li <?php if (in_array($activeAction, array('ppv', 'ppvbundles', 'advancePurchase', 'ppvSettings'))) { ?>class="active-sub-item"<?php } ?> <?php echo (isset($data) && (($data['menu'] & 2) || ($data['menu'] & 16) || ($data['menu'] & 64))) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                            <a href="javascript:void(0);">Pay per view
                                                <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                            </a>	
                                            <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                <li <?php if ($activeAction == 'ppvSettings') { ?>class="active-sub-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 2)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                                    <a href="<?php echo $this->createUrl('monetization/ppvSettings'); ?>">Settings</a>
                                                </li>
                                                <li <?php if ($activeAction == 'ppv') { ?>class="active-sub-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 2)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                                    <a href="<?php echo $this->createUrl('monetization/ppv'); ?>">Pay per view</a>
                                                </li>
                                                <li <?php if ($activeAction == 'ppvbundles') { ?>class="active-sub-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 64)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                                    <a href="<?php echo $this->createUrl('monetization/ppvbundles'); ?>">Pay per view Bundles</a>
                                                </li>
                                                <li <?php if ($activeAction == 'advancePurchase') { ?>class="active-sub-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 16)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                                    <a href="<?php echo $this->createUrl('monetization/advancePurchase'); ?>">Pre-Order</a>
                                                </li> 
                                            </ul>
                                        </li>
        <?php } ?>

                                    <li <?php if ($activeAction == 'freecontent') { ?>class="active-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 8)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                        <a href="<?php echo $this->createUrl('monetization/freecontent'); ?>">Free Content</a>
                                    </li>
                                    <li <?php if ($activeAction == 'videoad') { ?>class="active-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 4)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                        <a href="<?php echo $this->createUrl('admin/videoad'); ?>">Advertisement</a>
                                    </li>
                                    <li <?php if ($activeAction == 'coupons') { ?>class="active-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 32)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                        <a href="<?php echo $this->createUrl('monetization/coupons'); ?>">Coupons</a>
                                    </li>

                                    <li <?php if (in_array($activeAction, array('voucher', 'voucherSettings'))) { ?>class="active-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 128)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                        <a href="javascript:void(0);">Voucher
                                            <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                        </a>

                                        <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                            <li <?php if ($activeAction == 'voucherSettings') { ?>class="active-sub-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 128)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                                <a href="<?php echo $this->createUrl('monetization/voucherSettings'); ?>">Settings</a>
                                            </li>
                                            <li <?php if ($activeAction == 'voucher') { ?>class="active-sub-sub-item"<?php } ?> <?php echo (isset($data) && ($data['menu'] & 128)) ? 'style="display:block;"' : 'style="display:none;"' ?>>
                                                <a href="<?php echo $this->createUrl('monetization/voucher'); ?>">Voucher</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li <?php if ((Yii::app()->controller->id == 'monetization') && ($activeAction == 'invoice')) { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('monetization/invoice'); ?>">Invoice</a>
                                    </li>
                                    <li <?php if ($activeAction == 'payment') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('monetization/payment'); ?>">Payment Gateway</a>
                                    </li>
        <!--                                    <li <?php if ($activeAction == 'restrictions') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('monetization/restrictions'); ?>">Restriction</a>
                                    </li>-->

                                    <li <?php if ((Yii::app()->controller->id == 'monetization') && ($activeAction == 'settings')) { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('monetization/settings'); ?>">Settings</a>
                                    </li>
                            <?php /*
                              if ($pgconfig) {
                              ?>
                              <li <?php if (($activeAction == 'Preorder') && (Yii::app()->controller->id == 'store')) { ?>class="active-sub-item"<?php } ?>>
                              <a href="<?php echo $this->createUrl('/store/Preorder'); ?>">Pre-Order</a>
                              </li>
                              <?php
                              } */
                            ?>
                                </ul>
                            </li>
        <?php
    }
    if (Yii::app()->common->hasPermission('extension', 'view')) {
        $active_item = "";
        if (in_array($activeAction, array('marketplace', 'blog', 'blogcontent', 'blogcomment', 'faq', 'addfaq', 'editfaq', 'tvguide'))) {
            $active_item = "active-item";
        }
        ?> 
                            <li class="<?php echo $active_item; ?>">
                                <a href="javascript:void(0);">
                                    <em class="icon-link left-icon"></em> Marketplace
                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                </a>
                                <!--1st Level Submenu-->
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                    <li <?php if ((Yii::app()->controller->id == 'admin') && ($activeAction == 'marketplace')) { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('admin/marketplace/'); ?>">Manage Apps</a>
                                    </li>
                            <?php if ($this->has_blog > 0) { ?>
                                        <li <?php if ((Yii::app()->controller->id == 'admin') && ($activeAction == 'blog' || $activeAction == 'blogcontent' || $activeAction == 'blogcomment')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('admin/blog/'); ?>">Blog</a>
                                        </li>
                            <?php } ?>
                            <?php if ($this->has_faqs > 0) { ?>
                                        <li <?php if ((Yii::app()->controller->id == 'admin') && ($activeAction == 'faq' || $activeAction == 'addfaq' || $activeAction == 'editfaq')) { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('admin/faq/'); ?>">Help Center</a>
                                        </li>
        <?php } ?> 


                                </ul>
                            </li>
        <?php
    }
    if (Yii::app()->common->hasPermission('billing', 'view')) {
        $active_item = "";
        if (in_array($activeAction, array('managePayment', 'paymenthistory', 'packages'))) {
            $active_item = "active-item";
        }
        ?> 
                            <li class="<?php echo $active_item; ?>">
                                <a href="javascript:void(0);">
                                    <em class="fa fa-bitcoin left-icon"></em> Billing
                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                </a>
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                    <li <?php if ($activeAction == 'managePayment') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('payment/managePayment'); ?>">Payment Information</a>
                                    </li>
                            <?php if (isset($studio->is_subscribed) && isset($studio->status) && $studio->is_subscribed == 1 && $studio->status == 1) { ?>
                                        <li <?php if ($activeAction == 'paymenthistory') { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('payment/paymenthistory'); ?>">Payment History</a>
                                        </li>
                                        <li <?php if ($activeAction == 'packages') { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('payment/packages'); ?>"> Muvi Subscription</a>
                                        </li>
                            <?php } ?>
                                </ul>
                            </li>
    <?php } ?>
                        <!--li>
                                <a href="https://www.muvi.com/helps.html" target="_blank">
                                        <em class="icon-support left-icon"></em> Help
                                </a>
                        </li-->
    <?php
    $active_item = "";
    if (Yii::app()->common->hasPermission('support', 'view')) {
        if ((Yii::app()->controller->id == 'monetization' || Yii::app()->controller->id == 'ticket') && in_array($activeAction, array('ticketList', 'endusersupport'))) {
            $active_item = "active-item";
        }
        ?> 

                            <li class="<?php echo $active_item; ?>">

                                <a href="javascript:void(0);">
                                    <em class="icon-support left-icon"></em> Support
                                    <em class="icon-arrow-right right-icon <?php echo $icon_rotate; ?>"></em>
                                </a>
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                    <li>
                                        <a href="<?php echo Yii::app()->baseUrl; ?>/help.html"  target="_blank">
                                            Help
                                        </a>
                                    </li>
                                    <li class="<?php if (Yii::app()->controller->id == 'ticket') { ?>active-item<?php } ?>">
                                        <a href="<?php echo $this->createUrl('ticket/ticketList'); ?>">
                                            All Tickets
                                        </a>
                                    </li>
                                    <li <?php if ($activeAction == 'endusersupport') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('monetization/endusersupport'); ?>">End User Support</a>
                                    </li>
                                </ul>
                            </li>


                        <?php
                        }

                        if (Yii::app()->user->role_id == 1) {
                            $active_item = "";
                            if (in_array($activeAction, array('partners'))) {
                                $active_item = "active-item";
                            }
                            ?>
                            <li <?php if ($activeAction == 'partners') { ?>class="active-sub-item"<?php } ?>>
                                <a href="<?php echo $this->createUrl('admin/partners'); ?>"><em class="icon-people left-icon"></em>Content Partner</a>
                            </li>               
                            <?php if (PolicyRules::model()->getConfigRule()) { ?>
                                <li <?php if (@Yii::app()->controller->id == 'policy') { ?>class="active-sub-item"<?php } ?>>
                                    <a href="javascript:;"><em class="fa fa-clipboard left-icon"></em> Policy Rules</a>
                                    <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
                                        <li class="<?php if (Yii::app()->controller->action->id == 'index') { ?>active-item<?php } ?>">
                                            <a href="<?php echo $this->createUrl('/policy'); ?>">
                                                Manage Rules
                                            </a>
                                        </li>
                                        <li class="<?php if (Yii::app()->controller->action->id == 'rulesbuilder') { ?>active-item<?php } ?>">
                                            <a href="<?php echo $this->createUrl('policy/rulesbuilder'); ?>">
                                                Add Rule
                                            </a>
                                        </li>
                                    </ul>
                                </li>
							<?php
							}
						}
    if (Yii::app()->common->hasPermission('setting', 'view')) {
        $active_item = "";
        if (in_array($activeAction, array('serverLocation', 'account', 'manageUsers', 'advanced', 'emailNotifications', 'managelanguage'))) {
            $active_item = "active-item";
        }
        ?> 
                            <li class="<?php echo $active_item; ?>">
                                <a href="javascript:void(0);">
                                    <em class="icon-settings left-icon"></em> Settings
                                    <em class="icon-arrow-right right-icon  <?php echo $icon_rotate; ?>"></em>
                                </a>

                                <!--1st Level Submenu-->
                                <ul class="Sub-Menu-Level--1" <?php echo $style_menu; ?>>
        <?php if (Yii::app()->user->role_id == 1) { ?>
                                        <li <?php if ($activeAction == 'manageUsers') { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('permission/manageUsers'); ?>">Manage Permissions</a>
                                        </li>

        <?php } ?>	

                                    <li <?php if ($activeAction == 'account') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('admin/account'); ?>">Account Info</a>
                                    </li>
        <?php
        if (@IS_LANGUAGE == 2) {
            $active_item = "";
            if (in_array($activeAction, array('managelanguage', 'translation'))) {
                $active_item = "active-sub-item";
            }
            ?>
                                        <li class="<?php echo $active_item; ?>">
                                            <a href="javascript:void(0);">Language
                                                <em class="icon-arrow-right right-icon  <?php echo $icon_rotate; ?>"></em>
                                            </a>
                                            <ul class="Sub-Menu-Level--2" <?php echo $style_menu; ?>>
                                                <li <?php if ($activeAction == 'managelanguage') { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('language/managelanguage'); ?>">Manage Language</a>
                                                </li>
                                                <li <?php if ($activeAction == 'translation') { ?>class="active-sub-sub-item"<?php } ?>>
                                                    <a href="<?php echo $this->createUrl('language/translation'); ?>">Translation</a>
                                                </li>
                                            </ul>
                                        </li>
        <?php } ?>
                                    <li <?php if ($activeAction == 'emailNotifications') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('admin/emailNotifications'); ?>">Email Notifications</a>
                                    </li>
                                    <li <?php if ($activeAction == 'advanced') { ?>class="active-sub-item"<?php } ?>>
                                        <a href="<?php echo $this->createUrl('admin/advanced'); ?>">Advanced</a>
                                    </li>
        <?php if ($checkparentstore) { ?>
                                        <li <?php if ($activeAction == 'sync') { ?>class="active-sub-item"<?php } ?>>
                                            <a href="<?php echo $this->createUrl('/multistudio'); ?>">Sync DATA</a>
                                        </li>
        <?php } ?>
                                </ul>
                            </li>
    <?php } ?>
<?php } ?>
                </ul>
            </div>
            <!--Sidebar : Help Block-->
            <div class="sidebar right right-pos-and-initial-Hide  help-block--of--pages" style="overflow-y:scroll;">
                <div class="close-Action pull-right">
                    <a href="javascript:void(0);">
                        <em class="icon-close"></em>
                    </a>
                </div>
                <div class="row">
                    <div class="col-xs-12 m-t-10">
                        <div class="form-group fg-float input-group">
                            <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                            <div class="fg-line">
                                <input type="text" id="aq" class="form-control fg-input" placeholder="What are you searching for?">
                            </div>

                        </div>
                    </div>
                </div>

                <div class="article-Block row m-b-20 ">
                    <div class="col-xs-12">
                        <article>         
                            <div class="search_help" id="srch_help">
                                <div class="back_btn"> << Back</div>
                                <h3> <a href="#" id="help_title"></a></h3>
                                <div id="help_content">
                                    <div style="clear:both;"></div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="postdetails"></div>
                            <div class="default_cont" id="dflt_cont">
                            <?php
                            if ((HOST_IP != '127.0.0.1' && (DOMAIN != 'idogic')) && (SUB_DOMAIN == 'studio' || SUB_DOMAIN == 'devstudio')) {
                                echo Yii::app()->common->getDefaultHelp();
                            }
                            ?>

                            </div>

                        </article>
                    </div>
                </div>

            </div>
            <div class="content">
                <!--Active Page Header-->
                <div class="Current_PageHeader">
                    <div class="row">
                        <div class="col-xs-10">
                            <?php
                            if (Yii::app()->controller->action->id == 'dashboard') {
                                $activelink = '<li class="active"><span>{label}&nbsp;</span></li>';
                                $inactivelink = '';
                            } else {
                                $activelink = '<li><a href="{url}">{label}&nbsp;</a></li>';
                                $inactivelink = '<li class="active"><span>{label}</span></li>';
                            }
                            if (isset($this->breadcrumbs)):

                                if (Yii::app()->controller->route !== 'site/index')
                                    $this->breadcrumbs = array_merge(array(Yii::t('zii', 'Home') => Yii::app()->getBaseUrl(true) . '/admin/dashboard'), $this->breadcrumbs);

                                $this->widget('zii.widgets.CBreadcrumbs', array(
                                    'links' => $this->breadcrumbs,
                                    'homeLink' => false,
                                    'tagName' => 'ol',
                                    'separator' => '',
                                    'activeLinkTemplate' => $activelink,
                                    'inactiveLinkTemplate' => $inactivelink,
                                    'htmlOptions' => array('class' => 'breadcrumb font-10 hidden-xs')
                                ));

                            endif;
                            ?>	
<?php if (Yii::app()->controller->action->id == 'dashboard') { ?>
                                <h3 class="text-capitalize f-300">dashboard</h3>

<?php } elseif (isset($this->headerinfo) && $this->headerinfo) { ?>

                                <h3 class="text-capitalize f-300"><?php echo @$this->headerinfo; ?></h3>
                <?php } else { ?>
                                <h3 class="text-capitalize f-300"><?php
                    if (isset($this->breadcrumbs)) {
                        echo $this->breadcrumbs[0];
                    }
                    ?></h3>
<?php } ?>
                        </div>
                        <div class="col-xs-2 text-right">
                            <div class="info-Action pull-right" title="Help">
                                <a href="javascript:void(0);">
                                    <em class="icon-info"></em>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

<?php if (Yii::app()->user->hasFlash('success')) { ?>
                    <div class="alert alert-success alert-dismissable flash-msg m-t-20">
                        <i class="icon-check"></i> &nbsp;
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>

<?php } elseif (Yii::app()->user->hasFlash('error')) { ?>
                    <div class="alert alert-danger alert-dismissable flash-msg m-t-20">
                        <i class="icon-ban"></i>&nbsp;
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <?php echo Yii::app()->user->getFlash('error'); ?>
                    </div>
<?php } ?>
                <div class="pace  pace-inactive">
                    <div class="pace-progress" style="width: 100%;" data-progress-text="100%" data-progress="99">
                        <div class="pace-progress-inner"></div>
                    </div>
                    <div class="pace-activity"></div>
                </div>

                <!-- Main content -->

<?php echo $content; ?>

            </div>

        </div>

        <script type="text/javascript">
            /* Bootbox modal vertical center Align 
             $("body").on("shown.bs.modal", ".modal", function() {
             $(this).css({
             'top': '40%',
             'margin-top': function () {
             return -($(this).height() / 3);
             }
             });
             });
             /* Bootbox modal vertical center Align End */

            var sucerrpopup = '';
            $(document).ready(function () {
                sucerrpopup = setTimeout("clearmsgpopu()", 10000);
            });
            $(window).load(function () {
                $(".treeview.active").find('ul.treeview-menu').show();

            });
            function clearmsgpopu() {
                $('.alert').fadeOut('slow');
                clearTimeout(sucerrpopup);
            }
            var is_sdk = 0;
<?php if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) { ?>
                var is_sdk = 1;
<?php } ?>
            /*Help Block*/
            $('.info-Action').click(function () {
                $('.right-pos-and-initial-Hide').addClass('sidebar-toggled');
            });
            $('.close-Action').click(function () {
                $('.right-pos-and-initial-Hide').removeClass('sidebar-toggled');
            });
            $(document).ready(function () {
                if (document.cookie.indexOf("timezone") >= 0) {
                } else {
                    var d = new Date();
                    var n = -(d.getTimezoneOffset());
                    setCookie("timezone", n, 1);
                }
            });
        </script>  

        <!------- ends --->
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/validator.min.js"></script>
<?php
$hide_chat = Yii::app()->general->hideChatAdmin($studio->id);
?>
<?php if (HOST_IP == '52.0.232.150' && $hide_chat == 0) { ?>
            <!--Start of Zopim Live Chat Script-->
            <script type="text/javascript">
                window.$zopim || (function (d, s) {
                    var z = $zopim = function (c) {
                        z._.push(c)
                    }, $ = z.s =
                            d.createElement(s), e = d.getElementsByTagName(s)[0];
                    z.set = function (o) {
                        z.set.
                                _.push(o)
                    };
                    z._ = [];
                    z.set._ = [];
                    $.async = !0;
                    $.setAttribute("charset", "utf-8");
                    $.src = "//v2.zopim.com/?3Ck44EbYwaLXpRDAitMdvTyD4LH7YeoO";
                    z.t = +new Date;
                    $.
                            type = "text/javascript";
                    e.parentNode.insertBefore($, e)
                })(document, "script");
            </script>
            <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/zopim.js?v=<?php echo time(); ?>"></script>
            <!--End of Zopim Live Chat Script-->     
<?php } ?>


<?php
if ($this->subscription_popup == 1) {
    if (Yii::app()->controller->action->id != 'dashboard') {
        $redirect_url .= '/admin/dashboard';
        $this->redirect($redirect_url);
        exit;
    }
    ?>
            <script>
                $(document).ready(function () {
                    openinmodal("<?php echo Yii::app()->getBaseUrl(true) ?>/payment/subscription/purchase");
                });
                function openinmodal(url) {
                    //$('.loaderDiv').show();   
                    $.get(url, {"modalflag": 1}, function (data) {
                        //$('.loaderDiv').hide();
                        $('#mymodaldiv').html(data);
                        $("#mymodal").modal('show');
                    });
                }

            </script>
            <div  class="loaderDiv">
                <div class="preloader pls-blue">
                    <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                    </svg>
                </div>
            </div>
            <!-- Modal Starts Here -->
            <div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal End  -->
            <style type="text/css">
                .loaderDiv{position: absolute;left: 50%;top:25%;display: none;}


            </style>
            <?php } ?>  

        <div class="modal fade" id="monetization-menu-settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Settings</h4>
                    </div>
                    <div class="modal-body" id="monetization-menu-settings-body">
                    </div>
                </div>
            </div>
        </div>
        <div id="MediaModal" class="modal fade is-Large-Modal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<?php
$studio_id = Yii::app()->user->studio_id;
$all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
$max_file_size = Yii::app()->common->getMaxFileSize();
$max_file_size_kb = round(($max_file_size / 1024), 2);
?>    
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Image Library</h4>
                    </div>
                    <div class="modal-body">
                        <div role="tabpanel">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#Upload_image" aria-controls="Upload-Image" role="tab" data-toggle="tab" onclick="removeGallerydetails()">Upload Image</a>
                                </li>
                                <li role="presentation"> 
                                    <a href="#Choose-From-Library" aria-controls="Choose-From-Library" role="tab" data-toggle="tab" onclick="removeimagedetails()">Choose from Gallery</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="Upload_image">
                                    <div class="row is-Scrollable">
                                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                            <form name="image_upload_to_lib" method="post" id="image_upload_to_lib" enctype="multipart/form-data">
                                                <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="browse_from_pc()">
                                                <input id="image_to_lib" name="image_to_lib" type="file" onchange="UploadToLibrary()" style="display:none;" /><span id="browsefiledetails"> No file selected</span>
                                            </form>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="Preview-Block">
                                                <div class="thumbnail m-b-0">
                                                    <div id="imgtolib_preview" class=" col-md-12 margin-topdiv">
                                                        <img id="imgtolib" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="Choose-From-Library">
                                    <input type="hidden" name="choose_img_name" id="choose_img_name" />
                                    <div class="row  Gallery-Row">
                                        <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="tinygallery">
                                        </div>
                                        <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                            <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                                <div class="preloader pls-blue text-center " >
                                                    <svg class="pl-circular" viewBox="25 25 50 50">
                                                    <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="Preview-Block row">
                                                <div class="col-md-12 text-center" id="gallery_preview">
                                                    <img id="glry_preview" width="500" height="300"/>
                                                </div>
                                            </div>
                                        </div>        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="InsertPhoto" data-image="0" data-id="">Insert Image</button>
                        <button type="button" class="btn btn-default" id="cancelPhoto" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>     
        <div id="studio_login" style="display: none;">
            <form id="studioLoginForm" name="studioLoginForm" method="post" action="<?php echo Yii::app()->baseUrl; ?>/login/studioLogin" target="StudioWindow">
                <input type="hidden" id="studio_id11" name="studio_id" value="" />
                <input type="hidden" id="studio_name" name="studio_name" value="" />
                <input type="hidden" id="studio_email" name="studio_email" value="" />
            </form>
        </div>

        <script type="text/javascript">
            function openinmodal(url) {
                $.get(url, {"modalflag": 1}, function (data) {
                    $('#mymodaldiv').html(data);
                    $("#mymodal").modal('show');
                });
            }
            function multiStudioLogin(obj) {
                var studio_id = $(obj).attr('data-studio_id');
                var studio_name = $(obj).attr('data-studio_name');
                var studio_email = $(obj).attr('data-studio_email');
                var chkmaster = "";
                //if(chkmaster==1){
                var confmsg = "Are you sure you want to login to studio '" + studio_name + "'?";
                /*}else{
                 var confmsg = "Are you sure you want to login to studio '" + studio_name + "'?\nYou will be logout from partner portal.";
                 }*/
                if (confirm(confmsg)) {
                    $("#studio_login").show();
                    $("#studio_id11").val(studio_id);
                    $("#studio_name").val(studio_name);
                    $("#studio_email").val(studio_email);
                    $("#multistore_email").val('<?php echo $_SESSION['login_attempt_email_id']; ?>');
                    window.open('', 'StudioWindow');
                    document.getElementById('studioLoginForm').submit();
                    $("#studio_login").hide();
                }
            }
<?php if (Yii::app()->common->hasViewPermission('viewonly', 'viewonly') || $is_child_store) { ?>
                window.onload = function (e) {
                    $("body").find("a.confirm").each(function () {
                        this.href = "#";
                        $(this).removeClass("confirm");
                        $(this).off('click');
                        $(this).addClass("light-grey");
                    });
                    $('input[type="submit"]').prop('disabled', true).addClass('bgm-disabled');
                    $('button[type="submit"]').prop('disabled', true).addClass('bgm-disabled');
                    $('body').find('a:contains(Remove)').prop('onclick', null).off('click').addClass("light-grey");
                    $('.upload-video').prop('onclick', null).off('click').addClass("light-grey");
                    $('.btn-danger').prop('onclick', null).off('click').addClass("light-grey");

                    //added by ajit
                    $('.btn-primry').prop('disabled', true).off('click').addClass("bgm-disabled");
                    $('.btn').prop('disabled', true).off('click').addClass("bgm-disabled");
                    $("body").find("a.preview").each(function () {
                        this.href = "#";
                        $(this).removeClass("preview");
                        $(this).off('click');
                        $(this).addClass("light-grey");
                    });
                    $("body").find("a.confirm").each(function () {
                        this.href = "#";
                        $(this).removeClass("confirm");
                        $(this).off('click');
                        $(this).addClass("light-grey");
                    });
    <?php if ($checkmultistore || $checkchieldstore) {
        
    } else { ?>
                        $('body').find('a:contains(Remove)').prop('onclick', null).off('click').addClass("light-grey");
                        $("a").prop("onclick", null).addClass("light-grey");
    <?php } ?>
                };
<?php } ?>
        </script>
    </body>
</html>
