<div class="modal fade is-Large-Modal" id="addvideo_popup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addvideo_popupLabel">Upload Video</h4>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#Upload_preview" aria-controls="Upload_preview" role="tab" data-toggle="tab">Upload Video</a>
                        </li>
                        <li role="presentation">
                            <a href="#Choose_from_library" aria-controls="Choose_from_library" role="tab" data-toggle="tab">Choose from Gallery</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="Upload_preview">
                            <div class="row is-Scrollable">
                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                    <input type="hidden" value="?" name="utf8">
                                    <input type="button" value="Upload File" class="btn btn-default-with-bg btn-sm"  onclick="click_browse('videofile');">
                                    <input type="file" name="file" style="display:none;" id="videofile" required onchange="checkfileSize();" >
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="Choose_from_library">
                            <div class="row m-b-20 m-t-20">
                                <div class="col-xs-6">
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                                        <div class="fg-line">
                                            <input type="text" id="search_video" placeholder="What are you searching for?" class="form-control fg-input input-sm" onkeyup="searchvideo('video');">

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="text-center row m-b-20 loaderDiv" style="display: none;">
                                <div class="preloader pls-blue text-center " >
                                    <svg class="pl-circular" viewBox="25 25 50 50">
                                    <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                    </svg>
                                </div>
                            </div>

                            <div class="row  Gallery-Row">
                                <div class="col-md-12 is-Scrollable-has-search p-t-40 p-b-40" id="video_content_div"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>