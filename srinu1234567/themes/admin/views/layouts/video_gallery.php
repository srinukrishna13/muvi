<div class="modal fade is-Large-Modal" id="addvideo_popup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="addvideo_popupLabel">Upload Video</h4>
			</div>
			<div class="modal-body">
				<div role="tabpanel">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#Upload_preview" aria-controls="Upload_preview" role="tab" data-toggle="tab">Upload Video</a>
						</li>
						<li role="presentation">
							<a href="#Choose_from_library" aria-controls="Choose_from_library" role="tab" data-toggle="tab">Choose from Gallery</a>
						</li>
                                                <li role="presentation">
							<a href="#Embed_from_3rd_party" aria-controls="Embed_from_3rd_party" role="tab" data-toggle="tab">Embed from 3rd party</a>
						</li> 
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="Upload_preview">
							<div class="row is-Scrollable">
								<div class="col-xs-12 m-t-40 m-b-20 text-center">
									<input type="hidden" value="?" name="utf8">
									<input type="button" value="Upload File" class="btn btn-default-with-bg btn-sm"  onclick="click_browse('videofile');">
									<input type="file" name="file" style="display:none;" id="videofile" required onchange="checkfileSize();" >
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="Choose_from_library">
							<div class="row m-b-20 m-t-20">
								<div class="col-xs-6">
									<div class="form-group input-group">
										<span class="input-group-addon"><i class="icon-magnifier"></i></span>
										<div class="fg-line">
											<input type="text" id="search_video" placeholder="What are you searching for?" class="form-control fg-input input-sm" onkeyup="searchvideo('video');">
											
										</div>

									</div>
								</div>
							</div>
							<div class="text-center row m-b-20 loaderDiv" style="display: none;">
								<div class="preloader pls-blue text-center " >
									<svg class="pl-circular" viewBox="25 25 50 50">
									<circle class="plc-path" cx="50" cy="50" r="20"></circle>
									</svg>
								</div>
							</div>

							<div class="row  Gallery-Row">
								<div class="col-md-12 is-Scrollable-has-search p-t-40 p-b-40" id="video_content_div">

								</div>

							</div>
						</div>
                                             <!------Embed from 3rd Party------->
                                             <div role="tabpanel" class="tab-pane" id="Embed_from_3rd_party">
                                                 <div role="tabpanel" class="tab-pane" id="home">
                                                     <div class="row is-Scrollable m-b-20 m-t-20">
                                                         <div class="col-sm-12 m-t-40 m-b-20">
                                                             <div class="form-horizontal">
                                                                 <div class="form-group">
                                                                    <label for="uploadVideo" class="control-label col-md-3">Video from 3rd Party Platform &nbsp;</label>
                                                                    <div class="col-md-4">
                                                                        <input type="hidden" name="movie_id" id="movie_id" value=""/>
                                                                        <input type="text" id="embed_url" class="form-control  input-sm" name="embedurl" required="" placeholder="Link from YouTube, Vimeo or another OVP"  onkeyup="embedFromThirdPartyPlatformForTrailer();">
                                                                    </div>
                                                                 </div>  
                                                             </div> 
                                                             <div class="form-group">
                                                                 <div class="col-sm-offset-3 col-sm-10">
                                                                     <input type="button" value="Save" id="save-btn" onclick="embedThirdPartyPlatformForTrailer();" class="btn btn-primary btn-sm" >
                                                                 </div> 
                                                             </div>
                                                             <span class="error red help-block" id="embed_url_error"></span>
                                                         </div>  
                                                     </div>
                                                 </div>
                                             </div>
                                             <!-------End of Embed from 3rd party------>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
    function embedThirdPartyPlatformForTrailer() {
           var str =$("#embed_url").val();
            var videoUrl1 = "youtube.com";
            var videoUrl2 = "vimeo.com"; 
            var videoUrl3 = 'm3u8';
            var videoUrl4 = 'iframe';
            var videoUrl5 = "dailymotion.com";
          if(str.length ==0){
            $('#save-btn').attr('disabled', 'disabled');
            return false;
           }else if(str.length){
               // console.log((str.indexOf(videoUrl4))+'.......'+(str.indexOf(videoUrl3))+'.....'+(str.indexOf(videoUrl2))+'.....'+(str.indexOf(videoUrl1)));
                    if ((str.indexOf(videoUrl3) != -1) || 
                       (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl1) != -1)  || 
                       (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl2) != -1) ||
                       (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl5) != -1))
                    {                         
                        showLoader();
                        var embed_text =$("#embed_url").val();
                        var action_url = '<?php echo $this->createUrl('admin/embedThirdPartyPlatformForTrailer'); ?>'
                        $('#embed_url_error').html('');
                        var movie_id = $('#movie_id').val();
                        var movie_stream_id = $('#movie_stream_id').val();
                        $.post(action_url, {'third_party_url': embed_text,'movie_id': movie_id, 'movie_stream_id': movie_stream_id}, function (res) {
                        showLoader(1);
                        window.location.href = window.location.href;
                        });
                    }else{
                        swal("Oops! Provide a valid m3u8 or iframe embed url");
                        return false;
                }
            }
       }

function embedFromThirdPartyPlatformForTrailer(){
        var embedurl = $("#embed_url").val();
        if(embedurl.length === 0){
        $('#save-btn').attr('disabled', 'disabled');  
        //return false;
        }else{
        $('#save-btn').removeAttr('disabled');
        return true;
        }
    }

</script>