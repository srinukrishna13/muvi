<div class="Block m-t-40">
    <div class="Block-Header m-t-20">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-settings left-icon "></em>
        </div>
        <h3 class="text-capitalize f-300">Content Format & Metadata</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            Define the types of content you would like to have on your store
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 m-t-20">
            <div class="col-sm-2 p-l-0">
                <a href="<?= Yii::app()->getBaseUrl(true) . '/category/AddContentForm' ?>">
                    <button type="submit" class="btn btn-primary"> ADD CONTENT FORMAT </button>
                </a>
            </div>
            <div class="col-sm-2 pull-right">
                <a href="javascript:void(0);" onclick="delete_selected()">
                    <button type="button" class="btn btn-danger"> DELETE SELECTED</button>
                </a>
            </div>
        </div>
    </div>
    <hr>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-horizontal">	
            <table class="table" id="list_tbl">
                <thead>
                    <tr>
                        <th style="width:100px;">
                <div class="checkbox m-b-15">
                    <label>
                        <input type="checkbox" class="sub_chkall" id="check_all"  >
                        <i class="input-helper"></i>
                    </label>
                </div>
                </th>
                <th>CONTENT FORMAT</th>
                <th>CONTENT TYPE</th>
                <th data-hide="phone" class="width">MULTIPART</th>
                <th data-hide="phone" class="width">ACTIONS</th>
                </tr>
                </thead>
                <?php
                if (!empty($data)) {
                    foreach ($data as $key => $val) {
                        ?>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" class="sub_chk" data-id="<?php echo $val['id']; ?>" name="delete_all[]" >
                                            <i class="input-helper"></i>
                                        </label>
                                    </div> 
                                </td>
                                <td>
                                    <?php echo $val['name']; ?>
                                </td>
                                <td>
                                    <?php echo $parent_content_name[$val['parent_content_type_id']]; ?>
                                </td>    
                                <td>
                                    <?php if ($val['content_type'] == 1) {
                                        echo "YES";
                                    } else {
                                        echo "NO";
                                    } ?>
                                </td>   
                                <td> 
                                    <h5><a href="<?php echo $this->createUrl('/category/AddContentForm/', array('id' => $val['id'])); ?>" ><em class="icon-pencil"></em>&nbsp;&nbsp; Edit</a></h5>
                                    <?php if($val['studio_id']!=0){
                                        if(in_array($val['id'], $content_ids)){$disable = 1;}else{$disable = 0;}
                                        ?>
                                    <h5>
                                        <?php if($disable){?>
                                        <a href="javascript:void(0)" class="confirm" style="cursor:not-allowed" title="Form can't be deleted as you currently have content in this form. Please delete those content and try again."><em class="icon-trash"></em>&nbsp;&nbsp; Delete</a>
                                        <?php }else{?>
                                        <a href="javascript:void(0)" class="confirm" data-msg ="Are you sure?" onclick="confirmDelete('<?php echo $val['id'];?>');"><em class="icon-trash"></em>&nbsp;&nbsp; Delete</a>
                                        <?php }?>
                                    </h5>
                                    <?php }?>
                                </td>
                            </tr>	
                        </tbody>
                        <?php
                    }
                } else {
                    ?>					
                    <tbody>
                        <tr>
                            <td colspan="4">No Content Type Found</td>
                        </tr>
                    </tbody>
            <?php } ?> 
            </table>
        </div>
    </div>
</div>