<div class="row m-b-40">
    <div class="col-xs-12">
        <a href="<?php echo $this->createUrl('mrss/newMrssFeed'); ?>">
            <button type="submit" class="btn btn-primary btn-default m-t-10">
                New MRSS Feed
            </button>
        </a>

    </div>
</div>
<div class="loading_div" style="display:none">
    <div class="preloader pls-blue">
        <svg viewBox="25 25 50 50" class="pl-circular">
        <circle r="20" cy="50" cx="50" class="plc-path"/>
        </svg>
    </div>
</div>
<div role="tabpanel">
    <!-- Nav tabs -->
    <ul role="tablist" class="nav nav-tabs">
        <li class="active" role="presentation">            
            <a href="#tab1default" role="tab" aria-controls="tab1default" href="#tab1default" aria-expanded="true" data-toggle="tab">Export</a>
        </li>
        <li role="presentation" class="">
            <a href="#tab2default" role="tab" aria-controls="tab2default" href="#tab2default" aria-expanded="true" data-toggle="tab">Import</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div id="tab1default" class="tab-pane active" role="tabpanel">
            <div class="m-t-20">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="block">
                            <div class="table-responsive">
                                <table class="table table-hover tbl_repeat " >
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>MRSS Feed Title</th>
                                            <th>MRSS Feed Url</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($mrssfeedExport) {
                                            $sr = 0;
                                            foreach ($mrssfeedExport as $mrssfeedExportKey => $mrssfeedExportVal) {
                                                $sr++;
                                                echo "<tr>";
                                                echo "<td>" . $sr . "</td>";
                                                echo "<td>" . $mrssfeedExportVal['title'] . "</td>";
                                                echo "<td>http://" . Yii::app()->user->siteUrl . "/mrss_" . $studioMrssCode . "_" . $mrssfeedExportVal['id'] . ".xml</td>";
                                                echo '<td>
                                        <h5><a href="' . $this->createUrl('mrss/newMrssFeed/', array('feed_id' => $mrssfeedExportVal['id'])) . '"  mrrs_id_edit=' . $mrssfeedExportVal['id'] . ' > <em class="icon-pencil" ></em>&nbsp;&nbsp;Edit</a></h5>
                                        <h5><a href="javascript:void(0);"  mrrs_id_export=' . $mrssfeedExportVal['id'] . '  onclick="confirmDelete(this);" > <em class="icon-trash" ></em>&nbsp;&nbsp;Remove</a></h5>
                                      </td>';
                                                echo "</tr>";
                                            }
                                        } else {
                                            echo '<tr><td colspan="4">No Data found</td></tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div id="tab2default" class="tab-pane" role="tabpanel">
            <div class="m-t-20">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="block">
                            
                                <form class="form-horizontal m-b-20" name="mng_mrss_feed" id="mng_mrss_feed" method="post">
                                    <div class="form-group">
                                    <div class="col-md-3">
                                        <div class="fg-line">
                                            <input name="mrss_feed_url" autocomplete="off" id="mrss_feed_url" class="form-control input-sm" type="text" required="required" placeholder="MRSS Feed Url">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="filter_dropdown form-control input-sm" name="content_type" id="content_type" onchange="showContentTitle()">
                                                    <option value="">Content Type</option>
                                                    <?php
                                                        foreach($contenttypeList as $contenttypeListKey => $contenttypeListVal){
                                                            echo '<option value="'.$contenttypeListVal['id'].','.$contenttypeListVal['content_types_id'].'">'.$contenttypeListVal['display_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>	
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3"  id="div_content_name" style="display:none">
                                    </div>
                                    <div class="col-md-2">
                                        <button id="sub-btn" class="btn btn-primary" name="submit"  onclick="return validate_mrss_feed();">Import</button>
                                    </div>
                                    </div>
                                </form>
                            

                            <table class="table" >
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Feed Url</th>
                                        <th>Feed Id</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($mrssFeedDetails) {
                                        $sr = 1;
                                        foreach ($mrssFeedDetails as $mrssFeedDetailsKey => $mrssFeedDetailsVal) {
                                            echo '<tr>';
                                            echo '<td>' . $sr . '</td>';
                                            echo '<td>' . $mrssFeedDetailsVal['feed_url'] . '</td>';
                                            echo '<td>' . $mrssFeedDetailsVal['feed_id'] . '</td>';
                                            echo '<td>';
                                            if ($mrssFeedDetailsVal['is_video_gallery'] == 0) {
                                                //echo '<button class="btn btn-primary" id="add_to_video' . $mrssFeedDetailsVal['id'] . '" onclick="addToVideo(' . $mrssFeedDetailsVal['id'] . ');">Add To Video</button>&nbsp;';
                                            }
                                            echo '<button class="btn btn-primary" onclick="showFeedData(' . $mrssFeedDetailsVal['id'] . ');">Details</button>
                                                                        <a href="javascript:void(0);"  mrrs_id=' . $mrssFeedDetailsVal['id'] . '  onclick="showConfirmPopup(this);" > <em class="icon-trash" ></em>&nbsp;&nbsp;Remove</a>
                                                                      </td>';
                                            echo '</tr>';
                                            $sr++;
                                        }
                                    } else {
                                        echo "<tr><td colspan='4'>No data found.</td></tr>";
                                    }
                                    ?>
                                </tbody>
                            </table>

                            <div class="cb" style="clear: both;"></div>
                            <div class="pull-right">
                                <?php
                                if ($mrssFeedDetails) {
                                    $opts = array('class' => 'pagination m-t-0 m-b-0');
                                    $this->widget('CLinkPager', array(
                                        'currentPage' => $pages->getCurrentPage(),
                                        'itemCount' => $item_count,
                                        'pageSize' => $page_size,
                                        'htmlOptions' => $opts,
                                        'maxButtonCount' => 6,
                                        'nextPageLabel' => 'Next &gt;',
                                        'header' => '',
                                    ));
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal Start -->
<div class="modal fade" id="feed_data" role="dialog" style="overflow-y:hidden !important;">
    <div class="modal-dialog modal-lg"">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Feed Data</h4>
            </div>
            <div class="modal-body">
                <div id="feed_data_view">
                    <div class="box-body table-responsive no-padding ">
                        <table class="table table-hover tbl_repeat" style="word-wrap:break-word" >
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="mrssmodal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Delete MRSS Feed?</h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" action="<?php echo $this->createUrl('mrss/remove'); ?>"  name="submodal" id="submodal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span>Are you sure you want to <b>Delete MRSS Feed?</b> </span> 
                            <input type="hidden" id="mrrs_id_delete" name="mrrs_id_delete" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="mrss_list" class="btn btn-default" onclick="removeBox();">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mrssmodalExport" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Delete MRSS Feed Export Data?</h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" action="<?php echo $this->createUrl('mrss/deleteExportData'); ?>"  name="submodalExport" id="submodalExport" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span>Are you sure you want to <b>Delete MRSS Feed Export Data?</b> </span> 
                            <input type="hidden" id="mrrs_export_id_delete" name="mrrs_export_id_delete" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="mrss_list" class="btn btn-default" onclick="removeBoxExport();">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addvideo_res" role="dialog" style="overflow-y:hidden !important;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><div id="video_res"></div></h4>
            </div>
        </div>
    </div>
</div>
<div class="modal fade is-Large-Modal" id="showMetadata" role="dialog" style="overflow-y:hidden !important;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="showMetadataTitle" style="font-size:22px;"></h4>
            </div>

            <div class="modal-body" id="showMetadataContent">
            </div>
        </div>
    </div>
</div>
<!--Modal End -->
<script type="text/javascript">
    function validate_mrss_feed() {
        $("#mng_mrss_feed").validate({
            rules: {
                "mrss_feed_url": {
                    required: true
                },
                "content_type":{
                    required: true
                }
            },
            messages: {
                "mrss_feed_url": {
                    required: "Please provide MRSS Feed Url!"
                },
                "content_type": {
                    required: "Please select Content Type!"
                }
            },
            errorPlacement: function(error, element) {
                    error.addClass('red');
                switch (element.attr("name")) {
                    case 'content_type':
                        error.insertAfter(element.parent().parent());
                        break;
                    default:
                        error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                $('.loading_div').show();
                $('#importMrss').css({'opacity': '0.3'});
                $('.btn').attr('disabled', 'disabled');
                var mrss_feed_url = $("#mrss_feed_url").val();
                var content_type = $("#content_type").val();
                var content_name = '';
                if(document.getElementById("content_name")!==null){
                    var content_name = $("#content_name").val();
                }
                var url = "<?php echo Yii::app()->baseUrl; ?>/mrss/showMrssMetadata";
                $.post(url,{'mrss_feed_url':mrss_feed_url,'content_type':content_type,'content_name':content_name},function(res){
                    if(res == 0){
                        $("#showMetadataContent").html('Please provide a valid MRSS Feed!');
                    } else{
                        $("#showMetadataContent").html(res);
                        $("#showMetadataTitle").html('Import Metadata');
                    }
                    $("#showMetadata").modal('show');
                    $('#importMrss').css({'opacity': '1'});
                    $('.btn').removeAttr('disabled');
                    $('.loading_div').hide();
                });
            }
        });
    }
    function showFeedData(id) {
        $("#feed_data_view").html("");
        $('.loading_div').show();
        $('#importMrss').css({'opacity': '0.3'});
        $('.btn').attr('disabled', 'disabled');
        var url = "<?php echo Yii::app()->baseUrl; ?>/mrss/showFeedData";
        $.post(url, {id: id}, function (res) {
            var trHTML = '';
            $.each(res, function (i, item) {
                if( typeof item === 'string' ) {
                    trHTML += '<tr><td>' + i + '</td><td> : </td><td>' + item + '</td><</tr>';
                } else{
                    $.each(item, function (j, item1) {
                        trHTML += '<tr><td>' + j + '</td><td> : </td><td>' + item1 + '</td><</tr>';
                    });
                }
            });
            $("#feed_data_view").html(trHTML);
            $('.loading_div').hide();
            $('#importMrss').css({'opacity': '1'});
            $('.btn').removeAttr('disabled');
            $("#feed_data").modal('show');
        }, 'json');
    }
    function addToVideo(id) {
        $("#video_res").html("");
        $('.loading_div').show();
        $('#importMrss').css({'opacity': '0.3'});
        $('.btn').attr('disabled', 'disabled');
        var url = "<?php echo Yii::app()->baseUrl; ?>/management/addVideoFromMrssFeed";
        $.post(url, {id: id}, function (res) {
            var messg;
            if (res.error) {
                messg = 'No video found in the selected feed.';
            } else {
                if (res.is_video) {
                    $("#add_to_video" + id).hide();
                    messg = 'Video added to gallery successfully';
                } else {
                    messg = 'No video found in the selected feed.';
                }
            }
            $("#video_res").html(messg);
            $('.loading_div').hide();
            $('#importMrss').css({'opacity': '1'});
            $('.btn').removeAttr('disabled');
            $("#addvideo_res").modal('show');
        }, 'json');
    }
    function showConfirmPopup(obj) {
        //$("#mrssmodal").modal('show');
        $("#mrrs_id_delete").attr('value', $(obj).attr('mrrs_id'));
        swal({
            title: "Delete MRSS Feed?",
            text: "Are you sure you want to <b>Delete MRSS Feed?</b>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            removeBox();
        });
        
    }
    function confirmDelete(obj) {
        //$("#mrssmodalExport").modal('show');
        $("#mrrs_export_id_delete").attr('value', $(obj).attr('mrrs_id_export'));
        swal({
            title: "Delete MRSS Feed Export Data?",
            text: "Are you sure you want to <b>Delete MRSS Feed Export Data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            removeBoxExport();
        });
    }
    function removeBox() {
        document.submodal.submit();
    }
    function removeBoxExport() {
        document.submodalExport.submit();
    }
    function showContentTitle(){
        if($("#content_type").find('option:selected').val() != ''){
            var myNewarray = $("#content_type").find('option:selected').val().split(',');
            if(myNewarray[1] == 3){
                $('.loading_div').show();
                $('#importMrss').css({'opacity': '0.3'});
                $('.btn').attr('disabled', 'disabled');
                var url = "<?php echo Yii::app()->baseUrl; ?>/mrss/showFilterContentTitle";
                $.post(url,{'contentTypeId':myNewarray[0]},function(res){
                    $("#div_content_name").show();
                    $('#div_content_name').html(res);
                    $('#importMrss').css({'opacity': '1'});
                    $('.btn').removeAttr('disabled');
                    $('.loading_div').hide();
                });
            } else{
                $('#div_content_name').html();
                $("#div_content_name").hide();
            }
        } 
    }
</script>