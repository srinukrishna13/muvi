<?php $v = RELEASE;
$randomVar =rand();
?>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>if (!window.jQuery) { document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>'); }</script>
                
        <script type='text/javascript'>
            var muviWaterMark = "";
            <?php 
            if($waterMarkOnPlayer != ''){ ?>
                var muviWaterMark = "<?php echo $waterMarkOnPlayer; ?>";
            <?php } ?>
        </script>
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />          
        
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/video.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/vjs.youtube.js"></script>
        <!--<script src="<?php echo Yii::app()->baseUrl; ?>/js/videojs.hls.min.js"></script>-->
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/small_style.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs.progressTips.js"></script> 
        
 <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs.progressTips.css?v=<?php echo $RELEASE?>" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
  
<link href="<?php echo Yii::app()->baseUrl;?>/common/css/videojs.watermark.css" rel="stylesheet" type="text/css" />
        
        
	<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.css?rand=<?php echo $randomVar;?>" rel="stylesheet" type="text/css" />
	<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.js?rand=<?php echo $randomVar;?>"></script>        
        
        
        <script data-cfasync="false" type="text/javascript">
            videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
        </script>
        <style type="text/css">
        .video-js {height: 50%; padding-top: 47%;}
        .vjs-fullscreen {padding-top: 0px}  
        .RDVideoHelper{display: none;}
        video::-webkit-media-controls {
            display:none !important;
        }
        video::-webkit-media-controls-panel {
            display: none !important;
        }
    #pause_touch{
        margin: auto;
        left:0;
        right: 0;
        bottom: 0;
        top: 0;
        position: absolute;
        height: 64px;
        width: 64px;
        display: none;
    }
    #play_touch{
        margin: auto;
        left:0;
        right: 0;
        bottom: 0;
        top: 0;
        position: absolute;
        height: 64px;
        width: 64px;
        display: none; 
    }
        </style>
<center>
    <div class="wrapper" style="overflow:hidden;">
            <div class="videocontent">
                <video id="video_block" class="video-js moo-css vjs-default-skin" controls preload="auto" width="auto" height="auto" data-setup='{}' crossorigin="anonymous">
                    <?php
                        foreach($subtitleFiles as $subtitleFilesKey => $subtitleFilesval){
                            if($subtitleFilesKey == 1){
                                echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" default="true" ></track>';
                            } else{
                                echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" ></track>';
                            }
                        }
                    ?>
                </video>
            </div> 
        </div>
</center>
<div id="trailer_div" style="display:none;" class="trailer_div">
	<video id = "trailer_block" class="video-js" controls
		preload="auto"  width="650" height="375" poster="/images/black.png" data-setup="{}" style="margin-top:-5px;">
	</video>
    <div style="left:98%;top:0;position:absolute;cursor:pointer;" onclick="backto_info();"><img alt="Popup_close" src="<?php echo Yii::app()->baseUrl;?>/images/video/video_close.png"></div>
</div>
<script>
  var myPlayer,full_screen=false,request_sent = false,first_time = true, plus_content="", btn_html = "";
    var is_mobile = "<?php echo Yii::app()->common->isMobile(); ?>";
    var videoOnPause = 0;
    var player = '';
    $(document).ready(function () {
        var playerSetup = videojs('video_block');
        playerSetup.ready(function () {
            player = this;
            player.progressTips(); // Displying duration in the player during playback.
            $('#video_block').append('<img src="/images/touchpause.png" id="pause_touch" />');
            $('#video_block').append('<img src="/images/touchplay.png" id="play_touch" />');
            $('#video_block').append('<img src="/images/pause.png" id="pause"/>');
            $('#video_block').append('<img src="/images/play-button.png" id="play" />');
            if (is_mobile == 0) {
                $('#video_block_html5_api').on('click', function (e) {
                    e.preventDefault();
                    if (player.paused()) {
                        $('#pause').hide();
                        $('#play').show();
                    } else {
                        $('#play').hide();
                        $('#pause').show();
                    }
                });
            } else {
                player.on("play", function () {
                    videoOnPause = 0;
                    $('#play_touch').hide();
                    $('#pause_touch').hide();
                });
                player.on("pause", function () {
                    videoOnPause = 1;
                    $('#play_touch').show();
                    $('#pause_touch').hide();
                });
                $('#pause_touch').bind('touchstart', function (e) {
                    player.pause();
                });
                $('#play_touch').bind('touchstart', function (e) {
                    player.play();
                });
                $('#video_block_html5_api').bind('touchstart', function (e) {
                    if (player.play()) {
                        $('#pause_touch').show();
                        $('#play_touch').hide();
                        setTimeout(function () {
                            $('#pause_touch').hide();
                        }, 3000);
                    }
                });
            }
        });

      if (is_mobile != 0) {
                        $(".vjs-loading-spinner").remove();
                        $(".vjs-watermark").remove();
                    }
    $("#trailer_block .vjs-control-bar").remove();
	$(".vjs-tech").mousemove(function() {
      if(full_screen == true){
        $("#video_block .vjs-control-bar").show();
        $("#bottom_div").show();
        $(".vjs-watermark").show();
        setTimeout(function(){
          //full_screen = true;
          $("#video_block .vjs-control-bar").hide();
          $("#bottom_div").hide();
          $(".vjs-watermark").hide();
        },10000);
      }else{
        $("#video_block .vjs-control-bar").show();
        $("#video_block").unbind('mousemove');
      }
    });
    if($("div.vjs-subtitles-button").length)
    {
        var divtagg = $(".vjs-subtitles-button .vjs-control-content .vjs-menu .vjs-menu-content li:first-child");
        var divtaggVal = divtagg.html();
        if(divtaggVal === 'subtitles off'){
            divtagg.html("Subtitles Off");
        }
    }

	var movie_id = "<?php echo $_REQUEST['movie']?>";
    //var video_link = "<%= @video_link %>";
	var is_preview = "<?php echo isset($_REQUEST['preview'])? $_REQUEST['preview']:'';?>";
	if(is_preview == 1){
	 var main_video = videojs("video_block");
	 main_video.src("<?php echo $default_trailer?>");
	 main_video.play();
        <?php if($v_logo != ''){ ?>
            main_video.watermark({
                   file: "<?php echo $v_logo; ?>",
                   xrepeat: 0
           });
           $(".vjs-watermark").attr("style","top:80%;right:1%;width:7%;");
           $(".vjs-watermark").click(function(){
                   window.history.back();
           });
        <?php } ?>
	}else{
	 show_more_info(movie_id);
	}
  });
  function show_more_info(id){
	  
	  <?php if($_SERVER['HTTP_HOST']=='localhost'){?>
    var url = "https://180.87.253.79:3050/trailer_info";
	  <?php }else{?>
		var url = "https://www.muvi.com/trailer_info";
	  <?php }?>
	var upload_path = "<?php echo $default_trailer?>";	  
    if($("#vid_more_info").length == 0){
      var more_info = '<div id="vid_more_info" class="more_info"><div style="left:98%;top:0;position:absolute;cursor:pointer;display:none;" id="info_close" onclick="close_info();"><img alt="Popup_close" src="<?php echo Yii::app()->baseUrl;?>/images/video/video_close.png"></div><div style="float:left;"><img src="" id="img_block" height="30%"></div><div  class="all_info"><div id="name_block" style="font-weight:bold;"></div><div style="clear:both;height:10px;"></div><div><span id="cast1_name"  style="color:#fff;"></span><span id="cast2_name" style="padding-left:20px;color:#fff;"></span></div><div style="clear:both;height:10px;"></div><div><a href="javascript:void(0);" id="play_trailer" style="color:#fff;display:none;text-decoration:none;"><div style="float:left"><img src="<?php echo Yii::app()->baseUrl;?>/images/video/round_trailer.png" height="30px" width="30px"></div><div style="float:left;padding-left:10px;padding-top:5px;font-size:16px;">Trailer</div></a></div><div style="clear:both;height:10px;"></div><div id="story_block"></div><div style="clear:both;height:15px;"></div> <div style="float:left;" id="play_btn"></div><div style="clear:both;height:20px;"></div></div><div style="clear:both"></div></div>';

      $("#video_block").append(more_info);
    }
    $.post(url,{movie_id:id},function(res){
      $("#img_block").attr("src",res.poster_path);
      $("#img_block").css("height",get_infoimg_height(98));
      $("#img_block").css("width",get_infoimg_width(26));
      $("#name_block").html("<a href='/movies/"+res.movie_permalink+"' style='color:#fff;text-decoration:none;'>"+res.name+"</a>");
      $("#story_block").html(res.story);
      $("#why_watch_block").html(res.why_watch);
      $("#cast1_name").html("<a href='/stars/"+res.cast1_permalink+"' style='color:#fff;text-decoration:none;'>"+res.cast1_name+"</a>");
      $("#cast2_name").html("<a href='/stars/"+res.cast2_permalink+"' style='color:#fff;text-decoration:none;'>"+res.cast2_name+"</a>");
      $("#play_icon").removeAttr("onclick");
      //$(".payment_btn").removeAttr("onclick");
      $("#play_trailer").hide();
	  var can_see = "<?php echo $can_see ?>";
	  var is_free = "<?php echo $is_free ?>";
      $("#play_trailer").removeAttr("onclick");
      if(res.uploaded_path != ""){		
	    if(res.pay_per_view != ""){
			$("#play_btn").html('<div style="float:left;"><a href="javascript:void(0)"  style="font-weight:bold;padding:7px 25px;" class="btn btn-primary free_play">Play '+res.pay_per_view+' RS</a></div>');
	    }else{
			$("#play_btn").html('<div style="float:left;"><a href="javascript:void(0)"  style="font-weight:bold;padding:7px 25px;" class="btn btn-primary free_play">Play</a></div>');
		}
		if(is_free == "1"){
			$("#play_btn").attr("onclick","play_video('"+upload_path+"','"+id+"')");
		}else{
			if(can_see == "1"){
				if(res.pay_per_view == ""){
					$("#play_btn").attr("onclick","play_video('"+upload_path+"','"+id+"')");
				}
			}else{
				$("#play_btn").attr("onclick","show_warning();");
			}
		}

      }else{
        $("#play_btn").html("");
      }
      
      if(res.trailer_path != ""){
        $("#play_trailer").show();
        $("#play_trailer").attr("onclick","play_vid_trailer('"+res.trailer_path+"','"+res.trailer_poster+"')");
      }
      $("#trailer_div").hide();
      $("#vid_more_info").show();
      //$(".random_msg").show();
    });

    $("#movie_block_"+id).parent().children().removeClass("active_block");
    $("#movie_block_"+id).addClass("active_block");
    $("#vid_more_info").mouseover(function() {
      $("#info_close").show();
    }).mouseout(function() {
      $("#info_close").hide();
    });
  }
  
  function show_warning(){
	alert('Seems like you have already watched 3 movies this month. Please come back next month to watch again.');
  }
  function play_video(video_link,movie_id){
	var url = "<?php echo Yii::app()->baseUrl;?>/video/add_log";
	var is_free = "<?php echo $is_free ?>";
	var episode_id = "<?php echo $_REQUEST['episode']?>";
	if(is_free != "1"){
		$.post(url,{movie_id:movie_id,episode_id:episode_id},function(res){});
	}
	//video_link='https://www.w3schools.com/html/mov_bbb.mp4';
    $("#vid_more_info").hide();
    var main_video = videojs("video_block");
	main_video.src(video_link);
   //main_video.src({type: "video/mp4", src: video_link });
    main_video.play();
  }
  
  function play_vid_trailer(video_link,poster){
    $("#trailer_div").show();
    $("#vid_more_info").hide();
    var trailer = videojs("trailer_block");
    trailer.src(video_link);
    $("#trailer_block").attr('poster',poster);
    $("#trailer_block").css("margin-top","-5px");
    $("#trailer_block .vjs-control-bar").hide();
    trailer.play();
    trailer.on("ended", function(){
      $("#trailer_div").hide();
      $("#vid_more_info").show();
      myPlayer.play();
    });
    myPlayer = videojs("video_block");
    myPlayer.pause();
  }

   function get_infoimg_height(percent){
    var info_height = $("#vid_more_info").css("height");
    var img_px = parseInt(info_height)*(percent/100);
    return img_px+"px";
  }

  function get_infoimg_width(percent){
    var info_width = $("#vid_more_info").css("width");
    var img_px = parseInt(info_width)*(percent/100);
    return img_px+"px";
  }
  function backto_info(){
    var trailer = videojs("trailer_block");
    trailer.pause();
    $("#trailer_div").hide();
    $("#vid_more_info").show();
  }

  function close_info(){
    $("#vid_more_info").hide();
    $("#wannasee_block").hide();
    //$(".random_msg").hide();
  }

</script>