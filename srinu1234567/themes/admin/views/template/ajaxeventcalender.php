<div class="col-sm-12">
 <input type="hidden" id="date_picker_date" value="" />
     <div class="col-md-12">
                                 <div class="text-center m-b-20  loader" style="display: none;"></div>
                                </div>
 <div id="calendar"></div>
</div>
<script>
    /*** code for copy paste of event ***/
    
   
    $(function () {

        /* initialize the external events
         -----------------------------------------------------------------*/
       $('#external-events .fc-event').each(function () {
           //var test = $(this).children().find(".event_title").val();
           //alert(test);
           //alert($.trim($(".event_title").val())); 
           var externalEvents = {
            title: $.trim($(this).children().find(".event_title").val()),
            id: $.trim($(this).children().find(".event_id").val()),
            content_types_id:$.trim($(this).children().find(".content_types_id").val())
        };
         // creating event object and makes event text as its title
        $(this).data('externalEvents', externalEvents); //saving events into DOM
        // make the event draggable using jQuery UI
//        $(this).draggable({
//            zIndex: 999,
//            revert: true, // will cause the event to go back to its
//            revertDuration: 0 //  original position after the drag
//        });
    });

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
    
        $('#calendar').fullCalendar({
            header: {
                left: 'prev',
                center: '',
                right: 'agendaDay,agendaWeek,next'
            },
            defaultView: 'agendaWeek',
            allDaySlot: false,
            slotDuration: "00:30:01",
            axisFormat: 'HH:mm', //,'h(:mm)tt',


            buttonText: {
                today: 'Today',
                month: 'Month',
                week: 'Week',
                day: 'Day'
            },
            eventLimit: 3,
            //Random default events

            events: [
<?php foreach ($all_event as $key => $val) { ?>
                    {
                        id:"<?php echo $val['id']; ?>",
                        title: "<?php echo addslashes($val['event_name']); ?>",
                        start: "<?php echo $val['start_time']; ?>",
                        end: "<?php echo $val['end_time']; ?>",
                        backgroundColor: "#2cb7f6", //Primary (light-blue)
                        borderColor: "#17a2ac", //Primary (light-blue)
                        
                        textColor: 'white',
                        color: 'black'

                    },
<?php } ?>

            ],
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            eventDataTransform: function(event){  //stop dragging of events inside the full calendar
              
            event.editable = false;   
            return event;    
            },
            eventRender: function (event, element) {
                element.append('<div id="tool'+event.id+'" class="tooltipevent" style="padding:5px;min-height:35px;background:#fff;border:1px solid #d7d7d7;position:relative;z-index:10001;">' +'<div id="name_'+event.id+'">'+event.title+'</div><span class="caret"></span></div>');
                element.attr('href', 'javascript:void(0);');
                element.click(function () {
                    $("#rweekday").hide();
                     $("#rweekend").hide();
                     $("#rhours").hide();
                    // console.log(event);
                    //console.log(event.title);
                    //console.log(moment(event.start).format('MM-DD-YYYY HH:mm:ss'));
                    if (event.title != '' && moment(event.start).format('MM-DD-YYYY HH:mm:ss') != '')
                    {

                        $("#myModalLabel").html("Edit Content");
                        $("#submit_content").html("Save");
                      //  $("#delete_content").css("display","inline-block");
                      $("#delete_content").show();
                    }
                    $.ajax({
                        method: "POST",
                        url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/editEvent',
                        data: {'event_title': event.title, 'event_start': moment(event.start).format('MM-DD-YYYY HH:mm:ss'), 'event_end': moment(event.end).format('MM-DD-YYYY HH:mm:ss')},
                        dataType: "json",
                        success: function (res) {

                            document.getElementById("ls_schedule_id").value = res.event_id;
                            document.getElementById("content_types_id").value = res.stream_type;
                            document.getElementById("movie_id").value = res.movie_id;
                        }
                    });
                    $("#startDate").val(moment(event.start).format('MM-DD-YYYY'));
                    $("#startTime").val(moment(event.start).format('HH:mm:ss'));
                    $("#endTime").val(moment(event.end).format('MM-DD-YYYY HH:mm:ss'));
                    $("#search_text").val(event.title);

                    //$("#eventContent").dialog({ modal: true, title: event.title, width:500});
                    $("#repeat_weekday").val("");
                    $("#repeat_weekend").val("");
                    $('#eventContent').modal('show');
                    //$('#myModalLabel').html(event.title);
                    $('#search_text').autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/eventAutocomplete/movie_name?term=' + $('#search_text').val(),
                                dataType: "json",
                                init: function () {
                                },
                                success: function (data) {
                                    if (data.length == 0) {
                                        // $('#movie_id').val('');
                                        //  $('#preview_poster').html('');
                                    } else {
                                        response($.map(data.movies, function (item) {
                                            return {
                                                label: item.movie_name,
                                                value: item.movie_id,
                                                ctype: item.stream_type,
                                                duration: item.duration
                                            }
                                        }));
                                    }
                                }
                            });
                        },
                        select: function (event, ui) {
                            event.preventDefault();
                            $("#search_text").val(ui.item.label);

                        },
                        focus: function (event, ui) {
                            var lb = ui.item.label;
                            $("#search_text").val(ui.item.label);
                            $("#movie_id").val(ui.item.value);
                            $("#content_types_id").val(ui.item.ctype);
                            if (lb.indexOf("Episode") > -1) {
                                $("#is_episode").val(1);
                            } else {
                                $("#is_episode").val(0);
                            }
                            event.preventDefault(); // Prevent the default focus behavior.
                        }
                    });
                });
            },
            dayClick: function (date, allDay, jsEvent, view) {
                $("#delete_content").hide();
                var date_time=(moment(date).format('MM-DD-YYYY HH:mm:ss'));
                //alert(date_time);
                var date_split=date_time.split(" ")
                var start_day = date_split[0];
                var start_time1 = date_split[1].split(":");
                var start_time = start_time1[0]+":"+start_time1[1]+":00";
                
                //alert('Clicked on: ' + date.format("hh:mm:ss"));
                document.getElementById("add_event").reset();
                $("#startDate").val(start_day);
                $("#startTime").val(start_time);
                //$("#eventContent").dialog({ modal: true, title: , width:500});
                document.getElementById("ls_schedule_id").value = "";
                document.getElementById("content_types_id").value = "";
                document.getElementById("movie_id").value = "";
                $("#rweekday").show();
                $("#rweekend").show();
                $("#rhours").show();
                $("#repeat_weekday").val("");
                $("#repeat_weekend").val("");
                $('#eventContent').modal('show');
                $('#myModalLabel').html("Add Content");
                $("#submit_content").html("Add");
                $('#search_text').autocomplete({
                    source: function (request, response) {

                        $.ajax({
                            url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/eventAutocomplete/movie_name?term=' + $('#search_text').val(),
                            dataType: "json",
                            init: function () {
                            },
                            success: function (data) {
                                if (data.length == 0) {
                                    // $('#movie_id').val('');
                                    //  $('#preview_poster').html('');
                                } else {
                                    response($.map(data.movies, function (item) {

                                        return {
                                            label: item.movie_name,
                                            value: item.movie_id,
                                            ctype: item.stream_type,
                                            duration: item.duration
                                        }


                                    }));
                                }
                            }
                        });
                    },
                    select: function (event, ui) {
                        event.preventDefault();
                        $("#search_text").val(ui.item.label);
                    },
                    focus: function (event, ui) {
                        var lb = ui.item.label;
                        //$("#search_text").val(ui.item.label);
                        $("#movie_id").val(ui.item.value);
                        $("#content_types_id").val(ui.item.ctype);
                        if (lb.indexOf("Episode") > -1) {
                            $("#is_episode").val(1);
                        } else {
                            $("#is_episode").val(0);
                        }
                        event.preventDefault(); // Prevent the default focus behavior.
                    }
                });

            },
//            drop: function (date, allDay, externalEvents) { // this function is called when something is dropped
//                showLoader1();
//                //update database for events.
//                var id=$(this).data('externalEvents').id;
//                var title=$(this).data('externalEvents').title;
//                var start_date=moment(date).format('MM-DD-YYYY HH:mm:ss');
//                var content_type_id=$(this).data('externalEvents').content_types_id;
//                var selected_channel = $("#channel_list").val();
//              
//                if (selected_channel != 0) {
//                   
//                    $.ajax({
//                        url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/updateDroppedImage',       
//                        cache: false,
//                        data: {'id': id, 'title': title, 'start_date': start_date, 'content_type': content_type_id,'channel_id': selected_channel},
//                        
//                        success: function (res) {
//                           showLoader1(1);
//                            if(res == "error"){
//                                swal('Error in adding Event');
//                            }else{
//                               
//                                $('#calender_container').html();
//                                 swal('Event added successfully');
//                                $('#calender_container').html(res);
//                            }
//                        }
//                    });
//
//                   
//                } 
//                else
//                {
//                    swal("Please select a channel");
//                }
//                
//              $('#calendar').fullCalendar();
//            },
//            eventDragStart: function (event, jsEvent, ui, view) {
//              
//                if (!copyKey)
//                    return;
//                var eClone = {
//                    title: event.title,
//                    start: event.start,
//                    end: event.end
//                };
//                $('#calendar').fullCalendar('renderEvent', eClone);
//
//            }

        });




        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create events
            var event = $("<div />");
            event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
            event.html(val);
            $('#external-events').prepend(event);

            //Add draggable funtionality
            ini_events(event);

            //Remove event from text input
            $("#new-event").val("");
        });
    });

    function delete_event()
        { 
     var ls_schedule=document.getElementById("ls_schedule_id").value;
     console.log(ls_schedule);
   swal({
            title: "Delete Event?",
            text: "Are you sure to delete event",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/deleteEvent",
                cache: false,
                type: 'POST',
                data: {'event_id':ls_schedule},
                success: function (res) {
                    if (res) {

                        $('#calendar').fullCalendar('removeEvents', ls_schedule);
                           swal({
                            title: "Event deleted successfully",
                            confirmButtonColor:"#2cb7f6"
                           }); 
                    }
                    else
                    {
                        swal("Event already started");
                    }
                   
                }
            });
        });
}

function showLoader1(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loader').show();
            
        } else {
            $('.loader').hide();
            
        }
    }
    
    
function saveevent()
{
    document.getElementById('submit_content').disabled = 1;
     var end_time=$("#endTime").val();
        if(end_time!=''){
    var ls_schedule=$("#ls_schedule_id").val();
    var formData = new FormData($("#add_event")[0]);
    
    $.ajax({
    url:"<?php echo Yii::app()->getBaseUrl(true); ?>/template/saveEvent",
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (result) {
             document.getElementById('submit_content').disabled = 0;
                if ($.trim(result) === "Timingerror") {
                swal('Selected date should be greater than current date');
                }
                else if($.trim(result) === "Nochannel")
                {
                  swal('Please select a channel');  
                }
                else if($.trim(result) === "Eventexists")
                {
                 swal('Event already exists');      
                }
                else if($.trim(result) === "NoDetail")
                {
                  swal('Please enter the event details');     
                }
                else if($.trim(result) === "Eventstarted")
                {
                     swal('Event started already');    
                }
                else if($.trim(result) === "DurationError")
                 {
                   swal("Please upload the video again");    
                 }
                else {
                   
                    $("#calender_container").html();
                    $("#cancel_content").trigger("click");
                     if(ls_schedule=='')
                    {
                       swal({
                        title: "Event added successfully",
                        confirmButtonColor:"#2cb7f6"
                       });
                       $("#calender_container").html(result);
                    }
                    else
                    {
                      swal({
                        title: "Event updated successfully",
                        confirmButtonColor:"#2cb7f6"
                       });
                      $("#calender_container").html(result);
                    }
                    
                    
                }
            }
        });
        }else
        {
        swal("Please upload the video again");
        }
}
</script>
