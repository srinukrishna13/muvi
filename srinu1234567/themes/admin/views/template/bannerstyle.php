<style>
    #banner_sortable li{
        display: inline;
    }
    .img-thumbnails {
        height: 100%;
        margin-bottom: 3px;
        max-width: 100%;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        display: inline-block;
        line-height: 1.42857;
        padding: 4px;
        transition: all 0.2s ease-in-out 0s;
    }
    .upload-Image {
        height: 40px;
        width: 44px;
        background-color: #fff;
        display: inline-flex;
        background-color: #fff;
    }      
    .upload-Image em{
        font-size: 2.5em;
        top: 50%;
        position: absolute;
        margin-top: -17px;
        left: 47%;
        margin-left: -17px;
        color:#2cb7f6;
    }
    .upload-Image:hover{ 
        background-color: #edf1f2;
    }
    .upload-Image:hover em{
        color: #0aa1e5;
    }
    .btn.upload-Image.relative {
        margin-top: -31px;
        float: right;
    }    

    ul.sortable {float: left; margin: 20px 0; list-style: none; position: relative !important;}
    ul.sortable li {float: left;  cursor: move;}
    ul.sortable li.ui-sortable-helper {border-color: #3498db;}


    ul.sortable li.placeholder {height: 100px; 
    }
    .ui_placeholder{
        background: #eee; border: 2px dashed #bbb; display: block; opacity: 0.6;
        border-radius: 2px;
        -moz-border-radius: 2px;
        -webkit-border-radius: 2px;
    }
    .ui-sortable-handle {
        height: 100px ;
    }
    .slideshow .next {
		height: 20px;
		position: absolute;
		right: -9px;
		top: 45%;
		width: 20px;
		z-index: 10;
    }
    .slideshow .prev {
		height: 20px;
		position: absolute;
		left:0;
		top: 45%;
		width: 20px;
		z-index: 10;
    }
    .slower .carousel{
        width:100% !important;
    }
    
    .thumbnail-image{
        background-color: #ffffff;
        border: 1px solid #dddddd;
        border-radius: 4px;
        height: auto;
        line-height: 1.42857;
        max-width: 100%;
        padding: 4px;
        transition: all 0.2s ease-in-out 0s;
    }
    .current-thumb{
        border: 1px solid #2cb7f6 !important;
    }
    .image-thumb{
        height:345px;
    }
    .play-btn-big {
        background: rgba(0, 0, 0, 0) url("../images/play-button.png") repeat scroll 0 0;
        bottom: 0;
        height: 64px;
        left: 0;
        margin: auto;
        position: absolute;
        right: 0;
        top: 0;
        width: 64px;
    }
    .preloader{
        border: 0 none;
        left: 50%;
        position: absolute;
        top: 0;
        z-index: 9999;
    }
    .flexDelete{
        width:auto !important;
    }
    .BannerCarouselSec .carousel{
        margin-bottom: 0 !important;
    }
	
</style>

<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/bannerStyle/style_2/css/bannerslide.css" type="text/css" >
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/caraousellite.css" type="text/css" >
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/bannerStyle/style_2/css/flexslider.css" type="text/css" >

<?php
	$data = Yii::app()->db->createCommand()
		->select("banner_preview_image")
		->from('banner_style')
		->where('banner_style_code=:banner_style_code', array(':banner_style_code' => $bannerstyle['banner_style_code']))
		->queryRow();
        $preview_image = $data['banner_preview_image'];
		$thumbVisible = $bannerstyle['no_of_active_image'];
	if ($existing_style != '') {
		$width = $existing_style['h_banner_dimension'];
		$height = $existing_style['v_banner_dimension'];
	} else {
		$width = $bannerstyle['h_banner_dimension'];
		$height = $bannerstyle['v_banner_dimension'];
	}
	?>
    <form id="bannerSetting" method="post" name="bannerSetting" action="<?php echo Yii::app()->getBaseUrl(true) ?>/template/saveBannerSetting">
        <input type="hidden" value="<?php echo $bannerstyle['banner_style_code']; ?>" name="banner_code" id="banner_code">
        <input type="hidden" value="<?php echo $status; ?>">
       <input type="hidden" value="<?php echo $thumbVisible; ?>" id="visible_thumb">

        <div class="col-xs-12 BannerCarouselSec">

            <?php
            $studio_id = Yii::app()->common->getStudioId();
            $style_code = $bannerstyle['banner_style_code'];
            $total_sections = count($sections);
            $bannUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);

            if ($total_sections > 0) {
                ?>    

                <input type="hidden" name="banner_id" id="banner_id" />  
                <input type="hidden" name="section_id"  id="section_id" />
                <input type="hidden" name="update_text" value="0" id="update_text" />                
                <?php
                $p = 0;
                foreach ($sections as $section) {
                    $p++;
                    $section_id = $section->id;

                    $resp = Yii::app()->common->getSectionBannerDimension($section_id);
                    $max_width = $resp['width'];
                    $max_height = $resp['height'];
                    $placeholder = 'Add text that goes on top of banner here';
                    $btn_txt = 'JOIN NOW';
                    $url_text = '';
                    $thumb=Yii::app()->common->getThumbBannerDimension($section_id);
                    $thumb_width = $thumb['width'];
                    $thumb_height = $thumb['height'];
                    $ban = new StudioBanner;
                    $data = $ban->findAllByAttributes(array('banner_section' => $section_id, 'studio_id' => $studio_id), array('order' => 'id_seq ASC'));
                    $total_bnrs = count($data);
                    if ($total_bnrs > 0) {
                        if ($style_code == 'style_1' || $style_code == 'style_3') {
                            ?>   

                            <div id="myCarousel_<?php echo $section->id ?>" class="carousel slide relative carousel-fade" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner">
                                    <?php
                                    $seq = array();
                                    if ($total_bnrs > 0) {
                                        $j = 0;
                                        foreach ($data as $banner) {
                                            $banner_src = $banner->image_name;
                                            $banner_title = $banner->title;
                                            $banner_id = $banner->id;
                                            $banner_full_url = $bannUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
                                            $banner_video_image = $banner->video_placeholder_img;
                                            $banner_video = $banner->video_remote_url;
                                            $bnr_txt = ($banner->banner_text != '') ? $banner->banner_text : 'ADD TEXT THAT GOES ON TOP OF BANNER HERE';
                                            $join_btn_txt = 'JOIN NOW';
                                            $url_text = $banner->url_text;
                                            $txt_bnr = $banner->banner_text;
                                            $banner_type = $banner->banner_type;


                                            $seq[] = $banner_id;
                                            ?>
                                            <div class="item <?php echo ($j == 0) ? 'active' : '' ?>">
                                                <img class="<?php echo $j ?>-slide img-responsive" src="<?php
                                                if ($banner_type == '0') {
                                                    echo $banner_video_image;
                                                } else {
                                                    echo $banner_full_url;
                                                }
                                                ?>" alt="<?php echo $banner_title; ?>" data-id="<?php echo $banner_id; ?>" />
                                                <?php if ($banner_type == '0') { ?><a href="javascript:void(0);" class="play_video"  data-name="banner_video" id="<?php echo $banner_id; ?>" data-height ="<?php echo $max_height; ?>" data-width="<?php echo $max_width; ?>"><div class='play-btn-big'></div></a>
                                                <?php } ?>

                                                <?php
                                                if (@IS_LANGUAGE == 1) {
                                                    if ($this->language_id == $banner_text['language_id'] && $banner_text['parent_id'] == 0) {
                                                        ?>
                                                        <div class="overlay-bottom overlay-white">
                                                            <div class="overlay-Text">
                                                                <div>
                                                                    <?php if ($max_allowed < 1 || $total_bnrs < $max_allowed) {
                                                                        ?>
                                                                        <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn btn-success icon-with-fixed-width avatar-view" id="<?php echo $section->id ?>">
                                                                            <em class="icon-plus" title="Add Banner"></em>
                                                                        </a>
                                                                    <?php } ?>
                                                                    <?php
                                                                    if ($total_bnrs > 1) {
                                                                        if ($j > 0) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'previous')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-left-circle" title="Move Left"></em>
                                                                            </a>   
                                                                            <?php
                                                                        }
                                                                        if ($j == 0 || $j < ($total_bnrs - 1)) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'next')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-right-circle" title="Move Right"></em>
                                                                            </a>                                                                
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>                                                                      
                                                                    <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id; ?>)" class="btn btn-danger icon-with-fixed-width">
                                                                        <em class="icon-trash" title="Remove Banner"></em>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <div class="overlay-bottom overlay-white">
                                                        <div class="overlay-Text">
                                                            <div>

                                                                <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id; ?>)" class="btn btn-danger icon-with-fixed-width">
                                                                    <em class="icon-trash" title="Remove Banner"></em>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>


                                            </div>

                                            <?php
                                            $j++;
                                        }
                                    } else {
                                        ?>
                                        <div class="item active">                        
                                            <img class="0-slide img-responssive" src="<?php echo Yii::app()->getbaseUrl(true) ?>/images/dummy_banner.jpg" alt=""  />              
                                            <div class="overlay-bottom overlay-white">
                                                <div class="overlay-Text">
                                                    <div>
                                                        <a href="#" class="btn btn-success icon-with-fixed-width avatar-view" id="<?php echo $section->id ?>"  data-toggle="modal" data-target="#avatar-modal">
                                                            <em class="icon-plus" title="Add Banner"></em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>
                                </div>
                                <a class="left carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="prev"><span class="icon-arrow-left"></span></a>
                                <a class="right carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="next"><span class="icon-arrow-right"></span></a>

                                <?php
                            } elseif ($style_code == 'style_2') {
                               ?>
                                <div class="slider-one-wrap">
                                    <div class="flexslider">
                                        <ul class="slides">
                                            <?php
                                            foreach ($data as $banner) {
                                                $banner_src = $banner->image_name;
                                                $banner_title = $banner->title;
                                                $banner_id = $banner->id;
                                                $banner_full_url = $bannUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
                                                $banner_video_image = $banner->video_placeholder_img;
                                                $banner_video = $banner->video_remote_url;
                                                $bnr_txt = ($banner->banner_text != '') ? $banner->banner_text : 'ADD TEXT THAT GOES ON TOP OF BANNER HERE';
                                                $join_btn_txt = 'JOIN NOW';
                                                $url_text = $banner->url_text;
                                                $txt_bnr = $banner->banner_text;
                                                $banner_type = $banner->banner_type;


                                                $seq[] = $banner_id;
                                                ?>
                                                <li><img src="<?php
                                                    if ($banner_type == '0') {
                                                        echo $banner_video_image;
                                                    } else {
                                                        echo $banner_full_url;
                                                    }
                                                    ?>" />
                                                    <?php if ($banner_type == '0') { ?><a href="javascript:void(0);" class="play_video"  data-name="banner_video" id="<?php echo $banner_id; ?>" data-height ="<?php echo $max_height; ?>" data-width="<?php echo $max_width; ?>"><div class='play-btn-big'></div></a>
                                                    <?php } ?>
                                                 <?php
                                                if (@IS_LANGUAGE == 1) {
                                                    if ($this->language_id == $banner_text['language_id'] && $banner_text['parent_id'] == 0) {
                                                        ?>
                                                        <div class="overlay-bottom overlay-white flexDelete">
                                                            <div class="overlay-Text">
                                                                <div>
                                                                    <?php if ($max_allowed < 1 || $total_bnrs < $max_allowed) {
                                                                        ?>
                                                                        <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn btn-success icon-with-fixed-width avatar-view" id="<?php echo $section->id ?>">
                                                                            <em class="icon-plus" title="Add Banner"></em>
                                                                        </a>
                                                                    <?php } ?>
                                                                    <?php
                                                                    if ($total_bnrs > 1) {
                                                                        if ($j > 0) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'previous')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-left-circle" title="Move Left"></em>
                                                                            </a>   
                                                                            <?php
                                                                        }
                                                                        if ($j == 0 || $j < ($total_bnrs - 1)) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'next')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-right-circle" title="Move Right"></em>
                                                                            </a>                                                                
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>                                                                      
                                                                    <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id; ?>)" class="btn btn-danger icon-with-fixed-width">
                                                                        <em class="icon-trash" title="Remove Banner"></em>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <div class="overlay-bottom overlay-white flexDelete">
                                                        <div class="overlay-Text">
                                                            <div>

                                                                <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id; ?>)" class="btn btn-danger icon-with-fixed-width">
                                                                    <em class="icon-trash" title="Remove Banner"></em>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                </li>
                                            <?php } ?>
                                            <?php
                                            
                                            for ($x = $total_bnrs; $x < $thumbVisible; $x++) {
                                                ?>
                                                <li><img src="https://placeholdit.imgix.net/~text?txtsize=33&txt=<?php echo $width ?>x<?php echo $height ?>&w=<?php echo $width ?>&h=<?php echo $height ?>" /></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="thumb-list">
                                        <a class="dire-nav _top"></a>
                                        <a class="dire-nav _bottom"></a>
                                        <ul>
                                            <?php
                                            foreach ($data as $banner) {
                                                $banner_src = $banner->image_name;
                                                $banner_title = $banner->title;
                                                $banner_id = $banner->id;
                                                $banner_full_url = $bannUrl . "/system/studio_banner/" . $studio_id . "/studio_thumb/" . $banner_src;
                                                $banner_video_image = $banner->video_placeholder_img;
                                                $banner_video_image = $banner->video_placeholder_thumb_img;
                                                $banner_video = $banner->video_remote_url;
                                                $bnr_txt = ($banner->banner_text != '') ? $banner->banner_text : 'ADD TEXT THAT GOES ON TOP OF BANNER HERE';
                                                $join_btn_txt = 'JOIN NOW';
                                                $url_text = $banner->url_text;
                                                $txt_bnr = $banner->banner_text;
                                                $banner_type = $banner->banner_type;


                                                $seq[] = $banner_id;
                                                ?>
                                                <li><img src="<?php
                                                    if ($banner_type == '0') {
                                                        echo $banner_video_image;
                                                    } else {
                                                        echo $banner_full_url;
                                                    }
                                                    ?>" /></li>

                                            <?php } ?>
                                            <?php
                                            for ($x = $total_bnrs; $x < $thumbVisible; $x++) {
                                                ?>
                                                <li><img src="https://placeholdit.imgix.net/~text?txtsize=20&txt=<?php echo $width ?>x<?php echo $height ?>&w=<?php echo $thumb_width ?>&h=<?php echo $thumb_height ?>" /></li>
                                            <?php } ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div id="myCarousel_<?php echo $section->id ?>" class="carousel slide relative carousel-fade" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner">
                                    <?php
                                    $seq = array();
                                    if ($total_bnrs > 0) {
                                        $j = 0;
                                        foreach ($data as $banner) {
                                            $banner_src = $banner->image_name;
                                            $banner_title = $banner->title;
                                            $banner_id = $banner->id;
                                            $banner_full_url = $bannUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
                                            $banner_video_image = $banner->video_placeholder_img;
                                            $banner_video = $banner->video_remote_url;
                                            $bnr_txt = ($banner->banner_text != '') ? $banner->banner_text : 'ADD TEXT THAT GOES ON TOP OF BANNER HERE';
                                            $join_btn_txt = 'JOIN NOW';
                                            $url_text = $banner->url_text;
                                            $txt_bnr = $banner->banner_text;
                                            $banner_type = $banner->banner_type;


                                            $seq[] = $banner_id;
                                            ?>
                                            <div class="item <?php echo ($j == 0) ? 'active' : '' ?>">
                                                <img class="<?php echo $j ?>-slide img-responsive" src="<?php
                                                if ($banner_type == '0') {
                                                    echo $banner_video_image;
                                                } else {
                                                    echo $banner_full_url;
                                                }
                                                ?>" alt="<?php echo $banner_title; ?>"  />
                                                <?php if ($banner_type == '0') { ?><a href="javascript:void(0);" class="play_video"  data-name="banner_video" id="<?php echo $banner_id; ?>" data-height ="<?php echo $max_height; ?>" data-width="<?php echo $max_width; ?>"><div class='play-btn-big'></div></a>
                                                <?php } ?>

                                                <?php
                                                if (@IS_LANGUAGE == 1) {
                                                    if ($this->language_id == $banner_text['language_id'] && $banner_text['parent_id'] == 0) {
                                                        ?>
                                                        <div class="overlay-bottom overlay-white">
                                                            <div class="overlay-Text">
                                                                <div>
                                                                    <?php if ($max_allowed < 1 || $total_bnrs < $max_allowed) {
                                                                        ?>
                                                                        <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn btn-success icon-with-fixed-width avatar-view" id="<?php echo $section->id ?>">
                                                                            <em class="icon-plus" title="Add Banner"></em>
                                                                        </a>
                                                                    <?php } ?>
                                                                    <?php
                                                                    if ($total_bnrs > 1) {
                                                                        if ($j > 0) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'previous')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-left-circle" title="Move Left"></em>
                                                                            </a>   
                                                                            <?php
                                                                        }
                                                                        if ($j == 0 || $j < ($total_bnrs - 1)) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'next')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-right-circle" title="Move Right"></em>
                                                                            </a>                                                                
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>                                                                      
                                                                    <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id; ?>)" class="btn btn-danger icon-with-fixed-width">
                                                                        <em class="icon-trash" title="Remove Banner"></em>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <div class="overlay-bottom overlay-white">
                                                        <div class="overlay-Text">
                                                            <div>

                                                                <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id; ?>)" class="btn btn-danger icon-with-fixed-width">
                                                                    <em class="icon-trash" title="Remove Banner"></em>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>


                                            </div>

                                            <?php
                                            $j++;
                                        }
                                    } else {
                                        ?>
                                        <div class="item active">                        
                                            <img class="0-slide img-responssive" src="<?php echo Yii::app()->getbaseUrl(true) ?>/images/dummy_banner.jpg" alt=""  />              
                                            <div class="overlay-bottom overlay-white">
                                                <div class="overlay-Text">
                                                    <div>
                                                        <a href="#" class="btn btn-success icon-with-fixed-width avatar-view" id="<?php echo $section->id ?>"  data-toggle="modal" data-target="#avatar-modal">
                                                            <em class="icon-plus" title="Add Banner"></em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>
                                </div>
                                <a class="left carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="prev"><span class="icon-arrow-left"></span></a>
                                <a class="right carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="next"><span class="icon-arrow-right"></span></a>

                                <?php
                            }
                        } else {
                            $theme = $this->studio->parent_theme;
						if ($style_code == 'style_1' || $style_code == 'style_3') {
                            ?>
                            <div id="myCarousel_<?php echo $section->id ?>" class="carousel slide m-t-20" data-ride="carousel" data-interval="false">
                                <!-- Indicators -->
                                <div class="carousel-inner">
                                    <div class="item active <?php echo $status; ?>">
										<img class="<?php echo $bannerstyle['id']; ?>-slide img-responsive" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=<?php echo $width ?>x<?php echo $height ?>&w=<?php echo $width ?>&h=<?php echo $height ?>" />
									</div>
                                    <a class="left carousel-control" href="#myCarousel_<?php echo $bannerstyle['id'] ?>" data-slide="prev"><span class="icon-arrow-left"></span></a>
                                    <a class="right carousel-control" href="#myCarousel_<?php echo $bannerstyle['id'] ?>" data-slide="next"><span class="icon-arrow-right"></span></a>

                                </div>

                            </div> 
						<?php } else { 
						?>
							 <div class="slider-one-wrap">
                                    <div class="flexslider">
                                        <ul class="slides">
											 <?php 
												for ($x = $total_bnrs; $x < $thumbVisible; $x++) {
                                                ?>
                                                <li><img src="https://placeholdit.imgix.net/~text?txtsize=33&txt=<?php echo $width ?>x<?php echo $height ?>&w=<?php echo $width ?>&h=<?php echo $height ?>" /></li>
                                            <?php } ?>
										</ul>
									</div>
									<div class="thumb-list">
                                        <a class="dire-nav _top"></a>
                                        <a class="dire-nav _bottom"></a>
                                        <ul>
											<?php  for ($x = $total_bnrs; $x < $thumbVisible; $x++) {
											  ?>
											  <li><img src="https://placeholdit.imgix.net/~text?txtsize=20&txt=<?php echo $width ?>x<?php echo $height ?>&w=<?php echo $thumb_width ?>&h=<?php echo $thumb_height ?>" /></li>
										  <?php } ?>
										</ul>
									</div>
							 </div>
							<?php }?>
                            <?php
                        }
                    }
                }
                ?>        
            </div>
            <div class="col-sm-12 m-t-20">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-docs icon left-icon "></em>
                    </div>
                    <h4>Banner Properties</h4>    
                </div>

                <div class="row ">

                    <div class="col-md-8 m-t-20">
                        <p class="m-t-10"> Define how your banner looks and behaves</p>
					   <input type="hidden" value="<?php echo $thumbVisible; ?>" id="visible_thumb" name="thumb_visible">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Size&nbsp;(In Px)</label>
                                <div class="col-md-8">
                                    <div class="fg-line"> 
                                        <input type="hidden" class="previous_width" value="<?php
                                        if ($existing_style != '') {
                                            echo $existing_style['h_banner_dimension'];
                                        } else {
                                            echo $bannerstyle['h_banner_dimension'];
                                        }
                                        ?>">
                                        <input type="hidden" class="previous_height" value="<?php
                                        if ($existing_style != '') {
                                            echo $existing_style['v_banner_dimension'];
                                        } else {
                                            echo $bannerstyle['v_banner_dimension'];
                                        }
                                        ?>">
                                        <input type="hidden" class="change_height" value="">
                                        <input type="hidden" class="change_width" value="">
                                        <input type="text"  id="h_size" name="h_size" placeholder="<?php
                                        if ($existing_style != '') {
                                            echo $existing_style['h_banner_dimension'];
                                        } else {
                                            echo $bannerstyle['h_banner_dimension'];
                                        }
                                        ?>" value="<?php
                                               if ($existing_style != '') {
                                                   echo $existing_style['h_banner_dimension'];
                                               } else {
                                                   echo $bannerstyle['h_banner_dimension'];
                                               }
                                               ?>">
                                        x <input type="text"  id="v_size" name="v_size" placeholder="<?php
                                        if ($existing_style != '') {
                                            echo $existing_style['v_banner_dimension'];
                                        } else {
                                            echo $bannerstyle['v_banner_dimension'];
                                        }
                                        ?>" value="<?php
                                                 if ($existing_style != '') {
                                                     echo $existing_style['v_banner_dimension'];
                                                 } else {
                                                     echo $bannerstyle['v_banner_dimension'];
                                                 }
                                                 ?>">

                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label class="checkbox checkbox-inline m-r-20">
                                            <?php
                                            if ($existing_style != '') {
                                                $auto = $existing_style['auto_scroll'];
                                            } else {
                                                $auto = $bannerstyle['auto_scroll'];
                                            }
                                            ?>
                                            <input type="checkbox" class="autoSroll" value="1" name="auto_scroll"<?php ?> <?php echo ($auto > 0) ? ' checked="checked"' : ''; ?> id="auto_scroll">
                                            <i class="input-helper"></i> Auto Scroll &nbsp;(In Ms)
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" name="scroll_interval" id="scroll_interval" value="<?php
                                        if ($existing_style != '') {
                                            echo $existing_style['scroll_interval'];
                                        } else {
                                            echo $bannerstyle['scroll_interval'];
                                        }
                                        ?>">
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <div id="bnr_txt_error" class="error m-b-20"></div>
                                    <button class="btn btn-primary" id="update_btn_banner" value="update" type="submit">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<div class="col-sm-12 m-t-20">
    <?php
    foreach ($sections as $section) {
        $max_width = $resp['width'];
        $max_height = $resp['height'];
        $section_id = $section->id;
        $thumb = Yii::app()->common->getThumbBannerDimension($section->id);
        $thumb_width = $thumb['width'];
        $thumb_height = $thumb['height'];
        ?>
        <input type="hidden" value="<?php echo $thumb_width; ?>" id="thumbWidth">
        <input type="hidden" value="<?php echo $thumb_height; ?>" id="thumbHeight">
        <div class="Block-Header">
            <div class="icon-OuterArea--rectangular">
                <em class="icon-docs icon left-icon "></em>
            </div>
            <h4>Banner Items</h4>   
            <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn upload-Image relative">
                <em class="icon-plus absolute" title="Add Banner"></em>
            </a>

        </div>  
<?php 
		$ban = new StudioBanner;
		$data = $ban->findAllByAttributes(array('banner_section' => $section_id, 'studio_id' => $studio_id), array('order' => 'id_seq ASC'));
		$total_bnrs = count($data);
		if($total_bnrs > 0){
?>
    <div class="col-sm-12 m-t-10">           

		<div class="slideshow">
                <ul id="banner_sortable" class="p-l-0 slides sortable">

                    <?php
                    if ($total_bnrs > 0) {
                        $j = 0;
                        $bannUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                        foreach ($data as $banner) {
                            $banner_src = $banner->image_name;
                            $banner_title = $banner->title;
                            $banner_type = $banner->banner_type;
                            $banner_video_image = $banner->video_placeholder_thumb_img;
                            $banner_video = $banner->video_remote_url;
                            $banner_full_url = $bannUrl . "/system/studio_banner/" . $studio_id . "/studio_thumb/" . $banner_src;
                            ?>

                            <li id="sortable_<?php echo $banner->id; ?>" class="slide"> <img class="img-thumbnails  banner_item <?php if ($style_code == 'style_2') echo 'banner_style2'; ?>" data-id="<?php echo $banner->id; ?>" src="<?php
                                if ($banner_type == '0') {
                                    echo $banner_video_image;
                                } else {
                                    echo $banner_full_url;
                                }
                                ?>" />  </li>     

                            <?php
                        }
                    }
                    ?> 

                </ul> 
			    <a href="#" class="prev"><em class="fa fa-chevron-left"></em></a>
    <a href="#" class="next"><em class="fa fa-chevron-right"></em></a>
            </div>
        </div>
    <?php } } ?>

</div>

<?php if ($total_bnrs > 0){ ?>
<div class="col-sm-12 m-t-20 banner_properties">
    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-docs icon left-icon "></em>
        </div>
        <h4>Item Properties</h4>   
    </div>
    <div class="loader_properties"  style="display: none;">
        <div class="preloader pls-blue  ">
            <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20"></circle>
            </svg>
        </div>
    </div>
    <div class="banner_proper"></div>

</div>
<?php } ?>
</div>
<div id="play_video" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">                   
                <div class="embed-responsive embed-responsive-16by9 video_banner">

                </div>
            </div>
        </div>
    </div>
</div>


   
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.jcarousellite.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.flexslider.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/bannerStyle/style_2/js/bannerslide.js"></script>


<script type="text/javascript">


$(function () {
    var thumbHeight=$('#thumbHeight').val();
    var thumbWidth=$('#thumbWidth').val();
    var banner_width = $('#h_size').val();
    var banner_height = $('#v_size').val();
	var windowWidth = $(window).width();
	var thumbVis=Math.round(windowWidth/thumbWidth);
    $("#banner_sortable").sortable({
        revert: 100,
        placeholder: 'ui_placeholder',
        zIndex: 9999,
        start: function (e, ui) {
            ui.placeholder.width(banner_width / 6);
            ui.placeholder.height(banner_height / 6);
        },
        update: function () {
            var order = $('#banner_sortable').sortable('serialize');
            console.log(order);
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/BannerReorder",
                data: "order=" + $('#banner_sortable').sortable('serialize'),
                dataType: "json",
                cache: false,
                success: function (data) {
                    console.log(data);
                }
            });
        }
    });	


    $('.img-thumbnails').click(function () {
        var id = $(this).attr('data-id');
        $('.img-thumbnails[data-id=' + id + ']').addClass('current-thumb');
        $('.img-thumbnails').not('[data-id=' + id + ']').removeClass('current-thumb');

        var x = $(".item img[data-id$='" + id + "']");
        $('.item').removeClass('active');
        $(x).parent().addClass('active');
    });
	   $(".slideshow").jCarouselLite({
		containerSelector: '.slides',
        itemSelector: '.slide',
		autoWidth: true,
        responsive: true,
		visible:thumbVis,
        btnNext: ".slideshow .next",
        btnPrev: ".slideshow .prev",
        speed: 800,
        circular: false,
	   });
	   
});


$('#update_btn_banner').click(function (e) {
    e.preventDefault();
    var previous_width = $('.previous_width').val();
    var previous_height = $('.previous_height').val();
    var change_width = $('#h_size').val();
    var change_height = $('#v_size').val();
    var form = $(this).parents('form');
    if ((change_width != '' && change_width != previous_width) || (change_height != '' && change_height != previous_height)) {
        swal({
            title: "Modified Banner Size?",
            text: "You have modified the banner size, all images will be removed! Are you sure you would like to continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Continue",
            closeOnConfirm: true,
            html: true
        }, function (isConfirm) {
            if (isConfirm)
                form.submit();
        });
    } else {
        form.submit();
    }
});


var banner_id = $("#banner_sortable li .banner_item").first().attr('data-id');
$("#banner_sortable li .banner_item").first().addClass('current-thumb');
bannerSetting(banner_id);
$('.banner_item').click(function () {
    var banner_id = $(this).attr('data-id');
    bannerSetting(banner_id);
});
function bannerSetting(banner_id) {
    $.ajax({
        url: HTTP_ROOT + "/template/bannerproperties",
        data: {banner_id: banner_id},
        type: 'POST',
        beforeSend: function () {
            $('.loader_properties').show();
        },
        success: function (res) {
            $('.loader_properties').hide();
            $('.banner_proper').html(res);

        }
    });
}
$(window).load(function () {
    // The slider being synced must be initialized first
    loadBanner();
});
function loadBanner() {
    $('#carousel_flex').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 210,
        itemMargin: 5,
        asNavFor: '#slider'
    });

    $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel_flex"
    });
}

$('.play_video').click(function () {
    var id = $(this).attr('id');
    var height = $(this).attr('data-height');
    var width = $(this).attr('data-width');
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/PlayVideobanner",
        data: {'banner_id': id},
        success: function (data) {
            $('#play_video').modal('show');
            $('.video_banner').html(data);

        }
    });

});
$("#play_video").on('hidden.bs.modal', function () {
    $('.video_banner').html('');
});
</script>
