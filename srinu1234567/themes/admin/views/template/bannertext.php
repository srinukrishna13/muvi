<?php
if($data)
{
    foreach($data as $bntxt)
    {
        $txt_id = $bntxt->id;
        $banner_text = $bntxt->bannercontent;
    }
}
 else {
    $txt_id = 0;
    $banner_text = '';    
}
?>

<style>
#bannercontent_switch {
    display: none;
}    
</style>
<div class="row m-t-40">
    <div class="col-sm-12">
        
        <form method="post" class="form-horizontal" role="form" id="banner" name="banner" action="<?php echo Yii::app()->baseUrl; ?>/template/savebannertext">
            <input type="hidden" name="text_id" id="text_id" value="<?php echo $txt_id?>" />
            <div class="loading" id="subsc-loading"></div>  
            <div class="form-group">
                <label class="control-label col-sm-4">Enter Text:</label>
                <div class="col-sm-8">
                    <div class="fg-line">
                        <textarea id="bannercontent" class="form-control input-sm" name="bannercontent" aria-hidden="true"><?php echo html_entity_decode($banner_text); ?></textarea>  
                    </div>
                </div>
            </div>           
            <div class="form-group">
                <label class="control-label col-sm-4">&nbsp;</label>
                <div class="col-sm-8"><button type="submit" class="btn btn-default">Update</button></div>
            </div>                
        </form>        
    </div>        
</div>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true)?>/js/nicEdit-latest.js"></script>
<script type="text/javascript"> 
$(document).ready(function(){
    $('#subsc-loading').hide();
    //<![CDATA[
  bkLib.onDomLoaded(function() {
        new nicEditor({buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFormat','indent','outdent','image','link','unlink','forecolor','bgcolor', 'xhtml']}).panelInstance('bannercontent'); 
  });
  //]]>
    $("#banner").validate({ 
        rules: {    
            bannercontent: {
                required: true
            },          
        },         
        submitHandler: function(form) {
            $("#banner").submit();
        },
         errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
       }
    });
});
</script>