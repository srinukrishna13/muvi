<div class="row m-b-40 m-t-40">
    <div class="col-xs-3">
        <div class="fancy-collapse-panel">
        <div class="panel-group" id="accordion1">
            <div class="panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseThree">Files</a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="panel-group" id="accordion2">        
                            <?php
                            foreach ($view_folders as $key => $value) {
                                if (is_array($value) && count($value) > 0) {
                                    ?>
                                    <div class="panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $key; ?>"><?php echo $key; ?></a>
                                            </h4>
                                        </div>
                                        <div id="<?php echo $key; ?>" class="panel-collapse collapse">
                                            <div class="panel-body">                            
                                                <?php
                                                if (is_array($value)) {

                                                    foreach ($value as $k => $val) {
                                                        if (is_array($val) && count($val) > 0) {
                                                            ?>
                                                            <div class="panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#<?php echo $key . '-' . $k; ?>"><?php echo $k; ?></a>
                                                                    </h4>
                                                                </div>
                                                                <div id="<?php echo $key . '-' . $k; ?>" class="panel-collapse collapse">
                                                                    <div class="panel-body">                                            
                                                                        <?php
                                                                        if (is_array($val)) {
                                                                            foreach ($val as $ky => $vl) {
                                                                                if (is_array($vl)) {
                                                                                    //echo $k.' -> '.$val;
                                                                                } else {
                                                                                    if (Yii::app()->general->fileCanEdit($vl))
                                                                                        echo '<p><a class="fa fa-trash removefile" aria-hidden="true" data-loc="' . $key . '/' . $k . '/' . $vl . '"></a> <a href="#" class="editfile" id="' . $key . '/' . $k . '/' . $vl . '">' . $vl . '</a></p>';
                                                                                    else
                                                                                        echo '<p><a class="fa fa-trash removefile" aria-hidden="true" data-loc="' . $key . '/' . $k . '/' . $vl . '"></a> ' . $vl . '</p>';
                                                                                }
                                                                            }
                                                                        } else {
                                                                            ?>
                                                                            <?php foreach ($files as $file) { ?>
                                                                                <p><a class="editfile" id="<?php echo 'views/' . $fold_name . '/' . $file; ?>"><?php echo $val; ?></a></p>
                                                                                <?php
                                                                                if (Yii::app()->general->fileCanEdit($val))
                                                                                    echo '<p><a class="fa fa-trash removefile" aria-hidden="true" data-loc="' . $key . '/' . $k . '/' . $val . '"></a> <a href="#" class="editfile" id="' . $key . '/' . $k . '/' . $val . '">' . $val . '</a></p>';
                                                                                else
                                                                                    echo '<p><a class="fa fa-trash removefile" aria-hidden="true" data-loc="' . $key . '/' . $k . '/' . $vl . '"></a> ' . $vl . '</p>';
                                                                                ?>
                                                                            <?php } ?>                           
                                                                        <?php }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <?php
                                                        }else {
                                                            if (Yii::app()->general->fileCanEdit($val))
                                                                echo '<p><a class="fa fa-trash removefile" aria-hidden="true" data-loc="' . $key . '/' . $val . '"></a> <a href="#" class="editfile" id="' . $key . '/' . $val . '">' . $val . '</a></p>';
                                                            else
                                                                echo '<p><a class="fa fa-trash removefile" aria-hidden="true" data-loc="' . $key . '/' . $val . '"></a> ' . $val . '</p>';
                                                        }
                                                    }
                                                } else {
                                                    echo '<p><a class="fa fa-trash removefile" aria-hidden="true" data-loc="' . $value . '"></a> ' . $value . '</p>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>                                                  
                                    <?php
                                } else {
                                    echo '<p><a class="fa fa-trash removefile" aria-hidden="true" data-loc="' . $value . '"></a> ' . $value . '</p>';
                                }
                            }
                            ?>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        </div>
    </div>
    <div class="col-xs-9">
        <div><span id="current_path"></span></div>
        <div id="block_error" class="error"></div>
        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul role="tablist" class="nav nav-tabs">
                <li class="active" role="presentation">
                    <a data-toggle="tab" role="tab" aria-controls="home" href="#modified" id="tab_modified" aria-expanded="true">Modified File</a>
                </li>
                <li role="presentation" class="">
                    <a data-toggle="tab" role="tab" aria-controls="tab" href="#modified" id="tab_original" aria-expanded="false">Original File</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="modified" class="tab-pane active" role="tabpanel">
                    <div class="m-t-20">
                        <form id="editor_form" name="editor_form">
                            <div id="view_loader" class="loader"></div>                                                        
                            <input type="hidden" name="filepath" id="filepath" />
                            <div class="form-group">
                                <textarea name="editor_content" id="editor_content" class="editor"></textarea>
                            </div>
                            <input type="submit" id="save-file" class="btn btn-primary" value="Save" style="display:none" />
                        </form>
                    </div>
                </div>
            </div>
        </div>                              
    </div>
</div>
<style>
    .editor{width:100%; height:600px;}
    #accordion1 p{word-break: break-all;}
    #accordion1 p a{cursor: pointer;}
    .panel-default{border:1px solid #ddd}
    
    .fancy-collapse-panel .panel-default > .panel-heading {
        padding: 0;
    }
    .fancy-collapse-panel .panel-heading a {
        padding: 12px 35px 12px 15px;
        display: inline-block;
        width: 100%;
        position: relative;
    }
    .fancy-collapse-panel .panel-heading a:after {
        font-family: "FontAwesome";
        content: "\f147";
        position: absolute;
        right: 20px;
        font-size: 16px;
        font-weight: 400;
        top: 50%;
        line-height: 1;
        margin-top: -10px;
    }

    .fancy-collapse-panel .panel-heading a.collapsed:after {
        content: "\f196";
    }    
</style>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/components/jquery-linedtextarea.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/components/jquery-linedtextarea.css" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/css/custom.css" type="text/css" />
<script type="text/javascript">
    $('#view_loader').hide();        
    $(function() {
        $(".editor").linedtextarea();
        $('#editor_content').val('');
        var validator = $("#editor_form").submit(function() {

        }).validate({
            ignore: "",
            rules: {
                filepath: {
                    required: true
                }
            },
            messages: {
                filepath: {
                    required: 'Please select a file to edit'
                }
            },
            errorPlacement: function(label, element) {
                // position error label after generated textarea 
                label.addClass('red');
                label.insertAfter(element.parent());

            },
            submitHandler: function(form) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/saveeditorcontent",
                    data: $('#editor_form').serialize(),
                    dataType: "json",
                    cache: false,
                    success: function(data)
                    {
                        if (data.error == 1) {
                            $('#block_error').addClass('alert alert-danger');
                            $('#block_error').html(data.message).show();
                            $('#view_loader').hide();
                            return false;
                        } else {
                            $('#block_error').removeClass('alert alert-danger');
                            $('#block_error').addClass('alert alert-success').html(data.message).show();
                            $('#view_loader').hide();
                        }
                    },
                    beforeSend: function() {
                        $('#view_loader').show();
                        $('#block_error').html('').hide();
                    }                            
                });
            }
        });
        
        function fill_textarea(url, el) {
            $('#view_loader').show();
            $.get(url+'?_='+Math.round(Math.random()*10000), {cache: false}, function (data) {
                el.val(data);
                $('#block_error').html('').hide();
                $('#view_loader').hide();
            }, "text").fail(function() {
                $('#block_error').addClass('alert alert-danger');
                $('#block_error').html('Invalid URL').show();
                $('#view_loader').hide();
                el.val('');
            });
        }        
        $('.editfile').click(function(e){
            e.preventDefault();
            $('#current_path').html('<h4>You are currently editing: ' + $(this).attr('id') + '</h4>');
            $('#filepath').val($(this).attr('id'));
            $('.nav-tabs a:first').tab('show'); 
            var fileurl = '<?php echo $this->siteurl.'/themes/'.$this->studio->theme.'/';?>'+$(this).attr('id');
            fill_textarea(fileurl, $('#editor_content'));
            $('#save-file').show();
        });
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('#view_loader').show();
            var target = $(e.target).attr("id") // activated tab
            if(target == 'tab_original'){                
                var fileurl = '<?php echo $this->siteurl.'/sdkThemes/'.$this->studio->parent_theme.'/';?>'+$('#filepath').val();
                fill_textarea(fileurl, $('#editor_content'));    
                $('#save-file').hide();
            }else{
                var fileurl = '<?php echo $this->siteurl.'/themes/'.$this->studio->theme.'/';?>'+$('#filepath').val();
                fill_textarea(fileurl, $('#editor_content'));    
                $('#save-file').show();            
            }
        });        
        $('.removefile').click(function(){
            var removepath = $(this).attr('data-loc');
            swal({
                title: "Are you sure?",
                text: "File will be deleted from your template permanently!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/removefile",
                        data: {'filepath': removepath},
                        dataType: "json",
                        cache: false,
                        success: function(data)
                        {
                            if (data.error == 1) {
                                $('#block_error').addClass('alert alert-danger');
                                $('#block_error').html(data.message).show();
                                return false;
                            } else {
                                window.location.reload();
                            }
                            $('#view_loader').hide();
                        }
                    });  
                }
            });                                             
        });
    });
</script>