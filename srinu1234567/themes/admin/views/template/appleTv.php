<?php
$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';
$posterImg2 = POSTER_URL . '/no-image-a.png';
$app_icon_img_1 = POSTER_URL . '/no-image-a.png';
$app_icon_img_2 = POSTER_URL . '/no-image-a.png';
$app_icon_img_3 = POSTER_URL . '/no-image-a.png';
?>
<style>
    #developer_id{
        display: none;
    }
    .help-block{
        display: none;
    }
    .has-error .help-block {
        display: block;
    }     
</style>

<div class="row m-t-40 m-b-40">
    <div class="col-md-8 col-sm-12">
        <div class="Block">
            <form id="data-from" class="form-horizontal" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="app_type" value="appletv" />
                <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>    
                    <div class="form-group">
                        <div class="col-md-12">
                            <h4><span class=" bold red">Apple TV App is a paid add-on.</span><a href="javascript:void(0);" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/ios/1');"><span class="bold blue"> Purchase subscription</span></a><span class="bold red"> to enable it.</span></h4>
                        </div>
                    </div>
                <?php } ?> 

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <?php if (isset($appdata) && $appdata['android_app_info']) { ?>
                                    <input type="checkbox" name="android_check" id="android_check" value="" checked />
                                <?php } else { ?>
                                    <input type="checkbox" name="android_check" id="android_check" value="" />
                                <?php } ?>
                                <i class="input-helper"></i> Use the information from Android App
                            </label>
                        </div>
                    </div>
                </div>   
                <div class="form-group">
                    <label for="appname" class="col-md-4 control-label">App Name:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="app_name" placeholder="App Name  (max limit 50 characters)" id="app_name" class="form-control input-sm" value="<?php if (isset($appdata)) {
                                    echo $appdata['app_name'];
                                } ?>" required/>
                        </div>
                        <small class="help-block"></small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="Description" class="col-md-4 control-label">Description:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea name="description" placeholder="Description  (max limit 4000 characters)" id="description" class="form-control input-sm" rows="5" ><?php if (isset($appdata)) {
                                    echo $appdata['description'];
                                } ?></textarea>
                        </div>
                        <small class="help-block"></small>
                    </div>
                </div>
                <?php
                $app_icon_url_1 = $appdata['app_icon_1'];
                $app_icon_url_2 = $appdata['app_icon_2'];
                $app_icon_url_3 = $appdata['app_icon_3'];
                $splash_screen_url = $appdata['splash_screen'];
                $feature_graphic_url = $appdata['feature_graphic'];
                ?>
                <div class="form-group">
                    <label for="icon" class="col-md-4 control-label">App Launch Icon(For Parallax Effect):</label>

                    

                    <div class="col-md-8">
                       
                    <div class="Collapse-Block">
                       <div class="Block-Header has-right-icon">
                          <!-- Icon exist to left :--> 
                          <div class="icon-OuterArea--rectangular"> <a href="#"> <em class="icon-arrow-down icon left-icon icon-arrow-up"></em> </a> </div>
                          <h4 class="drag-cursor">Front App Icon </h4>
                          <p class="grey font-12">Choose a 1280x768 JPEG or PNG file. </p>
                           <div class=" m-t-10">
                                <button type="button" class="btn btn-deafult-with-bg btn-sm" id="app-icon-modal-btn1" style="">Upload Front App Icon</button>
                           </div>
                       </div>
                       <div class="Collapse-Content m-t-20">
                           <?php if (isset($app_icon_url_1) && $app_icon_url_1 != '') { ?>
                         <div class="fixedWidth--Preview">
                                    <img src="<?php echo $app_icon_url_1; ?>" alt="" id="fhd-poster-img" class="img-responsive">

                                </div>
                           <?php }?>
                       </div>
                    </div>
                        
                    <div class="Collapse-Block">
                       <div class="Block-Header has-right-icon">
                          <!-- Icon exist to left :--> 
                          <div class="icon-OuterArea--rectangular"> <a href="#"> <em class="icon-arrow-down icon left-icon icon-arrow-up"></em> </a> </div>
                          <h4 class="drag-cursor">Middle App Icon </h4>
                          <p class="grey font-12">Choose a 1280x768 JPEG or PNG file. </p>
                           <div class=" m-t-10">
                                <button type="button" class="btn btn-deafult-with-bg btn-sm" id="app-icon-modal-btn2">Upload Middle App Icon</button>
                           </div>
                       </div>
                       <div class="Collapse-Content m-t-20">
                           <?php if (isset($app_icon_url_2) && $app_icon_url_2 != '') { ?>
                         <div class="fixedWidth--Preview">
                                    <img src="<?php echo $app_icon_url_2; ?>" alt="" id="fhd-poster-img" class="img-responsive">

                                </div>
                           <?php }?>
                       </div> 
                    </div>
                        
                     
                     <div class="Collapse-Block">
                       <div class="Block-Header has-right-icon">
                          <!-- Icon exist to left :--> 
                          <div class="icon-OuterArea--rectangular"> <a href="#"> <em class="icon-arrow-down icon left-icon icon-arrow-up"></em> </a> </div>
                          <h4 class="drag-cursor">Back App Icon </h4>
                          <p class="grey font-12">Choose a 1280x768 JPEG or PNG file. </p>
                           <div class=" m-t-10">
                                <button type="button" class="btn btn-deafult-with-bg btn-sm" id="app-icon-modal-btn3">Upload Back App Icon</button>
                           </div>
                       </div>
                       <div class="Collapse-Content m-t-20">
                           <?php if (isset($app_icon_url_3) && $app_icon_url_3 != '') { ?>
                         <div class="fixedWidth--Preview">
                                    <img src="<?php echo $app_icon_url_3; ?>" alt="" id="fhd-poster-img" class="img-responsive">

                                </div>
                           <?php }?>
                       </div>
                    </div>
                     
                    </div>

                    </div>




                <div class="form-group">
                    <label for="screen" class="col-md-4 control-label">Splash Screen:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg btn-sm" id="splash-screen-modal-btn">Upload Splash Screen</button>

<?php if (isset($splash_screen_url) && $splash_screen_url != '') { ?>
                            <div class="fixedWidth--Preview m-t-20">
                                <img src="<?php echo $splash_screen_url; ?>"  rel="tooltip" />
                            </div>    
<?php } ?>
                    </div>
                </div>

                <!--NEW FEATURE GRAPHIC IMAGE START-->
                <div class="form-group">
                    <label for="screen" class="col-md-4 control-label">Feature Graphic:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg btn-sm" id="feature-graphic-modal-btn">Upload Top Self Image</button>

<?php if (isset($feature_graphic_url) && $feature_graphic_url != '') { ?>
                            <div class="fixedWidth--Preview m-t-20">
                                <img src="<?php echo $feature_graphic_url; ?>"  rel="tooltip" />
                            </div>    
<?php } ?>
                    </div>
                </div>
                <!--NEW FEATURE GRAPHIC IMAGE END-->
                <div class="form-group">
                    <label for="distribution" class="col-md-4 control-label">Distribution Geography:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="">
                                <input type="hidden" value="<?php if (isset($appdata)) {
    echo $appdata['distribution_geography'];
} ?>" id="geography" />
                                <select class="form-control input-sm" name="distribution_geography[]" style="height: 200px;" id="distribution_geography" multiple>
                                    <option value="All" selected>All Countries</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Virgin Islands">British Virgin Islands</option>
                                    <option value="Brunei">Brunei</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Federated States Of Micronesia">Federated States Of Micronesia</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="celand">celand</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakstan">Kazakstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Lao Peopleâ€™s Democratic Republic">Lao Peopleâ€™s Democratic Republic</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="Macau">Macau</option>
                                    <option value="Macedonia">Macedonia</option>
                                    <option value="Madagascar">Madagascar</option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Mauritania">Mauritania</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montserrat">Montserrat</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">Netherlands</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau">Palau</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Republic Of Congo">Republic Of Congo</option>
                                    <option value="Republic Of Korea">Republic Of Korea</option>
                                    <option value="Republic Of Moldova">Republic Of Moldova</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russia">Russia</option>
                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Seychelles">Seychelles</option>
                                    <option value="Sierra Leone">Sierra Leone</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">Solomon Islands</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="St. Kitts and Nevis">St. Kitts and Nevis</option>
                                    <option value="St. Lucia">St. Lucia</option>
                                    <option value="St. Vincent and The Grenadines">St. Vincent and The Grenadines</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Taiwan	">Taiwan	</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania">Tanzania</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Turks and Caicos">Turks and Caicos</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States">United States</option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Vietnam">Vietnam</option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="lang" class="col-md-4 control-label">Language:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <input type="hidden" value="<?php if (isset($appdata)) {
    echo $appdata['language'];
} ?>" id="language" />
                                <select class="form-control input-sm" name="language" id="language-dd">
                                    <option value="Australian English">Australian English</option>
                                    <option value="Brazilian Portuguese">Brazilian Portuguese</option>
                                    <option value="Canadian English">Canadian English</option>
                                    <option value="Canadian French">Canadian French</option>
                                    <option value="Danish">Danish</option>
                                    <option value="Dutch">Dutch</option>
                                    <option value="English" selected>English</option>
                                    <option value="Finnish">Finnish</option>
                                    <option value="French">French</option>
                                    <option value="German">German</option>
                                    <option value="Greek">Greek</option>
                                    <option value="Indonesian">Indonesian</option>
                                    <option value="Italian">Italian</option>
                                    <option value="Japanese">Japanese</option>
                                    <option value="Korean">Korean</option>
                                    <option value="Malay">Malay</option>
                                    <option value="Mexican Spanish">Mexican Spanish</option>
                                    <option value="Norwegian">Norwegian</option>
                                    <option value="Portuguese">Portuguese</option>
                                    <option value="Russian">Russian</option>
                                    <option value="Simplified Chinese">Simplified Chinese</option>
                                    <option value="Spanish">Spanish</option>
                                    <option value="Swedish">Swedish</option>
                                    <option value="Thai">Thai</option>
                                    <option value="Traditional Chinese">Traditional Chinese</option>
                                    <option value="Turkish">Turkish</option>
                                    <option value="UK English">UK English</option>
                                    <option value="Vietnamese">Vietnamese</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="website" class="col-md-4 control-label">Website:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" placeholder="Wensite" name="website" id="website" class="form-control input-sm" value="<?php if (isset($appdata) && ($appdata['website'] != '' || $appdata['website'] != null)) {
    echo $appdata['website'];
} else {
    echo $studio->domain;
} ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="Email" class="col-md-4 control-label">Email</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="email" placeholder="Email" id="email" class="form-control input-sm" value="<?php if (isset($appdata) && ($appdata['email'] != '' || $appdata['email'] != null)) {
    echo $appdata['email'];
} else {
    echo $studio->contact_us_email;
} ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="phone" class="col-md-4 control-label">Phone:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="phone" id="phone" placeholder="Phone" class="form-control input-sm" value="<?php if (isset($appdata)) {
    echo $appdata['phone'];
} ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="category" class="col-md-4 control-label">Category:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <input type="hidden" value="<?php if (isset($appdata)) {
    echo $appdata['category'];
} ?>" id="category" />
                                <select class="form-control input-sm" name="category" id="category-dd">
                                    <option>Select</option>
                                    <option value="Business">Business</option>
                                    <option value="Catalogs">Catalogs</option>
                                    <option value="Education">Education</option>
                                    <option value="Entertainment">Entertainment</option>
                                    <option value="Finance">Finance</option>
                                    <option value="Food &amp; Drink">Food &amp; Drink</option>
                                    <option value="Games">Games</option>
                                    <option value="Health &amp; Fitness">Health &amp; Fitness</option>
                                    <option value="Lifestyle">Lifestyle</option>
                                    <option value="Medical">Medical</option>
                                    <option value="Music">Music</option>
                                    <option value="Navigation">Navigation</option>
                                    <option value="News">News</option>
                                    <option value="Photo &amp; Video">Photo &amp; Video</option>
                                    <option value="Productivity">Productivity</option>
                                    <option value="Reference">Reference</option>
                                    <option value="Social Networking">Social Networking</option>
                                    <option value="Sports">Sports</option>
                                    <option value="Travel">Travel</option>
                                    <option value="Utilities">Utilities</option>
                                    <option value="Weather">Weather</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="rating" class="col-md-4 control-label">Rating:</label>
                    <div class="col-md-8">
                        <input type="hidden" value="<?php if (isset($appdata)) {
    echo $appdata['rating'];
} ?>" id="rating" />
                        <div class="row form-group-cancel-magin">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label">Cartoon or Fantasy Violence:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-1" name="rating[Cartoon or Fantasy Violence]">                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label">Realistic Violence:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-2" name="rating[Realistic Violence]" >                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label">Sexual Content or Nudity:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-3" name="rating[Sexual Content or Nudity]"  >                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label">Profanity or Crude Humor:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-4" name="rating[Profanity or Crude Humor]">                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label">Alcohol, Tobacco, or Drug Use or References:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-5" name="rating[Alcohol, Tobacco, or Drug Use or References]">                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label">Mature/Suggestive Themes:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-6" name="rating[Mature/Suggestive Themes]">                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label">Simulated Gambling:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-7" name="rating[Simulated Gambling]">                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label">Horror/Fear Themes:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-8" name="rating[Horror/Fear Themes]">                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label">Prolonged graphic or sadistic realistic violence:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-9" name="rating[Prolonged graphic or sadistic realistic violence]">                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rating" class="col-md-4 control-label"> Graphic Sexual Content and Nudity:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control rating-dd input-sm" id="rating-dd-10" name="rating[Graphic Sexual Content and Nudity]">                                    
                                                    <option value="None">None</option>
                                                    <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                    <option value="Frequent/Intense">Infrequent/Intense</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="keyword" class="col-md-4 control-label">Keywords:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" placeholder="Keywords" name="keywords" class="form-control input-sm" value="<?php if (isset($appdata)) {
    echo $appdata['keywords'];
} ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="copyright" class="col-md-4 control-label">Copyright:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="copyright" class="form-control" value="<?php if (isset($appdata)) {
                                    echo $appdata['copyright'];
                                } ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <div class="checkbox">
                            <label>
<?php if (isset($appdata) && $appdata['developer_username'] != '' && $appdata['developer_password'] != '') { ?>    
                                    <input type="checkbox" id="developer_id_checkbox" name="developer_check" value="1" checked />
<?php } else { ?>
                                    <input type="checkbox" id="developer_id_checkbox"  name="developer_check" value="1" />
<?php } ?>
                                <i class="input-helper"></i> Use my Apple TV Developers ID
                            </label>
                        </div>
                    </div>
                </div>   

<?php if (isset($appdata) && $appdata['developer_username'] != '' && $appdata['developer_password'] != '') { ?>    
                    <div id="developer_id" style="display:block">
<?php } else { ?>
                        <div id="developer_id">
<?php } ?>


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">User Name</label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" name="developer_username" placeholder="User Name" class="form-control input-sm" autocomplete="off" value="<?php if (isset($appdata)) {
    echo $appdata['developer_username'];
} ?>" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-md-4 control-label">Password</label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="password" name="developer_password" placeholder="Password" class="form-control input-sm" autocomplete="off" value="<?php if (isset($appdata)) {
    echo $appdata['developer_password'];
} ?>" />
                                </div>
                            </div>
                        </div>
                    </div>


            </form>
            <form <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?> action="javascript:void(0)"<?php } else { ?>action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/appleTvApp" method="POST" id="data-from-save" <?php } ?> class="form-horizontal">
                <input type="hidden" name="app_type" value="ios" />
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="button" class="btn btn-primary waves-effect btn-sm m-t-30" id="data-from-btn" <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>disabled<?php } ?>> &nbsp;&nbsp;Save&nbsp;&nbsp;  </button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-lg-2"></label>
                    <div class="col-lg-10">
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>
</div>

<!-- APP ICON-FRONT MODAL START-->

<div class="modal fade is-Large-Modal" id="app-icon-modal-box1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/AppleTvAppIconFront" method="POST" enctype="multipart/form-data" id="app-icon-from1">
                <input type="hidden" name="app_type" value="appletv" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image Front </h4>
                </div>
                <div class="modal-body">
                    <div class="row is-Scrollable">
                        <!--APP ICON 1 START-->
                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-deafult-with-bg btn-sm" onclick="click_browse('avatarInput-1')">Browse </button>

                            <p class="help-block">Upload image size of 1280x768</p>
                            <input class="avatar-input" id="avatarInput-1" name="lunchicon-1" type="file" style="display:none;" onchange="fileSelectHandlerOne();">
                            <span id="file_error" class="error red" for="subdomain" style="display: block;"></span>

                        </div>
                        <div class="col-xs-12">
<?php
if (isset($app_icon_url_1) && $app_icon_url_1 != '') {
    $app_icon_img_1 = $app_icon_url_1;
}
?>
                            <div class="Preview-Block">
                                <div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
                                    <div class="m-b-10" id="avatar_preview_div_icon_1" >
                                        <img class="jcrop-preview" src="<?php echo $app_icon_img_1; ?>" style="height:300px;width:500px" id="preview_content_img_icon_1" rel="tooltip" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="x101" name="jcrop_lunchicon_one[x101]" />
                            <input type="hidden" id="y101" name="jcrop_lunchicon_one[y101]" />
                            <input type="hidden" id="x201" name="jcrop_lunchicon_one[x201]" />
                            <input type="hidden" id="y201" name="jcrop_lunchicon_one[y201]" />
                            <input type="hidden" id="w01" name="jcrop_lunchicon_one[w01]">
                            <input type="hidden" id="h01" name="jcrop_lunchicon_one[h01]">

                        </div>
                        <!--APP ICON 1 END-->


                    </div>


                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="app-icon-btn1" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- APP ICON-FRONT MODAL END-->


<!-- APP ICON-MIDDLE MODAL START-->

<div class="modal fade is-Large-Modal" id="app-icon-modal-box2"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/AppleTvAppIconMiddle" method="POST" enctype="multipart/form-data" id="app-icon-from2">
                <input type="hidden" name="app_type" value="appletv" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image Middle </h4>
                </div>
                <div class="modal-body">
                    <div class="row is-Scrollable">
                        <!--APP ICON 2 START-->

                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-deafult-with-bg btn-sm" onclick="click_browse('avatarInput-2')">Browse </button>

                            <p class="help-block">Upload image size of 1280x768</p>
                            <input class="avatar-input" id="avatarInput-2" name="lunchicon-2" type="file" style="display:none;" onchange="fileSelectHandlerTwo();">
                            <span id="file_error" class="error red" for="subdomain" style="display: block;"></span>

                        </div>
                        <div class="col-xs-12">
<?php
if (isset($app_icon_url_2) && $app_icon_url_2 != '') {
    $app_icon_img_2 = $app_icon_url_2;
}
?>
                            <div class="Preview-Block">
                                <div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
                                    <div class="m-b-10" id="avatar_preview_div_icon_2" >
                                        <img class="jcrop-preview" src="<?php echo $app_icon_img_2; ?>" style="height:300px;width:500px" id="preview_content_img_icon_2" rel="tooltip" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="x102" name="jcrop_lunchicon_two[x102]" />
                            <input type="hidden" id="y102" name="jcrop_lunchicon_two[y102]" />
                            <input type="hidden" id="x202" name="jcrop_lunchicon_two[x202]" />
                            <input type="hidden" id="y202" name="jcrop_lunchicon_two[y202]" />
                            <input type="hidden" id="w02" name="jcrop_lunchicon_two[w02]">
                            <input type="hidden" id="h02" name="jcrop_lunchicon_two[h02]">

                        </div>
                        <!--APP ICON 2 END-->


                    </div>


                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="app-icon-btn2" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- APP ICON-MIDDLE MODAL END-->

<!-- APP ICON-BACK MODAL START-->

<div class="modal fade is-Large-Modal" id="app-icon-modal-box3"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/AppleTvAppIconBack" method="POST" enctype="multipart/form-data" id="app-icon-from3">
                <input type="hidden" name="app_type" value="appletv" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image Back </h4>
                </div>
                <div class="modal-body">
                    <div class="row is-Scrollable">
                        <!--APP ICON 3 START-->
                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-deafult-with-bg btn-sm" onclick="click_browse('avatarInput-3')">Browse </button>

                            <p class="help-block">Upload image size of 1280x768</p>
                            <input class="avatar-input" id="avatarInput-3" name="lunchicon-3" type="file" style="display:none;" onchange="fileSelectHandlerThree();">
                            <span id="file_error" class="error red" for="subdomain" style="display: block;"></span>

                        </div>
                        <div class="col-xs-12">
<?php
if (isset($app_icon_url_3) && $app_icon_url_3 != '') {
    $app_icon_img_3 = $app_icon_url_3;
}
?>
                            <div class="Preview-Block">
                                <div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
                                    <div class="m-b-10" id="avatar_preview_div_icon_3" >
                                        <img class="jcrop-preview" src="<?php echo $app_icon_img_3; ?>" style="height:300px;width:500px" id="preview_content_img_icon_3" rel="tooltip" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="x103" name="jcrop_lunchicon_three[x103]" />
                            <input type="hidden" id="y103" name="jcrop_lunchicon_three[y103]" />
                            <input type="hidden" id="x203" name="jcrop_lunchicon_three[x203]" />
                            <input type="hidden" id="y203" name="jcrop_lunchicon_three[y203]" />
                            <input type="hidden" id="w03" name="jcrop_lunchicon_three[w03]">
                            <input type="hidden" id="h03" name="jcrop_lunchicon_three[h03]">

                        </div>

                        <!--APP ICON 3 END-->


                    </div>


                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="app-icon-btn3" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- APP ICON-BACK MODAL END-->


<div class="modal fade is-Large-Modal" id="splash-screen-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/appleTvSplashScreen" method="POST" enctype="multipart/form-data" id="splash-screen-from">
                <input type="hidden" name="app_type" value="appletv" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
                </div>
                <div class="modal-body">
                    <div class="row is-Scrollable">
                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarSplashInput')">Browse </button>

                            <p class="help-block">Upload image size of 1242x2208</p>
                            <input class="avatar-input" id="avatarSplashInput" name="splashicon" type="file" style="display:none;" onchange="splashFileSelectHandler();">
                            <span id="file_error_2" class="error red" for="subdomain" style="display: block;"></span>

                        </div>
                        <div class="col-xs-12">
<?php
if (isset($splash_screen_url) && $splash_screen_url != '') {
    $posterImg2 = $splash_screen_url;
}
?>

                            <div class="Preview-Block">
                                <div class="thumbnail m-b-0 jcrop-thumb" id="splashicon_div">
                                    <div class="m-b-10" id="avatar_preview_div_splash">
                                        <img class="jcrop-preview" src="<?php echo $posterImg2; ?>" style="height:500px;width:300px;" id="preview_content_img_splash" rel="tooltip" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="x11" name="jcrop_splashicon[x11]" />
                            <input type="hidden" id="y11" name="jcrop_splashicon[y11]" />
                            <input type="hidden" id="x21" name="jcrop_splashicon[x21]" />
                            <input type="hidden" id="y21" name="jcrop_splashicon[y21]" />
                            <input type="hidden" id="w1" name="jcrop_splashicon[w1]">
                            <input type="hidden" id="h1" name="jcrop_splashicon[h1]">

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="splash-screen-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--    FEATURE GRAPHIC MODAL START-->
<div class="modal fade is-Large-Modal" id="feature-graphic-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/appleFeatureGraphic" method="POST" enctype="multipart/form-data" id="feature-graphic-from">
                <input type="hidden" name="app_type" value="appletv" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
                </div>
                <div class="modal-body">
                    <div class="row is-Scrollable">
                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarFeatureGraphicInput')">Browse </button>

                            <p class="help-block">Upload image size of 1920x720</p>
                            <input class="avatar-input" id="avatarFeatureGraphicInput" name="featuregraphic" type="file" style="display:none;" onchange="featureFileSelectHandler();">
                            <span id="file_error_2" class="error red" for="subdomain" style="display: block;"></span>

                        </div>
                        <div class="col-xs-12">
<?php
if (isset($feature_graphic_url) && $feature_graphic_url != '') {
    $posterImg3 = $feature_graphic_url;
}
?>
                            <div class="Preview-Block">
                                <div class="thumbnail m-b-0 jcrop-thumb" id="splashicon_div">
                                    <div class="m-b-10" id="avatar_preview_div_feature">
                                        <img class="jcrop-preview" src="<?php echo $posterImg3; ?>" style="height:200px; width: 500px;" id="preview_content_img_feature" rel="tooltip" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="x5" name="jcrop_featuregraphic[x5]" />
                            <input type="hidden" id="y5" name="jcrop_featuregraphic[y5]" />
                            <input type="hidden" id="x25" name="jcrop_featuregraphic[x25]" />
                            <input type="hidden" id="y25" name="jcrop_featuregraphic[y25]" />
                            <input type="hidden" id="w5" name="jcrop_featuregraphic[w5]">
                            <input type="hidden" id="h5" name="jcrop_featuregraphic[h5]">

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="feature-graphic-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--    FEATURE GRAPHIC MODAL END-->

<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<script>
                                var jcrop_api;
                                function fileSelectHandlerOne() {
                                    var reqwidth = 1280;
                                    var reqheight = 768;
                                    var aspectRatio = reqwidth / reqheight;
                                    clearInfoAppIconOne();

                                    // get selected file
                                    var oFile = $('#avatarInput-1')[0].files[0];
                                    // hide all errors
                                    // check for image type (jpg and png are allowed)
                                    var rFilter = /^(image\/jpeg|image\/png)$/i;
                                    if (!rFilter.test(oFile.type)) {
                                        swal('Please select a valid image file (jpg and png are allowed)');
                                        $("#avatar_preview_div_icon_1").html('');
                                        $("#avatar_preview_div_icon_1").html('<img class="jcrop-preview" id="preview_content_img_icon_1"/>');
                                        $('button[type="submit"]').attr('disabled', 'disabled');
                                        return;
                                    }

                                    // preview element
                                    var oImage = document.getElementById('preview_content_img_icon_1');

                                    // prepare HTML5 FileReader
                                    var oReader = new FileReader();
                                    oReader.onload = function (e) {
                                        $('#file_error').hide();
                                        oImage.src = e.target.result;

                                        oImage.onload = function () { // onload event handler
                                            var w = oImage.naturalWidth;
                                            var h = oImage.naturalHeight;
                                            console.log("Width=" + w + " Height=" + h);
                                            if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                                                swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                                                $("#avatar_preview_div_icon_1").html('');
                                                $("#avatar_preview_div_icon_1").html('<img class="jcrop-preview" id="preview_content_img_icon_1"/>');
                                                $('button[type="submit"]').attr('disabled', 'disabled');
                                                return;
                                            }

                                            // destroy Jcrop if it is existed
                                            if (typeof jcrop_api != 'undefined') {
                                                jcrop_api.destroy();
                                                jcrop_api = null;
                                            }
                                            $('#preview_content_img_icon_1').width(oImage.naturalWidth);
                                            $('#preview_content_img_icon_1').height(oImage.naturalHeight);
                                            //setTimeout(function(){
                                            // initialize Jcrop
                                            $('#preview_content_img_icon_1').Jcrop({
                                                minSize: [reqwidth, reqheight], // min crop size,                                       
                                                boxWidth: 300,
                                                aspectRatio: aspectRatio, // keep aspect ratio 1:1
                                                bgFade: true, // use fade effect
                                                bgOpacity: .3, // fade opacity
                                                onChange: updateAppIconOne,
                                                onSelect: updateAppIconOne,
                                                onRelease: clearInfoAppIconOne
                                            }, function () {

                                                // use the Jcrop API to get the real image size
                                                bounds = this.getBounds();
                                                boundx = bounds[0];
                                                boundy = bounds[1];
                                                // Store the Jcrop API in the jcrop_api variable
                                                jcrop_api = this;
                                                //jcrop_api.animateTo([10, 10, 290, 410]);
                                                jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                                            });
                                            //},100);

                                        };
                                    };

                                    // read selected file as DataURL
                                    oReader.readAsDataURL(oFile);
                                }



</script>

<script>
    var jcrop_api;
    function fileSelectHandlerTwo() {
        var reqwidth = 1280;
        var reqheight = 768;
        var aspectRatio = reqwidth / reqheight;
        clearInfoAppIconTwo();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarInput-2')[0].files[0];
        //  console.log(oFile);

        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_icon_2").html('');
            $("#avatar_preview_div_icon_2").html('<img class="jcrop-preview" id="preview_content_img_icon_2"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_icon_2');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_icon_2").html('');
                    $("#avatar_preview_div_icon_2").html('<img class="jcrop-preview" id="preview_content_img_icon_2"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_icon_2').width(oImage.naturalWidth);
                $('#preview_content_img_icon_2').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_icon_2').Jcrop({
                    minSize: [reqwidth, reqheight],
                    //aspectRatio: 1,
                    boxWidth: 640,
                    boxHeight: 384,
                    bgFade: true,
                    bgOpacity: .3,
                    onChange: updateAppIconTwo,
                    onSelect: updateAppIconTwo,
                    onRelease: clearInfoAppIconTwo

                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([0, 0, reqwidth, reqheight]);
                });
                //},100);

            };
        };
        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>

<script>
    var jcrop_api;
    function fileSelectHandlerThree() {
        var reqwidth = 1280;
        var reqheight = 768;
        var aspectRatio = reqwidth / reqheight;
        clearInfoAppIconThree();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarInput-3')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_icon_3").html('');
            $("#avatar_preview_div_icon_3").html('<img class="jcrop-preview" id="preview_content_img_icon_3"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_icon_3');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_icon_3").html('');
                    $("#avatar_preview_div_icon_3").html('<img class="jcrop-preview" id="preview_content_img_icon_3"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_icon_3').width(oImage.naturalWidth);
                $('#preview_content_img_icon_3').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_icon_3').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    //maxSize: [reqwidth, reqheight],
                    boxWidth: 300,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateAppIconThree,
                    onSelect: updateAppIconThree,
                    onRelease: clearInfoAppIconThree
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>

<script>
    $('input').on('keyup', function(){
        if($(this).val().length >= 2) {
            $(this).parent().parent().parent().removeClass('has-error');
            $(this).parent().parent().parent().parent().removeClass('has-error');
        }
    });
    $('textarea').on('keyup', function(){
        if($(this).val().length >= 2) {
            $(this).parent().parent().parent().removeClass('has-error');
            $(this).parent().parent().parent().parent().removeClass('has-error');
        }
    });    
    
    var app_name = "<?php if(isset($appdata['app_name']) && trim($appdata['app_name'])){echo $appdata['app_name'];}?>";
   
    
    function appFormValidation() {
        var name = $("#app_name").val();
        var description = $("#description").val();

        if(!$.trim(name)){
            $("#app_name").parent().siblings().text('Please give a name for your app.');
         $("#app_name").parent().parent().parent().addClass('has-error');  
         return false;
        } 
        if($.trim(name).length > 50 ){
         $("#app_name").parent().siblings().text('max limit 50 characters');   
         $("#app_name").parent().parent().parent().addClass('has-error');  
         return false;
        }         
        if(!$.trim(description)){
         $("#description").parent().siblings().text('Please give a description for your app.');                 
         $("#description").parent().parent().parent().addClass('has-error');  
         return false;
        }
        if($.trim(description).length > 4000){
         $("#description").parent().siblings().text('max limit 4000 characters');     
         $("#description").parent().parent().parent().addClass('has-error');  
         return false;
        }                      
        return true;
    }    
    
    
    
    $('#app-icon-modal-btn1').click(function () {
        $("#app-icon-modal-box1").modal('show');
    });

    $('#app-icon-modal-btn2').click(function () {
        $("#app-icon-modal-box2").modal('show');
    });

    $('#app-icon-modal-btn3').click(function () {
        $("#app-icon-modal-box3").modal('show');
    });

    $('#splash-screen-modal-btn').click(function () {
        $("#splash-screen-modal-box").modal('show');
    });

    $('#feature-graphic-modal-btn').click(function () {
        $("#feature-graphic-modal-box").modal('show');
    });
    $('#android_check').click(function () {
        var studio_id = <?php echo $studio->id; ?>;
        if ($(this).is(":checked")) {
            $.post(HTTP_ROOT + "/template/getAndroidData", {'is_ajax': 1, 'studio_id': studio_id}, function (res) {
                //console.log(res);
                var result = $.parseJSON(res);
                $('#app_name').val(result.app_name);
                $('#description').val(result.description);
                $('#website').val(result.website);
                $('#email').val(result.email);
                $('#phone').val(result.phone);
            });
        } else {
            $('#app_name').val('');
            $('#description').val('');
            $('#website').val('');
            $('#email').val('');
            $('#phone').val('');
        }
    });
    $('#developer_id_checkbox').click(function () {
        if ($(this).is(":checked")) {
            $('#developer_id').show();
        } else {
            $('#developer_id').hide();
        }
    });

    $(document).on('click', '#app-icon-btn1', function () {
        var data_from = $('#data-from').serialize();
        $.post(HTTP_ROOT + "/template/updateAppInfoAppleTv", {'is_ajax': 1, 'data_from': data_from}, function (res) {
            if (res) {
                $('#app-icon-from1').submit();
            }
        });
    });

    $(document).on('click', '#app-icon-btn2', function () {
        var data_from = $('#data-from').serialize();
        $.post(HTTP_ROOT + "/template/updateAppInfoAppleTv", {'is_ajax': 1, 'data_from': data_from}, function (res) {
            if (res) {
                $('#app-icon-from2').submit();
            }
        });
    });

    $(document).on('click', '#app-icon-btn3', function () {
        var data_from = $('#data-from').serialize();
        $.post(HTTP_ROOT + "/template/updateAppInfoAppleTv", {'is_ajax': 1, 'data_from': data_from}, function (res) {
            if (res) {
                $('#app-icon-from3').submit();
            }
        });
    });

    $(document).on('click', '#data-from-btn', function () {
        var is_valid = appFormValidation();
        if(is_valid){          
        var data_from = $('#data-from').serialize();
        $.post(HTTP_ROOT + "/template/updateAppInfoAppleTv", {'is_ajax': 1, 'data_from': data_from}, function (res) {
            if (res) {
                $('#data-from-btn').html('Wait...');
                $('#data-from-btn').attr('disabled', 'disabled');
                $('#data-from-save').submit();
            }
        });
      }
    });
    
    
    var geography = $('#geography').val();
    if (geography != '' || geography != null) {
        $("#distribution_geography").children('option').each(function () {
            $(this).removeAttr('selected');
        });
    }

    $.each(geography.split(","), function (i, e) {
        $("#distribution_geography option[value='" + e + "']").prop("selected", true);
    });
    var category = $('#category').val();
    $('#category-dd option[value="' + category + '"]').attr('selected', true);
    var language = $('#language').val();
    $('#language-dd option[value="' + language + '"]').attr('selected', true);
    var rating_string = $('#rating').val();
    var k = 1;
    $.each(rating_string.split('-'), function (i, e) {
        $("#rating-dd-" + k + " option[value='" + e + "']").prop("selected", true);
        k++;
    });
    function openinmodal(url) {
        //$('.loaderDiv').show();   
        $.get(url, {"modalflag": 1}, function (data) {
            //$('.loaderDiv').hide();
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
    function click_browse(modal_file) {
        $("#" + modal_file).click();
    }
</script>
<script>
    var jcrop_api;
    function splashFileSelectHandler() {
        var reqwidth = 1242;
        var reqheight = 2208;
        var aspectRatio = reqwidth / reqheight;
        clearInfoSplash();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarSplashInput')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_splash").html('');
            $("#avatar_preview_div_splash").html('<img id="preview_content_img_splash"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_splash');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_splash").html('');
                    $("#avatar_preview_div_splash").html('<img class="jcrop-preview" id="preview_content_img_splash"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_splash').width(oImage.naturalWidth);
                $('#preview_content_img_splash').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_splash').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    // maxSize: [reqwidth, reqheight],
                    boxWidth: 500,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoSplash,
                    onSelect: updateInfoSplash,
                    onRelease: clearInfoSplash
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
    $('#splash-screen-btn').click(function () {
        $('#splash-screen-from').submit();
    });

</script>

<!--SELECT FILE HANDLER FOR FEATURE GRAPHIC START-->
<script>
    var jcrop_api;
    function featureFileSelectHandler() {
        var reqwidth = 1920;
        var reqheight = 720;
        var aspectRatio = reqwidth / reqheight;
        clearInfoSplash();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarFeatureGraphicInput')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_feature").html('');
            $("#avatar_preview_div_feature").html('<img class="jcrop-preview" id="preview_content_img_feature"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_feature');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_feature").html('');
                    $("#avatar_preview_div_feature").html('<img class="jcrop-preview" id="preview_content_img_feature"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_feature').width(oImage.naturalWidth);
                $('#preview_content_img_feature').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_feature').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    // maxSize: [reqwidth, reqheight],
                    boxWidth: 500,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateAppleFeatureGraphic,
                    onSelect: updateAppleFeatureGraphic,
                    onRelease: clearAppleFeatureGraphic
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
    $('#feature-graphic-btn').click(function () {
        $('#feature-graphic-from').submit();
    });
    
    (function () {
        $('body').on('click', '.left-icon', function (e) {
            // e.preventDefault();
            $(this).toggleClass('icon-arrow-up');
            $(this).parent().parent().parent().next().slideToggle(200);
        })
    })();
</script>
<!--SELECT FILE HANDLER FOR FEATURE GRAPHIC END-->
<div  class="loaderDiv">
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.gif" />
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style type="text/css">
    .loaderDiv{position: absolute;left: 45%;top:15%;display: none;}
    .box.box-primary{border:none !important;}
    /*button.close{
        border-radius: 15px 15px 15px 15px;
        -moz-border-radius: 15px 15px 15px 15px;
        -webkit-border-radius: 15px 15px 15px 15px;
        width: 25px;
        height: 25px;
        margin: 5px;
        border: 2px solid #000;
    }*/
</style>
