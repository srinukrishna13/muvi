<?php
$page_id = isset($page->id) ? $page->id : 0;
$page_title = isset($page->title) ? $page->title : '';
$page_content = isset($page->content) ? $page->content : '';
?>

<div class="row m-t-40">

    <div class="col-md-12">
        <div class="Block">
            <form action="<?php echo $this->createUrl('template/cmspage'); ?>" method="post" id="add_page" class="form-horizontal">
                <input type="hidden" name="page_id" id="page_id" value="<?php echo $page_id ?>" />
                <input type="hidden" id="page_name" class="form-control" name="page_name" placeholder="Enter a Page Name" value="Terms of Use" />
                <div class="form-group">
                    <label for="content" class="col-sm-2 control-label">Content:</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <textarea id="page_content" class="form-control input-sm" cols="40" rows="10" name="page_content" aria-hidden="true"><?php echo html_entity_decode($page->content); ?></textarea> 
                        </div>
                    </div>
                </div>

                <div class="form-group m-t-30">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary btn-sm">Save and Continue</button>
                    </div>
                </div>
            </form>	    
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<!-- Add New CMS Page -->
<script type="text/javascript">
    tinymce.init({
        selector: "#page_content",
        formats: {
            bold: {inline: 'b'},  
        },
        valid_elements : '*[*]',
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : false,
        menubar: false,
        element_format : 'html',
        extended_valid_elements : 'div[*], style[*]',
        valid_children : "+body[style]",        
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code",
        setup: function (editor) {
            editor.on('init', function () {
                if (tinymce.editors.length) {
                    $("#content_ifr").removeAttr('title');
                }
            });
        }
    });
    function openDeletePagepopup(page) {
        $('#del_page_id').val(page);
        $("#PageDeletePopup").modal('show');
    }

    function openPagepopup() {
        $("#PagePopup").modal('show');
        return false;
    }

    function openEditPagepopup(page) {
        $('#frm_loader').show();
        $.ajax({
            type: "POST",
            url: '<?php echo $this->createUrl('template/getpagedetails'); ?>',
            data: {'page': page},
            dataType: 'json'
        }).done(function (data) {
            $('#page_id').val(page);
            $('#page_name').val(data.title);
            $('#page_name').attr('readonly');
            $('#page_content').val(data.content);
            $('#frm_loader').hide();
        });

        $("#PagePopup").modal('show');
        return false;
    }

    function openViewPagepopup(page) {
        $('#view_loader').show();
        $.ajax({
            type: "POST",
            url: '<?php echo $this->createUrl('template/getpagedetails'); ?>',
            data: {'page': page},
            dataType: 'json'
        }).done(function (data) {
            $('#page_title').html(data.title);
            $('#popup_page_content').html(data.content);
            $('#view_loader').hide();

        });

        $("#PageViewPopup").modal('show');
        return false;
    }

    $(document).ready(function () {
        $("#add_page").validate({
            rules: {
                page_content: {
                    required: true
                },
            }
        });
    });
</script>

