<style>
#external-events{
        height: 50vh;
        overflow: hidden;
        border-right: 1px solid #d7d7d7;
    }
    #external-events .list-inline > li{
        width:33%;
        margin-bottom: 5px;
        overflow: hidden;
    }
    .tooltipevent{
        top:5px !important;
        left:0 !important;
        display:none;
        transition:.5s;
        color:#333;
    }
    .tooltipevent .caret{
        position: absolute;
        top: -4px;
        color: #d7d7d7;
        left: 40px;
        -ms-transform: rotate(180deg);
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
    }
    .fc-time-grid-event {
        overflow: initial !important;
    }
    .fc-time-grid-event .fc-content{
        display: none !important;
    }
    .fc-time-grid-event:hover .tooltipevent{
        display:block;
    }
</style>
<ul class="list-inline text-center">

<?php
if ($all_list) {
    $default_img = '/img/No-Image-Vertical-Thumb.png';
    $default_episode_img = '/img/No-Image-Horizontal.png';
    $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);

    foreach ($all_list AS $key => $details) {
        $movie_id = $details['id'];
        $stream_id = $details['stream_id'];
        //View Count 
        $views = 0;
        if ($details['is_episode']) {
            $views = isset($episodeViewcount[$stream_id]) ? $episodeViewcount[$stream_id] : 0;
            if (isset($episodePosters[$stream_id]) && $episodePosters[$stream_id]['poster_file_name'] != '') {
                $img_path = $postUrl . '/system/posters/' . $episodePosters[$stream_id]['id'] . '/episode/' . $episodePosters[$stream_id]['poster_file_name'];
            } else {
                $img_path = $default_episode_img;
            }
            $contentName = $details['name'] . ' - ' . $details['episode_title'];
        } else {

            $views = isset($viewcount[$movie_id]) ? $viewcount[$movie_id] : 0;
            if ($details['content_types_id'] == 2 || $details['content_types_id'] == 4) {
                $size = 'episode';
            } else {
                $size = 'thumb';
            }

            if (isset($posters[$movie_id]) && $posters[$movie_id]['poster_file_name'] != '') {
                $img_path = $postUrl . '/system/posters/' . $posters[$movie_id]['id'] . '/' . $size . '/' . $posters[$movie_id]['poster_file_name'];
            } else {
                $img_path = $default_img;
            }
            $contentName = $details['name'];
        }
        $plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink'] : str_replace(' ', "-", strtolower($details['name']));
        if (false === file_get_contents($img_path)) {
            if ($details['is_episode'] || $details['content_types_id'] == 2 || $details['content_types_id'] == 4) {
                $img_path = $default_episode_img;
            } else {
                $img_path = $default_img;
            }
        }
        ?>
                                <li> 
                                    <div class="Preview-Block fc-event"> 
                                        <div class="thumbnail m-b-0 thumbnail-list"> 
                                            <div class="relative m-b-5">
                                                <!--Place your Image Preview Here--> 

                                                <img  class="img-responsive" src="<?php echo $img_path; ?>" alt="<?php echo $details['name']; ?>" title="<?php echo $details['name']; ?>" > 
                                                <input type="hidden" class="event_title" id="image_name_<?php echo $details['stream_id']; ?>" name="event_title" value="<?php echo $contentName; ?>" />
                                                <input type="hidden" class="event_id" id="event_id_<?php echo $details['stream_id']; ?>" name="event_id" value="<?php echo $details['stream_id']; ?>" />
                                                <input type="hidden" class="content_types_id" id="content_types<?php echo $details['stream_id']; ?>" name="content_types_id" value="<?php echo $details['content_types_id']; ?>" />
                                                <!--This is your Image overlay if exists --> 
<!--                                                <div class="caption overlay overlay-white"> 
                                                    <div class="overlay-Text"> 
                                                        <div> <a href="#">
                                                                <span class="btn btn-primary icon-with-fixed-width">
                                                                    <em class="icon-check"></em> 
                                                                </span> 
                                                            </a> 
                                                        </div> 
                                                    </div>
                                                </div> -->

                                            </div> 
                                        </div>
                                    </div> 
                                </li> 
    <?php }
}
?>
                    </ul>

<script>
    
    
 $(function () {

        /* initialize the external events
         -----------------------------------------------------------------*/
        $('#external-events .fc-event').each(function () {
            //var test = $(this).children().find(".event_title").val();
            //alert(test);
            //alert($.trim($(".event_title").val())); 
            var externalEvents = {
                title: $.trim($(this).children().find(".event_title").val()),
                id: $.trim($(this).children().find(".event_id").val()),
                content_types_id: $.trim($(this).children().find(".content_types_id").val())
            };
            // creating event object and makes event text as its title
            $(this).data('externalEvents', externalEvents); //saving events into DOM
            // make the event draggable using jQuery UI
//            $(this).draggable({
//                zIndex: 999,
//                revert: true, // will cause the event to go back to its
//                revertDuration: 0 //  original position after the drag
//            });
        });

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
       
        $('#calendar').fullCalendar({
            header: {
                left: 'prev',
                center: '',
                right: 'agendaDay,agendaWeek,next'
            },
            defaultView: 'agendaWeek',
            allDaySlot: false,
            slotDuration: "00:30:01",
            axisFormat: 'H(:mm)', //,'h(:mm)tt',


            buttonText: {
                today: 'Today',
                month: 'Month',
                week: 'Week',
                day: 'Day'
            },
            eventLimit: 3,
            //Random default events
            
            events: [
<?php foreach ($all_event as $key => $val) { ?>
                    {
                        id:"<?php echo $val['id']; ?>",
                        title: "<?php echo addslashes($val['event_name']); ?>",
                        start: "<?php echo $val['start_time']; ?>",
                        end: "<?php echo $val['end_time']; ?>",
                        backgroundColor: "#17a2ac", //Primary (light-blue)
                        borderColor: "#17a2ac", //Primary (light-blue)

                        textColor: 'white',
                        color: 'black'

                    },
<?php } ?>

            ],
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            eventDataTransform: function(event){  //stop dragging of events inside the full calendar
              
            event.editable = false;   
            return event;    
            },
            eventRender: function (event, element) {
                element.append('<div id="tool'+event.id+'" class="tooltipevent" style="padding:5px;min-height:35px;background:#fff;border:1px solid #d7d7d7;position:relative;z-index:10001;">' +'<div id="name_'+event.id+'">'+event.title+'</div><span class="caret"></span></div>');
                //element.tooltip("test");
                element.attr('href', 'javascript:void(0);');
                element.click(function () {
                    $("#rweekday").hide();
                     $("#rweekend").hide();
                     $("#rhours").hide();
                    // console.log(event);
                    //console.log(event.title);
                    //console.log(moment(event.start).format('YYYY-MM-DD HH:mm:ss'));
                    if (event.title != '' && moment(event.start).format('YYYY-MM-DD HH:mm:ss') != '')
                    {

                        $("#myModalLabel").text("Edit Content");
                        $("#submit_content").text("Edit");
                        //$("#delete_content").css("display","inline-block");
                         $("#delete_content").show();
                    }
                    
                    $.ajax({
                        method: "POST",
                        url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/editEvent',
                        data: {'event_title': event.title, 'event_start': moment(event.start).format('YYYY-MM-DD HH:mm:ss'), 'event_end': moment(event.end).format('YYYY-MM-DD HH:mm:ss')},
                        dataType: "json",
                        success: function (res) {

                            document.getElementById("ls_schedule_id").value = res.event_id;
                            document.getElementById("content_types_id").value = res.stream_type;
                            document.getElementById("movie_id").value = res.movie_id;
                        }
                    });
                    $("#startDate").val(moment(event.start).format('YYYY-MM-DD'));
                    $("#startTime").val(moment(event.start).format('HH:mm:ss'));
                    $("#endTime").val(moment(event.end).format('YYYY-MM-DD HH:mm:ss'));
                    $("#search_text").val(event.title);

                    //$("#eventContent").dialog({ modal: true, title: event.title, width:500});
                   $("#repeat_weekday").val("");
                    $("#repeat_weekend").val("");
                    $('#eventContent').modal('show');
                    
                    //$('#myModalLabel').html(event.title);
                    $('#search_text').autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/eventAutocomplete/movie_name?term=' + $('#search_text').val(),
                                dataType: "json",
                                init: function () {
                                },
                                success: function (data) {
                                    if (data.length == 0) {
                                        // $('#movie_id').val('');
                                        //  $('#preview_poster').html('');
                                    } else {
                                        response($.map(data.movies, function (item) {
                                            return {
                                                label: item.movie_name,
                                                value: item.movie_id,
                                                ctype: item.stream_type,
                                                duration: item.duration
                                            }
                                        }));
                                    }
                                }
                            });
                        },
                        select: function (event, ui) {
                            event.preventDefault();
                            $("#search_text").val(ui.item.label);

                        },
                        focus: function (event, ui) {
                            var lb = ui.item.label;
                            $("#search_text").val(ui.item.label);
                            $("#movie_id").val(ui.item.value);
                            $("#content_types_id").val(ui.item.ctype);
                            if (lb.indexOf("Episode") > -1) {
                                $("#is_episode").val(1);
                            } else {
                                $("#is_episode").val(0);
                            }
                            event.preventDefault(); // Prevent the default focus behavior.
                        }
                    });
                });
            },
            dayClick: function (date, allDay, jsEvent, view) {
                $("#delete_content").hide();
                var start_day = date.format("YYYY-MM-DD");
                var start_time = date.format("HH:mm:ss");
                //alert('Clicked on: ' + date.format("hh:mm:ss"));
                document.getElementById("add_event").reset();
                $("#startDate").val(start_day);
                $("#startTime").val(start_time);
                //$("#eventContent").dialog({ modal: true, title: , width:500});
                $("#rweekday").show();
                $("#rweekend").show();
                $("#rhours").show();
                $("#repeat_weekday").val("");
                 $("#repeat_weekend").val("");
                $('#eventContent').modal('show');
                $('#myModalLabel').html("Add Content");
                $('#search_text').autocomplete({
                    source: function (request, response) {

                        $.ajax({
                            url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/eventAutocomplete/movie_name?term=' + $('#search_text').val(),
                            dataType: "json",
                            init: function () {
                            },
                            success: function (data) {
                                if (data.length == 0) {
                                    // $('#movie_id').val('');
                                    //  $('#preview_poster').html('');
                                } else {
                                    response($.map(data.movies, function (item) {

                                        return {
                                            label: item.movie_name,
                                            value: item.movie_id,
                                            ctype: item.stream_type,
                                            duration: item.duration
                                        }


                                    }));
                                }
                            }
                        });
                    },
                    select: function (event, ui) {
                        event.preventDefault();
                        $("#search_text").val(ui.item.label);
                    },
                    focus: function (event, ui) {
                        var lb = ui.item.label;
                        //$("#search_text").val(ui.item.label);
                        $("#movie_id").val(ui.item.value);
                        $("#content_types_id").val(ui.item.ctype);
                        if (lb.indexOf("Episode") > -1) {
                            $("#is_episode").val(1);
                        } else {
                            $("#is_episode").val(0);
                        }
                        event.preventDefault(); // Prevent the default focus behavior.
                    }
                });

            },
//            drop: function (date, allDay, externalEvents) { // this function is called when something is dropped
//                //console.log($(this).data('externalEvents').title);
//
//                var originalEventObject = $(this).data('externalEvents');
//                // we need to copy it, so that multiple events don't have a reference to the same object
//                var copiedEventObject = $.extend({}, originalEventObject);
//                // assign it the date that was reported
//                copiedEventObject.title = externalEvents.title;
//                copiedEventObject.start = moment(date).format('YYYY-MM-DD HH:mm:ss');
//                copiedEventObject.allDay = allDay;
//                copiedEventObject.backgroundColor = $(this).css("#40B0A6");
//                copiedEventObject.borderColor = $(this).css("#515253");
//
//                //update database for events.
//                var id = $(this).data('externalEvents').id;
//                var title = $(this).data('externalEvents').title;
//                var start_date = moment(date).format('YYYY-MM-DD HH:mm:ss');
//                var content_type_id = $(this).data('externalEvents').content_types_id;
//                var selected_channel = $("#channel_list").val();
//               // alert(selected_channel);
//                if (selected_channel != 0) {
//                    $.ajax({
//                        url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/updateDroppedImage',       
//                        cache: false,
//                        data: {'id': id, 'title': title, 'start_date': start_date, 'content_type': content_type_id,'channel_id': selected_channel},
//                        dataType:json,
//                        success: function (res) {
//                           showLoader1(1);
//                            if(res == "error"){
//                                swal('Error in adding Event');
//                            }else{
//                               
//                                $('#calender_container').html();
//                                 swal('Event added successfully');
//                                $('#calender_container').html(res);
//                            }
//                        }
//                    });
//
//                   
//                } else
//                {
//                    swal("Please Select a Channel");
//                }
//                //update database for events.
//                // render the event on the calendar                
//                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
//
//                
//            },
//           
//            eventDragStart: function (event, jsEvent, ui, view) {
//
//                if (!copyKey)
//                    return;
//                var eClone = {
//                    title: event.title,
//                    start: event.start,
//                    end: event.end
//                };
//                $('#calendar').fullCalendar('renderEvent', eClone);
//
//            }

        });

      
        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create events
            var event = $("<div />");
            event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
            event.html(val);
            $('#external-events').prepend(event);

            //Add draggable funtionality
            ini_events(event);

            //Remove event from text input
            $("#new-event").val("");
        });
    });
       
function saveevent()
{
    document.getElementById('submit_content').disabled = 1;
     var end_time=$("#endTime").val();
        if(end_time!=''){
    var ls_schedule=$("#ls_schedule_id").val();
    var formData = new FormData($("#add_event")[0]);
    
    $.ajax({
    url:"<?php echo Yii::app()->getBaseUrl(true); ?>/template/saveEvent",
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (result) {
             document.getElementById('submit_content').disabled = 0;
                if ($.trim(result) === "Timingerror") {
                swal('Selected date should be greater than current date');
                }
                else if($.trim(result) === "Nochannel")
                {
                  swal('Please select a channel');  
                }
                else if($.trim(result) === "Eventexists")
                {
                 swal('Event already exists');      
                }
                else if($.trim(result) === "NoDetail")
                {
                  swal('Please enter the event details');     
                }
                else if($.trim(result) === "Eventstarted")
                {
                     swal('Event started already');    
                }
                else if($.trim(result) === "DurationError")
                 {
                   swal("Please upload the video again");    
                 }
                else {
                   
                    $("#calender_container").html();
                    $("#cancel_content").trigger("click");
                     if(ls_schedule=='')
                    {
                       swal('Event added successfully'); 
                        $("#calender_container").html(result);
                    }
                    else
                    {
                      swal('Event updated successfully');   
                       $("#calender_container").html(result);
                    }
                   
                    
                }
            }
        });
        }else
        {
        swal("Please upload the video again");
        }
}
    </script>
    