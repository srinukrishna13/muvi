<style>
    textarea::placeholder{
        color:#000000 !important;
    }
    .preloader{
        display: none;
    }
    .error{
        color: #f55753;
    }
</style>
<div class="form-group has-error text-center">
    <div class="fg-line">
        <label class="control-label" id="notification_exceed" for="inputError1"></label>
       </div>
</div>

<form class="form-horizontal" role="form" name="notificationcenter" id="notifications-form"  data-toggle='validator'>
    <hr>
    <div class="row">

        <div class="col-xs-12">
            <div class="fg-line">
                <textarea class="form-control" rows="5"  name="notification" id="message" placeholder="Enter Your message here (max limit 128 characters)"></textarea>
            </div>
        </div>

        <div class="preloader pls-blue loader_notification">
            <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20"/>
            </svg>
        </div>
        <div class="col-xs-12 text-right">
            <button type="reset" value="reset" class="btn btn-primary btn-default m-t-10">Reset</button>
            <button type="submit" value="send" class="btn btn-primary btn-default m-t-10" id="message-btn">Submit</button>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $('#message-btn').click(function () {
            $("#notifications-form").validate({
                rules: {
                    notification: {
                        required: true,
                        maxlength: 128
                    }
                },
                message: {
                    notification: {
                        required: "Please enter notification to send.",
                        maxlength: "You can send notification upto 128 characters."
                    }
                },
                submitHandler: function () {
                    sendNotification();
                },
                errorPlacement: function(error, element) {
                    error.addClass('red');
                    error.insertAfter(element.parent());
                }
            }); 
        });
    });

    function sendNotification() {
        var url = HTTP_ROOT + "/template/NotificationCenter";
        var message = $('#message').val();
        if (message != '') {
            $('.loader_notification').show();
            $.post(url, {'message': message, "action": "send"}, function (data) {
                var res = JSON.parse(data);
                $('.loader_notification').hide();
                if (res.status == 'success') {
                    swal(res.message, '', "success");
                    $('#message').val('');
                } else {
                    $('#notification_exceed').html(res.message);
                    $('#message').val('');
                }
            });
        } else {
            //$('#empty-notification').html('Please enter notification to send');
        }
      }
</script>